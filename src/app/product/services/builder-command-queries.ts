import {ApiAction} from 'src/app/core/services/command-queries/api-action';

export class GetNewArrangementFieldConfigs extends ApiAction {
  constructor() {
    super('GetNewArrangementFieldConfigs');
  }
}

export class DeployProduct extends ApiAction {
  sessionId: string;
  productGroupId: string;

  constructor(sessionId, productGroupId) {
    super('DeployProduct');
    this.sessionId = sessionId;
    this.productGroupId = productGroupId;
  }
}

export class UpdateFieldValues extends ApiAction {
  sessionId: string;
  productGroupId: string;
  propertyId: string;
  currencyId: string;
  fieldValues: any;

  constructor(sessionId, productGroupId, propertyId, currencyId, fieldValues) {
    super("UpdateFieldValues");
    this.sessionId = sessionId;
    this.productGroupId = productGroupId;
    this.propertyId = propertyId;
    this.currencyId = currencyId;
    this.fieldValues = fieldValues;
  }
}

export class GetPropertySelectedFieldValues extends ApiAction {
  SessionId: string;
  ProductGroupId: string;

  constructor(sessionId, productGroupId) {
    super('GetPropertySelectedFieldValues');
    this.SessionId = sessionId;
    this.ProductGroupId = productGroupId;
  }
}

export class SelectProduct extends ApiAction {
  sessionId: string;
  productId: string;

  constructor(sessionId, productId) {
    super('SelectProduct');
    this.sessionId = sessionId;
    this.productId = productId;
  }
}

export class ResetSession extends ApiAction {
  sessionId: string;

  constructor(sessionId) {
    super('ResetSession');
    this.sessionId = sessionId;
  }
}

export class GetArragmentFieldsBySessionId extends ApiAction {

}
