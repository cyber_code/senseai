import { Injectable } from '@angular/core';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Observable } from 'rxjs';
import { SelectProduct, ResetSession, GetNewArrangementFieldConfigs, DeployProduct, GetPropertySelectedFieldValues, UpdateFieldValues} from './builder-command-queries';
import { CommandMethod } from 'src/app/shared/command-method';
import { SignalRService } from './signalr.service';

@Injectable({
  providedIn: 'root'
})
export class BuilderService {
  private dataUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService,
    // private signalRService: SignalRService
  ) {
    this.dataUrl = `${configurationService.serverSettings.apiUrl}AAProductBuilder/`;
  }

  getNewArrangementFieldConfigs(): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetNewArrangementFieldConfigs());
  }

  deployProduct(seesionId, productGroupId): Observable<any> {
    return this.httpExecutor.executeCommand<any>(this.dataUrl, new DeployProduct(seesionId, productGroupId), CommandMethod.POST);
  }

  selectProduct(sessionId, productId): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new SelectProduct(sessionId, productId));
  }

  getPropertySelectedFieldValues(sessionId, productGroupId) : Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetPropertySelectedFieldValues(sessionId, productGroupId));
  } 

  updateFieldValues(sessionId, productGroupId, propertyId, currencyId, fieldValues): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.dataUrl, 
      new UpdateFieldValues(sessionId, productGroupId, propertyId, currencyId, fieldValues), 
      CommandMethod.POST
    );
  }

  resetSession(sessionId): Observable<any> {
    return this.httpExecutor.executeCommand<any>(this.dataUrl, new ResetSession(sessionId), CommandMethod.POST);
  }

  removeBotSession() {
    if (localStorage.getItem('botSessionId')) {
      this.resetSession(localStorage.getItem('botSessionId')).subscribe(() => {
        localStorage.removeItem('botSessionId');
      });
    }
  }
}
