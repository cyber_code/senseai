import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { SessionService } from 'src/app/core/services/session.service';
import { DataFlowService } from './../../data-management/services/data-flow.service';
import { BuilderService } from './builder.service';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  private hubConnection: signalR.HubConnection;
  public arrangementFields: any = [];
  public propAndAttributes: any = [];
  private apiUrl;
  private previousData;

  constructor(
    private sessionService: SessionService,
    private builderService: BuilderService,
    private configuration: ConfigurationService,
    private dataflow: DataFlowService
  ) {
    this.apiUrl = `${configuration.serverSettings.apiUrl}`;
  }

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl(this.apiUrl + 'notificationhub', {
        transport: signalR.HttpTransportType.LongPolling,
        accessTokenFactory: () => this.sessionService.getToken()
      })
      .build();
    this.hubConnection.onclose(function() {
      console.log('connecition closed');
    });

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }

  public addFieldValue = () => {
    this.hubConnection.on('AddFieldValue', data => {
      let propertyIndex = -1;
      let attributeIndex = -1;
      let property = this.propAndAttributes.find((el, i) => {
        if (
          el.property.propertyId === data.propertyId &&
          (el.property.currencyId === data.currencyId || (el.property.currencyId === '' && data.currencyId === 'N/A'))
        ) {
          propertyIndex = i;
          return true;
        }
        return false;
      });

      if (property) {
        let attributes = property.attributes;
        let attribute = attributes.find((attr, i) => {
          if (attr.name === data.fieldId) {
            attributeIndex = i;
            return true;
          }
          return false;
        });
        if (attribute && attribute.multiValueIndex === data.multiValueIndex && attribute.subValueIndex === data.subValueIndex) {
          attribute.value = data.value;
          this.runScroll('property' + propertyIndex + '_attribute' + attributeIndex);
          let fields = property.attributes.map(attr => {
            return {
              fieldId: attr.name,
              multiValueIndex: attr.multiValueIndex,
              subValueIndex: attr.subValueIndex,
              value: attr.value
            };
          });
          this.builderService
            .updateFieldValues(
              localStorage.getItem('botSessionId'),
              'C4439F79-685F-4B66-94B8-3C56C3C0B891',
              property.property.propertyId,
              property.property.currencyId,
              fields
            )
            .subscribe(res => {});
        }
      }
    });
  };

  private runScroll(elementId) {
    if (document.querySelector('#' + elementId)) {
      const offset = document.getElementById(elementId).offsetTop;
      document.getElementById('main-window').scrollTop = offset - 5;
    }
  }

  public headerQuestionAnswered = () => {
    return this.hubConnection.on('HeaderQuestionsAnswered', data => {
      this.arrangementFields = data;
    });
  };
  public SaveRecordedWorkflow = () => {
    return this.hubConnection.on('SaveRecordedWorkflow', data => {
      if (!this.previousData || this.previousData !== data) {
        this.previousData = data;
        this.dataflow.recordFinished(data);
      }
    });
  };
  public CancelRecordedWorkflow = () => {
    return this.hubConnection.on('CancelRecordedWorkflow', data => {
      if (!this.previousData || this.previousData !== data) {
        this.previousData = data;
        this.dataflow.recordCanceled(data);
      }
    });
  };

  public JiraSynchronizationProgress(cb) {
    this.hubConnection.on('JiraSynchronizationProgress', data => {
      cb(data);
      // if (!this.previousData || this.previousData !== data) {
      //   this.previousData = data;
      //   this.dataflow.recordCanceled(data);
      // }
    });
  }

  public configFields = () => {
    return this.hubConnection.on('ConfigFields', data => {
      if (
        !this.propAndAttributes.find(
          propAndAttribute =>
            propAndAttribute.property.propertyId === data.property.propertyId &&
            propAndAttribute.property.currencyId === data.property.currencyId
        )
      ) {
        this.propAndAttributes.push(data);
      }
    });
  };

  public resetProduct() {
    this.propAndAttributes = [];
    this.arrangementFields = [];
  }
}
