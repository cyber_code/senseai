export class PropertyAttributes {
  propertyName: string;
  visibility: boolean;
  attributes: Array<Object>;
}
