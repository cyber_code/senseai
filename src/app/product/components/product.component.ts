import {Component, OnInit, OnDestroy} from '@angular/core';
import {DataManagementService} from 'src/app/data-management/services/data-management.service';
import {BuilderService} from '../services/builder.service';
import {Guid} from 'src/app/shared/guid';
import {Location} from '@angular/common';
import { SignalRService } from '../services/signalr.service';
import { MessageType } from 'src/app/core/models/message.model';
import {MessageService} from 'src/app/core/services/message.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit, OnDestroy {
  public BotSessionId;
  public oldAttributes: any;
  public editedProperty: any;

  constructor(
    public dataManagementService: DataManagementService,
    public builderService: BuilderService,
    public signalRService: SignalRService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.showChatBtn();
    if (!localStorage.getItem('botSessionId')) {
      localStorage.setItem('botSessionId', Guid.newGuid());
    }
    this.BotSessionId = localStorage.getItem('botSessionId');
    this.signalRService.startConnection();
    this.signalRService.addFieldValue(); 
    this.signalRService.headerQuestionAnswered();
    this.signalRService.configFields();
    this.getPropertySelectedFieldValues();
  }


  onClickEdit(propAttr) {
    if (this.signalRService.propAndAttributes.find(propAndAttr => propAndAttr.property.isEditMode)) {
      this.messageService.sendMessage({
        text: 'You should finish with the configuration of this property in order to proceed to the others!', 
        type: MessageType.Warning
      });
      return;
    }
    this.editedProperty = _.cloneDeep(propAttr);
    propAttr.property.isEditMode = true;
  }

  onClickCancel(propAttr) {
    propAttr.property = this.editedProperty.property;
    propAttr.attributes = this.editedProperty.attributes;
    let refAttr = propAttr.allAttributes.find(attr => attr.currencyId === propAttr.property.currencyId);
    refAttr.attributes = this.editedProperty.attributes;
    this.setEditModeToFalse(propAttr);
  }

  onClickSave(propAttr) {
    let fields = propAttr.attributes.map(attr => {
      return {
        fieldId: attr.name,
        multiValueIndex: attr.multiValueIndex,
        subValueIndex: attr.subValueIndex,
        value: attr.value
      }
    })
    this.builderService
      .updateFieldValues(
        this.BotSessionId,
        'C4439F79-685F-4B66-94B8-3C56C3C0B891',
        this.editedProperty.property.propertyId,
        this.editedProperty.property.currencyId,
        fields
      ).subscribe(res => {
        if (res) {
          this.messageService.sendMessage({text: 'Changes have been saved!', type: MessageType.Success});
        }
      });
    this.setEditModeToFalse(propAttr);
  }

  setEditModeToFalse(propAttr) {
    propAttr.property.isEditMode = false;
  }

  onChangeField(event, attribute) {
    if (event.target.value) {
      attribute.value = event.target.value;
    }
  }

  deployProduct() {
    this.builderService
      .deployProduct(this.BotSessionId, 'C4439F79-685F-4B66-94B8-3C56C3C0B891')
      .subscribe(res => {
        if (res) {
          this.messageService.sendMessage({text: 'Deployment of the product has finished successfully!', type: MessageType.Success});
        } 
      });
  }

  getPropertySelectedFieldValues() {
    this.builderService
      .getPropertySelectedFieldValues(this.BotSessionId, 'C4439F79-685F-4B66-94B8-3C56C3C0B891')
      .subscribe(res => {
        if (res) {
          this.signalRService.arrangementFields = res.headerFieldValueConfigs;
          let propertiesConfig = res.propertyFieldValueConfigs;
          let configs = [];
          propertiesConfig.map(propConfig => {
            let fieldValues = propConfig.fieldValues.filter(field => field);
            let fieldConfigs = propConfig.fieldConfigs;
            if (fieldValues.length === 0) {
              configs.push({
                attributes: fieldConfigs,
                property: propConfig.property
              });
            } else {
              let attributes = fieldValues.map(fieldValue => {
                let configField = fieldConfigs.find(field => field.name === fieldValue.fieldId);
                let sameIdFields = fieldValues.filter(field => configField && field.fieldId === fieldValue.fieldId && 
                                                              field.propertyId === fieldValue.propertyId && 
                                                              field.currencyId === fieldValue.currencyId && (
                                                                !configField.secondGroupId || 
                                                                field.multiValueIndex === fieldValue.multiValueIndex 
                                                              ));
                return Object.assign({}, {
                  ...configField, 
                  value: fieldValue.value, 
                  multiValueIndex: fieldValue.multiValueIndex, 
                  subValueIndex: fieldValue.subValueIndex, 
                  showDeleteButton: (sameIdFields.length > 1 && 
                    (configField.expandSingleMultiValue || 
                    configField.expandSingleSubValue || 
                    configField.expandMultiValue || 
                    configField.expandSubValue)
                  )
                });
              });
              configs.push({
                attributes: attributes,
                property: propConfig.property
              });
            }
            this.setGroupedConfigurations(configs);
          });
        }
    });
  }
  
  private setGroupedConfigurations(configs) {
    let groupedConfigs = [];
    configs.map(config => {
      let sameProperty = groupedConfigs.find(conf => conf.property.propertyId === config.property.propertyId);
      if (sameProperty) {
        sameProperty.allAttributes.push({
          currencyId: config.property.currencyId,
          attributes: config.attributes
        }) 
        sameProperty.currencies.push(config.property.currencyId)
      } else {
        groupedConfigs.push({
          ...config,
          allAttributes: [{
            currencyId: config.property.currencyId,
            attributes: config.attributes
          }],
          currencies: [config.property.currencyId]
        });
      }
    });
    this.signalRService.propAndAttributes = groupedConfigs;
  }

  selectedCurrency(currency, propAndAttr) {
    if (currency && currency !== "" && currency !== propAndAttr.property.currencyId) {
      propAndAttr.property.currencyId = currency;
      let attributeObj = propAndAttr.allAttributes.find(attr => attr.currencyId === currency);
      propAndAttr.attributes = attributeObj.attributes;
    }
  }


  onClickDeleteButton(attribute, propAndAttr) {
    if (!propAndAttr.property.isEditMode) {
      return;
    }
    let attributes;
    let newAttributes = [];
    if (propAndAttr) {
      attributes = propAndAttr.attributes;
    }
    if (attribute.expandSingleMultiValue && attributes) {
      let sameGroupAttrs = attributes.filter(attr => attr.name === attribute.name);
      for (let i = 0; i < attributes.length; i ++){
        let attr = attributes[i];
        if (attr.name === attribute.name && sameGroupAttrs.length <= 2) {
          attr.showDeleteButton = false;
        }
        if (attr.name === attribute.name && attr.multiValueIndex > attribute.multiValueIndex) {
          attr.multiValueIndex = attr.multiValueIndex - 1;
          newAttributes.push(attr);
        } else if (attr.name !== attribute.name || attr.multiValueIndex !== attribute.multiValueIndex) {
          newAttributes.push(attr);
        }
      }
    } else if (attribute.secondGroupId && attributes) {
      let secondGroupAttrs = attributes.filter(attr => attr.secondGroupId === attribute.secondGroupId && 
        attr.firstGroupId === attribute.firstGroupId && attr.multiValueIndex === attribute.multiValueIndex);
      let maxIndex = Math.max.apply(Math, secondGroupAttrs.map(function(attr) { return attr.subValueIndex; }))
      for (let i = 0; i < attributes.length; i++) {
        let attr = attributes[i];
        if (attr.firstGroupId === attribute.firstGroupId && attr.secondGroupId === attribute.secondGroupId 
          && attr.multiValueIndex === attribute.multiValueIndex && maxIndex <= 2) {
          attr.showDeleteButton = false;
        }
        if (attr.firstGroupId === attribute.firstGroupId && attr.secondGroupId === attribute.secondGroupId && 
          attr.multiValueIndex === attribute.multiValueIndex && attr.subValueIndex > attribute.subValueIndex) {
            attr.subValueIndex = attr.subValueIndex - 1;
            newAttributes.push(attr);
        } else if (attr.firstGroupId !== attribute.firstGroupId || attr.secondGroupId !== attribute.secondGroupId ||
          attr.multiValueIndex !== attribute.multiValueIndex || attr.subValueIndex !== attribute.subValueIndex) {
            newAttributes.push(attr);
        }
      }
    } else if (attribute.firstGroupId && attributes) {
      let firstGroupAttrs = attributes.filter(attr => attr.firstGroupId === attribute.firstGroupId);
      let maxIndex = Math.max.apply(Math, firstGroupAttrs.map(function(attr) { return attr.multiValueIndex; }));
      for (let i = 0; i < attributes.length; i++) {
        let attr = attributes[i];
        if (attr.firstGroupId === attribute.firstGroupId && !attr.secondGroupId && maxIndex <= 2) {
          attr.showDeleteButton = false;
        } 
        if (attr.firstGroupId === attribute.firstGroupId && attr.multiValueIndex > attribute.multiValueIndex) {
          attr.multiValueIndex = attr.multiValueIndex - 1;
          newAttributes.push(attr);
        } else if (attr.firstGroupId !== attribute.firstGroupId || attr.multiValueIndex !== attribute.multiValueIndex) {
          newAttributes.push(attr);
        }
      }
    }
    propAndAttr.attributes = newAttributes;
  }

  onClickExpandButton(attribute, propAndAttr) {
    if (!propAndAttr.property.isEditMode) {
      return;
    }
    attribute.showDeleteButton = true;
    let attributes;
    let newAttributes = [];
    let isGroupAdded = false;
    if (propAndAttr) {
      attributes = propAndAttr.attributes;
    }
    if (attribute.expandSingleMultiValue && attributes) {
      let sameGroupAttrs = attributes.filter(attr => attr.name === attribute.name);
      let maxIndex = Math.max.apply(Math, sameGroupAttrs.map(function(attr) { return attr.multiValueIndex; }));
      for (let i = 0; i < attributes.length - 1; i++) {
        newAttributes = newAttributes.concat(attributes[i]);
        if (attributes[i].name === attribute.name && attributes[i+1].name !== attribute.name) {
          isGroupAdded = true;
          newAttributes = newAttributes.concat({...attribute, value: '', showDeleteButton: true, multiValueIndex: maxIndex + 1});
        }
      }
      newAttributes = newAttributes.concat(attributes[attributes.length - 1]);
      if (!isGroupAdded) {
        newAttributes = newAttributes.concat({...attribute, value: '', showDeleteButton: true, multiValueIndex: maxIndex + 1});
      }
    } else if (attribute.secondGroupId && attributes) {
      let newSecondGroupAttrs = [];
      let secondGroupAttrs = attributes.filter(attr => attr.secondGroupId === attribute.secondGroupId && 
        attr.firstGroupId === attribute.firstGroupId && attr.multiValueIndex === attribute.multiValueIndex);
      let maxIndex = Math.max.apply(Math, secondGroupAttrs.map(function(attr) { return attr.subValueIndex; }))
      for (let i = 0; i < attributes.length; i++) {
        let attr = attributes[i];
        if (attr.secondGroupId === attribute.secondGroupId && attr.firstGroupId === attribute.firstGroupId &&
          attr.multiValueIndex === attribute.multiValueIndex &&  !newSecondGroupAttrs.find(el => el.name === attr.name)) {
            newSecondGroupAttrs.push({...attr, value: '', subValueIndex: maxIndex + 1, showDeleteButton: (attribute.name === attr.name)});
          }
      }
      for (let j = 0; j < attributes.length - 1; j++) {
        newAttributes = newAttributes.concat(attributes[j]);
        if (attributes[j].firstGroupId === attribute.firstGroupId && 
          attributes[j].secondGroupId === attribute.secondGroupId && 
          attributes[j].multiValueIndex === attribute.multiValueIndex &&
          attributes[j + 1].firstGroupId === attribute.firstGroupId && 
          attributes[j + 1].secondGroupId !== attribute.secondGroupId
        ) {
          newAttributes = newAttributes.concat(newSecondGroupAttrs);
          isGroupAdded = true;
        }
      }
      newAttributes = newAttributes.concat(attributes[attributes.length - 1]);
      if (!isGroupAdded) {
        newAttributes = newAttributes.concat(newSecondGroupAttrs);
      }
    } else if (attribute.firstGroupId && attributes) {
      let newFirstGroupAttrs = [];
      let firstGroupAttrs = attributes.filter(attr => attr.firstGroupId === attribute.firstGroupId);
      let maxIndex = Math.max.apply(Math, firstGroupAttrs.map(function(attr) { return attr.multiValueIndex; }))
      for (let i = 0; i < attributes.length; i++) {
        let attr = attributes[i];
        if (attr.firstGroupId === attribute.firstGroupId &&
          !newFirstGroupAttrs.find(el => el.name === attr.name)) {
            newFirstGroupAttrs.push({...attr, value: '', multiValueIndex: maxIndex + 1, showDeleteButton: (attribute.name === attr.name)});
          }
      }
      for (let j = 0; j < attributes.length - 1; j++) {
        newAttributes = newAttributes.concat(attributes[j]);
        if (attributes[j].firstGroupId === attribute.firstGroupId && attributes[j + 1].firstGroupId !== attribute.firstGroupId) {
          newAttributes = newAttributes.concat(newFirstGroupAttrs);
          isGroupAdded = true;
        }
      }
      newAttributes = newAttributes.concat(attributes[attributes.length - 1]);
      if (!isGroupAdded) {
        newAttributes = newAttributes.concat(newFirstGroupAttrs);
      }
    } 
    propAndAttr.attributes = newAttributes;
  }

  getFullDescriptionForAttribute(attribute) {
    let fullDesciption = attribute.description;
    if (attribute.firstGroupId || attribute.expandSingleMultiValue) {
      fullDesciption += "." + attribute.multiValueIndex;
      if (attribute.secondGroupId || attribute.expandSingleSubValue) {
        fullDesciption += "." + attribute.subValueIndex;
      }
    }
    return fullDesciption;
  }

  ngOnDestroy(): void {
    this.hideChatBtn();
    if (document.getElementById('botDiv').children[0].parentNode['className'] === 'chat-container is-visible') {
      this.oldAttributes = null;
      this.clickBot();
    }
    this.signalRService.resetProduct();
  }

  public clickBot() {
    const element: HTMLElement = document.getElementsByClassName('btn-chat btn-sonar')[0] as HTMLElement;
    element.click();
  }

  public showChatBtn() {
    const chatBtn = document.getElementsByClassName('btn-chat btn-sonar');
    if (chatBtn && chatBtn.length) {
      for (let i = 0; i < chatBtn.length; i++) {
        chatBtn[i]['style'].display = 'block';
      }
    }
  }

  hideChatBtn() {
    const chatBtn = document.getElementsByClassName('btn-chat btn-sonar');
    if (chatBtn && chatBtn.length) {
      for (let i = 0; i < chatBtn.length; i++) {
        chatBtn[i]['style'].display = 'none';
      }
    }
  }
}
