export class TestStep {
  id: string;
  title: string;
  coverage: number;
  dataSetIds: [];
  typicalName?: string;
  actionType: number;
  typicalType: number;
  typicalId: number;
  tsIndex: number;
}
