import { TestStep } from './test-step.model';

export class TestCase {
  id: string;
  title: string;
  testSteps: TestStep[];
}
