export class WorkflowPath {
  workflowId: string;
  pathId: string;
  pathTitle: string;
  coverage: number;
}
