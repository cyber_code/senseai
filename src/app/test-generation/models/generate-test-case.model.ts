export class GenerateTestCase {
  id: string;
  workflowId: string;
  workflowPathId: string;
  testCaseId: string;
  testCaseTitle: string;
  tCIndex: number;
  testStepId: string;
  testStepTitle: string;
  testStepJson: string;
  typicalName: string;
  typicalId: string;
  tSIndex: number;
  testStepType: number;
  constructor(
    id: string,
    workflowId: string,
    workflowPathId: string,
    testCaseId: string,
    testCaseTitle: string,
    tCIndex: number,
    tSIndex: number
  ) {
    this.id = id;
    this.workflowId = workflowId;
    this.workflowPathId = workflowPathId;
    this.testCaseId = testCaseId;
    this.testCaseTitle = testCaseTitle;
    this.tCIndex = tCIndex;
    this.tSIndex = tSIndex;
  }
}
