export class TestSuiteItem {
  id: string;
  title: string;
  items?: TestSuiteItem[];
  typicalName?: string;
  actionType?: number;
  type: TestSuiteItemType;
  coverage: number;
  dataSetIds?: [];
  typicalId: number;
  tsIndex: number;

  constructor(
    id: string,
    title: string,
    type: TestSuiteItemType,
    items?: TestSuiteItem[],
    coverage?: number,
    dataSetIds?: [],
    typicalName?: string,
    actionType?: number,
    typicalId?: number,
    tsindex?: number
  ) {
    this.id = id;
    this.title = title;
    this.type = type;
    this.items = items;
    this.typicalName = typicalName;
    this.actionType = actionType;
    this.coverage = coverage;
    this.dataSetIds = dataSetIds;
    this.typicalId = typicalId;
    this.tsIndex = tsindex;
  }
}

export enum TestSuiteItemType {
  TestCase,
  TestStep,
  Root,
  TestCaseRoot
}
