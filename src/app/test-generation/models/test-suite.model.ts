import {TestCase} from './test-case.model';


export class TestSuite {
  title: string;
  testCases: TestCase[];
}
