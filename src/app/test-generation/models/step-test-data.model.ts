export class StepTestData {
  Name: string;
  Value: string;
}

export class StepTestDatas {
  id: string;
  title: string;
  rows: [];
}

export class DataSet {
  id: string;
  title: string;
  rows: [];
  nrRows: number;
  coverage: number;
  combinations?: DataSetCombination[];
}

export class DataSetCombination {
  attribute: string;
  values: string[];
}
