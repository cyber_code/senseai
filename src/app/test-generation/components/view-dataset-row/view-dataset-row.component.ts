import {Component, OnInit, Input} from '@angular/core';

import {GridDataResult} from '@progress/kendo-angular-grid';
import {State, process} from '@progress/kendo-data-query';
import {DataAttribute} from '../../models/data-attribute';

@Component({
  selector: 'app-view-dataset-row',
  templateUrl: './view-dataset-row.component.html',
  styleUrls: ['./view-dataset-row.component.css']
})
export class ViewDatasetRowComponent implements OnInit {
  instanceAttributes: DataAttribute[] = [];
  public gridData: GridDataResult;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };

  @Input()
  public set model(row: any) {
    this.instanceAttributes = row.row;
    this.gridData = process(this.instanceAttributes, this.gridState);
  }

  constructor() {
  }

  ngOnInit() {
  }

  //#region combinations
  public onStateChange(state: State) {
    this.gridState = state;
    this.gridData = process(this.instanceAttributes, this.gridState);
  }
}
