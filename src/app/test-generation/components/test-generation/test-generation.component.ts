import {Component, OnInit, ViewChild} from '@angular/core';
import {TreeViewItem} from 'src/app/shared/tree-view-item';
import {isNullOrUndefined} from 'util';
import {NodeType} from 'src/app/shared/node-type';
import {TestGenerationFlowService} from '../../services/test-generation-flow.service';
import {TestGenerationService} from '../../services/test-generation.service';
import {MessageService} from 'src/app/core/services/message.service';
import {MessageType} from 'src/app/core/models/message.model';
import {ProjectSelectionService} from 'src/app/core/components/project-selection/project-selection.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import {AlertComponent} from 'src/app/core/components/alert/alert.component';
import { HttpClient } from '@angular/common/http';
import { SessionService } from 'src/app/core/services/session.service';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';
import { PermissionService } from 'src/app/core/services/permission.service';
import { DesignService } from 'src/app/design/services/design.service';

@Component({
  selector: 'app-test-generation',
  templateUrl: './test-generation.component.html',
  styleUrls: ['./test-generation.component.css']
})
@AutoUnsubscribe()
export class TestGenerationComponent implements OnInit {
  public license = Licenses;
  @ViewChild(AlertComponent) alert: AlertComponent;
  public opened: Boolean = false;
  public mapTestCasesWindowOpen = false;
  public atsProjects = [];
  public atsSubProjects = [];
  public selectedATSProject;
  public selectedATSSubProject;
  public selectedProject;
  public selectedSubProject;
  public testSuiteSystems;
  public testSuiteCatalogs;
  public atsSystems;
  public atsCatalogs;
  public showAtsBtn: boolean = true;
  public allCatalogs =[];

  selectedTreeViewItem: TreeViewItem;
  exportTestCaseButton: Boolean = false;
  workflowId: string;

  constructor(
    private dataFlowService: TestGenerationFlowService,
    private testGenerationService: TestGenerationService,
    private messageService: MessageService,
    private projectSelectionService: ProjectSelectionService,
    private sessionService: SessionService,
    public licenseService: LicenseService,
    public permissionService: PermissionService,
    private designService: DesignService
  ) {
    this.projectSelectionService.subProjectSelected$.pipe(takeWhileAlive(this)).subscribe(p => {
      this.dataFlowService.changeWorkflowPath(null);
      this.dataFlowService.changeWorkflowId(null);
    });
    this.dataFlowService.workflowSelected$.pipe(takeWhileAlive(this)).subscribe(_workflowId => (this.workflowId = _workflowId));
  }

  ngOnInit() {}

  public close(status) {
    this.opened = false;
  }

  public open() {
    this.opened = true;
  }

  treeViewItemSelected(treeViewItem: TreeViewItem) {
    this.exportTestCaseButton = false;
    if (!isNullOrUndefined(this.selectedTreeViewItem) && this.selectedTreeViewItem.id === treeViewItem.id) {
      return;
    }
    this.selectedTreeViewItem = treeViewItem;
    if (treeViewItem.type === NodeType.Workflow) {
      this.exportTestCaseButton = true;
      this.dataFlowService.changeWorkflowId(treeViewItem.id);
      this.mapTestCasesData();
    } else {
      this.dataFlowService.changeWorkflowId(null);
    }
  }

  async mapTestCasesData() {
    this.selectedProject = this.sessionService.getProject();
    this.selectedSubProject = this.sessionService.getSubProject();
    if(this.permissionService.hasPermission('/api/Plan/GetATSProjects') && this.permissionService.hasPermission('/api/Plan/GetATSSubProjects')){
      this.setDefaultProjectSubProject();
    }
    if(this.permissionService.hasPermission('/api/Data/GetTestSuiteSystems')){
      this.testSuiteSystems = await this.testGenerationService.getTestSuiteSystems(this.selectedTreeViewItem.id).toPromise();
      this.testSuiteCatalogs = await this.testGenerationService.getTestSuiteCatalogs(this.selectedTreeViewItem.id).toPromise();
      if(this.permissionService.hasPermission('/api/Administration/GetTenantCatalogs'))
        this.allCatalogs = await this.designService.getTenantCatalogs(this.selectedProject.id, this.selectedSubProject.id).toPromise();

      this.testSuiteCatalogs = this.testSuiteCatalogs.map(row =>{
        let currentCatalog = this.allCatalogs.find(line =>line.id === row.catalogId);
        if (currentCatalog && currentCatalog.title !== row.title)
          return {...row, title: currentCatalog.title};
        return row;
      });
    }

    //this.atsSystems = await this.testGenerationService.getATSSystems().toPromise();
    //this.atsCatalogs = await this.testGenerationService.getATSCatalogs().toPromise();
    //this.setDefaultSystem(testSuiteSystems);
    //this.setDefaultCatalog(testSuiteCatalogs);
  }

  async setDefaultProjectSubProject() {
  
    this.atsProjects = await this.testGenerationService.getATSProjects().toPromise();
    this.selectedATSProject = this.atsProjects.find(project => project.nameField === this.selectedProject.title);
  
    if(!isNullOrUndefined(this.selectedATSProject)) {
    this.atsSubProjects = await this.testGenerationService.getATSSubProjects(this.selectedATSProject.noderefField).toPromise();
    this.selectedATSSubProject = this.atsSubProjects.find(subProject => subProject.nameField === this.selectedSubProject.title);

      if (isNullOrUndefined(this.selectedATSSubProject)) {
        this.selectedATSSubProject = this.atsSubProjects[0];
      }

      if(this.permissionService.hasPermission('/api/Plan/GetATSSystems') && this.permissionService.hasPermission('/api/Plan/GetATSCatalogs')){
        this.atsSystems = await this.testGenerationService.getATSSystems(
          this.selectedATSProject.noderefField, this.selectedATSSubProject.noderefField).toPromise();
        this.atsCatalogs = await this.testGenerationService.getATSCatalogs(this.selectedATSProject.noderefField,
          this.selectedATSSubProject.noderefField).toPromise();
      }

      this.setDefaultSystem(this.testSuiteSystems);
      this.setDefaultCatalog(this.testSuiteCatalogs);

    } else if (isNullOrUndefined(this.selectedATSProject) && this.atsProjects.length > 0) {
      this.selectedATSProject = this.atsProjects[0];
      this.atsSubProjects = await this.testGenerationService.getATSSubProjects(this.selectedATSProject.noderefField).toPromise();
      this.selectedATSSubProject = this.atsSubProjects[0];

        if(this.permissionService.hasPermission('/api/Plan/GetATSSystems') && this.permissionService.hasPermission('/api/Plan/GetATSCatalogs')){
          this.atsSystems = await this.testGenerationService.getATSSystems(
            this.selectedATSProject.noderefField, this.selectedATSSubProject.noderefField).toPromise();
          this.atsCatalogs = await this.testGenerationService.getATSCatalogs(this.selectedATSProject.noderefField,
            this.selectedATSSubProject.noderefField).toPromise();
        }
        this.setDefaultSystem(this.testSuiteSystems);
        this.setDefaultCatalog(this.testSuiteCatalogs);
    }
  }

  setDefaultSystem(testSuiteSystems) {
    this.testSuiteSystems = testSuiteSystems.map(system => {
      let atsSystem = this.atsSystems.find(sys => sys.nameField === system.title);
      return {...system, atsSystem: atsSystem};
    });
  }

  setDefaultCatalog(testSuiteCatalogs) {
    this.testSuiteCatalogs = testSuiteCatalogs.map(catalog => {
      let atsCatalog = this.atsCatalogs.find(cat => cat.nameField === catalog.title);
      return {...catalog, atsCatalog: atsCatalog};
    });
  }

  async menuItemClick(itemClicked) {
    if (this.selectedTreeViewItem.type === NodeType.Workflow) {
      if (itemClicked.item.data === 'ats') {
        if (!this.testSuiteSystems || this.testSuiteSystems.length === 0) {
          this.messageService.sendMessage({text: 'This workflow does not have any systems!', type: MessageType.Warning});
          return;
        } else if (!this.testSuiteCatalogs || this.testSuiteCatalogs.length === 0) {
          this.messageService.sendMessage({text: 'This workflow does not have any catalogs!', type: MessageType.Warning});
          return;
        } else {
          this.mapTestCasesWindowOpen = true;
        }
      } else if (itemClicked.item.data === 'vxml') {
        this.testGenerationService.testGeneration(this.selectedTreeViewItem.id).subscribe(
          x => {
            const newBlob = new Blob([x], {type: 'application/vxml'});
            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
            }
            const data = window.URL.createObjectURL(newBlob);
            const link = document.createElement('a');
            link.href = data;
            link.download = 'file.xml';
            link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
            setTimeout(function () {
              window.URL.revokeObjectURL(data);
              link.remove();
            }, 100);
          },
          error => {
            this.messageService.sendMessage({text: 'Could not generate Test Cases!', type: MessageType.Warning});
          }
        );
      }
    }
  }

  closeMapTestCasesWindow() {
    this.mapTestCasesWindowOpen = false;
    if(this.permissionService.hasPermission('/api/Plan/GetATSProjects') && this.permissionService.hasPermission('api/Plan/GetATSSubProjects')){
      this.setDefaultProjectSubProject();
    }
    this.setDefaultSystem(this.testSuiteSystems);
    this.setDefaultCatalog(this.testSuiteCatalogs);
  }
}
