import { GridComponent } from '@progress/kendo-angular-grid';
import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { TestGenerationService } from '../../services/test-generation.service';
import { TestGenerationFlowService } from '../../services/test-generation-flow.service';
import { WorkflowPath } from '../../models/workflow-path.model';
import { WindowService } from '@progress/kendo-angular-dialog';
import { AutoUnsubscribe, takeWhileAlive } from 'take-while-alive';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: "app-workflow-paths",
  templateUrl: "./workflow-paths.component.html",
  styleUrls: ["./workflow-paths.component.css"]
})
@AutoUnsubscribe()
export class WorkflowPathsComponent implements OnInit {
  @ViewChild("grid") grid: GridComponent;
  public selectedWorkflowPath: WorkflowPath;
  public workflowPathClicked: WorkflowPath;
  public workflowPaths: WorkflowPath[];
  public workflowId: string;
  public loadingPending: Boolean = false;
  public selectedKeys: string[] = [];

  constructor(
    private testGenerationService: TestGenerationService,
    private dataFlowService: TestGenerationFlowService,
    private windowService: WindowService,
    public permissionService: PermissionService
  ) {
    this.dataFlowService.workflowSelected$
      .pipe(takeWhileAlive(this))
      .subscribe(_workflowId => {
        // at first we have to clear paths, because workflow is changed
        this.loadWorkflowPaths(_workflowId);
      });
    this.dataFlowService.testSuiteLoaded$
      .pipe(takeWhileAlive(this))
      .subscribe(() => {
        this.loadingPending = false;
      });
  }

  private loadWorkflowPaths(_workflowId: string) {
    this.selectedKeys = [];
    this.workflowId = _workflowId;
    this.dataFlowService.changeWorkflowPath(null);
    if (isNullOrUndefined(_workflowId)) {
      this.workflowPaths = [];
    } else {
      this.grid.loading = true;
      if(this.permissionService.hasPermission('/api/Design/GetWorkflows') && this.permissionService.hasPermission('/api/Data/GetWorkflowPaths')){
        this.testGenerationService.getWorkflowPaths(_workflowId).subscribe(_workflowPaths => {
          this.workflowPaths = _workflowPaths ? _workflowPaths : [];
          this.workflowPaths.forEach(x => (x.workflowId = _workflowId));
          this.grid.loading = false;
        });
      }
      else {
        this.workflowPaths = [];
        this.grid.loading = false;
      }
    }
  }

  ngOnInit() {}

  handleSelection() {
    if (this.selectedWorkflowPath) {
      this.selectedKeys = [this.selectedWorkflowPath.pathId];
    } else {
      this.selectedKeys = [];
    }
  }

  pathClicked(path: WorkflowPath) {
    // const path = $event.selectedRows[0].dataItem as WorkflowPath;
    if (this.loadingPending) {
      return;
    }
    this.selectedWorkflowPath = path;
    this.dataFlowService.changeWorkflowPath(path);
    this.loadingPending = true;
    this.selectedKeys = [path.pathId];
  }

  highlightedPath(path: WorkflowPath, template: any) {
    this.workflowPathClicked = path;
    this.windowService.open({
      title: `Highlighted path`,
      content: template,
      width: 1200,
      height: 850
    });
  }
}
