import {Component, OnInit, Input} from '@angular/core';
import {WindowService} from '@progress/kendo-angular-dialog';
import {TestGenerationService} from '../../services/test-generation.service';
import {DataSet} from '../../models/step-test-data.model';

@Component({
  selector: 'app-dataset-rows',
  templateUrl: './dataset-rows.component.html',
  styleUrls: ['./dataset-rows.component.css']
})
export class DatasetRowsComponent implements OnInit {
  @Input() public dataSet: DataSet;
  selectedRow: any = {};
  rows: any[] = [];

  constructor(private testGenerationService: TestGenerationService, private windowService: WindowService) {
  }

  ngOnInit() {
  }

  viewRow(row: any, rowTemplate: any) {
    this.selectedRow = row;
    this.windowService.open({
      title: 'Row details',
      content: rowTemplate,
      width: 800,
      height: 600
    });
  }
}
