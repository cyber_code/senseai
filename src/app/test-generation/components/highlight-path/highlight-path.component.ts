import { HighLighGraphHelper } from './../../../shared/highlight-graph-helper';
import { InspectorService } from './../../../design/services/rapid-services/inspector-service';
import { Component, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { StencilService } from 'src/app/design/services/rapid-services/stencil-service';
import { ToolbarService } from 'src/app/design/services/rapid-services/toolbar-service';
import { HaloService } from 'src/app/design/services/rapid-services/halo-service';
import { KeyboardService } from 'src/app/design/services/rapid-services/keyboard-service';
import { KitchenSinkService } from 'src/app/design/services/rapid-services/kitchensink-service';
import { RapidEventsService } from 'src/app/design/services/rapid-events.service';
import { DesignService } from 'src/app/design/services/design.service';
import { TestGenerationService } from '../../services/test-generation.service';
import { zip } from 'rxjs';

@Component({
  selector: 'app-highlight-path',
  templateUrl: './highlight-path.component.html',
  styleUrls: ['./highlight-path.component.css']
})
export class HighlightPathComponent implements OnInit, AfterViewInit {
  private rappid: KitchenSinkService;
  @Input() public selectedPathId: string;
  @Input() public workflowId: string;

  constructor(
    private rapidEventsService: RapidEventsService,
    private element: ElementRef,
    private designService: DesignService,
    private testGenerationService: TestGenerationService
  ) {}

  ngOnInit(): void {
    this.rappid = new KitchenSinkService(
      this.element.nativeElement as any,
      new StencilService(),
      new ToolbarService(),
      new InspectorService(),
      new HaloService(),
      new KeyboardService(),
      this.rapidEventsService,
      false
    );

    this.rappid.startRappid();
    this.loadWorkflow();
  }

  ngAfterViewInit(): void {}

  loadWorkflow() {
    const workflow$ = this.designService.getWorkflow(this.workflowId);
    const highlightedPathItems$ = this.testGenerationService.getWorkflowPathItems(this.selectedPathId);
    zip(workflow$, highlightedPathItems$).subscribe(result => {
      const json = result[0].designerJson;
      const jsonParsed = json && JSON.parse(json);
      this.rappid.loadGraphJson(jsonParsed);
      const pathIds = result[1] && result[1].map(y => y.pathItemId);
      HighLighGraphHelper.unhighlightAll(this.rappid.graph);
      this.testGenerationService.getWorkflowPathItemIds(this.selectedPathId).subscribe(res => {
        HighLighGraphHelper.highlight(this.rappid.graph, res);
      });
    });
  }
}
