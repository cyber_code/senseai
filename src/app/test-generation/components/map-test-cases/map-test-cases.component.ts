import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MessageType } from "src/app/core/models/message.model";
import { MessageService } from "src/app/core/services/message.service";
import { PermissionService } from 'src/app/core/services/permission.service';
import { Guid } from "src/app/shared/guid";
import { TestGenerationFlowService } from "../../services/test-generation-flow.service";
import { TestGenerationService } from "../../services/test-generation.service";

@Component({
  selector: "map-test-cases",
  templateUrl: "./map-test-cases.component.html",
  styleUrls: ["./map-test-cases.component.css"]
})
export class MapTestCasesComponent implements OnInit {
  @Input() atsProjects;
  @Input() atsSubProjects;
  @Input() selectedATSProject;
  @Input() selectedATSSubProject;
  @Input() selectedProject;
  @Input() selectedSubProject;
  @Input() testSuiteSystems;
  @Input() testSuiteCatalogs;
  @Input() atsSystems;
  @Input() atsCatalogs;
  @Input() mapTestCasesWindowOpen;
  @Input() selectedTreeViewItem;
  public workflowPathId = Guid.empty;
  public workflowId = Guid.empty;

  @Output() public closeMapTestCasesWindow = new EventEmitter<any>();

  public maintainManualSteps = false;
  public loading: boolean = false;

  constructor(
    private testGenerationService: TestGenerationService,
    private messageService: MessageService,
    private testGenerationFlowService: TestGenerationFlowService,
    private permissionService: PermissionService
  ) {
    this.testGenerationFlowService.workflowPathSelected.subscribe(
      selectedPath => {
        this.workflowPathId = selectedPath ? selectedPath.pathId : Guid.empty;
        this.workflowId = selectedPath ? selectedPath.workflowId : Guid.empty;
      }
    );
  }

  ngOnInit() {}

  async changeSelectedProject(project) {
    this.selectedATSProject = project;
    this.selectedATSSubProject = null;
    if (project) {
      this.atsSubProjects = await this.testGenerationService
        .getATSSubProjects(project.noderefField)
        .toPromise();
    }
  }

  changeSelectedSubProject(subProject) {
    this.selectedATSSubProject = subProject;
    this.getAtsProjectAndSubproject();
  }

  async getAtsProjectAndSubproject() {
    this.atsSystems = await this.testGenerationService
      .getATSSystems(
        this.selectedATSProject.noderefField,
        this.selectedATSSubProject.noderefField
      )
      .toPromise();
    this.atsCatalogs = await this.testGenerationService
      .getATSCatalogs(
        this.selectedATSProject.noderefField,
        this.selectedATSSubProject.noderefField
      )
      .toPromise();
  }

  changeSelectedAtsSystem(atsSystem, testSuiteSystem) {
    testSuiteSystem.atsSystem = atsSystem;
  }

  changeSelectedAtsCatalog(atsCatalog, testSuiteCatalog) {
    testSuiteCatalog.atsCatalog = atsCatalog;
  }

  disableSubmitBtn() {
    return (
      !this.selectedATSProject ||
      !this.selectedATSSubProject ||
      !this.testSuiteSystems ||
      !this.testSuiteCatalogs ||
      this.testSuiteSystems.find(sys => !sys.atsSystem) ||
      this.testSuiteCatalogs.find(cat => !cat.atsCatalog)
    );
  }

  resetValues() {
    this.selectedATSProject = null;
    this.selectedATSSubProject = null;
    this.atsSystems = [];
    this.atsCatalogs = [];
  }

  close() {
    this.resetValues();
    this.closeMapTestCasesWindow.emit();
    this.maintainManualSteps = false;
  }

  submit() {
    this.loading = true;
    let projectId = this.selectedATSProject.noderefField;
    let subProjectId = this.selectedATSSubProject.noderefField;
    let catalogs = this.testSuiteCatalogs.map(cat => {
      return {
        From: cat.title,
        To: cat.atsCatalog.nameField
      };
    });
    let systems = this.testSuiteSystems.map(sys => {
      return {
        From: sys.title,
        To: sys.atsSystem.nameField
      };
    });
    let maintainManualSteps = this.maintainManualSteps;

    this.testGenerationService
      .checkATSTestCases(this.selectedTreeViewItem.id, this.workflowPathId, projectId, subProjectId)
      .subscribe(
        (res) => {
          if (res && res === true) {
            this.close();
            this.testGenerationService
              .savePlanToQualitySuite(
                this.selectedTreeViewItem.id,
                this.workflowPathId,
                projectId,
                subProjectId,
                maintainManualSteps,
                catalogs,
                systems
              )
              .subscribe(result => {
                if (result && result.successfullySaved > 0) {
                  this.messageService.sendMessage({
                    text: 'Test Cases Exported!',
                    type: MessageType.Success
                  });
                  this.closeLoadingModal();
                }
              });
          } else {
            this.closeLoadingModal();
          }
        },
        (err) => {
          this.closeLoadingModal();
        }
      );
  }

  closeLoadingModal() {
    this.loading = false;
  }
}
