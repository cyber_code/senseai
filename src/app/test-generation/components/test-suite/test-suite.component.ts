import { GridComponent } from '@progress/kendo-angular-grid';
import { TestSuiteItem, TestSuiteItemType } from './../../models/test-suite-item';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  QueryList,
  ViewChildren,
  AfterViewInit,
  AfterViewChecked,
  Renderer2,
  NgZone,
  OnDestroy
} from '@angular/core';
import { TestGenerationFlowService } from '../../services/test-generation-flow.service';
import { TestGenerationService } from '../../services/test-generation.service';
import { isNullOrUndefined } from 'util';
import { of } from 'rxjs/observable/of';
import { Observable, Subscription, fromEvent } from 'rxjs';
import { DataSet } from '../../models/step-test-data.model';
import { State } from '@progress/kendo-data-query';
import { WindowService, DialogService } from '@progress/kendo-angular-dialog';
import * as Wfact from '../../../core/models/workflow-item-action-type';
import * as TSfact from './../../models/test-suite-item';
import { TreeViewComponent } from '@progress/kendo-angular-treeview';
import { TestStep } from '../../models/test-step.model';
import { TestSuite } from '../../models/test-suite.model';
import { map, tap } from 'rxjs/operators';
import { takeWhileAlive, AutoUnsubscribe } from 'take-while-alive';
import * as _ from 'lodash';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';
import { PermissionService } from 'src/app/core/services/permission.service';
import { MessageService } from 'src/app/core/services/message.service';

const tableRow = node => node.tagName.toLowerCase() === 'tr';
const spanRow = node => node.tagName.toLowerCase() === 'span';
const tbody = node => node.tagName.toLowerCase() === 'tbody';
const closest = (node, predicate) => {
  while (node && !predicate(node)) {
    if (node.tagName.toLowerCase() === 'html') {
      return node;
    }
    node = node.parentNode;
  }

  return node;
};

@Component({
  selector: 'app-test-suite',
  templateUrl: './test-suite.component.html',
  styleUrls: ['./test-suite.component.css']
})
@AutoUnsubscribe()
export class TestSuiteComponent implements OnInit, AfterViewChecked, AfterViewInit, OnDestroy {
  @ViewChild('treeView') treeView: TreeViewComponent;
  @ViewChild('dataSetsGrid') dataSetsGrid: GridComponent;
  @ViewChild('grid') testSuiteGrid: GridComponent;
  @ViewChildren('inputDataItem') divs: QueryList<ElementRef>;
  @ViewChildren('testStepGrid') grids: QueryList<ElementRef>;
  public WorkflowItemActionType = Wfact.WorkflowItemActionType;
  public TestSuiteItemType = TSfact.TestSuiteItemType;
  public testSuiteData: any;
  public lastSavedSuiteData: any;
  public keys: string[] = [];
  public testStepSelected: TestStep;
  public DataSetItems: any[] = [];
  public dataSetItems: any[] = [];
  public selectedDataSetsIds: string[] = [];
  public formatedDataSetItems: any[] = [];
  public selectedDataSet: any[] = [];
  public expandedKeys: any[] = ['0'];
  public lastGrid: any;
  public draggedItemIndex: any;
  public lastparentIndex: any;
  public lastchildLength = 0;
  public dataItemInProcess: TestSuiteItem = {} as TestSuiteItem;
  public selectedData: any[] = [];
  public state: State = {
    filter: { logic: 'and', filters: [{ field: 'Name', operator: 'contains', value: '' }] }
  };
  private currentSubscription: Subscription;
  private selectedWorkflowPath: any;
  public license = Licenses;


  constructor(
      private dataFlowService: TestGenerationFlowService,
      private testGenerationService: TestGenerationService,
      private windowService: WindowService,
      private dialogService: DialogService,
      private renderer: Renderer2,
      private zone: NgZone,
      private messageService: MessageService,
      public licenseService: LicenseService,
      public permissionService: PermissionService) {



    // when path is changed we have to update test suite
    this.dataFlowService.workflowPathSelected$.pipe(takeWhileAlive(this)).subscribe(async workflowPath => {
      if (isNullOrUndefined(workflowPath)) {
        this.testSuiteData = [];
        this.lastSavedSuiteData = [];
        return;
      }
      this.testSuiteGrid.loading = true;
      this.selectedWorkflowPath = workflowPath;
      if(this.permissionService.hasPermission('/api/Data/GetTestSuite')){
        this.testGenerationService.getTestSuite(workflowPath.workflowId, workflowPath.pathId);
        this.testGenerationService
          .getTestSuite(workflowPath.workflowId, workflowPath.pathId)
          .pipe(map(ts => this.toTestSuiteItems(ts)))
          .subscribe(items => {
            this.testSuiteData = items;
            console.log('Test suite data', items);
            this.lastSavedSuiteData = _.cloneDeep(items);
            this.testSuiteGrid.loading = false;
            this.dataFlowService.fireTestSuiteLoaded(workflowPath.pathId);
            this.expandedKeys = ['0'];
          });
      }
      else {
        this.testSuiteGrid.loading = false;
        this.testSuiteData = [];
      }
    });
  }

  private toTestSuiteItems(testSuite: TestSuite) {
    if (isNullOrUndefined(testSuite)) {
      return [];
    }
    const testSuiteRoot = new TestSuiteItem('-1', testSuite.title, TestSuiteItemType.Root, []);
    for (const testCase of testSuite.testCases) {
      const tcRootItem = new TestSuiteItem(testCase.id, testCase.title, TestSuiteItemType.TestCaseRoot, []);
      const tcItem = new TestSuiteItem(testCase.id, testCase.title, TestSuiteItemType.TestCase, []);
      for (const tsItem of testCase.testSteps) {
        tcItem.items.push(
          new TestSuiteItem(
            tsItem.id,
            tsItem.title,
            TestSuiteItemType.TestStep,
            null,
            tsItem.coverage,
            tsItem.dataSetIds,
            tsItem.typicalName,
            tsItem.actionType,
            tsItem.typicalId,
            tsItem.tsIndex
          )
        );
      }
      tcRootItem.items.push(tcItem);
      testSuiteRoot.items.push(tcRootItem);
    }
    return [testSuiteRoot];
  }

  ngOnInit() {}
  public ngAfterViewInit(): void {}
  public ngOnDestroy(): void {
    if (this.currentSubscription !== undefined) {
      this.currentSubscription.unsubscribe();
    }
  }
  public contains(text: string, term: string): boolean {
    return text.toLowerCase().indexOf(term.toLowerCase()) >= 0;
  }

  public children = (dataitem: TestSuiteItem): Observable<TestSuiteItem[]> => of(dataitem.items);

  public hasChildren = (dataitem: TestSuiteItem): boolean =>
    !!(dataitem.items && dataitem.items.length > 0 && dataitem.type !== TestSuiteItemType.TestCase);

  public viewRows(item: DataSet, rowsTemplate: any) {
    this.selectedDataSet = item.rows || [];
    this.windowService.open({
      title: `Dataset rows`,
      content: rowsTemplate,
      width: 400,
      height: 600
    });
  }

  public viewCoverage(dataItem: any, dataSetsTemplate: any) {
    this.dialogService.open({
      title: 'Associated datasets',
      content: dataSetsTemplate,
      width: 600,
      height: 400
    });
    if (dataItem.dataSetIds && dataItem.dataSetIds.length > 0) {
      this.testGenerationService.GetDataSetItems(dataItem.dataSetIds).subscribe(response => {
        this.dataSetItems = response || [];
      });
    }
  }

  public viewAssociations(testStep: TestStep, associationsTemplate: any) {
    this.testStepSelected = testStep;
    this.windowService.open({
      title: `Associations graph`,
      content: associationsTemplate,
      width: 1200,
      height: 850
    });
  }

  ngAfterViewChecked() {}
}
