import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';
import { SessionService } from 'src/app/core/services/session.service';
import { WorkflowPathItem } from 'src/app/data-management/models/workflowPathItem.model';
import { CommandMethod } from 'src/app/shared/command-method';
import { CheckATSTestCases, ExportWorkflowToQualitySuite, GetATSCatalogs, GetATSProjects, GetATSSubProjects, GetATSSystems, GetDataSetItem, GetDataSetItems, GetModelVisualization, GetTestSuite, GetTestSuiteCatalogs, GetTestSuiteSystems, GetWorkflowPathItemIds, GetWorkflowPathItems, GetWorkflowPaths } from '../commands-queries/test-generation-command-queries';
import { TestSuite } from '../models/test-suite.model';
import { WorkflowPath } from '../models/workflow-path.model';
@Injectable()
export class TestGenerationService {
  private planUrl: string;
  private dataUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private _http: HttpClient,
    private sessionService: SessionService
  ) {
    this.planUrl = `${configurationService.serverSettings.apiUrl}Plan/`;
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Data/`;
  }

  testGeneration(workflowId: string): Observable<Blob> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    return this._http.get(this.planUrl + 'ExportTestCases?workflowId=' + workflowId, { responseType: 'blob', headers: headers });
  }

  checkATSTestCases(workflowId, workflowPathId, projectId, subprojectId) {
    // tslint:disable-next-line:max-line-length
    return this.httpExecutor.executeQuery<any>(this.planUrl, new CheckATSTestCases(workflowId, workflowPathId, projectId, subprojectId));
  }

  savePlanToQualitySuite(
    workflowId: string,
    workflowPathId: string,
    projectId: string,
    subProjectId: string,
    maintainManualSteps: boolean,
    catalogs: any,
    systems: any
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.planUrl,
      new ExportWorkflowToQualitySuite(workflowId, workflowPathId, projectId, subProjectId, maintainManualSteps, catalogs, systems),
      CommandMethod.POST
    );
  }
  getATSProjects(): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.planUrl, new GetATSProjects());
  }

  getATSSubProjects(NoderefField: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.planUrl, new GetATSSubProjects(NoderefField));
  }

  getWorkflowPaths(WorkflowId: string): Observable<WorkflowPath[]> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetWorkflowPaths(WorkflowId));
  }

  getTestSuiteSystems(WorkflowId: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetTestSuiteSystems(WorkflowId));
  }

  getTestSuiteCatalogs(WorkflowId: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetTestSuiteCatalogs(WorkflowId));
  }

  getATSSystems(projectId: string, subProjectId: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.planUrl, new GetATSSystems(projectId, subProjectId));
  }

  getATSCatalogs(projectId: string, subProjectId: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.planUrl, new GetATSCatalogs(projectId, subProjectId));
  }

  getTestSuite(WorkflowId: string, workflowPathId: string): Observable<TestSuite> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetTestSuite(WorkflowId, workflowPathId));
  }
  getWorkflowPathItems(workflowPathId: string): Observable<WorkflowPathItem[]> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetWorkflowPathItems(workflowPathId));
  }
  getWorkflowPathItemIds(workflowPathId: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetWorkflowPathItemIds(workflowPathId));
  }
  GetDataSetItems(query: any): Observable<any> {
    return this.httpExecutor.executeCommand<any[]>(this.dataUrl, new GetDataSetItems(query), CommandMethod.POST).pipe(
      map(response => {
        return response.map(row => {
          let rowNumber = 1;
          const dataSetWithRows = {};
          dataSetWithRows['id'] = row['id'];
          dataSetWithRows['title'] = row['title'];
          dataSetWithRows['rows'] = (row['rows'] as Array<any>).map(dataSetRow => {
            return {
              rowId: rowNumber++,
              row: JSON.parse(dataSetRow)
            };
          });
          return dataSetWithRows;
        });
      })
    );
  }

  getDataSetRows(datasetId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.dataUrl, new GetDataSetItem(datasetId)).pipe(
      map(x => {
        const dataSetWithRows = {};
        dataSetWithRows['id'] = x['dataSetId'];
        dataSetWithRows['rows'] = (x['rows'] as Array<any>).map(dataSetRow => {
          return {
            rowId: dataSetRow.id,
            row: JSON.parse(dataSetRow.row)
          };
        });

        return dataSetWithRows;
      })
    );
  }
  GetModelVisualization(typicalName: string, typiclId: number): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetModelVisualization(typicalName, typiclId, 1));
  }
}
