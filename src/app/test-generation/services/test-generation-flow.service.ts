import { WorkflowPath } from "./../models/workflow-path.model";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable()
export class TestGenerationFlowService {
  constructor() {}

  // workflow ID
  private workflowSelected = new Subject<string>();
  workflowSelected$ = this.workflowSelected.asObservable();

  // WorkflowPathId
  public workflowPathSelected = new Subject<WorkflowPath>();
  workflowPathSelected$ = this.workflowPathSelected.asObservable();

  private testSuiteLoaded = new Subject<string>();
  testSuiteLoaded$ = this.testSuiteLoaded.asObservable();

  fireTestSuiteLoaded(pathId: string) {
    this.testSuiteLoaded.next(pathId);
  }
  changeWorkflowId(workflowId: string) {
    this.workflowSelected.next(workflowId);
  }

  changeWorkflowPath(workflowPath: WorkflowPath) {
    this.workflowPathSelected.next(workflowPath);
  }
}
