import {AuthGuard} from '../shared/auth.guards';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TestGenerationComponent} from './components/test-generation/test-generation.component';
import {ProjectSelectionGuard} from '../shared/project-selection.guards';

const routes: Routes = [
  {path: 'test-generation', component: TestGenerationComponent, canActivate: [AuthGuard, ProjectSelectionGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class TestGenerationRoutingModule {
}
