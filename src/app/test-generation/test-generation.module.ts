import { CoreModule } from '../core/core.module';
import { TestGenerationRoutingModule } from './test-generation-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WindowModule, DialogModule } from '@progress/kendo-angular-dialog';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { PopupModule } from '@progress/kendo-angular-popup';
import { MenuModule } from '@progress/kendo-angular-menu';
import { TestGenerationService } from './services/test-generation.service';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { GridModule } from '@progress/kendo-angular-grid';
import { TestGenerationFlowService } from './services/test-generation-flow.service';
import { TestGenerationComponent } from './components/test-generation/test-generation.component';
import { WorkflowPathsComponent } from './components/workflow-paths/workflow-paths.component';
import { TestSuiteComponent } from './components/test-suite/test-suite.component';
import { AssociationsComponent } from './components/associations/associations.component';
import { DatasetRowsComponent } from './components/dataset-rows/dataset-rows.component';
import { ViewDatasetRowComponent } from './components/view-dataset-row/view-dataset-row.component';
import { MapTestCasesComponent } from './components/map-test-cases/map-test-cases.component';
import { HighlightPathComponent } from './components/highlight-path/highlight-path.component';

@NgModule({
  imports: [
    CommonModule,
    TestGenerationRoutingModule,
    CoreModule,
    LayoutModule,
    BrowserModule,
    BrowserAnimationsModule,
    TreeViewModule,
    HttpClientModule,
    FormsModule,
    WindowModule,
    ButtonsModule,
    DialogModule,
    ReactiveFormsModule,
    PopupModule,
    MenuModule,
    DropDownsModule,
    GridModule
  ],
  providers: [TestGenerationService, TestGenerationFlowService, TestSuiteComponent, DatasetRowsComponent],
  declarations: [
    TestGenerationComponent,
    WorkflowPathsComponent,
    TestSuiteComponent,
    DatasetRowsComponent,
    AssociationsComponent,
    HighlightPathComponent,
    ViewDatasetRowComponent,
    MapTestCasesComponent
  ],
  exports: [TestGenerationComponent, TestSuiteComponent, DatasetRowsComponent, ViewDatasetRowComponent, HighlightPathComponent],
  entryComponents: [TestSuiteComponent]
})
export class TestGenerationModule {}
