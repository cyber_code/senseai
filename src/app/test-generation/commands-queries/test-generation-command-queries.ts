import { ApiAction } from "src/app/core/services/command-queries/api-action";

export class TestGeneration extends ApiAction {
  constructor(workflowId: string) {
    super("TestGeneration");
    this.workflowId = workflowId;
  }

  workflowId: string;
}

export class GetATSProjects extends ApiAction {
  constructor() {
    super("GetATSProjects");
  }
}

export class GetATSSubProjects extends ApiAction {
  NoderefField: string;

  constructor(NoderefField) {
    super("GetATSSubProjects");
    this.NoderefField = NoderefField;
  }
}

export class GetTestSuiteCatalogs extends ApiAction {
  workflowId: string;

  constructor(workflowId: string) {
    super("GetTestSuiteCatalogs");
    this.workflowId = workflowId;
  }
}

export class GetTestSuiteSystems extends ApiAction {
  workflowId: string;

  constructor(workflowId: string) {
    super("GetTestSuiteSystems");
    this.workflowId = workflowId;
  }
}

export class GetATSSystems extends ApiAction {
  constructor(projectId: string, subProjectId: string) {
    super("GetATSSystems");
    this.projectId = projectId;
    this.subProjectId = subProjectId;
  }
  projectId: string;
  subProjectId: string;
}

export class GetATSCatalogs extends ApiAction {
  constructor(projectId: string, subProjectId: string) {
    super("GetATSCatalogs");
    this.projectId = projectId;
    this.subProjectId = subProjectId;
  }
  projectId: string;
  subProjectId: string;
}

export class CheckATSTestCases extends ApiAction {
  workflowId: string;
  workflowPathId: string;
  projectId: string;
  subprojectId: string;

  constructor(workflowId, workflowPathId, projectId, subprojectId) {
    super('CheckATSTestCases');
    this.workflowId = workflowId;
    this.workflowPathId = workflowPathId;
    this.projectId = projectId;
    this.subprojectId = subprojectId;
  }
}

export class ExportWorkflowToQualitySuite extends ApiAction {
  constructor(
    workflowId: string,
    workflowPathId: string,
    projectId: string,
    subProjectId: string,
    maintainManualSteps: boolean,
    catalogs: any,
    systems: any
  ) {
    super("ExportWorkflowToQualitySuite");
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.workflowId = workflowId;
    this.workflowPathId = workflowPathId;
    this.maintainManualSteps = maintainManualSteps;
    this.systems = systems;
    this.catalogs = catalogs;
  }

  projectId: string;
  subProjectId: string;
  workflowId: string;
  workflowPathId: string;
  maintainManualSteps: boolean;
  catalogs: any;
  systems: any;
}

export class GetWorkflowPaths extends ApiAction {
  WorkflowId: string;

  constructor(WorkflowId: string) {
    super("GetWorkflowPaths");
    this.WorkflowId = WorkflowId;
  }
}

export class GetTestSuite extends ApiAction {
  WorkflowId: string;
  WorkflowPathId: string;

  constructor(WorkflowId: string, WorkflowPathId: string) {
    super("GetTestSuite");
    this.WorkflowId = WorkflowId;
    this.WorkflowPathId = WorkflowPathId;
  }
}

export class GetWorkflowPathItems extends ApiAction {
  workflowPathId;

  constructor(workflowPathId) {
    super("GetWorkflowPathItems");
    this.workflowPathId = workflowPathId;
  }
}

export class GetWorkflowPathItemIds extends ApiAction {
  workflowPathId;

  constructor(workflowPathId) {
    super("GetWorkflowPathItemIds");
    this.workflowPathId = workflowPathId;
  }
}

export class GetDataSetItems extends ApiAction {
  dataSetIds: any;

  constructor(dataSetIds: any) {
    super("GetDataSetItems");
    this.dataSetIds = dataSetIds;
  }
}

export class GetDataSetItem extends ApiAction {
  DataSetId: string;

  constructor(dataSetId: string) {
    super("GetDataSetItem");
    this.DataSetId = dataSetId;
  }
}

export class GetModelVisualization extends ApiAction {
  typicalName: string;
  typicalId: number;
  associationLvl: number;

  constructor(typicalName: string, typicalId: number, associationLvl: number) {
    super("GetModelVisualization");
    this.typicalName = typicalName;
    this.associationLvl = associationLvl;
    this.typicalId = typicalId;
  }
}
