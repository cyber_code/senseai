import {NodeType} from './node-type';

export class TreeViewItem {
  id: string;
  title: string;
  type: NodeType;
  items?: TreeViewItem[];
  cutCopy?: boolean;
  status: number;
  isIssued: boolean;
  generatedTestCases: boolean;

  constructor(id: string, title: string, type: NodeType, items?: TreeViewItem[], cutCopy?: boolean, status?: number, isIssued?: boolean, generatedTestCases?: boolean) {
    this.id = id;
    this.title = title;
    this.type = type;
    this.items = items;
    this.cutCopy = cutCopy;
    this.status = status;
    this.isIssued = isIssued;
    this.generatedTestCases = generatedTestCases;
  }
}
