import {Injectable} from '@angular/core';
import * as _ from 'underscore';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() {
  }

  public filter(array, delegate) {
    return _.filter(array, function (s) {
      return s.hasOwnProperty(delegate) ;
    });
  }

}
