import {isNullOrUndefined} from 'util';

export class HighLighGraphHelper {
  public static highlight(graph: any, workflowItemIds: string[] = []) {
    const links = graph.getLinks();
    const cells = graph.getCells();
    let highlightedCells = [];
    for (let i = 0; i < workflowItemIds.length; i++) {
      const cellsToHighlight = cells.filter(x => ((x.attributes.customData && x.attributes.customData.refId === workflowItemIds[i]) || x.attributes.id === workflowItemIds[i]));

      if (cellsToHighlight && cellsToHighlight.length !== 0) {
        highlightedCells.push(...cellsToHighlight);
        for (let i = 0; i < cellsToHighlight.length; i++) {
          this.changeShapeOutline(cellsToHighlight[i], '#26ff51');
        }
      }
    }
    for (let i = 0; i < links.length; i ++) {
      if (
        highlightedCells.find(cell => cell.id === links[i].attributes.source.id) &&
        highlightedCells.find(cell => cell.id === links[i].attributes.target.id)
      ) {
        this.changeLineColor(links[i], '#26ff51');
      }
    }
  }

  public static unhighlightAll(graph: any) {
    const links = graph.getLinks();
    const cells = graph.getCells();
    cells.forEach(cv => this.changeShapeOutline(cv, '#c8d6e5'));
    links.forEach(link => this.changeLineColor(link, '#c8d6e5'));
  }

  private static changeLineColor(link: any, color: string) {
    link.attr('line/stroke', color);
  }

  private static changeShapeOutline(cell: any, color: string) {
    if (cell.attributes.type === 'fsa.State') {
      cell.attr('circle/stroke', color);
      cell.attr('body/stroke', color);
    } else if (cell.attributes.type === 'standard.Polygon' || cell.attributes.type === 'standard.Rectangle') {
      cell.attr('body/stroke', color);
    }
  }
}
