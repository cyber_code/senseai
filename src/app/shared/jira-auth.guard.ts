import { LicenseService } from "src/app/core/services/licenses.service";
import { Licenses } from "src/app/core/models/licenses.model";
import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot
} from "@angular/router";
import { SessionService } from "../core/services/session.service";
import { JiraIntegrationService } from "../process-managment/services/jira-integration.service";
import { Observable } from "rxjs";
import { PermissionService } from "../core/services/permission.service";

@Injectable({ providedIn: "root" })
export class JiraAuthGuard implements CanActivate {
  public licenses = Licenses;
  constructor(
    private router: Router,
    private jiraService: JiraIntegrationService,
    private session: SessionService,
    private licenseService: LicenseService,
    private permissionService: PermissionService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return new Observable<boolean>(observer => {
      if (!this.licenseService.hasLicense(this.licenses.Jira) || !this.permissionService.hasPermission('/api/Process/Jira/GetJiraIssueTypes')) {
        observer.next(true);
        observer.complete();
        return true as any;
      } else {
        const subprojectId = this.session.getSubProject().id;
        if (subprojectId) {
          this.jiraService
            .HasValidJiraCredentials(subprojectId, false)
            .subscribe(
              (res: any) => {
                if (res === true) {
                  observer.next(true);
                  observer.complete();
                  return true;
                } else {
                  observer.next(false);
                  observer.complete();
                  this.router.navigate(["/jira-auth"], {
                    queryParams: { returnUrl: state.url }
                  });
                  return false;
                }
              },
              err => {
                observer.next(false);
                observer.complete();
                this.router.navigate(["/jira-auth"], {
                  queryParams: { returnUrl: state.url }
                });
              }
            );
        } else {
          observer.next(false);
          observer.complete();
          this.router.navigate(["/jira-auth"], {
            queryParams: { returnUrl: state.url }
          });
        }
      }
    });
  }
}
