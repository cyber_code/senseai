import {ArisNodeType} from './aris-node-type';

export class ArisTreeViewItem {
  id: string;
  title: string;
  type: ArisNodeType;
  processId: string;
  items?: ArisTreeViewItem[];

  constructor(id: string, title: string, type: ArisNodeType, items?: ArisTreeViewItem[], processId?: string) {
    this.id = id;
    this.title = title;
    this.type = type;
    this.items = items;
    this.processId = processId;
  }
}
