import {Helper} from './helper';

describe('Helper', () => {
  it('should generate correct HashIds', () => {
    const tenantId = "30788c4a-ea74-4aa8-b505-891303dbaeaa";
    const projectId = "0b450e5c-9b76-4851-b341-a42fdc129d68";
    const subProjectId = "3adf6db6-b640-439d-b6d3-1aaa9d9fd626";
    const systemId = "b5bcc43f-afc8-4f67-8eb1-b633994ce363";
    const catalogId = "e516db6f-a95b-4cbb-b22b-c2fc2f8dd044"; 

    const hashId = Helper.generateHashId(tenantId, projectId, subProjectId, systemId, catalogId);
    expect(hashId).toBe("4F851B6820228FC49AEE5CAF1B1E7FF6");
  });
});
