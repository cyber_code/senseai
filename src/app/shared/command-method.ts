export enum CommandMethod {
  GET,
  POST,
  PUT,
  DELETE
}
