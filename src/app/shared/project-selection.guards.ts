import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {ProjectSelectionService} from '../core/components/project-selection/project-selection.service';
import {Observable} from 'rxjs';

@Injectable({providedIn: 'root'})
export class ProjectSelectionGuard implements CanActivate {
  constructor(private router: Router, private projectSelectionService: ProjectSelectionService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.projectSelectionService.subProjectSelected$.map(res => {
      if (res.hasValue) {
        return true;
      }
      this.router.navigate(['/project-selection'], {queryParams: {returnUrl: state.url}});
      return false;
    });
  }
}
