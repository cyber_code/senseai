import {isNullOrUndefined} from 'util';

export class Guid {
  static empty = '00000000-0000-0000-0000-000000000000';

  static newGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      // tslint:disable-next-line:no-bitwise
      const r = (Math.random() * 16) | 0,
        v = c === 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  static isEmpty(guid: string) {
    return isNullOrUndefined(guid) || guid === Guid.empty;
  }
}
