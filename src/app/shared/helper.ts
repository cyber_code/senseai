import * as MD5 from './md5';

export class Helper {
  public static getValueFromQueryString(url: string, key: string) {
    key = key.replace(/[\[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + key + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) {
      return null;
    }
    if (!results[2]) {
      return '';
    }
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  public static generateHashId(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string) {
    // ledio: the GUID dashes '-' are stripped AFTER the MD5 hashing, having no effect
    // replicated here to calculate consistent hashIds as in RPA

    let hashId = MD5(tenantId     + '|' +
                     projectId    + '|' +
                     subProjectId + '|' +
                     systemId     + '|' +
                     catalogId).replace(/-/g, '');
    return hashId;
  }

  public static dataSourceFilter(dsFilter: string, searchText: string): boolean {
    return dsFilter.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
  }
}
