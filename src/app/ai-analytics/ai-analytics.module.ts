import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChartsModule} from 'ng2-charts';
import {NgxBootstrapSliderModule} from 'ngx-bootstrap-slider';

import {AiAnalyticsRoutingModule} from './ai-analytics-routing.module';
import {DataTablesModule} from 'angular-datatables';
import {AiAnaliticsMenuComponent} from './components/reports-menu/reports-menu.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReportsFilterComponent} from './components/reports-filter/reports-filter.component';
import {Ng5SliderModule} from 'ng5-slider';
import 'chartjs-plugin-zoom';
import {PapaParseModule} from 'ngx-papaparse';
import {CoreModule} from '../core/core.module';
import {DataExtractionServiceService} from './services/data-extraction-service.service';
import { DefectReplayComponent } from './components/predictive-analytics-defect/defect-replay/defect-replay.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { TabsModule } from 'ngx-bootstrap';
import { SelectDropDownModule } from 'ngx-select-dropdown'
import { NgProgressModule } from 'ngx-progressbar';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { MatTableModule } from '@angular/material' 


import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { RequirementsPrioritizationComponent } from './components/requirement-reports/requirements-prioritization/requirements-prioritization.component';
import { ProcessManagmentReportsComponent } from './components/requirement-reports/process-managment-reports/process-managment-reports.component';
import { TraceabilityReportComponent } from './components/requirement-reports/traceability-report/traceability-report.component';


import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { WindowModule, DialogModule, DialogsModule } from '@progress/kendo-angular-dialog';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { PopupModule } from '@progress/kendo-angular-popup';
import { MenuModule } from '@progress/kendo-angular-menu';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { RootCausePerBusinessAreaTreemapComponent } from './components/predictive-analytics-defect/reports-statistics/sub-components/root-cause-per-business-area-treemap/root-cause-per-business-area-treemap.component';
import { TimeToFixPerBusAreaRootCauseComponent } from './components/predictive-analytics-defect/reports-statistics/sub-components/time-to-fix-per-bus-area-root-cause/time-to-fix-per-bus-area-root-cause.component';
import { SeverityPerBusinessAreaTreemapComponent } from './components/predictive-analytics-defect/reports-statistics/sub-components/severity-per-business-area-treemap/severity-per-business-area-treemap.component';
import { DiscoveryRatioGroupedBarChartComponent } from './components/predictive-analytics-defect/reports-statistics/sub-components/discovery-ratio-grouped-bar-chart/discovery-ratio-grouped-bar-chart.component';
import { ReportsStatisticsComponent } from './components/predictive-analytics-defect/reports-statistics/reports-statistics.component';
import { DefectsPerBussAreaComponent } from './components/predictive-analytics-defect/reports-statistics/sub-components/defects-per-buss-area/defects-per-buss-area.component';
import { PriorityStackedBarComponent } from './components/predictive-analytics-defect/reports-statistics/sub-components/priority-stacked-bar/priority-stacked-bar.component';
import { StatusPieChartComponent } from './components/predictive-analytics-defect/reports-statistics/sub-components/status-pie-chart/status-pie-chart.component';
import { PredictiveAnalyticsDefectComponent } from './components/predictive-analytics-defect/main/predictive-analytics-defect.component';
import { SummaryReportsComponent } from './components/predictive-analytics-project/summary-reports/main/summary-reports.component';
import { ImpactAnalysisComponent } from './components/predictive-analytics-project/impact-analysis/impact-analysis.component';
import { FiltersComponent } from './components/predictive-analytics-project/summary-reports/filters/filters.component';
import { CurrentSprintsComponent } from './components/predictive-analytics-project/summary-reports/current-sprints/current-sprints.component';
import { SearchSprintComponent } from './components/predictive-analytics-project/summary-reports/search-sprint/search-sprint.component';
import { PieChartComponent } from './components/predictive-analytics-project/summary-reports/pie-chart/pie-chart.component';
import { PassedSprintsComponent } from './components/predictive-analytics-project/summary-reports/passed-sprints/passed-sprints.component';
import { CompareSprintsComponent } from './components/predictive-analytics-project/summary-reports/compare-sprints/compare-sprints.component';
import { FutureProjectionComponent } from './components/predictive-analytics-project/summary-reports/future-projection/future-projection.component';
import { TestExecutionComponent } from './components/predictive-analytics-project/summary-reports/test-execution/test-execution.component';
import { PredictiveAnalyticsProjectComponent } from './components/predictive-analytics-project/main/predictive-analytics-project.component';
import { DashboardPopupComponent } from './components/dashboard-main/components/dashboard-popup/dashboard-popup.component';
import { DashboardMainComponent } from './components/dashboard-main/dashboard.component';
import { DataAssociationsComponent } from './components/data-mining/model-associations/associations/associations.component';
import { WorkflowsComponent } from './components/requirement-reports/workflows/workflows.component';
import { WorkflowBestPathComponent } from './components/requirement-reports/workflows/workflow-best-path/workflow-best-path.component';
import { ModalDataCoverageModule } from './components/requirement-reports/workflows/modal-data-coverage/modal-data-coverage.module';
import { ModalDataCoverageComponent } from './components/requirement-reports/workflows/modal-data-coverage/modal-data-coverage.component';
import { RiskOptimizerComponent } from './components/predictive-analytics-project/risk-optimizer/risk-optimizer.component';
import { MainDataMiningComponent } from './components/data-mining/main/data-mining-main.component';
import { ModelAssociationsComponent } from './components/data-mining/model-associations/data-mining.component';
import { DashboardComponent } from './components/data-mining/dashboard/dashboard.component';
import { CompareStatesComponent } from './components/predictive-analytics-project/impact-analysis/components/compare-states/compare-states.component';

@NgModule({
  declarations: [
    AiAnaliticsMenuComponent,
    ReportsStatisticsComponent,
    WorkflowsComponent,
    ReportsFilterComponent,
    DefectsPerBussAreaComponent,
    PriorityStackedBarComponent,
    StatusPieChartComponent,
    DiscoveryRatioGroupedBarChartComponent,
    SeverityPerBusinessAreaTreemapComponent,
    RootCausePerBusinessAreaTreemapComponent,
    TimeToFixPerBusAreaRootCauseComponent,
    WorkflowBestPathComponent,
    DefectReplayComponent,
    SummaryReportsComponent,
    ImpactAnalysisComponent,
    RiskOptimizerComponent,
    FiltersComponent,
    CurrentSprintsComponent,
    SearchSprintComponent,
    PieChartComponent,
    PassedSprintsComponent,
    CompareSprintsComponent,
    FutureProjectionComponent,
    TestExecutionComponent,
    RequirementsPrioritizationComponent,
    TraceabilityReportComponent,
    ProcessManagmentReportsComponent,
    PredictiveAnalyticsDefectComponent,
    PredictiveAnalyticsProjectComponent,
    DashboardMainComponent,
    DashboardPopupComponent,
    MainDataMiningComponent,
    DataAssociationsComponent,
    ModelAssociationsComponent,
    DashboardComponent,
    CompareStatesComponent
  ],
  imports: [
    PapaParseModule,
    CommonModule,
    CoreModule,
    AiAnalyticsRoutingModule,
    DataTablesModule,
    NgbModule,
    FormsModule,
    ModalDataCoverageModule,
    ChartsModule,
    NgxBootstrapSliderModule,
    Ng5SliderModule,
    ReactiveFormsModule,
    TreeViewModule,
    DropDownsModule,
    TabsModule.forRoot(),
    SelectDropDownModule,
    NgProgressModule,
    LoadingBarModule,
    MatTableModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    HttpClientModule,
    LayoutModule,
    WindowModule,
    DialogModule,
    DialogsModule,
    ButtonsModule,
    PopupModule,
    MenuModule,
    InputsModule,
    GridModule,
    ExcelModule,
    NgMultiSelectDropDownModule
  ],
  providers: [DataExtractionServiceService],
  exports: [MatTableModule],
  entryComponents: [ModalDataCoverageComponent]
})
export class AiAnalyticsModule {
}
