import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import { Observable } from 'rxjs';
import { SessionService } from 'src/app/core/services/session.service';

@Injectable({
  providedIn: 'root'
})
export class ReleaseSummaryReportService {
  constructor(
    private http: HttpClient,
    private configurationService: ConfigurationService,
    private sessionService: SessionService
  ) {
  }

  private reportsUrl = this.configurationService.serverSettings.reportsUrl;
  private apiUrl = this.configurationService.serverSettings.apiUrl;

  getTestExecutionBySubProject(projectId: string): Observable<any> {
    let url = this.reportsUrl + 'getTestExecutionBySubProject/projectId/' + projectId ;
    let res = this.http.get(url);
    return res;
  }

  getSprintsComparisonResults (data: any): Observable<any> {
    const url = this.reportsUrl + 'getsprintsComparisonResults';
    const res = this.http.post(url, data);
    return res;
  }

  getReleaseSummaryProjection(data: any): Observable<any> {
    const url = this.reportsUrl + 'getReleaseSummaryProjection';
    const res = this.http.post(url, data);
    return res;
  }

  getReleaseSummaryComparisonResult(projectId: string, subProjectId: string): Observable<any> {
    const url = this.reportsUrl + 'getReleaseSummaryComparisonResult?ProjectId=' + projectId + (subProjectId ? '&SubProjectId=' + subProjectId : '');
    const res = this.http.get(url);
    return res;
  }

  getProjectId(): Observable<any> {
    let url = this.reportsUrl + 'getProjectId';
    let res = this.http.get(url);
    return res;
  }

  getSubprojectId(projectId: string):Observable<any> {
    let url = this.reportsUrl + 'getsubprojectId/projectId/' + projectId;
    let res = this.http.get(url);
    return res;
  }

  getSprints(subProjectId: string):Observable<any> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    let url = this.apiUrl + 'Process/Sprint/GetSprintsBySubProject?SubProjectId=' + subProjectId;
    let res = this.http.get(url, {headers: headers});
    return res;
  }
}
