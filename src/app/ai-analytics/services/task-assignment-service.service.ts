import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskAssignmentServiceService {
  constructor(
    private http: HttpClient,
    private httpExecutor: HttpExecutorService,
    private configurationService: ConfigurationService,
    private sessionService: SessionService
  ) {
  }

  private reportsUrl = this.configurationService.serverSettings.reportsUrl;
  private apiUrl = this.configurationService.serverSettings.apiUrl;
  response: any;

  getAllDevelopersWorkload(hashId: string): Observable<any> {
    let url = this.reportsUrl + 'getAllDevelopersWorkload/hashId/' + hashId ;
    let res = this.http.get(url);
    return res;
  }

  // getOverdueTasksUI(hashId: string): Observable<any> {
  //   let url = this.reportsUrl + 'getOverdueTasksUI/hashId/' + hashId ;
  //   let res = this.http.get(url);
  //   return res;
  // }

  getOverdueTasksUI(hashId: string, data: any): Observable<any> {
    let url = this.reportsUrl + 'getOverdueTasksUI/hashId/' + hashId 
    let res = this.http.post(url, data)
    return res;
  }
  
  getAllDevelopers(hashId: string): Observable<any> {
    let url = this.reportsUrl + 'getAllDevelopers/hashId/' + hashId ;
    let res = this.http.get(url);
    return res;
  }

  getTaskSchedule(hashId: string): Observable<any> {
    let url = this.reportsUrl + 'getTaskSchedule/hashId/' + hashId ;
    let res = this.http.get(url);
    return res;
  }

  commitTaskSchedule(hashId: string): Observable<any> {
    let url = this.reportsUrl + 'commitTaskSchedule/hashId/' + hashId;
    let res = this.http.get(url);
    return res;
  }
}
