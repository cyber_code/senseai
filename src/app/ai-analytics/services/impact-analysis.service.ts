import {Injectable} from '@angular/core';
import {Report} from '../report';
import {REPORTS} from '../mock-reports';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import 'rxjs/add/operator/map';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import { SessionService } from 'src/app/core/services/session.service';

@Injectable({
  providedIn: 'root'
})
export class ImpactAnalysisService {

  constructor(
    private http: HttpClient,
    private httpExecutor: HttpExecutorService,
    private configurationService: ConfigurationService,
    private sessionService: SessionService
  ) {
  }

  private reportsUrl = this.configurationService.serverSettings.reportsUrl;
  private apiUrl = this.configurationService.serverSettings.apiUrl;

  response: any;

  getProjectId(): Observable<any> {
    let url = this.reportsUrl + 'getProjectId';
    let res = this.http.get(url);
    return res;
  }

  getsubprojectId(projectId:string):Observable<any> {
    let url = this.reportsUrl + 'getsubprojectId/projectId/' + projectId;
    let res = this.http.get(url);
    return res;
  }

  getsprintsId(Subproject:string):Observable<any> {
    let url = this.reportsUrl + 'getsprintsId/Subproject/' + Subproject;
    let res = this.http.get(url);
    return res;
  }

  gettreeviewHierarchy(sprintId:string):Observable<any> {
    let url = this.reportsUrl + 'gettreeviewHierarchy/sprintId/' + sprintId;
    let res = this.http.get(url);
    return res;
  }

  get_business_optimizer_UI(projectId:string, subProjectId:string, sprintId:string, Machine_Budget:number, Time_Spent:number, Personel_Budget:number, Resources:number, optimized:string):Observable<any> {
    let url = this.reportsUrl + 'RiskOptimizerUI/projectId/' + projectId + '/subProjectId/' + subProjectId + '/sprintId/' + sprintId + '/Machine_Budget/' + Machine_Budget + '/Time_Spent/' + Time_Spent + '/Personel_Budget/' + Personel_Budget + '/Resources/' + Resources + '/optimized/' + optimized;
    let res = this.http.get(url);
    return res;
  }


  get_defects_by_root_cause(projectId:string, subProjectId:string, sprintId:string):Observable<any> {
    let url = this.reportsUrl + 'get_defects_by_root_cause/projectId/' + projectId + '/subProjectId/' + subProjectId + '/sprintId/' + sprintId;
    let res = this.http.get(url);
    return res;
  }

  impactFilteringUI(subProjectId:string, sprintId1:string, sprintId2:string):Observable<any> {
    let url = this.reportsUrl + 'impactFilteringUI/subProjectId/' + subProjectId + '/sprintId1/' + sprintId1 + '/sprintId2/' + sprintId2;
    let res = this.http.get(url);
    return res;
  }


  allDatasunburstUI(subProjectId:string, data: any):Observable<any> {
    let url = this.reportsUrl + 'allDatasunburstUI/subProjectId/' + subProjectId ;
    let res = this.http.post(url, data);
    return res;
  }

  allDatasunburstAffectedUI(subProjectId: string, data: any): Observable<any> {
    let url = this.reportsUrl + 'allDatasunburstAffectedUI/subProjectId/' + subProjectId;
    let res = this.http.post(url, data);
    return res;
  }

  getSprintsYear(subProjectId:string, sprintId:string):Observable<any> {
    let url = this.reportsUrl + 'getSprintsYear/subProjectId/' + subProjectId + '/sprintId/' + sprintId;
    let res = this.http.get(url);
    return res;
  }

}
