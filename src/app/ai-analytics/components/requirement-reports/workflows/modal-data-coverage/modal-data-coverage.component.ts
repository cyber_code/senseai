import { Component, OnInit, Input } from '@angular/core';
import * as d3 from 'd3';
import { WorkflowsService } from '../../../../services/workflow.service';
import { ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Report } from '../../../../report';

@Component({
  selector: 'app-modal-data-coverage',
  templateUrl: './modal-data-coverage.component.html',
  styleUrls: ['./modal-data-coverage.component.css']
})
export class ModalDataCoverageComponent implements OnInit {
  title: string;
  closeBtnName: string;
  list: any[] = [];
  root: any;
  focus: any;
  svg: any;
  width = 960;
  height = 500;
  workflowTitleModal: string;
  percentage: Int16Array;
  nodata: any;
  nodataParam: any;
  datacoverageList: any;
  datafrequencyList: any;

  @Input() public hashId;
  @Input() public seq_id;
  @Input() public workflowTitle;
  @Input() public dataCoverage;

  constructor(public activeModal: NgbActiveModal, private workflowService: WorkflowsService) {}

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  zoomable_icicle_data: any;
  @Input() report: Report;
  treemapInput: any;

  drillDown(application, version, item_selected) {
    this.workflowService.getDataFrequency(application, version, item_selected, this.hashId).subscribe((receivedData: any) => {
      this.datafrequencyList = receivedData;
      if (receivedData.length == null) {
        this.nodataParam = false;
      } else {
        this.nodataParam = true;
      }
    });
  }
  createZoomableTree(receivedData: any): any {
    var box = document.querySelector('#domainDrillDown_data_Coverage');

    this.width = box.clientWidth;
    // var height = box.clientHeight;

    // svg.selectAll('.g-treemap-main-type_per_bus_areas').remove();

    var margin = { top: 20, right: 0, bottom: 0, left: 0 },
      width = this.width, //640
      height = 400,
      formatNumber = d3.format(',d'),
      transitioning;

    var x = d3
      .scaleLinear()
      .domain([0, width])
      .range([0, width]);

    var y = d3
      .scaleLinear()
      .domain([0, height - margin.top - margin.bottom])
      .range([0, height - margin.top - margin.bottom]);

    var fader = function(color) {
        return d3.interpolateRgb(color, '#fff')(0.2);
      },
      color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
      format = d3.format(',d');

    var format = d3.format(',d');

    var treemap;

    var svg, grandparent;

    function updateDrillDown() {
      d3.selectAll('.treemap_root_cause').remove();
      // d3.selectAll('.grandparent').remove();

      svg = d3
        .select('#domainDrillDown_data_Coverage')
        .append('svg')
        .attr('class', 'treemap_root_cause')
        .attr('width', width - margin.left - margin.right)
        .attr('height', height - margin.bottom - margin.top)
        .style('margin-left', -margin.left + 'px')
        .style('margin.right', -margin.right + 'px')
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .style('shape-rendering', 'crispEdges');

      grandparent = d3
        .select('#root_cause_grandparent')
        .attr('class', 'grandparent')
        .attr('class', 'grandparent')
        .style('border', '2px solid #9E9E9E')
        .style('border-radius', '12px')
        .style('width', '20%')
        .style('margin-top', '10px');

      // grandparent = svg.append('g')
      //   .attr('class', 'grandparent')
      //   .style('fill', 'orange')
      //   .style('color', '#000');;

      // grandparent.append('rect')
      //   .attr('y', -margin.top)
      //   .attr('width', width)
      //   .attr('height', margin.top);

      // grandparent.append('text')
      //   .attr('x', 6)
      //   .attr('y', 6 - margin.top)
      //   .attr('dy', '.75em');

      treemap = d3
        .treemap()
        //.tile(d3.treemapResquarify)
        .size([width, height])
        .round(false)
        .paddingInner(1);

      var root = d3
        .hierarchy(receivedData)
        .eachBefore(function(d: any) {
          d.id = (d.parent ? d.parent.id + '.' : '') + d.data.name;
        })
        .sum((d: any) => d.value) //d.size
        .sort(function(a, b) {
          return b.height - a.height || b.value - a.value;
        });

      initialize(root);
      accumulate(root);
      layout(root);
      treemap(root);
      display(root);
    }

    function initialize(root) {
      root.x = root.y = 0;
      root.x1 = width;
      root.y1 = height;
      root.depth = 0;
    }

    function accumulate(d) {
      return (d._children = d.children)
        ? (d.value = d.children.reduce(function(p, v) {
            return p + accumulate(v);
          }, 0))
        : d.value;
    }

    function layout(d) {
      if (d._children) {
        d._children.forEach(function(c) {
          c.x0 = d.x0 + c.x0 * d.x1;
          c.y0 = d.y0 + c.y0 * d.y1;
          c.x1 *= d.x1;
          c.y1 *= d.y1;
          c.parent = d;
          layout(c);
        });
      }
    }

    function display(d) {
      grandparent.datum(d.parent).on('click', transition);

      var g1 = svg
        .insert('g', '.grandparent')
        .datum(d)
        .attr('class', 'depth');

      var g = g1
        .selectAll('g')
        .data(d._children)
        .enter()
        .append('g');

      g.filter(function(d) {
        return d._children;
      })
        .classed('children', true)
        .on('click', transition);

      g.append('rect')
        .attr('class', 'parent')
        .call(rect);

      var t = g
        .append('text')
        .attr('class', 'ptext')
        .attr('dy', '.75em');

      t.append('tspan').text(function(d) {
        return d.data.name;
      });
      t.append('tspan')
        .attr('dy', '2.0em')
        .text(function(d) {
          return formatNumber(d.value);
        });
      t.call(text);

      g.selectAll('rect').style('fill', function(d) {
        return color(d.data.name);
      });

      function transition(d) {
        if (transitioning || !d) {
          return;
        }
        transitioning = true;
        var g2 = display(d),
          t1 = g1.transition().duration(750),
          t2 = g2.transition().duration(750);
        x.domain([d.x0, d.x0 + d.x1]);
        y.domain([d.y0, d.y0 + d.y1]);
        svg.style('shape-rendering', null);
        svg.selectAll('.depth').sort(function(a, b) {
          return a.depth - b.depth;
        });
        g2.selectAll('text').style('fill-opacity', 0);
        t1.selectAll('text')
          .call(text)
          .style('fill-opacity', 0);
        t2.selectAll('text')
          .call(text)
          .style('fill-opacity', 1);
        t1.selectAll('rect').call(rect);
        t2.selectAll('rect').call(rect);
        t1.remove().on('end', function() {
          svg.style('shape-rendering', 'crispEdges');
          transitioning = false;
        });
      }

      return g;
    }

    function text(text) {
      text.selectAll('tspan').attr('x', function(d) {
        return x(d.x0) + 6;
      });
      text
        .attr('x', function(d) {
          return x(d.x0) + 6;
        })
        .attr('y', function(d) {
          return y(d.y0) + 10;
        })
        .style('opacity', function(d) {
          return this.getComputedTextLength() < x(d.x0 + d.x1) - x(d.x0) ? 1 : 0;
        });
    }

    function text2(text) {
      text
        .attr('x', function(d) {
          return x(d.x0 + d.x1) - this.getComputedTextLength() - 6;
        })
        .attr('y', function(d) {
          return y(d.y0 + d.y1) - 6;
        })
        .style('opacity', function(d) {
          return this.getComputedTextLength() < x(d.x0 + d.x1) - x(d.x0) ? 1 : 0;
        });
    }

    function rect(rect) {
      rect
        .attr('x', function(d) {
          return x(d.x0);
        })
        .attr('y', function(d) {
          return y(d.y0);
        })
        .attr('width', function(d) {
          return x(d.x0 + d.x1) - x(d.x0);
        })
        .attr('height', function(d) {
          return y(d.y0 + d.y1) - y(d.y0);
        });
    }

    function name(d) {
      return d.parent
        ? name(d.parent) + ' / ' + d.data.name + ' (' + formatNumber(d.value) + ')'
        : d.data.name + ' (' + formatNumber(d.value) + ')';
    }

    $(function() {
      updateDrillDown();
    });
  }

  ngOnInit() {
    this.workflowTitleModal = this.workflowTitle;
    this.percentage = this.dataCoverage;
    this.workflowService.getDataCoverage(this.hashId, this.seq_id).subscribe((receivedData: any) => {
      this.datacoverageList = receivedData;
      this.nodataParam = false;
      if (receivedData.length == null) {
        this.nodata = true;
      } else {
        this.nodata = false;
      }
    });
  }
}
