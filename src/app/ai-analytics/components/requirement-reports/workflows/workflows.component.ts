import {
  Component,
  OnInit,
  Input,
  ViewChild,
  AfterViewInit
} from "@angular/core";
import { ReportService } from "../../../services/report.service";
import { WorkflowsService } from "../../../services/workflow.service";
import { Report } from "../../../report";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { DataTableDirective } from "angular-datatables";
import { ModalDataCoverageComponent } from "./modal-data-coverage/modal-data-coverage.component";
import { WorkflowBestPathComponent } from "./workflow-best-path/workflow-best-path.component";
import { SessionService } from "src/app/core/services/session.service";
import { environment } from "../../../../../environments/environment";
import { PermissionService } from "src/app/core/services/permission.service";

@Component({
  selector: "app-workflows",
  templateUrl: "./workflows.component.html",
  styleUrls: ["./workflows.component.css"]
})
export class WorkflowsComponent implements OnInit, AfterViewInit {
  @Input() report: Report;
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtInstance: DataTables.Api;
  coverage = "Coverage (%)";
  hashId = this.sessionService.getHashId();
  businessArea: any;
  workflowList: any;
  workflowTitle: string;
  workflowCoverage: string;
  workflowDataCoverage: any;
  closeResult: string;
  workflowTitleModal: string;
  rowClicked = false;
  splited_val: any;
  seq_id_selected: any;
  dataCoverage: any;
  ver1: string;
  ver2: string;
  modalReference: any;
  workflowId: string;
  loading: boolean = false;
  sequence;

  ProcessesFilter: Array<Object> = [
    { num: 0, name: "Created Workflows" },
    { num: 1, name: "Sequences" }
  ];

  businessAreaFilter: Array<Object> = [" "];
  selectedProcessesFilter = this.ProcessesFilter[0];
  selectedbusinessAreaFilter = this.businessAreaFilter[0];
  @ViewChild(WorkflowBestPathComponent) child;
  selectedRow: Number;
  constructor(
    private sessionService: SessionService,
    private reportService: ReportService,
    private workflowService: WorkflowsService,
    private modalService: NgbModal,
    public permissionService: PermissionService
  ) {}

  public onChangeProcessesFilter() {
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  onChangebusinessAreaFilter() {
    this.workflowService
      .getWorkflowsPerBusArea(
        this.hashId,
        this.selectedbusinessAreaFilter as string,
        this.selectedProcessesFilter["num"]
      )
      .subscribe((receivedData: any) => {
        this.workflowList = receivedData;
      });
  }

  public click_apply_btn(event) {
    if (this.selectedbusinessAreaFilter === " ") {
      this.onChangeProcessesFilter();
    } else {
      this.onChangebusinessAreaFilter();
    }
  }
  ngAfterViewInit() {}

  ngOnInit() {
    this.workflowList = [];
    // this.reportService
    //   .getDataForTreemap(this.hashId)
    //   .subscribe((receivedData: any) => {
    //     this.businessArea = receivedData;
    //     let data = {
    //       name: "Business Areas",
    //       children: []
    //     };
    //     let children_list = this.businessArea;
    //     data.children = children_list;
    //     this.businessAreaFilter.push(data.children);
    //   });
    if (this.permissionService.hasPermission("/getDataWorkFlowsTable/hashId/")){
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 10,
        serverSide: true,
        processing: true,
        searching: true,
        ajax: (data: any, callback) => {
          this.workflowService
          .getWorkflowsForTable(this.hashId, this.selectedProcessesFilter["num"], data)
          .subscribe((resp: any) => {
            var tmp_data = resp.data.length > 0 ? JSON.parse(resp.data) : [];

              for (var i = 0; i < tmp_data.length; i++) {
                for (var j = 0; j < resp.history.length; j++) {
                    let history = JSON.parse(resp.history[j].DATA);
                    tmp_data[i].history = history;
                }
              }
              this.workflowList = tmp_data;
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsFiltered,
                data: []
              });
            });
        },
        columns: [
          // { name: 'WORKFLOWID', orderable: false },
          { name: 'Title', orderable: true },
          { name: 'Coverage', orderable: true }
        ]
      };
    }
      
  }


  onClickForModalOpen(input, item) {
    if ((input == "DC") && (this.selectedProcessesFilter["num"] == 1)) {
      this.modalReference = this.modalService.open(ModalDataCoverageComponent);
      this.modalReference.componentInstance.hashId = this.hashId;
      this.modalReference.componentInstance.seq_id = this.seq_id_selected;
      this.modalReference.componentInstance.workflowTitle = this.workflowTitle;
      this.modalReference.componentInstance.dataCoverage = this.dataCoverage;
      this.modalReference.result.then((result: any) => {}, (reason: any) => {});
    }
  }

  onRowClick(item) {
    this.loading = true;
    this.sequence = item.SEQUENCE;
    this.workflowTitle = item.TITLE;
    this.workflowCoverage = item.COVERAGE;
    this.workflowDataCoverage = item.DATACOVERAGE;
    this.rowClicked = true;
    this.seq_id_selected = item.SEQ_ID;
    this.dataCoverage = item.DATACOVERAGE;
    this.workflowId = item.WORKFLOWID;
    this.loading = false;
  }
  highlightRow(i: number) {
    this.selectedRow = i;
  }
  ngOnDestroy() {}
}
