import { DesignService } from 'src/app/design/services/design.service';
import {WorkflowService} from 'src/app/design/services/workflow.service';
import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { SessionService } from 'src/app/core/services/session.service';
import { HighLighGraphHelper } from 'src/app/shared/highlight-graph-helper';
import KitchenSinkService from 'src/app/design/services/rapid-services/kitchensink-service';
import { StencilService } from 'src/app/design/services/rapid-services/stencil-service';
import { ToolbarService } from 'src/app/design/services/rapid-services/toolbar-service';
import { InspectorService } from 'src/app/design/services/rapid-services/inspector-service';
import { HaloService } from 'src/app/design/services/rapid-services/halo-service';
import { KeyboardService } from 'src/app/design/services/rapid-services/keyboard-service';
import { RapidEventsService } from 'src/app/design/services/rapid-events.service';
import { MessageType } from 'src/app/core/models/message.model';
import { MessageService } from 'src/app/core/services/message.service';
import { WorkflowsService } from 'src/app/ai-analytics/services/workflow.service';
import {WorkflowItemActionType} from 'src/app/core/models/workflow-item-action-type';
import {WorkflowItemType} from 'src/app/core/models/workflow-item-type';
import { WorkflowItemCustomData } from 'src/app/design/models/workflow-item-custom-data.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-workflow-best-path',
  templateUrl: './workflow-best-path.component.html',
  styleUrls: ['./workflow-best-path.component.css']
})
export class WorkflowBestPathComponent implements OnInit {
  private rappid: KitchenSinkService;
  private workflowId: string;
  private _sequence = [];
  private hashId = this.sessionService.getHashId();
  private actionTypes = [
    {action: 'I', type: WorkflowItemActionType.Input}, 
    {action: 'A', type: WorkflowItemActionType.Authorise}, 
    {action: 'S', type: WorkflowItemActionType.See},
    {action: 'E', type: WorkflowItemActionType.EnquiryResult},
    {action: 'EA', type: WorkflowItemActionType.EnquiryAction}
  ];

  @Input() public workflowTitle;

  @Input() public set workflowSelectedById(workflowId: string) {
    this.workflowId = workflowId;
    this.loadWorkflow();
  }

  @Input()
  public set sequence(sequence) {
    this._sequence = sequence;
    this.loadWorkflow();
  };

  constructor(
    private rapidEventsService: RapidEventsService,
    private element: ElementRef,
    private designService: DesignService,
    private sessionService: SessionService,
    private messageService: MessageService,
    private workflowsService: WorkflowsService,
    private workflowService: WorkflowService
  ) {}

  ngOnInit() {
    this.rappid = new KitchenSinkService(
      this.element.nativeElement as any,
      new StencilService(),
      new ToolbarService(),
      new InspectorService(),
      new HaloService(),
      new KeyboardService(),
      this.rapidEventsService,
      false
    );

    this.rappid.startRappid();
  }

  private loadCreatedWorkflows(workflowId) {
    this.designService.getWorkflow(this.workflowId).subscribe(workflow => {
      if (workflow) {
        const jsonParsed = (workflow && workflow.designerJson && JSON.parse(workflow.designerJson)) || { cells: [] };
        if (jsonParsed.cells.length === 0) {
          this.rappid.graph.clear();
          this.messageService.sendMessage({ text: `Workflow ${workflow.title} doesn't exist! `, type: MessageType.Error });
          return;
        }
        this.rappid.loadGraphJson(jsonParsed);
        this.loadPaths(workflow);
      }
    });
  }

  loadPaths(workflow) {
    const workContext = this.sessionService.getWorkContext();
    const graph = this.workflowService.getGraphToSave(JSON.parse(workflow.designerJson));
    const designerJson = JSON.stringify(graph);
    this.designService
    .getPaths(
      workContext.user.tenantId,
      workContext.project.id,
      workContext.subProject.id,
      workContext.defaultSettings.system.id,
      workContext.defaultSettings.catalog.id,
      designerJson
    )
    .subscribe(paths => {
      const bestPath = paths.find(x => x.isBestPath);
      if (bestPath) {
        HighLighGraphHelper.highlight(this.rappid.graph, bestPath.itemIds);
      }
    });
  }

  private async loadSequenceWorkflows() {
    if (this._sequence) {
      const startElement = {Type: WorkflowItemType.Start} as any;
      const startRappidElement = this.rappid.createRapidElement(startElement);
      let cells = [startRappidElement];
      const sequence = eval(this._sequence as any);
      for (let i = 0; i < sequence.length; i++) {
        const result = await this.workflowsService.getStep(this.hashId, sequence[i]).toPromise();
        const actionType = this.actionTypes.find(actiontype => actiontype.action === result.action);
        const element = {Title: result.ver1 + (result.ver2 !== '' ? ', ' + result.ver2 : ''), Type: WorkflowItemType.Action, ActionType: actionType.type} as any;
        const rappidItem = this.rappid.createRapidElement(element);
        const lastLink = _.cloneDeep(cells).reverse().find(cell => cell.attributes.type === "standard.Link");
        let link = this.rappid.createLink();
        if (!lastLink) {
          link.attributes.source = {id: startRappidElement.id};
          link.attributes.target = {id: rappidItem.id};
        } else {
          link.attributes.source = lastLink.attributes.target;
          link.attributes.target = {id: rappidItem.id};
        }
        cells = cells.concat([rappidItem, link]);
      }
      this.rappid.loadGraphJson({cells: cells});
      this.rappid.layoutGraph(true, true);
      this.loadPaths({designerJson: JSON.stringify({cells: cells})});
    }
  }

  public loadWorkflow() {
    if (this.workflowId) {
      this.loadCreatedWorkflows(this.workflowId);
    } else {
      setTimeout(() => {
        this.loadSequenceWorkflows()
      }, 150);
    }
  }
}
