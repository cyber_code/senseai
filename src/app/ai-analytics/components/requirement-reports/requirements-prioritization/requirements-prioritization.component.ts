import { Component, OnInit } from '@angular/core';
import { State, process} from '@progress/kendo-data-query';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { SessionService } from 'src/app/core/services/session.service';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { PermissionService } from 'src/app/core/services/permission.service';


@Component({
  selector: 'app-requirements-prioritization',
  templateUrl: './requirements-prioritization.component.html',
  styleUrls: ['./requirements-prioritization.component.css']
})
export class RequirementsPrioritizationComponent implements OnInit {
  gridData;
  state: State = {
    skip: 0,
    take: 10,
    filter: {
      logic: 'and',
      filters: []
    }
  };
  columns: Array<{title: string, field: string}> = [];
  requirements =  [];
  loading: boolean = false;
  priorities: any;
  selectedPriorities: [] ;
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'title',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    allowSearchFilter: false
  };

  constructor(
    private sessionService: SessionService,
    private processManagment: ProcessManagementService,
    public permissionService: PermissionService
  ) { 
    this.allData = this.allData.bind(this);
  }

  ngOnInit() {
    this.loading = true;
    this.permissionService.hasPermission("/api/Process/Reports/GetRequirementPrioritizationReport") ?
    this.GetRequirementPrioritizationReport().subscribe(result =>{
    if (result){
    this.columns = result['columnNames'].map(res => ({title: res, field: res}));
    let data = result['reportData'].map(datas =>{
			let record = {};
			datas.map((data, index) =>{
        if (index < datas.length -2)
				  record[this.columns[index].title] = data;
      });
      record['Priority'] =  datas[datas.length -1];
      record['Requirement'] =  datas[datas.length -2];
			return record;
		});
		this.requirements = data;
		this.gridData = process(this.requirements, this.state);
		this.loading = false;
      }
    }) : this.loading = false;
    this.permissionService.hasPermission("/api/Process/Configure/GetRequirementPriorities") ?
    this.getRequirementPriorities().subscribe(
      priorities=>{ this.priorities = priorities ;}
    ) : null;
  }

  GetRequirementPrioritizationReport() {
    let workContext = this.sessionService.getWorkContext();
    return this.processManagment.GetRequirementPrioritizationReport(workContext.subProject.id);
  }

  dataStateChange(state) {
    this.state = state;
    this.gridData = process(this.requirements, state);
  }

  allData(): ExcelExportData {
    const result: ExcelExportData =  {
      data: process(this.requirements, {skip: 0,
		  filter: this.state.filter,
		  sort: this.state.sort,
		  group: this.state.group
        }).data
    };
    return result;
  }

  getRequirementPriorities() {
    const subProjectId = this.sessionService.getWorkContext().subProject.id;
    return this.processManagment.getRequirementPriorities(subProjectId);
  }

  removePriorityFilters(filter){
    if(filter["logic"] && filter.filters[0])
    { 
      return false; 
    }
    return true;
  }

  filterByPriorities(){
    this.state.filter.filters = this.state.filter.filters.filter(this.removePriorityFilters);
    if(this.selectedPriorities.length!==0){
      let priorityFilters= [];
      this.selectedPriorities.map(priority=>{
        priorityFilters.push({field: 'Priority', operator: 'eq', value: priority["title"]});
      })
      this.state.filter.filters.push({
        logic: 'or',
        filters: priorityFilters
      });
    }
    this.gridData = process(this.requirements, this.state);
  }

  onSelectDeselectAll(){
    this.state.filter.filters = this.state.filter.filters.filter(this.removePriorityFilters);
    this.gridData = process(this.requirements, this.state);
  }

}
