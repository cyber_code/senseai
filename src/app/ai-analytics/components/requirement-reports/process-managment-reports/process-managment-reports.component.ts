import { Component, OnInit } from '@angular/core';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';

@Component({
  selector: 'app-process-managment-reports',
  templateUrl: './process-managment-reports.component.html',
  styleUrls: ['./process-managment-reports.component.css']
})
export class ProcessManagmentReportsComponent implements OnInit {

  public license = Licenses;

  constructor(public licenseService: LicenseService) { }

  ngOnInit() {}

}
