import {Component, OnInit} from '@angular/core';
import {Label, SingleDataSet} from 'ng2-charts';
import {ChartDataSets, ChartOptions} from 'chart.js';
import {SessionService} from '../../../../core/services/session.service';
import {IDropdownSettings} from 'ng-multiselect-dropdown';
import {formatDate} from '@angular/common';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-traceability-report',
  templateUrl: './traceability-report.component.html',
  styleUrls: ['./traceability-report.component.css']
})
export class TraceabilityReportComponent implements OnInit {
  public hierarchies: any[] = [];

  constructor(
    private processManagment: ProcessManagementService, 
    private sessionService: SessionService,
    public permissionService: PermissionService
  ) {}

  public hierarchyTypes: any[] = [];
  public doughnutChartLabels: Label[] = ['Failed', 'Successful'];
  public doughnutChartData: any[] = [];
  public doughnutChartType: any = 'doughnut';
  public doughnutColors: any[] = [{backgroundColor: ['rgba(150,201,110)', '#ff0000']}];
  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{stacked: true}],
      yAxes: [{stacked: true}]
    }
  };
  public barChartLabels: Label[] = ['2006', '2007', '2008'];
  public barChartType: any = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [];
  public barChartColors: any[] = [{backgroundColor: ['rgba(150,201,110)', 'rgba(150,201,110)', 'rgba(150,201,110)', 'rgba(150,201,110)', 'rgba(150,201,110)']}, {backgroundColor: ['#ff0000', '#ff0000', '#ff0000', '#ff0000', '#ff0000']}];
  public barChartData: ChartDataSets[] = [
    {data: [28, 48, 40], label: 'Success'},
    {data: [65, 59, 80], label: 'Failed'}
  ];
  public lineChartColors: any[] = [
    {
      borderColor: 'rgba(150,201,110)',
      backgroundColor: 'rgba(150,201,110)',
      pointBackgroundColor: 'rgba(150,201,110)',
      pointBorderColor: 'rgba(150,201,110)',
      pointHoverBackgroundColor: 'rgba(150,201,110)',
      pointHoverBorderColor: 'rgba(150,201,110)'
    }, {
      borderColor: '#ff0000',
      backgroundColor: '#ff0000',
      pointBackgroundColor: '#ff0000',
      pointBorderColor: '#ff0000',
      pointHoverBackgroundColor: '#ff0000',
      pointHoverBorderColor: '#ff0000'
    }
  ];
  public lineChartType: any = 'line';
  public nextLevel: any;
  public currentLevel: any;
  public currentLevelHierarchyId: any;
  public currentLevelTitle: any;
  public subProjectId = this.sessionService.getSubProject().id;
  public businessAreaData: any[] = [];
  public hierarchyTypeTitle: any = '';
  public i: any;

  // Level = 0, shows Hierarchy
  // type = 1, shows Process
  // type = 2, shows Workflow
  // type=3, new level

  public levelHistory: any[] = [];
  public hierarchyHistory: any[] = [];
  public hierarchyTitleHistory: any[] = [];
  public hierarchyTypePosition: any;
  public position: any;
  public collapseData: any[] = [];
  public hierarchyTypesByPosition: any = [];

  dropdownList = [];
  selectedItems = [];
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'title',
    selectAllText: 'Select All',
    unSelectAllText: 'Unselect All',
    allowSearchFilter: false
  };

  ngOnInit() {
    this.levelHistory.push(null);
    this.hierarchyHistory.push(null);
    this.hierarchyTitleHistory.push(null);
    this.position = 0;
    this.currentLevel = 0;
    this.updateHierarchyTypeTitle();
    
    this.getTraceabilityLine();
    this.permissionService.hasPermission("/api/Process/Configure/GetIssueTypes") ?
    this.processManagment.getIssueTypes(this.subProjectId).subscribe(issueTypes => {
      if (issueTypes) {
        this.dropdownList = issueTypes;
        this.selectedItems = issueTypes;
      }
    }): null;
    this.permissionService.hasPermission("/api/Process/Configure/GetHierarchyTypes") ?
      this.processManagment.getHierarchyTypes(this.subProjectId).subscribe(hierarchyTypes => {
        this.i = 0;
        if (hierarchyTypes.length > 0) {
          this.hierarchyTypePosition = 0;
        }
        if (hierarchyTypes) {
          hierarchyTypes.forEach(ht => {
            this.hierarchyTypes.push({i: this.i, level: 0, title: ht.title});
            this.i++;
          });
          this.hierarchyTypes.push({i: this.i, level: 1, title: 'Requirements'});
          this.i++;
          this.hierarchyTypes.push({i: this.i, level: 2, title: 'Workflows'});
          this.i++;
          // this.hierarchyTypes.push({i:this.i,level:2,title:'Test Case'});
          // this.i++;
        }
    }) : null;


  }

  showProcessesOfHierarchy(hierarchyId) {

    let searchLevel = 0;
    if (this.nextLevel > 0) {
      searchLevel = this.nextLevel - 1;
    }
    const issueTypes = [];
    if (this.selectedItems.length > 0) {
      this.selectedItems.forEach(si => {
        issueTypes.push(si.id);
      });
    }

    this.processManagment.getTraceabilityReportDetail(this.subProjectId, hierarchyId, searchLevel, issueTypes).subscribe(res => {
      this.businessAreaData.forEach(ba => {
        ba.collapseData = [];
        if (ba.id === hierarchyId) {
          if (res['length'] === 0) {
            ba.collapseData = [{
              title: 'No Data',
              workflows: '-',
              testCases: '-/-',
              openIssues: '-',
              failedExecutions: '-',
              successfulExecutions: '-'
            }];
          } else {
            ba.collapseData = res;
          }
        }
      });
    });
  }

  diveInNextHierarchy(hierarchyTitle, hiearachyId) {
    if (this.nextLevel === 3) {
      return;
    }

    if (this.hierarchyTypePosition < this.hierarchyTypes.length) {
      this.hierarchyTypePosition++;
    }
    this.currentLevelHierarchyId = hiearachyId;
    this.updateHierarchyTypeTitle();
    this.levelHistory.push(this.nextLevel);
    this.hierarchyHistory.push(hiearachyId);
    this.hierarchyTitleHistory.push(hierarchyTitle);
    this.position++;
    // if (this.position >= this.hierarchyTypes.length) {
    //   this.i++;
    //   this.hierarchyTypes.push({i: this.i, level: this.currentLevel, title: ''});
    // }

    this.getTraceabilityLine(this.nextLevel, hiearachyId);
  }

  backStep() {
    if (this.position - 1 < 0) {
      return;
    } else if (this.position - 1 === 0) {
      this.hierarchyHistory.pop();
      this.levelHistory.pop();
      this.hierarchyTitleHistory.pop();
      this.position--;
      this.hierarchyTypePosition--;

      this.getTraceabilityLine();
    } else {
      this.hierarchyHistory.pop();
      this.levelHistory.pop();
      this.hierarchyTitleHistory.pop();
      this.position--;
      this.hierarchyTypePosition--;

      this.getTraceabilityLine(this.levelHistory[this.position], this.hierarchyHistory[this.position]);
    }
  }

  getTraceabilityLine(Level?, HierarchyId?) {
    const issueTypes = [];
    if (this.selectedItems.length > 0) {
      this.selectedItems.forEach(si => {
        issueTypes.push(si.id);
      });
    }
    this.currentLevel = Level;

    if (this.permissionService.hasPermission("/api/Process/Reports/GetTraceabilityReportLine")){
      this.processManagment.getTraceabilityReportLine(this.subProjectId, Level, HierarchyId, issueTypes).subscribe(res => {
        const newDate = new Date();
        const dateToday = formatDate(newDate, 'yyyy-MM-dd', 'en-US');
        const response = res;
        // const response = {"nextLevel":0,"lines":[{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","title":"Core Parameter Maintenance Processes","processes":8,"workflows":5,"testCases":7,"plannedTestCases":8,"openIssues":4,"failedExecutions":2,"successfulExecutions":3},{"id":"3eb2e370-7b1e-4db2-8563-2efefad7cf17","title":"Corporate Banking Processes","processes":174,"workflows":14,"testCases":8,"plannedTestCases":27,"openIssues":6,"failedExecutions":1,"successfulExecutions":3},{"id":"a23f1a97-190f-4854-892a-41f1f95eab40","title":"Country/ Regional Processes","processes":9,"workflows":1,"testCases":2,"plannedTestCases":0,"openIssues":0,"failedExecutions":1,"successfulExecutions":1},{"id":"2dcccf44-3f12-47ec-9178-97866556ea01","title":"Customer Relationship Management Processes","processes":51,"workflows":2,"testCases":2,"plannedTestCases":3,"openIssues":2,"failedExecutions":1,"successfulExecutions":1},{"id":"1060b681-a817-4474-a41b-178c3d00b546","title":"Digital Engagement Processes","processes":22,"workflows":2,"testCases":2,"plannedTestCases":2,"openIssues":0,"failedExecutions":1,"successfulExecutions":1},{"id":"d11b4b1c-66c7-43e3-97a2-b7d3481defb3","title":"Financial Accounting Parameter Maintenance Processes","processes":28,"workflows":0,"testCases":0,"plannedTestCases":0,"openIssues":0,"failedExecutions":0,"successfulExecutions":0},{"id":"1d5c893b-1b72-49d9-b5a3-e25821c098eb","title":"Islamic Banking Processes","processes":99,"workflows":0,"testCases":0,"plannedTestCases":0,"openIssues":0,"failedExecutions":0,"successfulExecutions":0},{"id":"20996046-cfac-4775-841f-bdbd14aba655","title":"Online Channels Processes","processes":97,"workflows":0,"testCases":0,"plannedTestCases":0,"openIssues":0,"failedExecutions":0,"successfulExecutions":0},{"id":"85968989-01b7-4ce2-9823-33f5d2ce66de","title":"Private Wealth Management Processes","processes":216,"workflows":0,"testCases":0,"plannedTestCases":0,"openIssues":0,"failedExecutions":0,"successfulExecutions":0},{"id":"f1a5212f-88af-4fef-9c45-ff4e215b466e","title":"Regulatory Reporting & Compliance","processes":16,"workflows":0,"testCases":0,"plannedTestCases":0,"openIssues":0,"failedExecutions":0,"successfulExecutions":0},{"id":"88f7f394-6a5a-4a5f-854b-e2923c91c48a","title":"Retail Banking Processes","processes":127,"workflows":0,"testCases":0,"plannedTestCases":1,"openIssues":0,"failedExecutions":0,"successfulExecutions":0},{"id":"9aa2fdcf-8d5b-4a9e-a2c9-455ff335c9fb","title":"SMS Processes","processes":5,"workflows":0,"testCases":0,"plannedTestCases":0,"openIssues":0,"failedExecutions":0,"successfulExecutions":0},{"id":"f1e85faa-c078-47f1-ad5b-69f5980f58a9","title":"Transactional Processes","processes":120,"workflows":0,"testCases":0,"plannedTestCases":0,"openIssues":0,"failedExecutions":0,"successfulExecutions":0},{"id":"ba8c0eab-b0e1-4130-b345-df46e1eb62fa","title":"Treasury Processes","processes":61,"workflows":4,"testCases":0,"plannedTestCases":0,"openIssues":0,"failedExecutions":0,"successfulExecutions":0}],"executionLines":[{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","day":"2019-11-14","failedExecutions":3,"successfulExecutions":3},{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","day":"2019-11-15","failedExecutions":5,"successfulExecutions":8},{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","day":"2019-11-18","failedExecutions":1,"successfulExecutions":1},{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","day":"2019-11-19","failedExecutions":2,"successfulExecutions":3},{"id":"3eb2e370-7b1e-4db2-8563-2efefad7cf17","day":"2019-11-14","failedExecutions":2,"successfulExecutions":8},{"id":"3eb2e370-7b1e-4db2-8563-2efefad7cf17","day":"2019-11-19","failedExecutions":1,"successfulExecutions":3},{"id":"3eb2e370-7b1e-4db2-8563-2efefad7cf17","day":"2019-11-18","failedExecutions":3,"successfulExecutions":6},{"id":"3eb2e370-7b1e-4db2-8563-2efefad7cf17","day":"2019-11-15","failedExecutions":2,"successfulExecutions":1},{"id":"a23f1a97-190f-4854-892a-41f1f95eab40","day":"2019-11-15","failedExecutions":4,"successfulExecutions":4},{"id":"a23f1a97-190f-4854-892a-41f1f95eab40","day":"2019-11-19","failedExecutions":1,"successfulExecutions":1},{"id":"2dcccf44-3f12-47ec-9178-97866556ea01","day":"2019-11-19","failedExecutions":1,"successfulExecutions":1},{"id":"2dcccf44-3f12-47ec-9178-97866556ea01","day":"2019-11-15","failedExecutions":4,"successfulExecutions":4},{"id":"1060b681-a817-4474-a41b-178c3d00b546","day":"2019-11-15","failedExecutions":4,"successfulExecutions":4},{"id":"1060b681-a817-4474-a41b-178c3d00b546","day":"2019-11-19","failedExecutions":1,"successfulExecutions":1}],"issuesLines":[
        //       {"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","date":"2019-11-14","status":0,"count":2},{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","date":"2019-11-15","status":0,"count":2},{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","date":"2019-11-14","status":2,"count":1},{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","date":"2019-11-17","status":2,"count":8},{"id":"bbc2ff9a-9f0c-4e45-a7b2-d9545ca38d1d","date":"2019-11-18","status":2,"count":1},{"id":"3eb2e370-7b1e-4db2-8563-2efefad7cf17","date":"2019-11-18","status":0,"count":2},{"id":"2dcccf44-3f12-47ec-9178-97866556ea01","date":"2019-11-18","status":0,"count":1}]};
        this.nextLevel = response['nextLevel'];
        this.businessAreaData = res['lines'];
        let groupByIdDate = this.groupBy(response['issuesLines'], function (item) {
          return [item.id, item.date];
        });
  
        console.log(groupByIdDate);
        this.businessAreaData.forEach(ba => {
          ba.doughnutData = [0, 0];
          ba.barLabel = [];
          ba.barLabelData = [];
          ba.barData = [{data: [], label: 'Successful'}, {data: [], label: 'Failed'}];
          response['executionLines'].forEach(td => {
            if (ba.id === td.id) {
              // if (dateToday === td.day) {
              //   ba.doughnutData = [td.failedExecutions, td.successfulExecutions];
              // }
              ba.barLabelData.push({
                date: td.day,
                successfulExecutions: td.successfulExecutions,
                failedExecutions: td.failedExecutions
              });
            }
          });
          ba.barLabelData.sort(function (a, b) {
            return a.date > b.date ? 1 : a.date < b.date ? -1 : 0;
          });
          ba.barLabelData.forEach(bld => {
            ba.barLabel.push(bld.date);
            ba.barData[0].data.push(bld.successfulExecutions);
            ba.barData[1].data.push(bld.failedExecutions);
          });
          ba.lineLabel = [];
          ba.lineData = [
            {data: [], label: 'Open', fill: false, lineTension: 0, backgroundColor: 'rgba(150,201,110)'},
            {data: [], label: 'Closed', fill: false, lineTension: 0, backgroundColor: '#ff0000'}
          ];
          // let grouped = this.groupBy(response['issuesLines'], res => res.date);
          let check = 0;
          /*    response['issuesLines'].forEach(fd => {
                if (ba.id === fd.id) {
                  const dateGrouping = grouped.get(fd.date);
                  if (ba.lineLabel.indexOf(fd.date) === -1) {
                    ba.lineLabel.push(fd.date);
                    check = -1;
                  } else {
                    check = 1;
                  }
                  console.log(dateGrouping);
                  dateGrouping.forEach(dg => {
                    if (check === -1) {
                      if (dateGrouping.length === 1) {
                        if (dg.status === 0) {
                          ba.lineData[0].data.push(dg.count);
                          ba.lineData[1].data.push(0);
                        } else if (dg.status === 2) {
                          ba.lineData[1].data.push(dg.count);
                          ba.lineData[0].data.push(0);
                        }
                      }
                    } else if (check === 1) {
                      if (dg.status === 0) {
                        ba.lineData[0].data.push(dg.count);
                      }
                      if (dg.status === 2) {
                        ba.lineData[1].data.push(dg.count);
                      }
                    }
                  });
                }
              });*/
  
          console.log(groupByIdDate);
          groupByIdDate.forEach(gid => {
            console.log(gid.length);
            gid.forEach(g => {
              if (g.id === ba.id) {
                ba.lineLabel.indexOf(g.date) === -1 ? ba.lineLabel.push(g.date) : '';
                if (gid.length === 1) {
                  if (g.status === 0) {
                    ba.lineData[0].data.push(g.count);
                    ba.lineData[1].data.push(0);
                  }
                  if (g.status === 2) {
                    ba.lineData[1].data.push(g.count);
                    ba.lineData[0].data.push(0);
                  }
                } else {
                  if (g.status === 0) {
                    ba.lineData[0].data.push(g.count);
                  }
                  if (g.status === 2) {
                    ba.lineData[1].data.push(g.count);
                  }
                }
  
  
              }
            });
            // if (ba.id === gid[0].id) {
            //   ba.lineLabel.indexOf(gid[0].date) === -1 ? ba.lineLabel.push(gid[0].date) : '';
            //   console.log(ba.id);
            //   if (gid[0].length === 1) {
            //     if (gid[0].status === 0) {
            //       ba.lineData[0].data.push(gid[0].count);
            //       ba.lineData[1].data.push(0);
            //     } else if (gid[0].status === 2) {
            //       ba.lineData[1].data.push(gid[0].count);
            //       ba.lineData[0].data.push(0);
            //     }
            //   } else if (gid[0].length === 2) {
            //     if (gid[0].status === 0) {
            //       ba.lineData[0].data.push(gid[0].count);
            //     }
            //     if (gid[0].status === 2) {
            //       ba.lineData[1].data.push(gid[1].count);
            //     }
            //   } else {
            //     ba.lineData[0].data.push(0);
            //     ba.lineData[1].data.push(0);
            //   }
            // }
          });
          ba.lineLabel.sort(function (a, b) {
            return a > b ? 1 : a < b ? -1 : 0;
          });
  
  
        });
      });

    }

   
  }


  updateHierarchyTypeTitle() {
    if (!this.currentLevel || this.currentLevel === 0) {
      this.hierarchyTypeTitle = 'Business Areas';
    } else if (this.currentLevel === 1) {
      this.hierarchyTypeTitle = 'Processes';
    } else if (this.currentLevel === 2) {
      this.hierarchyTypeTitle = 'Workflows';
    }
  }

  filterDropdown() {
    this.getTraceabilityLine(this.currentLevel, this.currentLevelHierarchyId);
  }

  resetFilter() {
    this.selectedItems = [];
    this.getTraceabilityLine(this.currentLevel, this.currentLevelHierarchyId);
  }

  onItemSelect(item: any) {
    this.getTraceabilityLine(this.currentLevel, this.currentLevelHierarchyId);
  }

  onSelectAll(items: any) {
    console.log(items);
    this.selectedItems = items;
    console.log(this.selectedItems);
    this.getTraceabilityLine(this.currentLevel, this.currentLevelHierarchyId);
  }

  onItemDeSelect(items: any) {
    this.getTraceabilityLine(this.currentLevel, this.currentLevelHierarchyId);
  }

  onDeSelectAll(items: any) {
    this.selectedItems = [];
    this.getTraceabilityLine(this.currentLevel, this.currentLevelHierarchyId);
  }

  //
  // groupBy(list, keyGetter) {
  //   const map = new Map();
  //   list.forEach((item) => {
  //     const key = keyGetter(item);
  //     const collection = map.get(key);
  //     if (!collection) {
  //       map.set(key, [item]);
  //     } else {
  //       collection.push(item);
  //     }
  //   });
  //   return map;
  // }

  groupBy(array, f) {
    let groups = {};
    array.forEach(function (o) {
      let group = JSON.stringify(f(o));
      groups[group] = groups[group] || [];
      groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
      return groups[group];
    });
  }

}
