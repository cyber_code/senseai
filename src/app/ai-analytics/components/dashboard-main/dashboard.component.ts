import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import { Sprint } from 'src/app/core/models/sprint.model';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardMainComponent implements OnInit {
  // bar
  subProjectId: any;
  sprintData: any = [0, 0, 0];
  processes: any;
  workflow: any;
  testcases: any;
  teststeps: any;
  openissues: any;
  issuesInSprint: any = 0;
  processesInSprint: any = 0;
  workflowsInSprint: any = 0;
  testcasesInSprint: any = 0;
  sprints: Sprint[];
  selectedSprint: Sprint;
  @ViewChild('cmbSprint') cmbSprint;
  public pieChartLabels = [];

  public pieChartData = {

    data: [
      {
        label: 'Requirements',
        data: [],
        backgroundColor: ['#3e95cd']
      },
      {
        label: 'Workflows',
        data: []
      },
      {
        label: 'Test Cases',
        data: []
      },
      {
        label: 'Open Issues',
        data: []
      }
    ],
    options: {
      tooltips: {
        mode: 'index',
        intersect: false
      },
      responsive: true,
      scales: {
        xAxes: [
          {
            stacked: true
          }
        ],
        yAxes: [
          {
            stacked: true
          }
        ]
      }
    }
  };
  public pieChartType = 'bar';
  // pie
  public radarChartLabels = ['To Do', 'In Progress', 'Done'];
  public radarChartData = [
    {
      data: this.sprintData,
      label: ''
    }
  ];
  public radarChartType = 'doughnut';
  public radarChartTitle = 'Current Sprint';
  public radarChartColors = [{
    backgroundColor: [
      'rgba(206,206,206)',
      'rgba(88,192,231)',
      'rgba(150,201,110)',
  ]
  }]

  constructor(private processManagementService: ProcessManagementService,
    public permissionService: PermissionService) {
    this.subProjectId = JSON.parse(
      localStorage.getItem('RAP_SESSION_KEY')
    ).subProject.id;
    if(this.permissionService.hasPermission("/api/Process/Sprint/GetOpenSprintBySubProject"))
      this.processManagementService
        .getOpenSprintBySubProject(this.subProjectId)
        .subscribe(res => {
          this.sprints = res;
          if (res.length > 0) {
            this.selectedSprint = res[0];
            this.getSprintStatistics();
          }
        });
    if(this.permissionService.hasPermission("/api/Process/Browse/GetProcessesBySubProject"))
      this.processManagementService
        .getProcessesBySubProject(this.subProjectId)
        .subscribe(res => {
          this.processes = res.length;
        });
    if(this.permissionService.hasPermission("/api/Process/Browse/GetAllProcessWorkflowsBySubProject"))
      this.processManagementService
        .getAllProcessWorkflowsBySubProject(this.subProjectId)
        .subscribe(res => {
          this.workflow = res.length;
        });
    if(this.permissionService.hasPermission("/api/Process/Browse/GetAllProcessTestCasesBySubProject"))
      this.processManagementService
        .getAllProcessTestCasesBySubProject(this.subProjectId)
        .subscribe(res => {
          const groups = this.groupBy(res, teststep => teststep.testCaseId);
          this.testcases = groups.size;
          this.teststeps = res.length;
        });
    if(this.permissionService.hasPermission("/api/Process/Browse/GetAllProcessOpenIssuesBySubProject"))
      this.processManagementService
        .getAllProcessOpenIssuesBySubProject(this.subProjectId)
        .subscribe(res => {
          this.openissues = res.length;
        });
    if(this.permissionService.hasPermission("/api/Process/Browse/GetHierarchiesStatistics"))
      this.processManagementService
        .getHierarchiesStatistics(
          this.subProjectId,
          '00000000-0000-0000-0000-000000000000'
        )
        .subscribe(res => {
          let i = 0;
          let labels = [];
          let datasets = {
            backgroundColor: [
              '#b3b6b9',
              'green',
              '#19B5FE',
              '#d41d1d',
              '#D9B611',
              '#aa5d9f',
              '#aa5d9f'
            ],
            data: [
              {
                label: 'Requirements',
                data: []
              },
              {
                label: 'Workflows',
                data: []
              },
              {
                label: 'Test Cases',
                data: []
              },

              {
                label: 'Open Issues',
                data: []
              }
              // {
              //   label: "Test Steps",
              //   data: [],
              //   display: false,
              // }
            ],
            options: {
              onClick: this.chartClick.bind(this),
              tooltips: {
                mode: '',
                intersect: true
              },
              responsive: true,
              scales: {
                xAxes: [
                  {
                    stacked: true
                  }
                ],
                yAxes: [
                  {
                    stacked: true
                  }
                ]
              }
            }
          };
          res.forEach(element => {
            labels[i] = element.title;
            datasets.data[0].data[i] = element.nrProcesses;
            datasets.data[1].data[i] = element.nrWorkflows;
            datasets.data[2].data[i] = element.nrTestCases;
            datasets.data[3].data[i] = element.nrOpenIssues;
            //  datasets.data[4].data[i] = element.nrTestSteps;
            i++;
          });
          this.pieChartLabels = labels;
          this.pieChartData = datasets;
        });
  }

  private getSprintStatistics() {
    if (this.permissionService.hasPermission('/api/Process/Sprint/GetSprintIssues')){
      this.processManagementService
      .getSprintIssues(this.selectedSprint.id)
      .subscribe(resIssue => {
        if (resIssue.length > 0) {
          const groups = this.groupBy(resIssue, issue => issue.status);
          this.radarChartData = [
            {
              data: [
                groups.get(0) === undefined ? 0 : groups.get(0).length,
                groups.get(1) === undefined ? 0 : groups.get(1).length,
                groups.get(2) === undefined ? 0 : groups.get(2).length
              ],
              label: ''
            }
          ];

          const groupByProcess = this.groupBy(
            resIssue,
            issue => issue.processId
          );
          this.processesInSprint = groupByProcess.size;
        } else {
          this.processesInSprint = 0;
          this.radarChartData = [
            {
              data: [0, 0, 0],
              label: ''
            }
          ];
        }
        this.issuesInSprint = resIssue.length;
      });
    }

    if (this.permissionService.hasPermission('/api/Process/Browse/GetSprintIssuesWorkflow')){
      this.processManagementService
      .getSprintIssuesWorkflow(this.selectedSprint.id)
      .subscribe(resWorkflow => {
        this.workflowsInSprint = resWorkflow.length;
      });
    }
    if (this.permissionService.hasPermission('/api/Process/Browse/GetSprintIssuesTestCase')){
      this.processManagementService
      .getSprintIssuesTestCase(this.selectedSprint.id)
      .subscribe(resTestcase => {
        this.testcasesInSprint = resTestcase.length;
      });
    }


  }

  ngOnInit() {}

public seeAll() {
  if(this.permissionService.hasPermission("/api/Process/Browse/GetProcessesBySubProject"))
    this.processManagementService
        .getProcessesBySubProject(this.subProjectId)
        .subscribe(res => {
          this.processes = res.length;
        });
  if(this.permissionService.hasPermission("/api/Process/Browse/GetAllProcessWorkflowsBySubProject"))
      this.processManagementService
      .getAllProcessWorkflowsBySubProject(this.subProjectId)
      .subscribe(res => {
        this.workflow = res.length;
      });
  if(this.permissionService.hasPermission("/api/Process/Browse/GetAllProcessOpenIssuesBySubProject"))
      this.processManagementService
      .getAllProcessOpenIssuesBySubProject(this.subProjectId)
      .subscribe(res => {
        this.openissues = res.length;
      });
  if(this.permissionService.hasPermission("/api/Process/Browse/GetAllProcessTestCasesBySubProject"))
      this.processManagementService
      .getAllProcessTestCasesBySubProject(this.subProjectId)
      .subscribe(res => {
        const groups = this.groupBy(res, teststep => teststep.testCaseId);
        this.testcases = groups.size;
        this.teststeps = res.length;
      });
}

  groupBy = function(list, keyGetter) {
    const map = new Map();
    list.forEach(item => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  };

  sprintChanged(newValue: Sprint = {} as Sprint) {
    this.selectedSprint = newValue;
    this.getSprintStatistics();
  }

  chartClick(event, array) {
    if (array.length > 0) {
      this.processes =
        array[0]._chart.data.datasets[array[0]._datasetIndex].data[
          array[0]._index
        ];
      this.workflow =
        array[1]._chart.data.datasets[array[1]._datasetIndex].data[
          array[1]._index
        ];
      this.testcases =
        array[2]._chart.data.datasets[array[2]._datasetIndex].data[
          array[2]._index
        ];
      this.openissues =
        array[3]._chart.data.datasets[array[3]._datasetIndex].data[
          array[3]._index
        ];
      // this.teststeps =
      //   array[4]._chart.data.datasets[array[4]._datasetIndex].data[
      //     array[4]._index
      //   ];
    }
  }
}
