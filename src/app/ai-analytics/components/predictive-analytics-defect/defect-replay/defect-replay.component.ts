import { Component, OnInit, ViewChild } from '@angular/core';
import { ReportService } from '../../../services/report.service';
import { AlertComponent } from '../../../../core/components/alert/alert.component';
import * as d3 from 'd3';
import { SessionService } from 'src/app/core/services/session.service';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap';
import {Router, ActivatedRoute} from '@angular/router';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-defect-replay',
  templateUrl: './defect-replay.component.html',
  styleUrls: ['./defect-replay.component.css']
})
export class DefectReplayComponent implements OnInit {
  @ViewChild(AlertComponent) alert: AlertComponent;
  filters = {};
  hashId = this.sessionService.getHashId();
  //"491CE38B734EF3854CF3B28FECD4DEA4"; 
  showInitialMessage = false;
  duration = 30;

  severityFilter: Array<Object> = [
    { num: 0, name: 'Low', checked: false },
    { num: 1, name: 'Medium', checked: false },
    { num: 2, name: 'High', checked: false },
    { num: 3, name: 'Immediate', checked: false },
    { num: 4, name: 'No data', checked: false }
  ];

  priorityFilter: Array<Object> = [
    { num: 0, name: 'Low', checked: false },
    { num: 1, name: 'Medium', checked: false },
    { num: 2, name: 'High', checked: false },
    { num: 3, name: 'Immediate', checked: false },
    { num: 4, name: 'No data', checked: false }
  ];
  componentFilter: Array<Object> = [];

  dateFilter: Object = {
    from: '',
    to: ''
  };

  selectedSeverityFilter = this.severityFilter;
  selectedPriorityFilter = this.priorityFilter;
  selectedComponentFilter = this.componentFilter;
  selectedDateFilter = this.dateFilter;
  allDefects = [];
  showPauseAnimation = false;
  showContinueAnimation = false;
  nodesMetadata = {}
  totalNumberOfDefects = {
    'detected': 0,
    're_opened': 0,
    'in_progress': 0,
    'ready_for_release': 0,
    'not_a_defect': 0,
    'duplicated': 0,
    'closed': 0
  };

  minDate = {}
  maxDate = {}

  constructor(
    private reportService: ReportService,
    private sessionService: SessionService,
    private router: Router,
    public permissionService: PermissionService
  ) {
  }

  ngOnInit() {
    this.getAllComponents();
    this.getAvailableDateRange()
  }

  applyFilters() {
    const filters = {
      'severity': [],
      'priority': [],
      'components': [],
      'fromDate': '',
      'toDate': ''
    };


    for (let i = 0; i < this.selectedSeverityFilter.length; i++) {
      if (this.selectedSeverityFilter[i]['checked']) {
        filters.severity.push(this.selectedSeverityFilter[i]['name']);
      }
    }

    for (let i = 0; i < this.selectedPriorityFilter.length; i++) {
      if (this.selectedPriorityFilter[i]['checked']) {
        filters.priority.push(this.selectedPriorityFilter[i]['name']);
      }
    }

    for (let i = 0; i < this.selectedComponentFilter.length; i++) {
      if (this.selectedComponentFilter[i]['checked']) {
        filters.components.push(this.selectedComponentFilter[i]['name']);
      }
    }

    if (this.selectedDateFilter['from']['year']) {
      filters.fromDate = this.selectedDateFilter['from'];
      
    }

    if (this.selectedDateFilter['to']['year']) {
      filters.toDate = this.selectedDateFilter['to'];
    }

    if(this.selectedDateFilter['from']['year'] && this.selectedDateFilter['to']['year']){
      const fromDateTmp: NgbDate = new NgbDate(filters.fromDate['year'], filters.fromDate['month'], filters.fromDate['day']);
      const toDateTmp: NgbDate = new NgbDate(filters.toDate['year'], filters.toDate['month'], filters.toDate['day']);
      if(fromDateTmp.before(toDateTmp)){
        this.getDefectsForReplay(filters);
      }else{
        // this.alert.displayAlert('Alert!', 'Please select start date that is before the end date', 'info');
        alert("Please select start date that is before the end date")
      }
    }else{
      this.getDefectsForReplay(filters);
    }
    
  }

  pauseAnimation() {
    var svgDoc = document.getElementsByTagName('svg');
    svgDoc[0].pauseAnimations();
    this.showPauseAnimation = false;
    this.showContinueAnimation = true;

  }

  continueAnimation() {
    var svgDoc = document.getElementsByTagName('svg');
    svgDoc[0].unpauseAnimations();
    this.showPauseAnimation = true;
    this.showContinueAnimation = false;
  }

  getAllComponents() {
    if (this.permissionService.hasPermission("/getAllComponents/hashId/")){
      this.reportService.getAllComponents(this.hashId.toString()).subscribe((results: any) => {
        if (results) {
          for (let i = 0; i < results.length; i++) {
            this.componentFilter.push({ num: i, name: results[i], checked: false });
          }
        }
      });
    }
  }

  getAvailableDateRange(){
    if (this.permissionService.hasPermission("/getAvailableDateRange/hashId/")){
      this.reportService.getAvailableDateRange(this.hashId.toString()).subscribe((results: any) => {
        if (results) {
          this.minDate = results.minDate
          this.selectedDateFilter['from'] = results.minDate
          this.maxDate = results.maxDate
          this.selectedDateFilter['to'] = results.maxDate
        }
      });
    }

  }

  getDefectsForReplay(filters: any): any {
    if (this.permissionService.hasPermission("/getDefectsForReplay/hashId")){
      this.reportService.getDefectsForReplay(filters, this.hashId, this.duration)
      .subscribe((data: any) => {
        this.restartSVG();
        this.allDefects = data.defects;
        this.totalNumberOfDefects = data.totalDefects;
        this.populateDetails(data.paths);

        for (let i = 0; i < this.allDefects.length; i++) {
          if (this.allDefects[i].stages.length > 0) {
            this.addDefectToSVG(this.allDefects[i]);
          }
        }
        this.showPauseAnimation = true;

      });
    }
  }

  addDefectToSVG(defect: any): any {
    const svg = document.getElementById('defects_replay');
    const circleID = 'circle-' + defect.defectId;
    if (document.getElementById(circleID) == null) {
      const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
      circle.setAttribute('id', circleID);
      circle.setAttribute('class', 'animation_elements');
      circle.setAttribute('cx', '0');
      circle.setAttribute('cy', '0');
      circle.setAttribute('r', '10');
      circle.setAttribute('fill', 'tomato');
      var _this = this;

      for (let i = 0; i < defect.stages.length; i++) {

        const animateMotion = document.createElementNS('http://www.w3.org/2000/svg', 'animateMotion');
        animateMotion.setAttributeNS('http://www.w3.org/1999/xlink', 'href', '#' + circleID);
        animateMotion.setAttribute('dur', defect.stages[i].dur);
        animateMotion.setAttribute('begin', defect.stages[i].begin);
        animateMotion.setAttribute('fill', 'remove');
        animateMotion.setAttribute('repeatCount', '1');
        const mpath = document.createElementNS('http://www.w3.org/2000/svg', 'mpath');
        mpath.setAttributeNS('http://www.w3.org/1999/xlink', 'href', '#' + defect.stages[i].transition);

        animateMotion.appendChild(mpath);

        circle.appendChild(animateMotion);
        var key = "trans" + defect.stages[i].transition + "dur" + defect.stages[i].dur + "begin" + defect.stages[i].begin
        if (key in this.nodesMetadata) {
          this.nodesMetadata[key].push(defect)
        } else {
          this.nodesMetadata[key] = []
          this.nodesMetadata[key].push(defect)
        }
      }
      svg.append(circle);

      const tooltip = d3.select('#svg_container')
        .append('div')
        .style('opacity', 0)
        .style('position', 'fixed')
        .attr('class', 'tooltip')
        // .style('text-align', 'center')
        .style('padding', '8px')
        .style('font', '11px sans-serif')
        .style('background', '#FFFFFF')
        .style('border', '2px solid #73AD21')
        .style('border-radius', '25px');

      d3.select('#' + circleID)
        .on('mouseover', function () {
          _this.addNodeTooltip(circleID, tooltip)
        });
        // .on('mouseout', function () {
        //   _this.removeNodeTooltip(tooltip)
        // });

    }

  }

  restartSVG(): any {
    const svg = document.getElementById('defects_replay');
    const all_circles = document.getElementsByClassName('animation_elements');
    while (all_circles[0]) {
      all_circles[0].parentNode.removeChild(all_circles[0]);
    }

    const all_paths = document.getElementsByClassName('defect_path');
    for (let i = 0; i < all_paths.length; i++) {
      all_paths[i].setAttribute('stroke', '#000000');
    }

    const all_arrows = document.getElementsByClassName('defect_arrow');
    for (let i = 0; i < all_arrows.length; i++) {
      all_arrows[i].setAttribute('fill', '#000000');
    }

    const cloneSvg = svg.cloneNode(true);
    svg.parentNode.replaceChild(cloneSvg, svg);


    this.showPauseAnimation = false;
    this.showContinueAnimation = false;
    this.nodesMetadata = {}
  }

  populateDetails(paths: any): any {


    this.addLinkStylingAndTooltips('readyforrelease_reopened', paths['readyforrelease_reopened']);
    this.addLinkStylingAndTooltips('notadefect_closed', paths['notadefect_closed']);
    this.addLinkStylingAndTooltips('readyforrelease_closed', paths['readyforrelease_closed']);
    this.addLinkStylingAndTooltips('duplicate_closed', paths['duplicate_closed']);
    this.addLinkStylingAndTooltips('inprogress_readyforrelease', paths['inprogress_readyforrelease']);
    this.addLinkStylingAndTooltips('inprogress_notadefect', paths['inprogress_notadefect']);
    this.addLinkStylingAndTooltips('inprogress_duplicate', paths['inprogress_duplicate']);
    this.addLinkStylingAndTooltips('reopened_inprogress', paths['reopened_inprogress']);
    this.addLinkStylingAndTooltips('detected_inprogress', paths['detected_inprogress']);
    this.addLinkStylingAndTooltips('detected_notadefect', paths['detected_notadefect']);
    this.addLinkStylingAndTooltips('detected_duplicate', paths['detected_duplicate']);

  }

  addLinkStylingAndTooltips(pathId, pathData: any): any {
    const element = document.getElementById(pathId);
    const tooltip = d3.select('#svg_container')
      .append('div')
      .style('opacity', 0)
      .style('position', 'fixed')
      .style('text-align', 'center')
      .style('padding', '8px')
      .style('font', '11px sans-serif')
      .style('background', '#FFFFFF')
      .style('border', '2px solid #73AD21')
      .style('border-radius', '25px')
      .style('pointer-events', 'none');

    element.setAttribute('stroke-width', pathData['stroke-width']);
    if (pathData['bottleneck']) {
      element.setAttribute('stroke', '#92c94d');
      const elementArrow = document.getElementById(pathId + '_arrow');
      elementArrow.setAttribute('fill', '#92c94d');
    }

    d3.select('#' + pathId)
      .on('mouseover', function () {
        tooltip.html(
          '<strong>Average Days: </strong>' + pathData['avgDays'] + '</br>' +
          '<strong>Passed Defects: </strong>' + pathData['defects'])
          .style('opacity', 1)
          .style('left', (d3.event.clientX + 10) + 'px')
          .style('top', (d3.event.clientY + 10) + 'px');
      })
      .on('mouseout', function () {
        tooltip.style('opacity', 0);
      });


  }

  addNodeTooltip(circleID, tooltip: any): any {
    var all_animateMotion = document.getElementById(circleID).children
    var currentTime = document.getElementsByTagName('svg')[0].getCurrentTime()
    var tooltipKey = ""
    var defectAnimationTime = 0
    for (var j = 0; j < all_animateMotion.length; j++) {
      var dur = parseInt(all_animateMotion[j].getAttribute("dur").replace("s", ""));
      var begin = parseInt(all_animateMotion[j].getAttribute("begin").replace("s", ""));
      defectAnimationTime += dur
      if (currentTime >= begin && currentTime < (begin + dur)) {
        var trans = all_animateMotion[j].children[0].getAttribute("href").replace("#", "");
        tooltipKey = "trans" + trans + "dur" + dur + "s" + "begin" + begin + "s"
      }
    }
    var htmlTemplate = ""
    for (var k = 0; k < this.nodesMetadata[tooltipKey].length; k++) {
      htmlTemplate += "<i style='cursor: pointer;' class=' closeTooltip fa fa-close pull-right'></i><br>"
      htmlTemplate += "<strong>DefectId: </strong>" + this.nodesMetadata[tooltipKey][k]["defectId"] +"</br>"
      htmlTemplate += "<strong>Name: </strong>" + this.nodesMetadata[tooltipKey][k]["name"] +"</br>"
      htmlTemplate += "<strong>Description: </strong>" + this.nodesMetadata[tooltipKey][k]["description"] +"</br>"
      htmlTemplate += "<strong>Component: </strong>" + this.nodesMetadata[tooltipKey][k]["component"] +"</br>"
      htmlTemplate += "<strong>Priority: </strong>" + this.nodesMetadata[tooltipKey][k]["priority"] +"</br>"
      htmlTemplate += "<strong>Severity: </strong>" + this.nodesMetadata[tooltipKey][k]["severity"] +"</br>"
      htmlTemplate += "<strong>Type: </strong>" + this.nodesMetadata[tooltipKey][k]["type"]+"</br>"
      htmlTemplate += "<strong>Assigned to: </strong>" + this.nodesMetadata[tooltipKey][k]["assignedto"] +"</br> </br>"
      htmlTemplate += "<button class='btn btn-sm btn-block btn-primary' id='viewMoreDefectDetailsButton' data-id='" + this.nodesMetadata[tooltipKey][k]["defectId"] +"'>View More</button>"
      if (k < this.nodesMetadata[tooltipKey].length - 1) {
        htmlTemplate += "<p>-------</p></br>"
      }
    }
    tooltip
      .html(htmlTemplate)
      .style('opacity', 1)
      .style('left', (d3.event.clientX + 10) + 'px')
      .style('top', (d3.event.clientY + 10) + 'px');


      d3.select('#viewMoreDefectDetailsButton').on('click', () => {
        this.goToDetails(parseInt($('#viewMoreDefectDetailsButton').data('id')));
      });

      d3.select('.closeTooltip').on('click', () => {
        tooltip
          .style('opacity', 0);
      });
  }

  removeNodeTooltip(tooltip: any): any {
    tooltip.style('opacity', 0);
  }

  goToDetails (id: any) :any {
    this.router.navigate(['/defects-reports/detail/', id]);
  };




}


