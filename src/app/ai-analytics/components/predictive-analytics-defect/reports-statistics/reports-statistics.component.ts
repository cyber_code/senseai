import {Component, OnInit} from '@angular/core';
import {NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { PermissionService } from 'src/app/core/services/permission.service';


@Component({
  selector: 'app-reports-statistics',
  templateUrl: './reports-statistics.component.html',
  styleUrls: ['./reports-statistics.component.css']
})
export class ReportsStatisticsComponent implements OnInit {
  detectTimestampFilter: Array<Object> = ['Date detected', 'Date re-opened', 'Date assigned', 'Target date', 'Date closed'];
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  today = this.calendar.getToday();
  filters = {};

  // parentMessage :any;
  parentMessage: string;

  constructor(
    private calendar: NgbCalendar,
    public permissionService: PermissionService
    ) {
  }

  ngOnInit() {
    this.filters = {
      detectTimestamp: this.detectTimestampFilter[0],
      fromDate: this.fromDate,
      toDate: this.toDate
    };
    this.parentMessage = JSON.stringify(this.filters);
  }

  public applyFilters() {
    this.parentMessage = JSON.stringify(this.filters);
  }
}
