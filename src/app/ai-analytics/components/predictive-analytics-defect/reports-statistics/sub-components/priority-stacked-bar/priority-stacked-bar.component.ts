import { Component, OnInit, Input } from '@angular/core';
import { ReportService } from '../../../../../services/report.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import * as d3 from 'd3';
import { SessionService } from 'src/app/core/services/session.service';
import { environment } from "../../../../../../../environments/environment";

@Component({
  selector: 'app-priority-stacked-bar',
  templateUrl: './priority-stacked-bar.component.html',
  styleUrls: ['./priority-stacked-bar.component.css']
})
export class PriorityStackedBarComponent implements OnInit {
  hashId = this.sessionService.getHashId();
  data: any;
  filters: any;
  public barChartColors: Array<any> = [
    {
      // first color
      backgroundColor: '#d60000',
      borderColor: '#d60000',
      pointBackgroundColor: '#d60000',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#d60000'
    },
    {
      // second color
      backgroundColor: '#e67e22',
      borderColor: '#e67e22',
      pointBackgroundColor: '#e67e22',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#e67e22'
    },
    {
      // third color
      backgroundColor: '#bdc3c7',
      borderColor: '#bdc3c7',
      pointBackgroundColor: '#bdc3c7',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#bdc3c7'
    }
  ];

  @Input()
  set childMessage(parentMessage: string) {
    this.filters = parentMessage;
    this.createPriorityStackedBarChart(this.filters);
  }

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          stacked: true
        }
      ],
      yAxes: [
        {
          stacked: true
        }
      ]
    },
    plugins: {
      datalabels: {
        display: false
      }
    }
  };

  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];
  nodata = false;

  public barChartData: ChartDataSets[] = [{ data: [], label: '' }];

  constructor(
    private sessionService: SessionService,
    private reportService: ReportService
    ) {}

  ngOnInit() {}

  createPriorityStackedBarChart(filters: any): any {
    this.reportService.countPriorityPerType(filters, this.hashId).subscribe((data: any) => {
      if (data.length > 0) {
        this.nodata = false;
        var groups = d3
          .nest()
          .key(d => d['PRIORITY'])
          .entries(data);
        var types = d3
          .nest()
          .key(d => d['TYPE'])
          .entries(data)
          .map(d => d.key);
        var prefinal = groups.map(g => ({
          data: types.map(type => {
            var filtered = g.values.filter(d => d.TYPE == type);
            return filtered.length > 0 ? filtered[0].COUNT : 0;
          }),
          label: g.key
        }));
        this.barChartData = prefinal;
        this.barChartLabels = types;
      } else {
        this.nodata = true;
      }
    });
  }
  public chartClicked({ event, active }: { event: MouseEvent; active: {}[] }): void {}

  public chartHovered({ event, active }: { event: MouseEvent; active: {}[] }): void {}
}
