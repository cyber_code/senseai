import { Component, OnInit, Input } from '@angular/core';
import { ReportService } from '../../../../../services/report.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label, ChartsModule } from 'ng2-charts';
import * as d3 from "d3"
import 'hammerjs';
import 'chartjs-plugin-zoom';
import { Options } from 'ng5-slider';
import 'chartjs-plugin-zoom';
import { SessionService } from 'src/app/core/services/session.service';
import { environment } from "../../../../../../../environments/environment";

@Component({
  selector: 'app-discovery-ratio-grouped-bar-chart',
  templateUrl: './discovery-ratio-grouped-bar-chart.component.html',
  styleUrls: ['./discovery-ratio-grouped-bar-chart.component.css']
})
export class DiscoveryRatioGroupedBarChartComponent implements OnInit {
  timeToFixBarChartInput: any;
  hashId = this.sessionService.getHashId();
  data: any;
  filters: any;
  minValue: number = 0;
  maxValue: number = 100;
  options: Options = {
    floor: 0,
    ceil: 100,
  };
  range_slider_val: any;
  AllbarChartData: any;
  AllbarChartData_updated: any;


  @Input()
  set childMessage(parentMessage: string) {

    this.filters = parentMessage
    this.createDiscoveryRatioBarChart(this.filters)
  }

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        display: false,
        anchor: 'end',
        align: 'end',
      },
      zoom: {
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        }
      }
    }

  };

  public barChartLabels: Label[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = false;
  public barChartPlugins = [pluginDataLabels];
  public colorLegend =[];
  public barChartData: ChartDataSets[] = [
    { data: [], label: 'Test' },
  ];
  nodata = false;

  constructor(
    private sessionService: SessionService,
    private reportService: ReportService
    ) { }

  ngOnInit() {
  }

  createDiscoveryRatioBarChart(filters: any): any {
    this.reportService.getDiscoveryRatioByArea(filters, this.hashId)
      .subscribe((data: any) => {
        if (data.length > 0){
          this.nodata = false;
          var groups = d3.nest().key(d => d['COMPONENT']).entries(data)
          var types = d3.nest().key(d => d['TYPE']).entries(data).map(d => d.key)
          var prefinal = groups.map(g => ({ data: types.map(type => { var filtered = g.values.filter(d => d.TYPE == type);
            return filtered.length > 0 ? filtered[0].OCCURENCE : 0 }), label: g.key }));
          this.AllbarChartData = data;
          this.barChartData = prefinal;
          this.barChartLabels = types;
        } else {
          this.nodata = true;
        }
      });
      
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {

  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {  }

  onRangeCheck(data): any {
    var groups = d3.nest().key(d => d["COMPONENT"]).entries(data)
    var types = d3.nest().key(d => d["TYPE"]).entries(data).map(d => d.key)
    var prefinal = groups.map(g => ({ data: types.map(type => { var filtered = g.values.filter(d => d.TYPE == type); return filtered.length > 0 ? filtered[0].OCCURENCE : 0 }), label: g.key }))
    this.barChartData = prefinal;
    this.barChartLabels = types;
  }

  onRangeSliderApply_DiscRatio() {
    var minvalue = (this.minValue / 100) * this.AllbarChartData.length;
    var maxvalue = (this.maxValue / 100) * this.AllbarChartData.length;
    this.AllbarChartData_updated = this.AllbarChartData.slice(minvalue,  maxvalue)
    this.onRangeCheck(this.AllbarChartData_updated);
  };

}
