import { Component, OnInit, Input } from '@angular/core';
import { ReportService } from '../../../../../services/report.service';
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label } from 'ng2-charts';
import { SessionService } from 'src/app/core/services/session.service';
import { environment } from "../../../../../../../environments/environment";

@Component({
  selector: 'app-status-pie-chart',
  templateUrl: './status-pie-chart.component.html',
  styleUrls: ['./status-pie-chart.component.css']
})
export class StatusPieChartComponent implements OnInit {
  hashId = this.sessionService.getHashId();
  data: any;
  filters: any;
  public pieChartColors: Array<any> = [
    {
      backgroundColor: [
        '#b3b6b9',
        'green',
        '#19B5FE',
        '#d41d1d',
        '#D9B611',
        '#aa5d9f'
      ]
    }
  ];

  @Input()
  set childMessage(parentMessage: string) {
    this.filters = parentMessage;
    this.createStatusPieChart(this.filters);
  }

  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
        display: false,
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        }
      }
    }
  };

  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'doughnut';
  public pieChartLegend = true;
  nodata = false;
  constructor(
    private sessionService: SessionService,
    private reportService: ReportService
    ) {}

  ngOnInit() {}

  createStatusPieChart(filters: any): any {
    this.reportService
      .countStatus(filters, this.hashId)
      .subscribe((data: any) => {
        if (data.length > 0){
          this.nodata = false;
          this.data = data;
          let t = [];
          let t2 = [];
          for (let i = 0; i < this.data.length; i++) {
            t.push(this.data[i]['STATUS']);
            t2.push(this.data[i]['COUNT']);
          }
          this.pieChartLabels = t;
          this.pieChartData = t2;
        } else {
          this.nodata = true;
        }
      });
  }

  // events
  public chartClicked({
    event,
    active
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {}

  public chartHovered({
    event,
    active
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {}
}
