import { Component, OnInit, Input } from "@angular/core";
import { ReportService } from "../../../../../services/report.service";
import { ChartOptions, ChartType, ChartDataSets } from "chart.js";
import * as pluginDataLabels from "chartjs-plugin-datalabels";
import { Label } from "ng2-charts";
import * as d3 from "d3";
import { SessionService } from "src/app/core/services/session.service";
import { environment } from "../../../../../../../environments/environment";

@Component({
  selector: "app-defects-per-buss-area",
  templateUrl: "./defects-per-buss-area.component.html",
  styleUrls: ["./defects-per-buss-area.component.css"]
})
export class DefectsPerBussAreaComponent implements OnInit {
  businessAreasBarChartInput: any;
  hashId = this.sessionService.getHashId();

  data: any;
  filters: any;
  public defectsBarColor: Array<any> = [
    {
      // first color
      backgroundColor: "#d60000",
      borderColor: "#d60000",
      pointBackgroundColor: "#d60000",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "#d60000"
    }
  ];

  @Input()
  set childMessage(parentMessage: string) {
    this.filters = parentMessage;
    this.createDefectsPerBusinessArea(this.filters);
  }

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        display: false,
        anchor: "end",
        align: "end"
      }
    }
  };

  public barChartLabels: Label[] = [
    "2006",
    "2007",
    "2008",
    "2009",
    "2010",
    "2011",
    "2012"
  ];
  public barChartType: ChartType = "bar";
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [], label: "Series A" },
    { data: [], label: "Series B" }
  ];
  nodata = false;
  constructor(
    private sessionService: SessionService,
    private reportService: ReportService
    ) {}

  ngOnInit() {}

  createDefectsPerBusinessArea(filters: any): any {
    this.reportService
      .getDataForBusinessAreaBarChart(filters, this.hashId)
      .subscribe((data: any) => {
        if (data.length > 0) {
          this.nodata = false;
          var names = [];
          var values = [];
          if (data != null) {
            for (let i = 0; i < data.length; i++) {
              names.push(data[i]["name"]);
              values.push(data[i]["value"]);
            }
            this.barChartLabels = names;
            let valuesForChart: ChartDataSets[] = [
              { data: values, label: "Number of defects" }
            ];
            this.barChartData = valuesForChart;
          }
        } else {
          this.nodata = true;
        }
      });
  }
  public chartClicked({
    event,
    active
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {}

  public chartHovered({
    event,
    active
  }: {
    event: MouseEvent;
    active: {}[];
  }): void {}
}
