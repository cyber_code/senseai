import { Component, OnInit, Input } from '@angular/core';
import * as d3 from 'd3'
import { ReportService } from '../../../../../services/report.service';
import { SessionService } from 'src/app/core/services/session.service';
import { environment } from "../../../../../../../environments/environment";

@Component({
  selector: 'app-root-cause-per-business-area-treemap',
  templateUrl: './root-cause-per-business-area-treemap.component.html',
  styleUrls: ['./root-cause-per-business-area-treemap.component.css']
})
export class RootCausePerBusinessAreaTreemapComponent implements OnInit {
  hashId = this.sessionService.getHashId();
  data: any;
  filters: any;
  nodata = false;
  width: number;

  @Input()
  set childMessage(parentMessage: string) {

    this.filters = parentMessage;

    this.reportService.countTypePerBusinessArea(this.filters, this.hashId)
      .subscribe((receivedData: any) => {
        if (receivedData.children.length > 0) {
          this.nodata = false;
          this.data = receivedData;
          this.createTreemap(this.data); // this.filters
        } else {
          this.nodata = true;
        }
      });
  }

  constructor(
    private sessionService: SessionService,
    private reportService: ReportService
    ) { }

  colorLegend: Array<Object> = [
    { num: 0, name: 'Immediate', color: '#f7486e' },
    { num: 1, name: 'High', color: '#ff924a' },
    { num: 2, name: 'Medium', color: '#ffe146' },
    { num: 3, name: 'Low', color: '#6dbf47' }
  ];

  ngOnInit() {

  }

  filterData(minNumOfDefects: number, maxNumOfDefects: number): any {
    const filteredData = {};
    const children = [];
    filteredData['name'] = this.data['name']
    for (let i = 0; i < this.data['children'].length; i++) {
      const total_def = this.data['children'][i]['total_defects'];
      if (total_def >= minNumOfDefects && total_def <= maxNumOfDefects) {
        children.push(this.data['children'][i]);
      }
    }
    filteredData['children'] = children;
    return filteredData;
  }

  createTreemap(receivedData: any): any {
    const box = document.querySelector('#domainDrillDown_root_cause_treemap');
    var total_no_of_defects_10_perCent = 0.1 * parseInt(receivedData.total_no_of_defects);
    var total_no_of_defects_20_perCent = 0.2 * parseInt(receivedData.total_no_of_defects);
    var total_no_of_defects_30_perCent = 0.3 * parseInt(receivedData.total_no_of_defects);

    var ledgColors = ['#f7486e', '#ff924a', '#ffe146', '#6dbf47', '#f5f65a', '#ec9e50', '#ec5050', '#1f9043']
      , ledgLabels = ['Immediate', 'High', 'Medium', 'Low']

    this.width = box.clientWidth;
    var margin = { top: 0, right: 0, bottom: 0, left: 0 },
      width = this.width, // 640
      height = 400,
      formatNumber = d3.format(',d'),
      transitioning;


    const x = d3.scaleLinear()
      .domain([0, width])
      .range([0, width]);

    const y = d3.scaleLinear()
      .domain([0, height - margin.top - margin.bottom])
      .range([0, height - margin.top - margin.bottom]);

    var fader = function (color) { return d3.interpolateRgb(color, '#fff')(0.2); },
      color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
      format = d3.format(',d');


    var format = d3.format(',d');

    var treemap;

    var svg, grandparent;

    var tip = d3.select('#domainDrillDown').append('div')
      .attr('class', 'tooltip')
      .style('position', 'absolute');

    function updateDrillDown() {
      d3.selectAll('.treemap_root_cause').remove();
      // d3.selectAll('.grandparent').remove();


      svg = d3.select('#domainDrillDown_root_cause_treemap').append('svg')
        .attr('class', 'treemap_root_cause')
        .attr('width', width - margin.left - margin.right)
        .attr('height', height - margin.bottom - margin.top)
        .style('margin-left', -margin.left + 'px')
        .style('margin.right', -margin.right + 'px')
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .style('shape-rendering', 'crispEdges');


      grandparent = d3.select('#root_cause_grandparent')
        .attr('class', 'grandparent')
        .attr('class', 'grandparent')
        .style('border', '2px solid #9E9E9E')
        .style('border-radius', '12px')
        .style('width', '20%')
        .style('margin-top', '10px');

      treemap = d3.treemap()
        //.tile(d3.treemapResquarify)
        .size([width, height])
        .round(false)
        .paddingInner(1);

      var root = d3.hierarchy(receivedData)
        .eachBefore(function (d: any) { d.id = (d.parent ? d.parent.id + '.' : '') + d.data.name; })
        .sum((d: any) => d.value+100) //d.size //d.children ? 0 : 1
        .sort(function (a, b) {
          return b.height - a.height || b.value - a.value;
        });

      initialize(root);
      accumulate(root);
      layout(root);
      treemap(root);
      display(root);

    };



    function initialize(root) {
      root.x = root.y = 0;
      root.x1 = width;
      root.y1 = height;
      root.depth = 0;
    }

    function accumulate(d) {
      return (d._children = d.children)
        ? d.value = d.children.reduce(function (p, v) { return p + accumulate(v); }, 0)
        : d.value;
    }

    function layout(d) {
      if (d._children) {
        d._children.forEach(function (c) {
          c.x0 = d.x0 + c.x0 * d.x1;
          c.y0 = d.y0 + c.y0 * d.y1;
          c.x1 *= d.x1;
          c.y1 *= d.y1;
          c.parent = d;
          layout(c);
        });
      }
    }

    function toolTip(d) {

      if (d.depth == 1) {
        var critical_errors = d.data.children.filter(function (el) {
          return el.name == 'Immediate';
        });

        tip.style('opacity', 1)
          .html('<b>Business area: </b>' + d.data.name + '<br><b>Number of defects: </b>' + d.data.total_defects
            + '<br><b>Number of root causes: </b>' + d.data.children.length)
          .style('left', d.x0 + 60 + 'px')
          .style('top', (d.y0 + 550) + 'px')
          .style('fill', '#eee')
          .style('box-shadow', '0 0 5px #999999')
          .style('color', '#333')
          .style('font-size', '14px')
          .style('display', 'inline-block');
      }
    }

    function toolTipOff(d) {
      if (d.depth == 1) {
        tip.style('opacity', 0)
          .style('display', 'none');
      }
    };


    function display(d) {
      console.log("display aftertransition", d)

      if (d.depth == 1) {
        var total_no_of_selected_defects_10_perCent = 0.1 * parseInt(d.value);
        var total_no_of_selected_defects_20_perCent = 0.2 * parseInt(d.value);
        var total_no_of_selected_defects_30_perCent = 0.3 * parseInt(d.value);
      }

      grandparent
        .datum(d.parent)
        .on('click', transition);


      var g1 = svg.insert('g', '.grandparent')
        .datum(d)
        .attr('class', 'depth');

      var g = g1.selectAll('g')
        .data(d._children)
        .enter().append('g');

      g.filter(function (d) { return d._children; })
        .classed('children', true)
        .on('click', transition);

      g.append('rect')
        .attr('class', 'parent')
        .on('mousemove', function (d) { return toolTip(d) })
        .on('mouseout', function (d) { return toolTipOff(d) })
        .call(rect);


      var t = g.append('text')
        .attr('class', 'ptext')
        .attr('dy', '.75em')

      t.append('tspan')
        .style('font-size', '12px')
        .text(function (d) { return d.data.name; });
      t.append('tspan')
        .attr('dy', '1.0em')
        .style('font-size', '12px')
        .text(function (d) { return formatNumber(d.data.total_defects ? d.data.total_defects : d.data.value); });
      t.call(text);

      // g.selectAll('rect')
      //   .style('fill', function (d) { return color(d.data.name); });
      g.selectAll('rect')
        .style('fill', function (d) {
          if (d.depth == 1) {
            if (d.data.total_defects > total_no_of_defects_10_perCent && d.data.total_defects <= total_no_of_defects_20_perCent) {
              return ledgColors[4];
            }
            else if (d.data.total_defects > total_no_of_defects_20_perCent && d.data.total_defects <= total_no_of_defects_30_perCent) {
              return ledgColors[5];
            }
            else if (d.data.total_defects > total_no_of_defects_30_perCent) {
              return ledgColors[6];
            }
            else {
              return ledgColors[7];
            }
          } else {
            if (d.data.value > total_no_of_selected_defects_10_perCent && d.data.value <= total_no_of_selected_defects_20_perCent) {
              return ledgColors[4];
            }
            else if (d.data.value > total_no_of_selected_defects_20_perCent && d.data.value <= total_no_of_selected_defects_30_perCent) {
              return ledgColors[5];
            }
            else if (d.data.value > total_no_of_selected_defects_30_perCent) {
              return ledgColors[6];
            }
            else {
              return ledgColors[7];
            }
          }
        });


      function transition(d) {
        if (transitioning || !d) return;
        transitioning = true;

        var g2 = display(d),
          t1 = g1.transition().duration(750),
          t2 = g2.transition().duration(750);

        x.domain([d.x0, d.x0 + d.x1]);
        y.domain([d.y0, d.y0 + d.y1]);

        svg.style('shape-rendering', null);

        svg.selectAll('.depth').sort(function (a, b) {
          return a.depth - b.depth;
        });

        g2.selectAll('text').style('fill-opacity', 0);

        t1.selectAll('text').call(text).style('fill-opacity', 0);
        t2.selectAll('text').call(text).style('fill-opacity', 1);
        t1.selectAll('rect').call(rect);
        t2.selectAll('rect').call(rect);

        t1.remove().on('end', function () {
          svg.style('shape-rendering', 'crispEdges');
          transitioning = false;
        });
      }

      return g;
    }

    function text(text) {
      text.selectAll('tspan')
        .attr('x', function (d) { return x(d.x0) + 6; })
      text.attr('x', function (d) { return x(d.x0) + 6; })
        .attr('y', function (d) { return y(d.y0) + 10; })
        .style('opacity', function (d) {
          return this.getComputedTextLength() < x(d.x0 + d.x1) - x(d.x0) ? 1 : 0;
        });
    }

    function text2(text) {
      text.attr('x', function (d) { return x(d.x0 + d.x1) - this.getComputedTextLength() - 6; })
        .attr('y', function (d) { return y(d.y0 + d.y1) - 6; })
        .style('opacity', function (d) { return this.getComputedTextLength() < x(d.x0 + d.x1) - x(d.x0) ? 1 : 0; });
    }

    function rect(rect) {
      rect.attr('x', function (d) { return x(d.x0); })
        .attr('y', function (d) { return y(d.y0); })
        .attr('width', function (d) {
          return x(d.x0 + d.x1) - x(d.x0);
        })
        .attr('height', function (d) {
          return y(d.y0 + d.y1) - y(d.y0);

        });
    }

    function name(d) {
      return d.parent
        ? name(d.parent) + ' / ' + d.data.name + ' (' + formatNumber(d.value) + ')'
        : d.data.name + ' (' + formatNumber(d.value) + ')';
    }

    $(function () {
      updateDrillDown();
    });

  }


}
