import { Component, OnInit } from '@angular/core';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';

@Component({
  selector: 'app-predictive-analytics-defect',
  templateUrl: './predictive-analytics-defect.component.html',
  styleUrls: ['./predictive-analytics-defect.component.css']
})
export class PredictiveAnalyticsDefectComponent implements OnInit {

  public license = Licenses;
  
  constructor(public licenseService: LicenseService) { }

  ngOnInit() {}

}
