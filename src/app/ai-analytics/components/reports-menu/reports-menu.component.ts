import {Component, OnInit} from '@angular/core';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';

@Component({
  selector: 'app-ai-analitics-menu',
  templateUrl: './reports-menu.component.html',
  styleUrls: ['./reports-menu.component.css']
})
export class AiAnaliticsMenuComponent implements OnInit {

  public license = Licenses;

  constructor(public licenseService: LicenseService) {
  }

  ngOnInit() {}

}
