import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-sprint',
  templateUrl: './search-sprint.component.html',
  styleUrls: ['./search-sprint.component.css']
})
export class SearchSprintComponent implements OnInit {

    @Input()
    public allSprints;

    @Output()
    public sprintsFiltered = new EventEmitter();
    
    constructor() { }

    ngOnInit() {}

    searchSprint(event) {
        let search = event.target.value;
        let sprints = this.allSprints.filter(sp => sp.start !== '0001-01-01T00:00:00' && 
        sp.end === '0001-01-01T00:00:00' && sp.title.toLowerCase().includes(search.toLowerCase()));
        this.sprintsFiltered.emit(sprints);
    }

}
