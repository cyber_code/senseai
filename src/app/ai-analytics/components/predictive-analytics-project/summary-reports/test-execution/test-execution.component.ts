import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-test-execution',
  templateUrl: './test-execution.component.html',
  styleUrls: ['./test-execution.component.css']
})
export class TestExecutionComponent implements OnInit {

    @Output() setSubProjectToFilter = new EventEmitter();
    public radarChartLabels = [];
    public _testExecutionResults = [];
    public radarChartColors = [{
      backgroundColor: [
        'rgb(109, 191, 71)'
      ],
    }, {
      backgroundColor: [
        'rgb(247, 72, 110)'
      ],
    },
    {
      backgroundColor: [ 
        'rgb(255, 146, 74)'
      ],
    }];

    public radarChartData = {
  
      data: [{
        label: 'Passed Count',
        data: []
      },
      {
        label: 'Failed Count',
        data: []
      },
      {
        label: 'Blocked Count',
        data: []
      }],
      options: {
        onClick: this.chartClick.bind(this),
        tooltips: {
          mode: 'index',
          intersect: false
        },
        responsive: true,
        scales: {
          xAxes: [
            {
              stacked: true
            }
          ],
          yAxes: [
            {
              stacked: true
            }
          ]
        }
      }
    };
    
    public radarChartType = 'bar';

    @Input()
    set testExecutionResults(testExecutionResults) {
      if (testExecutionResults && testExecutionResults.length > 0) {
        this._testExecutionResults = testExecutionResults;
        let labels = [];
        let passedData = [];
        let failedData = [];
        let blockedData = [];
        let color = [{
          backgroundColor: [],
        }, {
          backgroundColor: [],
        },
        {
          backgroundColor: [],
        }]

        for (let i = 0; i < testExecutionResults.length; i++) {
          labels.push(testExecutionResults[i].SubProjectTitle);
          passedData.push(testExecutionResults[i].PassedCount);
          failedData.push(testExecutionResults[i].FailedCount);
          blockedData.push(testExecutionResults[i].BlockedCount);
          color[0].backgroundColor.push('rgb(109, 191, 71)');
          color[1].backgroundColor.push('rgb(247, 72, 110)');
          color[2].backgroundColor.push('rgb(255, 146, 74)');
        }
        this.radarChartLabels = labels;
        this.radarChartColors = color;

        this.radarChartData = {
  
          data: [{
            label: 'Passed Count',
            data: passedData,
          },
          {
            label: 'Failed Count',
            data: failedData
          },
          {
            label: 'Blocked Count',
            data: blockedData
          }],
          options: {
            onClick: this.chartClick.bind(this),
            tooltips: {
              mode: 'index',
              intersect: false
            },
            responsive: true,
            scales: {
              xAxes: [
                {
                  stacked: true
                }
              ],
              yAxes: [
                {
                  stacked: true
                }
              ]
            }
          }
        };
      }
    }

    constructor() { 
    }

    ngOnInit() {}

    chartClick(event, array) {
      if (array && array.length > 0) {
        let index = array[0]._index;
        let subProject = {Id: this._testExecutionResults[index].SubProjectId, Title:  this._testExecutionResults[index].SubProjectTitle};
        this.setSubProjectToFilter.emit(subProject);
      }
    }
}
