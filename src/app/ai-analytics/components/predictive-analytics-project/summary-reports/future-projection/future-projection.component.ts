import { Component, OnInit, Input } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import * as Chart from 'chart.js';

@Component({
  selector: 'app-future-projection',
  templateUrl: './future-projection.component.html',
  styleUrls: ['./future-projection.component.css']
})
export class FutureProjectionComponent implements OnInit {

    public currentSprint;

    public pieChartColors: Array<any> = [
        {
            backgroundColor: [
                '#0798ca',
                'rgba(206,206,206)'
            ]
        },
        {
            backgroundColor: [
                '#0798ca',
                'rgba(206,206,206)'
            ]
        }
    ];
    public pieChartLabels = ['Today', 'Projection'];
    public ratioChartData = [];
    public passChartData = [];
    public executedChartData = [];
    public outstandingChartData = [];
    public pieChartOptions: ChartOptions = {
        cutoutPercentage: 40,
        responsive: true,
        legend: {
            display: false
        },
        plugins: {
            datalabels: {
            display: false,
                formatter: (value, ctx) => {
                    const label = ctx.chart.data.labels[ctx.dataIndex];
                    return label;
                }
            },
        }
    };
    public pieChartType: ChartType = 'doughnut';

    @Input()
    public _currentSprint;

    @Input()
    public set projectionResults(projectionResults) {
        projectionResults = (projectionResults && projectionResults.project) ? projectionResults : {project: [], subProject: []};
        let projectRatioData = projectionResults.project.find(res => res.Metric === 'RatioTrend');
        let subProjectRatioData = projectionResults.subProject ? 
        projectionResults.subProject.find(res => res.Metric === 'RatioTrend') : null;
        this.setProjectionRatioTrend(projectRatioData, subProjectRatioData);
        let projectPassData = projectionResults.project.find(res => res.Metric === 'PassTrend');
        let subProjectPassData = projectionResults.subProject ? 
        projectionResults.subProject.find(res => res.Metric === 'PassTrend') : null;
        this.setProjectionPassTrend(projectPassData, subProjectPassData);
        let projectExecutedCount = projectionResults.project.find(res => res.Metric === 'ExecutedCount');
        let subProjectExecutedCount = projectionResults.subProject ? 
        projectionResults.subProject.find(res => res.Metric === 'ExecutedCount') : null;
        this.setProjectionExecutedCount(projectExecutedCount, subProjectExecutedCount);
        let projectOutstandingDefects = projectionResults.project.find(res => res.Metric === 'OutstandingDefects');
        let subProjectOutstandingDefects = projectionResults.subProject ? 
        projectionResults.subProject.find(res => res.Metric === 'OutstandingDefects') : null;
        this.setProjectionOutstandingDefects(projectOutstandingDefects, subProjectOutstandingDefects);
    }

    public ratioTrend;
    public passTrend;
    public executedCountTrend;
    public outstandingDefectsTrend;
    
    constructor() { }

    ngOnInit() {}

    setProjectionRatioTrend(projectRatioData, subProjectRatioData) {
        if (projectRatioData) {
            this.ratioChartData = subProjectRatioData ?
            [
                {
                    data: [Number(subProjectRatioData.Today.split('%')[0]), Number(subProjectRatioData.Projection.split('%')[0])],
                    label: ''
                },
                {
                    data: [Number(projectRatioData.Today.split('%')[0]), Number(projectRatioData.Projection.split('%')[0])],
                    label: ''
                }
            ] :
            [
                {
                    data: [Number(projectRatioData.Today.split('%')[0]), Number(projectRatioData.Projection.split('%')[0])],
                    label: ''
                },
                {
                    data: [],
                    label: ''
                }
            ];
            this.ratioTrend = subProjectRatioData ? subProjectRatioData.Trend : projectRatioData.Trend;
        } else {
            this.ratioChartData = [
                {
                    data: [],
                    label: ''
                },{
                    data: [],
                    label: ''
                }
            ];
            this.ratioTrend = '0%';
        }
        
    }

    setProjectionPassTrend(projectPassData, subProjectPassData) {
        if (projectPassData) {
            this.passChartData = subProjectPassData ?
            [
                {
                    data: [Number(subProjectPassData.Today.split('%')[0]), Number(subProjectPassData.Projection.split('%')[0])],
                    label: ''
                },
                {
                    data: [Number(projectPassData.Today.split('%')[0]), Number(projectPassData.Projection.split('%')[0])],
                    label: ''
                }
            ] :
            [
                {
                    data: [Number(projectPassData.Today.split('%')[0]), Number(projectPassData.Projection.split('%')[0])],
                    label: ''
                },
                {
                    data: [],
                    label: ''
                }
            ] ;
            this.passTrend = subProjectPassData ? subProjectPassData.Trend : projectPassData.Trend;
        } else {
            this.passChartData = [
                {
                    data: [],
                    label: ''
                },
                {
                    data: [],
                    label: ''
                }
            ];
            this.passTrend = '0%';
        }
        
    }

    setProjectionExecutedCount(projectExecutedCount, subProjectExecutedCount) {
        if (projectExecutedCount) {
            this.executedChartData = subProjectExecutedCount ? 
            [
                {
                    data: [Number(subProjectExecutedCount.Today), Number(subProjectExecutedCount.Projection)],
                    label: ''
                },
                {
                    data: [Number(projectExecutedCount.Today), Number(projectExecutedCount.Projection)],
                    label: ''
                }
            ] :
            [
                {
                    data: [Number(projectExecutedCount.Today), Number(projectExecutedCount.Projection)],
                    label: ''
                },
                {
                    data: [],
                    label: ''
                }
            ];
            this.executedCountTrend = subProjectExecutedCount ? subProjectExecutedCount.Trend : projectExecutedCount.Trend;
        } else {
            this.executedChartData = [
                {
                    data: [],
                    label: ''
                },
                {
                    data: [],
                    label: ''
                }
            ];
            this.executedCountTrend = 0;
        }
        
    }

    setProjectionOutstandingDefects(projectOutstandingDefects, subProjectOutstandingDefects) {
        if (projectOutstandingDefects) {
            this.outstandingChartData = subProjectOutstandingDefects ? 
            [
                {
                    data: [Number(subProjectOutstandingDefects.Today), Number(subProjectOutstandingDefects.Projection)],
                    label: ''
                },
                {
                    data: [Number(projectOutstandingDefects.Today), Number(projectOutstandingDefects.Projection)],
                    label: ''
                }
            ] :
            [
                {
                    data: [Number(projectOutstandingDefects.Today), Number(projectOutstandingDefects.Projection)],
                    label: ''
                },
                {
                    data: [],
                    label: ''
                }
            ];
            this.outstandingDefectsTrend = subProjectOutstandingDefects ? subProjectOutstandingDefects.Trend : projectOutstandingDefects.Trend;
        } else {
            this.outstandingChartData = [
                {
                    data: [],
                    label: ''
                },
                {
                    data: [],
                    label: ''
                }
            ];
            this.outstandingDefectsTrend = 0;
        }
    }
}
