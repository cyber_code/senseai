import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { PassedSprintsComponent } from '../passed-sprints/passed-sprints.component';

@Component({
  selector: 'app-compare-sprints',
  templateUrl: './compare-sprints.component.html',
  styleUrls: ['./compare-sprints.component.css']
})
export class CompareSprintsComponent implements OnInit {

    @ViewChild(PassedSprintsComponent)
    public passedSprintsComponent: PassedSprintsComponent;

    @Input()
    public passedSprints;
    public currentSprint;
    public passedSprint;

    public pieChartColors: Array<any> = [
        {
            backgroundColor: [
                'rgba(150,201,110)',
                '#0798ca',
            ]
        }, {
            backgroundColor: [
                'rgba(150,201,110)',
                '#0798ca',
            ]
        }
    ];
    public pieChartLabels = [];
    public ratioChartData = [];
    public passChartData = [];
    public executedChartData = [];
    public outstandingChartData = [];
    public pieChartOptions: ChartOptions = {
        cutoutPercentage: 40,
        responsive: true,
        legend: {
            display: false
        },
        plugins: {
            datalabels: {
                display: false
            }
            
        }
    };
    public pieChartType: ChartType = 'doughnut';

    @Input() 
    set _currentSprint(_currentSprint) {
        if (_currentSprint) {
            this.pieChartLabels[1] = _currentSprint.title;
        } else {
            this.pieChartLabels[1] = 'Yesterday';
        }
        this.currentSprint = _currentSprint;
    };

    @Input() 
    set _passedSprint(_passedSprint) {
        if (_passedSprint) {
            this.pieChartLabels[0] = _passedSprint.title;
        } else {
            this.pieChartLabels[0] = 'Today';
        }
        this.passedSprint = _passedSprint;
    }

    public currentRatio;
    public pastRatio;
    public ratioTrend;
    public currentPassTrend;
    public pastPassTrend;
    public passTrend;
    public currentExecutedCount;
    public pastExecutedCount;
    public executedCountTrend;
    public currentOutstandingDefects;
    public pastOutstandingDefects;
    public outstandingDefectsTrend;

    @Input()
    public set  sprintComparisonResults(sprintComparisonResults) {
        if (this.currentSprint && this.passedSprint) {
            let ratioData = sprintComparisonResults.find(res => res.Metric === 'RatioTrend');
            this.setSprintRatioTrend(ratioData);
            let passData = sprintComparisonResults.find(res => res.Metric === 'PassTrend');
            this.setSprintPassTrend(passData);
            let executedCount = sprintComparisonResults.find(res => res.Metric === 'ExecutedCount');
            this.setSprintExecutedCount(executedCount);
            let outstandingDefects = sprintComparisonResults.find(res => res.Metric === 'OutstandingDefects');
            this.setSprintOutstandingDefects(outstandingDefects);
        }
    }

    @Input()
    public set releaseComparisonResults(releaseComparisonResults) {
        if (!this.currentSprint || !this.passedSprint) {
            releaseComparisonResults = (releaseComparisonResults && releaseComparisonResults.project) ? releaseComparisonResults : {project: [], subProject: []};
            let projectRatioData = releaseComparisonResults.project.find(res => res.Metric === 'RatioTrend');
            let subProjectRatioData = releaseComparisonResults.subProject ? 
                releaseComparisonResults.subProject.find(res => res.Metric === 'RatioTrend') : null;
            this.setReleaseRatioTrend(projectRatioData, subProjectRatioData);
            let projectPassData = releaseComparisonResults.project.find(res => res.Metric === 'PassTrend');
            let subProjectPassData = releaseComparisonResults.subProject ? 
                releaseComparisonResults.subProject.find(res => res.Metric === 'PassTrend') : null;
            this.setReleasePassTrend(projectPassData, subProjectPassData);
            let projectExecutedCount = releaseComparisonResults.project.find(res => res.Metric === 'ExecutedCount');
            let subProjectExecutedCount = releaseComparisonResults.subProject ? 
                releaseComparisonResults.subProject.find(res => res.Metric === 'ExecutedCount') : null;
            this.setReleaseExecutedCount(projectExecutedCount, subProjectExecutedCount);
            let projectOutstandingDefects = releaseComparisonResults.project.find(res => res.Metric === 'OutstandingDefects');
            let subProjectOutstandingDefects = releaseComparisonResults.subProject ? 
                releaseComparisonResults.subProject.find(res => res.Metric === 'OutstandingDefects') : null;
            this.setReleaseOutstandingDefects(projectOutstandingDefects, subProjectOutstandingDefects);
        }
    }
    
    constructor() { }

    ngOnInit() {}

    setReleaseRatioTrend(projectRatioData, subProjectRatioData) {
        if (projectRatioData) {
            this.ratioChartData = subProjectRatioData ?
            [
                {
                    data: [Number(subProjectRatioData.Today.split('%')[0]), Number(subProjectRatioData.Yesterday.split('%')[0])],
                    label: ''
                },
                {
                    data: [Number(projectRatioData.Today.split('%')[0]), Number(projectRatioData.Yesterday.split('%')[0])],
                    label: ''
                }
            ] : 
            [
                {
                    data: [Number(projectRatioData.Today.split('%')[0]), Number(projectRatioData.Yesterday.split('%')[0])],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentRatio = subProjectRatioData ? subProjectRatioData.Yesterday : projectRatioData.Yesterday;
            this.pastRatio = subProjectRatioData ? subProjectRatioData.Today : projectRatioData.Today;
            this.ratioTrend = subProjectRatioData ? subProjectRatioData.Trend : projectRatioData.Trend;
        } else {
            this.ratioChartData = [
                {
                    data: [0, 0],
                    label: ''
                },{
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentRatio = '0%';
            this.pastRatio = '0%';
            this.ratioTrend = '0%';
        }
        
    }

    setReleasePassTrend(projectPassData, subProjectPassData) {
        if (projectPassData) {
            this.passChartData = subProjectPassData ?
            [
                {
                    data: [Number(subProjectPassData.Today.split('%')[0]), Number(subProjectPassData.Yesterday.split('%')[0])],
                    label: ''
                },
                {
                    data: [Number(projectPassData.Today.split('%')[0]), Number(projectPassData.Yesterday.split('%')[0])],
                    label: ''
                }
            ] : 
            [
                {
                    data: [Number(projectPassData.Today.split('%')[0]), Number(projectPassData.Yesterday.split('%')[0])],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentPassTrend = subProjectPassData ? subProjectPassData.Yesterday : projectPassData.Yesterday;
            this.pastPassTrend = subProjectPassData ? subProjectPassData.Today : projectPassData.Yesterday;
            this.passTrend = subProjectPassData ? subProjectPassData.Trend : projectPassData.Trend;
        } else {
            this.passChartData = [
                {
                    data: [0, 0],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentPassTrend = '0%';
            this.pastPassTrend = '0%';
            this.passTrend = '0%';
        }
        
    }

    setReleaseExecutedCount(projectExecutedCount, subProjectExecutedCount) {
        if (projectExecutedCount) {
            this.executedChartData = subProjectExecutedCount ?
            [
                {
                    data: [Number(subProjectExecutedCount.Today), Number(subProjectExecutedCount.Yesterday)],
                    label: ''
                },
                {
                    data: [Number(projectExecutedCount.Today), Number(projectExecutedCount.Yesterday)],
                    label: ''
                }
            ] :
            [
                {
                    data: [Number(projectExecutedCount.Today), Number(projectExecutedCount.Yesterday)],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentExecutedCount = subProjectExecutedCount ? subProjectExecutedCount.Yesterday : projectExecutedCount.Yesterday;
            this.pastExecutedCount = subProjectExecutedCount ? subProjectExecutedCount.Today : projectExecutedCount.Today;
            this.executedCountTrend = subProjectExecutedCount ? subProjectExecutedCount.Trend : projectExecutedCount.Trend;
        } else {
            this.executedChartData = [
                {
                    data: [0, 0],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentExecutedCount = 0;
            this.pastExecutedCount = 0;
            this.executedCountTrend = 0;
        }
        
    }

    setReleaseOutstandingDefects(projectOutstandingDefects, subProjectOutstandingDefects) {
        if (projectOutstandingDefects) {
            this.outstandingChartData = subProjectOutstandingDefects ? 
            [
                {
                    data: [Number(subProjectOutstandingDefects.Today), Number(subProjectOutstandingDefects.Yesterday)],
                    label: ''
                },
                {
                    data: [Number(projectOutstandingDefects.Today), Number(projectOutstandingDefects.Yesterday)],
                    label: ''
                }
            ] :
            [
                {
                    data: [Number(projectOutstandingDefects.Today), Number(projectOutstandingDefects.Yesterday)],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentOutstandingDefects = subProjectOutstandingDefects ? subProjectOutstandingDefects.Yesterday : projectOutstandingDefects.Yesterday;
            this.pastOutstandingDefects = subProjectOutstandingDefects ? subProjectOutstandingDefects.Today : projectOutstandingDefects.Today;
            this.outstandingDefectsTrend = subProjectOutstandingDefects ? subProjectOutstandingDefects.Trend : projectOutstandingDefects.Trend;
        } else {
            this.outstandingChartData = [
                {
                    data: [0, 0],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentOutstandingDefects = 0;
            this.pastOutstandingDefects = 0;
            this.outstandingDefectsTrend = 0;
        }
    }

    setSprintRatioTrend(ratioData) {
        if (ratioData) {
            this.ratioChartData = [
                {
                    data: [Number(ratioData.PastSprint.split('%')[0]), Number(ratioData.CurrentSprint.split('%')[0])],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentRatio = ratioData.CurrentSprint;
            this.pastRatio = ratioData.PastSprint;
            this.ratioTrend = ratioData.Trend;
        } else {
            this.ratioChartData = [
                {
                    data: [0, 0],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentRatio = '0%';
            this.pastRatio = '0%';
            this.ratioTrend = '0%';
        }
        
    }

    setSprintPassTrend(passData) {
        if (passData) {
            this.passChartData = [
                {
                    data: [Number(passData.PastSprint.split('%')[0]), Number(passData.CurrentSprint.split('%')[0])],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentPassTrend = passData.CurrentSprint;
            this.pastPassTrend = passData.PastSprint;
            this.passTrend = passData.Trend;
        } else {
            this.passChartData = [
                {
                    data: [0, 0],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentPassTrend = '0%';
            this.pastPassTrend = '0%';
            this.passTrend = '0%';
        }
        
    }

    setSprintExecutedCount(executedCount) {
        if (executedCount) {
            this.executedChartData = [
                {
                    data: [Number(executedCount.PastSprint), Number(executedCount.CurrentSprint)],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentExecutedCount = executedCount.CurrentSprint;
            this.pastExecutedCount = executedCount.PastSprint;
            this.executedCountTrend = executedCount.Trend;
        } else {
            this.executedChartData = [
                {
                    data: [0, 0],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentExecutedCount = 0;
            this.pastExecutedCount = 0;
            this.executedCountTrend = 0;
        }
        
    }

    setSprintOutstandingDefects(outstandingDefects) {
        if (outstandingDefects) {
            this.outstandingChartData = [
                {
                    data: [Number(outstandingDefects.PastSprint), Number(outstandingDefects.CurrentSprint)],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentOutstandingDefects = outstandingDefects.CurrentSprint;
            this.pastOutstandingDefects = outstandingDefects.PastSprint;
            this.outstandingDefectsTrend = outstandingDefects.Trend;
        } else {
            this.outstandingChartData = [
                {
                    data: [0, 0],
                    label: ''
                },
                {
                    data: [0, 0],
                    label: ''
                }
            ];
            this.currentOutstandingDefects = 0;
            this.pastOutstandingDefects = 0;
            this.outstandingDefectsTrend = 0;
        }
    }

}
