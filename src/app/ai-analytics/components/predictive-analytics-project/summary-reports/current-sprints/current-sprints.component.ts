import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-current-sprints',
  templateUrl: './current-sprints.component.html',
  styleUrls: ['./current-sprints.component.css']
})
export class CurrentSprintsComponent implements OnInit {

    @Input()
    public sprints;

    public selectedSprint;

    @Output()
    public setCurrentSprint = new EventEmitter();

    constructor() { }

    ngOnInit() {}

    onSprintClick(sprint) {
        this.selectedSprint = sprint;
        this.setCurrentSprint.emit(sprint)
    }

}
