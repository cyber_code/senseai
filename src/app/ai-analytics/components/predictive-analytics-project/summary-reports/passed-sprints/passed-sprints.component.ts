import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-passed-sprints',
  templateUrl: './passed-sprints.component.html',
  styleUrls: ['./passed-sprints.component.css']
})
export class PassedSprintsComponent implements OnInit {
    
    public selectedSprint;
    public passedSprints;

    @Input()
    public set _passedSprints(_passedSprints) {
        this.passedSprints = _passedSprints;
    };

    @Output()
    public setPassedSprint = new EventEmitter();
    
    constructor() { }

    ngOnInit() {}

    format(sprintDate) {
        let date = new Date(sprintDate)
        let month = date.getUTCMonth() + 1;
        let day = date.getUTCDate();
        let year = date.getUTCFullYear();
    
        return day + "/" + month + "/" + year;
    }

    onClickSprint(sprint) {
        this.selectedSprint = sprint;
        this.setPassedSprint.emit(sprint);
    }

}
