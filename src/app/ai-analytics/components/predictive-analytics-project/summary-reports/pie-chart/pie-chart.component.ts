import { Component, OnInit, Input } from '@angular/core';
import { ConfigurationService } from 'src/app/core/services/configuration.service';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {

    @Input()
    public datasets;
    
    @Input()
    public labels;

    @Input()
    public chartType;

    @Input()
    public options;

    @Input()
    public colors;

    @Input()
    public currentSprint;

    @Input()
    public passedSprint;

    @Input()
    public currentSprintPercentage;

    @Input()
    public passedSprintPercentage;

    @Input()
    public trend;

    @Input() 
    public trendPercentage;

    @Input()
    public showSprintsPercentage;

    constructor(public configuration: ConfigurationService) {}

    ngOnInit() {}

    returnColor(trend, trendPercentage) {
      if (trend === 'Execution Trend' || trend === 'Pass Trend') {
        let trendValue = trendPercentage ? Number(trendPercentage.split('%')[0]) : 0;
        if (trendValue < this.configuration.serverSettings.negativeTrend) {
          return 'red';
        } else if (trendValue > this.configuration.serverSettings.positiveTrend) {
          return 'green';
        } else {
          return 'orange';
        }
      }
    }

}
