import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ReleaseSummaryReportService } from 'src/app/reports/services/release-summary-report.service';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent implements OnInit {

    public selectedProject;
    public selectedSubproject;
    public selectedProjectId = '';
    public selectedSubProjectId = '';
    public subProjects = [];

    @Input() 
    public projects;

    @Output()
    public changedSubProject = new EventEmitter();

    @Output()
    public changedProject = new EventEmitter();

    constructor(
        private releaseSummaryReportService: ReleaseSummaryReportService
    ) { }

    ngOnInit() {}

    onChangeSelectedProject(event) {
        let projectId = event.target.value;
        this.selectedProjectId = projectId;
        this.selectedProject = this.projects.find(project => project.Id === projectId);
        this.changedProject.emit(this.selectedProject);
        this.releaseSummaryReportService.getSubprojectId(projectId).subscribe(res => {
            if (res) {
                this.subProjects = [{Id: '', Title: ''}, ...res.SubProjects];
                this.selectedSubProjectId = '';
                this.selectedSubproject = null;
                this.changedSubProject.emit(null);
            }
        });
    }

    onChangeSelectedSubProject(event) {
        let subProjectId = event.target.value;
        this.selectedSubProjectId = subProjectId;
        let selectedSubproject = this.subProjects.find(subProject => subProject.Id === subProjectId);
        this.selectedSubproject = selectedSubproject;
        this.changedSubProject.emit(selectedSubproject);
    }

}
