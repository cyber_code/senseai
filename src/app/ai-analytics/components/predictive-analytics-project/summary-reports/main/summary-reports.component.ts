import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { isNullOrUndefined } from 'util';
import { FiltersComponent } from '../filters/filters.component';
import { PassedSprintsComponent } from '../passed-sprints/passed-sprints.component';
import { CurrentSprintsComponent } from '../current-sprints/current-sprints.component';
import { CompareSprintsComponent } from '../compare-sprints/compare-sprints.component';
import { PermissionService } from 'src/app/core/services/permission.service';
import { ReleaseSummaryReportService } from 'src/app/ai-analytics/services/release-summary-report.service';

@Component({
  selector: 'app-summary-reports',
  templateUrl: './summary-reports.component.html',
  styleUrls: ['./summary-reports.component.css']
})
export class SummaryReportsComponent implements OnInit {

  public projects = [];
  public selectedSubProject;
  public selectedProject;

  public allSprints = [];
  public sprints = [];
  public passedSprints = [];
  public sprintYears = [];
  public selectedYear;
  public currentSprint;
  public passedSprint;
  public sprintComparisonResults = [];
  public releaseComparisonResults = null;
  public projectionResults = null;
  public testExecutionResults = [];

  public predictionSelection = [
    { value: 'next_week', name: 'Next week' },
    { value: 'next_month', name: 'Next month' },
    { value: 'next_semester', name: 'Next semester' }
  ];

  public selectedPredictionSelection = this.predictionSelection[0].value;

  public currentSprintToFilter;
  public passedSprintToFilter;
  public subProjectToFilter;
  public projectToFilter;

  @ViewChild(FiltersComponent)
  private filtersComponent: FiltersComponent;

  @ViewChild(CompareSprintsComponent)
  private compareSprintsComponent: CompareSprintsComponent;

  @ViewChild(CurrentSprintsComponent)
  private currentSprintsComponent: CurrentSprintsComponent;


  constructor(
    private releaseSummaryReportService: ReleaseSummaryReportService,
    public permissionService: PermissionService
  ) { }

  ngOnInit() {
    if (this.permissionService.hasPermission("/getTestExecutionBySubProject/projectId/"))
      this.releaseSummaryReportService.getProjectId().subscribe(res => {
        if (res) {
          this.projects = res.Projects;
        }
      });
  }

  onChangeSubProject(subProject) {
    this.selectedSubProject = subProject;
    if (this.currentSprintsComponent)
      this.currentSprintsComponent.onSprintClick(null);
    if (this.compareSprintsComponent && this.compareSprintsComponent.passedSprintsComponent)
      this.compareSprintsComponent.passedSprintsComponent.onClickSprint(null);

    if (isNullOrUndefined(subProject) || subProject.Id === '') {
      this.allSprints = [];

      this.sprints = [];
      this.setPassedSprints();
      return;
    }

    this.releaseSummaryReportService.getSprints(subProject.Id).subscribe((res: any) => {
      if (res && res.result) {
        this.allSprints = res.result;
        this.sprints = res.result.filter(sprint => sprint.start !== '0001-01-01T00:00:00' && sprint.end === '0001-01-01T00:00:00');
        this.setPassedSprints();
      } else {
        this.allSprints = [];
        this.sprints = [];
        this.setPassedSprints();
      }
    });
  }

  onChangeProject(project) {
    this.selectedProject = project;
  }

  setPassedSprints() {
    let passedSprints = this.allSprints.filter(sprint => sprint.start !== '0001-01-01T00:00:00' && sprint.end !== '0001-01-01T00:00:00');
    let years = [];
    passedSprints.map(sprint => {
      if (!years.find(year => year === new Date(sprint.start).getUTCFullYear())) {
        years.push(new Date(sprint.start).getUTCFullYear());
      }
      if (!years.find(year => year === new Date(sprint.end).getUTCFullYear())) {
        years.push(new Date(sprint.end).getUTCFullYear());
      }
    });
    this.sprintYears = years;
    this.selectedYear = null;
    this.passedSprints = passedSprints;
  }

  setFilteredSprints(sprints) {
    this.sprints = sprints;
  }

  setCurrentSprint(currentSprint) {

    if (!currentSprint) {
      this.currentSprint = null;
      return;
    }

    if (!this.currentSprint || this.currentSprint.id !== currentSprint.id) {
      this.currentSprint = currentSprint;
    }
  }

  applyFilters() {
    this.currentSprintToFilter = this.currentSprint ? Object.assign({}, this.currentSprint) : null;
    this.passedSprintToFilter = this.passedSprint ? Object.assign({}, this.passedSprint) : null;
    this.subProjectToFilter = this.selectedSubProject ? Object.assign({}, this.selectedSubProject) : null;
    this.projectToFilter = this.selectedProject ? Object.assign({}, this.selectedProject) : null;

    if (this.currentSprint && this.passedSprint) {
      this.setSprintComparison();
    } else {
      this.setReleaseComparison();
    }
    
    this.setProjection();
    this.setExecution();
  }

  setExecution() {
    if (this.projectToFilter) {
      this.releaseSummaryReportService
        .getTestExecutionBySubProject(this.projectToFilter.Id)
        .subscribe(res => {
          if (res) {
            this.testExecutionResults = res;
          }
        });
    }
  }

  async setProjection() {
    if (this.projectToFilter && this.selectedPredictionSelection) {
      let projectRes = await this.releaseSummaryReportService.getReleaseSummaryProjection(
        {
          ProjectId: this.projectToFilter.Id,
          ProjectionPeriod: this.selectedPredictionSelection
        }
      ).toPromise();
      let subProjectRes = null;
      if (this.subProjectToFilter && this.subProjectToFilter.Id !== '') {
        subProjectRes = await this.releaseSummaryReportService.getReleaseSummaryProjection(
          {
            SubProjectId: this.subProjectToFilter.Id,
            ProjectId: this.projectToFilter.Id,
            ProjectionPeriod: this.selectedPredictionSelection
          }
        ).toPromise();
      }
      this.projectionResults = {project: projectRes ? projectRes.Results : null, subProject: subProjectRes ? subProjectRes.Results : null};
    }
  }

  setSubProjectToFilter(event) {
    if (event) {
      this.subProjectToFilter = event;
      this.currentSprintToFilter = null;
      this.passedSprintToFilter = null;
      this.setReleaseComparison();
      this.setProjection();
    }
  }

  setSprintComparison() {
    if (this.currentSprintToFilter && this.passedSprintToFilter) {
      this.releaseSummaryReportService.getSprintsComparisonResults(
        {
          SubProjectId: this.subProjectToFilter.Id,
          CurrentSprintId: this.currentSprintToFilter.id,
          PastSprintId: this.passedSprintToFilter.id
        }).subscribe(res => {
          if (res && res.Results) {
            this.sprintComparisonResults = res.Results;
          }
        }); 
    }
  }

  async setReleaseComparison() {
    if (this.projectToFilter) {
      let projectRes = await this.releaseSummaryReportService.getReleaseSummaryComparisonResult(this.projectToFilter.Id, null).toPromise();
      let subProjectRes = null;
      if (this.subProjectToFilter && this.subProjectToFilter.Id !== '') {
        subProjectRes = await this.releaseSummaryReportService.getReleaseSummaryComparisonResult(this.projectToFilter.Id, this.subProjectToFilter.Id).toPromise();
      }
      this.releaseComparisonResults = {project: projectRes ? projectRes.Results : null, subProject: subProjectRes ? subProjectRes.Results : null};
    } 
  }

  onChangeYear() {
    this.passedSprints = this.allSprints.filter(sprint => 
      sprint.start !== '0001-01-01T00:00:00' && 
      sprint.end !== '0001-01-01T00:00:00' && 
      (new Date(sprint.start).getUTCFullYear() === this.selectedYear || 
      new Date(sprint.end).getUTCFullYear() === this.selectedYear)
    );
  }

  onChangePredictionSelection() {
    this.setProjection();
  }

  setPassedSprint(event) {

    if (!event) {
      this.passedSprint = null;
      return;
    }

    if (!this.passedSprint || this.passedSprint.id !== event.id) {
      this.passedSprint = event;
    }
  }
}
