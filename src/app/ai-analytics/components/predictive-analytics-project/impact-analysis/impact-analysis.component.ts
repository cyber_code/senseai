import { Component, OnInit, Injectable, ElementRef, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import Sunburst from 'sunburst-chart';
import { of } from 'rxjs/observable/of';
import { SelectableSettings, CheckedState } from '@progress/kendo-angular-treeview';
import { ImpactAnalysisService } from '../../../services/impact-analysis.service';
import { SessionService } from '../../../../core/services/session.service';
import { TaskAssignmentServiceService } from 'src/app/reports/services/task-assignment-service.service';
import {TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
declare var vis: any;
@Component({
  selector: 'app-impact-analysis',
  templateUrl: './impact-analysis.component.html',
  styleUrls: ['./impact-analysis.component.css']
})
export class ImpactAnalysisComponent implements OnInit {
  @ViewChild("timeline") timelineContainer: ElementRef;
  @ViewChild('tabset') tabset: TabsetComponent;

  hashId = this.sessionService.getHashId();

  ProjectsFilter: any;
  SubprojectsFilter: any;
  SprintFilter1:any;
  SprintFilter2:any;
  width: number;
  selectedProject: any;
  selectedSubproject:any;
  selectedSprint1:any;
  selectedSprint2:any;
  treeview_data_display = false;
  optimization_clicked = false;
  selectedColorPerDeveloper = {};
  selectedColorPerQA = {};
  postData = {};

  public SubprojectsData = false;
  public SprintsData = false;
  public openCompareStatusWindow = false;
  public currentStatus;
  public datas:any;
  public totalWorklfows : number =0;
  public totalTestCases: number =0;



  public checkedKeys: any[] = [];
  public expandedKeys: any[] = [];
  public selectedKeys: any[] = [];

  public selection: SelectableSettings = { mode: 'multiple' };
  public disabledKeys: any[] = [];
  public treeView_data: any[] = [];

  public hasChildren = (item: any) => item.items && item.items.length > 0;
  public fetchChildren = (item: any) => of(item.items);

  colorLegendDeveloper: Array<Object> = [
    { num: 0, name: 'Immediate', color: '#f7486e' },
    { num: 1, name: 'High', color: '#ff924a' },
    { num: 2, name: 'Medium', color: '#ffe146' },
    { num: 3, name: 'Low', color: '#6dbf47' }
  ];

  colorLegendQA: Array<Object> = [
    { num: 0, name: 'Immediate', color: '#f7486e' },
    { num: 1, name: 'High', color: '#ff924a' },
    { num: 2, name: 'Medium', color: '#ffe146' },
    { num: 3, name: 'Low', color: '#6dbf47' }
  ];

  data: any;
  groups: any;
  timeline: any;
  showOnlyUncommitedTasks: boolean = false;

  colorPer: Array<Object> = [
    { num: 1, name: 'Priority' },
    { num: 2, name: 'Severity' },
  ];

  task_color: any;
  colorList: any = {
    Immediate: 'red',
    High: 'orange',
    Medium: 'yellow',
    Moderate: 'yellowgreen',
    Low: 'green',

  };

  constructor(
    private ImpactAnalysisService: ImpactAnalysisService,
    private taskAssignmentServiceService: TaskAssignmentServiceService,
    private sessionService: SessionService
  ) {
    this.data = new vis.DataSet();
    this.groups = new vis.DataSet();
  }

  ngOnInit() {
    this.colorLegendDeveloper = [
      { num: 0, name: 'Immediate', color: '#f7486e' },
      { num: 1, name: 'High', color: '#ff924a' },
      { num: 2, name: 'Medium', color: '#ffe146' },
      { num: 3, name: 'Low', color: '#6dbf47' }
    ];

    this.colorLegendQA = [
      { num: 0, name: 'Immediate', color: '#f7486e' },
      { num: 1, name: 'High', color: '#ff924a' },
      { num: 2, name: 'Medium', color: '#ffe146' },
      { num: 3, name: 'Low', color: '#6dbf47' }
    ];


    this.ImpactAnalysisService.getProjectId().subscribe((data: any) => {
      if (data.Projects.length > 0) {
        this.ProjectsFilter = data.Projects;
      } else {
        alert("No projects available");
      }
    });

  }

  onChangeColorPeronChangeColorPerDeveloper(newObj) {
    this.colorLegendDeveloper = [];
    if (this.selectedColorPerDeveloper['name'] === 'Severity') {
      this.colorLegendDeveloper = [
        { num: 0, name: 'Immediate', color: '#f7486e' },
        { num: 1, name: 'High', color: '#ff924a' },
        { num: 2, name: 'Medium', color: '#ffe146' },
        { num: 2, name: 'Moderate', color: '#C9DF79' },
        { num: 3, name: 'Low', color: '#6dbf47' }
      ];
    } else if (this.selectedColorPerDeveloper['name'] === 'Priority') {
      this.colorLegendDeveloper = [
        { num: 0, name: 'Immediate', color: '#f7486e' },
        { num: 1, name: 'High', color: '#ff924a' },
        { num: 2, name: 'Medium', color: '#ffe146' },
        { num: 3, name: 'Low', color: '#6dbf47' }
      ];
    }
    this.getTimelineDataDeveloper();
  }

  onChangeColorPeronChangeColorPerQA(newObj) {
    this.colorLegendQA = [];
    if (this.selectedColorPerQA['name'] === 'Severity') {
      this.colorLegendQA = [
        { num: 0, name: 'Immediate', color: '#f7486e' },
        { num: 1, name: 'High', color: '#ff924a' },
        { num: 2, name: 'Medium', color: '#ffe146' },
        { num: 2, name: 'Moderate', color: '#C9DF79' },
        { num: 3, name: 'Low', color: '#6dbf47' }
      ];
    } else if (this.selectedColorPerQA['name'] === 'Priority') {
      this.colorLegendQA = [
        { num: 0, name: 'Immediate', color: '#f7486e' },
        { num: 1, name: 'High', color: '#ff924a' },
        { num: 2, name: 'Medium', color: '#ffe146' },
        { num: 3, name: 'Low', color: '#6dbf47' }
      ];
    }
    this.getTimelineDataQA();
  }


  onChangeprojectRangeFilter(ownprojectRangeFilter: number) {
    this.selectedProject = ownprojectRangeFilter;
    this.treeview_data_display = false;
    this.optimization_clicked = false;
    this.ImpactAnalysisService.getsubprojectId(this.selectedProject).subscribe((data: any) => {
      if (data.SubProjects.length > 0) {
        this.SubprojectsData = true;
        this.SubprojectsFilter = data.SubProjects;
      } else {
        this.SubprojectsData = false;
        alert("No Subprojects available");
      }
    });
  }

  onChangesubprojectRangeFilter(ownsubprojectRangeFilter: number) {
    this.treeview_data_display = false;
    this.optimization_clicked = false;
    this.selectedSubproject = ownsubprojectRangeFilter;
    this.ImpactAnalysisService.getsprintsId(this.selectedSubproject).subscribe((data: any) => {
      if (data.Sprints.length > 0) {
        this.SprintsData = true;
        this.SprintFilter1 = data.Sprints;
        this.SprintFilter2 = data.Sprints;
      } else {
        this.SprintsData = false;
        alert("No Sprints available");
      }
    });
  }

  onChangeSprintFilter(){
    this.treeview_data_display = false;
    this.optimization_clicked = false;
  }

  viewHierarchy(selectedSprint1:string, selectedSprint2: string) {
    this.treeview_data_display = false;
    this.optimization_clicked = false;
    this.ImpactAnalysisService.impactFilteringUI(this.selectedSubproject, selectedSprint1, selectedSprint2).subscribe((data: any) => {

      if (data.status !== "fail") {
        this.treeview_data_display = true;
        this.treeView_data = data.items ? data.items : [];
      } else {
        this.treeview_data_display = false;
        alert("No data available");
      }
    });
  }


  getCheckedItems() {
    let allItems = [];
    for(let i = 0; i < this.checkedKeys.length; i++) {
      let key = this.checkedKeys[i];
      let indexes = key.split('_');
      let item = this.treeView_data[indexes[0]];
      for(let i = 1; i < indexes.length; i++) {
        item = item.items[indexes[i]];
      }
      allItems = allItems.concat(this.getAllChildren(item, allItems));
    }
    return allItems;
  }

  createTreemap(receivedData: any): any {

    const box = document.querySelector('#domainDrillDown_root_cause_treemap');
    this.width = box.clientWidth || 900;
    var margin = { top: 20, right: 0, bottom: 0, left: 0 },
      width = this.width, // 640
      height = 400,
      formatNumber = d3.format(',d'),
      transitioning;


    const x = d3.scaleLinear()
      .domain([0, width])
      .range([0, width]);

    const y = d3.scaleLinear()
      .domain([0, height - margin.top - margin.bottom])
      .range([0, height - margin.top - margin.bottom]);

    var fader = function (color) { return d3.interpolateRgb(color, '#fff')(0.2); },
      color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
      format = d3.format(',d');


    var format = d3.format(',d');

    var treemap;

    var svg, grandparent;

    var tip = d3.select('#domainDrillDown').append('div')
      .attr('class', 'tooltip')
      .style('position', 'absolute');

    function updateDrillDown() {
      d3.selectAll('.treemap_root_cause').remove();
      // d3.selectAll('.grandparent').remove();
      svg = d3.select('#domainDrillDown_root_cause_treemap').append('svg')
        .attr('class', 'treemap_root_cause')
        .attr('width', width - margin.left - margin.right)
        .attr('height', height - margin.bottom - margin.top)
        .style('margin-left', -margin.left + 'px')
        .style('margin.right', -margin.right + 'px')
        .append('g')
        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
        .style('shape-rendering', 'crispEdges');


      grandparent = d3.select('#root_cause_grandparent')
        .attr('class', 'grandparent')
        .attr('class', 'grandparent')
        .style('border', '2px solid #9E9E9E')
        .style('border-radius', '12px')
        .style('width', '20%')
        .style('margin-top', '10px');
      treemap = d3.treemap()
        //.tile(d3.treemapResquarify)
        .size([width, height])
        .round(false)
        .paddingInner(1);
      var root = d3.hierarchy(receivedData)
        .eachBefore((d: any) =>{
          return d.id = (d.parent ? d.parent.id + '.' : '') + d.data.name;
         })
        .sum((d: any) => {
          return 1;
        }) //d.size
        .sort(function (a, b) {
          return b.height - a.height || b.value - a.value;
        });
      initialize(root);
      accumulate(root);
      layout(root);
      treemap(root);
      display(root);

    };



    function initialize(root) {
      root.x = root.y = 0;
      root.x1 = width;
      root.y1 = height;
      root.depth = 0;
    }

    function accumulate(d) {
      return (d._children = d.children)
        ? d.value = d.children.reduce(function (p, v) { return p + accumulate(v); }, 0)
        : d.value;
    }

    function layout(d) {
      if (d._children) {
        d._children.forEach(function (c) {
          c.x0 = d.x0 + c.x0 * d.x1;
          c.y0 = d.y0 + c.y0 * d.y1;
          c.x1 *= d.x1;
          c.y1 *= d.y1;
          c.parent = d;
          layout(c);
        });
      }
    }

    function toolTip(d) {
      tip.style('opacity', 1)
          .html('<b>Name: </b>' + d.data.name)
          .style('left', d.x0 + 60 + 'px')
          .style('top', (d.y0 + 750) + 'px')
          .style('fill', '#eee')
          .style('box-shadow', '0 0 5px #999999')
          .style('color', '#333')
          .style('font-size', '14px')
          .style('display', 'inline-block');

    }

    function toolTipOff(d) {
      tip.style('opacity', 0)
          .style('display', 'none');
    };

    function display(d) {

      grandparent
        .datum(d.parent)
        .on('click', transition);


      var g1 = svg.insert('g', '.grandparent')
        .datum(d)
        .attr('class', 'depth');

      var g = g1.selectAll('g')
        .data(d._children)
        .enter().append('g');

      g.filter(function (d) { return d._children; })
        .classed('children', true)
        .on('click', transition);

      g.append('rect')
        .attr('class', 'parent')
        .on('mousemove', function (d) { return toolTip(d) })
        .on('mouseout', function (d) { return toolTipOff(d) })
        .call(rect);


      var t = g.append('text')
        .attr('class', 'ptext')
        .attr('dy', '.75em')

      t.append('tspan')
        .text(function (d) { 
          return d.data.name.toString().length > 30 ? d.data.name.toString().substring(0,30) : d.data.name; });
  
      t.call(text);

      g.selectAll('rect')
        .style('fill', function (d) { 
          return color(d.data.name); });

      function transition(d) {
        if (transitioning || !d) return;
        transitioning = true;

        var g2 = display(d),
          t1 = g1.transition().duration(750),
          t2 = g2.transition().duration(750);

        x.domain([d.x0, d.x0 + d.x1]);
        y.domain([d.y0, d.y0 + d.y1]);

        svg.style('shape-rendering', null);

        svg.selectAll('.depth').sort(function (a, b) {
          return a.depth - b.depth;
        });

        g2.selectAll('text').style('fill-opacity', 0);

        t1.selectAll('text').call(text).style('fill-opacity', 0);
        t2.selectAll('text').call(text).style('fill-opacity', 1);
        t1.selectAll('rect').call(rect);
        t2.selectAll('rect').call(rect);

        t1.remove().on('end', function () {
          svg.style('shape-rendering', 'crispEdges');
          transitioning = false;
        });
      }

      return g;
    }

    function text(text) {
      text.selectAll('tspan')
        .attr('x', function (d) { return x(d.x0) + 6; })
      text.attr('x', function (d) { return x(d.x0) + 6; })
        .attr('y', function (d) { return y(d.y0) + 10; })
        .style('opacity', function (d) {
          return this.getComputedTextLength() < x(d.x0 + d.x1) - x(d.x0) ? 1 : 0;
        });
    }

    function text2(text) {
      text.attr('x', function (d) { return x(d.x0 + d.x1) - this.getComputedTextLength() - 6; })
        .attr('y', function (d) { return y(d.y0 + d.y1) - 6; })
        .style('opacity', function (d) { return this.getComputedTextLength() < x(d.x0 + d.x1) - x(d.x0) ? 1 : 0; });
    }

    function rect(rect) {
      rect.attr('x', function (d) { return x(d.x0); })
        .attr('y', function (d) { return y(d.y0); })
        .attr('width', function (d) {
          return x(d.x0 + d.x1) - x(d.x0);
        })
        .attr('height', function (d) {
          return y(d.y0 + d.y1) - y(d.y0);
        });
    }

    function name(d) {
      return d.parent
        ? name(d.parent) + ' / ' + d.data.name + ' (' + formatNumber(d.value) + ')'
        : d.data.name + ' (' + formatNumber(d.value) + ')';
    }

    $(function () {
      updateDrillDown();
    });

  }

  getAllChildren(item, elements) {
    if (!item.items) {
      if (!elements.find(element => element.id === item.id)) {
        return [item];
      }
      return [];
    }
    let allItems = [];
    if (!elements.find(element => element.id === item.id)) {
      allItems = [item];
    } else {
      allItems = [];
    }
    item.items.map(el => {
      allItems.push(...this.getAllChildren(el, elements));
    });
    return allItems;
  }

  applyFilters(){
    let totalTestCases = 0;
    let totalWorklfows = 0;
    let checkedItems = this.getCheckedItems();
    let processItems = checkedItems.filter(item => item.type === 'Process');
    this.optimization_clicked = true;
    this.postData = {
      Processes: processItems.map(item => item.id)
    }

    this.ImpactAnalysisService.allDatasunburstUI(this.selectedSubproject, this.postData).subscribe((resp: any) => {
      this.currentStatus = resp;
      const color = d3.scaleOrdinal(d3.schemeCategory20);
      const myDOMElement = document.getElementById('myDOMElement');
      const treemapElement = document.getElementById('domainDrillDown_root_cause_treemap');
      myDOMElement.innerHTML = '';
      treemapElement.innerHTML = '';
      if (resp && resp.name) {
        const myChart = Sunburst();
        myChart
          .data(resp)
          .excludeRoot(true)
          .height(400)
          .width(450)
          .size(20)
          .tooltipTitle((item) => item.name)
          .color((item) => color(item.name))
          (myDOMElement);
          this.createTreemap(resp);
          d3.hierarchy(resp)
            .eachBefore((d: any) => {
              if (d.data.workflowId)
                totalWorklfows = totalWorklfows +1;
              if (d.data.testcaseId)
                totalTestCases = totalTestCases +1;
            })
          this.totalTestCases = totalTestCases;
          this.totalWorklfows = totalWorklfows;
      } else {
        this.totalTestCases = 0;
        this.totalWorklfows = 0;
      }
    });
  }
  
  getTimelineDataDeveloper() {
    // Create a DataSet (allows two way data-binding)
    // create items
    this.data.clear();
    this.taskAssignmentServiceService
      .getTaskSchedule(this.hashId)
      .subscribe((data: any) => {
        if(data.status != "fail"){
          for (var j = 0; j < data.Jobs.length; j++) {
            if(this.showOnlyUncommitedTasks === true && data.Jobs[j].Commited)
              continue;

            if (this.selectedColorPerDeveloper['name'] === 'Priority') {
              this.task_color = this.colorList[data.Jobs[j].PriorityLevel];
            } else {
              this.task_color = this.colorList[data.Jobs[j].SeverityLevel];
            }

            this.data.update({
              id: parseInt(data.Jobs[j].DefectID),
              group: parseInt(data.Jobs[j].DeveloperID),
              start: data.Jobs[j].StartTime,
              end: data.Jobs[j].EndTime,
              content: data.Jobs[j].Name,
              editable: { updateTime: false, updateGroup: false, remove: false },
              className: this.task_color + (data.Jobs[j].Commited === true ? " commited" : " uncommited"),
              title: "<b>Defect ID</b>: " + data.Jobs[j].DefectID + "<br> <b>Name: </b>" + data.Jobs[j].Name + "<br> <b>Severity Level: </b>" + data.Jobs[j].SeverityLevel + "<br> <b>Priority Level: </b>" + data.Jobs[j].PriorityLevel + "<br> <b>Start Date: </b>" + data.Jobs[j].StartTime
            });
          }
          this.timeline.setData({
            data: data
          });
        }
      });
  }


  getTimelineDataQA() {
    // Create a DataSet (allows two way data-binding)
    // create items
    this.data.clear();
    this.taskAssignmentServiceService
      .getTaskSchedule(this.hashId)
      .subscribe((data: any) => {
        if(data.status != "fail"){
          for (var j = 0; j < data.Jobs.length; j++) {
            if(this.showOnlyUncommitedTasks === true && data.Jobs[j].Commited)
              continue;

            if (this.selectedColorPerQA['name'] === 'Priority') {
              this.task_color = this.colorList[data.Jobs[j].PriorityLevel];
            } else {
              this.task_color = this.colorList[data.Jobs[j].SeverityLevel];
            }

            this.data.update({
              id: parseInt(data.Jobs[j].DefectID),
              group: parseInt(data.Jobs[j].DeveloperID),
              start: data.Jobs[j].StartTime,
              end: data.Jobs[j].EndTime,
              content: data.Jobs[j].Name,
              editable: { updateTime: false, updateGroup: false, remove: false },
              className: this.task_color + (data.Jobs[j].Commited === true ? " commited" : " uncommited"),
              title: "<b>Defect ID</b>: " + data.Jobs[j].DefectID + "<br> <b>Name: </b>" + data.Jobs[j].Name + "<br> <b>Severity Level: </b>" + data.Jobs[j].SeverityLevel + "<br> <b>Priority Level: </b>" + data.Jobs[j].PriorityLevel + "<br> <b>Start Date: </b>" + data.Jobs[j].StartTime
            });
          }
          this.timeline.setData({
            data: data
          });
        }
      });
  }

  onSelect(data: TabDirective): void {
    if(data.heading === "Smart Capacity Planner"){
      this.getTimelineDataQA();
      this.getTimelineDataDeveloper();
    }
  }

  compare() {
    this.openCompareStatusWindow = true;
  }

  closeCompareStatusWindow() {
    this.openCompareStatusWindow = false;
    let navbar = document.getElementById('navbar') as any;
      if (navbar)
        navbar.style.zIndex = 1;
  }

  clearInputs(){
    this.selectedSubproject = ""
    this.selectedProject = ""
    this.selectedSprint1 = ""
    this.selectedSprint2 = ""
  }
}
