import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ImpactAnalysisService } from 'src/app/ai-analytics/services/impact-analysis.service';
import Sunburst from 'sunburst-chart';
import * as d3 from 'd3';

@Component({
  selector: 'app-compare-states',
  templateUrl: './compare-states.component.html',
  styleUrls: ['./compare-states.component.css']
})
export class CompareStatesComponent implements OnInit {
  public _openCompareStatusWindow = false;
  public planedCurrentStatus;
  public predictedImpact;
  public currentStatusWorkflowCount = 0;
  public predictedStatusWorkflowCount = 0;

  @Input() selectedSubproject;
  @Input() postData;
  @Input()
  set openCompareStatusWindow(openCompareStatusWindow) {
    this._openCompareStatusWindow = openCompareStatusWindow;
    if (openCompareStatusWindow) {
      let navbar = document.getElementById('navbar') as any;
      if (navbar)
        navbar.style.zIndex = 0;
      this.impactAnalysisService.allDatasunburstUI(this.selectedSubproject, this.postData).subscribe((resp: any) => {
        this.planedCurrentStatus = resp;
        const color = d3.scaleOrdinal(d3.schemeCategory20);
        const plannedCurrentStatusEl = document.getElementById('planned-current-status');
        plannedCurrentStatusEl.innerHTML = '';
        if (resp && resp.name) {
          let workflows = this.getWorkflows(resp);
          this.currentStatusWorkflowCount = workflows.length;
          const myChart = Sunburst();
          myChart
            .data(resp)
            .excludeRoot(true)
            .height(350)
            .width(400)
            .size(20)
            .tooltipTitle((item) => item.name)
            .color((item) => color(item.name))
            (plannedCurrentStatusEl);
        }
        
      });
      this.impactAnalysisService.allDatasunburstAffectedUI(this.selectedSubproject, this.postData).subscribe((resp: any) => {
        this.predictedImpact = resp;
        const color = d3.scaleOrdinal(d3.schemeCategory20);
        const predictedImpactEl = document.getElementById('predicted-impact');
        predictedImpactEl.innerHTML = '';
        if (resp && resp.name) {
          let workflows = this.getWorkflows(resp);
          this.predictedStatusWorkflowCount = workflows.length;
          const myChart = Sunburst();
          myChart
            .data(resp)
            .excludeRoot(true)
            .height(350)
            .width(400)
            .size(20)
            .tooltipTitle((item) => item.name)
            .color((item) => color(item.name))
            (predictedImpactEl);
        } 
      });
    } 
  }

  @Output()
  public closeCompareStatusWindow = new EventEmitter();
  
  constructor(private impactAnalysisService: ImpactAnalysisService) { }

  ngOnInit() {
  }

  close() {
    this.predictedImpact = null;
    this.planedCurrentStatus = null;
    this.closeCompareStatusWindow.emit();
  }

  getWorkflows(item) {
    if (!item.children) {
      return item.workflowId ? [item] : [];
    }
    let allItems = item.workflowId ? [item] : [];
    item.children.map(el => {
        allItems.push(...this.getWorkflows(el));
    });
    return allItems;
  }

}
