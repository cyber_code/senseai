import { Component, OnInit } from '@angular/core';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-predictive-analytics-project',
  templateUrl: './predictive-analytics-project.component.html',
  styleUrls: ['./predictive-analytics-project.component.css']
})
export class PredictiveAnalyticsProjectComponent implements OnInit {

  public license = Licenses;

  constructor(public licenseService: LicenseService, private permissionService: PermissionService) { }

  ngOnInit() {}

  hasImpactAnalysisPermission() {
    return (this.licenseService.hasLicense(this.license.Live_Impact_Analysis) &&
    this.permissionService.hasPermission('/LiveImpactAnalysis/')
    );
  }

}
