import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiskOptimizerComponent } from './risk-optimizer.component';

describe('RiskOptimizerComponent', () => {
  let component: RiskOptimizerComponent;
  let fixture: ComponentFixture<RiskOptimizerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiskOptimizerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiskOptimizerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
