import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {ChartOptions, ChartType, ChartDataSets} from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {Label, ChartsModule, SingleDataSet} from 'ng2-charts';
import * as d3 from 'd3';
import 'hammerjs';
import 'chartjs-plugin-zoom';
import {Options} from 'ng5-slider';
import 'chartjs-plugin-zoom';
import {ImpactAnalysisService} from '../../../services/impact-analysis.service';
import {SessionService} from '../../../../core/services/session.service';
import {HttpClient} from '@angular/common/http';
import {DataTableDirective} from 'angular-datatables';
import {LoadingBarService} from '@ngx-loading-bar/core';
import {NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {MatTableDataSource, MatSort} from '@angular/material';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-risk-optimizer',
  templateUrl: './risk-optimizer.component.html',
  styleUrls: ['./risk-optimizer.component.css']
})
export class RiskOptimizerComponent implements OnInit {
  @ViewChild(DataTableDirective)
  @ViewChild(MatSort) sort: MatSort;
  tableArr: Element[] = [];


  datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  table = null;


  hashId = this.sessionService.getHashId();
  subProjectId = this.sessionService.getSubProject();
  display_charts = false;
  display_sev_prioritycharts = false;
  nodata = false;

  test_case_label_old: string;
  test_case_label_new: string;
  defects_label_old: string;
  defects_label_new: string;
  cost_new: string;
  cost_old: string;
  coverage_old: string;
  coverage_new: string;
  datacoverage_old: string;
  datacoverage_new: string;
  totalRisk_old: string;
  totalRisk_new: string;

  ProjectsFilter: any;
  SubprojectsFilter: any;
  SprintFilter: any;
  width: number;
  selectedProject: any;
  selectedSubproject: any;
  selectedSprint: any;
  public SubprojectsData = false;
  public SprintsData = false;
  public Runbtn = false;
  public status = false;

  number_Defects: any;
  number_Cost: any;
  number_Resources: any;
  old_Risk_Data: any;

  colorLegend: Array<Object>;

  public barChartColorsSev_Prior: Array<any> = [
    {
      backgroundColor: [
        '#599CD3',
        '#599CD3',
        '#599CD3',
        '#599CD3',
      ]
    }
  ];

  public barChartColorsDouble: Array<any> = [
    {
      backgroundColor: [
        '#599CD3',
        '#599CD3'
      ]
    }
  ];

  public barChartColorDefects: Array<any> = [
    {
      backgroundColor: [
        '#599CD3',
        '#599CD3'
      ]
    }
  ];


  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [{
        ticks: {
          suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
          beginAtZero: true   // minimum value will be 0.
        }
      }], yAxes: [{}]
    },
    plugins: {
      datalabels: {
        display: false,
        anchor: 'end',
        align: 'end',
      },
      // zoom: {
      //   pan: {
      //     enabled: true,
      //     mode: 'x'
      //   },
      //   zoom: {
      //     enabled: true,
      //     mode: 'x'
      //   }
      // }
    }

  };
  Machine_Budget: number;
  Time_Spent: number;
  Personel_Budget: number;

  public barChartType: ChartType = 'horizontalBar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartSeverityData: ChartDataSets[] = [
    {data: [2, 1, 6, 12], label: 'Project Current Status'},
    {data: [1, 6, 8, 10], label: 'Optimized'}
  ];
  public barChartSeverityLabels: string[] = ['Immediate', 'High', 'Moderate', 'Low'];
  public barChartPriorityData: ChartDataSets[] = [
    {data: [9, 7, 16, 12], label: 'Project Current Status'},
    {data: [2, 8, 4, 10], label: 'Optimized'}
  ];

  public barChartPriorityLabels: string[] = ['Immediate', 'High', 'Moderate', 'Low'];


  public barChartTestCasesLabels: string[] = [''];
  public barChartTestCasesData: ChartDataSets[] = [
    {data: [59], label: 'Project Current Status'},
    {data: [47], label: 'Optimized'}
  ];

  public barChartDefectsLabels: string[] = [''];
  public barChartDefectsData: ChartDataSets[] = [
    {data: [72], label: 'Project Current Status'},
    {data: [39], label: 'Optimized'}
  ];

  public barChartCostLabels: string[] = [''];
  public barChartCostData: ChartDataSets[] = [
    {data: [182], label: 'Project Current Status'},
    {data: [99], label: 'Optimized'}
  ];

  public barChartCoverageLabels: string[] = [''];
  public barChartCoverageData: ChartDataSets[] = [
    {data: [40], label: 'Project Current Status'},
    {data: [51], label: 'Optimized'}
  ];

  public barChartDataCoverageLabels: string[] = [''];
  public barChartDataCoverageData: ChartDataSets[] = [
    {data: [48], label: 'Project Current Status'},
    {data: [59], label: 'Optimized'}
  ];


  AllbarChartData: any;
  AllbarChartData_updated: any;

  startedClass = false;
  completedClass = false;
  preventAbuse = false;

  filters = {};
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  waiting: String;


  // accordeondata: any =
  //   [
  //     {
  //       'parentName': 'Test Cases details',
  //       'childProperties': []
  //     },
  //     {
  //       'parentName': 'Test Cases details',
  //       'childProperties': []
  //     }
  //   ];
  accordeondata: any[] = [];

  TCid: number;
  defects: string;
  execTime: number;
  coverage: string;
  dataCoverage: string;
  Severity: string;
  Prioriyt: string;
  Machcost: string;
  defect_risk: number;
  budget_risk: number;
  severity_risk: number;
  total_risk: number;
  probability: number;
  coverage_risk: string;

  displayedColumns: string[] = ['TCid', 'defects', 'execTime', 'coverage', 'dataCoverage', 'Severity', 'Prioriyt', 'Machcost', 'probability', 'coverage_risk', 'defect_risk', 'budget_risk', 'severity_risk', 'total_risk'];
  dataSource: any[] = [];


  public barChartLabelsDefectsRootCause: Label[] = [];
  public barChartTypeDefectsRootCause: ChartType = 'bar';
  public barChartDataDefectsRootCause: ChartDataSets[] = [
    {data: [], label: '1'},
    {data: [], label: '2'}
  ];

  public barChartRootCauseOptions: ChartOptions = {
    responsive: true,
    scales: {xAxes: [{}], yAxes: [{}]},
    plugins: {
      datalabels: {
        display: false,
        anchor: 'end',
        align: 'end',
      },
      zoom: {
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        }
      }
    }

  };

  public barChartRootCauseLabels: Label[] = [];
  public barChartRootCauseType: ChartType = 'bar';
  public barChartRootCauseLegend = false;
  public barChartRootCausePlugins = [pluginDataLabels];
  public colorRootCauseLegend = [];
  // public barChartRootCauseData: ChartDataSets[] = [];
  public barChartRootCauseData: any[] = [
    {data: [], label: '1'},
    {data: [], label: '2'}
  ];

  // Pie
  public pieChartRiskContributionOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'right',
    },
    plugins: {
      datalabels: {
        display: false,
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        }
      }
    }
  };

  public pieChartRiskContributionLabels: any[] = [];
  public pieChartRiskContributionData: any[] = [];
  public pieChartRiskContributionType: ChartType = 'pie';
  public pieChartRiskContributionLegend = true;
  public pieChartRiskContributionColors: any[] = [{backgroundColor: []}];

  ProductsDistinctList: any[] = [];
  selectedProduct: any;
  total_risk_values: any;
  test_cases_all: any;
  new_data: any;

  constructor(
    private sessionService: SessionService,
    private reportService: ImpactAnalysisService,
    public permissionService: PermissionService,
    private http: HttpClient,
    private loadingBar: LoadingBarService
  ) {
    this.Time_Spent = 1;
    this.Machine_Budget = 500;
    this.Personel_Budget = 5;
    this.number_Resources = 3;

  }

  ngOnInit() {
    this.colorLegend = [
      {num: 0, name: 'Current Project Status', color: '#599CD3'},
      {num: 1, name: 'Predicted', color: '#FFAA22'},
      {num: 2, name: 'Optimized', color: '#93C953'}
    ];
    this.filters = {
      fromDate: this.fromDate,
      toDate: this.toDate
    };
    if (this.permissionService.hasPermission("/RiskOptimizerUI/projectId/"))
        this.reportService.getProjectId().subscribe((data: any) => {
          if (data.Projects.length > 0) {
            this.ProjectsFilter = data.Projects;
          } else {
            alert('No projects available');
          }
        });
  }

  onChangeProductRangeFilter(ownProductRangeFilter: any) {
    const selectedProductValues = JSON.parse(ownProductRangeFilter);
    this.pieChartRiskContributionData = [];
    this.pieChartRiskContributionLabels = [];
    this.pieChartRiskContributionColors = [{backgroundColor: []}];
    selectedProductValues.forEach(pr => {
      // match product with total risk
      for (let k = 0; k < Object.keys(this.total_risk_values).length; k++) {
        if (pr == Object.keys(this.total_risk_values)[k]) {
          this.pieChartRiskContributionData.push(Object.values(this.total_risk_values)[k]);
        }
      }
      const letters = '0123456789ABCDEF';
      let color = '#';
      // match product with test cases
      for (let k = 0; k < Object.keys(this.test_cases_all).length; k++) {
        if (pr == Object.keys(this.test_cases_all)[k]) {
          for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
          }
          this.pieChartRiskContributionColors[0].backgroundColor.push(color);
          this.pieChartRiskContributionLabels.push(Object.values(this.test_cases_all)[k]);
        }
      }

    });
  }


  onChangeprojectRangeFilter(ownprojectRangeFilter: number) {
    this.selectedProject = ownprojectRangeFilter;
    this.Runbtn = false;
    this.display_charts = false;
    this.test_case_label_new = '';
    this.defects_label_new = '';
    this.cost_new = '';
    this.coverage_new = '';
    this.datacoverage_new = '';
    this.SubprojectsData = false;
    this.SprintsData = false;
    this.reportService.getsubprojectId(this.selectedProject).subscribe((data: any) => {
      if (data.SubProjects.length > 0) {
        this.SubprojectsData = true;
        this.SubprojectsFilter = data.SubProjects;
      } else {
        this.SubprojectsData = false;
        alert('No Subprojects available');
      }
    });
  }

  onChangesubprojectRangeFilter(ownsubprojectRangeFilter: number) {
    this.selectedSubproject = ownsubprojectRangeFilter;
    this.Runbtn = false;
    this.display_charts = false;
    this.test_case_label_new = '';
    this.defects_label_new = '';
    this.cost_new = '';
    this.coverage_new = '';
    this.datacoverage_new = '';
    this.SprintsData = false;
    this.reportService.getsprintsId(this.selectedSubproject).subscribe((data: any) => {
      if (data.Sprints.length > 0) {
        this.SprintsData = true;
        this.SprintFilter = data.Sprints;
      } else {
        this.SprintsData = false;
        alert('No Sprints available');
      }
    });
  }

  onChangeSprintRangeFilter(ownsprintRangeFilter: number) {
    this.selectedSprint = ownsprintRangeFilter;
    this.Runbtn = false;
    this.display_charts = false;
    this.test_case_label_new = '';
    this.defects_label_new = '';
    this.cost_new = '';
    this.coverage_new = '';
    this.datacoverage_new = '';
    this.display_sev_prioritycharts = true;
    this.reportService.get_business_optimizer_UI(this.selectedProject, this.selectedSubproject, this.selectedSprint, this.Machine_Budget, this.Time_Spent, this.Personel_Budget, this.number_Resources, 'False').subscribe((data: any) => {
      if (data.status == 'pass') {
        this.status = true;
        this.old_Risk_Data = data;
        this.Runbtn = true;
        this.test_case_label_old = data.TestCasesData;
        this.defects_label_old = data.DefectsData;
        var calcTime = data.Execution_Time / 60;
        this.cost_old = calcTime.toFixed(2);
        this.coverage_old = data.CoverageData.toFixed(2) + '%';
        this.datacoverage_old = data.DataCoverageData.toFixed(2) + '%';
        this.barChartSeverityLabels = data.SeverityLabels;
        this.barChartPriorityLabels = data.PriorityLabels;
        this.barChartColorsSev_Prior = [{
          backgroundColor: [
            '#599CD3',
            '#599CD3',
            '#599CD3',
            '#599CD3',
            '#599CD3'
          ]
        }];
        console.log(data.SeverityData);
        this.barChartSeverityData = [
          {data: data.SeverityData, label: 'Project Current Status'}
        ];
        this.barChartPriorityData = [
          {data: data.PriorityData, label: 'Project Current Status'}
        ];
        this.totalRisk_old = '-';
      } else {
        this.status = false;
        this.Runbtn = false;
        alert('No results available.');
      }
    });
  }

  onStarted() {
    this.startedClass = true;
    setTimeout(() => {
      this.startedClass = false;
    }, 800);
  }

  onCompleted() {
    this.completedClass = true;
    setTimeout(() => {
      this.completedClass = false;
    }, 800);
  }

  runOptmization() {
    this.preventAbuse = true;
    this.display_charts = false;
    this.display_sev_prioritycharts = false;
    this.loadingBar.start();
    this.waiting = 'This may take a while. Please wait.';
    console.log('Machine_Budget', this.Machine_Budget);
    console.log('Time_Spent', this.Time_Spent);

    this.reportService.get_business_optimizer_UI(this.selectedProject, this.selectedSubproject, this.selectedSprint, this.Machine_Budget, this.Time_Spent, this.Personel_Budget, this.number_Resources, 'True').subscribe((new_data: any) => {
      this.loadingBar.complete();
      this.waiting = '';
      console.log(new_data);
      if (new_data.status == 'pass') {
        this.status = true;
        this.test_case_label_old = '/' + new_data.Summary_results[0].All_cases[0].TestCasesData;
        this.test_case_label_new = new_data.Summary_results[0].Optimized_cases[0].TestCasesData;
        this.defects_label_old = '/' + new_data.Summary_results[0].All_cases[0].DefectsData;
        this.defects_label_new = new_data.Summary_results[0].Optimized_cases[0].DefectsData;
        var cost_old_calc = new_data.Summary_results[0].All_cases[0].Execution_Time / 60;
        this.cost_old = '/' + cost_old_calc.toFixed(2);
        var cost_new_calc = new_data.Summary_results[0].Optimized_cases[0].Execution_Time / 60;
        this.cost_new = cost_new_calc.toFixed(2);
        this.coverage_old = '/' + new_data.Summary_results[0].All_cases[0].CoverageData.toFixed(2) + '%';
        this.coverage_new = new_data.Summary_results[0].Optimized_cases[0].CoverageData.toFixed(2);
        this.datacoverage_old = '/' + new_data.Summary_results[0].All_cases[0].DataCoverageData.toFixed(2) + '%';
        this.datacoverage_new = new_data.Summary_results[0].Optimized_cases[0].DataCoverageData.toFixed(2);
        document.getElementById('processing_span').style.display = 'inline';
        this.display_charts = true;
        this.display_sev_prioritycharts = true;
        document.getElementById('processing_span').style.display = 'none';
        this.barChartSeverityLabels = new_data.Summary_results[0].Optimized_cases[0].SeverityLabels;
        this.barChartPriorityLabels = new_data.Summary_results[0].Optimized_cases[0].PriorityLabels;
        console.log(this.barChartSeverityData);

        this.barChartRootCauseLabels = new_data.Roots_labels;
        new_data.Roots_opt.forEach(opt => {
          this.barChartRootCauseData[0].data.push(opt[0].data[0]);
          this.barChartRootCauseData[1].data.push(opt[0].data[1]);
          // this.barChartRootCauseDat a.push(opt[0]);
        });
        this.barChartCostLabels = [];
        this.barChartSeverityData = [
          {data: new_data.Summary_results[0].All_cases[0].SeverityData, label: 'Project Current Status'},
          {data: new_data.Summary_results[0].Optimized_cases[0].SeverityData, label: 'Optimized'}
        ];
        this.total_risk_values = new_data.Big_Table[0].total_risk;
        this.test_cases_all = new_data.Big_Table[0].Testcase;
        this.ProductsDistinctList = [];
        const ProductGrouping = new_data.Big_Table[0].ProductGrouping;
        for (let k = 0; k < Object.keys(ProductGrouping).length; k++) {
          this.ProductsDistinctList.push({
            title: Object.keys(ProductGrouping)[k],
            value: JSON.stringify(Object.values(ProductGrouping)[k])
          });
        }
        // this.selectedProduct = {
        //   title: Object.keys(ProductGrouping)[0],
        //   value: JSON.stringify(Object.values(ProductGrouping)[0])
        // };
        this.new_data = new_data;
        this.accordeondata = [];
        for (let k = 0; k < Object.keys(ProductGrouping).length; k++) {
          this.accordeondata.push({
            parentName: Object.keys(ProductGrouping)[k],
            childProperties: [],
            testCaseKeys: Object.values(ProductGrouping)[k],
            isActive: false
          });
        }

        this.barChartColorsSev_Prior = [{
          backgroundColor: [
            '#599CD3',
            '#599CD3',
            '#599CD3',
            '#599CD3',
            '#599CD3'
          ]
        }, {
          backgroundColor: [
            '#93C953',
            '#93C953',
            '#93C953',
            '#93C953',
            '#93C953'
          ]
        }];

        this.barChartColorsDouble = [{
          backgroundColor: [
            '#599CD3'
          ]
        }, {
          backgroundColor: [
            '#93C953'
          ]
        }];

        this.barChartColorDefects = [{
          backgroundColor: [
            '#599CD3'
          ]
        }, {
          backgroundColor: [
            '#FFAA22'
          ]
        }];

        this.barChartPriorityData = [
          {data: new_data.Summary_results[0].All_cases[0].PriorityData, label: 'Project Current Status'},
          {data: new_data.Summary_results[0].Optimized_cases[0].PriorityData, label: 'Optimized'}
        ];

        this.barChartTestCasesData = [
          {data: [new_data.Summary_results[0].All_cases[0].TestCasesData], label: 'Project Current Status'},
          {data: [new_data.Summary_results[0].Optimized_cases[0].TestCasesData], label: 'Optimized'}
        ];


        this.barChartDefectsData = [
          {data: [new_data.Summary_results[0].All_cases[0].DefectsData], label: 'Project Current Status'},
          {data: [new_data.Summary_results[0].Optimized_cases[0].DefectsData], label: 'Predicted'}
        ];

        this.barChartCostData = [
          {data: [parseInt(cost_old_calc.toFixed(2))], label: 'Project Current Status'},
          {data: [parseInt(cost_new_calc.toFixed(2))], label: 'Optimized'}
        ];

        this.barChartCoverageData = [
          {data: [new_data.Summary_results[0].All_cases[0].CoverageData], label: 'Project Current Status'},
          {data: [new_data.Summary_results[0].Optimized_cases[0].CoverageData], label: 'Optimized'}
        ];
        this.barChartDataCoverageData = [
          {data: [new_data.Summary_results[0].All_cases[0].DataCoverageData], label: 'Project Current Status'},
          {data: [new_data.Summary_results[0].Optimized_cases[0].DataCoverageData], label: 'Optimized'}
        ];
        this.totalRisk_old = '';
        var totalRisk_calc_AVG = 0;
        for (var k = 0; k < Object.keys(new_data.Big_Table[0].total_risk).length; k++) {
          totalRisk_calc_AVG = totalRisk_calc_AVG + new_data.Big_Table[0].total_risk[k];
        }
        var calc_val = totalRisk_calc_AVG / Object.keys(new_data.Big_Table[0].total_risk).length;
        if (calc_val < 25) {
          this.totalRisk_new = 'Low';
        } else if (calc_val >= 25 && calc_val < 50) {
          this.totalRisk_new = 'Medium';
        } else if (calc_val >= 50 && calc_val < 75) {
          this.totalRisk_new = 'High';
        } else if (calc_val >= 75) {
          this.totalRisk_new = 'Critical';
        }
        // this.accordeondata[0].childProperties.push(new_data.Big_Table);
        // for (var k = 0; k < Object.keys(new_data.Big_Table[0].Testcase).length; k++) {
        //   this.tableArr.push({
        //     TCid: new_data.Big_Table[0].Testcase[k],
        //     defects: new_data.Big_Table[0].defects[k],
        //     execTime: new_data.Big_Table[0].time_points[k].toFixed(2),
        //     coverage: new_data.Big_Table[0].coverage[k].toFixed(2),
        //     dataCoverage: new_data.Big_Table[0].data_coverage[k].toFixed(2),
        //     Severity: new_data.Big_Table[0].severity[k],
        //     Prioriyt: new_data.Big_Table[0].priority[k],
        //     Machcost: new_data.Big_Table[0].Machine_Cost[k].toFixed(2),
        //     defect_risk: new_data.Big_Table[0].defect_risk_l[k],
        //     budget_risk: new_data.Big_Table[0].budget_risk_l[k],
        //     severity_risk: new_data.Big_Table[0].severity_risk_l[k],
        //     coverage_risk: new_data.Big_Table[0].coverage_risk_l[k],
        //     total_risk: new_data.Big_Table[0].total_risk_l[k],
        //     probability: new_data.Big_Table[0].probability[k].toFixed(2)
        //   });
        // }
        // this.dataSource = new MatTableDataSource(this.tableArr);
        // this.dataSource.sort = this.sort;

      } else {
        this.Runbtn = false;
        this.status = false;
        alert('No results available.');
      }
    });
    this.reportService.get_defects_by_root_cause(this.selectedProject, this.selectedSubproject, this.selectedSprint).subscribe((data: any) => {
      if (data.status == 'pass') {
        this.nodata = false;
        var groups = d3.nest().key(d => d['COMPONENT']).entries(data);
        var types = d3.nest().key(d => d['TYPE']).entries(data).map(d => d.key);
        var prefinal = groups.map(g => ({
          data: types.map(type => {
            var filtered = g.values.filter(d => d.TYPE == type);
            return filtered.length > 0 ? filtered[0].AVGTIMETOFIX : 0;
          }), label: g.key
        }));
        this.AllbarChartData = data;
        // this.barChartDataDefectsRootCause = prefinal;
        // this.barChartLabelsDefectsRootCause = types;
      } else {
        this.Runbtn = false;
        this.nodata = true;
        this.status = false;
        alert('No defects available.');
      }
    });
  }

  // toggleAccordian(event, index) {
  //   var element = event.target;
  //   element.classList.toggle('active');
  //   console.log(this.accordeondata[0].childProperties);
  //   if (this.accordeondata[index].isActive) {
  //     this.accordeondata[index].isActive = false;
  //   } else {
  //     this.accordeondata[index].isActive = true;
  //   }
  //   var panel = element.nextElementSibling;
  //   if (panel.style.maxHeight) {
  //     panel.style.maxHeight = null;
  //   } else {
  //     panel.style.maxHeight = panel.scrollHeight + 'px';
  //   }
  // }

  toggleAccordian(event, index, testCaseKeys) {
    var element = event.target;
    element.classList.toggle('active');
    this.tableArr = [];
    this.accordeondata[index].childProperties = [];
    this.accordeondata[index].childProperties.push(this.new_data.Big_Table);
    for (var k = 0; k < Object.keys(this.new_data.Big_Table[0].Testcase).length; k++) {
      if (testCaseKeys.includes(Object.keys(this.new_data.Big_Table[0].Testcase)[k])) {
        this.tableArr.push({
          TCid: this.new_data.Big_Table[0].Testcase[k],
          defects: this.new_data.Big_Table[0].defects[k],
          execTime: this.new_data.Big_Table[0].time_points[k].toFixed(2),
          coverage: this.new_data.Big_Table[0].coverage[k].toFixed(2),
          dataCoverage: this.new_data.Big_Table[0].data_coverage[k].toFixed(2),
          Severity: this.new_data.Big_Table[0].severity[k],
          Prioriyt: this.new_data.Big_Table[0].priority[k],
          Machcost: this.new_data.Big_Table[0].Machine_Cost[k].toFixed(2),
          defect_risk: this.new_data.Big_Table[0].defect_risk_l[k],
          budget_risk: this.new_data.Big_Table[0].budget_risk_l[k],
          severity_risk: this.new_data.Big_Table[0].severity_risk_l[k],
          coverage_risk: this.new_data.Big_Table[0].coverage_risk_l[k],
          total_risk: this.new_data.Big_Table[0].total_risk_l[k],
          probability: this.new_data.Big_Table[0].probability[k].toFixed(2)
        });
      }
    }
    this.dataSource[index] = new MatTableDataSource(this.tableArr);
    this.dataSource[index].sort = this.sort;
    setTimeout(() => {
      if (this.accordeondata[index].isActive) {
        this.accordeondata[index].isActive = false;
      } else {
        this.accordeondata[index].isActive = true;
      }
      var panel = element.nextElementSibling;
      if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + 'px';
      }
    }, 1000);
  }
}

export interface Element {
  TCid: number,
  defects: string,
  execTime: number,
  coverage: string,
  dataCoverage: string,
  Severity: string,
  Prioriyt: string,
  Machcost: string,
  defect_risk: number,
  budget_risk: number,
  severity_risk: number,
  total_risk: number,
  probability: number,
  coverage_risk: string
}
