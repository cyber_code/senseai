import { Component, OnInit, Input, ViewChild } from '@angular/core';
import * as d3 from 'd3';
import {ReportService} from '../../../services/report.service';
import {SessionService} from '../../../../core/services/session.service';
import {Report} from '../../../report';
import {Router, ActivatedRoute} from '@angular/router';
import {environment} from '../../../../../environments/environment';
import {
  NgbCalendar,
  NgbDateStruct
} from '@ng-bootstrap/ng-bootstrap';
import {Options} from 'ng5-slider';
import {AlertComponent} from 'src/app/core/components/alert/alert.component';
import { reduce } from 'rxjs/operators';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild(AlertComponent) alert: AlertComponent;
  @Input() report: Report;
  clusteringData = {};
  treemapInput: any;
  hashId = this.sessionService.getHashId();

  graphData: any;
  defectsRangeFilter: Array<Object> = ['Week', 'Month', 'Quarter', 'Semester', 'Year', 'All Time'];
  today = this.calendar.getToday();
  filters = {};
  svg2: any;
  svg: any;
  treemap_filtering: string = ' ';
  treemapInput_per_severity_high: any;
  treemapInput_per_severity_medium: any;
  treemapInput_per_severity_low: any;
  treemapInput_updated: any;
  treemap_data: any;
  colorLegend: Array<Object>;
  noDataInGraph = false;
  noDataInTreemap = false;
  showInitialMessage = true;

  colorPer: Array<Object> = [
    { num: 1, name: 'Severity' },
    { num: 2, name: 'Priority' },
    { num: 3, name: 'Component' },
    { num: 4, name: 'Status' },
    { num: 5, name: 'Root cause' }
  ];

  severityFilter: Array<Object> = [
    { num: 0, name: 'Low', checked: false },
    { num: 1, name: 'Medium', checked: false },
    { num: 2, name: 'High', checked: false },
    { num: 3, name: 'Immediate', checked: false },
    { num: 4, name: 'No data', checked: false }
  ];

  priorityFilter: Array<Object> = [
    { num: 0, name: 'Low', checked: false },
    { num: 1, name: 'Medium', checked: false },
    { num: 2, name: 'High', checked: false },
    { num: 3, name: 'Immediate', checked: false },
    { num: 4, name: 'No data', checked: false }
  ];

  statusFilter: Array<Object> = [
    { num: 0, name: 'Detected', checked: false },
    { num: 1, name: 'In Progress', checked: false },
    { num: 2, name: 'Ready for Release', checked: false },
    { num: 3, name: 'Closed', checked: false },
    { num: 4, name: 'Not a Defect', checked: false },
    { num: 5, name: 'Duplicate', checked: false },
    { num: 6, name: 'Re-opened', checked: false }
  ];

  defectsPositioningPer: Array<Object> = [
    { num: 0, name: 'Name', checked: false },
    { num: 1, name: 'Description', checked: false },
    { num: 2, name: 'Component', checked: false },
    { num: 3, name: 'Severity', checked: false },
    { num: 4, name: 'Priority', checked: false },
    { num: 5, name: 'Status', checked: false },
    { num: 6, name: 'Type', checked: false },
    { num: 7, name: 'AssignedTo', checked: false }
  ];

  treemap_colorLegend: Array<Object> = [
    { num: 0, name: 'High', color: '#f7486e' },
    { num: 1, name: 'Medium', color: '#ff924a' },
    { num: 2, name: 'Low', color: '#ffe146' }
  ];

  selectedColorPer = {};
  selectedSeverityFilter = this.severityFilter;
  selectedPriorityFilter = this.priorityFilter;
  selectedStatusFilter = this.statusFilter;
  selectedDefectsPositioningPer = this.defectsPositioningPer;
  selectedDefectsRange = {};

  ///////Range slider////////
  minValue: number = 0;
  maxValue: number = 100;
  options: Options = {
    floor: 0,
    ceil: 100
  };
  range_slider_val: any;
  filters_for_reports = {};
  constructor(
    private route: ActivatedRoute,
    private reportService: ReportService,
    private sessionService: SessionService,
    private router: Router,
    private calendar: NgbCalendar,
    public permissionService: PermissionService
  ) {
    this.filters_for_reports = {};
    localStorage.setItem('filters', JSON.stringify(this.filters_for_reports));
    localStorage.setItem('showPredictions', 'Real');
  }

  ngOnInit() {
    this.selectedColorPer = this.colorPer[0];
    this.colorLegend = [
      { num: 0, name: 'Immediate', color: '#f7486e' },
      { num: 1, name: 'High', color: '#ff924a' },
      { num: 2, name: 'Medium', color: '#ffe146' },
      { num: 3, name: 'Low', color: '#6dbf47' }
    ];
    this.selectedDefectsRange = this.defectsRangeFilter[0];
    this.clusteringData = {
      params: [],
      filters: {},
      graphForPrediction: 'False',
      hashId: this.hashId
    };

    this.loadForceDirectedGraph();

    this.createTreeMap();
  }

  loadForceDirectedGraph() {}

  applyFilters() {
    // alert("THis functionality will be added on Monday 1 April 2019")
    this.noDataInGraph = false;

    let positioningParamValues = [];
    let severityFilterValues = [];
    let priorityFilterValues = [];
    let statusFilterValues = [];

    for (let i = 0; i < this.selectedDefectsPositioningPer.length; i++) {
      if (this.selectedDefectsPositioningPer[i]['checked']) {
        positioningParamValues.push(this.selectedDefectsPositioningPer[i]['name']);
      }
    }

    for (let i = 0; i < this.selectedSeverityFilter.length; i++) {
      if (this.selectedSeverityFilter[i]['checked']) {
        severityFilterValues.push(this.selectedSeverityFilter[i]['name']);
      }
    }

    for (let i = 0; i < this.selectedPriorityFilter.length; i++) {
      if (this.selectedPriorityFilter[i]['checked']) {
        priorityFilterValues.push(this.selectedPriorityFilter[i]['name']);
      }
    }

    for (let i = 0; i < this.selectedStatusFilter.length; i++) {
      if (this.selectedStatusFilter[i]['checked']) {
        statusFilterValues.push(this.selectedStatusFilter[i]['name']);
      }
    }

    this.clusteringData['filters']['severity'] = severityFilterValues;
    this.clusteringData['filters']['priority'] = priorityFilterValues;
    this.clusteringData['filters']['status'] = statusFilterValues;
    this.clusteringData['filters']['range'] = this.selectedDefectsRange;

    let ableToCalculate = true;

    if (positioningParamValues.length == 0) {
      this.alert.displayAlert('Alert!', 'Please select at least one positioning value', 'info');
      ableToCalculate = false;
    } else {
      this.clusteringData['params'] = positioningParamValues;
      for (let i = 0; i < positioningParamValues.length; i++) {
        if (positioningParamValues[i] === 'Severity') {
          if (severityFilterValues.length === 0) {
            this.alert.displayAlert('Alert!', 'Please select at least one severity value', 'info');
            ableToCalculate = false;
          }
        }
        if (positioningParamValues[i] === 'Priority') {
          if (priorityFilterValues.length === 0) {
            this.alert.displayAlert('Alert!', 'Please select at least one priority type', 'info');
            ableToCalculate = false;
          }
        }
        if (positioningParamValues[i] === 'Status') {
          if (statusFilterValues.length === 0) {
            this.alert.displayAlert('Alert!', 'Please select at least one status type', 'info');
            ableToCalculate = false;
          }
        }
      }
    }

    if (ableToCalculate) {
      document.getElementById('processing_span').style.display = 'inline';
      this.reportService.generateForceDirectedGraph(this.hashId, this.clusteringData).subscribe((data: any) => {
        if (data.nodes.length > 0) {
          this.noDataInGraph = false;
          this.showInitialMessage = false;
          this.graphData = data;
          this.createForceDirectedGraph(this.graphData);
        } else {
          this.showInitialMessage = false;
          this.noDataInGraph = true;
          this.clearForceDirectedGraph();
          document.getElementById('processing_span').style.display = 'none';
        }
      });
    }
  }

  clearForceDirectedGraph() {
    let svg: any = d3
      .select('svg')
      .attr('width', 0)
      .attr('height', 0);

    // remove any previous graphs
    svg.selectAll('.g-main').remove();

    let gMain = svg.append('g').classed('g-main', true);
  }

  createForceDirectedGraph(graph: any): any {
    let tooltipDiv = d3
      .select('#d3_selectable_force_directed_graph')
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);

    let parentWidth = (d3.select('svg').node() as any).parentNode.clientWidth;
    let parentHeight = (d3.select('svg').node() as any).parentNode.clientHeight;

    let svg: any = d3
      .select('svg')
      .attr('width', parentWidth)
      .attr('height', parentHeight);

    svg.append('p').text('Select radius size:');
    svg
      .append('input')
      .attr('class', 'inputRadius')
      .attr('type', 'range')
      .attr('min', 0)
      .attr('max', 100)
      .attr('step', 1)
      .attr('value', 10)
      .on('change', function() {
        let radiusChoice = d3.select('.inputRadius').property('value');
        d3.select('circle')
          .transition()
          // .duration(1000)
          .attr('r', radiusChoice);
      });

    // remove any previous graphs
    svg.selectAll('.g-main').remove();

    let gMain = svg.append('g').classed('g-main', true);

    let rect = gMain
      .append('rect')
      .attr('class', 'brushRect')
      .attr('width', parentWidth)
      .attr('height', parentHeight)
      .style('fill', 'white');

    let gDraw = gMain.append('g');

    let zoom = d3.zoom().on('zoom', zoomed);

    gMain.call(zoom);

    function zoomed() {
      gDraw.attr('transform', d3.event.transform);
    }

    let color = d3.scaleOrdinal(d3.schemeCategory20);

    if (!('links' in graph)) {
      return;
    }

    let nodes = {};
    let i;
    for (i = 0; i < graph.nodes.length; i++) {
      nodes[graph.nodes[i].id] = graph.nodes[i];
      graph.nodes[i].weight = 1.01;
    }

    // the brush needs to go before the nodes so that it doesn't
    // get called when the mouse is over a node
    let gBrushHolder = gDraw.append('g');
    let gBrush = null;

    let link = gDraw
      .append('g')
      .attr('class', 'link')
      .selectAll('line')
      .data(graph.links)
      .enter()
      .append('line')
      .attr('class', 'graph-edges')
      .attr('stroke', '#ccc') // add this line
      // .attr("stroke-width", function (d) { return Math.sqrt(d.value) * 10; });
      // .attr("stroke-width", function (d) { return Math.sqrt(d.value); });
      .attr('stroke-width', function(d) {
        return 1;
      });

    let goToDetails = (id: any) => {
      this.router.navigate(['/defects-reports/detail/', id]);
    };

    let node = gDraw
      .append('g')
      .attr('class', 'node')
      .selectAll('circle')
      .data(graph.nodes)
      .enter()
      .append('circle')
      // .attr("r", 5)
      .attr('r', 8)
      // .attr('r', d => {
      //   let debug_defects = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
        
      //   if(debug_defects.includes(d.id)){
      //     return 18;
      //   }
      //   else{
      //     return 10;
      //   }
      // })
      .attr('fill', d => {   
        // let debug_defects = ['2010', '2011', '2012', '2013', '2014', '2015', '2016', '2017']
        // if(debug_defects.includes(d.id)){
        //   return '#000000';
        // }
        if ('color' in d) {
          return d.color;
        } else if (this.selectedColorPer['name'] === 'Root cause') {
          return color(d.group_types);
        } else if (this.selectedColorPer['name'] === 'Severity') {
          this.colorLegend = [
            { num: 0, name: 'Immediate', color: '#f7486e' },
            { num: 1, name: 'High', color: '#ff924a' },
            { num: 2, name: 'Medium', color: '#ffe146' },
            { num: 3, name: 'Low', color: '#6dbf47' }
          ];
          if (d.group_severity === 'Immediate') {
            return '#f7486e';
          } else if (d.group_severity === 'High') {
            return '#ff924a';
          } else if (d.group_severity === 'Medium') {
            return '#ffe146';
          } else if (d.group_severity === 'Low') {
            return '#6dbf47';
          } else {
            return color(d.group_severity);
          }
        } else if (this.selectedColorPer['name'] === 'Priority') {
          this.colorLegend = [
            { num: 0, name: 'Immediate', color: '#f7486e' },
            { num: 1, name: 'High', color: '#ff924a' },
            { num: 2, name: 'Medium', color: '#ffe146' },
            { num: 3, name: 'Low', color: '#6dbf47' }
          ];
          if (d.group_priority === 'Immediate') {
            return '#f7486e';
          } else if (d.group_priority === 'High') {
            return '#ff924a';
          } else if (d.group_priority === 'Medium') {
            return '#ffe146';
          } else if (d.group_priority === 'Low') {
            return '#6dbf47';
          } else {
            return color(d.group_priority);
          }
        } else if (this.selectedColorPer['name'] === 'Component') {
          return color(d.group_component);
        } else if (this.selectedColorPer['name'] === 'Status') {
          this.colorLegend = [
            { num: 0, name: 'Detected', color: '#78d0ef' },
            { num: 1, name: 'Re-opened', color: '#2b95ba' },
            { num: 2, name: 'In progress', color: '#fac364' },
            { num: 3, name: 'Ready for re-test', color: '#7554af' },
            { num: 4, name: 'Ready for release', color: '#c04aaf' },
            { num: 5, name: 'Closed', color: '#6dbf47' },
            { num: 6, name: 'Not a defect', color: '#aaaaaa' }
          ];
          if (d.group_status === 'Detected') {
            return '#78d0ef';
          } else if (d.group_status.toLowerCase() === 'Re-opened'.toLowerCase()) {
            return '#2b95ba';
          } else if (d.group_status.toLowerCase() === 'In progress'.toLowerCase()) {
            return '#fac364';
          } else if (d.group_status.toLowerCase() === 'Ready for re-test'.toLowerCase()) {
            return '#7554af';
          } else if (d.group_status.toLowerCase() === 'Ready for release'.toLowerCase()) {
            return '#c04aaf';
          } else if (d.group_status.toLowerCase() === 'Closed'.toLowerCase()) {
            return '#6dbf47';
          } else if (d.group_status.toLowerCase() === 'Not a defect'.toLowerCase()) {
            return '#aaaaaa';
          } else {
            return color(d.group_status);
          }
        }
      })
      .attr('fx', function(d) {
        return d.x;
      })
      .attr('fy', function(d) {
        return d.y;
      })
      .call(
        d3
          .drag()
          .on('start', dragstarted)
          .on('drag', dragged)
          .on('end', dragended)
      )
      // .on('click', (d) => {
      // })
      .on('contextmenu', (d: any) => {
        d3.event.preventDefault();
        this.reportService
          .getReportById(d.id, this.hashId)

          .subscribe((data: Report) => {
            this.report = data[0];
            tooltipDiv
              .transition()
              .duration(200)
              .style('opacity', 0.9);
            tooltipDiv
              .html(
                "<i style='cursor: pointer;' class=' closeTooltip fa fa-close pull-right'></i><br>" +
                  '<strong>DefectId: </strong>' +
                  d.id +
                  '</br>' +
                  '<strong>Name: </strong>' +
                  this.report.NAME +
                  '</br>' +
                  '<strong>Root cause: </strong>' +
                  this.report.TYPE +
                  '</br>' +
                  '<strong>Severity: </strong>' +
                  this.report.SEVERITY +
                  '</br>' +
                  '<strong>Priority: </strong>' +
                  this.report.PRIORITY +
                  '</br>' +
                  '<strong>Status: </strong>' +
                  this.report.STATUS +
                  '</br>' +
                  '<strong>Business Area: </strong>' +
                  this.report.COMPONENT +
                  '</br>' +
                  "<button class='btn btn-sm btn-block btn-primary' id='viewMoreButton' data-id='" +
                  d.id +
                  "'>View More</button>"
              )
              .style('left', '0px')
              .style('top', '0px');

            d3.select('#viewMoreButton').on('click', () => {
              goToDetails(parseInt($('#viewMoreButton').data('id')));
            });

            d3.select('.closeTooltip').on('click', () => {
              tooltipDiv
                .transition()
                .duration(500)
                .style('opacity', 0);
            });
          });
      });

    let lables = node
      .append('text')
      .text(function(d: any) {
        if ('name' in d) {
          return d.name;
        } else {
          return d.id;
        }
      })
      .attr('x', 6)
      .attr('y', 3)
      .attr('class', 'node-text');

    let simulation = d3.forceSimulation().force(
      'link',
      d3
        .forceLink()
        .id(function(d: any) {
          return d.id;
        })
        .distance(function(d: any) {
          return 30;
        })
    );

    simulation
      .nodes(graph.nodes)
      .on('tick', ticked)
      .on('end', function() {
        document.getElementById('processing_span').style.display = 'none';
      });

    function ticked() {
      link
        .attr('x1', function(d) {
          return d.source.x;
        })
        .attr('y1', function(d) {
          return d.source.y;
        })
        .attr('x2', function(d) {
          return d.target.x;
        })
        .attr('y2', function(d) {
          return d.target.y;
        });

      node
        .attr('cx', function(d) {
          return d.x;
        })
        .attr('cy', function(d) {
          return d.y;
        });
    }

    // simulation.stop();

    var brushMode = false;
    var brushing = false;

    var brush = d3
      .brush()
      .on('start', brushstarted)
      .on('brush', brushed)
      .on('end', brushended);

    function brushstarted() {
      // keep track of whether we're actively brushing so that we
      // don't remove the brush on keyup in the middle of a selection
      brushing = true;

      node.each(function(d) {
        d.previouslySelected = shiftKey && d.selected;
      });
    }

    rect.on('click', () => {
      node.each(function(d) {
        d.selected = false;
        d.previouslySelected = false;
      });
      node.classed('selected', false);
    });

    function brushed() {
      if (!d3.event.sourceEvent) {
        return;
      }
      if (!d3.event.selection) {
        return;
      }

      let extent = d3.event.selection;

      node.classed('selected', function(d: any) {
        return (d.selected =
          d.previouslySelected ^ ((extent[0][0] <= d.x && d.x < extent[1][0] && extent[0][1] <= d.y && d.y < extent[1][1]) as any));
      });
    }

    function brushended() {
      if (!d3.event.sourceEvent) {
        return;
      }
      if (!d3.event.selection) {
        return;
      }
      if (!gBrush) {
        return;
      }

      gBrush.call(brush.move, null);

      if (!brushMode) {
        // the shift key has been release before we ended our brushing
        gBrush.remove();
        gBrush = null;
      }

      brushing = false;
    }

    d3.select('body').on('keydown', keydown);
    d3.select('body').on('keyup', keyup);

    var shiftKey;

    function keydown() {
      shiftKey = d3.event.shiftKey;

      if (shiftKey) {
        // if we already have a brush, don't do anything
        if (gBrush) {
          return;
        }

        brushMode = true;

        if (!gBrush) {
          gBrush = gBrushHolder.append('g');

          gBrush.call(brush);
        }
      }
    }

    function keyup() {
      shiftKey = false;
      brushMode = false;

      if (!gBrush) {
        return;
      }

      if (!brushing) {
        // only remove the brush if we're not actively brushing
        // otherwise it'll be removed when the brushing ends
        gBrush.remove();
        gBrush = null;
      }
    }

    function dragstarted(d) {
      if (!d3.event.active) {
        simulation.alphaTarget(0.9).restart();
      }
      if (!d.selected && !shiftKey) {
        // if this node isn't selected, then we have to unselect every other node
        node.classed('selected', function(p) {
          return (p.selected = p.previouslySelected = false);
        });
      }

      d3.select(this).classed('selected', function(p) {
        d.previouslySelected = d.selected;
        return (d.selected = true);
      });

      node
        .filter(function(d) {
          return d.selected;
        })
        .each(function(d) {
          d.fx = d.x;
          d.fy = d.y;
        });
    }

    function dragged(d) {
      node
        .filter(function(d) {
          return d.selected;
        })
        .each(function(d) {
          d.fx += d3.event.dx;
          d.fy += d3.event.dy;
        });
    }

    function dragended(d) {
      if (!d3.event.active) {
        simulation.alphaTarget(0);
      }
      d.fx = null;
      d.fy = null;
      node
        .filter(function(d): any {
          return d.selected;
        })
        .each(function(d) {
          d.fx = null;
          d.fy = null;
        });
    }

    let texts = ['Use the scroll wheel to zoom', 'Hold the shift key to select nodes'];

    svg
      .selectAll('text')
      .data(texts)
      .enter()
      .append('text')
      .attr('x', 900)
      .attr('y', function(d, i) {
        return 470 + i * 18;
      })
      .text(function(d) {
        return d;
      });
    document.getElementById('processing_span').style.display = 'none';
    return graph;
  }

  createTreeMap(): any {
    if (this.permissionService.hasPermission("/getDataForTreemap/hashId/")){
      this.reportService.getDataForTreemap(this.hashId).subscribe((receivedData: any) => {
        if (receivedData.length > 0) {
          this.treemapInput = receivedData;
          this.maxValue = 100;
          this.minValue = 0;
  
          let data = {
            name: 'Business Areas',
            children: []
          };
  
          let children_list = this.treemapInput;
          data.children = children_list;
  
          let parentWidth = (d3.select('#svg_d3_treemap').node() as any).clientWidth;
          let parentHeight = (d3.select('#svg_d3_treemap').node() as any).clientHeight;
  
          let svg: any = d3.select('#svg_d3_treemap'),
            width = +parentWidth,
            height = +parentHeight;
  
          let fader = function(color) {
              return d3.interpolateRgb(color, '#fff')(0.2);
            },
            color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
            format = d3.format(',d');
  
          let treemap = d3
            .treemap()
            .tile(d3.treemapResquarify)
            .size([width, height])
            .round(true)
            .paddingInner(1);
  
          let root = d3
            .hierarchy(data)
  
            .eachBefore(function(d: any) {
              d.data.id = (d.parent ? d.parent.data.id + '.' : '') + d.data.name;
            })
            .sum(function(d: any) {
              return d.value;
            })
            .sort(function(a, b) {
              return b.height - a.height || b.value - a.value;
            });
  
          treemap(root);
  
          let cell = svg
            .selectAll('g')
            .data(root.leaves())
            .enter()
            .append('g')
            .attr('transform', function(d: any) {
              return 'translate(' + d.x0 + ',' + d.y0 + ')';
            });
  
          cell
            .append('rect')
            .attr('id', function(d: any) {
              return d.data.id;
            })
            .attr('width', function(d: any) {
              return d.x1 - d.x0;
            })
            .attr('height', function(d: any) {
              return d.y1 - d.y0;
            })
            .attr('fill', function(d: any) {
              let severity_percentage = 0;
              if (d.data.value > 0) {
                severity_percentage = (d.data.high_sev + d.data.immediate_sev) / d.data.value;
              }
              if (severity_percentage >= 0.7) {
                return '#f7486e';
              } else if (severity_percentage >= 0.5 && severity_percentage < 0.7) {
                return '#ff924a';
              } else {
                return '#ffe146';
              }
              return color(d.parent.data.id);
            });
  
          cell
            .append('clipPath')
            .attr('id', function(d: any) {
              return 'clip-' + d.data.id;
            })
            .append('use')
            .attr('xlink:href', function(d: any) {
              return '#' + d.data.id;
            });
  
          cell
            .append('text')
            .attr('clip-path', function(d: any) {
              return 'url(#clip-' + d.data.id + ')';
            })
            .selectAll('tspan')
            .data(function(d: any) {
              if (d.data.name == null) {
                return 'N/A';
              } else {
                return d.data.name.split(/(?=[A-Z][^A-Z])/g);
              }
            })
            .enter()
            .append('tspan')
            .attr('x', 4)
            .attr('y', function(d: any, i: number) {
              return 13 + i * 10;
            })
            .text(function(d: any) {
              return d;
            });
  
          cell.append('title').text(function(d: any) {
            let severity_percentage = 0;
            let risk = '';
            if (d.data.value > 0) {
              severity_percentage = (d.data.high_sev + d.data.immediate_sev) / d.data.value;
            }
            if (severity_percentage >= 0.7) {
              risk = 'High';
            } else if (severity_percentage >= 0.5 && severity_percentage < 0.7) {
              risk = 'Medium';
            } else {
              risk = 'Low';
            }
            let high = '';
            if((d.data.high_sev / d.data.value) == 1){
              high = ((d.data.high_sev / d.data.value) * 100).toString();
            }else{
              high = ((d.data.high_sev / d.data.value) * 100).toPrecision(2).toString();
            }
  
            let immediate = '';
            if((d.data.immediate_sev / d.data.value) == 1){
              immediate = ((d.data.immediate_sev / d.data.value) * 100).toString();
            }else{
              immediate = ((d.data.immediate_sev / d.data.value) * 100).toPrecision(2).toString();
            }
  
            let text_message = 'Business area: ' + d.data.id + '\n';
            text_message = text_message + 'Risk based on the severity: ' + risk + '\n';
            text_message = text_message + 'Num. of open defects: ' + format(d.value) + '\n';
            text_message = text_message + 'High: ' + high + '%\n';
            text_message = text_message + 'Percentage of immediate severity: ' + immediate + '%';
            return text_message;
          });
          this.treemap_data = this.treemapInput;
        } else {
          this.noDataInTreemap = true;
        }
      });
    }
    // this.treemap_data = this.treemapInput;
    return this.treemap_data;
  }

  updateTreemap(svg: any, treemap_filtering: any): any {
    this.maxValue = 100;
    this.minValue = 0;
    const data = {
      name: 'Business Areas',
      children: []
    };
    this.treemapInput_updated = [];
    this.treemapInput_per_severity_high = [];
    this.treemapInput_per_severity_medium = [];
    this.treemapInput_per_severity_low = [];
    for (var d = 0; d < this.treemapInput.length; d++) {
      var severity_percentage = 0;
      if (this.treemapInput[d].value > 0) {
        severity_percentage = (this.treemapInput[d].high_sev + this.treemapInput[d].immediate_sev) / this.treemapInput[d].value;
      }

      if (severity_percentage >= 0.7) {
        this.treemapInput_per_severity_high.push(this.treemapInput[d]);
      } else if (severity_percentage >= 0.5 && severity_percentage < 0.7) {
        this.treemapInput_per_severity_medium.push(this.treemapInput[d]);
      } else {
        this.treemapInput_per_severity_low.push(this.treemapInput[d]);
      }
    }

    if (treemap_filtering === 'low') {
      this.treemapInput_updated = this.treemapInput_per_severity_low;
    } else if (treemap_filtering === 'medium') {
      this.treemapInput_updated = this.treemapInput_per_severity_medium;
    } else if (treemap_filtering === 'high') {
      this.treemapInput_updated = this.treemapInput_per_severity_high;
    } else if (treemap_filtering === 'all') {
      this.treemapInput_updated = this.treemapInput;
    }

    this.maxValue = 100;
    this.minValue = 0;

    let children_list = this.treemapInput_updated;
    data.children = children_list;

    let parentWidth = (d3.select('#svg_d3_treemap').node() as any).clientWidth;
    let parentHeight = (d3.select('#svg_d3_treemap').node() as any).clientHeight;
    d3.selectAll('#svg_d3_treemap > *').remove();

    var svg: any = d3.select('#svg_d3_treemap'),
      width = +parentWidth,
      height = +parentHeight;

    let fader = function(color) {
        return d3.interpolateRgb(color, '#fff')(0.2);
      },
      color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
      format = d3.format(',d');

    let treemap = d3
      .treemap()
      .tile(d3.treemapResquarify)
      .size([width, height])
      .round(true)
      .paddingInner(1);

    let root = d3
      .hierarchy(data)
      // .eachBefore(function (d: any) { d.data.id = (d.parent ? d.parent.data.id + "." : "") + d.data.name; })
      .eachBefore(function(d: any) {
        d.data.id = (d.parent ? d.parent.data.id + '.' : '') + d.data.name;
      })
      .sum(function(d: any) {
        return d.value;
      })
      .sort(function(a, b) {
        return b.height - a.height || b.value - a.value;
      });

    treemap(root);

    let cell = svg
      .selectAll('g')
      .data(root.leaves())
      .enter()
      .append('g')
      .attr('transform', function(d: any) {
        return 'translate(' + d.x0 + ',' + d.y0 + ')';
      });

    cell
      .append('rect')
      .attr('id', function(d: any) {
        return d.data.id;
      })
      .attr('width', function(d: any) {
        return d.x1 - d.x0;
      })
      .attr('height', function(d: any) {
        return d.y1 - d.y0;
      })
      .attr('fill', function(d: any) {
        let severity_percentage = 0;
        if (d.data.value > 0) {
          severity_percentage = (d.data.high_sev + d.data.immediate_sev) / d.data.value;
        }
        if (severity_percentage >= 0.7) {
          return '#f7486e';
        } else if (severity_percentage >= 0.5 && severity_percentage < 0.7) {
          return '#ff924a';
        } else {
          return '#ffe146';
        }
        return color(d.parent.data.id);
      });

    cell
      .append('clipPath')
      .attr('id', function(d: any) {
        return 'clip-' + d.data.id;
      })
      .append('use')
      .attr('xlink:href', function(d: any) {
        return '#' + d.data.id;
      });

    cell
      .append('text')
      .attr('clip-path', function(d: any) {
        return 'url(#clip-' + d.data.id + ')';
      })
      .selectAll('tspan')
      .data(function(d: any) {
        if (d.data.name === null) {
          return 'N/A';
        } else {
          return d.data.name.split(/(?=[A-Z][^A-Z])/g);
        }
      })
      .enter()
      .append('tspan')
      .attr('x', 4)
      .attr('y', function(d: any, i: number) {
        return 13 + i * 10;
      })
      .text(function(d: any) {
        return d;
      });

    cell.append('title').text(function(d: any) {
      let severity_percentage = 0;
      let risk = '';
      if (d.data.value > 0) {
        severity_percentage = (d.data.high_sev + d.data.immediate_sev) / d.data.value;
      }
      if (severity_percentage >= 0.7) {
        risk = 'High';
      } else if (severity_percentage >= 0.5 && severity_percentage < 0.7) {
        risk = 'Medium';
      } else {
        risk = 'Low';
      }
      let text_message = 'Business area: ' + d.data.id + '\n';
      text_message = text_message + 'Risk: ' + risk + '\n';
      text_message = text_message + 'Num. of open defects: ' + format(d.value) + '\n';
      text_message = text_message + 'High: ' + ((d.data.high_sev / d.data.value) * 100).toPrecision(2).toString() + '%\n';
      text_message = text_message + 'Immediate: ' + ((d.data.immediate_sev / d.data.value) * 100).toPrecision(2).toString() + '%';
      return text_message;
    });
    this.treemap_data = this.treemapInput_updated;
    console.log(this.treemap_data)
    return this.treemap_data;
  }

  setNewCeil(newCeil: number): void {
    const newOptions: Options = Object.assign({}, this.options);
    newOptions.ceil = newCeil;
    this.options = newOptions;
  }

  setradio(choice: any): void {
    if (choice === 3) {
      if (this.selectedDefectsPositioningPer[3]['checked']) {
        document.getElementById('severity_choices').style.display = 'none';
      } else {
        document.getElementById('severity_choices').style.display = 'block';
      }
    } else if (choice === 4) {
      if (this.selectedDefectsPositioningPer[4]['checked']) {
        document.getElementById('priority_choices').style.display = 'none';
      } else {
        document.getElementById('priority_choices').style.display = 'block';
      }
    } else if (choice === 5) {
      if (this.selectedDefectsPositioningPer[5]['checked']) {
        document.getElementById('status_choices').style.display = 'none';
      } else {
        document.getElementById('status_choices').style.display = 'block';
      }
    }
  }

  onChangeColorPer(newObj) {
    let color = d3.scaleOrdinal(d3.schemeCategory20);
    this.colorLegend = [];
    let nodes = d3.selectAll('circle').style('fill', (d: any) => {
      if (this.selectedColorPer['name'] === 'Root cause') {
        this.colorLegend = [];
        return color(d.group_types);
      } else if (this.selectedColorPer['name'] === 'Severity') {
        this.colorLegend = [
          { num: 0, name: 'Immediate', color: '#f7486e' },
          { num: 1, name: 'High', color: '#ff924a' },
          { num: 2, name: 'Medium', color: '#ffe146' },
          { num: 3, name: 'Low', color: '#6dbf47' }
        ];
        if (d.group_severity === 'Immediate') {
          return '#f7486e';
        } else if (d.group_severity === 'High') {
          return '#ff924a';
        } else if (d.group_severity === 'Medium') {
          return '#ffe146';
        } else if (d.group_severity === 'Low') {
          return '#6dbf47';
        } else {
          return color(d.group_severity);
        }
      } else if (this.selectedColorPer['name'] === 'Priority') {
        this.colorLegend = [
          { num: 0, name: 'Immediate', color: '#f7486e' },
          { num: 1, name: 'High', color: '#ff924a' },
          { num: 2, name: 'Medium', color: '#ffe146' },
          { num: 3, name: 'Low', color: '#6dbf47' }
        ];
        if (d.group_priority === 'Immediate') {
          return '#f7486e';
        } else if (d.group_priority === 'High') {
          return '#ff924a';
        } else if (d.group_priority === 'Medium') {
          return '#ffe146';
        } else if (d.group_priority === 'Low') {
          return '#6dbf47';
        } else {
          return color(d.group_priority);
        }
      } else if (this.selectedColorPer['name'] === 'Status') {
        this.colorLegend = [
          { num: 0, name: 'Detected', color: '#78d0ef' },
          { num: 1, name: 'Re-opened', color: '#2b95ba' },
          { num: 2, name: 'In progress', color: '#fac364' },
          { num: 3, name: 'Ready for re-test', color: '#7554af' },
          { num: 4, name: 'Ready for release', color: '#c04aaf' },
          { num: 5, name: 'Closed', color: '#6dbf47' },
          { num: 6, name: 'Not a defect', color: '#aaaaaa' }
        ];
        if (d.group_status.toLowerCase() === 'Detected'.toLowerCase()) {
          return '#78d0ef';
        } else if (d.group_status.toLowerCase() === 'Re-opened'.toLowerCase()) {
          return '#2b95ba';
        } else if (d.group_status.toLowerCase() === 'In progress'.toLowerCase()) {
          return '#fac364';
        } else if (d.group_status.toLowerCase() === 'Ready for re-test'.toLowerCase()) {
          return '#7554af';
        } else if (d.group_status.toLowerCase() === 'Ready for release'.toLowerCase()) {
          return '#c04aaf';
        } else if (d.group_status.toLowerCase() === 'Closed'.toLowerCase()) {
          return '#6dbf47';
        } else if (d.group_status.toLowerCase() === 'Not a defect'.toLowerCase()) {
          return '#aaaaaa';
        } else {
          return color(d.group_status);
        }
      } else if (this.selectedColorPer['name'] === 'Component') {
        this.colorLegend = [];
        return color(d.group_component);
      }
    });
  }

  onRangeCheck(minvalue_for_slice: any, maxvalue_for_slice: any, treemap_data: any): any {
    let minvalue = (minvalue_for_slice / 100) * treemap_data.length;
    let maxvalue = (maxvalue_for_slice / 100) * treemap_data.length;
    this.treemapInput_updated = this.treemap_data.slice(minvalue, maxvalue);
    let data = {
      name: 'Business Areas',
      children: []
    };

    let children_list = this.treemapInput_updated;
    data.children = children_list;

    let parentWidth = (d3.select('#svg_d3_treemap').node() as any).clientWidth;
    let parentHeight = (d3.select('#svg_d3_treemap').node() as any).clientHeight;
    d3.selectAll('#svg_d3_treemap > *').remove();

    let svg: any = d3.select('#svg_d3_treemap'),
      width = +parentWidth, //+svg.attr("width"),
      height = +parentHeight; //+svg.attr("height");

    let fader = function(color) {
        return d3.interpolateRgb(color, '#fff')(0.2);
      },
      color = d3.scaleOrdinal(d3.schemeCategory20.map(fader)),
      format = d3.format(',d');

    let treemap = d3
      .treemap()
      .tile(d3.treemapResquarify)
      .size([width, height])
      .round(true)
      .paddingInner(1);

    let root = d3
      .hierarchy(data)
      .eachBefore(function(d: any) {
        d.data.id = (d.parent ? d.parent.data.id + '.' : '') + d.data.name;
      })
      .sum(function(d: any) {
        return d.value;
      })
      .sort(function(a, b) {
        return b.height - a.height || b.value - a.value;
      });

    treemap(root);

    let cell = svg
      .selectAll('g')
      .data(root.leaves())
      .enter()
      .append('g')
      .attr('transform', function(d: any) {
        return 'translate(' + d.x0 + ',' + d.y0 + ')';
      });

    cell
      .append('rect')
      .attr('id', function(d: any) {
        return d.data.id;
      })
      .attr('width', function(d: any) {
        return d.x1 - d.x0;
      })
      .attr('height', function(d: any) {
        return d.y1 - d.y0;
      })
      .attr('fill', function(d: any) {
        var severity_percentage = 0;
        if (d.data.value > 0) {
          severity_percentage = (d.data.high_sev + d.data.immediate_sev) / d.data.value;
        }
        if (severity_percentage >= 0.7) {
          return '#f7486e';
        } else if (severity_percentage >= 0.5 && severity_percentage < 0.7) {
          return '#ff924a';
        } else {
          return '#ffe146';
        }
        return color(d.parent.data.id);
      });

    cell
      .append('clipPath')
      .attr('id', function(d: any) {
        return 'clip-' + d.data.id;
      })
      .append('use')
      .attr('xlink:href', function(d: any) {
        return '#' + d.data.id;
      });

    cell
      .append('text')
      .attr('clip-path', function(d: any) {
        return 'url(#clip-' + d.data.id + ')';
      })
      .selectAll('tspan')
      .data(function(d: any) {
        if (d.data.name == null) {
          return 'N/A';
        } else {
          return d.data.name.split(/(?=[A-Z][^A-Z])/g);
        }
      })
      .enter()
      .append('tspan')
      .attr('x', 4)
      .attr('y', function(d: any, i: number) {
        return 13 + i * 10;
      })
      .text(function(d: any) {
        return d;
      });

    cell.append('title').text(function(d: any) {
      var severity_percentage = 0;
      var risk = '';
      if (d.data.value > 0) {
        severity_percentage = (d.data.high_sev + d.data.immediate_sev) / d.data.value;
      }
      if (severity_percentage >= 0.7) {
        risk = 'High';
      } else if (severity_percentage >= 0.5 && severity_percentage < 0.7) {
        risk = 'Medium';
      } else {
        risk = 'Low';
      }
      var text_message = 'Business area: ' + d.data.id + '\n';
      text_message = text_message + 'Risk: ' + risk + '\n';
      text_message = text_message + 'Num. of open defects: ' + format(d.value) + '\n';
      text_message = text_message + 'High: ' + ((d.data.high_sev / d.data.value) * 100).toPrecision(2).toString() + '%\n';
      text_message = text_message + 'Immediate: ' + ((d.data.immediate_sev / d.data.value) * 100).toPrecision(2).toString() + '%';
      return text_message;
    });
  }

  onRangeSliderApply() {
    if (this.treemap_filtering === ' ') {
      this.onRangeCheck(this.minValue, this.maxValue, this.treemap_data);
    } else {
      this.onRangeCheck(this.minValue, this.maxValue, this.treemap_data);
    }
  }

  onRadioBtnApply() {
    this.treemap_data = this.updateTreemap(this.svg2, this.treemap_filtering);
  }

  goToDetails(id: any) {
    this.router.navigate(['/defects-reports/detail/', id]);
  }
}
