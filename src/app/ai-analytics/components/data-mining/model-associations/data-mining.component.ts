import { Component, OnInit } from '@angular/core';
import 'hammerjs';
import 'chartjs-plugin-zoom';
import 'chartjs-plugin-zoom';
import { DesignService } from 'src/app/design/services/design.service';
import { SessionService } from 'src/app/core/services/session.service';
import { PermissionService } from 'src/app/core/services/permission.service';
@Component({
  selector: 'app-model-associations',
  templateUrl: './data-mining.component.html',
  styleUrls: ['./data-mining.component.css']
})
export class ModelAssociationsComponent implements OnInit {
    public allCatalogs = [];
    public catalogs = [];
    public typicals = [];
    public selectedTypical = null;
    public selectedCatalog = null;
    public typicalForAssociations = null;

    constructor(
        private designService: DesignService,
        private sessionService: SessionService,
        public permissionService: PermissionService
    ) {}

    async ngOnInit() {
        const projectId = this.sessionService.getProject().id;
        const subProjectId = this.sessionService.getSubProject().id;
        let catalogs = await this.designService.getTenantCatalogs(projectId, subProjectId).toPromise();
        if (catalogs) {
            this.allCatalogs = catalogs.filter(cat => cat.type.toString() !== '4');
            this.catalogs = catalogs.filter(cat => cat.type.toString() !== '4');
        }
    }

    async catalogChanged(catalog) {
        this.selectedCatalog = catalog;
        if (catalog) {
            let typicals = await this.designService.searchTypicals('', catalog.id).toPromise();
            if (typicals) {
                this.typicals = typicals;
                this.selectedTypical = null;
                this.typicalForAssociations = null;
            } else {
                this.typicals = [];
            }
        } else {
            this.typicals = [];
        }
    }

    handleCatalogChange(search) {
        this.catalogs = this.allCatalogs.filter(catalog => catalog.title.toLowerCase().includes(search.toLowerCase()));
    }

    changeTypical(typical) {
        this.selectedTypical = typical;
        this.typicalForAssociations = null;
    }

    async handleTypicalChange(search) {
        if (this.selectedCatalog) {
            let typicals = await this.designService.searchTypicals(search, this.selectedCatalog.id).toPromise();
            if (typicals) {
                this.typicals = typicals;
            } else {
                this.typicals = [];
            }
        } else {
            this.typicals = [];
        }
    }

    applyFilters() {
        this.typicalForAssociations = this.selectedTypical;
    }

}
