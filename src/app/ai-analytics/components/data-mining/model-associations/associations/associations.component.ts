import {Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ElementRef, ViewChild} from '@angular/core';
import * as go from 'gojs';
import { TestGenerationService } from 'src/app/test-generation/services/test-generation.service';

@Component({
  selector: 'app-data-associations',
  templateUrl: './associations.component.html',
  styleUrls: ['./associations.component.css']
})
export class DataAssociationsComponent implements OnInit, AfterViewInit {
  public diagram: go.Diagram = new go.Diagram();
  public palette: go.Palette = new go.Palette();

  @Input() 
  set selectedTypical(selectedTypical) {
    if (selectedTypical) {
      this.testGenerationService
      .GetModelVisualization(selectedTypical.title, selectedTypical.id)
      .subscribe(result => {
        this.model = new go.GraphLinksModel(result.items, result.links);
      });
    } else {
      this.model = new go.GraphLinksModel([], []);
    }
  };

  @Input()
  get model(): go.Model {
    return this.diagram.model;
  }

  set model(val: go.Model) {
    this.diagram.model = val;
  }

  public data: any;
  public node: go.Node;

  @ViewChild('diagramDiv')
  private diagramRef: ElementRef;

  @ViewChild('paletteDiv')
  private paletteRef: ElementRef;
  @Output()
  nodeSelected = new EventEmitter<go.Node | null>();

  @Output()
  modelChanged = new EventEmitter<go.ChangedEvent>();

  @ViewChild('text')
  private textField: ElementRef;

  constructor(private testGenerationService: TestGenerationService) {
    const $ = go.GraphObject.make;
    this.diagram = new go.Diagram();
    this.diagram.initialContentAlignment = go.Spot.Center;
    this.diagram.initialAutoScale = go.Diagram.UniformToFill;
    this.diagram.contentAlignment = go.Spot.Center;
    this.diagram.layout = $(go.ForceDirectedLayout, {defaultSpringLength: 10});
    this.diagram.padding = 10;
    this.diagram.maxSelectionCount = 2;
    this.diagram.allowDrop = true;
    this.diagram.undoManager.isEnabled = true;
    this.diagram.addDiagramListener('ChangedSelection', e => {
      const node = e.diagram.selection.first();
      this.nodeSelected.emit(node instanceof go.Node ? node : null);
    });
    this.diagram.addModelChangedListener(e => e.isTransactionFinished && this.modelChanged.emit(e));
    this.diagram.nodeTemplate = $(
      go.Node,
      'Horizontal',
      {
        locationSpot: go.Spot.Center,
        locationObjectName: 'SHAPE',
        selectionAdorned: false
      },
      $(
        go.Panel,
        'Auto',
        $(
          go.Shape,
          'Ellipse',
          {
            name: 'SHAPE',
            fill: 'lightgray', // default value, but also data-bound
            stroke: 'transparent', // modified by highlighting
            strokeWidth: 2,
            desiredSize: new go.Size(30, 30),
            portId: ''
          }, // so links will go to the shape, not the whole node
          new go.Binding('fill', 'isSelected', function (s, obj) {
            return s ? 'red' : obj.part.data.color;
          }).ofObject()
        ),
        $(
          go.TextBlock,
          new go.Binding('text', 'distance', function (d) {
            if (d === Infinity) {
              return 'INF';
            } else {
              return d | 0;
            }
          })
        )
      ),
      $(go.TextBlock, new go.Binding('text')),
      new go.Binding('location', 'loc', go.Point.parse).makeTwoWay(go.Point.stringify)
    );

    this.diagram.linkTemplate = $(
      go.Link,
      {
        selectable: false, // links cannot be selected by the user
        curve: go.Link.Bezier,
        layerName: 'Background' // don't cross in front of any nodes
      },
      $(
        go.Shape, // this shape only shows when it isHighlighted
        {isPanelMain: true, stroke: null, strokeWidth: 5},
        new go.Binding('stroke', 'isHighlighted', function (h) {
          return h ? 'red' : null;
        }).ofObject()
      ),
      $(
        go.Shape,
        // mark each Shape to get the link geometry with isPanelMain: true
        {isPanelMain: true, stroke: 'black', strokeWidth: 1},
        new go.Binding('stroke', 'color')
      ),
      $(go.Shape, {toArrow: 'Standard'})
    );

    this.palette = new go.Palette();
    this.palette.nodeTemplateMap = this.diagram.nodeTemplateMap;

    // initialize contents of Palette
    this.palette.model.nodeDataArray = [
      {key: 0, text: 'CUSTOMER', color: go.Brush.randomColor(128, 240)},
      {key: 1, text: 'ACCOUNT', color: go.Brush.randomColor(128, 240)},
      {key: 2, text: 'FUNDS.TRANSFER', color: go.Brush.randomColor(128, 240)}
    ];
  }

  ngOnInit() {
    
  }

  ngAfterViewInit() {
    this.diagram.div = this.diagramRef.nativeElement;

    // this.palette.div = this.paletteRef.nativeElement;
  }

  showDetails(node: go.Node | null) {
    this.node = node;
    if (node) {
      this.data = {
        text: node.data.text,
        color: node.data.color
      };
    } else {
      this.data = null;
    }
  }
}
