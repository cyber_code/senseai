import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DefectReplayComponent} from './components/predictive-analytics-defect/defect-replay/defect-replay.component';
import { AuthGuard } from '../shared/auth.guards';
import { ProjectSelectionGuard } from '../shared/project-selection.guards';
import { RequirementsPrioritizationComponent } from './components/requirement-reports/requirements-prioritization/requirements-prioritization.component';
import { ProcessManagmentReportsComponent } from './components/requirement-reports/process-managment-reports/process-managment-reports.component';
import { TraceabilityReportComponent } from './components/requirement-reports/traceability-report/traceability-report.component';
import { PredictiveAnalyticsDefectComponent } from './components/predictive-analytics-defect/main/predictive-analytics-defect.component';
import { ReportsStatisticsComponent } from './components/predictive-analytics-defect/reports-statistics/reports-statistics.component';
import { PredictiveAnalyticsProjectComponent } from './components/predictive-analytics-project/main/predictive-analytics-project.component';
import { SummaryReportsComponent } from './components/predictive-analytics-project/summary-reports/main/summary-reports.component';
import { ImpactAnalysisComponent } from './components/predictive-analytics-project/impact-analysis/impact-analysis.component';
import { DashboardMainComponent } from './components/dashboard-main/dashboard.component';
import { WorkflowsComponent } from './components/requirement-reports/workflows/workflows.component';
import { RiskOptimizerComponent } from './components/predictive-analytics-project/risk-optimizer/risk-optimizer.component';
import { MainDataMiningComponent } from './components/data-mining/main/data-mining-main.component';
import { ModelAssociationsComponent } from './components/data-mining/model-associations/data-mining.component';
import { DashboardComponent } from './components/data-mining/dashboard/dashboard.component';


const routes: Routes = [
  {path: 'requirement-reports/workflows', component: WorkflowsComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'predictive-analytics-project', component: PredictiveAnalyticsProjectComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'predictive-analytics-project/summary-report', component: SummaryReportsComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'predictive-analytics-project/impact-analysis', component: ImpactAnalysisComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'predictive-analytics-project/risk-optimizer', component: RiskOptimizerComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'predictive-analytics-defect', component: PredictiveAnalyticsDefectComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'predictive-analytics-defect/defects-replay', component: DefectReplayComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'predictive-analytics-project/dashboard-main', component: DashboardMainComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'predictive-analytics-defect/defects-statistics', component: ReportsStatisticsComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'requirement-reports', component: ProcessManagmentReportsComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'requirement-reports/requirements-prioritization', component: RequirementsPrioritizationComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'requirement-reports/tr-report', component: TraceabilityReportComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'data-mining', component: MainDataMiningComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'data-mining/model-associations', component: ModelAssociationsComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'data-mining/defect-classification', component: DashboardComponent, canActivate: [AuthGuard, ProjectSelectionGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AiAnalyticsRoutingModule {
}
