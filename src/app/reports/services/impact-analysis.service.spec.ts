import { TestBed } from '@angular/core/testing';

import { ImpactAnalysisService } from './impact-analysis.service';

describe('ImpactAnalysisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImpactAnalysisService = TestBed.get(ImpactAnalysisService);
    expect(service).toBeTruthy();
  });
});
