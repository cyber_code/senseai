import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { ConfigurationService } from 'src/app/core/services/configuration.service';

@Injectable({
  providedIn: 'root'
})
export class WorkflowService {
  seq_id_selected: any | null;
  workflowTitle: string | null;

  constructor(
    private http: HttpClient,
    private configurationService: ConfigurationService
  ) { }

  private reportsUrl = this.configurationService.serverSettings.reportsUrl;
  response: any;

  getWorkflowsForTable(hashId: string, processType: string, data: any): Observable<any> {
    let url = this.reportsUrl + 'getDataWorkFlowsTable/hashId/' + hashId + '/processType/' + processType
    let res = this.http.post(url, data)
    return res;
  }

  getDataCoverage(hashId: string, seq_id_selected: string): Observable<any> {
    let url = this.reportsUrl + 'getDataCoverage/hashId/' + hashId + '/seq_id_selected/' + seq_id_selected
    let res = this.http.get(url)
    return res;
  }

  getworkflowInfo(hashId: string, seq_id_selected: string): Observable<any> {
    let url = this.reportsUrl + 'getworkflowInfo/hashId/' + hashId + '/seq_id_selected/' + seq_id_selected
    let res = this.http.get(url)
    return res;
  }

  getWorkflowsPerBusArea(hashId: string, businessArea: string, processType: string): Observable<any> {
    let url = this.reportsUrl + 'getWorkflowsPerBusArea/hashId/' + hashId + '/businessArea/' + businessArea + '/processType/' + processType
    let res = this.http.get(url)
    return res;
  }

  getDataFrequency(hashId: string, application: string, version: string, item_selected: string): Observable<any> {
    let url = this.reportsUrl + 'getDataFrequency/application/' + application + '/version/' + version + '/param_table/' + item_selected + '/hashId/' + hashId
    let res = this.http.get(url)
    return res;
  }
}
