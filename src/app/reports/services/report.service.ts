import {Injectable} from '@angular/core';
import {Report} from '../report';
import {REPORTS} from '../mock-reports';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import 'rxjs/add/operator/map';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import { SessionService } from 'src/app/core/services/session.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(
    private http: HttpClient,
    private httpExecutor: HttpExecutorService,
    private configurationService: ConfigurationService,
    private sessionService: SessionService
  ) {
  }

  private reportsUrl = this.configurationService.serverSettings.reportsUrl;
  private apiUrl = this.configurationService.serverSettings.apiUrl;

  response: any;

  getDefectsWithLimit(hashId: string, numOfDefects: number): Observable<any> {
    let url = this.reportsUrl + 'getDefectsWithLimit/hashId/' + hashId + '/limit/' + numOfDefects;
    let res = this.http.get(url);
    return res;
  }

  getTotalNumberOfDefects(hashId: string): Observable<any> {
    const url = this.reportsUrl + 'getTotalNumberOfDefects/hashId/' + hashId;
    const res = this.http.get(url);
    return res;
  }

  getFilteredDefects(hashId: string, data: any): Observable<any> {
    const url = this.reportsUrl + 'getFilteredDefects/hashId/' + hashId;
    const res = this.http.post(url, data);
    return res;
  }

  getListOfDefects(hashId: string, data: any): Observable<any> {
    const url = this.reportsUrl + 'getListOfDefects/hashId/' + hashId;
    const res = this.http.post(url, data);
    return res;
  }


  getReportById(id: number, hashId: string): Observable<any> {
    const url = this.reportsUrl + 'getDefectById/hashId/' + hashId + '/defectId/' + id;
    const res = this.http.get(url);
    return res;
  }

  getDeveloperById(id: number): Observable<any> {
    const url = this.reportsUrl + 'getDeveloperById/developerId/' + id;
    const res = this.http.get(url);
    return res;
  }

  getDataForTreemap(hashId: string): Observable<any> {
    const url = this.reportsUrl + 'getDataForTreemap/hashId/' + hashId;
    const res = this.http.get(url);
    return res;
  }

  getDataForBusinessAreaBarChart(params: any, hashId: string): Observable<any> {
    const url = this.reportsUrl + 'getDataForBusinessAreaBarChart/hashId/' + hashId + '/';
    const res = this.http.post(url, params);
    return res;
  }

  getDefectsGraph(data: any): Observable<any> {
    const url = this.reportsUrl + 'getDefectsGraph';
    const res = this.http.post(url, data);
    return res;
  }

  getDefectsGraphForPrediction(data: any): Observable<any> {
    const url = this.reportsUrl + 'getDefectsGraphForPrediction';
    const res = this.http.post(url, data);
    return res;
  }

  generateForceDirectedGraph(hashId: string, data: any): Observable<any> {
    const url = this.reportsUrl + 'generateForceDirectedGraph/hashId/' + hashId;
    const res = this.http.post(url, data);
    return res;
  }

  countPriorityPerType(params: any, hashId: string): Observable<any> {
    const url = this.reportsUrl + 'countPriorityPerType/hashId/' + hashId + '/';
    const res = this.http.post(url, params);
    return res;
  }

  getTimeToFixPerBusinessAreaAndType(params: any, hashId: string): Observable<any> {
    const url = this.reportsUrl + 'getTimeToFixPerBusinessAreaAndType/hashId/' + hashId + '/';
    const res = this.http.post(url, params);
    return res;
  }

  countStatus(params: any, hashId: string): Observable<any> {
    const url = this.reportsUrl + 'countStatus/hashId/' + hashId + '/';
    const res = this.http.post(url, params);
    return res;
  }

  countSeverityPerBusinessArea(params: any, hashId: string): Observable<any> {
    const url = this.reportsUrl + 'countSeverityPerBusinessArea/hashId/' + hashId + '/';
    const res = this.http.post(url, params);
    return res;
  }

  countTypePerBusinessArea(params: any, hashId: string): Observable<any> {
    const url = this.reportsUrl + 'countTypePerBusinessArea/hashId/' + hashId + '/';
    const res = this.http.post(url, params);
    return res;
  }

  getLimeJustification(hashId: string, reportId: number): Observable<any> {
    const url = this.reportsUrl + 'getLimeJustification/hashId/' + hashId + '/reportId/' + reportId;
    const res = this.http.get(url);
    return res;
  }

  getDiscoveryRatioByArea(params: any, hashId: string): Observable<any> {
    const url = this.reportsUrl + 'getDiscoveryRatioByArea/hashId/' + hashId + '/';
    const res = this.http.post(url, params);
    return res;
  }

  getDefectsColumnMapping(): Observable<any> {
    const url = this.apiUrl + 'Insight/GetDefectsColumnMapping';
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const res = this.http.get(url, {headers: headers});
    return res;
  }

  findBestMatchingDeveloper(hashId: string, defectId: number): Observable<any> {
    const url = this.reportsUrl + 'findBestMatchingDeveloper/hashId/' + hashId + '/defectId/' + defectId;
    const res = this.http.get(url);
    return res;
  }

  getAllComponents(hashId: string): Observable<any> {
    const url = this.reportsUrl + 'getAllComponents/hashId/' + hashId ;
    const res = this.http.get(url);
    return res;
  }

  getAvailableDateRange(hashId: string): Observable<any> {
    const url = this.reportsUrl + 'getAvailableDateRange/hashId/' + hashId ;
    const res = this.http.get(url);
    return res;
  }

  getDefectsForReplay(params: any, hashId: string, duration: any): Observable<any> {
    const url = this.reportsUrl + 'getDefectsForReplay/hashId/' + hashId + "/duration/" + duration;
    const res = this.http.post(url, params);
    return res;
  }
  

}
