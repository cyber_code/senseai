import { TestBed } from '@angular/core/testing';

import { TaskAssignmentServiceService } from './task-assignment-service.service';

describe('TaskAssignmentServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaskAssignmentServiceService = TestBed.get(TaskAssignmentServiceService);
    expect(service).toBeTruthy();
  });
});
