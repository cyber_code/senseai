import {Injectable} from '@angular/core';
import {Report} from '../report';
import {REPORTS} from '../mock-reports';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, tap} from 'rxjs/operators';
import 'rxjs/add/operator/map';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {ConfigurationService} from 'src/app/core/services/configuration.service';

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {

  constructor(
    private http: HttpClient,
    private httpExecutor: HttpExecutorService,
    private configurationService: ConfigurationService
  ) {}

  private reportsUrl = this.configurationService.serverSettings.reportsUrl;

  getAllDevelopers(hashId): Observable<any> {
    let url = this.reportsUrl + 'getAllDevelopers/hashId/' + hashId;
    let res = this.http.get(url);
    return res;
  }

  getAllQAs(hashId): Observable<any> {
    let url = this.reportsUrl + 'getAllQAs/hashId/' + hashId;
    let res = this.http.get(url);
    return res;
  }

  getDeveloper(hashId, devId): Observable<any> {
    let url = this.reportsUrl + '/getDeveloper/hashId/' + hashId + '/developerId/' + devId;
    let res = this.http.get(url);
    return res;
  }

  addDeveloper(hashId, developer): Observable<any> {
    let url = this.reportsUrl + 'addDeveloper/hashId/' + hashId;
    let res = this.http.post(url, developer);
    return res;
  }

  updateDeveloper(hashId, developer): Observable<any> {
    let url = this.reportsUrl + 'updateDeveloper/hashId/' + hashId;
    let res = this.http.post(url, developer);
    return res;
  }

  deleteDeveloper(hashId, devId): Observable<any> {
    let url = this.reportsUrl + 'deleteDeveloperById/hashId/' + hashId + '/developerId/' + devId;
    let res = this.http.get(url);
    return res;
  }

}
