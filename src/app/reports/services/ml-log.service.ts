import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {SessionService} from 'src/app/core/services/session.service';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import { map } from 'rxjs/operators';
import { MessageType } from 'src/app/core/models/message.model';
import {MessageService} from 'src/app/core/services/message.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';

@Injectable({
  providedIn: 'root'
})
export class MlLogService {

  constructor(
    private http: HttpClient,
    private sessionService: SessionService,
    private configurationService: ConfigurationService,
    private messageService: MessageService
  ) {
  }

  executeMlLogs(data: any): Observable<any> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const formData = new FormData();
    for (let i = 0; i < data.files.length; i++) {
      formData.append('files', data.files[i]);
    }
    formData.append('tenantId', data.tenantId);
    formData.append('projectId', data.projectId);
    formData.append('subProjectId', data.subProjectId);
    formData.append('catalogId', data.catalogId);
    formData.append('systemId', data.systemId);
    formData.append('workflowId', data.workflowId);
    const url = this.configurationService.serverSettings.apiUrl + 'Insight/UploadExecutionLogs';
    return this.http.post(url, formData, {headers: headers}).pipe(
      map((res: any) => {
        if (res && res.hasResult && res.successful) {
          return res.result;
        } else if (res && res.error && res.error.exceptionObject && res.error.exceptionObject.Message) {
          this.messageService.sendMessage({type: MessageType.Error, text: res.error.exceptionObject.Message});
          return;
        } 
      })
    );
  }

}
