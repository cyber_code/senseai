import { catchError, retry, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable, of, Subject, throwError } from 'rxjs';
import { CommandMethod } from 'src/app/shared/command-method';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';
import { SaveDefects } from 'src/app/core/services/command-queries/command-queries';
import { HttpClient, HttpRequest, HttpEvent, HttpEventType, HttpResponse,
  HttpErrorResponse, HttpHandler, HttpHeaders, HttpParams} from '@angular/common/http';
import { Http, Headers, RequestOptions } from '@angular/http';
import { SessionService } from 'src/app/core/services/session.service';
import { MessageType } from 'src/app/core/models/message.model';
import {MessageService} from 'src/app/core/services/message.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Router } from '@angular/router';


const projectNotifier = new Subject<void>();
const subProjectNotifier = new Subject<void>();

@Injectable()
export class DataExtractionServiceService {
  private apiUrl: string;
  constructor(private configuration: ConfigurationService,
    private http: HttpClient,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService,
    private messageService: MessageService
    ) {
      this.apiUrl = `${configuration.serverSettings.apiUrl}Insight/`;
  }

  
  saveDefects(formModel): Observable<any> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const url = this.apiUrl + 'SaveDefects';
    return this.http.post(url, formModel, {headers : headers,
      reportProgress: true, observe: 'events'
    });
  }

 }

