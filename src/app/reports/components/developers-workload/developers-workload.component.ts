// import { Component, OnInit } from '@angular/core';
import { AfterViewInit, Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ChartOptions, ChartType, ChartDataSets } from "chart.js";
import * as pluginDataLabels from "chartjs-plugin-datalabels";
import { Label } from "ng2-charts";
import * as d3 from "d3";
import { SessionService } from "src/app/core/services/session.service";
// import { VisTimelineService, VisTimelineItems , Network, DataSet, Node, Edge, IdType } from 'vis';
import { TaskAssignmentServiceService } from 'src/app/reports/services/task-assignment-service.service';
import { ModalOverdueTasksComponent } from 'src/app/reports/components/developers-workload/modal-overdue-tasks/modal-overdue-tasks.component';


import { GanttModule } from '@syncfusion/ej2-angular-gantt';
import { PermissionService } from 'src/app/core/services/permission.service';
import { QueryAst } from '@angular/animations/browser/src/dsl/animation_ast';
import { MessageService } from 'src/app/core/services/message.service';
declare var vis: any;


@Component({
  selector: 'app-developers-workload',
  templateUrl: './developers-workload.component.html',
  styleUrls: ['./developers-workload.component.css']
})
export class DevelopersWorkloadComponent implements OnInit {
  hashId = this.sessionService.getHashId();
  modalReference: any;
  task_color: any;

  @ViewChild("timeline") timelineContainer: ElementRef;
  @ViewChild("timelineQA") timelineContainerQA: ElementRef;

  tlContainer: any;
  tlContainerQA:any;
  timeline: any;
  timelineQA: any;
  data: any;
  groups: any;
  dataQA: any;
  groupsQA: any;
  options: {};

  colorList: any = {
    Immediate: 'red',
    High: 'orange',
    Medium: 'yellow', 
    Moderate: 'yellowgreen', 
    Low: 'green',

  };

  colorLegend: Array<Object> = [
    { num: 0, name: 'Immediate', color: '#f7486e' },
    { num: 1, name: 'High', color: '#ff924a' },
    { num: 2, name: 'Medium', color: '#ffe146' },
    { num: 3, name: 'Low', color: '#6dbf47' }
  ];

  colorPer: Array<Object> = [
    { num: 1, name: 'Priority' },
    { num: 2, name: 'Severity' },
  ];
  selectedColorPer = {};
  minValue: number = 0;
  maxValue: number = 100;
  showOnlyUncommitedTasks: boolean = false;

  range_slider_val: any;
  AllbarChartData: any;
  AllbarChartData_updated: any;
  AllbarChartDataQA: any;
  AllbarChartDataQA_updated: any;
  loading: boolean = false;

  public constructor(
    private taskAssignmentServiceService: TaskAssignmentServiceService,
    private sessionService: SessionService,
    private permssionService: PermissionService,
    private messageService: MessageService,
    private modalService: NgbModal) {
    this.data = new vis.DataSet();
    this.groups = new vis.DataSet();
    this.groupsQA = new vis.DataSet();
    this.dataQA = new vis.DataSet();

  }

  public barChartOptions: ChartOptions = {
    responsive: true,
    scales: { xAxes: [{}], yAxes: [{ticks: {min: 0, max:100}}] },
    plugins: {
      datalabels: {
        display: false,
        anchor: "end",
        align: "end"
      }
    }
  };

  public barChartLabels: Label[] = [
    "2006",
    "2007",
    "2008",
    "2009",
    "2010",
    "2011",
    "2012"
  ];
  public barChartLabelsQA: Label[] = [
    "2006",
    "2007",
    "2008",
    "2009",
    "2010",
    "2011",
    "2012"
  ];
  public barChartType: ChartType = "bar";
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [], label: "Series A" },
    { data: [], label: "Series B" }
  ];
  public barChartDataQA: ChartDataSets[] = [
    { data: [], label: "Series A" },
    { data: [], label: "Series B" }
  ];
  nodata = false;
  workload_timespan = 0;

  // 

  public chartClicked({
    event,
    active
  }: {
    event: MouseEvent;
    active: {}[];
  }): void { }

  public chartHovered({
    event,
    active
  }: {
    event: MouseEvent;
    active: {}[];
  }): void { }


  onClickForModalOpen() {

    this.modalReference = this.modalService.open(ModalOverdueTasksComponent);
    this.modalReference.componentInstance.hashId = this.hashId;
    this.modalReference.result.then((result: any) => { }, (reason: any) => { });

  }

  onChangeColorPer(newObj) {
    this.colorLegend = [];
    if (this.selectedColorPer['name'] === 'Severity') {
      this.colorLegend = [
        { num: 0, name: 'Immediate', color: '#f7486e' },
        { num: 1, name: 'High', color: '#ff924a' },
        { num: 2, name: 'Medium', color: '#ffe146' },
        { num: 2, name: 'Moderate', color: '#C9DF79' },
        { num: 3, name: 'Low', color: '#6dbf47' }
      ];
    } else if (this.selectedColorPer['name'] === 'Priority') {
      this.colorLegend = [
        { num: 0, name: 'Immediate', color: '#f7486e' },
        { num: 1, name: 'High', color: '#ff924a' },
        { num: 2, name: 'Medium', color: '#ffe146' },
        { num: 3, name: 'Low', color: '#6dbf47' }
      ];
    }
    this.getTimelineData();
    this.getTimelineDataQA();
  }


  ngOnInit() {
    this.selectedColorPer = this.colorPer[0];
    this.colorLegend = [
      { num: 0, name: 'Immediate', color: '#f7486e' },
      { num: 1, name: 'High', color: '#ff924a' },
      { num: 2, name: 'Medium', color: '#ffe146' },
      { num: 3, name: 'Low', color: '#6dbf47' }
    ];
  if (this.permssionService.hasPermission("/getAllDevelopersWorkload/hashId/"))
    this.taskAssignmentServiceService
      .getAllDevelopersWorkload(this.hashId)
      .subscribe((data: any) => {
        
        this.workload_timespan = data.workload_timespan;
        if (data.workloads.length > 0) {
          this.nodata = false;
          var names = [];
          var values = [];
          if (data.workloads != null) {
            for (let i = 0; i < data.workloads.length; i++) {
              names.push(data.workloads[i]["NAME"]);
              values.push(data.workloads[i]["WORKLOAD"]);
            }
            this.barChartLabels = names;
            let valuesForChart: ChartDataSets[] = [
              { data: values, label: "Workload" }
            ];
            this.barChartData = valuesForChart;
            this.AllbarChartData = valuesForChart;
          }

        } else {
          this.nodata = true;
        }
      });
      if (this.permssionService.hasPermission("/getAllDevelopersWorkload/hashId/"))
      this.taskAssignmentServiceService
      .getAllQAsWorkload(this.hashId)
      .subscribe((data: any) => {
        
        this.workload_timespan = data.workload_timespan;
        if (data.workloads.length > 0) {
          this.nodata = false;
          var names = [];
          var values = [];
          if (data.workloads != null) {
            for (let i = 0; i < data.workloads.length; i++) {
              names.push(data.workloads[i]["NAME"]);
              values.push(data.workloads[i]["WORKLOAD"]);
            }
            this.barChartLabelsQA = names;
            let valuesForChart: ChartDataSets[] = [
              { data: values, label: "Workload" }
            ];
            this.barChartDataQA = valuesForChart;
            this.AllbarChartDataQA = valuesForChart;
          }

        } else {
          this.nodata = true;
        }
      });
    this.getTimelineData();
    this.getTimelineDataQA();
    this.getTimelineGroups();
    this.getTimelineGroupsQA();
    this.getOptions();
  }

  ngAfterViewInit() {
    this.tlContainer = this.timelineContainer.nativeElement;
    this.timeline = new vis.Timeline(this.tlContainer, null, this.options);
    this.timeline.setGroups(this.groups);
    this.timeline.setItems(this.data);
    this.tlContainerQA = this.timelineContainerQA.nativeElement;
    this.timelineQA = new vis.Timeline(this.tlContainerQA, null, this.options);
    this.timelineQA.setGroups(this.groupsQA);
    this.timelineQA.setItems(this.dataQA);
  }

  calculatePlan(){
    this.data.clear();
    this.dataQA.clear();
    this.loading = true;
    this.taskAssignmentServiceService.getTaskAssignment(this.hashId, 1).subscribe((data: any)=>{
      if (data && data.status === true){
        this.getTimelineData();
        this.getTimelineDataQA();
        this.loading = false;
      }else{
        this.messageService.sendMessage({type: 3, text: data.message});
        this.getTimelineData();
        this.getTimelineDataQA();
        this.loading = false;
      }
      
    });
  }

  getTimelineGroups() {
    // this.groups = new vis.DataSet();
    // create groups
    if (this.permssionService.hasPermission("/getAllDevelopersWorkload/hashId/"))
    this.taskAssignmentServiceService
      .getAllDevelopers(this.hashId)
      .subscribe((data: any) => {
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            this.groups.update({ id: data[i].DEV_ID, content: data[i].NAME })
          }
        } else {
          this.groups = [];
        }
      });
  }

  getTimelineGroupsQA() {
    // this.groups = new vis.DataSet();
    // create groups
    if (this.permssionService.hasPermission("/getAllDevelopersWorkload/hashId/"))
    this.taskAssignmentServiceService
      .getAllQAs(this.hashId)
      .subscribe((data: any) => {
        if (data.length > 0) {
          for (var i = 0; i < data.length; i++) {
            this.groupsQA.update({ id: data[i].DEV_ID, content: data[i].NAME })
          }
        } else {
          this.groupsQA = [];
        }
      });
  }


  getTimelineData() {
    // Create a DataSet (allows two way data-binding)
    // create items
    this.data.clear();
    if (this.permssionService.hasPermission("/getAllDevelopersWorkload/hashId/"))
    this.taskAssignmentServiceService
      .getTaskSchedule(this.hashId)
      .subscribe((data: any) => {
        for (var j = 0; j < data.Jobs.length; j++) {
          if(this.showOnlyUncommitedTasks === true && data.Jobs[j].Commited)
            continue;
          if (this.selectedColorPer['name'] === 'Priority') {
            this.task_color = this.colorList[data.Jobs[j].PriorityLevel];
          } else {
            this.task_color = this.colorList[data.Jobs[j].SeverityLevel];
          }
          this.data.update({
            id: parseInt(data.Jobs[j].DefectID),
            group: parseInt(data.Jobs[j].DeveloperID),
            start: data.Jobs[j].StartTime,
            end: data.Jobs[j].EndTime,
            content: data.Jobs[j].Name,
            editable: { updateTime: false, updateGroup: false, remove: false },
            className: this.task_color + (data.Jobs[j].Commited === true ? " commited" : " uncommited"),
            title: "<b>Defect ID</b>: " + data.Jobs[j].DefectID + "<br> <b>Name: </b>" + data.Jobs[j].Name + "<br> <b>Severity Level: </b>" + data.Jobs[j].SeverityLevel + "<br> <b>Priority Level: </b>" + data.Jobs[j].PriorityLevel + "<br> <b>Start Date: </b>" + data.Jobs[j].StartTime
          });
        }        
        this.timeline.setData({
          data: data
        });
      });
  }

  getTimelineDataQA() {
    // Create a DataSet (allows two way data-binding)
    // create items
    this.data.clear();
    if (this.permssionService.hasPermission("/getAllDevelopersWorkload/hashId/"))
    this.taskAssignmentServiceService
      .getTaskScheduleQA(this.hashId)
      .subscribe((data: any) => {
        for (var j = 0; j < data.Jobs.length; j++) {
          if(this.showOnlyUncommitedTasks === true && data.Jobs[j].Commited)
            continue;

          if (this.selectedColorPer['name'] === 'Priority') {
            this.task_color = this.colorList[data.Jobs[j].PriorityLevel];
          } else {
            this.task_color = this.colorList[data.Jobs[j].SeverityLevel];
          }

          this.dataQA.update({
            id: parseInt(data.Jobs[j].DefectID),
            group: parseInt(data.Jobs[j].DeveloperID),
            start: data.Jobs[j].StartTime,
            end: data.Jobs[j].EndTime,
            content: data.Jobs[j].Name,
            editable: { updateTime: false, updateGroup: false, remove: false },
            className: this.task_color + (data.Jobs[j].Commited === true ? " commited" : " uncommited"),
            title: "<b>Defect ID</b>: " + data.Jobs[j].DefectID + "<br> <b>Name: </b>" + data.Jobs[j].Name + "<br> <b>Severity Level: </b>" + data.Jobs[j].SeverityLevel + "<br> <b>Priority Level: </b>" + data.Jobs[j].PriorityLevel + "<br> <b>Start Date: </b>" + data.Jobs[j].StartTime
          });
        }        
        this.timelineQA.setData({
          data: data
        });
      });
  }


  commitSchedule() {
    if (this.permssionService.hasPermission("/getAllDevelopersWorkload/hashId/"))
    this.taskAssignmentServiceService
    .commitTaskSchedule(this.hashId)
    .subscribe((data: any) => {
      console.log('/commitTaskSchedule response:', data);
      this.getTimelineData();
    });
  }

  onChangeShowOnlyUncommitedTasks(event: any) {
    this.getTimelineData();
  }

  getOptions() {
    // specify options
    this.options = {
      stack: false,
      start: new Date(),
      end: new Date(1000 * 60 * 60 * 24 + (new Date()).valueOf()),
      editable: false,
      showTooltips: true,
      tooltip: {
        followMouse: false,
        overflowMethod: 'cap'
      },
      margin: {
        item: 10, // minimal margin between items
        axis: 5   // minimal margin between items and the axis
      },
      orientation: 'top'
    };
  }
}
