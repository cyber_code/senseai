import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { TaskAssignmentServiceService } from 'src/app/reports/services/task-assignment-service.service';
import { ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from "angular-datatables";

@Component({
  selector: 'app-modal-overdue-tasks',
  templateUrl: './modal-overdue-tasks.component.html',
  styleUrls: ['./modal-overdue-tasks.component.css']
})
export class ModalOverdueTasksComponent implements OnInit {
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  dtInstance: DataTables.Api;
  OverdueTasksInfo: any;
  @Input() public hashId;

  constructor(public activeModal: NgbActiveModal,
    private taskAssignmentServiceService: TaskAssignmentServiceService) { }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: (data: any, callback) => {
        this.taskAssignmentServiceService
          .getOverdueTasksUI(this.hashId, data)
          .subscribe((resp: any) => {
            var tmp_data = resp.data.length > 0 ? JSON.parse(resp.data) : [];
              for (var i = 0; i < tmp_data.length; i++) {
                for (var j = 0; j < resp.history.length; j++) {
                  let history = JSON.parse(resp.history[j].DATA);
                  tmp_data[i].history = history;
                }
              }
              this.OverdueTasksInfo = tmp_data;
              callback({
                recordsTotal: resp.recordsTotal,
                recordsFiltered: resp.recordsFiltered,
                data: []
              });
            });
      },
      columns: [
        { name: 'DEFECT_ID', orderable: false },
        { name: 'DEFECT_NAME', orderable: false },
        { name: 'DEVELOPER_NAME', orderable: false },
        { name: 'STARTDATE', orderable: false },
        { name: 'ENDDATE', orderable: false },
      ]
    };
  }

}
