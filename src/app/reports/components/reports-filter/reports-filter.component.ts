import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports-filter',
  templateUrl: './reports-filter.component.html',
  styleUrls: ['./reports-filter.component.css']
})
export class ReportsFilterComponent {
  defectTimestampsVals = ['Date detected', 'Date assigned', 'Target date', 'Date resolved'];
  severityVals = ['All', 'Low', 'Medium', 'High', 'Critical'];
  priorityVals = ['All', 'Low', 'Medium', 'High', 'Critical'];
  statusVals = ['All', 'Detected', 'Assigned', 'Resolved', 'Overdue'];

  dateTimestamp = '';
  dateRange = '';
  severity = '';
  status = '';
  priority = '';

  submitted = false;

  onSubmit() {
    this.submitted = true;
  }
}
