import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {Report} from '../../report';
import {ReportService} from '../../services/report.service';
import {HttpClient} from '@angular/common/http';
import {DataTableDirective} from 'angular-datatables';
import {NgbCalendar, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import { SessionService } from 'src/app/core/services/session.service';
import {environment} from '../../../../environments/environment';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-defects-reports',
  templateUrl: './defects-reports.component.html',
  styleUrls: ['./defects-reports.component.css']
})
export class DefectsReportsComponent implements OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  defectsReport = 'Defects report';
  title = 'Bugs Hunting';
  reports: Report[];
  reportsFromDB_json = '';
  dtOptions: DataTables.Settings = {};
  hashId = this.sessionService.getHashId();
  totalDefects = -1;
  detectTimestampFilter: Array<Object> = [
    'All',
    'Date detected',
    'Date re-opened',
    'Date assigned',
    'Target date',
    'Date closed'
  ];
  severityFilter: Array<Object> = [
    'All',
    'Low',
    'Medium',
    'High',
    'Immediate',
    'No data'
  ];
  priorityFilter: Array<Object> = [
    'All',
    'Low',
    'Medium',
    'High',
    'Immediate',
    'No data'
  ];
  statusFilter: Array<Object> = [
    'All',
    'Detected',
    'In Progress',
    'Ready for Release',
    'Closed',
    'Not a Defect',
    'Duplicate',
    'Re-opened'
  ];
  bussinessAreasFilter: Array<Object> = ['All'];
  tasksFilter: Array<Object> = ['All'];
  possibleShownValues: Array<string> = ['Real', 'Suggested'];
  shownValues = '';
  filters = {};
  input_filters:any;
  table = null;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  today = this.calendar.getToday();
  showPredictions = false;

  constructor(
    private sessionService: SessionService,
    private reportService: ReportService,
    private http: HttpClient,
    private calendar: NgbCalendar,
    public permissionService : PermissionService
  ) {
  }

  
  

  ngOnInit() {
    this.reports = [];
    if(localStorage.getItem('showPredictions') == 'Real'){
      this.shownValues = this.possibleShownValues[0];
      this.showPredictions = false;
    }else{
      if (localStorage.getItem('showPredictions') === 'Suggested') {
        this.showPredictions = true;
        this.shownValues = this.possibleShownValues[1];
      } else if (localStorage.getItem('showPredictions') === 'Real') {
        this.shownValues = this.possibleShownValues[0];
        this.showPredictions = false;
      }
    }
    
    this.input_filters = JSON.parse(localStorage.getItem('filters'));
    if(!this.input_filters || (Object.entries(this.input_filters).length === 0)){
      this.filters = {
        detectTimestamp: this.detectTimestampFilter[0],
        severity: this.severityFilter[0],
        priority: this.priorityFilter[0],
        status: this.statusFilter[0],
        bussinessArea: this.bussinessAreasFilter[0],
        fromDate: this.fromDate,
        toDate: this.toDate,
        issuetype: this.tasksFilter[0],
      };
      localStorage.setItem('filters', JSON.stringify(this.filters));
    }else{
      this.filters = JSON.parse(localStorage.getItem('filters'));
    }

    this.permissionService.hasPermission("/getFilteredDefects/hashId/")?
    
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      serverSide: true,
      processing: true,
      ajax: (data: any, callback) => {
        data.filters = JSON.parse(localStorage.getItem('filters'));
        this.reportService
          .getFilteredDefects(this.hashId, data)
          .subscribe((resp: any) => {  
            this.bussinessAreasFilter = ['All'].concat(resp.components);
            this.tasksFilter = ['All'].concat(resp.issuetypes)
            var tmp_data = resp.data.length > 0 ? JSON.parse(resp.data) : [];
            for (var i = 0; i < tmp_data.length; i++) {
              tmp_data[i]['PRIORITYPREDICTION'] = tmp_data[i]['PRIORITYPREDICTION'] != null ? JSON.parse(JSON.parse(tmp_data[i]['PRIORITYPREDICTION'])) : null;
              tmp_data[i]['SEVERITYPREDICTION'] = tmp_data[i]['SEVERITYPREDICTION'] != null ? JSON.parse(JSON.parse(tmp_data[i]['SEVERITYPREDICTION'])) : null;
              tmp_data[i]['TYPEPREDICTION'] = tmp_data[i]['TYPEPREDICTION'] != null ? JSON.parse(JSON.parse(tmp_data[i]['TYPEPREDICTION'])) : null;
              tmp_data[i]['TIMETOFIXPREDICTION'] = tmp_data[i]['TIMETOFIXPREDICTION'] != null ? parseInt(tmp_data[i]['TIMETOFIXPREDICTION']) : null;
              tmp_data[i]['TARGETDATE'] = tmp_data[i]['TARGETDATE'] != null ? this.convertTimestampToDate(tmp_data[i]['TARGETDATE']) : null;
              tmp_data[i]['TARGETDATEPREDICTION'] = tmp_data[i]['TARGETDATEPREDICTION'] != null ? this.convertTimestampToDate(tmp_data[i]['TARGETDATEPREDICTION']) : null;
              tmp_data[i]['STARTDATE'] = tmp_data[i]['STARTDATE'] != null ? this.convertTimestampToDate(tmp_data[i]['STARTDATE']) : null;
              tmp_data[i]['MODIFIEDDATE'] = tmp_data[i]['MODIFIEDDATE'] != null ? this.convertTimestampToDate(tmp_data[i]['MODIFIEDDATE']) : null;

              for (var j = 0; j < resp.history.length; j++) {
                if (tmp_data[i].DEFECTID === resp.history[j].DEFECTID) {
                  let history = JSON.parse(resp.history[j].DATA);

                  const totalDays = this.calculateTotalDays(history, new Date(tmp_data[i].TARGETDATE));
                  history = this.createDefectStatusModel(tmp_data[i], history, totalDays);
                  tmp_data[i].history = history;
                  break;
                }
              }
            }
            this.reports = tmp_data;
            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },
      columns: [
        {name: 'DefectId', orderable: true},
        {name: 'Name', orderable: true},
        {name: 'Component', orderable: true},
        {name: 'Type', orderable: true},
        {name: 'Severity', orderable: true},
        {name: 'Priority', orderable: true},
        {name: 'TargetDate', orderable: true},
        {name: 'TimeToFix', orderable: true},
        {name: 'Status', orderable: true},
        {name: 'Ageing', orderable: false},
        {name: 'Details', orderable: false}
      ]
    } : null;
  }

  severityColor(severity: string) {
    if (severity === 'High') {
      return 'high';
    } else if (severity === 'Medium') {
      return 'medium';
    } else if (severity === 'Low') {
      return 'low';
    } else if (severity === 'Immediate') {
      return 'immediate';
    }
  }

  priorityColor(priority: string) {
    if (priority === 'High') {
      return 'high';
    } else if (priority === 'Medium') {
      return 'medium';
    } else if (priority === 'Low') {
      return 'low';
    } else if (priority === 'Immediate') {
      return 'immediate';
    }
  }

  createAgeingLabels(totalDays, totalRealDays) {
    let ob = [];
    for (var i = 1; i <= totalRealDays; i++) {
      let label = {text: '', type: 0};
      if (i === 1) {
        label.text = '1';
      }

      if (i === totalRealDays) {
        label.text = i.toString();
      }

      if (i === totalDays) {
        label.text = i.toString();
        label.type = 1;
      }

      ob.push({
        label: label
      });
    }

    return ob;
  }

  createDefectStatusModel(currentDefectData, currentHistoryData, totalDays) {
    let nextDate = new Date(currentDefectData.TARGETDATE);

    for (var k = 0; k < currentHistoryData.length; k++) {
      currentHistoryData[k].alias = this.getStatusAlias(
        currentHistoryData[k].STATUS
      );

      const currentDate = new Date(
        parseInt(currentHistoryData[k].MODIFIEDDATE)
      );

      if (k + 1 < currentHistoryData.length) {
        nextDate = new Date(parseInt(currentHistoryData[k + 1].MODIFIEDDATE));
      } else {
        if (
          currentHistoryData[k].STATUS === 'Closed' ||
          currentHistoryData[k].STATUS === 'Close'
        ) {
          nextDate = new Date(parseInt(currentHistoryData[k].MODIFIEDDATE));
        } else {
          nextDate = new Date(currentDefectData.TARGETDATE);
          if (nextDate < new Date()) {
            nextDate = new Date();
          }
        }
      }

      var timeDiff = Math.abs(nextDate.getTime() - currentDate.getTime());
      var diffDays = timeDiff / (1000 * 3600 * 24);

      const percentage = (100 * diffDays) / totalDays; // this.getPercentage(diffDays, 0, totalDays - 1);

      currentHistoryData[k].statusDays = Math.round(diffDays);
      currentHistoryData[k].statusDaysPercentage = percentage + '%';
      if (
        currentHistoryData[k].STATUS === 'Closed' ||
        currentHistoryData[k].STATUS === 'Close'
      ) {
        currentHistoryData[k].statusDaysPercentage = '5px';
      }
    }

    currentHistoryData = this.fixPercentage(currentHistoryData);
    return currentHistoryData;
  }

  fixPercentage(history) {
    var total = 0;

    for (var k = 0; k < history.length; k++) {
      if (history[k].STATUS != 'Closed' && history[k].STATUS != 'Close') {
        total += parseFloat(history[k].statusDaysPercentage);
      }
    }

    if (total > 100) {
      history[history.length - 1].statusDaysPercentage =
        (
          parseFloat(history[history.length - 1].statusDaysPercentage) -
          (total - 100)
        ).toString() + '%';
    }

    history[history.length - 1].statusDaysPercentage =
      'calc(' + history[history.length - 1].statusDaysPercentage + ' - 5px)';

    return history;
  }

  calculateTotalDays(history, targetDate) {
    var days = 1;
    var currentDate = null;
    for (var k = 0; k < history.length; k++) {
      if (history[k].STATUS == 'Close' || history[k].STATUS == 'Closed') {
        if (currentDate == null) {
          return days;
        }
        var nextDate = new Date(parseInt(history[k].MODIFIEDDATE));
        var timeDiff = Math.abs(nextDate.getTime() - currentDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        days += diffDays;
        currentDate = new Date(parseInt(history[k].MODIFIEDDATE));
        continue;
      }

      if (currentDate != null) {
        var nextDate = new Date(parseInt(history[k].MODIFIEDDATE));
        if (nextDate < new Date() && k == history.length - 1) {
          nextDate = new Date();
        }
        var timeDiff = Math.abs(nextDate.getTime() - currentDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        days += diffDays;
        currentDate = new Date(parseInt(history[k].MODIFIEDDATE));
      } else {
        currentDate = new Date(parseInt(history[k].MODIFIEDDATE));
      }
    }

    return days;
  }

  getStatusAlias(status) {
    let alias = '';
    switch (status) {
      case 'Open':
      case 'Detected':
        alias = 'detected';
        break;
      case 'In Progress':
        alias = 'inprogress';
        break;
      case 'Ready for Release':
        alias = 'readyForRelease';
        break;
      case 'Close':
      case 'Closed':
        alias = 'closed';
        break;
      case 'Re-open':
      case 'Re-opened':
        alias = 'reopen';
        break;
    }

    return alias;
  }

  getPercentage(value, inputLow, inputHeight) {
    return ((value - inputLow) / (inputHeight - inputLow)) * 100;
  }

  getFirstClosedDateStartingAtIndex(index, historyData) {
    for (var k = index; k < historyData.length; k++) {
      if (
        historyData[k].STATUS === 'Close' ||
        historyData[k].STATUS == 'Closed'
      ) {
        return new Date(parseInt(historyData[k].MODIFIEDDATE));
      }
    }

    return null;
  }

  getLastClosedDateStartingAtIndex(historyData, targetDate) {
    var d = targetDate;
    for (var k = 0; k < historyData.length; k++) {
      if (
        historyData[k].STATUS === 'Close' ||
        historyData[k].STATUS === 'Closed'
      ) {
      }
    }

    d = new Date(parseInt(historyData[historyData.length - 1].MODIFIEDDATE));

    if (
      historyData[historyData.length - 1].STATUS === 'Close' ||
      historyData[historyData.length - 1].STATUS === 'Closed'
    ) {
      return d;
    }

    if (targetDate.getTime() < new Date().getTime()) {
      d = new Date();
    }

    return d;
  }

  ngOnDestroy() {
  }

  public applyFilters() {
    if (this.shownValues === 'Suggested') {
      this.showPredictions = true;
    } else if (this.shownValues === 'Real') {
      this.showPredictions = false;
    }
    localStorage.setItem('filters', JSON.stringify(this.filters));
    localStorage.setItem('showPredictions', String(this.shownValues));

    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  private convertTimestampToDate(timestamp: any) {
    const date = new Date(parseInt(timestamp)).toLocaleString();
    return date;
  }

  public clearFilters(){
    this.input_filters = {};
    this.shownValues = 'Real';
    this.showPredictions = false;
    localStorage.setItem('showPredictions', this.shownValues);
    if((Object.entries(this.input_filters).length === 0)){
      this.filters = {
        detectTimestamp: this.detectTimestampFilter[0],
        severity: this.severityFilter[0],
        priority: this.priorityFilter[0],
        status: this.statusFilter[0],
        bussinessArea: this.bussinessAreasFilter[0],
        issuetype: this.tasksFilter[0],
        fromDate: this.fromDate,
        toDate: this.toDate
      };
      localStorage.setItem('filters', JSON.stringify(this.filters));
    }
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }
}
