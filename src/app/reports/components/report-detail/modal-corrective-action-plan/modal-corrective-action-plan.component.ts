import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportService } from '../../../services/report.service';
import { Report } from 'src/app/reports/report';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal-corrective-action-plan',
  templateUrl: './modal-corrective-action-plan.component.html',
  styleUrls: ['./modal-corrective-action-plan.component.css']
})
export class ModalCorrectiveActionPlanComponent implements OnInit {
  @Input() public hashId: String;
  @Input() public report: Report;
  @Input() public modalType: number;
  @Input() public childNodes: string;
  developersRanking = [];
  developerDetails = null;

  constructor(
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private reportService: ReportService,
    private router: Router
  ) {}

  ngOnInit() {
    this.findBestMatchingDeveloper();
  }

  findBestMatchingDeveloper() {
    this.reportService.findBestMatchingDeveloper(this.hashId.toString(), this.report.DEFECTID).subscribe((results: any) => {
      if (results) {
        this.developersRanking = results;
      }
    });
  }

  getDeveloperById(developerId: number) {
    this.reportService.getDeveloperById(developerId).subscribe((results: any) => {
      if (results) {
        this.developerDetails = results[0];
      }
    });
  }
}
