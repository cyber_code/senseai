import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportService } from '../../../services/report.service';
import { Report } from 'src/app/reports/report';
import * as d3 from 'd3';
import { Router } from '@angular/router';
import { namespace } from 'd3';

@Component({
  selector: 'app-modal-justification',
  templateUrl: './modal-justification.component.html',
  styleUrls: ['./modal-justification.component.css']
})
export class ModalJustificationComponent implements OnInit {
  @Input() public hashId: String;
  @Input() public report: Report;
  @Input() public modalType: number;
  @Input() public childNodes: string;

  lines: Array<any> = [];

  model: Array<any> = [];
  forceDirectedGraph_resp: any;
  forceDirectedGraphOriginal: any;
  colorPer: Array<Object> = [
    { num: 0, name: 'Root cause' },
    { num: 1, name: 'Severity' },
    { num: 2, name: 'Priority' },
    { num: 3, name: 'Component' },
    { num: 4, name: 'Status' }
  ];
  selectedColorPer = this.colorPer[0];

  lime_justification: any;

  constructor(
    public activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private reportService: ReportService,
    private router: Router
  ) {}

  ngOnInit() {
    this.createModel();
    this.loadForceDirectedGraph2();
    this.limeJustification_TableCreation(this.hashId, this.report.ID);
  }

  createModel() {
    this.model = [];

    if (this.modalType == 0 && this.report.TYPEPREDICTION) {
      this.report.PRIORITYPREDICTION.sort(this.compare);
      this.report.SEVERITYPREDICTION.sort(this.compare);
      this.report.TYPEPREDICTION.sort(this.compare);
      for (var i = 0; i < 3; i++) {
        this.model.push({
          priority: this.report.PRIORITYPREDICTION.length > i ? this.report.PRIORITYPREDICTION[i] : null,
          severity: this.report.SEVERITYPREDICTION.length > i ? this.report.SEVERITYPREDICTION[i] : null,
          type: this.report.TYPEPREDICTION.length > i ? this.report.TYPEPREDICTION[i] : null
        });
      }
      // this.report.PRIORITYPREDICTION.forEach((x: any, index: number) => {
      //   this.model.push({
      //     priority: x,
      //     severity: this.report.SEVERITYPREDICTION[index],
      //     type: this.report.TYPEPREDICTION[index],
      //   })
      // });
    }
  }

  compare(a, b) {
    if (a.value > b.value) return -1;
    if (a.value < b.value) return 1;
    return 0;
  }

  getSimilatiesClass(value: number) {
    if (value >= 0 && value < 33) {
      return 'success';
    }
    if (value >= 33 && value < 66) {
      return 'medium';
    }
    if (value >= 66) {
      return 'critical';
    }
  }

  loadForceDirectedGraph2() {
    var svg = d3.select('#d3_selectable_force_directed_graph');
    let data = {
      tenantId: '54b6b87c-41cd-4a21-8dde-62818624678b',
      projectId: '0f76004b-f131-409c-b830-781665f1124c',
      subProjectId: '6b79c2a0-2ef2-48fa-b31f-02b8cc98d62a',
      systemId: '4540ddf2-eed3-4dbe-bf7c-34c17c5d464d',
      catalogId: 'e0603d97-851e-4cf4-a0fb-ab13848ba0c8',
      hashId: this.hashId
    };

    this.reportService.getReportById(this.report.DEFECTID, this.hashId.toString()).subscribe((selectedDefect: any) => {
      if (selectedDefect) {
        let nn = JSON.parse(selectedDefect[0].NN);
        this.reportService.getListOfDefects(this.hashId.toString(), nn).subscribe((defectsList: any) => {
          this.forceDirectedGraph_resp = JSON.parse(
            JSON.stringify(this.createJustificationGraph(this.report.DEFECTID, nn, JSON.parse(defectsList)))
          );
          this.createV4SelectableForceDirectedGraph(svg, this.forceDirectedGraph_resp);
        });
      }
    });
  }

  createJustificationGraph(defectId: number, nn: any, defectsList: any) {
    let graph = {};
    let links = [];
    let nodes = [];
    let node = {};
    node['id'] = defectId.toString();
    node['NAME'] = this.report['NAME'];
    node['TYPE'] = this.report['TYPE'];
    node['SEVERITY'] = this.report['SEVERITY'];
    node['PRIORITY'] = this.report['PRIORITY'];
    node['STATUS'] = this.report['STATUS'];
    node['COMPONENT'] = this.report['COMPONENT'];
    nodes.push(node);
    for (let i = 0; i < nn.length; i++) {
      let node = {};
      let link = {};
      if (nn[i] != defectId) {
        for (let j = 0; j < defectsList.length; j++) {
          if (nn[i] == defectsList[j]['DEFECTID']) {
            node['NAME'] = defectsList[j]['NAME'];
            node['TYPE'] = defectsList[j]['TYPE'];
            node['SEVERITY'] = defectsList[j]['SEVERITY'];
            node['PRIORITY'] = defectsList[j]['PRIORITY'];
            node['STATUS'] = defectsList[j]['STATUS'];
            node['COMPONENT'] = defectsList[j]['COMPONENT'];
            break;
          }
        }
        node['id'] = parseInt(nn[i]).toString();
        nodes.push(node);
        link['source'] = defectId.toString();
        link['target'] = nn[i].toString();
        link['value'] = 1;
        links.push(link);
      }
    }
    graph['links'] = links;
    graph['nodes'] = nodes;
    return graph;
  }

  loadForceDirectedGraph() {
    var svg = d3.select('#d3_selectable_force_directed_graph');
    let data = {
      tenantId: '54b6b87c-41cd-4a21-8dde-62818624678b',
      projectId: '0f76004b-f131-409c-b830-781665f1124c',
      subProjectId: '6b79c2a0-2ef2-48fa-b31f-02b8cc98d62a',
      systemId: '4540ddf2-eed3-4dbe-bf7c-34c17c5d464d',
      catalogId: 'e0603d97-851e-4cf4-a0fb-ab13848ba0c8',
      hashId: this.hashId
    };

    this.reportService.getDefectsGraphForPrediction(data).subscribe((data: any) => {
      this.forceDirectedGraphOriginal = data;
      this.forceDirectedGraph_resp = JSON.parse(JSON.stringify(this.filterData()));
      this.createV4SelectableForceDirectedGraph(svg, this.forceDirectedGraph_resp);
    });
  }

  filterData() {
    let defectId = this.report.DEFECTID.toString();
    const ids: Array<string> = [];
    const usedIds: Array<string> = [];
    const data = {
      links: [],
      nodes: []
    };

    let index = 1;

    var arr_diff = (a1, a2) => {
      var a = [],
        diff = [];

      for (var i = 0; i < a1.length; i++) {
        a[a1[i]] = true;
      }

      for (var i = 0; i < a2.length; i++) {
        if (a[a2[i]]) {
          delete a[a2[i]];
        } else {
          a[a2[i]] = true;
        }
      }

      for (var k in a) {
        diff.push(k);
      }

      return diff;
    };

    var getNextDefectId = () => {
      let a = arr_diff(ids, usedIds);
      if (a.length > 0) {
        return a[0];
      }
    };

    var childNodes = () => {
      this.forceDirectedGraphOriginal.links.forEach((x: any) => {
        if (x.source == defectId || x.target == defectId) {
          data.links.push({
            source: x.source,
            target: x.target,
            value: x.value
          });

          let sourceExists = false;
          let targetExists = false;
          ids.forEach(z => {
            if (z == x.source) sourceExists = true;

            if (z == x.target) targetExists = true;
          });

          if (!sourceExists) ids.push(x.source);
          if (!targetExists) ids.push(x.target);
        }
      });

      if (index < parseInt(this.childNodes)) {
        index += 1;
        usedIds.push(defectId);
        defectId = getNextDefectId();
        childNodes();
      }
    };

    childNodes();

    this.forceDirectedGraphOriginal.nodes.forEach((x: any) => {
      let exists = ids.find(z => z == x.id) != null;
      if (exists) data.nodes.push(x);
    });

    return data;
  }

  createV4SelectableForceDirectedGraph(svg: any, graph: any): any {
    var tooltipDiv = d3
      .select('#d3_selectable_force_directed_graph')
      .append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);

    let parentWidth = (d3.select('svg').node() as any).parentNode.clientWidth;
    let parentHeight = (d3.select('svg').node() as any).parentNode.clientHeight;

    var svg: any = d3
      .select('svg')
      .attr('width', parentWidth)
      .attr('height', parentHeight);

    // remove any previous graphs
    svg.selectAll('.g-main').remove();

    var gMain = svg.append('g').classed('g-main', true);

    var rect = gMain
      .append('rect')
      .attr('class', 'brushRect')
      .attr('width', parentWidth)
      .attr('height', parentHeight)
      .style('fill', 'white');

    var gDraw = gMain.append('g');

    var zoom = d3.zoom().on('zoom', zoomed);

    gMain.call(zoom);

    function zoomed() {
      gDraw.attr('transform', d3.event.transform);
    }

    var nodes = {};
    var i;
    for (i = 0; i < graph.nodes.length; i++) {
      nodes[graph.nodes[i].id] = graph.nodes[i];
      graph.nodes[i].weight = 1.01;
    }

    var link = gDraw
      .append('g')
      .attr('class', 'link')
      .selectAll('line')
      .data(graph.links)
      .enter()
      .append('line')
      .attr('class', 'graph-edges')
      .attr('stroke', 'gray')
      .attr('stroke-width', function(d) {
        return 1;
      });

    var node = gDraw
      .append('g')
      .attr('class', 'node')
      .selectAll('circle')
      .data(graph.nodes)
      .enter()
      .append('circle')
      .attr('r', d => {
        if (this.report.DEFECTID.toString() == d.id) {
          return 13;
        } else {
          return 5;
        }
      })
      .attr('fill', d => {
        if (this.report.DEFECTID.toString() == d.id) {
          return 'red';
        } else {
          return 'gray';
        }
      })
      // .attr("fx", function (d) { return d.x; })
      // .attr("fy", function (d) { return d.y; })

      // .on('click', (d) => {

      // })
      .on('mouseover', (d: any) => {
        d3.event.preventDefault();
        tooltipDiv
          .transition()
          .duration(200)
          .style('opacity', 0.9);
        tooltipDiv
          .html(
            '<strong>DefectId: </strong>' +
              d.id +
              '</br>' +
              '<strong>Name: </strong>' +
              d.NAME +
              '</br>' +
              '<strong>Root cause: </strong>' +
              d.TYPE +
              '</br>' +
              '<strong>Severity: </strong>' +
              d.SEVERITY +
              '</br>' +
              '<strong>Priority: </strong>' +
              d.PRIORITY +
              '</br>' +
              '<strong>Status: </strong>' +
              d.STATUS +
              '</br>' +
              '<strong>Business Area: </strong>' +
              d.COMPONENT //+
              // '</br>' +
              // "<button class='btn btn-sm btn-block btn-primary' id='viewMoreButton' data-id='" +
              // d.id +
              // "'>View More</button>"
          )
          .style('left', '0px')
          .style('top', '0px');

        // .style("left", (d3.event.pageX - tooltipDiv.node().offsetWidth + 50) + "px")
        // .style("top", (d3.event.pageY - tooltipDiv.node().offsetHeight + 50) + "px");
      })

      .on('mouseout', function(d: any) {
        tooltipDiv
          .transition()
          .duration(500)
          .style('opacity', 0);
      });

    var simulation = d3
      .forceSimulation()
      .force(
        'link',
        d3
          .forceLink()
          .id(function(d: any) {
            return d.id;
          })
          .distance(function(d: any) {
            return 30;
          })
      )
      .force('charge', d3.forceManyBody())
      .force('charge', d3.forceManyBody())
      .force('center', d3.forceCenter(parentWidth / 2, parentHeight / 2));
    // .alphaDecay(0.003)

    simulation.nodes(graph.nodes).on('tick', ticked);

    (simulation.force('link') as any).links(graph.links);

    function ticked() {
      // update node and line positions at every step of
      // the force simulation
      link
        .attr('x1', function(d) {
          return d.source.x;
        })
        .attr('y1', function(d) {
          return d.source.y;
        })
        .attr('x2', function(d) {
          return d.target.x;
        })
        .attr('y2', function(d) {
          return d.target.y;
        });

      node
        .attr('cx', function(d) {
          return d.x;
        })
        .attr('cy', function(d) {
          return d.y;
        });
    }

    rect.on('click', () => {
      node.each(function(d) {
        d.selected = false;
        d.previouslySelected = false;
      });
      node.classed('selected', false);
    });

    var texts = ['Use the scroll wheel to zoom', 'Hold the shift key to select nodes'];

    svg
      .selectAll('text')
      .data(texts)
      .enter()
      .append('text')
      .attr('x', 900)
      .attr('y', function(d, i) {
        return 470 + i * 18;
      })
      .text(function(d) {
        return d;
      });
    return graph;
  }

  limeJustification_TableCreation(hashId, reportId) {
    this.reportService.getLimeJustification(hashId, reportId).subscribe((receivedData: any) => {
      this.lime_justification = receivedData;
      document.getElementById('processing_span1').style.display = 'none';
      for (var i = 0; i < this.lime_justification.length; i++) {
        this.lime_justification[i][1] = Math.abs(this.lime_justification[i][1]);
      }
    });
  }
}
