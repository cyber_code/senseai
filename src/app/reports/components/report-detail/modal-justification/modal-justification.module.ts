import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule }   from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DataTablesModule,
    NgbModule,
    FormsModule
  ]
})
export class ModalJustificationModule { }
