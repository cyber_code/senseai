import { Component, OnInit, Input } from '@angular/core';
import { Report } from '../../report';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ReportService } from '../../services/report.service';
import { ModalJustificationComponent } from './modal-justification/modal-justification.component';
import { ModalCorrectiveActionPlanComponent } from './modal-corrective-action-plan/modal-corrective-action-plan.component';
import { SessionService } from 'src/app/core/services/session.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.css']
})
export class ReportDetailComponent implements OnInit {
  @Input() report: Report;
  hashId = this.sessionService.getHashId();
  dtOptions: DataTables.Settings = {};
  modalReference: any;
  childNodes: number = 1;

  constructor(
    private sessionService: SessionService,
    private route: ActivatedRoute,
    private reportService: ReportService,
    private location: Location,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.getReportById(this.hashId);
  }

  getReportById(hashId: string): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.reportService.getReportById(id, hashId).subscribe((data: Report) => {
      data[0]['PRIORITYPREDICTION'] = data[0]['PRIORITYPREDICTION'] != null ? JSON.parse(JSON.parse(data[0]['PRIORITYPREDICTION'])) : null;
      data[0]['SEVERITYPREDICTION'] = data[0]['SEVERITYPREDICTION'] != null ? JSON.parse(JSON.parse(data[0]['SEVERITYPREDICTION'])) : null;
      data[0]['TYPEPREDICTION'] = data[0]['TYPEPREDICTION'] != null ? JSON.parse(JSON.parse(data[0]['TYPEPREDICTION'])) : null;
      data[0]['TIMETOFIXPREDICTION'] = data[0]['TIMETOFIXPREDICTION'] != null ? parseInt(data[0]['TIMETOFIXPREDICTION']) : null;
      data[0]['TARGETDATE'] = data[0]['TARGETDATE'] != null ? this.convertTimestampToDate(data[0]['TARGETDATE']) : null;
      data[0]['TARGETDATEPREDICTION'] =
        data[0]['TARGETDATEPREDICTION'] != null ? this.convertTimestampToDate(data[0]['TARGETDATEPREDICTION']) : null;
      data[0]['STARTDATE'] = data[0]['STARTDATE'] != null ? this.convertTimestampToDate(data[0]['STARTDATE']) : null;
      data[0]['MODIFIEDDATE'] = data[0]['MODIFIEDDATE'] != null ? this.convertTimestampToDate(data[0]['MODIFIEDDATE']) : null;
      data[0]['ENRICHEDNAME'] = data[0]['ENRICHEDNAME'] != null ? data[0]['ENRICHEDNAME'] : data[0]['NAME'];
      this.report = data[0];
    });
  }

  goBack(): void {
    this.location.back();
  }

  openJustificationModal(type: number): void {
    this.modalReference = this.modalService.open(ModalJustificationComponent, {
      size: 'lg',
      centered: true
    });
    this.modalReference.componentInstance.hashId = this.hashId;
    this.modalReference.componentInstance.report = this.report;
    this.modalReference.componentInstance.modalType = type;
    this.modalReference.componentInstance.childNodes = this.childNodes;
    this.modalReference.result.then((result: any) => {}, (reason: any) => {});
  }

  openCorrectiveActionPlanModal(type: number): void {
    this.modalReference = this.modalService.open(ModalCorrectiveActionPlanComponent, {
      size: 'lg',
      centered: true
    });
    this.modalReference.componentInstance.hashId = this.hashId;
    this.modalReference.componentInstance.report = this.report;
    this.modalReference.componentInstance.modalType = type;
    this.modalReference.componentInstance.childNodes = this.childNodes;
    this.modalReference.result.then((result: any) => {}, (reason: any) => {});
  }

  private convertTimestampToDate(timestamp: any) {
    var date = new Date(parseInt(timestamp)).toLocaleString();
    return date;
  }
}
