import { Component, OnInit, ViewChild } from '@angular/core';
import { MlLogService } from '../../services/ml-log.service';
import { SessionService } from 'src/app/core/services/session.service';
import { MessageType } from 'src/app/core/models/message.model';
import { MessageService } from '../../../core/services/message.service';
import { AlertComponent } from '../../../core/components/alert/alert.component';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Router } from '@angular/router';
import {ComboBoxComponent} from '@progress/kendo-angular-dropdowns'; 
import { DesignService } from 'src/app/design/services/design.service';
import { SubProject } from 'src/app/core/models/sub-project.model';
import { isNullOrUndefined } from 'util';
import { Workflow } from 'src/app/design/models/workflow.model';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-ml-logs',
  templateUrl: './ml-logs.component.html',
  styleUrls: ['./ml-logs.component.css']
})
export class MlLogsComponent implements OnInit {
  @ViewChild(AlertComponent) alert: AlertComponent;
  files: any = [];
  @ViewChild('cmbWorkflows') cmbWorkflows: ComboBoxComponent;
  private _subProject: SubProject;
  public workflowId: string = "";
  public workflowList: Workflow[];

  constructor(
    private mlLogService: MlLogService,
    private session: SessionService,
    private messageService: MessageService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private designService: DesignService,
    public permissionService: PermissionService,
  ) {
  }

  ngOnInit() {}

  searchWorkflows(event){ 
    this.designService.searchWorkflows(event, this.getSubProject().id).subscribe(res => {
      this.workflowList = res;  
    });
  }

  selectionChange(value) {
    try {
      this.workflowId = value.id;
    } catch (error) {
      this.messageService.sendMessage({ text: 'Please,  select workflow!', type: MessageType.Warning});
    } 
  }
  executeMlLog() {
    if(this.workflowId.length == 0){
      this.messageService.sendMessage({text: 'Please, select workflow!', type: MessageType.Warning});
      return;
    }
    if (this.files.length === 0) {
        this.messageService.sendMessage({ text: 'Please, select at least one file to upload!', type: MessageType.Warning });
        return;
    }
    this.mlLogService.executeMlLogs({
      tenantId: this.session.getUser().tenantId,
      projectId: this.session.getProject().id,
      subProjectId: this.session.getSubProject().id,
      systemId: this.session.getDefaultSettings().system.id,
      catalogId: this.session.getDefaultSettings().catalog.id,
      files: this.files,
      workflowId: this.workflowId
    }).subscribe(
      res => {},
      err => {
        if (err.status === 401) {
          this.authenticationService.logout();
          this.router.navigateByUrl('/login');
          return;
        }
        this.alert.displayAlert('Error!', 'There was an error while uploading the files!', 'error');
      }
    );
  }

  onDeleteClicked(fileNameToDelete) {
    this.files = this.files.filter(file => file.fileName !== fileNameToDelete);
  }

  private getSubProject(): SubProject {
    if (isNullOrUndefined(this._subProject)) {
      this._subProject = this.session.getWorkContext().subProject;
    }
    return this._subProject;
  }

  reset() {
    this.files = [];
    this.workflowId = "";
    this.workflowList = []; 
    this.cmbWorkflows.reset(); 
  }

  onFileSelected(event) {
    let fileList = event.target.files;
    Object.keys(fileList).forEach(key => {
      let duplicateFiles = this.files.filter(
        file => file.name === fileList[key].name
      );
      if (duplicateFiles.length > 0) {
        fileList[key].fileName =
          fileList[key].name + ' (' + duplicateFiles.length + ')';
      } else {
        fileList[key].fileName = fileList[key].name;
      }
      this.files.push(fileList[key]);
    });
    event.target.value = '';
  }
}
