import {Component, OnInit, Inject, Input} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {DesignService} from '../../../design/services/design.service';
import {State, process} from '@progress/kendo-data-query';
import { isNullOrUndefined } from 'util';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ResourcesService } from '../../services/resources.service';
import { SessionService } from 'src/app/core/services/session.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { Guid } from 'src/app/shared/guid';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css']
})

export class ResourcesComponent implements OnInit {
  public gridData: any = [];
  public formGroup: FormGroup;
  private editedRowIndex: number;
  private hashId = this.sessionService.getHashId();
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor

    filter: {
      logic: 'and',
      filters: []
    }
  };
  public resources = [];
  public deleteResourceWindowOpen = false;
  public selectedResource;
  public datesSelected:NgbDateStruct[] = []; 
  public itemToShowCalendar;
  public newResource;
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    selectAllText: 'Select All',
    unSelectAllText: 'Unselect All',
    allowSearchFilter: true
  };
  public countryList = [
    "Afghanistan",
    "Albania",
    "Algeria",
    "American Samoa",
    "Andorra",
    "Angola",
    "Anguilla",
    "Antarctica",
    "Antigua and Barbuda",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahamas (the)",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belgium",
    "Belize",
    "Benin",
    "Bermuda",
    "Bhutan",
    "Bolivia (Plurinational State of)",
    "Bonaire, Sint Eustatius and Saba",
    "Bosnia and Herzegovina",
    "Botswana",
    "Bouvet Island",
    "Brazil",
    "British Indian Ocean Territory (the)",
    "Brunei Darussalam",
    "Bulgaria",
    "Burkina Faso",
    "Burundi",
    "Cabo Verde",
    "Cambodia",
    "Cameroon",
    "Canada",
    "Cayman Islands (the)",
    "Central African Republic (the)",
    "Chad",
    "Chile",
    "China",
    "Christmas Island",
    "Cocos (Keeling) Islands (the)",
    "Colombia",
    "Comoros (the)",
    "Congo (the Democratic Republic of the)",
    "Congo (the)",
    "Cook Islands (the)",
    "Costa Rica",
    "Croatia",
    "Cuba",
    "Curaçao",
    "Cyprus",
    "Czechia",
    "Côte d'Ivoire",
    "Denmark",
    "Djibouti",
    "Dominica",
    "Dominican Republic (the)",
    "Ecuador",
    "Egypt",
    "El Salvador",
    "Equatorial Guinea",
    "Eritrea",
    "Estonia",
    "Eswatini",
    "Ethiopia",
    "Falkland Islands (the) [Malvinas]",
    "Faroe Islands (the)",
    "Fiji",
    "Finland",
    "France",
    "French Guiana",
    "French Polynesia",
    "French Southern Territories (the)",
    "Gabon",
    "Gambia (the)",
    "Georgia",
    "Germany",
    "Ghana",
    "Gibraltar",
    "Greece",
    "Greenland",
    "Grenada",
    "Guadeloupe",
    "Guam",
    "Guatemala",
    "Guernsey",
    "Guinea",
    "Guinea-Bissau",
    "Guyana",
    "Haiti",
    "Heard Island and McDonald Islands",
    "Holy See (the)",
    "Honduras",
    "Hong Kong",
    "Hungary",
    "Iceland",
    "India",
    "Indonesia",
    "Iran (Islamic Republic of)",
    "Iraq",
    "Ireland",
    "Isle of Man",
    "Israel",
    "Italy",
    "Jamaica",
    "Japan",
    "Jersey",
    "Jordan",
    "Kazakhstan",
    "Kenya",
    "Kiribati",
    "Korea (the Democratic People's Republic of)",
    "Korea (the Republic of)",
    "Kuwait",
    "Kyrgyzstan",
    "Lao People's Democratic Republic (the)",
    "Latvia",
    "Lebanon",
    "Lesotho",
    "Liberia",
    "Libya",
    "Liechtenstein",
    "Lithuania",
    "Luxembourg",
    "Macao",
    "Madagascar",
    "Malawi",
    "Malaysia",
    "Maldives",
    "Mali",
    "Malta",
    "Marshall Islands (the)",
    "Martinique",
    "Mauritania",
    "Mauritius",
    "Mayotte",
    "Mexico",
    "Micronesia (Federated States of)",
    "Moldova (the Republic of)",
    "Monaco",
    "Mongolia",
    "Montenegro",
    "Montserrat",
    "Morocco",
    "Mozambique",
    "Myanmar",
    "Namibia",
    "Nauru",
    "Nepal",
    "Netherlands (the)",
    "New Caledonia",
    "New Zealand",
    "Nicaragua",
    "Niger (the)",
    "Nigeria",
    "Niue",
    "Norfolk Island",
    "Northern Mariana Islands (the)",
    "Norway",
    "Oman",
    "Pakistan",
    "Palau",
    "Palestine, State of",
    "Panama",
    "Papua New Guinea",
    "Paraguay",
    "Peru",
    "Philippines (the)",
    "Pitcairn",
    "Poland",
    "Portugal",
    "Puerto Rico",
    "Qatar",
    "Republic of North Macedonia",
    "Romania",
    "Russian Federation (the)",
    "Rwanda",
    "Réunion",
    "Saint Barthélemy",
    "Saint Helena, Ascension and Tristan da Cunha",
    "Saint Kitts and Nevis",
    "Saint Lucia",
    "Saint Martin (French part)",
    "Saint Pierre and Miquelon",
    "Saint Vincent and the Grenadines",
    "Samoa",
    "San Marino",
    "Sao Tome and Principe",
    "Saudi Arabia",
    "Senegal",
    "Serbia",
    "Seychelles",
    "Sierra Leone",
    "Singapore",
    "Sint Maarten (Dutch part)",
    "Slovakia",
    "Slovenia",
    "Solomon Islands",
    "Somalia",
    "South Africa",
    "South Georgia and the South Sandwich Islands",
    "South Sudan",
    "Spain",
    "Sri Lanka",
    "Sudan (the)",
    "Suriname",
    "Svalbard and Jan Mayen",
    "Sweden",
    "Switzerland",
    "Syrian Arab Republic",
    "Taiwan (Province of China)",
    "Tajikistan",
    "Tanzania, United Republic of",
    "Thailand",
    "Timor-Leste",
    "Togo",
    "Tokelau",
    "Tonga",
    "Trinidad and Tobago",
    "Tunisia",
    "Turkey",
    "Turkmenistan",
    "Turks and Caicos Islands (the)",
    "Tuvalu",
    "Uganda",
    "Ukraine",
    "United Arab Emirates (the)",
    "United Kingdom of Great Britain and Northern Ireland (the)",
    "United States Minor Outlying Islands (the)",
    "United States of America (the)",
    "Uruguay",
    "Uzbekistan",
    "Vanuatu",
    "Venezuela (Bolivarian Republic of)",
    "Viet Nam",
    "Virgin Islands (British)",
    "Virgin Islands (U.S.)",
    "Wallis and Futuna",
    "Western Sahara",
    "Yemen",
    "Zambia",
    "Zimbabwe",
    "Åland Islands"
  ];
  public filteredCountryList = this.countryList;
  
  public experiences = ['Trainee', 'Novice', 'Junior', 'Intermediate', 'Senior'];
  public filteredExperiences = this.experiences;
  public positions = ['Technical', 'QA'];
  public filteredPositions = this.positions;
  public expertiseFields =  ['Software', 'Configuration', 'Enviromental', 'Cosmetic', 'Database', 'Performance', 'Documentation'];
  public filteredExpertiseFields = this.expertiseFields;
  public skills = ['SQL', 'IIS', 'Test Engine', 'TAFC', 'TAFJ', 'XPath', 'REST', 'SOAP', 'Suggestions', 'SignalR'];
  public filteredSkills = this.skills;
  public businessAreas = [];
  public filteredBusinessAreas = [];

  constructor(
    private resourcesService: ResourcesService, 
    private messageService: MessageService,
    private sessionService: SessionService,
    private processManagementService: ProcessManagementService
  ) {
  }

  async ngOnInit() {
    const subProjectId = this.sessionService.getSubProject().id;
    let businessAreas = await this.processManagementService.getHierarchiesByParent(subProjectId, Guid.empty, Guid.empty).toPromise();
    this.businessAreas = businessAreas.map(businessArea => businessArea.title);
    this.filteredBusinessAreas = this.businessAreas;
    this.loadResources();
  }

  public addHandler({sender}) {
    this.newResource = {
      NAME: '',
      BUSINESS_AREAS: [],
      EXPERIENCE: '',
      EXPERTISE: [],
      SKILLSET: [],
      POSITION: '',
      RATE: 0,
      WORKLOAD: 0,
      DAYS_OFF: [],
      COUNTRY: '',
      SLACK_ID: ''
    };
    this.closeEditor(sender);
    sender.addRow(this.newResource);
  }
  public editHandler({sender, rowIndex, dataItem}) {
    this.closeEditor(sender);
    this.newResource = {...dataItem, BUSINESS_AREAS: eval(dataItem.BUSINESS_AREAS), EXPERTISE: eval(dataItem.EXPERTISE), SKILLSET: eval(dataItem.SKILLSET)};
    this.editedRowIndex = rowIndex;
    sender.editRow(rowIndex, this.newResource);
    setTimeout(() => {
      this.calculateTop(this.newResource);
    }, 150);
    
  }

  public cancelHandler({sender, rowIndex}) {
    this.itemToShowCalendar = null;
    if (this.resources && this.resources[rowIndex])
      this.resources[rowIndex].datesSelected = eval(this.resources[rowIndex].DAYS_OFF).map(date => ({month: new Date(date).getMonth() + 1, 
            day: new Date(date).getDate(), year: new Date(date).getFullYear()}))
    this.closeEditor(sender, rowIndex);
    this.newResource = null;
  }

  public saveHandler({sender, rowIndex, isNew}): void {
    let arr = this.resources.map(resource => resource.DEV_ID)
    let maxId = ( arr && arr.length > 0) ? Math.max.apply(null, arr) : 0;
    this.newResource.datesSelected = this.newResource.datesSelected ? this.newResource.datesSelected : [];
    let daysOff = this.newResource.datesSelected.map(date => new Date(date.year, date.month - 1, date.day));
    let experienceLevels = [
      {
          "ID": "1",
          "Value": "Trainee"
      },
      {
          "ID": "2",
          "Value": "Novice"
      },
      {
          "ID": "3",
          "Value": "Junior"
      },
      {
          "ID": "4",
          "Value": "Intermediate"
      },
      {
          "ID": "5",
          "Value": "Senior"
      }
    ];
    let developer = {
      "Experience Levels": experienceLevels,
      "Developer": {
        "Id": isNew ? maxId + 1 : this.newResource.DEV_ID,
        "Name": this.newResource.NAME,
        "General Skillset": this.newResource.SKILLSET,
        "Business Areas": this.newResource.BUSINESS_AREAS,
        "Types of Expertise": this.newResource.EXPERTISE,
        "Experience": experienceLevels.find(level => level.Value === this.newResource.EXPERIENCE).ID,
        "Country": this.newResource.COUNTRY,
        "Rate": this.newResource.RATE,
        "Slack ID": this.newResource.SLACK_ID,
        "DaysOff": daysOff,
        "Role": this.newResource.POSITION
      }
    };
    if (!isNew) {
      this.updateResource(developer, sender, rowIndex);
    } else {
      this.addResource(developer, sender, rowIndex);
    }
  }

  async addResource(developer, sender, rowIndex) {
    let res = await this.resourcesService.addDeveloper(this.hashId, developer).toPromise();
    if (res) {
      this.loadResources();
      sender.closeRow(rowIndex);
      this.newResource = null;
      this.itemToShowCalendar = null;
      this.messageService.sendMessage({text: 'Resource was added sucessfully!', type: MessageType.Success});
    } else {
      this.messageService.sendMessage({text: 'There was an error while adding the resource!', type: MessageType.Error});
      this.newResource = null;
      this.itemToShowCalendar = null;
      sender.closeRow(rowIndex);
    }
  }

  async updateResource(developer, sender, rowIndex) {
    let res = await this.resourcesService.updateDeveloper(this.hashId, developer).toPromise();
    if (res) {
      this.loadResources();
      sender.closeRow(rowIndex);
      this.newResource = null;
      this.itemToShowCalendar = null;
      this.messageService.sendMessage({text: 'Resource was updated sucessfully!', type: MessageType.Success});
    } else {
      this.messageService.sendMessage({text: 'There was an error during the update of the resource!', type: MessageType.Error});
      this.newResource = null;
      this.itemToShowCalendar = null;
      sender.closeRow(rowIndex);
    }
  }

  async loadResources() {
    let developers = await this.resourcesService.getAllDevelopers(this.hashId).toPromise();
    let qas = await this.resourcesService.getAllQAs(this.hashId).toPromise();
    let resources = developers.concat(qas);
    this.resources = resources.map(resource => {
      return {
        ...resource, 
        businessAreas: eval(resource.BUSINESS_AREAS).join(', '),
        expertise: eval(resource.EXPERTISE).join(', '),
        skillset: eval(resource.SKILLSET).join(', '),
        datesSelected: eval(resource.DAYS_OFF).map(date => ({month: new Date(date).getMonth() + 1, 
          day: new Date(date).getDate(), year: new Date(date).getFullYear()}))
      };
    });
    this.gridData = process(this.resources, this.state);
  }

  public removeHandler({dataItem}): void {
    this.selectedResource = dataItem;
    this.deleteResourceWindowOpen = true;
  }

  get disableAddBtn() {
    return (
      !this.newResource ||
      this.newResource.NAME === '' ||
      !this.newResource.EXPERIENCE || 
      this.newResource.EXPERIENCE === '' ||
      !this.newResource.POSITION ||
      this.newResource.POSITION === ''
    );
  }

  removeResource(deletedResource) {
    let resources = this.resources.filter(attribute => attribute.DEV_ID !== deletedResource.DEV_ID);
    this.resources = resources;
    this.gridData = process(resources, this.state);
    this.closeDeleteResource();
  }

  closeDeleteResource() {
    this.deleteResourceWindowOpen = false;
    this.selectedResource = null;
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  dataStateChange(state) {
    this.state = state;
    this.gridData = process(this.resources, this.state);
  }

  listChange(resources) {
    this.resources = resources;
    this.gridData = process(resources, this.state);
  }

  handleCountryChange(search) {
    this.filteredCountryList = this.countryList.filter(country => country.toLowerCase().includes(search.toLowerCase()));
  }

  handlePositionChange(search) {
    this.filteredPositions = this.positions.filter(position => position.toLowerCase().includes(search.toLowerCase()));
  }

  handleSkillsetChange(search) {
    this.filteredSkills = this.skills.filter(skill => skill.toLowerCase().includes(search.toLowerCase()));
  }

  handleExpertiseChange(search) {
    this.filteredExpertiseFields = this.expertiseFields.filter(expertise => expertise.toLowerCase().includes(search.toLowerCase()));
  }

  handleBusinessAreasChange(search) {
    this.filteredBusinessAreas = this.businessAreas.filter(businessArea => businessArea.toLowerCase().includes(search.toLowerCase()));
  }

  handleExperienceChange(search) {
    this.filteredExperiences = this.experiences.filter(experience => experience.toLowerCase().includes(search.toLowerCase()));
  }

  change(value:NgbDateStruct[])
  {
    this.datesSelected = value;
    this.itemToShowCalendar.datesSelected = value;
    if (this.newResource && this.newResource.DEV_ID === this.itemToShowCalendar.DEV_ID)
      this.newResource.datesSelected = value;
  }

  toggleCalendar(item) {
    this.calculateTop(item);
    
    if (this.itemToShowCalendar) {
      if (item.DEV_ID === this.itemToShowCalendar.DEV_ID)
        this.itemToShowCalendar = null;
      else {
        this.itemToShowCalendar = item;
        this.datesSelected = item.datesSelected;
      }
    } else {
      this.itemToShowCalendar = item;
      this.datesSelected = item.datesSelected;
    }
  }

  calculateTop(item) {
    let bodyRect = document.body.getBoundingClientRect();
    let element = document.getElementById(item.DEV_ID);
    let offset   = element.getBoundingClientRect().top - bodyRect.top;
    let datePicker = document.getElementsByTagName('ngb-datepicker')[0] as HTMLElement;
    datePicker.style.top = offset - 20 + 'px';
  }

}
