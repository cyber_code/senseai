import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ResourcesService } from 'src/app/reports/services/resources.service';
import { SessionService } from 'src/app/core/services/session.service';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';

@Component({
  selector: 'app-delete-resource',
  templateUrl: './delete-resource.component.html',
  styleUrls: ['./delete-resource.component.css']
})
export class DeleteResourceComponent implements OnInit {
  @Input()
  public deleteResourceWindowOpen;

  @Input() 
  public selectedResource;

  @Output()
  public closeDeleteResourceWindow = new EventEmitter();

  @Output()
  public removeResourceFromList = new EventEmitter();

  private hashId = this.sessionService.getHashId();

  constructor(
    private resourcesService: ResourcesService, 
    private sessionService: SessionService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
  }

  deleteResource() {
    this.resourcesService.deleteDeveloper(this.hashId, this.selectedResource.DEV_ID).subscribe(res => {
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({text: 'Resource was deleted successfully!', type: MessageType.Success});
      this.removeResourceFromList.emit(this.selectedResource);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeleteResourceWindow.emit();
  }

}
