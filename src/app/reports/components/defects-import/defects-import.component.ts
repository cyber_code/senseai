import {Component, OnInit} from '@angular/core';
import {Papa} from 'ngx-papaparse';
import {ReportService} from '../../services/report.service';
import {DataExtractionServiceService} from '../../services/data-extraction-service.service';
import {SessionService} from 'src/app/core/services/session.service';
import {MessageService} from 'src/app/core/services/message.service';
import {MessageType} from 'src/app/core/models/message.model';
import {ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Subscription, of} from 'rxjs';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Router } from '@angular/router';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-defects-import',
  templateUrl: './defects-import.component.html',
  styleUrls: ['./defects-import.component.css']
})
export class DefectsImportComponent implements OnInit {
  selectedFile: any;
  csvHeaders: any;
  uploadPercentage = 0;
  validataHeaders = [];
  selectionHolder = {};
  mappingDetails;
  selectedValues = [];
  responseStatus = '';
  form: FormGroup;
  loading = false;
  optionLanguage = ['En', 'Es', 'Fr', 'Ru', 'Pt'];
  selectedLang = 'En';
  saveDefectsSubscription: Subscription;
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(
    public http: HttpClient,
    public papa: Papa,
    public reportService: ReportService,
    private dataExtractionService: DataExtractionServiceService,
    private sessionService: SessionService,
    public messageService: MessageService,
    private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router,
    public permissionService: PermissionService,
  ) {
    this.createForm();
    this.permissionService.hasPermission("/api/Insight/GetDefectsColumnMapping") ?
    this.reportService.getDefectsColumnMapping().subscribe(res => {
      if (res && res.hasResult && res.successful) {
        this.validataHeaders =  res.result;
        this.validataHeaders.forEach(x => {
          this.selectionHolder[x] = 'Empty';
        });
      } else if (res && res.error && res.error.exceptionObject && res.error.exceptionObject.Message) {
        this.messageService.sendMessage({type: MessageType.Error, text: res.error.exceptionObject.Message});
        return;
      }
    },
    err => {
      if (err.status === 401) {
        this.authenticationService.logout();
        this.router.navigateByUrl('/login');
      }
    }) : null;
  }

  ngOnInit() {
  }

  createForm() {
    this.form = this.fb.group({
      TenantId: '',
      ProjectId: '',
      SubProjectId: '',
      SystemId: '',
      CatalogId: '',
      Mapping: [],
      Files: null
    });
  }

  cancel() {
    this.saveDefectsSubscription.unsubscribe();
    this.clearFile();
  }

  onFileChange(event) {
    if (event.target.files[0].name.search('csv') === -1) {
      this.messageService.sendMessage({text: 'Please select CSV file', type: MessageType.Warning});
      this.clearFile();
      return;
    } else {
      this.csvHeaders = undefined;
      this.responseStatus = '';
      this.uploadPercentage = 0;
      this.loading = false;
      const file = event.target.files[0];
      this.form.get('Files').setValue(file);
      this.selectedFile = file.name;
      this.transform(file);
      this.validataHeaders.forEach(x => {
        this.selectionHolder[x] = 'Empty';
      });
    }

  }

  private prepareSave(): any {
    const input = new FormData();
    const workcontext = this.sessionService.getWorkContext();
    input.append('TenantId', workcontext.user.tenantId);
    input.append('ProjectId', workcontext.project.id);
    input.append('SubProjectId', workcontext.subProject.id);
    input.append('SystemId', workcontext.defaultSettings.system.id);
    input.append('CatalogId', workcontext.defaultSettings.catalog.id);
    input.append('Mapping', this.mappingDetails);
    input.append('Files', this.form.get('Files').value);
    input.append('Lang', this.selectedLang);
    return input;
  }

  onSubmit() {
    if (!this.selectedFile) {
      this.messageService.sendMessage({text: 'Please,select a file to upload', type: MessageType.Warning});
      return;
    }
    
    this.finalMapping();
    let emptyFields = JSON.parse(this.mappingDetails[0]).filter(detail => detail.CsvField === 'Empty');
    if (emptyFields.length > 0) {
      let fieldNames = emptyFields.map(field => field.OriginalField);
      fieldNames = fieldNames.join(', ');
      this.messageService.sendMessage({text: 'Please, map ' + fieldNames + ' fields!', type: MessageType.Warning});
      return;
    }
    const formModel = this.prepareSave();
    this.loading = true;
    this.responseStatus = '';
    this.uploadPercentage = 0;
    this.saveDefectsSubscription = this.dataExtractionService
      .saveDefects(formModel)
      .pipe(
        catchError(err => {
          this.errorCatched(err);
          return of(null);
        })
      )
      .subscribe(event => {
        if (event.type === 1) {
          this.uploadPercentage = Math.round((event.loaded / event.total) * 100);
        } else if (event.type === 4) {
          this.loading = false;
          if (event.status === 200) {
            if (event.body.successful) {
              this.responseStatus = 'true';
              this.messageService.sendMessage({text: 'File upload successful', type: MessageType.Info});
            } else {
              this.responseStatus = 'false';
              this.messageService.sendMessage({text: event.body.response.message, type: MessageType.Error});
            }
            this.loading = false;
          }
        }
      });
  }

  errorCatched(err) {
    if (err.status === 401) {
      this.authenticationService.logout();
      this.router.navigateByUrl('/login');
      return;
    }
    this.messageService.sendMessage({text: 'Uploaded failed', type: MessageType.Error});
    this.loading = false;
    this.responseStatus = 'false';
  }

  clearFile() {
    const file = undefined;
    this.form.get('Files').setValue(null);
    this.fileInput.nativeElement.value = '';
    this.selectedFile = file;
    this.csvHeaders = undefined;
    this.responseStatus = '';
    this.uploadPercentage = 0;
    this.loading = false;
  }

  transform(csv) {
    const csvData = csv;
    const options = {
      complete: results => {
        this.csvHeaders = results.meta.fields;
      },
      delimiter: ',',
      header: true
      // Add options here.
    };
    this.papa.parse(csvData, options);
  }

  finalMapping() {
    this.selectedValues = [];
    this.mappingDetails = [];
    const csvNames = [];
    for (const prop in this.selectionHolder) {
      if (prop) {
        csvNames.push(prop);
        this.selectedValues.push(this.selectionHolder[prop]);
      }
    }
    let i = 0;
    this.validataHeaders.forEach(x => {
      const mappingModel = {OriginalField: '', CsvField: '', Index: ''};
      mappingModel.OriginalField = x;
      mappingModel.CsvField = this.selectedValues[i];
      const index = this.csvHeaders.indexOf(this.selectedValues[i]);
      mappingModel.Index = index;
      this.mappingDetails.push(mappingModel);
      i++;
    });
    this.mappingDetails = [JSON.stringify(this.mappingDetails)];
  }

  setProccesBar() {
    const styles = {
      width: this.uploadPercentage + '%'
    };
    return styles;
  }
}
