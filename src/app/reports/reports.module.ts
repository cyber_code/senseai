import { DefectsImportPopupComponent } from './components/defects-import/defects-import-popup/defects-import-popup.component';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChartsModule} from 'ng2-charts';
import {NgxBootstrapSliderModule} from 'ngx-bootstrap-slider';

import {ReportsRoutingModule} from './reports-routing.module';
import {DefectsReportsComponent} from './components/defects-reports/defects-reports.component';
import {ReportDetailComponent} from './components/report-detail/report-detail.component';
import {DataTablesModule} from 'angular-datatables';
import {ReportsMenuComponent} from './components/reports-menu/reports-menu.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReportsFilterComponent} from './components/reports-filter/reports-filter.component';
import {ModalJustificationComponent} from './components/report-detail/modal-justification/modal-justification.component';
import {ModalJustificationModule} from './components/report-detail//modal-justification/modal-justification.module';
import {Ng5SliderModule} from 'ng5-slider';
import 'chartjs-plugin-zoom';
import {DefectsImportComponent} from './components/defects-import/defects-import.component';
import {PapaParseModule} from 'ngx-papaparse';
import {MlLogsComponent} from './components/ml-logs/ml-logs.component';
import {CoreModule} from '../core/core.module';
import {ModalCorrectiveActionPlanComponent} from './components/report-detail/modal-corrective-action-plan/modal-corrective-action-plan.component';
import {ModalCorrectiveActionPlanModule} from './components/report-detail/modal-corrective-action-plan/modal-corrective-action-plan.module';
import {DataExtractionServiceService} from './services/data-extraction-service.service';
import { DevelopersWorkloadComponent } from './components/developers-workload/developers-workload.component';
import { ModalOverdueTasksComponent } from './components/developers-workload/modal-overdue-tasks/modal-overdue-tasks.component';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { TabsModule } from 'ngx-bootstrap';
import { SelectDropDownModule } from 'ngx-select-dropdown'
import { NgProgressModule } from 'ngx-progressbar';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { MatTableModule } from '@angular/material'
import { WindowModule, DialogModule } from '@progress/kendo-angular-dialog';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {PopupModule} from '@progress/kendo-angular-popup';


import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { DefectReportsPopupComponent } from './components/defects-reports/defect-reports-popup/defect-reports-popup.component';
import { MlLogsPopupComponent } from './components/ml-logs/ml-logs-popup/ml-logs-popup.component';
import { DevelopersWorkloadPopupComponent } from './components/developers-workload/developers-workload-popup/developers-workload-popup.component';
import { ResourcesComponent } from './components/resources/resources.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { DeleteResourceComponent } from './components/resources/components/delete-resource/delete-resource.component';
import { MultiDatePickerComponent } from './components/resources/components/multi-date-picker/multi-date-picker.component';

@NgModule({
  declarations: [
    DefectsReportsComponent,
    ReportDetailComponent,
    ReportsMenuComponent,
    ReportsFilterComponent,
    ModalJustificationComponent,
    DefectsImportComponent,
    MlLogsComponent,
    ModalCorrectiveActionPlanComponent,
    DevelopersWorkloadComponent,
    ModalOverdueTasksComponent,
    DefectReportsPopupComponent,
    DefectsImportPopupComponent,
    MlLogsPopupComponent,
    DevelopersWorkloadPopupComponent,
    ResourcesComponent,
    DeleteResourceComponent,
    MultiDatePickerComponent
  ],
  imports: [
    GridModule,
    DialogModule,
    WindowModule,
    PapaParseModule,
    CommonModule,
    CoreModule,
    ReportsRoutingModule,
    DataTablesModule,
    NgbModule,
    FormsModule,
    ChartsModule,
    NgxBootstrapSliderModule,
    ModalJustificationModule,
    ModalCorrectiveActionPlanModule,
    Ng5SliderModule,
    ReactiveFormsModule,
    TreeViewModule,
    DropDownsModule,
    TabsModule.forRoot(),
    SelectDropDownModule,
    NgProgressModule,
    LoadingBarModule,
    MatTableModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    NgMultiSelectDropDownModule,
    PopupModule
  ],
  providers: [DataExtractionServiceService],
  exports: [MatTableModule],
  entryComponents: [ModalJustificationComponent, ModalCorrectiveActionPlanComponent, ModalOverdueTasksComponent]
})
export class ReportsModule {
}
