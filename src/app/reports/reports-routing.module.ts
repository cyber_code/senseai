import {MlLogsComponent} from './components/ml-logs/ml-logs.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DefectsReportsComponent} from './components/defects-reports/defects-reports.component';
import {ReportDetailComponent} from './components/report-detail/report-detail.component';
import {DefectsImportComponent} from './components/defects-import/defects-import.component';
import { DevelopersWorkloadComponent } from './components/developers-workload/developers-workload.component';
import { ResourcesComponent } from './components/resources/resources.component';

const routes: Routes = [

  {path: 'reports', component: DefectsReportsComponent, pathMatch: 'full'},
  {path: 'defects-reports', component: DefectsReportsComponent},
  {path: 'defects-reports/detail/:id', component: ReportDetailComponent},
  {path: 'defects-import', component: DefectsImportComponent},
  {path: 'ml-logs', component: MlLogsComponent},
  {path: 'developers-workload', component: DevelopersWorkloadComponent},
  {path: 'resources', component: ResourcesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule {
}
