import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../shared/auth.guards';
import { JiraAuthGuard } from '../shared/jira-auth.guard';
import { ProjectSelectionGuard } from '../shared/project-selection.guards';
import { BacklogMainComponent } from './components/backlog-main/components/main/backlog-main.component';
import { HierarchiesComponent } from './components/hierarchy-type/hierarchies/hierarchies.component';
import { ImportArisSettingsComponent } from './components/import-aris-settings/import-aris-settings.component';
import { ImportHierarchyComponent } from './components/import-hierarchy/import-hierarchy.component';
import { IssueTrackingComponent } from './components/issue-tracking/issue-tracking.component';
import { IssueTypesViewComponent } from './components/issue-types-main/issue-types-main/components/issue-types-view/issue-types-view.component';
import { IssuesComponent } from './components/issues/issues.component';
import { JiraIntegrationComponent } from './components/jira-integration/jira-integration.component';
import { LinkProcessWfComponent } from './components/link-process-wf/link-process-wf.component';
import { LinkWfIssueComponent } from './components/link-wf-issue/link-wf-issue.component';
import { PeopleViewComponent } from './components/people/people-view/people-view.component';
import { ProcessHierarchiesComponent } from './components/process-main/components/process-hierarchies/process-hierarchies.component';
import { RolesViewComponent } from './components/role/roles-view/roles-view.component';
import { RequirmentTypeComponent } from './components/settings/components/requirment-type/requirment-type.component';
import { SettingsAppComponent } from './components/settings/components/settings-app/settings-app.component';
import { SettingsComponent } from './components/settings/settings.component';
import { WorkflowIssuesComponent } from './components/workflow-issues/workflow-issues.component';



const routes: Routes = [
  { path: 'issues', component: IssuesComponent, canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]},
  { path: 'settings', component: SettingsComponent , children: [
    { path: '', component: SettingsAppComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
    { path: 'hierarchy-types', component: HierarchiesComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
    { path: 'import-hierarchy', component: ImportHierarchyComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
    { path: 'issue-types', component: IssueTypesViewComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
    {path: 'requirment-type', component: RequirmentTypeComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
    { path: 'roles', component: RolesViewComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
    { path: 'people', component: PeopleViewComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
    { path: 'jira-integration', component: JiraIntegrationComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  ], canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]},
  { path: 'backlog', component: BacklogMainComponent, canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]},
  { path: 'process-main', component: ProcessHierarchiesComponent, canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]},
  { path: 'import-aris-workflow-to-aris-designer-component', component: ImportArisSettingsComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  { path: 'workflow-issues', component: WorkflowIssuesComponent, canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]},
  { path: 'link-issues', component: LinkWfIssueComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  { path: 'link-process-workflows', component: LinkProcessWfComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  { path: 'import-hierarchy', component: ImportHierarchyComponent, canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]},
  { path: 'issue-types', component: IssueTypesViewComponent, canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]},
  {path: 'requirment-type', component: RequirmentTypeComponent, canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]},
  {path: 'issueTracking', component: IssueTrackingComponent, canActivate: [AuthGuard, ProjectSelectionGuard, JiraAuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProcessManagementRoutingModule {}
