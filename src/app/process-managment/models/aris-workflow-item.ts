import {ArisWorkflowItemCustomData} from './aris-workflow-item-custom-data.model';

export class ArisWorkflowItem {
  id: string;
  text: string;
  customData: ArisWorkflowItemCustomData;
  previousWorkflowItemCustomData: ArisWorkflowItemCustomData;

  constructor(
    id: string,
    text: string,
    customData: ArisWorkflowItemCustomData,
    previousWorkflowItemCustomData?: ArisWorkflowItemCustomData
  ) {
    this.id = id;
    this.text = text;
    this.customData = customData;
    if (customData.Title === '') {
      customData.Title = text;
    }
    this.customData.Id = id;
    this.previousWorkflowItemCustomData = previousWorkflowItemCustomData;
  }
}
