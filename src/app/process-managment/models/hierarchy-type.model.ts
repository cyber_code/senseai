export class HierarchyType{
    parentId: string;
    subProjectId: string;
    title: string;
    description: string;
}