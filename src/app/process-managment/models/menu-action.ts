export enum MenuAction {
  Export,
  NewFolder,
  NewProcess,
  NewWorkflow,
  Edit,
  Rename,
  Copy,
  Paste,
  Cut,
  Delete,
  Save,
  Cancel,
  Issue,
  Versions,
  ViewCoverage
}
