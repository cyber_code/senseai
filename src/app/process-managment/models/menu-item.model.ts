import {ArisNodeType} from 'src/app/shared/aris-node-type';
import {MenuAction} from './menu-action';

export class MenuItem {
  types: ArisNodeType[];
  menuAction: MenuAction;
  text: string;
  isActive: boolean;

  constructor(types: ArisNodeType[], menuAction: MenuAction, isActive: boolean) {
    this.types = types;
    this.menuAction = menuAction;
    this.text = MenuAction[menuAction].replace(/([A-Z])/g, ' $1').trim();
    this.isActive = isActive;
  }
}
