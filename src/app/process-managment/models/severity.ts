export enum Severity {
    Low,
    Medium,
    High,
    Immediate
}
  