import {GenericWorkflowItemCustomData} from './generic-workflow-item-custom-data.model';

export class GenericWorkflowItem {
  id: string;
  text: string;
  customData: GenericWorkflowItemCustomData;
  previousWorkflowItemCustomData: GenericWorkflowItemCustomData;

  constructor(
    id: string,
    text: string,
    customData: GenericWorkflowItemCustomData,
    previousWorkflowItemCustomData?: GenericWorkflowItemCustomData
  ) {
    this.id = id;
    this.text = text;
    this.customData = customData;
    if (customData.Title === '') {
      customData.Title = text;
    }
    this.customData.Id = id;
    this.previousWorkflowItemCustomData = previousWorkflowItemCustomData;
  }
}
