export class Role {
  id: string;
  title: string;
  description: string;
}
