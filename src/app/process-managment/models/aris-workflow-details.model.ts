export class ArisWorkflowDetails {
    description: string;
    designerJson: string;
    id: string;
    processId: string;
    title: string;
    isparsed: boolean;
}