import { ArisWorkflowType } from '../../core/models/aris-workflow-item-type';

export class ArisWorkflowItemCustomData {
  Type: ArisWorkflowType; 
  Id: string;
  Title: string;
  Description: string;
  TypicalName: string;
  Screen: string;
  FileType: string;
  X?: number;
  Y?: number;
}
