import { GenericWorkflowType } from '../../core/models/generic-workflow-item-type';

export class GenericWorkflowItemCustomData {
  Type: GenericWorkflowType; 
  Id: string;
  Title: string;
  Description: string;
}
