export class Issue {
    processId: string;
    subProjectId: string;
    issueTypeId: string;
    assignedId: string;
    title: string;
    description: string;
    status: 0;
}