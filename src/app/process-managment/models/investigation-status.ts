export enum InvestigationStatus {
    Identified,
    Accepted,
    Deferred,
    Evaluated
}
  