export class Statistics {
  nrProcesses: number;
  nrWorkflows: number;
  nrTestCases: number;
  nrTestSteps: number;
  nrTotalIssues: number;
  nrOpenIssues: number;
  hierarchyId: string;
  title: string;
}
