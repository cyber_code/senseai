export enum ChangeStatus {
    Submitted,
    Studied,
    Reviewed,
    PMOApproved,
    SecurityReviewed,
    QAReviewed,
    InDevelopment
}
  