export class Menu {
  id: string;
  name: string;
  url: string;
  parentId: string;
}
