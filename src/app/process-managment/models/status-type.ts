export enum StatusType {
    ToDo = 0,
    InProgress = 1,
    Done = 2
}
  