export enum Type {
    Software,
    Cosmetic,
    Performance,
    Database,
    Configuration,
    Environmental,
    Documentation 
}
  