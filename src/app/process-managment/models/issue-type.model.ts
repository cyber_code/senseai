export class IssueType {
    subProjectId: string;
    title: string;
    description: string;
    default: boolean;
}
