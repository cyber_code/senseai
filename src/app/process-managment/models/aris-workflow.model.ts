export class ArisWorkflow {
    id: string;
    title: string;
    description: string;
    processId: string;
}
  