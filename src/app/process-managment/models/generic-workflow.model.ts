export class GenericWorkflow {
    id: string;
    title: string;
    description: string;
    processId: string;
}
  