// tslint:disable:max-line-length
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import { DialogModule, DialogsModule, WindowModule } from '@progress/kendo-angular-dialog';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { ExcelModule, GridModule } from '@progress/kendo-angular-grid';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { MenuModule } from '@progress/kendo-angular-menu';
import { PopupModule } from '@progress/kendo-angular-popup';
import { SortableModule } from '@progress/kendo-angular-sortable';
import { ToolBarModule } from '@progress/kendo-angular-toolbar';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { UploadModule } from '@progress/kendo-angular-upload';
import { ArchwizardModule } from 'angular-archwizard';
import { ResizableModule } from 'angular-resizable-element';
import { AngularDraggableModule } from 'angular2-draggable';
import { NgDragDropModule } from 'ng-drag-drop';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChartsModule } from 'ng2-charts';
import { CoreModule } from '../core/core.module';
import { SessionService } from '../core/services/session.service';
import { TestGenerationModule } from '../test-generation/test-generation.module';
import { DeleteProcessResourceComponent } from './components/aris-workflow/components/delete-resource/delete-resource.component';
import { DesignerDetailsComponent } from './components/aris-workflow/components/designer-details/designer-details.component';
import { DetailsComponent } from './components/aris-workflow/components/details/details.component';
import { LinkProcessWorkflowComponent } from './components/aris-workflow/components/link-process-workflow/link-process-workflow.component';
import { ProcessResourcesComponent } from './components/aris-workflow/components/process-resources/process-resources.component';
import { DescriptionComponent } from './components/aris-workflow/components/properties/controls/description/description.component';
import { FileTypeComponent } from './components/aris-workflow/components/properties/controls/file-type/file-type.component';
import { ProcessComponent } from './components/aris-workflow/components/properties/controls/process/process.component';
import { ScreenComponent } from './components/aris-workflow/components/properties/controls/screen/screen.component';
import { SystemTypeComponent } from './components/aris-workflow/components/properties/controls/system-type/system-type.component';
import { TitleComponent } from './components/aris-workflow/components/properties/controls/title/title.component';
import { TypicalNameComponent } from './components/aris-workflow/components/properties/controls/typical-name/typical-name.component';
import { PropetiesContainerComponent } from './components/aris-workflow/components/properties/propeties-container/propeties-container.component';
import { ActivityItemComponent } from './components/aris-workflow/components/properties/workflow-items/activity-item/activity-item.component';
import { ApplicationSystemTypeItemComponent } from './components/aris-workflow/components/properties/workflow-items/application-system-type-item/application-system-type-item.component';
import { EventItemComponent } from './components/aris-workflow/components/properties/workflow-items/event-item/event-item.component';
import { ProcessItemComponent } from './components/aris-workflow/components/properties/workflow-items/process-item/process-item.component';
import { ResourceItemComponent } from './components/aris-workflow/components/properties/workflow-items/resource-item/resource-item.component';
import { RoleItemComponent } from './components/aris-workflow/components/properties/workflow-items/role-item/role-item.component';
import { ScreenItemComponent } from './components/aris-workflow/components/properties/workflow-items/screen-item/screen-item.component';
import { WorkflowVersionComponent } from './components/aris-workflow/components/properties/workflow-version/workflow-version.component';
import { SelectHierarchyComponent } from './components/aris-workflow/components/select-hierarchy/select-hierarchy.component';
import { TimeVarianceComponent } from './components/aris-workflow/components/time-variance/time-variance.component';
import { UpdateResourceComponent } from './components/aris-workflow/components/update-resource/update-resource.component';
import { WorkFlowComponent } from './components/aris-workflow/components/work-flow/work-flow.component';
import { WorkflowToolbarComponent } from './components/aris-workflow/components/workflow-toolbar/workflow-toolbar.component';
import { AddEditIssueComponent } from './components/backlog-main/components/add-edit-issue/add-edit-issue.component';
import { AddSprintComponent } from './components/backlog-main/components/add-sprint/add-sprint.component';
import { BacklogComponent } from './components/backlog-main/components/backlog/backlog.component';
import { CloneIssueComponent } from './components/backlog-main/components/clone-issue/clone-issue.component';
import { CompleteSprintComponent } from './components/backlog-main/components/complete-sprint/complete-sprint.component';
import { DeleteIssueComponent } from './components/backlog-main/components/delete-issue/delete-issue.component';
import { IssueDetailsComponent } from './components/backlog-main/components/details/details.component';
import { FilterComponent } from './components/backlog-main/components/filter/filter.component';
import { BacklogMainComponent } from './components/backlog-main/components/main/backlog-main.component';
import { MoveIssueComponent } from './components/backlog-main/components/move-issue/move-issue.component';
import { RequirementDialogComponent } from './components/backlog-main/components/requirement-dialog/requirement-dialog.component';
import { SprintPlanerPopupComponent } from './components/backlog-main/components/sprint-planer-popup/sprint-planer-popup.component';
import { SprintsComponent } from './components/backlog-main/components/sprints/sprints.component';
// import { DashboardComponent } from './components/dashboard-main/dashboard.component';
import { AddEditHierarchyTypeComponent } from './components/hierarchy-type/add-edit-hierarchy-type/add-edit-hierarchy-type.component';
import { DeleteHierarchyTypeComponent } from './components/hierarchy-type/delete-hierarchy-type/delete-hierarchy-type.component';
import { HierarchiesComponent } from './components/hierarchy-type/hierarchies/hierarchies.component';
import { HierarchyPopupComponent } from './components/hierarchy-type/hierarchy-popup/hierarchy-popup.component';
import { HierarchyTypesComponent } from './components/hierarchy-type/hierarchy-types/hierarchy-types.component';
import { ImportArisSettingsComponent } from './components/import-aris-settings/import-aris-settings.component';
import { ImportArisWorkflowToArisDesignerComponent } from './components/import-aris-workflow-to-aris-designer/import-aris-workflow-to-aris-designer.component';
import { ImportBankReqPopupComponent } from './components/import-hierarchy/import-bank-req-popup/import-bank-req-popup.component';
import { ImportHierarchyComponent } from './components/import-hierarchy/import-hierarchy.component';
import { IssueTrackingPopupComponent } from './components/issue-tracking/components/issue-tracking-popup/issue-tracking-popup.component';
import { IssueTrackingComponent } from './components/issue-tracking/issue-tracking.component';
import { AddEditIssuePopupComponent } from './components/issue-types-main/issue-types-main/components/add-edit-issue-type/add-edit-issue-popup/add-edit-issue-popup.component';
import { AddEditIssueTypeComponent } from './components/issue-types-main/issue-types-main/components/add-edit-issue-type/add-edit-issue-type.component';
import { DeleteIssueTypeComponent } from './components/issue-types-main/issue-types-main/components/delete-issue-type/delete-issue-type.component';
import { IssueTypesPopupComponent } from './components/issue-types-main/issue-types-main/components/issue-types-popup/issue-types-popup.component';
import { IssueTypesViewComponent } from './components/issue-types-main/issue-types-main/components/issue-types-view/issue-types-view.component';
import { IssueTypesComponent } from './components/issue-types-main/issue-types-main/components/issue-types/issue-types.component';
import { IssuesComponent } from './components/issues/issues.component';
import { JiAuthComponent } from './components/jira-integration/ji-auth/ji-auth.component';
import { JiIssueTypeMapComponent } from './components/jira-integration/ji-issue-type-map/ji-issue-type-map.component';
import { JiProjectMapComponent } from './components/jira-integration/ji-project-map/ji-project-map.component';
import { JiSummaryComponent } from './components/jira-integration/ji-summary/ji-summary.component';
import { JiUserMapComponent } from './components/jira-integration/ji-user-map/ji-user-map.component';
import { JiWebhookConfigComponent } from './components/jira-integration/ji-webhook-config/ji-webhook-config.component';
import { JiraIntegrationPopupComponent } from './components/jira-integration/jira-integration-popup/jira-integration-popup.component';
import { JiraIntegrationComponent } from './components/jira-integration/jira-integration.component';
import { JiraProjectDetailsComponent } from './components/jira-integration/jira-project-details/jira-project-details.component';
import { LinkProcessWfComponent } from './components/link-process-wf/link-process-wf.component';
import { LinkWfIssueComponent } from './components/link-wf-issue/link-wf-issue.component';
import { AddEditPeopleComponent } from './components/people/add-edit-people/add-edit-people.component';
import { AddPeoplePopupComponent } from './components/people/add-edit-people/add-people-popup/add-people-popup.component';
import { DeletePeopleComponent } from './components/people/delete-people/delete-people.component';
import { PeoplePopupComponent } from './components/people/people-popup/people-popup.component';
import { PeopleViewComponent } from './components/people/people-view/people-view.component';
import { PeopleComponent } from './components/people/people/people.component';
import { AddEditHierarchyComponent } from './components/process-main/components/add-edit-hierarchy/add-edit-hierarchy.component';
import { AddEditProcessComponent } from './components/process-main/components/add-edit-process/add-edit-process.component';
import { DeleteHierarchyComponent } from './components/process-main/components/delete-hierarchy/delete-hierarchy.component';
import { DeleteProcessComponent } from './components/process-main/components/delete-process/delete-process.component';
import { GenerateDocumentationComponent } from './components/process-main/components/generate-documentation/generate-documentation.component';
import { HierarchyItemsComponent } from './components/process-main/components/hierarchy-items/hierarchy-items.component';
import { HierarchyStatisticsComponent } from './components/process-main/components/hierarchy-statistics/hierarchy-statistics.component';
import { MoveCopyHierarchyComponent } from './components/process-main/components/move-copy-hierarchy/move-copy-hierarchy.component';
import { PreviewWorkflowComponent } from './components/process-main/components/preview-workflow/preview-workflow.component';
import { ProcessDescriptionComponent } from './components/process-main/components/process-description/process-description.component';
import { ProcessHeaderComponent } from './components/process-main/components/process-header/process-header.component';
import { ProcessHierarchiesComponent } from './components/process-main/components/process-hierarchies/process-hierarchies.component';
import { ProcessItemsComponent } from './components/process-main/components/process-items/process-items.component';
import { ProcessStatisticsComponent } from './components/process-main/components/process-statistics/process-statistics.component';
import { AddEditRolePopupComponent } from './components/role/add-edit-role/add-edit-role-popup/add-edit-role-popup.component';
import { AddEditRoleComponent } from './components/role/add-edit-role/add-edit-role.component';
import { DeleteRoleComponent } from './components/role/delete-role/delete-role.component';
import { RolePopupComponent } from './components/role/role-popup/role-popup.component';
import { RolesViewComponent } from './components/role/roles-view/roles-view.component';
import { RolesComponent } from './components/role/roles/roles.component';
// import { ProcessmanagmentReportsAppComponent } from './components/reports/processmanagment-reports-app/processmanagment-reports-app.component';
import { AddEditPriorityComponent } from './components/settings/components/add-edit-priority/add-edit-priority.component';
import { AddEditRequirementTypesComponent } from './components/settings/components/add-edit-requirement-types/add-edit-requirement-types.component';
import { DeletePriorityComponent } from './components/settings/components/delete-priority/delete-priority.component';
import { DeleteRequirementTypeComponent } from './components/settings/components/delete-requirement-type/delete-requirement-type.component';
import { PriorityPopupComponent } from './components/settings/components/priority/priority-popup/priority-popup.component';
import { PriorityComponent } from './components/settings/components/priority/priority.component';
import { RequirmentTypeComponent } from './components/settings/components/requirment-type/requirment-type.component';
import { EditService } from './components/settings/components/requirment-type/requirment.service';
import { SettingsAppComponent } from './components/settings/components/settings-app/settings-app.component';
import { SettingsPopupComponent } from './components/settings/components/settings-popup/settings-popup.component';
import { SettingsComponent } from './components/settings/settings.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SubnavMenuComponent } from './components/subnav-menu/subnav-menu.component';
import { ViewWorkflowComponent } from './components/view-workflow/view-workflow.component';
import { WorkflowIssuesComponent } from './components/workflow-issues/workflow-issues.component';
import { ProcessManagementRoutingModule } from './process-managment-routing.module';
import { ImportHierarchyService } from './services/import-hierarchy.service';
import { JiraIntegrationService } from './services/jira-integration.service';
import { ProcessManagementService } from './services/process-management.service';
import { ProcessResourcesService } from './services/process-resources.service';
import { WorkflowIssuesDataService } from './services/workflow-issues-data.service';
import { ImportArisWfService } from './services/import-aris-workflow.service';
import { RapidEventsService } from './services/rapid-events.service';
import { WorkflowItemService } from './services/workflow-item.service';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    TreeViewModule,
    HttpClientModule,
    FormsModule,
    CoreModule,
    LayoutModule,
    WindowModule,
    ButtonsModule,
    DialogModule,
    ReactiveFormsModule,
    PopupModule,
    MenuModule,
    InputsModule,
    ToolBarModule,
    DropDownsModule,
    GridModule,
    ExcelModule,
    NgDragDropModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    SortableModule,
    AngularDraggableModule,
    DialogsModule,
    ProcessManagementRoutingModule,
    ChartsModule,
    UploadModule,
    TestGenerationModule,
    DragDropModule,
    ResizableModule,
    DateInputsModule,
    MatBadgeModule,
    ArchwizardModule,
    NgMultiSelectDropDownModule.forRoot()
  ],
  providers: [
    ProcessManagementService,
    RapidEventsService,
    WorkflowItemService,
    ImportArisWfService,
    SessionService,
    WorkflowIssuesDataService,
    ProcessResourcesService,
    ImportHierarchyService,
    EditService,
    JiraIntegrationService
  ],
  declarations: [
    RequirementDialogComponent,
    WorkFlowComponent,
    TitleComponent,
    ApplicationSystemTypeItemComponent,
    ActivityItemComponent,
    EventItemComponent,
    ProcessItemComponent,
    ResourceItemComponent,
    RoleItemComponent,
    ScreenItemComponent,
    ScreenComponent,
    SystemTypeComponent,
    ProcessComponent,
    PropetiesContainerComponent,
    FileTypeComponent,
    DescriptionComponent,
    TypicalNameComponent,
    SidebarComponent,
    SubnavMenuComponent,
    IssuesComponent,
    SettingsComponent,
    BacklogMainComponent,
    IssueTypesViewComponent,
    AddEditIssueTypeComponent,
    DeleteIssueTypeComponent,
    IssueTypesComponent,
    HierarchiesComponent,
    HierarchyTypesComponent,
    DeleteHierarchyTypeComponent,
    AddEditHierarchyTypeComponent,
    ImportArisSettingsComponent,
    WorkflowIssuesComponent,
    ViewWorkflowComponent,
    DesignerDetailsComponent,
    ImportArisWorkflowToArisDesignerComponent,
    ProcessHierarchiesComponent,
    HierarchyItemsComponent,
    ProcessItemsComponent,
    HierarchyStatisticsComponent,
    ProcessStatisticsComponent,
    AddEditProcessComponent,
    AddEditHierarchyComponent,
    DeleteHierarchyComponent,
    MoveCopyHierarchyComponent,
    DeleteProcessComponent,
    PreviewWorkflowComponent,
    LinkWfIssueComponent,
    WorkflowToolbarComponent,
    WorkflowVersionComponent,
    DetailsComponent,
    ProcessResourcesComponent,
    DeleteProcessResourceComponent,
    UpdateResourceComponent,
    LinkProcessWfComponent,
    RolesComponent,
    RolesViewComponent,
    DeleteRoleComponent,
    AddEditRoleComponent,
    PeopleComponent,
    PeopleViewComponent,
    DeletePeopleComponent,
    AddEditPeopleComponent,
    FilterComponent,
    BacklogComponent,
    SprintsComponent,
    IssueDetailsComponent,
    LinkProcessWorkflowComponent,
    AddSprintComponent,
    MoveIssueComponent,
    ProcessHeaderComponent,
    ImportHierarchyComponent,
    ImportHierarchyComponent,
    ProcessDescriptionComponent,
    SelectHierarchyComponent,
    TimeVarianceComponent,
    AddEditIssueComponent,
    JiraIntegrationComponent,
    JiAuthComponent,
    JiProjectMapComponent,
    JiIssueTypeMapComponent,
    JiUserMapComponent,
    JiSummaryComponent,
    JiraProjectDetailsComponent,
    CompleteSprintComponent,
    DeleteIssueComponent,
    RequirmentTypeComponent,
    PriorityComponent,
    AddEditRequirementTypesComponent,
    DeleteRequirementTypeComponent,
    AddEditPriorityComponent,
    DeletePriorityComponent,
    DeleteIssueComponent,
    GenerateDocumentationComponent,
    IssueTrackingComponent,
    CloneIssueComponent,
    // ProcessmanagmentReportsAppComponent,
    SettingsAppComponent,
    JiWebhookConfigComponent,
    SettingsAppComponent,
    SettingsAppComponent,
    SettingsPopupComponent,
    SprintPlanerPopupComponent,
    IssueTrackingPopupComponent,
    RolePopupComponent,
    PeoplePopupComponent,
    AddPeoplePopupComponent,
    AddEditRolePopupComponent,
    HierarchyPopupComponent,
    ImportBankReqPopupComponent,
    IssueTypesPopupComponent,
    AddEditIssuePopupComponent,
    PriorityPopupComponent,
    JiraIntegrationPopupComponent
  ],
  exports: [WorkFlowComponent]
})
export class ProcessManagmentModule {}
