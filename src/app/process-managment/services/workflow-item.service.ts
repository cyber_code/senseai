import { ArisWorkflowItem } from "../models/aris-workflow-item";
import { Subject } from "rxjs";
import { Injectable } from "@angular/core";
import { GenericWorkflowItem } from "../models/generic-workflow-item";

@Injectable()
export class WorkflowItemService {
  itemUpdated: Subject<any> = new Subject<any>();
  itemUpdated$ = this.itemUpdated.asObservable();

  workflowItemSelected: any;
  editable: Boolean = false;

  workflowItemSelectedModified: Subject<any> = new Subject<any>();
  worfklowItemSelectedModified$ = this.workflowItemSelectedModified.asObservable();

  workflowEditable: Subject<boolean> = new Subject<boolean>();
  workflowEditable$ = this.workflowEditable.asObservable();

  workflowSelectionChanged: Subject<string> = new Subject<string>();
  workflowSelectionChanged$ = this.workflowSelectionChanged.asObservable();

  private _getGraphJson: Subject<JsonNeededBy> = new Subject<JsonNeededBy>();
  getGraphJson$ = this._getGraphJson.asObservable();

  private _jsonLoaded: Subject<GraphJson> = new Subject<GraphJson>();
  jsonLoaded$ = this._jsonLoaded.asObservable();

  changedWorkflow: Subject<any> = new Subject<any>();
  changedWorkflow$ = this.changedWorkflow.asObservable();

  rollbackWorkflow: Subject<any> = new Subject<any>();
  rollbackWorkflow$ = this.rollbackWorkflow.asObservable();

  closeRollbackDialog: Subject<any> = new Subject<any>();
  closeRollbackDialog$ = this.closeRollbackDialog.asObservable();

  workflowVersionChanged: Subject<any> = new Subject<any>();
  workflowVersionChanged$ = this.workflowVersionChanged.asObservable();

  constructor() {
    this.itemUpdated$.subscribe(itemSelected => {
      this.workflowItemSelected = itemSelected;
    });
    this.workflowEditable$.subscribe(result => {
      this.editable = result;
    });
  }

  getGraphJson(neededBy: JsonNeededBy) {
    this._getGraphJson.next(neededBy);
  }

  sendGraphJson(graphJson: string, neededBy: JsonNeededBy) {
    this._jsonLoaded.next({ json: graphJson, neededBy: neededBy });
  }
}

export enum JsonNeededBy {
  Paths,
  DynamicData,
  Filters
}

export class GraphJson {
  neededBy: JsonNeededBy;
  json: string;
}
