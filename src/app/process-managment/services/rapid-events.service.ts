import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class RapidEventsService {
  itemSelected: Subject<any> = new Subject<any>();

  itemSelected$ = this.itemSelected.asObservable();

  public newElementAdded: Subject<any> = new Subject<any>();
  public newElementAdded$ = this.newElementAdded.asObservable();
}
