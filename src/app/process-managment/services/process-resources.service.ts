import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { SessionService } from 'src/app/core/services/session.service';
import {MessageService} from 'src/app/core/services/message.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';

@Injectable()
export class ProcessResourcesService {
  private browseUrl: string;

  constructor(
        private configuration: ConfigurationService,
        private http: HttpClient,
        private sessionService: SessionService,
        private httpExecutorService: HttpExecutorService
    ) {
      this.browseUrl = `${configuration.serverSettings.apiUrl}Process/Browse/`;
  }


  addProcessResources(processId, fileCollection) {
      if (fileCollection && fileCollection.length > 0) {
        let headers = {};
        headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
        const url = this.browseUrl + 'AddProcessResources';
        let formData = new FormData;
        formData.append('ProcessId', processId);
        for (let i = 0; i < fileCollection.length; i++) {
            formData.append('FileCollection', fileCollection[i]);
        }
        return this.http.post(url, formData, {headers : headers}).pipe(
            map((res: any) => {
                return this.httpExecutorService.handleResponse(url, res);
            }),
            catchError(error => {
                return this.httpExecutorService.handleError(error);
            })
        );
      }
    
  }

  updateProcessResource(id, fileName, processId, file) {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const url = this.browseUrl + 'UpdateProcessResources';
    let formData = new FormData;
    formData.append('Id', id);
    formData.append('FileName', fileName);
    formData.append('ProcessId', processId);
    formData.append('File', file);
    return this.http.put(url, formData, {headers : headers}).pipe(
        map((res: any) => {
            return this.httpExecutorService.handleResponse(url, res);
        }),
        catchError(error => {
            return this.httpExecutorService.handleError(error);
        })
    );
  }

  downloadProcessResource(id: string): Observable<Blob> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    return this.http.get(this.browseUrl + 'DownloadProcessResource?Id=' + id, { responseType: 'blob', headers: headers });
  }
  
 }

