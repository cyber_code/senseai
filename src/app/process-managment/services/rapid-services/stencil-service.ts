import { ArisWorkflowType } from '../../../core/models/aris-workflow-item-type';
import { ui, dia } from '../../../../vendor/rappid';
import * as appShapes from '../../../../shapes/app-shapes';
import { translateAggregateResults } from '@progress/kendo-data-query';
import { GenericWorkflowType } from 'src/app/core/models/generic-workflow-item-type';

export class StencilService {
  stencil: ui.Stencil;
  isArisWorkflow: Boolean = true;

  constructor(isArisWorkflow) {
    this.isArisWorkflow = isArisWorkflow;
  }

  create(paperScroller: ui.PaperScroller, snaplines: ui.Snaplines) {
    this.stencil = new ui.Stencil({
      paper: paperScroller,
      snaplines: snaplines,
      width: 460,
      groups: this.getStencilGroups(),
      dropAnimation: true,
      groupsToggleButtons: true,
      paperOptions: function() {
        return {
          model: new dia.Graph(
            {},
            {
              cellNamespace: appShapes
            }
          ),
          cellViewNamespace: appShapes
        };
      },
      layout: true,
      dragStartClone: cell => cell.clone().removeAttr('root/dataTooltip')
    });
  }

  setShapes() {
    this.stencil.load(this.getStencilShapes());
  }

  getShapeModel(itemType: ArisWorkflowType | GenericWorkflowType) {
    const defaultModels = this.getStencilShapes().standard as any;

    const model = defaultModels.find(x => x.customData.Type === itemType);
    return model;
  }

  getStencilGroups() {
    return <{ [key: string]: ui.Stencil.Group }>{
      standard: { index: 1, label: 'Standard shapes' }
    };
  }

  getStencilShapes() {
    return this.isArisWorkflow ? this.getArisStencilShapes() : this.getGenericStencilShapes();
  }

  getGenericStencilShapes() {
    return {
      standard: [
        {
          type: 'fsa.State',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'State',
              dataTooltipPosition: 'top'
            },
            circle: {
              width: 60,
              height: 60,
              fill: '#ffffff',
              stroke: '#c8d6e5',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Start',
              fill: 'black',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Bold',
              'font-size': 11,
              'stroke-width': 0
            }
          },
          customData: {
            Type: GenericWorkflowType.Start,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: 'Start',
            Description: '',
          }
        },
        {
          type: 'standard.InscribedImage',
          attrs: {
            image: {
              xlinkHref: 'assets/actionicons/cross.png',
              width: 60,
              height: 60
            },
            border: {
              stroke: '#666666',
              strokeWidth: 1,
              strokeDasharray: '0',
              rx: 20,
              ry: 20
            }
          },
          customData: {
            Type: GenericWorkflowType.Conditional,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: 'Condition',
            Description: ''
          },
          position: { x: 2, y: 2 }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Activity',
              dataTooltipPosition: 'top',
              type: 'Activity'
            },
            body: {
              refHeight: 10,
              refWidth: 0.9,
              refX: 15,
              refY: 2,
              fill: 'rgba(210, 230, 210, 1)',
              stroke: 'rgba(167, 204, 165, 1)',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 30,
              ry: 30
            },
            label: {
              text: 'Activity',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0,
              textVerticalAnchor: 'middle',
              textAnchor: 'middle',
              refX: '23%',
              refY: '63%'
            }
          },
          customData: {
            Type: GenericWorkflowType.Activity,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: ''
          }
        }
      ],
      fsa: [
        {
          type: 'fsa.StartState',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'Start State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            circle: {
              width: 50,
              height: 30,
              fill: '#61549c',
              'stroke-width': 0
            },
            text: {
              text: 'startState',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        },
        {
          type: 'fsa.EndState',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'End State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.inner': {
              fill: '#6a6c8a',
              stroke: 'transparent'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'endState',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        },
        {
          type: 'fsa.State',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            circle: {
              fill: '#6a6c8a',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'state',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        }
      ],
      pn: [
        {
          type: 'pn.Place',
          preserveAspectRatio: true,
          tokens: 3,
          attrs: {
            root: {
              dataTooltip: 'Place',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.root': {
              fill: 'transparent',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            '.tokens circle': {
              fill: '#6a6c8a',
              stroke: '#000',
              'stroke-width': 0
            },
            '.label': {
              text: '',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            }
          }
        },
        {
          type: 'pn.Transition',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'Transition',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            rect: {
              rx: 3,
              ry: 3,
              width: 12,
              height: 50,
              fill: '#61549c',
              stroke: '#7c68fc',
              'stroke-width': 0,
              'stroke-dasharray': '0'
            },
            '.label': {
              text: 'transition',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'stroke-width': 0,
              fill: '#222138'
            }
          }
        }
      ],
      erd: [
        {
          type: 'erd.Entity',
          attrs: {
            root: {
              dataTooltip: 'Entity',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              rx: 3,
              ry: 3,
              fill: '#31d0c6',
              'stroke-width': 2,
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Entity',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.WeakEntity',
          attrs: {
            root: {
              dataTooltip: 'Weak Entity',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#26ff51',
              'stroke-width': 2,
              points: '100,0 100,60 0,60 0,0',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#26ff51',
              stroke: 'transparent',
              points: '97,5 97,55 3,55 3,5',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Weak entity',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Relationship',
          attrs: {
            root: {
              dataTooltip: 'Relationship',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: '#61549c',
              stroke: 'transparent',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Relation',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.IdentifyingRelationship',
          attrs: {
            root: {
              dataTooltip: 'Identifying Relationship',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#6a6c8a',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#6a6c8a',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Relation',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.ISA',
          attrs: {
            root: {
              dataTooltip: 'ISA',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            text: {
              text: 'ISA',
              fill: '#f6f6f6',
              'letter-spacing': 0,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            polygon: {
              fill: '#26ff51',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            }
          }
        },
        {
          type: 'erd.Key',
          attrs: {
            root: {
              dataTooltip: 'Key',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#fe854f',
              stroke: 'transparent',
              display: 'block',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Key',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Normal',
          attrs: {
            root: {
              dataTooltip: 'Normal',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: '#26ff51',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Normal',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Multivalued',
          attrs: {
            root: {
              dataTooltip: 'Mutltivalued',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#fe854f',
              stroke: 'transparent',
              rx: 43,
              ry: 21,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'MultiValued',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Derived',
          attrs: {
            root: {
              dataTooltip: 'Derived',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#26ff51',
              stroke: 'transparent',
              display: 'block',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Derived',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        }
      ],
      uml: [
        {
          type: 'uml.Class',
          name: 'Class',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Class',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },
        {
          type: 'uml.Interface',
          name: 'Interface',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Interface',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },
        {
          type: 'uml.Abstract',
          name: 'Abstract',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Abstract',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },

        {
          type: 'uml.State',
          name: 'State',
          events: ['entry/', 'create()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-state-body': {
              fill: '#26ff51',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8,
              'stroke-dasharray': '0'
            },
            '.uml-state-separator': {
              stroke: '#f6f6f6',
              'stroke-width': 1,
              'stroke-dasharray': '0'
            },
            '.uml-state-name': {
              fill: '#f6f6f6',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            },
            '.uml-state-events': {
              fill: '#f6f6f6',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            }
          }
        }
      ],
      org: [
        {
          type: 'org.Member',
          attrs: {
            root: {
              dataTooltip: 'Member',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.rank': {
              text: 'Rank',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-size': 12,
              'font-weight': 'Bold',
              'text-decoration': 'none'
            },
            '.name': {
              text: 'Person',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 10
            },
            '.card': {
              fill: '#31d0c6',
              stroke: 'transparent',
              'stroke-width': 0,
              'stroke-dasharray': '0'
            },
            image: {
              width: 32,
              height: 32,
              x: 16,
              y: 13,
              ref: <string>null,
              'ref-x': <number>null,
              'ref-y': <number>null,
              'y-alignment': <string>null,
              'xlink:href': 'assets/member-male.png'
            }
          }
        }
      ]
    };
  }

  getArisStencilShapes() {
    return {
      standard: [
        {
          type: 'standard.InscribedImage',
          attrs: {
            image: {
              xlinkHref: 'assets/actionicons/cross.png',
              width: 60,
              height: 60
            },
            border: {
              stroke: '#666666',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 20,
              ry: 20
            }
          },
          customData: {
            Type: ArisWorkflowType.Conditional,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            TypicalName: '',
            Screen: '',
            FileType: ''
          },
          position: { x: 2, y: 2 }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Activity',
              dataTooltipPosition: 'top',
              type: 'Activity'
            },
            body: {
              refHeight: 10,
              refWidth: 0.9,
              refX: 15,
              refY: 2,
              fill: 'rgba(210, 230, 210, 1)',
              stroke: 'rgba(167, 204, 165, 1)',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 30,
              ry: 30
            },
            label: {
              text: 'Activity',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0,
              textVerticalAnchor: 'middle',
              textAnchor: 'middle',
              refX: '23%',
              refY: '63%'
            }
          },
          customData: {
            Type: ArisWorkflowType.Activity,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            TypicalName: '',
            Screen: '',
            FileType: ''
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Application System Type',
              dataTooltipPosition: 'top',
              type: 'Application System Type'
            },
            body: {
              refHeight: 10,
              refWidth: 0.9,
              refX: 15,
              refY: 15,
              fill: 'rgba(156, 255, 255, 1)',
              stroke: 'rgba(195, 230, 245, 1)',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 30,
              ry: 30
            },
            label: {
              text: 'Application System Type',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0,
              textVerticalAnchor: 'middle',
              textAnchor: 'middle',
              refX: '23%',
              refY: '105%'
            }
          },
          customData: {
            Type: ArisWorkflowType.SystemType,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            TypicalName: '',
            Screen: '',
            FileType: ''
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Event',
              dataTooltipPosition: 'top',
              type: 'Event'
            },
            body: {
              refHeight: 10,
              refWidth: 0.9,
              refX: 15,
              refY: 35,
              fill: 'rgba(246, 178, 246, 1)',
              stroke: 'rgba(202, 123, 221, 1)',
              refPoints: 'M 50 0 L 0 20 0 80 50 100 100 80 100 20 z',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 30,
              ry: 30
            },
            label: {
              text: 'Event',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0,
              textVerticalAnchor: 'middle',
              textAnchor: 'middle',
              refX: '23%',
              refY: '160%'
            }
          },
          customData: {
            Type: ArisWorkflowType.Event,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            TypicalName: '',
            Screen: '',
            FileType: ''
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Process',
              dataTooltipPosition: 'top',
              type: 'Process'
            },
            body: {
              refHeight: 10,
              refWidth: 0.9,
              refX: 15,
              refY: 55,
              fill: 'rgba(211, 210, 210, 1)',
              stroke: 'rgba(154, 153, 153, 1)',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 30,
              ry: 30
            },
            label: {
              text: 'Process',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0,
              textVerticalAnchor: 'middle',
              textAnchor: 'middle',
              refX: '23%',
              refY: '200%'
            }
          },
          customData: {
            Type: ArisWorkflowType.Process,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            TypicalName: '',
            Screen: '',
            FileType: ''
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Role',
              dataTooltipPosition: 'top',
              type: 'Role'
            },
            body: {
              refHeight: 10,
              refWidth: 0.9,
              refX: 15,
              refY: 80,
              fill: 'rgba(255, 255, 155, 1)',
              stroke: 'rgba(210, 180, 140, 1)',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 30,
              ry: 30
            },
            label: {
              text: 'Role',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0,
              textVerticalAnchor: 'middle',
              textAnchor: 'middle',
              refX: '23%',
              refY: '270%'
            }
          },
          customData: {
            Type: ArisWorkflowType.Role,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            TypicalName: '',
            Screen: '',
            FileType: ''
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Screen',
              dataTooltipPosition: 'top',
              type: 'Screen'
            },
            body: {
              refHeight: 10,
              refWidth: 0.9,
              refX: 15,
              refY: 100,
              fill: 'rgba(248, 248, 248, 1)',
              stroke: 'grey',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 30,
              ry: 30
            },
            label: {
              text: 'T24 Screen',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0,
              textVerticalAnchor: 'middle',
              textAnchor: 'middle',
              refX: '23%',
              refY: '315%'
            }
          },
          customData: {
            Type: ArisWorkflowType.T24Screen,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            TypicalName: '',
            Screen: '',
            FileType: ''
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Resource',
              dataTooltipPosition: 'top',
              type: 'Resource'
            },
            body: {
              refHeight: 10,
              refWidth: 0.9,
              refX: 15,
              refY: 120,
              fill: 'rgba(201, 207, 237, 1)',
              stroke: 'rgba(187, 195, 204, 1)',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 30,
              ry: 30
            },
            label: {
              text: 'Resource',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0,
              textVerticalAnchor: 'middle',
              textAnchor: 'middle',
              refX: '23%',
              refY: '360%'
            }
          },
          customData: {
            Type: ArisWorkflowType.Resource,
            Id: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            TypicalName: '',
            Screen: '',
            FileType: ''
          }
        }
      ],
      fsa: [
        {
          type: 'fsa.StartState',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'Start State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            circle: {
              width: 50,
              height: 30,
              fill: '#61549c',
              'stroke-width': 0
            },
            text: {
              text: 'startState',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        },
        {
          type: 'fsa.EndState',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'End State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.inner': {
              fill: '#6a6c8a',
              stroke: 'transparent'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'endState',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        },
        {
          type: 'fsa.State',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            circle: {
              fill: '#6a6c8a',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'state',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        }
      ],
      pn: [
        {
          type: 'pn.Place',
          preserveAspectRatio: true,
          tokens: 3,
          attrs: {
            root: {
              dataTooltip: 'Place',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.root': {
              fill: 'transparent',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            '.tokens circle': {
              fill: '#6a6c8a',
              stroke: '#000',
              'stroke-width': 0
            },
            '.label': {
              text: '',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            }
          }
        },
        {
          type: 'pn.Transition',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'Transition',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            rect: {
              rx: 3,
              ry: 3,
              width: 12,
              height: 50,
              fill: '#61549c',
              stroke: '#7c68fc',
              'stroke-width': 0,
              'stroke-dasharray': '0'
            },
            '.label': {
              text: 'transition',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'stroke-width': 0,
              fill: '#222138'
            }
          }
        }
      ],
      erd: [
        {
          type: 'erd.Entity',
          attrs: {
            root: {
              dataTooltip: 'Entity',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              rx: 3,
              ry: 3,
              fill: '#31d0c6',
              'stroke-width': 2,
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Entity',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.WeakEntity',
          attrs: {
            root: {
              dataTooltip: 'Weak Entity',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#26ff51',
              'stroke-width': 2,
              points: '100,0 100,60 0,60 0,0',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#26ff51',
              stroke: 'transparent',
              points: '97,5 97,55 3,55 3,5',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Weak entity',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Relationship',
          attrs: {
            root: {
              dataTooltip: 'Relationship',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: '#61549c',
              stroke: 'transparent',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Relation',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.IdentifyingRelationship',
          attrs: {
            root: {
              dataTooltip: 'Identifying Relationship',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#6a6c8a',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#6a6c8a',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Relation',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.ISA',
          attrs: {
            root: {
              dataTooltip: 'ISA',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            text: {
              text: 'ISA',
              fill: '#f6f6f6',
              'letter-spacing': 0,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            polygon: {
              fill: '#26ff51',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            }
          }
        },
        {
          type: 'erd.Key',
          attrs: {
            root: {
              dataTooltip: 'Key',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#fe854f',
              stroke: 'transparent',
              display: 'block',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Key',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Normal',
          attrs: {
            root: {
              dataTooltip: 'Normal',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: '#26ff51',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Normal',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Multivalued',
          attrs: {
            root: {
              dataTooltip: 'Mutltivalued',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#fe854f',
              stroke: 'transparent',
              rx: 43,
              ry: 21,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'MultiValued',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Derived',
          attrs: {
            root: {
              dataTooltip: 'Derived',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#26ff51',
              stroke: 'transparent',
              display: 'block',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Derived',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        }
      ],
      uml: [
        {
          type: 'uml.Class',
          name: 'Class',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Class',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },
        {
          type: 'uml.Interface',
          name: 'Interface',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Interface',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },
        {
          type: 'uml.Abstract',
          name: 'Abstract',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Abstract',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },

        {
          type: 'uml.State',
          name: 'State',
          events: ['entry/', 'create()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-state-body': {
              fill: '#26ff51',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8,
              'stroke-dasharray': '0'
            },
            '.uml-state-separator': {
              stroke: '#f6f6f6',
              'stroke-width': 1,
              'stroke-dasharray': '0'
            },
            '.uml-state-name': {
              fill: '#f6f6f6',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            },
            '.uml-state-events': {
              fill: '#f6f6f6',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            }
          }
        }
      ],
      org: [
        {
          type: 'org.Member',
          attrs: {
            root: {
              dataTooltip: 'Member',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.rank': {
              text: 'Rank',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-size': 12,
              'font-weight': 'Bold',
              'text-decoration': 'none'
            },
            '.name': {
              text: 'Person',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 10
            },
            '.card': {
              fill: '#31d0c6',
              stroke: 'transparent',
              'stroke-width': 0,
              'stroke-dasharray': '0'
            },
            image: {
              width: 32,
              height: 32,
              x: 16,
              y: 13,
              ref: <string>null,
              'ref-x': <number>null,
              'ref-y': <number>null,
              'y-alignment': <string>null,
              'xlink:href': 'assets/member-male.png'
            }
          }
        }
      ]
    };
  }
}
