import * as joint from '../../../../vendor/rappid';
import * as _ from 'lodash';
import { StencilService } from './stencil-service';
import { ToolbarService } from './toolbar-service';
import { InspectorService } from './inspector-service';
import { HaloService } from './halo-service';
import { KeyboardService } from './keyboard-service';
import * as appShapes from '../../../../shapes/app-shapes';
import { RapidEventsService } from '../rapid-events.service';
import { Subject } from 'rxjs';
import { ArisWorkflowItemCustomData } from '../../models/aris-workflow-item-custom-data.model';
import { ArisWorkflowType } from 'src/app/core/models/aris-workflow-item-type';

export class KitchenSinkService {
  el: Element;

  graph: joint.dia.Graph;
  paper: joint.dia.Paper;
  public paperScroller: joint.ui.PaperScroller;

  commandManager: joint.dia.CommandManager;
  snaplines: joint.ui.Snaplines;
  clipboard: joint.ui.Clipboard;
  selection: joint.ui.Selection;
  navigator: joint.ui.Navigator;

  stencilService: StencilService;
  toolbarService: ToolbarService;
  inspectorService: InspectorService;
  haloService: HaloService;
  keyboardService: KeyboardService;
  public graphJson: any;
  public undoStack: any = [];
  public redoStack: any = [];
  public removedLinks: any = [];
  public selectedItems: any = [];
  public isSectionSelected: boolean = false;
  lastUndoElement: any;
  lastRedoElement: any;
  rapidEventService: RapidEventsService;

  undoRedoClicked: Subject<any> = new Subject<any>();
  undoRedoClicked$ = this.undoRedoClicked.asObservable();

  constructor(
    el: Element,
    stencilService: StencilService,
    toolbarService: ToolbarService,
    inspectorService: InspectorService,
    haloService: HaloService,
    keyboardService: KeyboardService,
    rapidEventService: RapidEventsService
  ) {
    this.el = el;
    // apply current joint js theme
    const view = new joint.mvc.View({ el: this.el });

    this.stencilService = stencilService;
    this.toolbarService = toolbarService;
    this.inspectorService = inspectorService;
    this.haloService = haloService;
    this.keyboardService = keyboardService;
    this.rapidEventService = rapidEventService;
  }

  getGraphJson() {
    return this.graph.toJSON();
  }

  loadGraphJson(graphJson: any) {
    this.graph.fromJSON(graphJson);
  }

  startRappid() {
    joint.setTheme('modern');

    this.initializePaper();
    this.initializeStencil();
    this.initializeSelection();
    this.initializeToolsAndInspector();
    this.initializeNavigator();
    this.initializeToolbar();
    this.initializeKeyboardShortcuts();
    this.initializeTooltips();
  }

  initializePaper() {
    const graph = (this.graph = new joint.dia.Graph(
      {},
      {
        cellNamespace: appShapes
      }
    ));

    graph.on('add', (cell: joint.dia.Cell, collection: any, opt: any) => {
      if (opt.stencil) {
        this.inspectorService.create(cell);

        this.rapidEventService.itemSelected.next(cell);
        this.rapidEventService.newElementAdded.next({
          cell: cell,
          collection: collection
        });
      }
    });

    this.commandManager = new joint.dia.CommandManager({ graph: graph });

    const paper = (this.paper = new joint.dia.Paper({
      width: 1000,
      height: 1000,
      gridSize: 10,
      drawGrid: true,
      model: graph,
      cellViewNamespace: appShapes,
      defaultLink: <joint.dia.Link>new appShapes.app.Link()
    }));

    paper.on('blank:mousewheel', _.partial(this.onMousewheel, null), this);
    paper.on('cell:mousewheel', this.onMousewheel.bind(this));

    this.snaplines = new joint.ui.Snaplines({ paper: paper });

    const paperScroller = (this.paperScroller = new joint.ui.PaperScroller({
      paper,
      autoResizePaper: true,
      cursor: 'grab'
    }));

    this.renderPlugin('.paper-container', paperScroller);
    paperScroller.render().center();
  }

  addTextToConnector(link, state) {
    const ara = this.graph.getCell(link.model.id);
    if (state === 'yes') {
      ara.label(0, {
        position: {
          distance: 0.5,
          offset: 9
        },
        attrs: {
          rect: { fill: '#eeeeee' },
          text: { text: 'Y', 'font-size': 15, ref: 'rect' }
        }
      });
    } else if (state === 'no') {
      ara.label(0, {
        position: {
          distance: 0.5,
          offset: 9
        },
        attrs: {
          rect: { fill: '#eeeeee' },
          text: { text: 'N', 'font-size': 15, ref: 'rect' }
        }
      });
    } else if ((state = 'remove')) {
      ara.label(0, {
        position: { distance: 0, offset: 0 },
        attrs: { text: { text: '', 'font-size': 1 } }
      });
    }
  }

  initializeStencil() {
    this.stencilService.create(this.paperScroller, this.snaplines);
    this.renderPlugin('.stencil-container', this.stencilService.stencil);
    this.stencilService.setShapes();
  }

  initializeSelection() {
    this.clipboard = new joint.ui.Clipboard();
    this.selection = new joint.ui.Selection({ paper: this.paper });
    const keyboard = this.keyboardService.keyboard;

    // Initiate selecting when the user grabs the blank area of the paper while the Shift key is pressed.
    // Otherwise, initiate paper pan.
    keyboard.on('delete shift+delete', () => {
      this.graph.removeCells(this.selection.collection.toArray(), {
        selection: this.selectedItems.length > 0,
        clear: true
      } as joint.dia.Cell.DisconnectableOptions);
    });
    this.paper.on(
      'blank:pointerdown',
      (evt: JQuery.Event, x: number, y: number) => {
        if (keyboard.isActive('shift', evt)) {
          this.isSectionSelected = true;
          this.selection.startSelecting(evt);
        } else {
          this.selection.cancelSelection();
          this.paperScroller.startPanning(evt);
        }
        this.rapidEventService.itemSelected.next(null);
      }
    );

    this.paper.on(
      'blank:pointerup',
      (evt: JQuery.Event, x: number, y: number) => {
        if (this.isSectionSelected) {
          this.selectedItems = this.selection.collection.models;
          this.isSectionSelected = false;
        }
      }
    );

    this.paper.on(
      'element:pointerdown',
      (elementView: joint.dia.ElementView, evt: JQuery.Event) => {
        this.rapidEventService.itemSelected.next(elementView.model);

        // Select an element if CTRL/Meta key is pressed while the element is clicked.
        if (keyboard.isActive('ctrl meta', evt)) {
          this.selection.collection.add(elementView.model);
        }
      }
    );

    this.selection.on(
      'selection-box:pointerdown',
      (elementView: joint.dia.ElementView, evt: JQuery.Event) => {
        // Unselect an element if the CTRL/Meta key is pressed while a selected element is clicked.
        if (keyboard.isActive('ctrl meta', evt)) {
          this.selection.collection.remove(elementView.model);
        }
      }
    );
  }

  initializeToolsAndInspector() {
    this.paper.on('element:pointerup', (elementView: joint.dia.ElementView) => {
      const element = elementView.model;

      if (this.selection.collection.contains(element)) {
        return;
      }

      new joint.ui.FreeTransform({
        cellView: elementView,
        allowRotation: false,
        preserveAspectRatio: !!element.get('preserveAspectRatio'),
        allowOrthogonalResize: element.get('allowOrthogonalResize') !== false
      }).render();
      this.haloService.create(elementView);
      this.selection.collection.reset([]);
      this.selection.collection.add(element, { silent: true });
      this.paper.removeTools();
      this.inspectorService.create(element);
    });

    this.paper.on('link:pointerup', this.linkPointerUp.bind(this));

    this.paper.on('link:mouseenter', (linkView: joint.dia.LinkView) => {
      // Open tool only if there is none yet
      if (linkView.hasTools()) {
        return;
      }

      const ns = joint.linkTools;
      const toolsView = new joint.dia.ToolsView({
        name: 'link-hover',
        tools: [
          new ns.Vertices({ vertexAdding: false }),
          new ns.TargetArrowhead()
        ]
      });

      linkView.addTools(toolsView);
    });

    this.paper.on('link:mouseleave', (linkView: joint.dia.LinkView) => {
      // Remove only the hover tool, not the pointerdown tool
      if (linkView.hasTools('link-hover')) {
        linkView.removeTools();
      }
    });

    this.graph.on('change', (cell: joint.dia.Cell, opt: any) => {
      if (!cell.isLink() || !opt.inspector) {
        return;
      }

      const ns = joint.linkTools;
      const toolsView = new joint.dia.ToolsView({
        name: 'link-inspected',
        tools: [new ns.Boundary({ padding: 15 })]
      });

      cell.findView(this.paper).addTools(toolsView);
    });
  }

  public linkPointerUp(linkView: joint.dia.LinkView) {
    const link = linkView.model;
    const ns = joint.linkTools;
    const toolsView = new joint.dia.ToolsView({
      name: 'link-pointerdown',
      tools: [
        new ns.Vertices(),
        new ns.SourceAnchor(),
        new ns.TargetAnchor(),
        new ns.TargetArrowhead(),
        new ns.Segments(),
        new ns.Boundary({ padding: 15 }),
        new ns.Remove({ offset: -20, distance: 40 })
      ]
    });
    this.selection.collection.reset([]);
    this.selection.collection.add(link, { silent: true });
    const paper = this.paper;
    joint.ui.Halo.clear(paper);
    joint.ui.FreeTransform.clear(paper);
    paper.removeTools();
    linkView.addTools(toolsView);
    this.inspectorService.create(link);
  }

  initializeNavigator() {
    const navigator = (this.navigator = new joint.ui.Navigator({
      width: 240,
      height: 115,
      paperScroller: this.paperScroller,
      zoom: false,
      paperOptions: {
        elementView: appShapes.NavigatorElementView,
        linkView: appShapes.NavigatorLinkView,
        cellViewNamespace: {
          /* no other views are accessible in the navigator */
        }
      }
    }));

    this.renderPlugin('.navigator-container', navigator);
  }

  initializeToolbar() {
    this.toolbarService.create(
      this.commandManager,
      this.paperScroller
    );

    this.toolbarService.toolbar.on({
      'undo:pointerclick': this.onUndoClick.bind(this),
      'redo:pointerclick': this.onRedoClick.bind(this),
      'svg:pointerclick': this.openAsSVG.bind(this),
      'png:pointerclick': this.openAsPNG.bind(this),
      'fullscreen:pointerclick': joint.util.toggleFullScreen.bind(
        joint.util,
        document.body
      ),
      'to-front:pointerclick': this.selection.collection.invoke.bind(
        this.selection.collection,
        'toFront'
      ),
      'to-back:pointerclick': this.selection.collection.invoke.bind(
        this.selection.collection,
        'toBack'
      ),
      'layout:pointerclick': this.layoutGraph.bind(this),
      'snapline:change': this.changeSnapLines.bind(this),
      'clear:pointerclick': this.clearGraph.bind(this),
      'print:pointerclick': this.paper.print.bind(this.paper),
      'grid-size:change': this.paper.setGridSize.bind(this.paper)
    });
    this.renderPlugin('.toolbar-container', this.toolbarService.toolbar);
  }

  onUndoClick() {
    let lastElement = this.undoStack.pop();
    if (lastElement) {
      this.redoStack.push({
        graphToLoad: this.getGraphJson(),
        graphToUndo: lastElement.graph,
        action: lastElement.action
      });
      this.loadGraphJson(lastElement.graph);
      this.rapidEventService.itemSelected.next(null);
    }
  }

  onRedoClick() {
    let lastElement = this.redoStack.pop();
    if (lastElement) {
      this.undoStack.push({
        graph: lastElement.graphToUndo
      });
      this.loadGraphJson(lastElement.graphToLoad);
      this.rapidEventService.itemSelected.next(null);
    }
  }

  clearGraph() {
    this.undoStack.push({
      graph: this.getGraphJson(),
      action: 'clearGraph'
    });
    this.loadGraphJson({ cells: [] });
  }

  layoutGraph() {
    joint.layout.DirectedGraph.layout(this.graph, {
      setVertices: true,
      rankDir: 'TB',
      marginX: 100,
      marginY: 100
    });

    this.paperScroller.centerContent();
  }

  changeSnapLines(checked: boolean) {
    if (checked) {
      this.snaplines.startListening();
      this.stencilService.stencil.options.snaplines = this.snaplines;
    } else {
      this.snaplines.stopListening();
      this.stencilService.stencil.options.snaplines = null;
    }
  }

  initializeKeyboardShortcuts() {
      this.keyboardService.create(
        this.graph,
        this.clipboard,
        this.selection,
        this.paperScroller,
        this.commandManager
      );
  }

  initializeTooltips(): joint.ui.Tooltip {
    return new joint.ui.Tooltip({
      rootTarget: document.body,
      target: '[data-tooltip]',
      direction: joint.ui.Tooltip.TooltipArrowPosition.Auto,
      padding: 10
    });
  }

  openAsSVG() {
    this.paper.toSVG(
      (svg: string) => {
        new joint.ui.Lightbox({
          image: 'data:image/svg+xml,' + encodeURIComponent(svg),
          downloadable: true,
          fileName: 'Rappid'
        }).open();
      },
      { preserveDimensions: true, convertImagesToDataUris: true }
    );
  }

  openAsPNG() {
    this.paper.toPNG(
      (dataURL: string) => {
        new joint.ui.Lightbox({
          image: dataURL,
          downloadable: true,
          fileName: 'Rappid'
        }).open();
      },
      { padding: 10 }
    );
  }

  onMousewheel(
    cellView: joint.dia.CellView,
    evt: JQuery.Event,
    ox: number,
    oy: number,
    delta: number
  ) {
    if (this.keyboardService.keyboard.isActive('alt', evt)) {
      evt.preventDefault();
      this.paperScroller.zoom(delta * 0.2, {
        min: 0.2,
        max: 5,
        grid: 0.2,
        ox,
        oy
      });
    }
  }

  renderPlugin(selector: string, plugin: any): void {
    let select = this.el.querySelector(selector);
    if (select) {
      this.el.querySelector(selector).appendChild(plugin.el);
    }
    plugin.render();
  }
  createRapidElement(suggestedElementModel: ArisWorkflowItemCustomData) {
    const itemConfig = this.stencilService.getShapeModel(
      suggestedElementModel.Type
    );
    let elementToAdd;

    if (itemConfig.customData.Type === ArisWorkflowType.Conditional) {
      elementToAdd = new joint.shapes.standard.InscribedImage(itemConfig);
      elementToAdd.attr('label/text', '');
      elementToAdd.resize(40, 40);
    } else {
      elementToAdd = new joint.shapes.standard.Rectangle(itemConfig);
      elementToAdd.removeAttr('body/refY');
      elementToAdd.removeAttr('body/refX');
      elementToAdd.attr('label/refY', '60%');
      let title = suggestedElementModel.Title.length > 30 ? suggestedElementModel.Title.slice(0, 29) + '...' : suggestedElementModel.Title;
      elementToAdd.attr('label/text', title);
      elementToAdd.attr('root/dataTooltip', suggestedElementModel.Title);
      elementToAdd.resize(250, 40);
    }
    

    elementToAdd.attributes.customData = Object.assign(this.getCustomData(elementToAdd), suggestedElementModel);
    elementToAdd.attributes.position.x = suggestedElementModel.X;
    elementToAdd.attributes.position.y = suggestedElementModel.Y;
    elementToAdd.attributes.customData.Id = elementToAdd.id;
    return elementToAdd;
  }

  getCustomData(workflowItemModel: any) {
    workflowItemModel.attributes.customData = Object.assign(
      new ArisWorkflowItemCustomData(),
      workflowItemModel.attributes.customData
    );
    return workflowItemModel.attributes
      .customData as ArisWorkflowItemCustomData;
  }

  createLink() {
    const link = new joint.shapes.standard.Link({
      router: {
        name: 'normal'
      },
      connector: {
        name: 'rounded'
      }
    });

    link.attr({
      line: {
        stroke: '#c8d6e5',
        'stroke-width': 2,
        'stroke-dasharray': '0'
      }
    });

    return link;
  }
}

export default KitchenSinkService;
