import {
  DeleteRole,
  DeletePeople,
  UpdatePeople,
  AddPeople,
  MoveHierarchyType,
  MoveHierarchy,
  GetArisWorkflowVersionsByDate,
  GetGenericWorkflowVersionsByDate,
  GetIssueTypeFieldsNames,
  GetIssueTypeFields,
  DeleteSprintIssue,
  DeleteIssue,
  GetRequirementTypes,
  AddRequirementType,
  UpdateRequirementType,
  DeleteRequirementType,
  GetRequirementPriorities,
  AddRequirementPriority,
  UpdateRequirementPriority,
  UpdateIndexesRequirementPriority,
  DeleteRequirementPriority,
  GetTypical,
  GetTypicalAttribute,
  GetIssueResources,
  DeleteIssueResource,
  GetIssuesByIssueType,
  GetLinkedIssues,
  LinkIssue,
  UnlinkIssue,
  GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType,
  UpdateSprint,
  DeleteSprint,
  GetSprint,
  GetPeopleByUserFromAdmin
} from './../commands-queries/workflow-command-queries';
import { Observable, of, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Process } from 'src/app/core/models/process.model';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';
import { GenericWorkflow } from '../models/generic-workflow.model';
import { ArisWorkflow } from '../models/aris-workflow.model';
import { ArisWorkflowDetails } from '../models/aris-workflow-details.model';
import { TestCase } from './../../test-generation/models/test-case.model';

import {
  GetProcesses,
  GetArisWorkflows,
  GetArisWorkflow,
  SaveArisWorkflow,
  AddArisWorkflow,
  DeleteArisWorkflow,
  AddProcess,
  DeleteProcess,
  IssueArisWorkflow,
  RenameArisWorkflow,
  RollbackArisWorkflow,
  GetGenericWorkflows,
  GetGenericWorkflow,
  AddGenericWorkflow,
  SearchArisWorkflows,
  UpdateProcess,
  UpdateIssueType,
  AddIssueType,
  DeleteIssueType,
  IssueGenericWorkflow,
  SaveGenericWorkflow,
  RenameGenericWorkflow,
  DeleteGenericWorkflow,
  GetIssueTypes,
  AddHierarchyType,
  UpdateHierarchyType,
  GetHierarchyTypes,
  GetRequirementPrioritizationReport,
  DeleteHierarchyType,
  AddIssue,
  UpdateIssue,
  GetHierarchiesByParent,
  GetHierarchyStatistics,
  GetProcessStatistics,
  GetHierarchyType,
  GetPeoples,
  GetHierarchy,
  AddHierarchy,
  UpdateHierarchy,
  DeleteHierarchy,
  SearchHierarchies,
  GetIssues,
  GetIssuesWithDetails,
  GetWorkflowIssues,
  GetIssueWorkflows,
  GetWorkflows,
  GetIssue,
  GetWorkflow,
  GetOpenSprintBySubProject,
  GetSprintsBySubProject,
  GetSprintIssues,
  GetProcessesBySubProject,
  GetAllProcessWorkflowsBySubProject,
  GetAllProcessTestCasesBySubProject,
  GetAllProcessOpenIssuesBySubProject,
  GetSprintIssuesWorkflow,
  GetSprintIssuesTestCase,
  PasteProcess,
  PasteHierarchy,
  GetProcessWorkflows,
  GetHierarchies,
  GetHierarchiesStatistics,
  GetProcessResources,
  DeleteProcessResources,
  AddWorkflowIssue,
  UnlinkWorkflowIssue,
  AddProcessWorkflow,
  GetProcess,
  GetArisWorkflowVersions,
  GetGenericWorkflowVersions,
  RollbackGenericWorkflow,
  GetRoles,
  AddRole,
  UpdateRole,
  AddSprint,
  GetPeople,
  UpdateSprintIssue,
  GetFolders,
  GetLastHierarchyType,
  GetIssuesForSprintPlannerWithFilters,
  AddSprintIssue,
  DeleteProcessWorkflow,
  GetTraceabilityReportLine,
  GetTraceabilityReportDetail,
  GetRequirementType,
  GetRequirementPriority,
  UnlinkIssueFromProcess,
  LinkIssueToProcess
} from '../commands-queries/workflow-command-queries';
import { CommandMethod } from 'src/app/shared/command-method';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { ArisNodeType } from 'src/app/shared/aris-node-type';
import { Injectable } from '@angular/core';
import { ArisTreeViewItem } from 'src/app/shared/aris-tree-view-item';
import { IssueType } from '../models/issue-type.model';
import { HierarchyType } from '../models/hierarchy-type.model';
import { GetSystems, UpdateSystem } from 'src/app/design/commands-queries/workflow-command-queries';
import { WorkflowIssueModel } from '../models/workflow-issue.model';
import { Sprint } from 'src/app/core/models/sprint.model';
import { Issue } from '../models/issue.model';
import { Workflow } from '../models/workflow.model';
import { Statistics } from '../models/statistics.model';
import { Role } from '../models/role.model';

import { SessionService } from 'src/app/core/services/session.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { util } from '../../../vendor/rappid';
import interpolate = util.interpolate;
import { Cacheable } from 'ngx-cacheable';
import { System } from '../../design/models/system.model';
import { Guid } from '@syncfusion/ej2-pdf-export';

@Injectable()
export class ProcessManagementService {
  private browseUrl: string;
  private arisUrl: string;
  private designUrl: string;
  private genericUrl: string;
  private configureUrl: string;
  private peopleUrl: string;
  private sprintUrl: string;
  private reportsUrl: string;
  private dataUrl: string;
  private subject = new Subject<any>();
  private apiURL: string;
  private jiraURL: string;

  issueReqWorkflowUpdated: Subject<any> = new Subject<any>();
  issueReqWorkflowUpdated$ = this.issueReqWorkflowUpdated.asObservable();

  hierarchyAdded: Subject<any> = new Subject<any>();
  hierarchyAdded$ = this.hierarchyAdded.asObservable();

  hierarchyRemoved: Subject<any> = new Subject<any>();
  hierarchyRemoved$ = this.hierarchyRemoved.asObservable();

  processAdded: Subject<any> = new Subject<any>();
  processAdded$ = this.processAdded.asObservable();

  processRemoved: Subject<any> = new Subject<any>();
  processRemoved$ = this.processRemoved.asObservable();
  previewProcess: Subject<any> = new Subject<any>();
  previewProcess$ = this.previewProcess.asObservable();

  processUpdated: Subject<any> = new Subject<any>();
  processUpdated$ = this.processUpdated.asObservable();

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private http: HttpClient,
    private sessionService: SessionService
  ) {
    this.browseUrl = `${configurationService.serverSettings.apiUrl}Process/Browse/`;
    this.arisUrl = `${configurationService.serverSettings.apiUrl}Process/ArisWorkflow/`;
    this.genericUrl = `${configurationService.serverSettings.apiUrl}Process/GenericWorkflow/`;
    this.configureUrl = `${configurationService.serverSettings.apiUrl}Process/Configure/`;
    this.peopleUrl = `${configurationService.serverSettings.apiUrl}People/`;
    this.designUrl = `${configurationService.serverSettings.apiUrl}Design/`;
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Data/`;
    this.sprintUrl = `${configurationService.serverSettings.apiUrl}Process/Sprint/`;
    this.reportsUrl = `${configurationService.serverSettings.apiUrl}Process/Reports/`;
    this.jiraURL = `${configurationService.serverSettings.apiUrl}Process/Jira/`;
    this.apiURL = `${configurationService.serverSettings.apiUrl}`;
  }

  sendMessage(message: string) {
    this.subject.next({ text: message });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  getProcesses(hierarchyId: string, subProjectId: string): Observable<Process[]> {
    return this.httpExecutor.executeQuery<Process[]>(this.browseUrl, new GetProcesses(hierarchyId, subProjectId));
  }

  addProcess(
    title: string,
    description: string,
    expectedNumberOfTestCases: number,
    hierarchyId: string,
    subProjectId: string,
    createdId: string,
    ownerId: string,
    processType: number,
    requirementPriorityId: number,
    requirementTypeId: number,
    typicalId: string
  ): Observable<Process> {
    return this.httpExecutor.executeCommand<Process>(
      this.browseUrl,
      new AddProcess(
        title,
        description,
        expectedNumberOfTestCases,
        hierarchyId,
        subProjectId,
        createdId,
        ownerId,
        processType,
        requirementPriorityId,
        requirementTypeId,
        typicalId
      ),
      CommandMethod.POST
    );
  }

  updateProcess(
    id: string,
    title: string,
    description: string,
    expectedNumberOfTestCases: number,
    hierarchyId: string,
    subProjectId: string,
    createdId: string,
    ownerId: string,
    processType: number,
    requirementPriorityId: number,
    requirementTypeId: number,
    typicalId: string
  ): Observable<Process> {
    return this.httpExecutor.executeCommand<Process>(
      this.browseUrl,
      new UpdateProcess(
        id,
        title,
        description,
        expectedNumberOfTestCases,
        hierarchyId,
        subProjectId,
        createdId,
        ownerId,
        processType,
        requirementPriorityId,
        requirementTypeId,
        typicalId
      ),
      CommandMethod.PUT
    );
  }

  deleteProcess(id: string, forceDelete: boolean): Observable<Process> {
    return this.httpExecutor.executeCommand<Process>(this.browseUrl, new DeleteProcess(id, forceDelete), CommandMethod.DELETE);
  }

  getArisWorkflows(processId: string): Observable<ArisWorkflow[]> {
    return this.httpExecutor.executeQuery<ArisWorkflow[]>(this.arisUrl, new GetArisWorkflows(processId));
  }

  getArisWorkflow(id: string): Observable<ArisWorkflowDetails> {
    return this.httpExecutor.executeQuery<ArisWorkflowDetails>(this.arisUrl, new GetArisWorkflow(id));
  }

  issueArisWorkflow(arisWorkflowId: string, arisWorkflowImage: string, newVersion: boolean): Observable<ArisWorkflow> {
    return this.httpExecutor.executeCommand<ArisWorkflow>(
      this.arisUrl,
      new IssueArisWorkflow(arisWorkflowId, arisWorkflowImage, newVersion),
      CommandMethod.POST
    );
  }

  rollbackArisWorkflow(arisWorkflowId: string, version: number): Observable<ArisWorkflow> {
    return this.httpExecutor.executeCommand<ArisWorkflow>(
      this.arisUrl,
      new RollbackArisWorkflow(arisWorkflowId, version),
      CommandMethod.PUT
    );
  }

  renameArisWorkflow(id: string, title: string): Observable<ArisWorkflow> {
    return this.httpExecutor.executeCommand<ArisWorkflow>(this.arisUrl, new RenameArisWorkflow(id, title), CommandMethod.PUT);
  }

  fetchArisWorkflows(processId: string): Observable<ArisTreeViewItem[]> {
    return this.getArisWorkflows(processId).pipe(
      map(workflows => {
        return workflows.map(item => new ArisTreeViewItem(item.id, item.title, ArisNodeType.ArisWorkflow, [], processId));
      })
    );
  }

  saveArisWorkflow(id: string, designerJson: string): Observable<ArisWorkflow> {
    return this.httpExecutor.executeCommand(this.arisUrl, new SaveArisWorkflow(id, designerJson), CommandMethod.PUT);
  }

  addArisWorkflow(processId: string, title: string, description: string): Observable<ArisWorkflow> {
    return this.httpExecutor.executeCommand(this.arisUrl, new AddArisWorkflow(processId, title, description), CommandMethod.POST);
  }

  deleteArisWorkflow(id: string): Observable<ArisWorkflow> {
    return this.httpExecutor.executeCommand(this.arisUrl, new DeleteArisWorkflow(id), CommandMethod.DELETE);
  }

  searchArisWorkflows(searchString: string, subProjectId: string): Observable<ArisWorkflow[]> {
    return this.httpExecutor.executeQuery<ArisWorkflow[]>(this.arisUrl, new SearchArisWorkflows(searchString, subProjectId));
  }

  getGenericWorkflows(processId: string): Observable<GenericWorkflow[]> {
    return this.httpExecutor.executeQuery(this.genericUrl, new GetGenericWorkflows(processId));
  }

  getGenericWorkflow(id: string): Observable<GenericWorkflow> {
    return this.httpExecutor.executeQuery(this.genericUrl, new GetGenericWorkflow(id));
  }

  addGenericWorkflow(processId: string, title: string, description: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.genericUrl, new AddGenericWorkflow(processId, title, description), CommandMethod.POST);
  }

  issueGenericWorkflow(genericWorkflowId: string, genericWorkflowImage: string, newVersion: boolean): Observable<GenericWorkflow> {
    return this.httpExecutor.executeCommand(
      this.genericUrl,
      new IssueGenericWorkflow(genericWorkflowId, genericWorkflowImage, newVersion),
      CommandMethod.POST
    );
  }

  rollbackGenericWorkflow(genericWorkflowId: string, version: number): Observable<GenericWorkflow> {
    return this.httpExecutor.executeCommand(this.genericUrl, new RollbackGenericWorkflow(genericWorkflowId, version), CommandMethod.PUT);
  }

  saveGenericWorkflow(id: string, designerJson: string): Observable<GenericWorkflow> {
    return this.httpExecutor.executeCommand(this.genericUrl, new SaveGenericWorkflow(id, designerJson), CommandMethod.PUT);
  }

  renameGenericWorkflow(id: string, title: string): Observable<GenericWorkflow> {
    return this.httpExecutor.executeCommand(this.genericUrl, new RenameGenericWorkflow(id, title), CommandMethod.PUT);
  }

  deleteGenericWorkflow(id: string): Observable<GenericWorkflow> {
    return this.httpExecutor.executeCommand(this.genericUrl, new DeleteGenericWorkflow(id), CommandMethod.DELETE);
  }

  addHierarchyType(parentId: string, subProjectId: string, title: string, description: string, forceDelete: boolean): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.configureUrl,
      new AddHierarchyType(parentId, subProjectId, title, description, forceDelete),
      CommandMethod.POST
    );
  }

  updateHierarchyType(id: string, parentId: string, subProjectId: string, title: string, description: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.configureUrl,
      new UpdateHierarchyType(id, parentId, subProjectId, title, description),
      CommandMethod.PUT
    );
  }

  getHierarchyTypes(subProjectId: string): Observable<any> {
    return this.httpExecutor.executeQuery<HierarchyType[]>(this.configureUrl, new GetHierarchyTypes(subProjectId));
  }

  getLastHierarchyType(subProjectId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.configureUrl, new GetLastHierarchyType(subProjectId), CommandMethod.GET);
  }

  deleteHierarchyType(id: string, forceDelete: boolean): Observable<any> {
    return this.httpExecutor.executeCommand(this.configureUrl, new DeleteHierarchyType(id, forceDelete), CommandMethod.DELETE);
  }

  getIssueTypes(subProjectId: string): Observable<IssueType[]> {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetIssueTypes(subProjectId));
  }

  addIssueType(
    subProjectId: string,
    title: string,
    description: string,
    unit: string,
    isDefault: boolean,
    issueTypeFields: Array<Object>
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.configureUrl,
      new AddIssueType(subProjectId, title, description, unit, isDefault, issueTypeFields),
      CommandMethod.POST
    );
  }

  updateIssueType(
    id: string,
    subProjectId: string,
    title: string,
    description: string,
    unit: string,
    isDefault: boolean,
    issueTypeFields: Array<Object>
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.configureUrl,
      new UpdateIssueType(id, subProjectId, title, description, unit, isDefault, issueTypeFields),
      CommandMethod.PUT
    );
  }

  deleteIssueType(id: string, forceDelete: boolean): Observable<any> {
    return this.httpExecutor.executeCommand(this.configureUrl, new DeleteIssueType(id, forceDelete), CommandMethod.DELETE);
  }

  addIssue(
    processId: string,
    subProjectId: string,
    issueTypeId: string,
    assignedId: string,
    title: string,
    description: string,
    status: number,
    estimate: number,
    reason: string,
    implication: string,
    comment: string,
    typicalId: string,
    attribute: string,
    approvedForInvestigation: boolean,
    investigationStatus: number,
    changeStatus: number,
    reporterId: string,
    approvedId: string,
    dateSubmitted: string,
    approvalDate: string,
    startDate: string,
    priority: number,
    severity: number,
    dueDate: string,
    type: number,
    componentId: string,
    testCaseId: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.browseUrl,
      new AddIssue(
        processId,
        subProjectId,
        issueTypeId,
        assignedId,
        title,
        description,
        status,
        estimate,
        reason,
        implication,
        comment,
        typicalId,
        attribute,
        approvedForInvestigation,
        investigationStatus,
        changeStatus,
        reporterId,
        approvedId,
        dateSubmitted,
        approvalDate,
        startDate,
        priority,
        severity,
        dueDate,
        type,
        componentId,
        testCaseId
      ),
      CommandMethod.POST
    );
  }

  updateIssue(
    id: string,
    processId: string,
    subProjectId: string,
    issueTypeId: string,
    assignedId: string,
    title: string,
    description: string,
    status: number,
    estimate: number,
    reason: string,
    implication: string,
    comment: string,
    typicalId: string,
    attribute: string,
    approvedForInvestigation: boolean,
    investigationStatus: number,
    changeStatus: number,
    reporterId: string,
    approvedId: string,
    dateSubmitted: string,
    approvalDate: string,
    startDate: string,
    priority: number,
    severity: number,
    dueDate: string,
    type: number,
    componentId: string,
    testCaseId: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.browseUrl,
      new UpdateIssue(
        id,
        processId,
        subProjectId,
        issueTypeId,
        assignedId,
        title,
        description,
        status,
        estimate,
        reason,
        implication,
        comment,
        typicalId,
        attribute,
        approvedForInvestigation,
        investigationStatus,
        changeStatus,
        reporterId,
        approvedId,
        dateSubmitted,
        approvalDate,
        startDate,
        priority,
        severity,
        dueDate,
        type,
        componentId,
        testCaseId
      ),
      CommandMethod.PUT
    );
  }

  getHierarchiesByParent(subProjectId: string, hierarchyTypeId: string, parentId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetHierarchiesByParent(subProjectId, hierarchyTypeId, parentId));
  }

  getHierarchyStatistics(subProjectId: string, hierarchyId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetHierarchyStatistics(subProjectId, hierarchyId));
  }

  getProcessStatistics(processId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetProcessStatistics(processId));
  }

  getRequirementType(id: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetRequirementType(id));
  }

  getRequirementPriority(id: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetRequirementPriority(id));
  }

  getHierarchyType(id: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetHierarchyType(id));
  }

  getHierarchy(id: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetHierarchy(id));
  }

  getPeoples(subProjectId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.peopleUrl, new GetPeoples(subProjectId));
  }

  addHierarchy(parentId: string, subProjectId: string, hierarchyTypeId: string, title: string, description: string) {
    return this.httpExecutor.executeCommand(
      this.browseUrl,
      new AddHierarchy(parentId, subProjectId, hierarchyTypeId, title, description),
      CommandMethod.POST
    );
  }

  updateHierarchy(id: string, parentId: string, subProjectId: string, hierarchyTypeId: string, title: string, description: string) {
    return this.httpExecutor.executeCommand(
      this.browseUrl,
      new UpdateHierarchy(id, parentId, subProjectId, hierarchyTypeId, title, description),
      CommandMethod.PUT
    );
  }

  getProcessResources(processId: string) {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetProcessResources(processId));
  }

  deleteProcessResources(id: string) {
    return this.httpExecutor.executeCommand(this.browseUrl, new DeleteProcessResources(id), CommandMethod.DELETE);
  }

  deleteHierarchy(id: string, forceDelete: boolean) {
    return this.httpExecutor.executeCommand(this.browseUrl, new DeleteHierarchy(id, forceDelete), CommandMethod.DELETE);
  }

  searchHierarchies(subProjectId: string, parentId: string, searchCriteria: string) {
    return this.httpExecutor.executeQuery(this.browseUrl, new SearchHierarchies(subProjectId, parentId, searchCriteria));
  }

  getIssues(subProjectId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new GetIssues(subProjectId), CommandMethod.GET);
  }

  getIssuesWithDetails(subProjectId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new GetIssuesWithDetails(subProjectId), CommandMethod.GET);
  }

  getIssue(issueId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new GetIssue(issueId), CommandMethod.GET);
  }

  getWorkflow(WorkflowId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.designUrl, new GetWorkflow(WorkflowId), CommandMethod.GET);
  }

  addWorkflowIssue(workflowId: string, issueId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new AddWorkflowIssue(workflowId, issueId), CommandMethod.POST);
  }

  unlinkWorkflowIssue(workflowId: string, issueId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new UnlinkWorkflowIssue(workflowId, issueId), CommandMethod.DELETE);
  }

  linkIssueToProcess(issueId: string, processId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new LinkIssueToProcess(issueId, processId), CommandMethod.PUT);
  }

  unlinkIssueFromProcess(issueId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new UnlinkIssueFromProcess(issueId), CommandMethod.PUT);
  }

  getWorkflowIssues(WorkflowId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new GetWorkflowIssues(WorkflowId), CommandMethod.GET);
  }

  getIssueWorkflows(issueId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new GetIssueWorkflows(issueId), CommandMethod.GET);
  }

  getWorkflows(folderId: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.designUrl, new GetWorkflows(folderId));
  }

  getProcessWorkflows(processId: string): Observable<any> {
    return this.httpExecutor.executeQuery<any>(this.browseUrl, new GetProcessWorkflows(processId));
  }

  addProcessWorkflow(processId: string, workflowId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new AddProcessWorkflow(processId, workflowId), CommandMethod.POST);
  }

  deleteProcessWorkflow(id: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new DeleteProcessWorkflow(id), CommandMethod.DELETE);
  }

  getSprintsBySubproject(subProjectId: string): Observable<Sprint[]> {
    return this.httpExecutor.executeQuery(this.sprintUrl, new GetSprintsBySubProject(subProjectId));
  }

  getOpenSprintBySubProject(subProjectId: string): Observable<Sprint[]> {
    return this.httpExecutor.executeQuery(this.sprintUrl, new GetOpenSprintBySubProject(subProjectId));
  }

  getSprintIssues(sprintId: string): Observable<Issue[]> {
    return this.httpExecutor.executeQuery(this.sprintUrl, new GetSprintIssues(sprintId));
  }

  getProcessesBySubProject(subProjectId: string): Observable<Process[]> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetProcessesBySubProject(subProjectId));
  }

  getAllProcessWorkflowsBySubProject(subProjectId: string): Observable<Workflow[]> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetAllProcessWorkflowsBySubProject(subProjectId));
  }

  getAllProcessTestCasesBySubProject(subProjectId: string): Observable<TestCase[]> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetAllProcessTestCasesBySubProject(subProjectId));
  }

  getAllProcessOpenIssuesBySubProject(subProjectId: string): Observable<Issue[]> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetAllProcessOpenIssuesBySubProject(subProjectId));
  }

  getHierarchies(subProjectId: string, parentId: string): Observable<Statistics[]> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetHierarchies(subProjectId, parentId));
  }

  getHierarchiesStatistics(subProjectId: string, parentId: string): Observable<Statistics[]> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetHierarchiesStatistics(subProjectId, parentId));
  }

  getSprintIssuesWorkflow(sprintId: string): Observable<Workflow[]> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetSprintIssuesWorkflow(sprintId));
  }

  getSprintIssuesTestCase(sprintId: string): Observable<TestCase[]> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetSprintIssuesTestCase(sprintId));
  }

  pasteProcess(processId: string, hierarchyId: string, type: number): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new PasteProcess(processId, hierarchyId, type), CommandMethod.POST);
  }

  pasteHierarchy(hierarchyId: string, parentId: string, pasteType: number): Observable<any> {
    return this.httpExecutor.executeCommand(this.browseUrl, new PasteHierarchy(hierarchyId, parentId, pasteType), CommandMethod.POST);
  }

  getProcess(id: string): Observable<Process> {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetProcess(id));
  }

  getArisWorkflowVersions(arisWorkflowId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.arisUrl, new GetArisWorkflowVersions(arisWorkflowId));
  }

  getGenericWorkflowVersions(genericWorkflowId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.genericUrl, new GetGenericWorkflowVersions(genericWorkflowId));
  }

  getRoles(subProjectId: string): Observable<Role[]> {
    return this.httpExecutor.executeQuery(this.peopleUrl, new GetRoles(subProjectId));
  }

  addRole(title: string, description: string, subProjectId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.peopleUrl, new AddRole(title, description, subProjectId), CommandMethod.POST);
  }

  updateRole(id: string, title: string, description: string, subProjectId: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.peopleUrl, new UpdateRole(id, title, description, subProjectId), CommandMethod.PUT);
  }

  deleteRole(id: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.peopleUrl, new DeleteRole(id), CommandMethod.DELETE);
  }

  addPeople(
    roleId: string,
    name: string,
    surname: string,
    email: string,
    address: string,
    userIdFromAdminPanel: string,
    subProjectId: string,
    color: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.peopleUrl,
      new AddPeople(roleId, name, surname, email, address, userIdFromAdminPanel, subProjectId, color),
      CommandMethod.POST
    );
  }

  updatePeople(
    id: string,
    roleId: string,
    name: string,
    surname: string,
    email: string,
    address: string,
    userIdFromAdminPanel: string,
    subProjectId: string,
    color: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.peopleUrl,
      new UpdatePeople(id, roleId, name, surname, email, address, userIdFromAdminPanel, subProjectId, color),
      CommandMethod.PUT
    );
  }

  deletePeople(id: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.peopleUrl, new DeletePeople(id), CommandMethod.DELETE);
  }

  addSprint(
    title: string,
    description: string,
    start: string,
    end: string,
    subProjectId: string,
    duration: number,
    durationUnit: number
  ): Observable<Sprint> {
    return this.httpExecutor.executeCommand(
      this.sprintUrl,
      new AddSprint(title, description, start, end, subProjectId, duration, durationUnit),
      CommandMethod.POST
    );
  }

  deleteSprint(id: string) {
    return this.httpExecutor.executeCommand(this.sprintUrl, new DeleteSprint(id), CommandMethod.DELETE);
  }

  updateSprint(
    id: string,
    title: string,
    description: string,
    start: string,
    end: string,
    subProjectId: string,
    duration: number,
    durationUnit: number
  ): Observable<Sprint> {
    return this.httpExecutor.executeCommand(
      this.sprintUrl,
      new UpdateSprint(id, title, description, start, end, subProjectId, duration, durationUnit),
      CommandMethod.PUT
    );
  }

  getFolders(subProjectId: string) {
    return this.httpExecutor.executeQuery(this.designUrl, new GetFolders(subProjectId));
  }

  getPeople(id: string) {
    return this.httpExecutor.executeQuery(this.peopleUrl, new GetPeople(id));
  }

  getSprint(id: string) {
    return this.httpExecutor.executeQuery(this.sprintUrl, new GetSprint(id));
  }

  startSprint(id: string) {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const url = this.sprintUrl + 'StartSprint?Id=' + id;
    return this.http.put(url, {}, { headers: headers }).pipe(
      map((res: any) => {
        return this.httpExecutor.handleResponse(url, res);
      }),
      catchError(error => {
        return this.httpExecutor.handleError(error);
      })
    );
  }

  endSprint(id: string) {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const url = this.sprintUrl + 'EndSprint?Id=' + id;
    return this.http.put(url, {}, { headers: headers }).pipe(
      map((res: any) => {
        return this.httpExecutor.handleResponse(url, res);
      }),
      catchError(error => {
        return this.httpExecutor.handleError(error);
      })
    );
  }

  updateSprintIssue(id: string, sprintId: string, issueId: string, status: number) {
    return this.httpExecutor.executeCommand(this.sprintUrl, new UpdateSprintIssue(id, sprintId, issueId, status), CommandMethod.PUT);
  }

  getIssuesForSprintPlannerWithFilters(subProjectId: string, processId: string, searchText: string, assigneeIds: Array<string>) {
    return this.httpExecutor.executeCommand(
      this.browseUrl,
      new GetIssuesForSprintPlannerWithFilters(subProjectId, processId, searchText, assigneeIds),
      CommandMethod.POST
    );
  }

  addSprintIssue(sprintId: string, issueId: string, status: number) {
    return this.httpExecutor.executeCommand(this.sprintUrl, new AddSprintIssue(sprintId, issueId, status), CommandMethod.POST);
  }

  moveHierarchyType(id: string) {
    return this.httpExecutor.executeCommand(this.configureUrl, new MoveHierarchyType(id), CommandMethod.PUT);
  }

  moveHierarchy(fromHierarchyId: string, toHierarchyId: string) {
    return this.httpExecutor.executeCommand(this.browseUrl, new MoveHierarchy(fromHierarchyId, toHierarchyId), CommandMethod.PUT);
  }

  getTraceabilityReportLine(subProjectId: string, Level?: number, Id?: string, issueTypes?: any[]) {
    return this.httpExecutor.executeQuery(this.reportsUrl, new GetTraceabilityReportLine(subProjectId, Level, Id, issueTypes));
  }

  getTraceabilityReportDetail(subProjectId: string, Id: string, Level?: number, issueTypes?: any[]) {
    return this.httpExecutor.executeQuery(this.reportsUrl, new GetTraceabilityReportDetail(subProjectId, Id, Level, issueTypes));
  }

  GetRequirementPrioritizationReport(subProjectId: string) {
    return this.httpExecutor.executeQuery(this.reportsUrl, new GetRequirementPrioritizationReport(subProjectId));
  }

  getArisWorkflowVersionsByDate(arisWorkflowId: string, dateFrom: string, dateTo: string) {
    return this.httpExecutor.executeQuery(this.arisUrl, new GetArisWorkflowVersionsByDate(arisWorkflowId, dateFrom, dateTo));
  }

  getGenericWorkflowVersionsByDate(genericWorkflowId: string, dateFrom: string, dateTo: string) {
    return this.httpExecutor.executeQuery(this.genericUrl, new GetGenericWorkflowVersionsByDate(genericWorkflowId, dateFrom, dateTo));
  }

  getIssueTypeFieldsNames() {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetIssueTypeFieldsNames());
  }

  getIssueTypeFields(issueTypeId: string) {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetIssueTypeFields(issueTypeId));
  }

  GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType(subProjectId: string, issueTypeId: string, processId: string) {
    return this.httpExecutor.executeQuery(
      this.browseUrl,
      new GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType(subProjectId, issueTypeId, processId)
    );
  }

  deleteSprintIssue(id: string) {
    return this.httpExecutor.executeCommand(this.sprintUrl, new DeleteSprintIssue(id), CommandMethod.DELETE);
  }

  deleteIssue(id: string) {
    return this.httpExecutor.executeCommand(this.browseUrl, new DeleteIssue(id), CommandMethod.DELETE);
  }

  getTypical(id: string) {
    return this.httpExecutor.executeQuery(this.dataUrl, new GetTypical(id));
  }

  getTypicalAttribute(typicalId: string, name: string) {
    return this.httpExecutor.executeQuery(this.dataUrl, new GetTypicalAttribute(typicalId, name));
  }

  getIssueResources(issueId: string) {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetIssueResources(issueId));
  }

  getIssuesByIssueType(subProjectId: string, issueTypeId: string) {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetIssuesByIssueType(subProjectId, issueTypeId));
  }

  getLinkedIssues(issueId: string) {
    return this.httpExecutor.executeQuery(this.browseUrl, new GetLinkedIssues(issueId));
  }

  deleteIssueResource(id: string) {
    return this.httpExecutor.executeCommand(this.browseUrl, new DeleteIssueResource(id), CommandMethod.DELETE);
  }

  linkIssue(srcIssueId: string, linkType: number, dstIssueId: string) {
    return this.httpExecutor.executeCommand(this.browseUrl, new LinkIssue(srcIssueId, linkType, dstIssueId), CommandMethod.POST);
  }

  unlinkIssue(srcIssueId: string, dstIssueId: string) {
    return this.httpExecutor.executeCommand(this.browseUrl, new UnlinkIssue(srcIssueId, dstIssueId), CommandMethod.DELETE);
  }

  getPeopleByUserFromAdmin(id: number, subProjectId: string) {
    return this.httpExecutor.executeQuery(this.peopleUrl, new GetPeopleByUserFromAdmin(id, subProjectId));
  }

  addIssueResources(issueId, fileCollection) {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const url = this.browseUrl + 'AttachIssueResource';
    const formData = new FormData();
    formData.append('IssueId', issueId);
    for (let i = 0; i < fileCollection.length; i++) {
      formData.append('FileCollection', fileCollection[i]);
    }
    return this.http.post(url, formData, { headers: headers }).pipe(
      map((res: any) => {
        return this.httpExecutor.handleResponse(url, res);
      }),
      catchError(error => {
        return this.httpExecutor.handleError(error);
      })
    );
  }

  getRequirementTypes(subProjectId: string): Observable<any[]> {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetRequirementTypes(subProjectId));
  }
  addRequirementType(subProjectId: string, title: string, code: string): Observable<any[]> {
    return this.httpExecutor.executeCommand(this.configureUrl, new AddRequirementType(subProjectId, title, code), CommandMethod.POST);
  }
  updateRequirementType(id: string, subProjectId: string, title: string, code: string): Observable<any[]> {
    return this.httpExecutor.executeCommand(this.configureUrl, new UpdateRequirementType(id, subProjectId, title, code), CommandMethod.PUT);
  }
  deleteRequirementType(id: string): Observable<any[]> {
    return this.httpExecutor.executeCommand(this.configureUrl, new DeleteRequirementType(id), CommandMethod.DELETE);
  }

  getRequirementPriorities(subProjectId: string): Observable<any[]> {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetRequirementPriorities(subProjectId));
  }
  addRequirementPriority(subProjectId: string, title: string, code: string): Observable<any[]> {
    return this.httpExecutor.executeCommand(this.configureUrl, new AddRequirementPriority(subProjectId, title, code), CommandMethod.POST);
  }
  updateRequirementPriority(id: string, subProjectId: string, title: string, code: string): Observable<any[]> {
    return this.httpExecutor.executeCommand(
      this.configureUrl,
      new UpdateRequirementPriority(id, subProjectId, title, code),
      CommandMethod.PUT
    );
  }

  UpdateIndexesRequirementPriority(data: Array<any>): Observable<any[]> {
    return this.httpExecutor.executeCommand(this.configureUrl, new UpdateIndexesRequirementPriority(data), CommandMethod.PUT);
  }

  deleteRequirementPriority(id: string): Observable<any[]> {
    return this.httpExecutor.executeCommand(this.configureUrl, new DeleteRequirementPriority(id), CommandMethod.DELETE);
  }

  downloadIssueResource(id: string): Observable<Blob> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    return this.http.get(this.browseUrl + 'DownloadIssueResource?Id=' + id, { responseType: 'blob', headers: headers });
  }

  GenerateHierarchyDocsByDate(hierarchyId: string, date: string, uiURL: string): Observable<Blob> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    return this.http.get(
      this.browseUrl +
        'GenerateHierarchyDocsByDate?HierarchyId=' +
        hierarchyId +
        '&Date=' +
        date +
        '&ApiURL=' +
        this.apiURL +
        '&UiURL=' +
        uiURL,
      { responseType: 'blob', headers: headers }
    );
  }

  GenerateHierarchyDocs(hierarchyId: string, uiURL: string): Observable<Blob> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    return this.http.get(
      this.browseUrl + 'GenerateHierarchyDocs?HierarchyId=' + hierarchyId + '&ApiURL=' + this.apiURL + '&UiURL=' + uiURL,
      { responseType: 'blob', headers: headers }
    );
  }

  GenerateRequirementDocsByDate(processId: string, date: string, uiURL: string): Observable<Blob> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    return this.http.get(
      this.browseUrl +
        'GenerateRequirementDocsByDate?ProcessId=' +
        processId +
        '&Date=' +
        date +
        '&ApiURL=' +
        this.apiURL +
        '&UiURL=' +
        uiURL,
      { responseType: 'blob', headers: headers }
    );
  }

  GenerateRequirementDocs(processId: string, uiURL: string): Observable<Blob> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    return this.http.get(this.browseUrl + 'GenerateRequirementDocs?ProcessId=' + processId + '&ApiURL=' + this.apiURL + '&UiURL=' + uiURL, {
      responseType: 'blob',
      headers: headers
    });
  }
}
