import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class WorkflowIssuesDataService {
  constructor() {}

  // workflow ID
  // private workflowSelected = new Subject<string>();
  // workflowSelected$ = this.workflowSelected.asObservable();

  // process Id
  private processIdSelected = new Subject<string>();
  processIdSelected$ = this.processIdSelected.asObservable();

  // changeWorkflowId(workflowId: string) {
  //   this.workflowSelected.next(workflowId);
  // }

  changeProcessId(processId: string) {
    this.processIdSelected.next(processId);
  }
}
