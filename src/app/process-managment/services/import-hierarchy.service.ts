import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { SessionService } from 'src/app/core/services/session.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';

@Injectable()
export class ImportHierarchyService {
  private browseUrl: string;

  constructor(
        private configuration: ConfigurationService,
        private http: HttpClient,
        private sessionService: SessionService
    ) {
      this.browseUrl = `${configuration.serverSettings.apiUrl}Process/Browse/`;
  }


  importHierarchy(formModel): Observable<any> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const url = this.browseUrl + 'ImportHierarchy';
    return this.http.post(url, formModel, {headers : headers,
      reportProgress: true, observe: 'events'
    });
  }
  
 }

