// tslint:disable:max-line-length
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GetSubProject } from 'src/app/core/services/command-queries/command-queries';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';
import { CommandMethod } from 'src/app/shared/command-method';
import { AddJiraIssueTypeMapping, AddJiraProjectMapping, AddJiraUserMapping, AddJiraWebhooks, DeleteJiraIssueTypeMapping, DeleteJiraUserMapping, FirstTimeSync, GetIssueTypeFieldsAndAttributes, GetIssueTypeRequirementFields, GetIssueTypes, GetJiraIntegrationSummary, GetJiraIssueTypeFieldsAndAttributes, GetJiraIssueTypeListByProjectMapping, GetJiraIssueTypeMapping, GetJiraIssueTypes, GetJiraProjectMapping, GetJiraProjects, GetJiraUserMapping, GetJiraUsers, GetJiraUserSubprojectConfigs, GetJiraWebhooks, GetSenseaiIssueTypes, GetSenseAiPeoples, HasValidJiraCredentials, SetJiraUserSubprojectConfigs, UpdateJiraProjectMapping, UpdateJiraWebhooks, UpdateSubProjectJiraCredentials } from '../commands-queries/workflow-command-queries';

@Injectable()
export class JiraIntegrationService {
  private jiraUrl: string;
  private configureUrl: string;
  private peopleUrl: string;
  // private identityUrl: string;
  private administrationUrl: string;

  constructor(configuration: ConfigurationService, private httpExecutor: HttpExecutorService, private http: HttpClient) {
    this.jiraUrl = `${configuration.serverSettings.apiUrl}Process/Jira/`;
    this.configureUrl = `${configuration.serverSettings.apiUrl}Process/Configure/`;
    this.peopleUrl = `${configuration.serverSettings.apiUrl}People/`;
    // this.identityUrl = `${configuration.serverSettings.identityUrl}User/`;
    this.administrationUrl = `${configuration.serverSettings.apiUrl}Administration/`;
  }

  GetSubProject(id) {
    return this.httpExecutor.executeQuery(this.administrationUrl, new GetSubProject(id));
  }

  SetJiraUserSubprojectConfigs(userId, subprojectId, jiraUsername, jiraToken, jiraUrl) {
    return this.httpExecutor.executeCommand(this.jiraUrl, new SetJiraUserSubprojectConfigs(userId, subprojectId, jiraUsername, jiraToken, jiraUrl), CommandMethod.POST);
  }

  GetJiraUserSubprojectConfigs(subprojectId: string, userId: string) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraUserSubprojectConfigs(subprojectId, userId));
  }

  HasValidJiraCredentials(subprojectId, skipIsEnabled) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new HasValidJiraCredentials(subprojectId, skipIsEnabled));
  }

  GetJiraProjects(subprojectId): Observable<any> {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraProjects(subprojectId));
  }

  AddJiraProjectMapping(projectId, subprojectId, jiraProjectKey, jiraProjectName, mapHierarchy, hierarchyParentId, hierarchyTypeId): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.jiraUrl,
      new AddJiraProjectMapping(projectId, subprojectId, jiraProjectKey, jiraProjectName, mapHierarchy, hierarchyParentId, hierarchyTypeId),
      CommandMethod.POST
    );
  }

  GetJiraProjectMapping(subprojectId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraProjectMapping(subprojectId));
  }

  UpdateJiraProjectMapping(id, projectId, subprojectId, jiraProjectKey, jiraProjectName, mapHierarchy, hierarchyParentId, hierarchyTypeId)  {
    return this.httpExecutor.executeCommand(
      this.jiraUrl,
      new UpdateJiraProjectMapping(id, projectId, subprojectId, jiraProjectKey, jiraProjectName, mapHierarchy, hierarchyParentId, hierarchyTypeId) ,
      CommandMethod.PUT
    );
  }

  GetJiraIssueTypes(projectKey: string, subprojectId: string) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraIssueTypes(projectKey, subprojectId));
  }

  GetSenseAiIssueTypes(subprojectId) {
    return this.httpExecutor.executeQuery(this.configureUrl, new GetIssueTypes(subprojectId));
  }

  GetIssueTypeRequirementFields(subprojectId: string) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetIssueTypeRequirementFields(subprojectId));
  }

  GetSenseaiIssueTypes(subprojectId: string) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetSenseaiIssueTypes(subprojectId));
  }

  GetSenseAiIssueTypeFields(issueTypeId, subprojectId) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetIssueTypeFieldsAndAttributes(issueTypeId, subprojectId));
  }

  GetJiraIssueTypeFields(issueTypeId, jiraProjectKey, subprojectId) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraIssueTypeFieldsAndAttributes(issueTypeId, jiraProjectKey, subprojectId));
  }

  GetJiraIssueTypeMapping(projectMapId) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraIssueTypeMapping(projectMapId));
  }

  AddJiraIssueTypeMapping(id, jiraProjectMappingId, senseaiIssueTypeField, jiraIssueTypeFieldId, jiraIssueTypeFieldName, attributeMapping, allowAttachments, isUpdate) {
    return this.httpExecutor.executeCommand(
      this.jiraUrl,
      new AddJiraIssueTypeMapping(
        id,
        jiraProjectMappingId,
        senseaiIssueTypeField,
        jiraIssueTypeFieldId,
        jiraIssueTypeFieldName,
        attributeMapping,
        allowAttachments,
        isUpdate
      ),
      CommandMethod.POST
    );
  }

  DeleteJiraIssueTypeMapping(id: string) {
    return this.httpExecutor.executeCommand(this.jiraUrl, new DeleteJiraIssueTypeMapping(id), CommandMethod.POST);
  }

  GetJiraIssueTypeListByProjectMapping(jiraProjectMappingId: string) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraIssueTypeListByProjectMapping(jiraProjectMappingId));
  }

  GetJiraUsers(subprojectId) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraUsers(subprojectId));
  }

  GetPeoples(subprojectId: string) {
    return this.httpExecutor.executeQuery(this.peopleUrl, new GetSenseAiPeoples(subprojectId));
  }

  GetJiraUserMapping(subprojectId: string) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraUserMapping(subprojectId));
  }

  AddJiraUserMapping(subprojectId: string, jiraUserId: string, senseaiUserId: string) {
    return this.httpExecutor.executeCommand(
      this.jiraUrl,
      new AddJiraUserMapping(subprojectId, jiraUserId, senseaiUserId),
      CommandMethod.POST
    );
  }

  DeleteJiraUserMapping(id: string) {
    return this.httpExecutor.executeCommand(this.jiraUrl, new DeleteJiraUserMapping(id), CommandMethod.DELETE);
  }

  GetJiraIntegrationSummary(id: string) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraIntegrationSummary(id));
  }

  UpdateSubProjectJiraCredentials(id, isJiraIntegrationEnabled) {
    return this.httpExecutor.executeCommand(this.administrationUrl, new UpdateSubProjectJiraCredentials(id, isJiraIntegrationEnabled), CommandMethod.PUT);
  }

  FirstTimeSync(subprojectId, senseAiUsername, sync) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new FirstTimeSync(subprojectId, senseAiUsername, sync));
  }


  // START: WEBHOOKS
  // GetWebhooksEvents() {
  //   const url = 'http://localhost:9876/EventList';
  //   return this.http.get(url);
  // }

  SaveWebhook(subProjectId, name, events, filters, url) {
    return this.httpExecutor.executeCommand(this.jiraUrl, new AddJiraWebhooks(subProjectId, name, events, filters, url), CommandMethod.POST);
  }

  UpdateWebhook(subProjectId, name, events, filters, url, self, enabled) {
    return this.httpExecutor.executeCommand(this.jiraUrl, new UpdateJiraWebhooks(subProjectId, name, events, filters, url, self, enabled), CommandMethod.PUT);
  }

  GetJiraWebhooks(subprojectId) {
    return this.httpExecutor.executeQuery(this.jiraUrl, new GetJiraWebhooks(subprojectId));
  }

  // GetProjectName(projectKey) {
  //   const url = 'http://localhost:9876/GetProject?projectName=' + projectKey;
  //   return this.http.get(url);
  // }
  // END: WEBHOOKS
 }

