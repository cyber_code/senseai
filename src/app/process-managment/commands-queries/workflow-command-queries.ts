import { ApiAction } from 'src/app/core/services/command-queries/api-action';

export class GetProcesses extends ApiAction {
  hierarchyId: string;
  SubProjectId: string;

  constructor(hierarchyId: string, subProjectId: string) {
    super('GetProcesses');
    this.hierarchyId = hierarchyId;
    this.SubProjectId = subProjectId;
  }
}

export class GetProcessResources extends ApiAction {
  ProcessId: string;

  constructor(processId: string) {
    super('GetProcessResources');
    this.ProcessId = processId;
  }
}

export class DeleteProcessResources extends ApiAction {
  Id: string;

  constructor(id: string) {
    super('DeleteProcessResources');
    this.Id = id;
  }
}

export class AddProcess extends ApiAction {
  title: string;
  description: string;
  expectedNumberOfTestCases: number;
  hierarchyId: string;
  subProjectId: string;
  createdId: string;
  ownerId: string;
  processType: number;
  requirementPriorityId: number;
  requirementTypeId: number;
  typicalId: string;

  constructor(
    title: string,
    description: string,
    expectedNumberOfTestCases: number,
    hierarchyId: string,
    subProjectId: string,
    createdId: string,
    ownerId: string,
    processType: number,
    requirementPriorityId: number,
    requirementTypeId: number,
    typicalId: string
  ) {
    super('AddProcess');
    this.title = title;
    this.description = description;
    this.expectedNumberOfTestCases = expectedNumberOfTestCases;
    this.hierarchyId = hierarchyId;
    this.subProjectId = subProjectId;
    this.createdId = createdId;
    this.ownerId = ownerId;
    this.processType = processType;
    this.requirementPriorityId = requirementPriorityId;
    this.requirementTypeId = requirementTypeId;
    this.typicalId = typicalId;
  }
}

export class UpdateProcess extends ApiAction {
  id: string;
  title: string;
  description: string;
  expectedNumberOfTestCases: number;
  hierarchyId: string;
  subProjectId: string;
  createdId: string;
  ownerId: string;
  processType: number;
  requirementPriorityId: number;
  requirementTypeId: number;
  typicalId: string;

  constructor(
    id: string,
    title: string,
    description: string,
    expectedNumberOfTestCases: number,
    hierarchyId: string,
    subProjectId: string,
    createdId: string,
    ownerId: string,
    processType: number,
    requirementPriorityId: number,
    requirementTypeId: number,
    typicalId: string
  ) {
    super('UpdateProcess');
    this.id = id;
    this.title = title;
    this.description = description;
    this.expectedNumberOfTestCases = expectedNumberOfTestCases;
    this.hierarchyId = hierarchyId;
    this.subProjectId = subProjectId;
    this.createdId = createdId;
    this.ownerId = ownerId;
    this.processType = processType;
    this.requirementPriorityId = requirementPriorityId;
    this.requirementTypeId = requirementTypeId;
    this.typicalId = typicalId;
  }
}
export class GetArisWorkflows extends ApiAction {
  processId: string;

  constructor(processId: string) {
    super('GetArisWorkflows');
    this.processId = processId;
  }
}

export class GetArisWorkflow extends ApiAction {
  id: string;

  constructor(id: string) {
    super('GetArisWorkflow');
    this.id = id;
  }
}

export class IssueArisWorkflow extends ApiAction {
  arisWorkflowId: string;
  arisWorkflowImage: string;
  newVersion: boolean;

  constructor(arisWorkflowId: string, arisWorkflowImage: string, newVersion: boolean) {
    super('IssueArisWorkflow');
    this.arisWorkflowId = arisWorkflowId;
    this.arisWorkflowImage = arisWorkflowImage;
    this.newVersion = newVersion;
  }
}

export class RollbackArisWorkflow extends ApiAction {
  arisWorkflowId: string;
  version: number;

  constructor(arisWorkflowId: string, version: number) {
    super('RollbackArisWorkflow');
    this.arisWorkflowId = arisWorkflowId;
    this.version = version;
  }
}

export class RenameArisWorkflow extends ApiAction {
  id: string;
  title: string;

  constructor(id: string, title: string) {
    super('RenameArisWorkflow');
    this.id = id;
    this.title = title;
  }
}

export class SaveArisWorkflow extends ApiAction {
  id: string;
  designerJson: string;

  constructor(id: string, designerJson: string) {
    super('SaveArisWorkflow');
    this.id = id;
    this.designerJson = designerJson;
  }
}

export class AddArisWorkflow extends ApiAction {
  processId: string;
  title: string;
  description: string;

  constructor(processId: string, title: string, description: string) {
    super('AddArisWorkflow');
    this.processId = processId;
    this.title = title;
    this.description = description;
  }
}

export class DeleteArisWorkflow extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteArisWorkflow');
    this.id = id;
  }
}

export class SearchArisWorkflows extends ApiAction {
  searchString: string;
  subProjectId: string;

  constructor(searchString: string, subProjectId: string) {
    super('SearchArisWorkflows');
    this.searchString = searchString;
    this.subProjectId = subProjectId;
  }
}

export class GetGenericWorkflows extends ApiAction {
  processId: string;

  constructor(processId: string) {
    super('GetGenericWorkflows');
    this.processId = processId;
  }
}

export class GetGenericWorkflow extends ApiAction {
  id: string;

  constructor(id: string) {
    super('GetGenericWorkflow');
    this.id = id;
  }
}

export class AddGenericWorkflow extends ApiAction {
  processId: string;
  title: string;
  description: string;

  constructor(processId: string, title: string, description: string) {
    super('AddGenericWorkflow');
    this.processId = processId;
    this.title = title;
    this.description = description;
  }
}

export class GetIssueTypes extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetIssueTypes');
    this.subProjectId = subProjectId;
  }
}

export class GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType extends ApiAction {
  subProjectId: string;
  issueTypeId: string;
  processId: string;

  constructor(subProjectId: string, issueTypeId: string, processId: string) {
    super('GetUnlinkedIssuesAndIssueLinkedToASpecificRequirementByIssueType');
    this.subProjectId = subProjectId;
    this.issueTypeId = issueTypeId;
    this.processId = processId;
  }
}

export class AddIssueType extends ApiAction {
  subProjectId: string;
  title: string;
  description: string;
  unit: string;
  default: boolean;
  issueTypeFields: Array<Object>;

  constructor(subProjectId: string, title: string, description: string, unit: string, isDefault: boolean, issueTypeFields: Array<Object>) {
    super('AddIssueType');
    this.subProjectId = subProjectId;
    this.title = title;
    this.description = description;
    this.unit = unit;
    this.default = isDefault;
    this.issueTypeFields = issueTypeFields;
  }
}

export class IssueGenericWorkflow extends ApiAction {
  genericWorkflowId: string;
  genericWorkflowImage: string;
  newVersion: boolean;

  constructor(genericWorkflowId: string, genericWorkflowImage: string, newVersion: boolean) {
    super('IssueGenericWorkflow');
    this.genericWorkflowId = genericWorkflowId;
    this.genericWorkflowImage = genericWorkflowImage;
    this.newVersion = newVersion;
  }
}

export class RollbackGenericWorkflow extends ApiAction {
  genericWorkflowId: string;
  version: number;

  constructor(genericWorkflowId: string, version: number) {
    super('RollbackGenericWorkflow');
    this.genericWorkflowId = genericWorkflowId;
    this.version = version;
  }
}

export class SaveGenericWorkflow extends ApiAction {
  id: string;
  designerJson: string;

  constructor(id: string, designerJson: string) {
    super('SaveGenericWorkflow');
    this.id = id;
    this.designerJson = designerJson;
  }
}

export class RenameGenericWorkflow extends ApiAction {
  id: string;
  title: string;

  constructor(id: string, title: string) {
    super('RenameGenericWorkflow');
    this.id = id;
    this.title = title;
  }
}

export class DeleteGenericWorkflow extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteGenericWorkflow');
    this.id = id;
  }
}

export class GetHierarchies extends ApiAction {
  SubProjectId: string;
  HierarchyTypeId: string;

  constructor(subProjectId: string, HierarchyTypeId: string) {
    super('GetHierarchies');
    this.SubProjectId = subProjectId;
    this.HierarchyTypeId = HierarchyTypeId;
  }
}

export class GetHierarchy extends ApiAction {
  Id: string;

  constructor(Id: string) {
    super('GetHierarchy');
    this.Id = Id;
  }
}

export class GetHierarchiesByParent extends ApiAction {
  SubProjectId: string;
  HierarchyTypeId: string;
  ParentId: string;

  constructor(SubProjectId: string, HierarchyTypeId: string, ParentId: string) {
    super('GetHierarchiesByParent');
    this.SubProjectId = SubProjectId;
    this.HierarchyTypeId = HierarchyTypeId;
    this.ParentId = ParentId;
  }
}

export class AddHierarchyType extends ApiAction {
  parentId: string;
  subProjectId: string;
  title: string;
  description: string;
  forceDelete: boolean;

  constructor(parentId: string, subProjectId: string, title: string, description: string, forceDelete: boolean) {
    super('AddHierarchyType');
    this.parentId = parentId;
    this.subProjectId = subProjectId;
    this.title = title;
    this.description = description;
    this.forceDelete = forceDelete;
  }
}

export class UpdateHierarchyType extends ApiAction {
  id: string;
  parentId: string;
  subProjectId: string;
  title: string;
  description: string;

  constructor(id: string, parentId: string, subProjectId: string, title: string, description: string) {
    super('UpdateHierarchyType');
    this.id = id;
    this.parentId = parentId;
    this.subProjectId = subProjectId;
    this.title = title;
    this.description = description;
  }
}

export class GetHierarchyType extends ApiAction {
  id: string;

  constructor(id: string) {
    super('GetHierarchyType');
    this.id = id;
  }
}

export class GetHierarchyTypes extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetHierarchyTypes');
    this.subProjectId = subProjectId;
  }
}

export class GetRequirementPrioritizationReport extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetRequirementPrioritizationReport');
    this.subProjectId = subProjectId;
  }
}

export class GetLastHierarchyType extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetLastHierarchyType');
    this.subProjectId = subProjectId;
  }
}

export class DeleteHierarchyType extends ApiAction {
  id: string;
  forceDelete: boolean;

  constructor(id: string, forceDelete: boolean) {
    super('DeleteHierarchyType');
    this.id = id;
    this.forceDelete = forceDelete;
  }
}

export class UpdateIssueType extends ApiAction {
  id: string;
  subProjectId: string;
  title: string;
  description: string;
  unit: string;
  default: boolean;
  issueTypeFields: Array<Object>;

  constructor(
    id: string,
    subProjectId: string,
    title: string,
    description: string,
    unit: string,
    isDefault: boolean,
    issueTypeFields: Array<Object>
  ) {
    super('UpdateIssueType');
    this.id = id;
    this.subProjectId = subProjectId;
    this.title = title;
    this.description = description;
    this.unit = unit;
    this.default = isDefault;
    this.issueTypeFields = issueTypeFields;
  }
}

export class DeleteIssueType extends ApiAction {
  Id: string;
  ForceDelete: boolean;

  constructor(id: string, forceDelete: boolean) {
    super('DeleteIssueType');
    this.Id = id;
    this.ForceDelete = forceDelete;
  }
}

export class AddIssue extends ApiAction {
  processId: string;
  subProjectId: string;
  issueTypeId: string;
  assignedId: string;
  title: string;
  description: string;
  status: number;
  estimate: number;
  reason: string;
  implication: string;
  comment: string;
  typicalId: string;
  attribute: string;
  approvedForInvestigation: boolean;
  investigationStatus: number;
  changeStatus: number;
  reporterId: string;
  approvedId: string;
  dateSubmitted: string;
  approvalDate: string;
  startDate: string;
  priority: number;
  severity: number;
  dueDate: string;
  type: number;
  componentId: string;
  testCaseId: string;

  constructor(
    processId: string,
    subProjectId: string,
    issueTypeId: string,
    assignedId: string,
    title: string,
    description: string,
    status: number,
    estimate: number,
    reason: string,
    implication: string,
    comment: string,
    typicalId: string,
    attribute: string,
    approvedForInvestigation: boolean,
    investigationStatus: number,
    changeStatus: number,
    reporterId: string,
    approvedId: string,
    dateSubmitted: string,
    approvalDate: string,
    startDate: string,
    priority: number,
    severity: number,
    dueDate: string,
    type: number,
    componentId: string,
    testCaseId: string
  ) {
    super('AddIssue');
    this.processId = processId;
    this.subProjectId = subProjectId;
    this.issueTypeId = issueTypeId;
    this.assignedId = assignedId;
    this.title = title;
    this.description = description;
    this.status = status;
    this.estimate = estimate;
    this.reason = reason;
    this.implication = implication;
    this.comment = comment;
    this.typicalId = typicalId;
    this.attribute = attribute;
    this.approvedForInvestigation = approvedForInvestigation;
    this.investigationStatus = investigationStatus;
    this.changeStatus = changeStatus;
    this.reporterId = reporterId;
    this.approvedId = approvedId;
    this.dateSubmitted = dateSubmitted;
    this.approvalDate = approvalDate;
    this.startDate = startDate;
    this.priority = priority;
    this.severity = severity;
    this.dueDate = dueDate;
    this.type = type;
    this.componentId = componentId;
    this.testCaseId = testCaseId;
  }
}

export class UpdateIssue extends ApiAction {
  id: string;
  processId: string;
  subProjectId: string;
  issueTypeId: string;
  assignedId: string;
  title: string;
  description: string;
  status: number;
  estimate: number;
  reason: string;
  implication: string;
  comment: string;
  typicalId: string;
  attribute: string;
  approvedForInvestigation: boolean;
  investigationStatus: number;
  changeStatus: number;
  reporterId: string;
  approvedId: string;
  dateSubmitted: string;
  approvalDate: string;
  startDate: string;
  priority: number;
  severity: number;
  dueDate: string;
  type: number;
  componentId: string;
  testCaseId: string;

  constructor(
    id: string,
    processId: string,
    subProjectId: string,
    issueTypeId: string,
    assignedId: string,
    title: string,
    description: string,
    status: number,
    estimate: number,
    reason: string,
    implication: string,
    comment: string,
    typicalId: string,
    attribute: string,
    approvedForInvestigation: boolean,
    investigationStatus: number,
    changeStatus: number,
    reporterId: string,
    approvedId: string,
    dateSubmitted: string,
    approvalDate: string,
    startDate: string,
    priority: number,
    severity: number,
    dueDate: string,
    type: number,
    componentId: string,
    testCaseId: string
  ) {
    super('UpdateIssue');
    this.id = id;
    this.processId = processId;
    this.subProjectId = subProjectId;
    this.issueTypeId = issueTypeId;
    this.assignedId = assignedId;
    this.title = title;
    this.description = description;
    this.status = status;
    this.estimate = estimate;
    this.reason = reason;
    this.implication = implication;
    this.comment = comment;
    this.typicalId = typicalId;
    this.attribute = attribute;
    this.approvedForInvestigation = approvedForInvestigation;
    this.investigationStatus = investigationStatus;
    this.changeStatus = changeStatus;
    this.reporterId = reporterId;
    this.approvedId = approvedId;
    this.dateSubmitted = dateSubmitted;
    this.approvalDate = approvalDate;
    this.startDate = startDate;
    this.priority = priority;
    this.severity = severity;
    this.dueDate = dueDate;
    this.type = type;
    this.componentId = componentId;
    this.testCaseId = testCaseId;
  }
}

export class GetHierarchyStatistics extends ApiAction {
  SubProjectId: string;
  HierarchyId: string;

  constructor(SubProjectId: string, HierarchyId: string) {
    super('GetHierarchyStatistics');
    this.SubProjectId = SubProjectId;
    this.HierarchyId = HierarchyId;
  }
}

export class GetProcessStatistics extends ApiAction {
  ProcessId: string;

  constructor(ProcessId: string) {
    super('GetProcessStatistics');
    this.ProcessId = ProcessId;
  }
}

export class GetPeoples extends ApiAction {
  subProjectId: string;
  constructor(subProjectId) {
    super('GetPeoples');
    this.subProjectId = subProjectId;
  }
}

export class GetPeople extends ApiAction {
  id: string;

  constructor(id: string) {
    super('GetPeople');
    this.id = id;
  }
}

export class AddHierarchy extends ApiAction {
  parentId: string;
  subProjectId: string;
  hierarchyTypeId: string;
  title: string;
  description: string;

  constructor(parentId: string, subProjectId: string, hierarchyTypeId: string, title: string, description: string) {
    super('AddHierarchy');
    this.parentId = parentId;
    this.subProjectId = subProjectId;
    this.hierarchyTypeId = hierarchyTypeId;
    this.title = title;
    this.description = description;
  }
}

export class UpdateHierarchy extends ApiAction {
  id: string;
  parentId: string;
  subProjectId: string;
  hierarchyTypeId: string;
  title: string;
  description: string;

  constructor(id: string, parentId: string, subProjectId: string, hierarchyTypeId: string, title: string, description: string) {
    super('UpdateHierarchy');
    this.id = id;
    this.parentId = parentId;
    this.subProjectId = subProjectId;
    this.hierarchyTypeId = hierarchyTypeId;
    this.title = title;
    this.description = description;
  }
}

export class DeleteHierarchy extends ApiAction {
  id: string;
  forceDelete: boolean;

  constructor(id: string, forceDelete: boolean) {
    super('DeleteHierarchy');
    this.id = id;
    this.forceDelete = forceDelete;
  }
}

export class DeleteProcess extends ApiAction {
  id: string;
  forceDelete: boolean;

  constructor(id: string, forceDelete: boolean) {
    super('DeleteProcess');
    this.id = id;
    this.forceDelete = forceDelete;
  }
}

export class SearchHierarchies extends ApiAction {
  subProjectId: string;
  parentId: string;
  searchCriteria: string;

  constructor(subProjectId: string, parentId: string, searchCriteria: string) {
    super('SearchHierarchies');
    this.subProjectId = subProjectId;
    this.parentId = parentId;
    this.searchCriteria = searchCriteria;
  }
}

export class GetIssues extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetIssues');
    this.subProjectId = subProjectId;
  }
}

export class GetIssuesWithDetails extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetIssuesWithDetails');
    this.subProjectId = subProjectId;
  }
}

export class GetIssue extends ApiAction {
  id: string;

  constructor(issueId: string) {
    super('GetIssue');
    this.id = issueId;
  }
}

export class GetWorkflowIssues extends ApiAction {
  WorkflowId: string;

  constructor(WorkflowId: string) {
    super('GetWorkflowIssues');
    this.WorkflowId = WorkflowId;
  }
}

export class AddWorkflowIssue extends ApiAction {
  workflowId: string;
  issueId: string;

  constructor(workflowId: string, issueId: string) {
    super('AddWorkflowIssue');
    this.workflowId = workflowId;
    this.issueId = issueId;
  }
}

export class UnlinkWorkflowIssue extends ApiAction {
  workflowId: string;
  issueId: string;

  constructor(workflowId: string, issueId: string) {
    super('UnlinkWorkflowIssue');
    this.workflowId = workflowId;
    this.issueId = issueId;
  }
}

export class AddProcessWorkflow extends ApiAction {
  processId: string;
  workflowId: string;

  constructor(processId: string, workflowId: string) {
    super('AddProcessWorkflow');
    this.processId = processId;
    this.workflowId = workflowId;
  }
}

export class DeleteProcessWorkflow extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteProcessWorkflow');
    this.id = id;
  }
}

export class GetIssueWorkflows extends ApiAction {
  IssueId: string;

  constructor(issueId: string) {
    super('GetIssueWorkflows');
    this.IssueId = issueId;
  }
}

export class GetIssuesByIssueType extends ApiAction {
  subProjectId: string;
  issueTypeId: string;
  constructor(subProjectId: string, issueTypeId: string) {
    super('GetIssuesByIssueType');
    this.subProjectId = subProjectId;
    this.issueTypeId = issueTypeId;
  }
}

export class GetLinkedIssues extends ApiAction {
  issueId: string;
  constructor(issueId: string) {
    super('GetLinkedIssues');
    this.issueId = issueId;
  }
}

export class LinkIssue extends ApiAction {
  srcIssueId: string;
  linkType: number;
  dstIssueId: string;

  constructor(srcIssueId: string, linkType: number, dstIssueId: string) {
    super('LinkIssue');
    this.srcIssueId = srcIssueId;
    this.linkType = linkType;
    this.dstIssueId = dstIssueId;
  }
}

export class UnlinkIssue extends ApiAction {
  srcIssueId: string;
  dstIssueId: string;

  constructor(srcIssueId: string, dstIssueId: string) {
    super('UnlinkIssue');
    this.srcIssueId = srcIssueId;
    this.dstIssueId = dstIssueId;
  }
}

export class GetWorkflows extends ApiAction {
  folderId: string;

  constructor(folderId: string) {
    super('GetWorkflows');
    this.folderId = folderId;
  }
}

export class GetFolders extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetFolders');
    this.subProjectId = subProjectId;
  }
}

export class GetProcessWorkflows extends ApiAction {
  processId: string;

  constructor(processId: string) {
    super('GetProcessWorkflows');
    this.processId = processId;
  }
}

export class GetWorkflow extends ApiAction {
  WorkflowId: string;

  constructor(WorkflowId: string) {
    super('GetWorkflow');
    this.WorkflowId = WorkflowId;
  }
}

export class GetSprintsBySubProject extends ApiAction {
  SubProjectId: string;

  constructor(SubProjectId: string) {
    super('GetSprintsBySubProject');
    this.SubProjectId = SubProjectId;
  }
}

export class GetOpenSprintBySubProject extends ApiAction {
  SubProjectId: string;

  constructor(SubProjectId: string) {
    super('GetOpenSprintBySubProject');
    this.SubProjectId = SubProjectId;
  }
}

export class GetProcessesBySubProject extends ApiAction {
  SubProjectId: string;

  constructor(SubProjectId: string) {
    super('GetProcessesBySubProject');
    this.SubProjectId = SubProjectId;
  }
}

export class GetAllProcessWorkflowsBySubProject extends ApiAction {
  SubProjectId: string;

  constructor(SubProjectId: string) {
    super('GetAllProcessWorkflowsBySubProject');
    this.SubProjectId = SubProjectId;
  }
}

export class GetAllProcessTestCasesBySubProject extends ApiAction {
  SubProjectId: string;

  constructor(SubProjectId: string) {
    super('GetAllProcessTestCasesBySubProject');
    this.SubProjectId = SubProjectId;
  }
}

export class GetAllProcessOpenIssuesBySubProject extends ApiAction {
  SubProjectId: string;

  constructor(SubProjectId: string) {
    super('GetAllProcessOpenIssuesBySubProject');
    this.SubProjectId = SubProjectId;
  }
}

export class GetHierarchiesStatistics extends ApiAction {
  SubProjectId: string;
  ParentId: string;

  constructor(SubProjectId: string, ParentId: string) {
    super('GetHierarchiesStatistics');
    this.SubProjectId = SubProjectId;
    this.ParentId = ParentId;
  }
}

export class GetSprintIssues extends ApiAction {
  SprintId: string;

  constructor(SprintId: string) {
    super('GetSprintIssues');
    this.SprintId = SprintId;
  }
}

export class GetSprintIssuesWorkflow extends ApiAction {
  SprintId: string;

  constructor(SprintId: string) {
    super('GetSprintIssuesWorkflow');
    this.SprintId = SprintId;
  }
}

export class GetSprintIssuesTestCase extends ApiAction {
  SprintId: string;

  constructor(SprintId: string) {
    super('GetSprintIssuesTestCase');
    this.SprintId = SprintId;
  }
}

export class GetSprint extends ApiAction {
  Id: string;

  constructor(Id: string) {
    super('GetSprint');
    this.Id = Id;
  }
}

export class GetPeopleByUserFromAdmin extends ApiAction {
  userIdFromAdminPanel: number;
  subProjectId: string;

  constructor(userIdFromAdminPanel: number, subProjectId: string) {
    super('GetPeopleByUserFromAdmin');
    this.userIdFromAdminPanel = userIdFromAdminPanel;
    this.subProjectId = subProjectId;
  }
}

export class PasteProcess extends ApiAction {
  processId: string;
  hierarchyId: string;
  type: number;

  constructor(processId: string, hierarchyId: string, type: number) {
    super('PasteProcess');
    this.processId = processId;
    this.hierarchyId = hierarchyId;
    this.type = type;
  }
}

export class PasteHierarchy extends ApiAction {
  hierarchyId: string;
  parentId: string;
  pasteType: number;

  constructor(hierarchyId: string, parentId: string, pasteType: number) {
    super('PasteHierarchy');
    this.hierarchyId = hierarchyId;
    this.parentId = parentId;
    this.pasteType = pasteType;
  }
}

export class GetProcess extends ApiAction {
  id: string;

  constructor(id: string) {
    super('GetProcess');
    this.id = id;
  }
}

export class GetGenericWorkflowVersions extends ApiAction {
  genericWorkflowId: string;

  constructor(genericWorkflowId: string) {
    super('GetGenericWorkflowVersions');
    this.genericWorkflowId = genericWorkflowId;
  }
}

export class GetArisWorkflowVersions extends ApiAction {
  arisWorkflowId: string;

  constructor(arisWorkflowId: string) {
    super('GetArisWorkflowVersions');
    this.arisWorkflowId = arisWorkflowId;
  }
}

export class GetRoles extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetRoles');
    this.subProjectId = subProjectId;
  }
}

export class AddRole extends ApiAction {
  title: string;
  description: string;
  subProjectId: string

  constructor(title: string, description: string, subProjectId: string) {
    super('AddRole');
    this.title = title;
    this.description = description;
    this.subProjectId = subProjectId;
  }
}

export class UpdateRole extends ApiAction {
  id: string;
  title: string;
  description: string;
  subProjectId: string

  constructor(id: string, title: string, description: string, subProjectId: string) {
    super('UpdateRole');
    this.id = id;
    this.title = title;
    this.description = description;
    this.subProjectId = subProjectId;
  }
}

export class DeleteRole extends ApiAction {
  Id: string;

  constructor(id: string) {
    super('DeleteRole');
    this.Id = id;
  }
}

export class AddPeople extends ApiAction {
  roleId: string;
  name: string;
  surname: string;
  email: string;
  address: string;
  userIdFromAdminPanel: string;
  subProjectId: string;
  color: string;

  constructor(
    roleId: string,
    name: string,
    surname: string,
    email: string,
    address: string,
    userIdFromAdminPanel: string,
    subProjectId: string,
    color: string
  ) {
    super('AddPeople');
    this.roleId = roleId;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.address = address;
    this.userIdFromAdminPanel = userIdFromAdminPanel;
    this.subProjectId = subProjectId;
    this.color = color;
  }
}

export class UpdatePeople extends ApiAction {
  id: string;
  roleId: string;
  name: string;
  surname: string;
  email: string;
  address: string;
  userIdFromAdminPanel: string;
  subProjectId: string;
  color: string;

  constructor(
    id: string,
    roleId: string,
    name: string,
    surname: string,
    email: string,
    address: string,
    userIdFromAdminPanel: string,
    subProjectId: string,
    color: string
  ) {
    super('UpdatePeople');
    this.id = id;
    this.roleId = roleId;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.address = address;
    this.userIdFromAdminPanel = userIdFromAdminPanel;
    this.subProjectId = subProjectId;
    this.color = color;
  }
}

export class DeletePeople extends ApiAction {
  Id: string;

  constructor(id: string) {
    super('DeletePeople');
    this.Id = id;
  }
}

export class AddSprint extends ApiAction {
  title: string;
  description: string;
  start: string;
  end: string;
  subProjectId: string;
  duration: number;
  durationUnit: number;

  constructor(
    title: string,
    description: string,
    start: string,
    end: string,
    subProjectId: string,
    duration: number,
    durationUnit: number
  ) {
    super('AddSprint');
    this.title = title;
    this.description = description;
    this.start = start;
    this.end = end;
    this.subProjectId = subProjectId;
    this.duration = duration;
    this.durationUnit = durationUnit;
  }
}

export class UpdateSprint extends ApiAction {
  id: string;
  title: string;
  description: string;
  start: string;
  end: string;
  subProjectId: string;
  duration: number;
  durationUnit: number;

  constructor(
    id: string,
    title: string,
    description: string,
    start: string,
    end: string,
    subProjectId: string,
    duration: number,
    durationUnit: number
  ) {
    super('UpdateSprint');
    this.id = id;
    this.title = title;
    this.description = description;
    this.start = start;
    this.end = end;
    this.subProjectId = subProjectId;
    this.duration = duration;
    this.durationUnit = durationUnit;
  }
}

export class DeleteSprint extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteSprint');
    this.id = id;
  }
}

export class UpdateSprintIssue extends ApiAction {
  id: string;
  sprintId: string;
  issueId: string;
  status: number;

  constructor(id: string, sprintId: string, issueId: string, status: number) {
    super('UpdateSprintIssue');
    this.id = id;
    this.sprintId = sprintId;
    this.issueId = issueId;
    this.status = status;
  }
}

export class GetIssuesForSprintPlannerWithFilters extends ApiAction {
  subProjectId: string;
  processId: string;
  searchText: string;
  assigneeIds: Array<string>;

  constructor(subProjectId: string, processId: string, searchText: string, assigneeIds: Array<string>) {
    super('GetIssuesForSprintPlannerWithFilters');
    this.subProjectId = subProjectId;
    this.processId = processId;
    this.searchText = searchText;
    this.assigneeIds = assigneeIds;
  }
}

export class AddSprintIssue extends ApiAction {
  sprintId: string;
  issueId: string;
  status: number;

  constructor(sprintId: string, issueId: string, status: number) {
    super('AddSprintIssue');
    this.sprintId = sprintId;
    this.issueId = issueId;
    this.status = status;
  }
}

export class MoveHierarchyType extends ApiAction {
  id: string;

  constructor(id: string) {
    super('MoveHierarchyType');
    this.id = id;
  }
}

export class MoveHierarchy extends ApiAction {
  fromHierarchyId: string;
  toHierarchyId: string;

  constructor(fromHierarchyId: string, toHierarchyId: string) {
    super('MoveHierarchy');
    this.fromHierarchyId = fromHierarchyId;
    this.toHierarchyId = toHierarchyId;
  }
}

export class GetTraceabilityReportLine extends ApiAction {
  subProjectId: string;
  Level?: number;
  Id?: string;
  issueTypes?: any[];

  constructor(subProjectId: string, Level?: number, Id?: string, issueTypes?: any[]) {
    super('GetTraceabilityReportLine');
    this.subProjectId = subProjectId;
    if (Level) {
      this.Level = Level;
    }
    if (Id) {
      this.Id = Id;
    }
    if (issueTypes.length > 0) {
      this.issueTypes = issueTypes;
    }
  }
}

export class GetTraceabilityReportDetail extends ApiAction {
  subProjectId: string;
  Id: string;
  Level?: number;
  issueTypes?: any[];

  constructor(subProjectId: string, Id: string, Level?: number, issueTypes?: any[]) {
    super('GetTraceabilityReportDetail');
    this.subProjectId = subProjectId;
    this.Id = Id;
    if (Level) {
      this.Level = Level;
    }
    if (issueTypes.length > 0) {
      this.issueTypes = issueTypes;
    }
  }
}

export class GetRequirementType extends ApiAction {
  Id: string;

  constructor(Id: string, Level?: number) {
    super('GetRequirementType');
    this.Id = Id;
  }
}

export class GetRequirementPriority extends ApiAction {
  Id: string;

  constructor(Id: string, Level?: number) {
    super('GetRequirementPriority');
    this.Id = Id;
  }
}

export class UnlinkIssueFromProcess extends ApiAction {
  issueId: string;

  constructor(issueId: string) {
    super('UnlinkIssueFromProcess');
    this.issueId = issueId;
  }
}

export class LinkIssueToProcess extends ApiAction {
  issueId: string;
  processId: string;

  constructor(issueId: string, processId: string) {
    super('LinkIssueToProcess');
    this.issueId = issueId;
    this.processId = processId;
  }
}
export class GetArisWorkflowVersionsByDate extends ApiAction {
  arisWorkflowId: string;
  dateFrom: string;
  dateTo: string;

  constructor(arisWorkflowId: string, dateFrom: string, dateTo: string) {
    super('GetArisWorkflowVersionsByDate');
    this.arisWorkflowId = arisWorkflowId;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
  }
}

export class GetGenericWorkflowVersionsByDate extends ApiAction {
  genericWorkflowId: string;
  dateFrom: string;
  dateTo: string;

  constructor(genericWorkflowId: string, dateFrom: string, dateTo: string) {
    super('GetGenericWorkflowVersionsByDate');
    this.genericWorkflowId = genericWorkflowId;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
  }
}

export class GetIssueTypeFieldsNames extends ApiAction {
  constructor() {
    super('GetIssueTypeFieldsNames');
  }
}

export class GetIssueTypeFieldsAndAttributes extends ApiAction {
  issueTypeId: string;
  subprojectId: string;

  constructor(issueTypeId: string, subproectId: string) {
    super('GetIssueTypeFieldsAndAttributes');
    this.issueTypeId = issueTypeId;
    this.subprojectId = subproectId;
  }
}
export class GetIssueTypeFields extends ApiAction {
  issueTypeId: string;

  constructor(issueTypeId: string) {
    super('GetIssueTypeFields');
    this.issueTypeId = issueTypeId;
  }
}

///////////////////////////////////////////////////////////
// START: JIRA INTEGRATION
export class HasValidJiraCredentials extends ApiAction {
  SubprojectId: string;
  SkipIsEnabled: string;
  constructor(subprojectId, skipIsEnabled) {
    super('HasValidJiraCredentials');
    this.SubprojectId = subprojectId;
    this.SkipIsEnabled = skipIsEnabled;
  }
}

export class GetJiraUserSubprojectConfigs extends ApiAction {
  SubprojectId: string;
  UserId: string;
  constructor(subprojectId: string, userId: string) {
    super('GetJiraUserSubprojectConfigs');
    this.SubprojectId = subprojectId;
    this.UserId = userId;
  }
}

export class SetJiraUserSubprojectConfigs extends ApiAction {
  userId: string;
  subprojectId: string;
  jiraUsername: string;
  jiraToken: string;
  jiraUrl: string;

  constructor(userId, subprojectId, jiraUsername, jiraToken, jiraUrl) {
    super('SetJiraUserSubprojectConfigs');
    this.userId = userId;
    this.subprojectId = subprojectId;
    this.jiraUsername = jiraUsername;
    this.jiraToken = jiraToken;
    this.jiraUrl = jiraUrl;
  }
}

export class UpdateJiraCredentials extends ApiAction {
  Id: string;
  JiraUsername: string;
  JiraToken: string;
  constructor(userId: string, jiraUsername: string, jiraToken: string) {
    super('UpdateJiraCredentials');
    this.Id = userId;
    this.JiraUsername = jiraUsername;
    this.JiraToken = jiraToken;
  }
}

export class AuthenticateToJira extends ApiAction {
  email: string;
  password: string;

  constructor(email: string, password: string) {
    super('AuthenticateToJira');
    this.email = email;
    this.password = password;
  }
}

export class GetJiraProjects extends ApiAction {
  SubprojectId: string;
  constructor(subprojectId: string) {
    super('GetJiraProjects');
    this.SubprojectId = subprojectId;
  }
}

export class AddJiraProjectMapping extends ApiAction {
  projectId: string;
  subprojectId: string;
  jiraProjectKey: string;
  jiraProjectName: string;
  mapHierarchy: boolean;
  hierarchyParentId: string;
  hierarchyTypeId: string;

  constructor(projectId, subprojectId, jiraProjectKey, jiraProjectName, mapHierarchy, hierarchyParentId, hierarchyTypeId) {
    super('AddJiraProjectMapping');
    this.projectId = projectId;
    this.subprojectId = subprojectId;
    this.jiraProjectKey = jiraProjectKey;
    this.jiraProjectName = jiraProjectName;
    this.mapHierarchy = mapHierarchy;
    this.hierarchyParentId = hierarchyParentId;
    this.hierarchyTypeId = hierarchyTypeId;
  }
}

export class GetJiraProjectMapping extends ApiAction {
  subprojectId: string;

  constructor(subprojectId: string) {
    super('GetJiraProjectMapping');
    this.subprojectId = subprojectId;
  }
}

export class UpdateJiraProjectMapping extends ApiAction {
  id: string;
  projectId: string;
  subprojectId: string;
  jiraProjectKey: string;
  jiraProjectName: string;
  mapHierarchy: boolean;
  hierarchyParentId: string;
  hierarchyTypeId: string;

  constructor(id, projectId, subprojectId, jiraProjectKey, jiraProjectName, mapHierarchy, hierarchyParentId, hierarchyTypeId) {
    super('UpdateJiraProjectMapping');
    this.id = id;
    this.projectId = projectId;
    this.subprojectId = subprojectId;
    this.jiraProjectKey = jiraProjectKey;
    this.jiraProjectName = jiraProjectName;
    this.mapHierarchy = mapHierarchy;
    this.hierarchyParentId = hierarchyParentId;
    this.hierarchyTypeId = hierarchyTypeId;
  }
}

export class GetSenseaiIssueTypes extends ApiAction {
  SubProjectId: string;
  constructor(subprojectId: string) {
    super('GetSenseaiIssueTypes');
    this.SubProjectId = subprojectId;
  }
}

export class GetIssueTypeRequirementFields extends ApiAction {
  subprojectId: string;
  constructor(subprojectId: string) {
    super('GetIssueTypeRequirementFields');
    this.subprojectId = subprojectId;
  }
}

export class GetJiraIssueTypes extends ApiAction {
  projectKey: string;
  subprojectId: string;
  constructor(projectKey: string, subprojectId: string) {
    super('GetJiraIssueTypes');
    this.projectKey = projectKey;
    this.subprojectId = subprojectId;
  }
}

export class GetJiraIssueTypeFieldsAndAttributes extends ApiAction {
  IssueTypeId: string;
  JiraProjectKey: string;
  SubprojectId: string;
  constructor(issueTypeId: string, jiraProjectKey: string, subprojectId: string) {
    super('GetJiraIssueTypeFieldsAndAttributes');
    this.IssueTypeId = issueTypeId;
    this.JiraProjectKey = jiraProjectKey;
    this.SubprojectId = subprojectId;
  }
}

export class GetJiraIssueTypeMapping extends ApiAction {
  id: string;
  constructor(projectMapId: string) {
    super('GetJiraIssueTypeMapping');
    this.id = projectMapId;
  }
}

export class AddJiraIssueTypeMapping extends ApiAction {
  Id: string;
  jiraProjectMappingId: string;
  senseaiIssueTypeField: string;
  jiraIssueTypeFieldId: string;
  jiraIssueTypeFieldName: string;
  attributeMapping: any;
  allowAttachments: boolean;
  isUpdate: boolean;

  // tslint:disable-next-line:max-line-length
  constructor(id, jiraProjectMappingId, senseaiIssueTypeField, jiraIssueTypeFieldId, jiraIssueTypeFieldName, attributeMapping, allowAttachments, isUpdate) {
    // console.log(isUpdate ? 'Updating' : 'Adding' , jiraProjectMappingId)
    super('AddJiraIssueTypeMapping');
    this.Id = id;
    this.jiraProjectMappingId = jiraProjectMappingId;
    this.senseaiIssueTypeField = senseaiIssueTypeField;
    this.jiraIssueTypeFieldId = jiraIssueTypeFieldId;
    this.jiraIssueTypeFieldName = jiraIssueTypeFieldName;
    this.attributeMapping = attributeMapping;
    this.allowAttachments = allowAttachments;
    this.isUpdate = isUpdate;
  }
}

export class GetJiraIssueTypeListByProjectMapping extends ApiAction {
  JiraProjectMappingId: string;
  constructor(jiraProjectMappingId: string) {
    super('GetJiraIssueTypeListByProjectMapping');
    this.JiraProjectMappingId = jiraProjectMappingId;
  }
}

export class DeleteJiraIssueTypeMapping extends ApiAction {
  Id: string;
  constructor(id: string) {
    super('DeleteJiraIssueTypeMapping');
    this.Id = id;
  }
}

export class GetSenseAiPeoples extends ApiAction {
  SubProjectId: string;
  constructor(subprojectId: string) {
    super('GetPeoples');
    this.SubProjectId = subprojectId;
  }
}

export class GetJiraUsers extends ApiAction {
  SubprojectId: string;
  constructor(subprojectId: string) {
    super('GetJiraUsers');
    this.SubprojectId = subprojectId;
  }
}

export class GetJiraUserMapping extends ApiAction {
  subprojectId: string;
  constructor(subprojectId: string) {
    super('GetJiraUserMapping');
    this.subprojectId = subprojectId;
  }
}

export class AddJiraUserMapping extends ApiAction {
  subprojectId: string;
  jiraUserId: string;
  senseaiUserId: string;
  constructor(subprojectId: string, jiraUserId: string, senseaiUserId: string) {
    super('AddJiraUserMapping');
    this.subprojectId = subprojectId;
    this.jiraUserId = jiraUserId;
    this.senseaiUserId = senseaiUserId;
  }
}

export class DeleteJiraUserMapping extends ApiAction {
  Id: string;
  constructor(id: string) {
    super('DeleteJiraUserMapping');
    this.Id = id;
  }
}

export class AddJiraWebhooks extends ApiAction {
  subProjectId: string;
  name: string;
  events: string[];
  filters: any;
  url: string;
  constructor(subProjectId, name, events, filters, url) {
    super('AddJiraWebhooks');
    this.subProjectId = subProjectId;
    this.name = name;
    this.events = events;
    this.filters = filters;
    this.url = url;
  }
}

export class UpdateJiraWebhooks extends ApiAction {
  subProjectId: string;
  name: string;
  events: string[];
  filters: any;
  url: string;
  self: string;
  enabled: boolean;
  constructor(subProjectId, name, events, filters, url, self, enabled) {
    super('UpdateJiraWebhooks');
    this.subProjectId = subProjectId;
    this.name = name;
    this.events = events;
    this.filters = filters;
    this.url = url;
    this.self = self;
    this.enabled = enabled;
  }
}

export class GetJiraWebhooks extends ApiAction {
  subProjectId: string;
  constructor(subProjectId: string) {
    super('GetJiraWebhooks');
    this.subProjectId = subProjectId;
  }
}

export class GetJiraIntegrationSummary extends ApiAction {
  ProjectMappingId: string;
  constructor(id: string) {
    super('GetJiraIntegrationSummary');
    this.ProjectMappingId = id;
  }
}

export class UpdateSubProjectJiraCredentials extends ApiAction {
  id: string;
  isJiraIntegrationEnabled: boolean;
  constructor(id, isJiraIntegrationEnabled) {
    super('UpdateSubProjectJiraCredentials');
    this.id = id;
    this.isJiraIntegrationEnabled = isJiraIntegrationEnabled;
  }
}

export class FirstTimeSync extends ApiAction {
  SubprojectId: string;
  SenseAIUser: string;
  Sync: boolean;
  constructor(subprojectId, senseAiUsername, sync) {
    super('FirstTimeSync');
    this.SubprojectId = subprojectId;
    this.SenseAIUser = senseAiUsername;
    this.Sync = sync;
  }
}

// END: JIRA INTEGRATION
//////////////////////////////////////////////////////////
export class DeleteSprintIssue extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteSprintIssue');
    this.id = id;
  }
}

export class DeleteIssue extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteIssue');
    this.id = id;
  }
}

export class GetRequirementTypes extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetRequirementTypes');
    this.subProjectId = subProjectId;
  }
}

export class AddRequirementType extends ApiAction {
  subProjectId: string;
  title: string;
  code: string;

  constructor(subProjectId: string, title: string, code: string) {
    super('AddRequirementType');
    this.subProjectId = subProjectId;
    this.title = title;
    this.code = code;
  }
}

export class UpdateRequirementType extends ApiAction {
  id: string;
  subProjectId: string;
  title: string;
  code: string;

  constructor(id: string, subProjectId: string, title: string, code: string) {
    super('UpdateRequirementType');
    this.id = id;
    this.subProjectId = subProjectId;
    this.title = title;
    this.code = code;
  }
}

export class DeleteRequirementType extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteRequirementType');
    this.id = id;
  }
}

export class GetTypical extends ApiAction {
  id: string;
  constructor(id: string) {
    super('GetTypical');
    this.id = id;
  }
}
export class GetRequirementPriorities extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetRequirementPriorities');
    this.subProjectId = subProjectId;
  }
}

export class AddRequirementPriority extends ApiAction {
  subProjectId: string;
  title: string;
  code: string;

  constructor(subProjectId: string, title: string, code: string) {
    super('AddRequirementPriority');
    this.subProjectId = subProjectId;
    this.title = title;
    this.code = code;
  }
}

export class UpdateRequirementPriority extends ApiAction {
  id: string;
  subProjectId: string;
  title: string;
  code: string;

  constructor(id: string, subProjectId: string, title: string, code: string) {
    super('UpdateRequirementPriority');
    this.id = id;
    this.subProjectId = subProjectId;
    this.title = title;
    this.code = code;
  }
}

export class UpdateIndexesRequirementPriority extends ApiAction {
  requirementPriorities: Array<any>;

  constructor(data: Array<any>) {
    super('UpdateIndexesRequirementPriority');
    this.requirementPriorities = data;
  }
}

export class DeleteRequirementPriority extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteRequirementPriority');
    this.id = id;
  }
}
export class GetTypicalAttribute extends ApiAction {
  typicalId: string;
  name: string;
  constructor(typicalId: string, name: string) {
    super('GetTypicalAttribute');
    this.typicalId = typicalId;
    this.name = name;
  }
}

export class GetIssueResources extends ApiAction {
  issueId: string;
  constructor(issueId: string) {
    super('GetIssueResources');
    this.issueId = issueId;
  }
}

export class DeleteIssueResource extends ApiAction {
  id: string;
  constructor(id: string) {
    super('DeleteIssueResource');
    this.id = id;
  }
}
