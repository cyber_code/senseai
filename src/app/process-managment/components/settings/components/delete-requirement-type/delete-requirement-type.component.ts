import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DesignService } from '../../../../../design/services/design.service';
import { MessageService } from '../../../../../core/services/message.service';
import { MessageType } from '../../../../../core/models/message.model';
import { ProcessManagementService } from '../../../../services/process-management.service';

@Component({
  selector: 'app-delete-requirement-type',
  templateUrl: './delete-requirement-type.component.html',
  styleUrls: ['./delete-requirement-type.component.css']
})
export class DeleteRequirementTypeComponent implements OnInit {
  @Input() deleteTypeWindowOpen;
  @Input() selectedTypes;

  @Output() public closeDeleteTypesWindow = new EventEmitter<any>();
  @Output() public removeTypeFromList = new EventEmitter<boolean>();

  constructor(
    private processManagementService: ProcessManagementService,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  deleteType() {
    this.processManagementService
      .deleteRequirementType(this.selectedTypes.id)
      .subscribe(res => {
        this.handleResponse(res);
      });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: 'Requirement Type was deleted successfully!',
        type: MessageType.Success
      });
      this.removeTypeFromList.emit(this.selectedTypes);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeleteTypesWindow.emit();
  }
}
