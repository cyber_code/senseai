import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProcessManagementService} from '../../../../services/process-management.service';
import {MessageService} from '../../../../../core/services/message.service';
import {MessageType} from '../../../../../core/models/message.model';

@Component({
  selector: 'app-delete-priority',
  templateUrl: './delete-priority.component.html',
  styleUrls: ['./delete-priority.component.css']
})
export class DeletePriorityComponent implements OnInit {

  @Input() deletePriorityWindowOpen;
  @Input() selectedPriority;

  @Output() public closeDeletePriorityWindow = new EventEmitter<any>();
  @Output() public removePriorityFromList = new EventEmitter<boolean>();

  constructor(
    private processManagementService: ProcessManagementService,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  deletePriority() {
    this.processManagementService.deleteRequirementPriority(this.selectedPriority.id).subscribe(res => {
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({text: 'Requirement Priority was deleted successfully!', type: MessageType.Success});
      this.removePriorityFromList.emit(this.selectedPriority);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeletePriorityWindow.emit();
  }
}
