import { Component, OnInit } from '@angular/core';
import {Licenses} from 'src/app/core/models/licenses.model';
import { LicenseService } from 'src/app/core/services/licenses.service';
import { PermissionService } from 'src/app/core/services/permission.service';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-settings-app',
  templateUrl: './settings-app.component.html',
  styleUrls: ['./settings-app.component.css']
})
export class SettingsAppComponent implements OnInit {

  public license = Licenses;

  constructor(public licenseService: LicenseService, private sessionService: SessionService, private permissionService: PermissionService) {}

  ngOnInit() {}

  hasJiraPermissions() {
    return (this.licenseService.hasLicense(this.license.Jira) && this.permissionService.hasPermission('/api/Process/Jira/GetJiraIssueTypes'));
  }

}
