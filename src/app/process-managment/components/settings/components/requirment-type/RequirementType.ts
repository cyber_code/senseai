export class RequirementType {
  public subProjectId: string;
  public title: string;
  public code: string;
}
