import {Component, OnInit, Inject, Input, Output, EventEmitter} from '@angular/core';
import {Observable} from 'rxjs';
import {FormGroup, FormControl, Validators} from '@angular/forms';

import {GridDataResult} from '@progress/kendo-angular-grid';
import {State, process} from '@progress/kendo-data-query';

import {SessionService} from '../../../../../core/services/session.service';
import * as _ from 'lodash';
import {ProcessManagementService} from '../../../../services/process-management.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-requirment-type',
  templateUrl: './requirment-type.component.html',
  styleUrls: ['./requirment-type.component.css']
})

export class RequirmentTypeComponent implements OnInit {

  public types = [];
  public gridData: any = [];
  public state: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };

  @Output() typesDataStateChanged = new EventEmitter<any>();
  @Output() typesListChanged = new EventEmitter<any>();

  public selectedTypes = {
    title: '',
    code: ''
  };

  public typesWindowOpen = false;
  public deleteTypeWindowOpen = false;
  public isTypeEdited = false;

  constructor(
    private sessionService: SessionService, 
    private processManagementService: ProcessManagementService,
    public permissionService: PermissionService
  ) {}

  async ngOnInit() {
    const types = this.permissionService.hasPermission('/api/Process/Configure/GetRequirementTypes') ? await this.getRequirementTypes() : [];
    this.types = types;
    this.gridData = process(types, this.state);
  }

  getRequirementTypes() {
    const subProjectId = this.sessionService.getWorkContext().subProject.id;
    return this.processManagementService.getRequirementTypes(subProjectId).toPromise();
  }


  typesListChange(types) {
    this.types = types;
    this.gridData = process(types, this.state);
  }

  dataStateChange(state) {
    this.state = state;
    this.gridData = process(this.types, state);
  }

  openTypesWindow(catalog?) {
    this.typesWindowOpen = true;
    if (catalog) {
      this.isTypeEdited = true;
      this.selectedTypes = _.cloneDeep(catalog);
    } else {
      this.resetType();
    }
  }

  close() {
    this.typesWindowOpen = false;
    this.deleteTypeWindowOpen = false;
    this.resetType();
  }

  resetType() {
    this.isTypeEdited = false;
    this.selectedTypes = {
      title: '',
      code: ''
    };
  }

  updateTypeList(req) {
    let types = [];
    if (this.isTypeEdited) {
      types = this.types.map(sys => {
        if (sys.id === req.id) {
          return req;
        }
        return sys;
      });
    } else {
      types = this.types.concat(req);
    }
    this.types = types;
    this.gridData = process(this.types, this.state);
    this.close();
  }

  removeTypeFromList(type) {
    let typeF = this.types.filter(sys => sys.id !== type.id);
    this.types = typeF;
    this.gridData = process(typeF, this.state);
    this.close();
  }

  openDeleteTypeWindow(type) {
    this.deleteTypeWindowOpen = true;
    this.selectedTypes = type;
  }

  addRequirementTypePermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Configure/GetRequirementTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/AddRequirementType')
    );
  }

  editRequirementTypePermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Configure/GetRequirementTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/UpdateRequirementType')
    );
  }

  deleteRequirementTypePermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Configure/GetRequirementTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/DeleteRequirementType')
    );
  }
}
