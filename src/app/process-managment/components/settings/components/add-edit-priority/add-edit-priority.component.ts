import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProcessManagementService} from '../../../../services/process-management.service';
import {MessageService} from '../../../../../core/services/message.service';
import {SessionService} from '../../../../../core/services/session.service';
import {MessageType} from '../../../../../core/models/message.model';

@Component({
  selector: 'app-add-edit-priority',
  templateUrl: './add-edit-priority.component.html',
  styleUrls: ['./add-edit-priority.component.css']
})
export class AddEditPriorityComponent implements OnInit {


  @Input() isPriorityEdited;
  @Input() selectedPriority;
  @Input() priorityWindowOpen;

  @Output() public closePriorityWindow = new EventEmitter<any>();
  @Output() public priorityListUpdated = new EventEmitter<boolean>();


  constructor(
    private processManagementService: ProcessManagementService,
    private messageService: MessageService,
    private sessionService: SessionService) {
  }

  ngOnInit() {
  }

  close() {
    this.closePriorityWindow.emit();
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedPriority.title = title;
  }

  changeCode(event) {
    let code = event.target.value.trim('');
    this.selectedPriority.code = code;
  }


  get disableSubmitBtn() {
    return (
      this.selectedPriority.title === ''
    );
  }

  submitClicked() {
    if (this.isPriorityEdited) {
      this.editPriority();
    } else {
      this.addPriority();
    }
  }

  addPriority() {
    const subProjectId = this.sessionService.getWorkContext().subProject.id;
    this.processManagementService
      .addRequirementPriority(subProjectId, this.selectedPriority.title, this.selectedPriority.code)
      .subscribe(res => {
        this.handlePriorityResponse(res);
      });
  }

  editPriority() {
    const subProjectId = this.sessionService.getWorkContext().subProject.id;
    this.processManagementService
      .updateRequirementPriority(this.selectedPriority.id, subProjectId, this.selectedPriority.title, this.selectedPriority.code)
      .subscribe(res => {
        this.handlePriorityResponse(res);
      });
  }

  handlePriorityResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isPriorityEdited
          ? 'Requirement Priority was updated successfully!'
          : 'Requirement Priority was added successfully!',
        type: MessageType.Success
      });
      this.priorityListUpdated.emit(
        this.isPriorityEdited
          ? this.selectedPriority
          : {...this.selectedPriority, id: res.id}
      );
    } else {
      this.close();
    }
  }
}
