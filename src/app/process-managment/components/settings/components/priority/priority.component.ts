import {Component, EventEmitter, OnInit, Output, AfterViewChecked, OnDestroy, Renderer2, NgZone, ViewEncapsulation  } from '@angular/core';
import {process, State} from '@progress/kendo-data-query';
import {SessionService} from '../../../../../core/services/session.service';
import {ProcessManagementService} from '../../../../services/process-management.service';
import * as _ from 'lodash';
import { Subscription, fromEvent } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AutoUnsubscribe } from 'take-while-alive';
import { PermissionService } from 'src/app/core/services/permission.service';

const tableRow = node => node.tagName.toLowerCase() === 'tr';

const closest = (node, predicate) => {
    while (node && !predicate(node)) {
        node = node.parentNode;
    }

    return node;
};

@Component({
  selector: 'app-priority',
  templateUrl: './priority.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./priority.component.css']
})
@AutoUnsubscribe()
export class PriorityComponent implements OnInit, AfterViewChecked, OnDestroy  {

  public priority = [];
  public gridData: any = [];
  public state: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };
  draggedItemIndex = null;

  @Output() priorityDataStateChanged = new EventEmitter<any>();
  @Output() priorityListChanged = new EventEmitter<any>();
  

  public selectedPriority = {
    title: '',
    code: ''
  };

  public priorityWindowOpen = false;
  public deletePriorityWindowOpen = false;
  public isPriorityEdited = false;
  private currentSubscription: Subscription;

  constructor(
    private sessionService: SessionService, 
    private processManagementService: ProcessManagementService, 
    public permissionService: PermissionService,
    private renderer: Renderer2, 
    private zone: NgZone
    ) {
  }

  public ngAfterViewChecked(): void {
    this.currentSubscription = this.handleDragAndDrop(); 
}

  async ngOnInit() {
    const priority = this.permissionService.hasPermission('/api/Process/Configure/GetRequirementPriorities') ? 
      await this.getRequirementPriorities() : [];
    this.priority = priority;
    this.gridData = process(priority, this.state);
  }

  public ngOnDestroy(): void {
    this.currentSubscription.unsubscribe();
}

public rowCallback(context: any) {
  return {
      dragging: context.dataItem.dragging
  };
}

private handleDragAndDrop(): Subscription {

  if (this.currentSubscription !== undefined) {
    this.currentSubscription.unsubscribe();
  }
  const sub = new Subscription(() => {});
  const table = document.querySelector(".priorityTable");
  const tableRows = Array.from(table.querySelectorAll('.k-grid tr'));
  tableRows.forEach(item => {
      this.renderer.setAttribute(item, 'draggable', 'true');
      const dragStart = fromEvent<DragEvent>(item, 'dragstart');
      const dragOver = fromEvent(item, 'dragover');
      const dragEnd = fromEvent(item, 'dragend');
      sub.add(
        dragStart.pipe(
          tap(({ dataTransfer }) => {
              try {
                const dragImgEl = document.createElement('span');
                dragImgEl.setAttribute('style', 'position: absolute; display: block; top: 0; left: 0; width: 0; height: 0;');
                document.body.appendChild(dragImgEl);
                dataTransfer.setDragImage(dragImgEl, 0, 0);
              } catch (err) {
                // IE doesn't support setDragImage
              }
              try {
                  // Firefox won't drag without setting data
                  dataTransfer.setData('application/json', "");
              } catch (err) {
                  // IE doesn't support MIME types in setData
              }
          })
      ).subscribe(({ target }) => {
          const row: HTMLTableRowElement = <HTMLTableRowElement>target;
          this.draggedItemIndex = row.rowIndex;
          const dataItem = this.gridData.data[this.draggedItemIndex];
          dataItem.dragging = true;
      }));

      sub.add(dragOver.subscribe((e: any) => {
          e.preventDefault();
          const dropIndex = closest(e.target, tableRow).rowIndex;
          const dataItem = this.gridData.data.splice(this.draggedItemIndex, 1)[0];
          this.draggedItemIndex = dropIndex;
          this.zone.run(() =>
              this.gridData.data.splice(dropIndex, 0, dataItem)
          );
      }));

      sub.add(dragEnd.subscribe((e: any) => {
          e.preventDefault();
          const dataItem = this.gridData.data[this.draggedItemIndex];
          dataItem.dragging = false;
          this.updateSequenceRows();
      }));
  });

  return sub;
}

updateSequenceRows(){
  let newRows = this.priority.map(row =>{
    let indexGrid = this.gridData.data.findIndex(line => line.id === row.id);
    if (indexGrid !== -1)
      return {...row, index: indexGrid + this.state.skip + 1};
    return row;
  });
  newRows = _.sortBy( newRows, 'index' );
  this.processManagementService.UpdateIndexesRequirementPriority(newRows).subscribe((res)=>{
    this.priority = newRows;
  })

}


  getRequirementPriorities() {
    const subProjectId = this.sessionService.getWorkContext().subProject.id;
    return this.processManagementService.getRequirementPriorities(subProjectId).toPromise();
  }


  priorityListChange(priority) {
    this.priority = priority;
    this.gridData = process(priority, this.state);
  }

  dataStateChange(state) {
    this.state = state;
    this.gridData = process(this.priority, state);
  }

  openPriorityWindow(catalog?) {
    this.priorityWindowOpen = true;
    if (catalog) {
      this.isPriorityEdited = true;
      this.selectedPriority = _.cloneDeep(catalog);
    } else {
      this.resetPriority();
    }
  }

  close() {
    this.priorityWindowOpen = false;
    this.deletePriorityWindowOpen = false;
    this.resetPriority();
  }

  resetPriority() {
    this.isPriorityEdited = false;
    this.selectedPriority = {
      title: '',
      code: ''
    };
  }

  updatePriorityList(priority) {
    if (this.isPriorityEdited) {
      priority = this.priority.map(sys => {
        if (sys.id === priority.id) {
          return priority;
        }
        return sys;
      });
    } else {
      priority = {...priority, index: this.priority.length + 1}
      priority = this.priority.concat(priority);
    }
    this.priority = priority;
    this.gridData = process(priority, this.state);
    this.close();
  }

  removePriorityFromList(priority) {
    let priorityF = this.priority.filter(sys => sys.id !== priority.id);
    this.priority = priorityF;
    this.gridData = process(priorityF, this.state);
    this.close();
  }

  openDeletePriorityWindow(priority) {
    this.deletePriorityWindowOpen = true;
    this.selectedPriority = priority;
  }

  addPriorityPermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Configure/GetRequirementPriorities') &&
      this.permissionService.hasPermission('/api/Process/Configure/AddRequirementPriority')
    );
  }

  editPriorityPermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Configure/GetRequirementPriorities') &&
      this.permissionService.hasPermission('/api/Process/Configure/UpdateRequirementPriority')
    );
  }

  deletePriorityPermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Configure/GetRequirementPriorities') &&
      this.permissionService.hasPermission('/api/Process/Configure/DeleteRequirementPriority')
    );
  }

}
