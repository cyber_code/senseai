import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Adapter} from '../../../../../design/models/adapter.model';
import {DesignService} from '../../../../../design/services/design.service';
import {MessageService} from '../../../../../core/services/message.service';
import {MessageType} from '../../../../../core/models/message.model';
import {SessionService} from '../../../../../core/services/session.service';
import {ProcessManagementService} from '../../../../services/process-management.service';

@Component({
  selector: 'app-add-edit-requirement-types',
  templateUrl: './add-edit-requirement-types.component.html',
  styleUrls: ['./add-edit-requirement-types.component.css']
})
export class AddEditRequirementTypesComponent implements OnInit {

  @Input() isTypeEdited;
  @Input() selectedTypes;
  @Input() typesWindowOpen;

  @Output() public closeTypesWindow = new EventEmitter<any>();
  @Output() public typeListUpdated = new EventEmitter<boolean>();


  constructor(
    private processManagementService: ProcessManagementService,
    private messageService: MessageService,
    private sessionService: SessionService) {
  }

  ngOnInit() {
  }

  close() {
    this.closeTypesWindow.emit();
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedTypes.title = title;
  }

  changeCode(event) {
    let code = event.target.value.trim('');
    this.selectedTypes.code = code;
  }


  get disableSubmitBtn() {
    return (
      this.selectedTypes.title === ''
    );
  }

  submitClicked() {
    if (this.isTypeEdited) {
      this.editType();
    } else {
      this.addType();
    }
  }

  addType() {
    const subProjectId = this.sessionService.getWorkContext().subProject.id;
    this.processManagementService
      .addRequirementType(subProjectId, this.selectedTypes.title, this.selectedTypes.code)
      .subscribe(res => {
        this.handleTypeResponse(res);
      });
  }

  editType() {
    const subProjectId = this.sessionService.getWorkContext().subProject.id;
    this.processManagementService
      .updateRequirementType(this.selectedTypes.id, subProjectId, this.selectedTypes.title, this.selectedTypes.code)
      .subscribe(res => {
        this.handleTypeResponse(res);
      });
  }

  handleTypeResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isTypeEdited
          ? 'Requirement Type was updated successfully!'
          : 'Requirement Type was added successfully!',
        type: MessageType.Success
      });
      this.typeListUpdated.emit(
        this.isTypeEdited
          ? this.selectedTypes
          : {...this.selectedTypes, id: res.id}
      );
    } else {
      this.close();
    }
  }

}
