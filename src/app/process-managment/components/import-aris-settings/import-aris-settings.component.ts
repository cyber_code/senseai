import { Component, OnInit } from '@angular/core';
import { AppArisTreViewComponent } from 'src/app/core/components/aris-tree-view/aris-tree-view.component';
import * as _ from 'lodash';
import { ArisTreeViewItem } from 'src/app/shared/aris-tree-view-item';

@Component({
  selector: 'app-import-aris-settings',
  templateUrl: './import-aris-settings.component.html',
  styleUrls: ['./import-aris-settings.component.css']
})
export class ImportArisSettingsComponent implements OnInit {
  public selectedTreeViewItem: ArisTreeViewItem;
  private treeViewComponent: AppArisTreViewComponent;


  constructor() { }

  ngOnInit() {
  }

  treeViewItemCreated(treeViewItem: ArisTreeViewItem) {
    this.treeViewComponent.addTreeViewElement(treeViewItem);
  }

  treeViewItemDeleted(treeViewItem: ArisTreeViewItem) {
    this.treeViewComponent.deleteTreeViewElement(treeViewItem);
  }

  reloadFolder(treeViewItem: ArisTreeViewItem) {
    this.treeViewComponent.reloadFolderItems(treeViewItem);
  }

}
