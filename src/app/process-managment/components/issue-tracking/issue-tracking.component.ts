import { Component, OnInit } from '@angular/core';
import { ProcessManagementService } from '../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { SelectableSettings, PageChangeEvent } from '@progress/kendo-angular-grid';
import { State, process, CompositeFilterDescriptor} from '@progress/kendo-data-query';
import { Guid } from 'src/app/shared/guid';

import { Priorities } from '../../models/priorities' ;
import { StatusType } from '../../models/status-type';
import { ExcelExportData } from '@progress/kendo-angular-excel-export';
import { cloneDeep } from 'lodash';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
    selector: 'app-issue-tracking',
    templateUrl: './issue-tracking.component.html',
    styleUrls: ['./issue-tracking.component.css']
})

export class IssueTrackingComponent implements OnInit{

    
    gridData;
    state: State = {
      skip: 0,
      take: 10,
      filter: {
        logic: 'and',
        filters: [ ]
      }
    };
    issues;
    people = [];
    loading = false;
    openIssueDetailsPopUp=false;
    selectableSettings: SelectableSettings;
    selectedIssueId= [];
    selectedIssue;
    subProjectId : string;
    searchCriteria: string = '' ;

    assignees: {id: string , title: string}[];
    selectedAssignees: any;

    priorities = [
        {id: Priorities.High, title: "High"},
        {id: Priorities.Medium, title: "Medium"},
        {id: Priorities.Low, title: "Low"},
        {id: Priorities.Critical, title: "Critical"}
    ];
    selectedPriorities: any;

    issueTypes: any;
    selectedIssueTypes: any;

    statuses = [
        {id: StatusType.Done , title: "Done"},
        {id: StatusType.ToDo , title: "ToDo"},
        {id: StatusType.InProgress , title: "InProgress"}
    ];
    selectedStatuses:any;

    sprints: any;
    selectedSprints;

    hierarchyTypes: any;
    selectedHierarchyTypes: any;

    dropdownSettings: IDropdownSettings = {
        singleSelection: false,
        idField: 'id',
        textField: 'title',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: false
    };

    dropdownSettingsAssignees: IDropdownSettings = {
        singleSelection: false,
        idField: 'id',
        textField: 'name',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        allowSearchFilter: false
    };

    constructor(
        private sessionService: SessionService,
        private processManagementService: ProcessManagementService,
        private permissionService: PermissionService
    ){  
        this.setSelectableSettings();
        this.subProjectId = this.sessionService.getSubProject().id; 
        this.exportDataExcel=this.exportDataExcel.bind(this);
    }

    ngOnInit(){
        this.loading = true;
        if (this.permissionService.hasPermission("/api/Process/Configure/GetIssueTypes"))
            this.getIssueTypes().subscribe(types=>{this.issueTypes = types;})
        if (this.permissionService.hasPermission("/api/Process/Sprint/GetSprintsBySubProject"))
            this.getSprintsBySubproject().subscribe(sprints=>{this.sprints = sprints});
        if (this.permissionService.hasPermission("/api/People/GetPeople"))
            this.getPeople().subscribe(people=>{ this.assignees = people; }) ; 
        if (this.permissionService.hasPermission("/api/Process/Browse/GetHierarchies"))
            this.getHierarchies().subscribe(types=> {this.hierarchyTypes=types; });
        if (this.permissionService.hasPermission("/api/Process/Browse/GetIssuesWithDetails"))
            this.getAllIssues().subscribe(res=>{
                this.issues=res;
                this.gridData = process(this.issues, this.state);
                this.loading = false;
            });
    }

    refreshIssues(){
        this.loading=true;
        if (this.permissionService.hasPermission("/api/Process/Browse/GetIssuesWithDetails"))
            this.getAllIssues().subscribe(res=>{
                this.issues=res;
                this.gridData = process(this.issues, this.state);
                this.loading = false;
            });
    }

    dataStateChange(state) {
        this.state = state;
        this.gridData = process(this.gridData, state);
    }

    getPeopleById(id) {
        return this.processManagementService.getPeople(id);
    }

    getPeople(){
        return this.processManagementService.getPeoples(this.subProjectId);
    }

    getAllIssues() {
        let workContext = this.sessionService.getWorkContext();
        return this.processManagementService.getIssuesWithDetails(workContext.subProject.id);
    }

    getIssueTypes() {
        let workContext = this.sessionService.getWorkContext();
        return this.processManagementService.getIssueTypes(workContext.subProject.id);
    }

    getSprintsBySubproject(){
        let workContext = this.sessionService.getWorkContext();
        return this.processManagementService.getSprintsBySubproject(workContext.subProject.id);
    }

    getHierarchies(){
        let workContext = this.sessionService.getWorkContext();
        return this.processManagementService.getHierarchiesByParent(workContext.subProject.id, Guid.empty, Guid.empty);
    }

    setSelectableSettings(){
        this.selectableSettings = {
            checkboxOnly: false,
            mode: 'single'
        };
    }

    closeIssueDetailsPopup(){
        this.openIssueDetailsPopUp=false;
        this.selectedIssueId=[];
    }

    openIssueDetails(){
        this.openIssueDetailsPopUp=true;
        this.selectedIssue=this.issues.find((issue)=>{ 
            return issue.id === "" + this.selectedIssueId[0]
        });
        this.selectedIssue.assignedId=this.selectedIssue.assigneeId;
    }

    pageChange({ skip, take }: PageChangeEvent): void{
        this.state.skip=skip;
        this.state.take=take;
        this.gridData=process(this.issues, this.state);

    }
    
    onSearch(ev){
        this.searchCriteria = ev.target.value.trim('');
        if(ev.target.value!==""){
        let searchFilters: CompositeFilterDescriptor = {logic: "or", filters: []};
        searchFilters.filters.push({field: "title", operator: 'contains', value: this.searchCriteria});
        searchFilters.filters.push({field: "id", operator: 'contains', value: this.searchCriteria});
        this.state.filter.filters.push(searchFilters);
        this.gridData=process(this.issues, this.state); 
        }
        else {
            this.clearSearch();
        }      
    }

    clearSearch(){
        this.searchCriteria="";
        this.state.filter.filters=this.state.filter.filters.filter(filterObject=>{
            if( filterObject["logic"] && filterObject["filters"][0] && 
            (filterObject["filters"][0].field==="title" || filterObject["filters"][0].field==="id" )) return false;
            return true;
        });
        this.gridData=process(this.issues, this.state);
    }
      
    filterByPriorities(){
    this.state.filter.filters=this.state.filter.filters.filter(filterObject=>{
        if(filterObject["logic"] && filterObject["filters"][0] && filterObject["filters"][0].field==="priorityTitle") return false;
        return true;
    });
    if(this.selectedPriorities.length!==0){
        let priorityFilters= [];
        this.selectedPriorities.map(priority=>{
        priorityFilters.push({field: 'priorityTitle', operator: 'eq', value: priority["title"]});
        })
        this.state.filter.filters.push({
        logic: 'or',
        filters: priorityFilters
        });
    }
    this.gridData = process(this.issues, this.state);
    }

    filterByIssueTypes(){
    this.state.filter.filters=this.state.filter.filters.filter(filterObject=>{
        if(filterObject["logic"] && filterObject["filters"][0] && filterObject["filters"][0].field==="issueTypeTitle") return false;
        return true;
    });
    if(this.selectedIssueTypes.length!==0){
        let issueTypesFilters= [];
        this.selectedIssueTypes.map(issueType=>{
            issueTypesFilters.push({field: 'issueTypeTitle', operator: 'eq', value: issueType["title"]});
        })
        this.state.filter.filters.push({
        logic: 'or',
        filters: issueTypesFilters
        });
    }
    this.gridData = process(this.issues, this.state);
    }

    filterByStatuses(){
    this.state.filter.filters=this.state.filter.filters.filter(filterObject=>{
        if(filterObject["logic"] && filterObject["filters"][0] && filterObject["filters"][0].field==="statusTitle") return false;
        return true;
    });
    if(this.selectedStatuses.length!==0){
        let statusFilters= [];
        this.selectedStatuses.map(status=>{
            statusFilters.push({field: 'statusTitle', operator: 'eq', value: status["title"]});
        })
        this.state.filter.filters.push({
        logic: 'or',
        filters: statusFilters
        });
    }
    this.gridData = process(this.issues, this.state);
    }

    filterByAssignees(){
        this.state.filter.filters=this.state.filter.filters.filter(filterObject=>{
            if(filterObject["logic"] && filterObject["filters"][0] && filterObject["filters"][0].field==="assigneeName") return false;
            return true;
        });
        if(this.selectedAssignees.length!==0){
            let assigneesFilters= [];
            this.selectedAssignees.map(assignee=>{
                assigneesFilters.push({field: 'assigneeName', operator: 'eq', value: assignee["name"]});
            })
            this.state.filter.filters.push({
            logic: 'or',
            filters: assigneesFilters
            });
        }
        this.gridData = process(this.issues, this.state);
    }

    filterByHierarchies(){
    this.state.filter.filters=this.state.filter.filters.filter(filterObject=>{
        if(filterObject["logic"] && filterObject["filters"][0] && filterObject["filters"][0].field==="componentId") return false;
        return true;
    });
    if(this.selectedHierarchyTypes.length!==0){
        let hierarchyTypesFilters= [];
        this.selectedHierarchyTypes.map(hierarchy=>{
            hierarchyTypesFilters.push({field: 'componentId', operator: 'eq', value: hierarchy["id"]});
        })
        this.state.filter.filters.push({
        logic: 'or',
        filters: hierarchyTypesFilters
        });
    }
    this.gridData = process(this.issues, this.state);   
    }

    filterBySprints(){
    this.state.filter.filters=this.state.filter.filters.filter(filterObject=>{
        if(filterObject["logic"] && filterObject["filters"][0] && filterObject["filters"][0].field==="sprintId") return false;
        return true;
    });
    if(this.selectedSprints.length!==0){
        let sprintFilters= [];
        this.selectedSprints.map(sprints=>{
            sprintFilters.push({field: 'sprintId', operator: 'eq', value: sprints["id"]});
        })
        this.state.filter.filters.push({
        logic: 'or',
        filters: sprintFilters
        });
    }
    this.gridData = process(this.issues, this.state);
    }

    onSelectDeselectAll(value){
        this.state.filter.filters=this.state.filter.filters.filter(filterObject=>{
            if(filterObject["logic"] && filterObject["filters"][0] && filterObject["filters"][0].field===value) return false;
            return true;
        });
        this.gridData = process(this.issues, this.state);
    }

    exportDataExcel(): ExcelExportData {
        let exportState=cloneDeep(this.state);
        exportState.take=100;
        const result: ExcelExportData =  {
            data: process(this.issues, exportState).data
        };
        return result;
    }
} 