import { Component, Input, OnInit } from '@angular/core';
import { of, forkJoin } from 'rxjs';
import { ProcessManagementService } from '../../services/process-management.service';
import { WindowService } from '@progress/kendo-angular-dialog';
import { SessionService } from '../../../core/services/session.service';
import { WorkflowIssuesDataService } from '../../services/workflow-issues-data.service';
import { isNullOrUndefined } from 'util';
import { takeWhileAlive, AutoUnsubscribe } from 'take-while-alive';
import * as _ from 'lodash';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-workflow-issues',
  templateUrl: './workflow-issues.component.html',
  styleUrls: ['./workflow-issues.component.css']
})
@AutoUnsubscribe()
export class WorkflowIssuesComponent implements OnInit {
  public expandedKeys: any[] = ['0'];
  public workflowId: any;
  public folderId: any;
  public processId: any;
  subProjectId: any;
  public openedPopup = false;
  public workflowIssues: any[] = [];
  public issueWorkflows: any[] = [];
  public linkedWorkflows: any = [];
  public issueOpen = null;
  public issueTypes = [];
  public openIssueDetail : boolean = false;
  public people: any[] = [];
  public sprints: any[] = [];
  public sprintIssues = [];
  public allIssue : any[] = [];


  @Input() 
  set allIssues(allIssues) {
    if (allIssues){
      this.allIssue = allIssues;
    }
  }

  @Input() 
  set process(process) {
    if (process && process.id) {
      this.loadWorkflowAndIssues(process.id);
    } else {
      this.loadWorkflowAndIssues(null);
    }
  }

  constructor(
    private processManagementService: ProcessManagementService,
    private windowService: WindowService,
    private sessionService: SessionService,
    private workflowIssuesDataService: WorkflowIssuesDataService,
    public permissionService: PermissionService
  ) {
  }

  ngOnInit() {
    this.folderId = '4f34555b-38d7-4d81-86b9-1612f5158b62';
    let subProjectId = this.sessionService.getSubProject().id;
    if (this.permissionService.hasPermission('/api/People/GetPeoples')) {
      this.processManagementService.getPeoples(subProjectId).subscribe(people => {
        this.people = people;
      });
    }
    
    if (this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypes')) {
      this.processManagementService.getIssueTypes(subProjectId).subscribe(issueTypes => {
        if (issueTypes) {
          this.issueTypes = issueTypes;
        }
      });
    }
    
    if (this.permissionService.hasPermission('/api/Process/Sprint/GetOpenSprintBySubProject') && 
      this.permissionService.hasPermission('/api/Process/Sprint/GetSprintIssues')) {
        this.processManagementService.getOpenSprintBySubProject(subProjectId).subscribe(sprints =>{
          this.sprints = sprints.map(sprint =>{
            return {...sprint, elType: 'Sprint'};
          });
          let sprintIssues = this.sprints.map(sprint =>{
            return this.processManagementService.getSprintIssues(sprint.id);
          });
          forkJoin(sprintIssues).subscribe(res =>{
            this.sprintIssues = res;
          })
        });
    }
  }

  openIssueDetails(id){
    let issue = this.allIssue.find(issue => issue.id === id);
    this.sprintIssues.map(sprint =>{
      sprint.map(issueSprint =>{
        if (issueSprint.issueId === issue.id)
          issue = {...issue, sprintId: issueSprint.sprintId, issueSprint: issueSprint};
      })
    })
    this.issueOpen = issue;
    this.openIssueDetail = !this.openIssueDetail;
  }


  updateIssue(issue) {
    this.allIssue = this.allIssue.map(issueRow =>{
      if (issueRow.id === issue.id)
        return issue;
      return issueRow
    });
    return _.cloneDeep(issue);
  
  }

  loadWorkflowAndIssues(processId) {
    this.subProjectId = this.sessionService.getSubProject().id;
    if (isNullOrUndefined(processId)) {
      this.workflowIssues = [];
      this.issueWorkflows = [];
      return;
    }
    if (this.permissionService.hasPermission('/api/Process/Browse/GetProcessWorkflows')) {
      this.processManagementService.getProcessWorkflows(processId).subscribe(workflows => {
        this.workflowIssues = [];
         workflows.forEach(wi => {
           this.workflowIssues.push({
             id: wi.workflowId,
             text: wi.title,
             type: 1,
             items: this.getWorkflowIssues(wi.workflowId)
           });
         });
       });
    }
    
    let issues = this.allIssue.filter(issue => issue.processId === processId);
    this.issueWorkflows = [];
    issues.forEach(iw => {
      this.issueWorkflows.push({
        id: iw.id,
        text: iw.title,
        type: 2,
        items: this.getIssueWorkflows(iw.id)
      });
    });
  }

  getWorkflowIssues(workflowId) {
    let wi = [];
    if (this.permissionService.hasPermission('/api/Process/Browse/GetWorkflowIssues')) {
      this.processManagementService.getWorkflowIssues(workflowId).subscribe(res => {
        res.forEach(re => {
          wi.push({
            id: re.issueId,
            text: re.issueTitle,
            type: 2
          });
        });
      });
    }
    return wi;
  }

  getIssueWorkflows(issueId) {
    let iw = [];
    if (this.permissionService.hasPermission('/api/Process/Browse/GetIssueWorkflows')) {
      this.processManagementService.getIssueWorkflows(issueId).subscribe(res => {
        if (res && res.length) {
          res.forEach(re => {
            iw.push({
              id: re.workflowId,
              text: re.workflowTitle,
              type: 1
            });
          });
        }
      });
    }
    return iw;
  }

  public hasChildren = (item: any) => item.items && item.items.length > 0;
  public children = (item: any) => of(item.items);

  public closePopup(status) {
    this.openedPopup = false;
  }

  public closeIssuePopup(){
    this.openIssueDetail = !this.openIssueDetail;
  }

  public openPopup(id) {
    this.workflowId = id;
    this.openedPopup = true;
  }
}
