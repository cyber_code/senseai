import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from '../../../services/process-management.service';

@Component({
  selector: 'app-delete-people',
  templateUrl: './delete-people.component.html',
  styleUrls: ['./delete-people.component.css']
})
export class DeletePeopleComponent implements OnInit {
  @Input() deletePeopleWindowOpen;
  @Input() selectedPeople;

  @Output() public closeDeletePeopleWindow = new EventEmitter<any>();
  @Output() public removePeopleFromList = new EventEmitter<boolean>();

  constructor(private processManagment: ProcessManagementService, private messageService: MessageService) {}

  ngOnInit() {}

  deletePeople() {
    this.processManagment.deletePeople(this.selectedPeople.id).subscribe(res => {
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({ text: 'Assignee was deleted successfully!', type: MessageType.Success });
      this.removePeopleFromList.emit(this.selectedPeople);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeletePeopleWindow.emit();
  }
}
