import { Component, OnInit, EventEmitter, Output, Input, ViewChild, AfterViewInit } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from '../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Role } from 'src/app/process-managment/models/role.model';
import { tap, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Helper } from 'src/app/shared/helper';
import { validateComplexValues } from '@progress/kendo-angular-dropdowns/dist/es2015/util';
import { Guid } from 'src/app/shared/guid';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-add-edit-people',
  templateUrl: './add-edit-people.component.html',
  styleUrls: ['./add-edit-people.component.css']
})
export class AddEditPeopleComponent implements OnInit, AfterViewInit {
  @Input() isPeopleEdited;
  @Input() selectedPeople;
  @Input() peopleWindowOpen;
  @Input() 
  set usersAdminPanel(usersAdminPanel){
    this._usersAdminPanel = usersAdminPanel;
    this.filteredUsers = this._usersAdminPanel;
  }
  @Input() 
  set registerUsers (registerUsers){
    this._registerUsers = registerUsers;
    this.filteredUsers = this._usersAdminPanel.filter(row => registerUsers.indexOf(row.id) == -1);
  }
  @ViewChild('cmbRole') cmbRole;
  @Output() public closePeopleWindow = new EventEmitter<any>();
  @Output() public peopleListUpdated = new EventEmitter<boolean>();
  workContext: any;
  subProjectId: string = '';
  filteredRoles: Role[] = [];
  filteredUsers: any[] = [];
  allRoles: Role[];
  _usersAdminPanel: any[];
  _registerUsers: any[] =[];
  public colors = ['#5cb85c', '#00a4be', '#0151cc', '#df340c', '#ff9921', '#5443ac', '#172a4f'];
  constructor(
    private processManagment: ProcessManagementService,
    private messageService: MessageService,
    private sessionService: SessionService,
    public permissionService: PermissionService
  ) {
    if (this.permissionService.hasPermission('/api/People/GetRoles')) {
      this.processManagment.getRoles(this.sessionService.getWorkContext().subProject.id).subscribe(res => {
        this.allRoles = res;
        this.filteredRoles = res;
      });
    }
  }

  ngOnInit() {
    this.workContext = this.sessionService.getWorkContext();
    this.subProjectId = this.sessionService.getWorkContext().subProject.id;
  }

  close() {
    this.closePeopleWindow.emit();
  }

  changeName(event) {
    const name = event.target.value.trim('');
    this.selectedPeople.name = name;
  }

  changeSurname(event) {
    const surname = event.target.value.trim('');
    this.selectedPeople.surname = surname;
  }

  changeAddress(event) {
    const address = event.target.value.trim('');
    this.selectedPeople.address = address;
  }
  changeEmail(event) {
    const email = event.target.value.trim('');
    this.selectedPeople.email = email;
  }
  changeRole(newValue: Role = {} as Role) {
    this.selectedPeople.RoleObject = newValue;
    this.selectedPeople.roleId = newValue.id;
    this.selectedPeople.role = newValue.title;
  }

  changeUser(newValue: any) {
    this.selectedPeople.userAdminPanelObject = newValue ? newValue : {};
    this.selectedPeople.UserAdminPanelId = newValue ? newValue.id : 0;
    this.selectedPeople.userAdminPanel = newValue ? newValue.fullName : "";
    if (newValue && newValue.email)
      this.selectedPeople.email = newValue.email.trim('');
    else if (!this.isPeopleEdited)
      this.selectedPeople.email = "";
    if (newValue && newValue.address)
      this.selectedPeople.address = newValue.address.trim('');
    else if (!this.isPeopleEdited)
      this.selectedPeople.address = "";
    if (newValue && newValue.fullName){
        this.selectedPeople.surname = newValue.fullName.trim('');
    }else if (!this.isPeopleEdited){
      this.selectedPeople.name = "";
      this.selectedPeople.surname = "";
    }
  }
  disableSubmitBtn() {
    return (
      this.selectedPeople.name === '' ||
      this.selectedPeople.roleId === '' ||
      this.selectedPeople.roleId === undefined
    );
  }

  submitClicked() {
    if (this.isPeopleEdited) {
      this.editPeople();
    } else {
      this.addPeople();
    }
  }

  addPeople() {
    this.processManagment
      .addPeople(
        this.selectedPeople.roleId,
        this.selectedPeople.name,
        this.selectedPeople.surname,
        this.selectedPeople.email,
        this.selectedPeople.address,
        this.selectedPeople.UserAdminPanelId,
        this.subProjectId,
        this.colors[Math.floor(Math.random() * 7)]
      )
      .subscribe(res => {
        this.handlePeopleResponse(res);
      });
  }

  editPeople() {
    this.processManagment
      .updatePeople(
        this.selectedPeople.id,
        this.selectedPeople.roleId,
        this.selectedPeople.name,
        this.selectedPeople.surname,
        this.selectedPeople.email,
        this.selectedPeople.address,
        this.selectedPeople.UserAdminPanelId,
        this.subProjectId,
        this.selectedPeople.color
      )
      .subscribe(res => {
        this.handlePeopleResponse(res);
      });
  }
  handleFilter(value, field) {
    if (field === 'role')
      return this.filteredRoles = this.allRoles.filter(x => Helper.dataSourceFilter(x.title, value));
    this.filteredUsers = value ? this._usersAdminPanel.filter(row => this._registerUsers.indexOf(row.id) == -1).filter(x => Helper.dataSourceFilter(x.fullName, value)) : 
    this._usersAdminPanel.filter(row => this._registerUsers.indexOf(row.id) == -1);
  }
  ngAfterViewInit() {}
  handlePeopleResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isPeopleEdited ? 'Assignee was updated successfully!' : 'Assignee was added successfully!',
        type: MessageType.Success
      });
      this.peopleListUpdated.emit(this.isPeopleEdited ? this.selectedPeople : { ...this.selectedPeople, id: res.id });
    } else {
      this.close();
    }
  }
}
