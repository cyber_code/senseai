import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { SessionService } from 'src/app/core/services/session.service';
import * as _ from 'lodash';
import { Role } from 'src/app/process-managment/models/role.model';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit {
  @Input() gridData;
  @Input() state;
  @Input() people;
  @Input() 
  set usersAdminPanel(usersAdminPanel) {
    this._usersAdminPanel = usersAdminPanel;
  };
  @Input() searchCriteria;

  @Output() peopleDataStateChanged = new EventEmitter<any>();
  @Output() peopleListChanged = new EventEmitter<any>();
  @Output() search = new EventEmitter();
  @Output() onClearSearch = new EventEmitter();

  public selectedPeople = {
    name: '',
    surname: '',
    address: '',
    email: '',
    roleId: '',
    role: '',
    RoleObject: new Role(),
    userAdminPanelObject: {},
    UserAdminPanelId: 0,
    userAdminPanel: ''
  };
  public _usersAdminPanel = [];
  public peopleWindowOpen = false;
  public deletePeopleWindowOpen = false;
  public isPeopleEdited = false;
  public registerUsers : any[] =[];
  public subProjectId: string;

  constructor(private sessionService: SessionService, public permissionService: PermissionService) {}

  ngOnInit() {
    this.subProjectId = this.sessionService.getSubProject().id;
  }

  dataStateChange(state) {
    this.peopleDataStateChanged.emit(state);
  }

  openPeopleWindow(people?) {
    this.peopleWindowOpen = true;
    this.registerUsers = this.people.filter(row => (people ? people.id !== row.id : true)  && (row.userIdFromAdminPanel !== 0 || (row.userAdminPanelObject && row.userAdminPanelObject.id))).map(row => {
      return (row.userIdFromAdminPanel !== 0 && row.userIdFromAdminPanel !== undefined) ? row.userIdFromAdminPanel: row.userAdminPanelObject.id;
    });
    if (people) {
      this.isPeopleEdited = true;
      this.selectedPeople = _.cloneDeep(people);
      this.selectedPeople.RoleObject = new Role();
      this.selectedPeople.RoleObject.id = this.selectedPeople.roleId;
      this.selectedPeople.RoleObject.title = this.selectedPeople.role;
    } else {
      this.resetPeople();
    }
  }

  close() {
    this.peopleWindowOpen = false;
    this.deletePeopleWindowOpen = false;
    this.resetPeople();
  }

  resetPeople() {
    this.isPeopleEdited = false;
    this.selectedPeople = {
      name: '',
      surname: '',
      address: '',
      email: '',
      roleId: '',
      role: '',
      RoleObject: new Role(),
      userAdminPanelObject: {},
      UserAdminPanelId: 0,
      userAdminPanel: ''
    };
  }

  onSearch(ev) {
    this.search.emit(ev);
  }

  clearSearch() {
    this.onClearSearch.emit();
  }


  updatePeopleList(person) {
    let people = [];
    if (this.isPeopleEdited) {
      people = this.people.map(iType => {
        if (iType.id === person.id) {
          return person;
        }
        return iType;
      });
    } else {
      people = this.people.concat(person);
    }
    this.peopleListChanged.emit(people);
    this.close();
  }

  removePeopleFromList(person) {
    let people = this.people.filter(iType => iType.id !== person.id);
    this.peopleListChanged.emit(people);
    this.close();
  }

  openDeletePeopleWindow(people) {
    this.deletePeopleWindowOpen = true;
    this.selectedPeople = people;
  }

  addPeoplePermission() {
    return(
      this.permissionService.hasPermission('/api/People/GetPeoples') &&
      this.permissionService.hasPermission('/api/People/AddPeople')
    );
  }

  editPeoplePermission() {
    return(
      this.permissionService.hasPermission('/api/People/GetPeoples') &&
      this.permissionService.hasPermission('/api/People/UpdatePeople')
    );
  }

  deletePeoplePermission() {
    return(
      this.permissionService.hasPermission('/api/People/GetPeoples') &&
      this.permissionService.hasPermission('/api/People/DeletePeople')
    );  
  }
}
