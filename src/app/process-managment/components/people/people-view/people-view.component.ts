import { Component, OnInit } from '@angular/core';
import { State, process, CompositeFilterDescriptor } from '@progress/kendo-data-query';
import { SessionService } from 'src/app/core/services/session.service';
import { DesignService } from 'src/app/design/services/design.service';
import { ProcessManagementService } from '../../../services/process-management.service';
import {UserService} from './users.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-people-view',
  templateUrl: './people-view.component.html',
  styleUrls: ['./people-view.component.css']
})
export class PeopleViewComponent implements OnInit {
  public people = [];
  public peopleGridData: any = [];
  public usersAdminPanel: any[] = [];
  public searchCriteria = "";

  workContext: any;
  public peopleState: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };

  constructor(
    private processManagment: ProcessManagementService, 
    private sessionService: SessionService, 
    private userService: UserService,
    public permissionService: PermissionService
  ) {}

  async ngOnInit() {
    const people = this.permissionService.hasPermission('/api/People/GetPeoples') ?  await this.getPeople() : [];
    this.usersAdminPanel = await this.userService.GetAllUsers().toPromise();
    this.people = people.map(p =>{
      let userAdminPanel = this.usersAdminPanel.find(row => row.id === p.userIdFromAdminPanel);
      return {...p, userAdminPanelObject: userAdminPanel, UserAdminPanelId: userAdminPanel ? userAdminPanel.id : 0, userAdminPanel: userAdminPanel ? userAdminPanel.fullName: '' };
    });
    this.peopleGridData = process(this.people, this.peopleState);

  }

  getPeople() {
    this.workContext = this.sessionService.getWorkContext();
    let subProjectId = this.sessionService.getSubProject().id;
    return this.processManagment.getPeoples(subProjectId).toPromise();
  }

  peopleDataStateChange(state) {
    this.peopleState = state;
    this.peopleGridData = process(this.people, state);
  }

  peopleListChange(people) {
    this.people = people;
    this.peopleGridData = process(people, this.peopleState);
  }

  onSearch(ev){
    this.searchCriteria = ev.target.value.trim('');
    if (ev.target.value !== "") {
      let searchFilters: CompositeFilterDescriptor = {logic: "or", filters: []};
      searchFilters.filters.push({field: "name", operator: 'contains', value: this.searchCriteria});
      searchFilters.filters.push({field: "userAdminPanel", operator: 'contains', value: this.searchCriteria});
      searchFilters.filters.push({field: "surname", operator: 'contains', value: this.searchCriteria});
      searchFilters.filters.push({field: "role", operator: 'contains', value: this.searchCriteria});
      searchFilters.filters.push({field: "email", operator: 'contains', value: this.searchCriteria});
      this.peopleState.filter.filters.push(searchFilters);
      this.peopleGridData = process(this.people, this.peopleState); 
    }
    else {
        this.clearSearch();
    }      
}

clearSearch(){
    this.searchCriteria = "";
    this.peopleState.filter.filters  = this.peopleState.filter.filters.filter(filterObject => {
      if (filterObject["logic"] && filterObject["filters"][0] && 
      (filterObject["filters"][0].field === "name" || 
      filterObject["filters"][0].field === "userAdminPanel" ||
      filterObject["filters"][0].field === "surname" ||
      filterObject["filters"][0].field === "role" ||
      filterObject["filters"][0].field === "email"))
        return false;
      return true;
    });
    this.peopleGridData = process(this.people, this.peopleState);
}
}
