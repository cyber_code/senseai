import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import {Observable} from 'rxjs';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {GetAllUsers} from './command-queries';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private identityUrl = '';
  public authenticationChallange: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public authenticationChallange$ = this.authenticationChallange.asObservable();

  constructor(private configurationService: ConfigurationService, private httpExecutor: HttpExecutorService) {
    this.identityUrl = `${configurationService.serverSettings.identityUrl}User/`;
  }

  GetAllUsers(): Observable<[]> {
    return this.httpExecutor.executeQuery<[]>(this.identityUrl, new GetAllUsers());
  }
}
