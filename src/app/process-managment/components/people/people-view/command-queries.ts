import {ApiAction} from 'src/app/core/services/command-queries/api-action';

export class GetAllUsers extends ApiAction {
  constructor() {
    super('GetAllUsers');
  }

  userId: string;
}

