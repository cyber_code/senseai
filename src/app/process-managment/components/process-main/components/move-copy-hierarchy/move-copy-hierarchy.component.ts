import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import { SessionService } from "src/app/core/services/session.service";
import { ProcessManagementService } from "src/app/process-managment/services/process-management.service";
import { HierarchyItemsComponent } from "../hierarchy-items/hierarchy-items.component";
import { Guid } from "src/app/shared/guid";
import { isNullOrUndefined } from "util";

@Component({
  selector: "app-move-copy-hierarchy",
  templateUrl: "move-copy-hierarchy.component.html",
  styleUrls: ["move-copy-hierarchy.component.css"]
})
export class MoveCopyHierarchyComponent implements OnInit {
    public isCopyHierarchyChecked = true;
    public isMoveHierarchyChecked = false;
    public _openMoveCopyHierarchy;
    public hierarchies = [];

    @Input() hierarchyToMoveCopy;
    @Input() 
    set openMoveCopyHierarchy(openMoveCopyHierarchy) {
        if (openMoveCopyHierarchy) {
            if (!this.hierarchyToMoveCopy || this.hierarchyToMoveCopy.parentId === Guid.empty) {
                this.copyHierarchy(Guid.empty);
                this.close();
            } else {
                this.processManagementService.getHierarchy(this.hierarchyToMoveCopy.parentId).subscribe(hierarchy => {
                    if (!isNullOrUndefined(hierarchy)) {
                        this.getHierarchies(hierarchy);
                    }
                });
                this._openMoveCopyHierarchy = openMoveCopyHierarchy;
            }
        }

        
    };
    

    

    @Output() closeMoveCopyHierarchy = new EventEmitter();

    constructor(
        private processManagementService: ProcessManagementService,
        private sessionService: SessionService,
        private messageService: MessageService
    ) {}

    ngOnInit() {}

    copyHierarchyChecked() {
        this.isCopyHierarchyChecked = true;
        this.isMoveHierarchyChecked = false;
    }

    moveHierarchyChecked() {
        this.isCopyHierarchyChecked = false;
        this.isMoveHierarchyChecked = true;
    }

    disableSubmitBtn() {
        return ((!this.hierarchyToMoveCopy || !this.hierarchyToMoveCopy.parent) && this.hierarchies.length > 0);
    }

    close() {
        this.closeMoveCopyHierarchy.emit();
        this.isCopyHierarchyChecked = true;
        this.isMoveHierarchyChecked = false;
        this._openMoveCopyHierarchy = null;
        this.hierarchies = [];
    }

    async getHierarchies(hierarchy) {
        if (hierarchy.firstLevel) {
            this.hierarchies = [];
            return;
        }

        let subProjectId = this.sessionService.getSubProject().id;

        hierarchy.parentId = isNullOrUndefined(hierarchy.parentId) ? '' : hierarchy.parentId;
        hierarchy.hierarchyTypeId = isNullOrUndefined(hierarchy.hierarchyTypeId) ? '' : hierarchy.hierarchyTypeId;
        let processHierarchy = hierarchy;

        if (hierarchy.parentId !== '' && hierarchy.parentId !== Guid.empty) {
            processHierarchy = await this.processManagementService.getHierarchy(hierarchy.parentId).toPromise();
        }

        let hierarchyTypeId = (hierarchy.parentId === '' || hierarchy.parentId === Guid.empty) ? '': processHierarchy.hierarchyTypeId;
        let parentId = (hierarchy.parentId === '' || hierarchy.parentId === Guid.empty) ? '' : processHierarchy.id;

        this.processManagementService.getHierarchiesByParent(subProjectId, hierarchyTypeId, parentId).subscribe(hierarchies => {
            if (hierarchies) {
                this.hierarchies = [...hierarchies];
                this.hierarchyToMoveCopy.parent = hierarchy;
            }
        });
    }

    onChangeHierarchy(hierarchy) {
        this.hierarchyToMoveCopy.parent = hierarchy;
    }

    copyHierarchy(parentId) {
        let subProjectId = this.sessionService.getSubProject().id;
        this.processManagementService
            .addHierarchy(
                parentId, 
                subProjectId,
                this.hierarchyToMoveCopy.parent ? this.hierarchyToMoveCopy.parent.hierarchyTypeId : Guid.empty,
                this.hierarchyToMoveCopy.title + " - Copy",
                this.hierarchyToMoveCopy.description ? this.hierarchyToMoveCopy.description : '').subscribe((res : any) => {
                    if (res) {
                        this.processManagementService.hierarchyAdded.next({...this.hierarchyToMoveCopy, id: res.id, title: this.hierarchyToMoveCopy.title + " - Copy", parentId: parentId});
                        this.messageService.sendMessage({text: 'Hierarchy was copied successfully!', type: MessageType.Success});
                    }
                });
    }

    cutHierarchy(parentId) {
        let subProjectId = this.sessionService.getSubProject().id;

        this.processManagementService
            .updateHierarchy(
                this.hierarchyToMoveCopy.id,
                parentId,
                subProjectId,
                (parentId === Guid.empty) ? Guid.empty : this.hierarchyToMoveCopy.hierarchyTypeId,
                this.hierarchyToMoveCopy.title,
                this.hierarchyToMoveCopy.description ? this.hierarchyToMoveCopy.description : ''
            ).subscribe(res => {
                this.processManagementService.hierarchyRemoved.next(this.hierarchyToMoveCopy);
                this.processManagementService.hierarchyAdded.next({...this.hierarchyToMoveCopy, parentId: parentId});
                this.messageService.sendMessage({text: 'Hierarchy was moved successfully!', type: MessageType.Success});
            });
    }

    submitClicked() {
        let parentId = this.hierarchyToMoveCopy.parent? this.hierarchyToMoveCopy.parent.id : Guid.empty;
        let type = this.isCopyHierarchyChecked ? 0 : 1;

        if (type === 0) {
            this.copyHierarchy(parentId);
        }

        else if (type === 1) {
            this.cutHierarchy(parentId);
        }

        this.close();

    
    }
} 
