import { Component, OnInit, AfterViewInit, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { InspectorService } from '../../../../services/rapid-services/inspector-service';
import { StencilService } from '../../../../services/rapid-services/stencil-service';
import { ToolbarService } from '../../../../services/rapid-services/toolbar-service';
import { HaloService } from '../../../../services/rapid-services/halo-service';
import { KeyboardService } from '../../../../services/rapid-services/keyboard-service';
import { KitchenSinkService } from '../../../../services/rapid-services/kitchensink-service';
import { RapidEventsService } from '../../../../services/rapid-events.service';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import {ProcessType} from '../../../../models/process-type';

@Component({
  selector: 'app-preview-workflow',
  templateUrl: './preview-workflow.component.html',
  styleUrls: ['./preview-workflow.component.css']
})
export class PreviewWorkflowComponent implements OnInit, AfterViewInit {
  private rappid: KitchenSinkService;
  public _openPreviewWorkflow;
  
  @Input() process;
  
  @Input() 
  set openPreviewWorkflow(openPreviewWorkflow) {
    if (openPreviewWorkflow && !this._openPreviewWorkflow && this.process) {
        this.rappid = new KitchenSinkService(
          this.element.nativeElement as any,
          new StencilService(this.process && this.process.processType === ProcessType.Aris),
          new ToolbarService(),
          new InspectorService(),
          new HaloService(),
          new KeyboardService(),
          this.rapidEventsService
        );
    
        this.rappid.startRappid();
        this.loadWorkflow(this.process);
    }

    this._openPreviewWorkflow = openPreviewWorkflow;
  }

  constructor(
    private rapidEventsService: RapidEventsService,
    private element: ElementRef,
    private processManagementService: ProcessManagementService
  ) {}

  ngOnInit(): void {
    
  }

  ngAfterViewInit(): void {}

  async loadWorkflow(process) {

    if (process.processType === ProcessType.Aris) {
      this.processManagementService.getArisWorkflows(process.id).subscribe((res: any) => {
        let designerJson = res && JSON.parse(res.designerJson);
        this.rappid.loadGraphJson(designerJson);
      });
    } else {
      this.processManagementService.getGenericWorkflows(process.id).subscribe((res: any) => {
        let designerJson = res && JSON.parse(res.designerJson);
        this.rappid.loadGraphJson(designerJson);
      });
    }
  }
    
}

 
