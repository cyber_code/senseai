import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Guid } from 'src/app/shared/guid';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';

@Component({
  selector: 'app-process-description',
  templateUrl: './process-description.component.html',
  styleUrls: ['./process-description.component.css']
})
export class ProcessDescriptionComponent implements OnInit {

    @Input() selectedProcess;
    public descriptionHeight = '20vh';

    constructor(private processManagementService: ProcessManagementService) {
        this.processManagementService.processUpdated.subscribe(process => {
            if (this.selectedProcess && process && this.selectedProcess.id === process.id) {
                this.selectedProcess = process;
            }
        });
    }

    ngOnInit() {}

    onResizeEnd(event) {
        this.descriptionHeight = event.rectangle.height + 'px';
    }
}
