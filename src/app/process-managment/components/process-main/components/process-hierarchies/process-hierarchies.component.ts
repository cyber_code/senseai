import {Component, OnInit, ViewChild} from '@angular/core';
import {ProcessManagementService} from '../../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Guid } from 'src/app/shared/guid';
import { HierarchyItemsComponent } from '../hierarchy-items/hierarchy-items.component';
import {WorkflowIssuesDataService} from '../../../../services/workflow-issues-data.service';
import { isNullOrUndefined } from 'util';
import { ProcessStatisticsComponent } from '../process-statistics/process-statistics.component';
import { HierarchyStatisticsComponent } from '../hierarchy-statistics/hierarchy-statistics.component';
import { WorkflowIssuesComponent } from '../../../workflow-issues/workflow-issues.component';
import { ProcessItemsComponent } from '../process-items/process-items.component';
import { PermissionService } from 'src/app/core/services/permission.service';
import { takeWhileAlive, AutoUnsubscribe } from 'take-while-alive';

@Component({
  selector: 'app-process-hierarchies',
  templateUrl: './process-hierarchies.component.html',
  styleUrls: ['./process-hierarchies.component.css']
})
@AutoUnsubscribe()
export class ProcessHierarchiesComponent implements OnInit {

    public selectedHierarchy;
    public selectedProcess;
    public searchCriteria = '';
    public listOfHierarchies = [];
    public listOfProcesses = [];
    public resetHierarchyProcessList = false;
    public processRemoved;
    public processAdded;
    public subProjectId:string;
    public navigation = [];
    public processDetail;
    public openProcessDetailsWindow = false;
    public hierarchyTypes = [];
    public isLastLevelHierarchy = false;
    public openProcessDetails = false;
    public allIssues = [];
    public selectedProcesses = [];

    @ViewChild(HierarchyItemsComponent)
    public hierarchyItemsComponent: HierarchyItemsComponent;

    @ViewChild(ProcessItemsComponent)
    public processItemsComponent: ProcessItemsComponent;

    @ViewChild(HierarchyStatisticsComponent)
    public hierarhcyStatisticsComponent: HierarchyStatisticsComponent;

    @ViewChild(ProcessStatisticsComponent)
    public processStatisticsComponent: ProcessStatisticsComponent;

    @ViewChild(WorkflowIssuesComponent)
    public workflowIssuesComponent: WorkflowIssuesComponent;

    constructor(
        private processManagementService: ProcessManagementService,
        private sessionService: SessionService,
        private workflowIssuesDataService: WorkflowIssuesDataService,
        public permissionService: PermissionService
    ) {
        this.processManagementService.issueReqWorkflowUpdated.pipe(takeWhileAlive(this)).subscribe(() => {
            if (this.hierarhcyStatisticsComponent) {
                this.hierarhcyStatisticsComponent.updateStatistics(this.selectedHierarchy);
            }
            if (this.processStatisticsComponent) {
                this.processStatisticsComponent.updateProcessStatistics(this.selectedProcess);
            }
            if (this.workflowIssuesComponent) {
                this.workflowIssuesComponent.loadWorkflowAndIssues(this.selectedProcess.id);
            }
        });
    }

    ngOnInit() {
      let subProjectId = this.sessionService.getSubProject().id;
      this.subProjectId = subProjectId;
      if (this.permissionService.hasPermission('/api/Process/Configure/GetHierarchyTypes')) {
        this.processManagementService.getHierarchyTypes(subProjectId).subscribe(hierarchyTypes => {
            if (hierarchyTypes) {
                this.hierarchyTypes = hierarchyTypes;
            }
        });
      }
      if (this.permissionService.hasPermission('/api/Process/Browse/GetIssues')) {
        this.processManagementService.getIssues(subProjectId).subscribe(issues => {
            if (issues) {
                this.allIssues = issues;
            }
        });
      }
    }

    hierarchyClicked(hierarchy) {
        this.selectedHierarchy = hierarchy;
        if (hierarchy) {
            let navigation = [hierarchy];
            this.navigation = this.getNavigationRecursively(navigation);
        } else {
            let currentHierarchyLevel = this.hierarchyItemsComponent.hierarchies[0].index;
            if (isNullOrUndefined(currentHierarchyLevel)) {
                this.navigation = [];
            } else {
                let navigation = this.navigation.filter(nav => !nav.index || nav.index < currentHierarchyLevel);
                this.navigation = navigation;
                let parentEl = navigation[navigation.length - 1];
                if (parentEl) {
                    this.hierarchyItemsComponent.onHierarchyClick(parentEl);
                } 
                
            }
        }
        
    }

    getNavigationRecursively(navigation) {
        if (this.hierarchyItemsComponent && this.hierarchyItemsComponent.allHierarchies){
            const parent = this.hierarchyItemsComponent.allHierarchies.find(hierarchy => hierarchy.id === navigation[0].parentId);
            if (isNullOrUndefined(parent)) {
              return navigation;
            } else {
              navigation.unshift(parent);
              return this.getNavigationRecursively(navigation);
            }
          } else {
            return [];
          }
    }

    processClicked(process) {
        this.selectedProcess = process;
    }

    processSelection(event) {
        this.selectedProcesses = event;
    }

    requirementsMoved() {
        this.selectedProcesses = [];
        if (this.processItemsComponent) {
            this.processItemsComponent.selectedProcesses = [];
            this.processItemsComponent.processClick(null);
        }
    }

    navigateClicked(hierarchy) {
        if (hierarchy.id !== this.navigation[this.navigation.length - 1].id) {
            this.hierarchyItemsComponent.lastHierarchyIndex = hierarchy.index ? hierarchy.index : 0;
            let filterIndex = this.hierarchyItemsComponent.lastHierarchyIndex;
            this.hierarchyItemsComponent.hierarchies = this.hierarchyItemsComponent.allHierarchies
                .filter(hrc => hrc.index === filterIndex ||  (filterIndex === 0 && !hrc.index));
            this.hierarchyItemsComponent.allHierarchies = this.hierarchyItemsComponent.allHierarchies
                .filter(hrc => hrc.index <= filterIndex || !hrc.index);
            this.navigation = this.navigation.filter(nav => !nav.index || nav.index <= hierarchy.index);
            this.hierarchyItemsComponent.onHierarchyClick(hierarchy);
        }
        
    }

    goBack() {
        if (this.hierarchyItemsComponent.lastHierarchyIndex > 0) {
            this.hierarchyItemsComponent.lastHierarchyIndex -= 1;
            let filterIndex = this.hierarchyItemsComponent.lastHierarchyIndex;
            this.hierarchyItemsComponent.hierarchies = this.hierarchyItemsComponent.allHierarchies
                .filter(hrc => hrc.index === filterIndex 
                    || (filterIndex === 0 && !hrc.index)
                );
            this.hierarchyItemsComponent.allHierarchies = this.hierarchyItemsComponent.allHierarchies
            .filter(hrc => hrc.index !== (filterIndex + 1));
            if (!this.selectedHierarchy || !this.hierarchyItemsComponent.hierarchies.find(hrc => hrc.id === this.selectedHierarchy.id)) {
                let currentHierarchyLevel = this.hierarchyItemsComponent.hierarchies[0].index;
                let navigation = this.navigation.filter(nav => !nav.index || nav.index <= currentHierarchyLevel);
                this.navigation = navigation;
                this.hierarchyClicked(navigation[navigation.length - 1]);
                this.hierarchyItemsComponent.onHierarchyClick(navigation[navigation.length - 1]);
            }
        }
    }

    onChangeHierarchies(event) {
        this.resetHierarchyProcessList = false;
        let searchCriteria = event.target.value.trim('');
        this.searchCriteria = searchCriteria;

        let parentId = this.selectedHierarchy ? this.selectedHierarchy.id : Guid.empty;
        if (searchCriteria !== '') {
            this.processManagementService.searchHierarchies(this.subProjectId, parentId, searchCriteria).subscribe((res: any) => {
                this.listOfHierarchies = res.listOfHierarchies;
                this.listOfProcesses = res.listOfProcesses;
            });
        } else {
            this.resetHierarchyProcessList = true;
        }
    }

    setRemovedProcess(process) {
        this.processRemoved = process;
    }

    setAddedProcess(process) {
        this.processAdded = process;
    }

    setLastLevelHierarchy(hierarchyTypeTitle) {
        this.isLastLevelHierarchy = (isNullOrUndefined(hierarchyTypeTitle) || hierarchyTypeTitle === '');
    }

    processDetailsClicked() {
        this.openProcessDetails = true;
    }

    goBackClicked(process) {
        this.openProcessDetails = false;
        if (this.hierarhcyStatisticsComponent) {
            this.hierarhcyStatisticsComponent.updateStatistics(this.selectedHierarchy);
        }
        if (this.processStatisticsComponent) {
            this.processStatisticsComponent.updateProcessStatistics(process);
        }
        if (this.workflowIssuesComponent) {
            this.workflowIssuesComponent.loadWorkflowAndIssues(process.id);
        }
    }

    

    processUpdated(process) {
        if (this.selectedProcess && this.selectedProcess.id === process.id) {
            this.selectedProcess = Object.assign({}, process);
        }
    }
}
