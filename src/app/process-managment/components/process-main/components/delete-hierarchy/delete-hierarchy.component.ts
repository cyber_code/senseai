import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';

@Component({
  selector: 'app-delete-hierarchy',
  templateUrl: 'delete-hierarchy.component.html',
  styleUrls: ['delete-hierarchy.component.css']
})
export class DeleteHierarchyComponent implements OnInit {
  public _deleteHierarchyWindowOpen;
  public selectedHierarchy;
  public message = '';
  public deleted = false;
  public deleteHrc = true;
  public moveHrc = false;
  public selectedParent;
  public hierarchies = [];
  @Input() allHierarchies;
  @Input() hierarchyTypeTitle;
  @Input()
  set _selectedHierarchy(selectedHierarchy) {
    if (selectedHierarchy) {
      this.message = 'Are you sure you want to delete ' + selectedHierarchy.title + ' ' + this.hierarchyTypeTitle + '?';
      this.hierarchies = this.allHierarchies
        ? this.allHierarchies.filter(hrc => hrc.parentId === selectedHierarchy.parentId && hrc.id !== selectedHierarchy.id)
        : [];
    }
    this.selectedHierarchy = selectedHierarchy;
  }

  @Input()
  set deleteHierarchyWindowOpen(deleteHierarchyWindowOpen) {
    this.reset();
    this._deleteHierarchyWindowOpen = deleteHierarchyWindowOpen;
  }

  @Output() public closeDeleteHierarchyWindow = new EventEmitter<any>();
  @Output() public removeHierarchyFromList = new EventEmitter<boolean>();

  constructor(private processManagementService: ProcessManagementService, private messageService: MessageService) {
    this.processManagementService.hierarchyRemoved.subscribe(hierarchy => {
      this.removeHierarchyFromList.emit(hierarchy);
    });
  }

  ngOnInit() {}

  deleteHierarchy() {
    if (!this.deleted) {
      this.processManagementService.deleteHierarchy(this.selectedHierarchy.id, false).subscribe((res: any) => {
        if (res && res.result) {
          this.handleResponse(res);
        } else {
          this.deleted = true;
          this.message = this.selectedHierarchy.title + ' is associated with other hierarchies! You can choose to: ';
        }
      });
    } else {
      if (this.deleteHrc) {
        this.processManagementService.deleteHierarchy(this.selectedHierarchy.id, true).subscribe((res: any) => {
          this.handleResponse(res);
        });
      } else {
        this.processManagementService.moveHierarchy(this.selectedHierarchy.id, this.selectedParent.id).subscribe(res => {
          this.handleResponse(res);
        });
      }
    }
  }

  disableSubmitBtn() {
    return this.moveHrc && !this.selectedParent;
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({ text: this.hierarchyTypeTitle + ' was deleted successfully!', type: MessageType.Success });
      this.removeHierarchyFromList.emit(this.selectedHierarchy);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeleteHierarchyWindow.emit();
  }

  reset() {
    this.selectedParent = null;
    this.deleted = false;
    this.deleteHrc = true;
    this.moveHrc = false;
  }

  deleteHrcClicked() {
    this.deleteHrc = true;
    this.moveHrc = false;
  }

  moveHrcClicked() {
    this.deleteHrc = false;
    this.moveHrc = true;
  }
}
