import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';

@Component({
  selector: 'app-delete-process',
  templateUrl: 'delete-process.component.html',
  styleUrls: ['delete-process.component.css']
})
export class DeleteProcessComponent implements OnInit {
  public _deleteProcessWindowOpen;
  public message = 'Are you sure you want to delete this requirement?';

  @Input() selectedProcess;

  @Input()
  set deleteProcessWindowOpen(deleteProcessWindowOpen) {
    this._deleteProcessWindowOpen = deleteProcessWindowOpen;
  }

  @Output() public closeDeleteProcessWindow = new EventEmitter<any>();
  @Output() public removeProcessFromList = new EventEmitter<any>();

  constructor(private processManagementService: ProcessManagementService, private messageService: MessageService) {
    this.processManagementService.processRemoved.subscribe(process => {
      this.removeProcessFromList.emit(process);
    });
  }

  ngOnInit() {}

  deleteProcess() {
    this.processManagementService.deleteProcess(this.selectedProcess.id, true).subscribe((res: any) => {
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({ text: 'Requirement was deleted successfully!', type: MessageType.Success });
      this.removeProcessFromList.emit(this.selectedProcess);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeleteProcessWindow.emit();
  }
}
