import {Component, OnInit, Input, EventEmitter, Output, TemplateRef, HostListener} from '@angular/core';
import {ProcessManagementService} from '../../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Guid } from 'src/app/shared/guid';
import {ProcessType} from '../../../../models/process-type';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-process-items',
  templateUrl: './process-items.component.html',
  styleUrls: ['./process-items.component.css']
})

export class ProcessItemsComponent implements OnInit {
    public _selectedHierarchy;
    public selectedProcesses = [];
    @Output() processClicked = new EventEmitter();
    @Output() listsReseted = new EventEmitter();
    @Output() processRemoved = new EventEmitter();
    @Output() processAdded = new EventEmitter();
    @Output() processUpdated = new EventEmitter();
    @Output() processSelection = new EventEmitter();
    @Input()
    set selectedHierarchy(selectedHierarchy) {
        if (selectedHierarchy && this.permissionService.hasPermission('/api/Process/Browse/GetProcesses')) {
            let subProjectId = this.sessionService.getSubProject().id;
            this.processManagementService.getProcesses(selectedHierarchy.id, subProjectId).subscribe(processes => {
                if (processes) {
                    this.processes = processes.sort((p1, p2) => (p1.title.toLowerCase() > p2.title.toLowerCase()) ? 1 : -1);
                    this.allProcesses = processes.sort((p1, p2) => (p1.title.toLowerCase() > p2.title.toLowerCase()) ? 1 : -1);
                    this.selectedProcess = null;
                    this.selectedProcesses = [];
                    this.processSelection.emit([]);
                    this.processClicked.emit(null);
                }
            });
        } else {
            this.processes = [];
            this.allProcesses = [];
            this.selectedProcess = null;
            this.selectedProcesses = [];
            this.processSelection.emit([]);
            this.processClicked.emit(null);
        }

        this._selectedHierarchy = selectedHierarchy;
    }

    @Input() hideIcons;
    @Input() allIssues;

   

    public processes = [];
    public allProcesses = [];
    public selectedProcess;
    public activateClick;
    public actionClicked;
    public openAddProcess;
    public processToAdd = {
        title: '',
        description: '',
        hierarchyId: '',
        ownerId: Guid.empty,
        processType: ProcessType.Aris
    };
    public deleteProcessWindowOpen;
    public processToDelete;
    public processToPreview;
    public openPreviewWorkflow;
    

    @Input()
    set listOfProcesses(listOfProcesses) {
        this.processes = listOfProcesses.sort((p1, p2) => (p1.title.toLowerCase() > p2.title.toLowerCase()) ? 1 : -1);
    }

    @Input() 
    set resetHierarchyProcessList(resetHierarchyProcessList) {
        if (resetHierarchyProcessList) {
            this.processes = this.allProcesses;
        }
    }

    @Input() searchCriteria;
    
    constructor(
        private processManagementService: ProcessManagementService,
        private sessionService: SessionService,
        public permissionService: PermissionService
    ) {
        this.processManagementService.processAdded.subscribe(process => {
            this.addProcess(process);
        });
    }

    @HostListener("click", ['$event'])
    onDropdownItemClick( evt: any) {
        if (evt && evt.target && evt.ctrlKey && (evt.target.className.includes('process') || evt.target.className.includes('title'))) {
            if (evt.target.id && !this.selectedProcesses.find(el => el.id === evt.target.id)) {
                let item = this.processes.find(el => el.id === evt.target.id);
                this.selectedProcesses.push(item);
                this.processSelection.emit(this.selectedProcesses);
            }
            else if (evt.target.id) {
                this.selectedProcesses = this.selectedProcesses.filter(el => el.id !== evt.target.id);
                if (this.selectedProcess && !this.selectedProcesses.find(process => process.id === this.selectedProcess.id)) {
                    this.selectedProcess = null;
                    this.processClick(null);
                }
                this.processSelection.emit(this.selectedProcesses);
            }
        } else if (evt && evt.target && (evt.target.className.includes('process') || evt.target.className.includes('title'))) {
            if (evt.target.id) {
                let item = this.processes.find(el => el.id === evt.target.id);
                this.selectedProcesses = [item];
                this.processClick(item);
                this.processSelection.emit(this.selectedProcesses);
            }
        }
    }

    ngOnInit() {}
    
    closeProcessDialog() {
        this.openAddProcess = false;
    }

    processClick(item) {

        if (this.actionClicked) {
            this.actionClicked = false;
            return;
        }

        if (!this.selectedProcess || !item || item.id !== this.selectedProcess.id) {
            this.selectedProcess = item;
            this.processClicked.emit(item);
        } 
    }

    isActive(item) {
        return this.selectedProcesses.find(process => process.id === item.id);
    }

    showActions(process) {
        this.processes = this.processes.map(item => {
          if (item.id === process.id) {
            return {...item, showActions: item.showActions ? false : true};
          }
          return {...item, showActions: false};
        });
      }
    

    editProcess(process) {
        this.actionClicked = true;
        process.showActions = false;
        this.openAddProcess = true;
        this.processToAdd = Object.assign({}, process);
    }

    deleteProcess(process) {
        this.actionClicked = true;
        process.showActions = false;
        this.deleteProcessWindowOpen = true;
        this.processToDelete = process;
    }

    closeDeleteProcessWindow() {
        this.deleteProcessWindowOpen = false;
    }

    removeProcessFromList(process) {
        this.processes = this.processes.filter(proc => proc.id !== process.id);
        this.allProcesses = this.allProcesses.filter(proc => proc.id !== process.id);
        if (this.selectedProcess && this.selectedProcess.id === process.id) {
            this.processClick(null);
        }
        this.processRemoved.emit(process);
        this.closeDeleteProcessWindow();
    }

    addProcess(process) {
        if (this._selectedHierarchy && this._selectedHierarchy.id === process.hierarchyId && 
            (this.searchCriteria === '' || this.searchCriteria.toLowerCase().includes(process.title.toLowerCase()))) {
            this.processes = this.processes.concat(process).sort((p1, p2) => (p1.title.toLowerCase() > p2.title.toLowerCase()) ? 1 : -1);
            this.allProcesses = this.allProcesses.concat(process).sort((p1, p2) => (p1.title.toLowerCase() > p2.title.toLowerCase()) ? 1 : -1);
            this.processAdded.emit(process);
        }
    }

    updateProcessList(process) {
        if (process.hierarchyId === this._selectedHierarchy.id) {
            let processes = this.processes.map(proc => {
                if (proc.id === process.id) {
                    return process;
                }
                return proc;
            });
    
            let allProcesses = this.allProcesses.map(proc => {
                if (proc.id === process.id) {
                    return process;
                }
                return proc;
            });
            this.processUpdated.emit(process);
            this.processes = processes.sort((p1, p2) => (p1.title.toLowerCase() > p2.title.toLowerCase()) ? 1 : -1);
            this.allProcesses = allProcesses.sort((p1, p2) => (p1.title.toLowerCase() > p2.title.toLowerCase()) ? 1 : -1);
        } else {
            this.processes = this.processes.filter(proc => proc.id !== process.id);
            this.allProcesses = this.allProcesses.filter(proc => proc.id !== process.id);
            this.processRemoved.emit(process);
            if (this.selectedProcess && this.selectedProcess.id === process.id) {
                this.selectedProcess = null;
                this.processClicked.emit(null);
            }
        }
        this.processManagementService.processUpdated.next(process);
        this.closeProcessDialog();
    }

    previewWorkflow(process: any) {
        this.actionClicked = true;
        process.showActions = false;
        this.processToPreview = process;
        this.openPreviewWorkflow = true;
    }

    closePreviewWorkflow() {
        this.openPreviewWorkflow = false;
        this.processToPreview = null;
    }

    previewProcessPermission(process) {
        return (
            (process.processType === ProcessType.Aris && this.permissionService.hasPermission('/api/Process/ArisWorkflow/GetArisWorkflows')) ||
            (process.processType === ProcessType.Generic && this.permissionService.hasPermission('/api/Process/GenericWorkflow/GetGenericWorkflows'))
        );
    }

    updateProcessPermission() {
        return(
            this.permissionService.hasPermission('/api/Process/Browse/GetHierarchiesByParent') &&
            this.permissionService.hasPermission('/api/Process/Browse/UpdateProcess')
        );
    }

    showProcessActions() {
        return (
            this.updateProcessPermission() ||
            this.permissionService.hasPermission('/api/Process/ArisWorkflow/GetArisWorkflows') ||
            this.permissionService.hasPermission('/api/Process/Browse/DeleteProcess')
        );
    }
}
