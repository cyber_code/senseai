import {Component, OnInit, Input} from '@angular/core';
import {ProcessManagementService} from '../../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-hierarchy-statistics',
  templateUrl: './hierarchy-statistics.component.html',
  styleUrls: ['./hierarchy-statistics.component.css']
})
export class HierarchyStatisticsComponent implements OnInit {
    public hierarchyStatistics = {
        nrProcesses: 0,
        nrWorkflows: 0,
        nrTotalIssues: 0,
        nrOpenIssues: 0
    };

    public _selectedHierarchy;

    @Input()
    set selectedHierarchy(selectedHierarchy) {
        this.updateStatistics(selectedHierarchy);
        this._selectedHierarchy = selectedHierarchy;
    }

    updateStatistics(selectedHierarchy) {
        if (selectedHierarchy && this.permissionService.hasPermission('/api/Process/Browse/GetHierarchyStatistics')) {
            let subProjectId = this.sessionService.getSubProject().id;
            this.processManagementService.getHierarchyStatistics(subProjectId, selectedHierarchy.id).subscribe(hierarchyStatistics => {
                this.hierarchyStatistics = hierarchyStatistics;
            });
        } else {
            this.hierarchyStatistics = {
                nrProcesses: 0,
                nrWorkflows: 0,
                nrTotalIssues: 0,
                nrOpenIssues: 0
            };
        }
    }

    @Input()
    set processRemoved(processRemoved) {
        if (this._selectedHierarchy && processRemoved && this._selectedHierarchy.id === processRemoved.hierarchyId) {
            this.updateStatistics(this._selectedHierarchy);
        }
    }

    @Input()
    set processAdded(processAdded) {
        if (this._selectedHierarchy && processAdded && this._selectedHierarchy.id === processAdded.hierarchyId) {
            this.updateStatistics(this._selectedHierarchy);
        }
    }

    constructor(
        private processManagementService: ProcessManagementService,
        private sessionService: SessionService,
        public permissionService: PermissionService
    ) {}

    ngOnInit() {
    }
}
