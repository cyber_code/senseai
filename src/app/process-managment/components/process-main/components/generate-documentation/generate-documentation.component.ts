import { Component, Inject, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-generate-documentation',
  templateUrl: './generate-documentation.component.html',
  styleUrls: ['./generate-documentation.component.css']
})
export class GenerateDocumentationComponent implements OnInit {
  public _openDocumentGeneration;
  latestCheck = 'checked';
  dateCheck = '';
  public value: Date = new Date();
  disableDate = true;
  public loading = false;

  @Input() selectedProcess;
  @Input() selectedHierarchy;
  @Input()
  set openDocumentGeneration(openDocumentGeneration) {
  this._openDocumentGeneration = openDocumentGeneration;
  }

  @Output() public closeOpenDocumentGeneration = new EventEmitter<any>();
  constructor(
    public processManagementService: ProcessManagementService,
    public messageService: MessageService,
    @Inject(DOCUMENT) private document: Document
  ) {
  }

  ngOnInit() {

  }

  close() {
    this.closeOpenDocumentGeneration.emit();
    this.loading = false;
  }

  submitClicked() {
    this.loading = true;
    if (this.selectedProcess === null && this.latestCheck === 'checked') {
      this.GenerateHierarchyDocs(this.selectedHierarchy.id, this.document.location.href);
    } else if (this.selectedProcess === null && this.dateCheck === 'checked') {
      this.GenerateHierarchyDocsByDate(this.selectedHierarchy.id, this.value.toLocaleDateString('en-US'), this.document.location.href);
    } else if (this.selectedProcess !== null && this.latestCheck === 'checked') {
      this.GenerateRequirementDocs(this.selectedProcess.id, this.document.location.href);
    } else if (this.selectedProcess !== null && this.dateCheck === 'checked') {
      this.GenerateRequirementDocsByDate(this.selectedProcess.id, this.value.toLocaleDateString('en-US'), this.document.location.href);
    }
  }

  radioClicked() {
    if (this.latestCheck === 'checked') {
      this.latestCheck = '';
      this.dateCheck = 'checked';
      this.disableDate = false;
    } else {
      this.latestCheck = 'checked';
      this.dateCheck = '';
      this.disableDate = true;
    }
  }

  GenerateHierarchyDocsByDate(id: string, date: string, uiURL: string) {
    this.processManagementService.GenerateHierarchyDocsByDate(id, date, uiURL).subscribe(
      (x: any) => {
        this.loading = false;
        this.close();
        const newBlob = new Blob([x], { type: 'application/vxml' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = 'HierarchyDocumentationByDate.zip';
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      },
      error => {
        this.messageService.sendMessage({ text: 'There was an error during the download of the file!', type: MessageType.Error });
      }
    );
  }

  GenerateHierarchyDocs(id: string, uiURL: string) {
    this.processManagementService.GenerateHierarchyDocs(id, uiURL).subscribe(
      (x: any) => {
        this.loading = false;
        this.close();
        const newBlob = new Blob([x], { type: 'application/vxml' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = 'HierarchyDocumentation.zip';
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      },
      error => {
        this.messageService.sendMessage({ text: 'There was an error during the download of the file!', type: MessageType.Error });
      }
    );
  }

  GenerateRequirementDocsByDate(id: string, date: string, uiURL: string) {
    this.processManagementService.GenerateRequirementDocsByDate(id, date, uiURL).subscribe(
      (x: any) => {
        this.loading = false;
        const newBlob = new Blob([x], { type: 'application/vxml' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = 'RequirementDocumentationByDate.zip';
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      },
      error => {
        this.messageService.sendMessage({ text: 'There was an error during the download of the file!', type: MessageType.Error });
      }
    );
  }

  GenerateRequirementDocs(id: string, uiURL: string) {
    this.processManagementService.GenerateRequirementDocs(id, uiURL).subscribe(
      (x: any) => {
        this.loading = false;
        const newBlob = new Blob([x], { type: 'application/vxml' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = 'RequirementDocumentation.zip';
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      },
      error => {
        this.messageService.sendMessage({ text: 'There was an error during the download of the file!', type: MessageType.Error });
      }
    );
  }
}
