import {Component, OnInit, Input} from '@angular/core';
import {ProcessManagementService} from '../../../../services/process-management.service';
import { forkJoin, Observable } from 'rxjs';
import { Guid } from 'src/app/shared/guid';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-process-statistics',
  templateUrl: './process-statistics.component.html',
  styleUrls: ['./process-statistics.component.css']
})
export class ProcessStatisticsComponent implements OnInit {
    public processStatistics = {
        nrWorkflows: 0,
        nrTotalIssues: 0,
        nrOpenIssues: 0,
        requirementType: "",
        priority: ""
    };
    public showStatistics = false;

    @Input()
    set selectedProcess(selectedProcess) {
       this.updateProcessStatistics(selectedProcess);
    }

    constructor(
        private processManagementService: ProcessManagementService,
        public permissionService: PermissionService
    ) {}

    ngOnInit() {
    }

    async updateProcessStatistics(selectedProcess) {
        this.showStatistics = false;
        if (selectedProcess) {
            let requirementPriority = (selectedProcess.requirementPriorityId !== Guid.empty && this.permissionService.hasPermission('/api/Process/Configure/GetRequirementPriority')) ? 
                await this.processManagementService.getRequirementPriority(selectedProcess.requirementPriorityId).toPromise() : null;
            let requirementType = (selectedProcess.requirementTypeId !== Guid.empty && this.permissionService.hasPermission('/api/Process/Configure/GetRequirementType')) ? 
                await this.processManagementService.getRequirementType(selectedProcess.requirementTypeId).toPromise() : null;
            let processStatistics = this.permissionService.hasPermission('/api/Process/Browse/GetProcessStatistics') ? 
                await this.processManagementService.getProcessStatistics(selectedProcess.id).toPromise() : {nrWorkflows: 0, nrTotalIssues: 0, nrOpenIssues: 0};
            this.processStatistics = {...processStatistics, requirementType: requirementType ? requirementType.title : "", priority: requirementPriority ? requirementPriority.title : ""};
            this.showStatistics = true;
        } else {
            this.processStatistics = {
                nrWorkflows: 0,
                nrTotalIssues: 0,
                nrOpenIssues: 0,
                requirementType: "",
                priority: ""
                
            };
            this.showStatistics = false;
        }
    }
}
