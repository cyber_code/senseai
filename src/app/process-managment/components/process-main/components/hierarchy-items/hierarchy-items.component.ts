import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ProcessManagementService } from '../../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Guid } from 'src/app/shared/guid';
import { headerCellInfo } from '@syncfusion/ej2-grids';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-hierarchy-items',
  templateUrl: './hierarchy-items.component.html',
  styleUrls: ['./hierarchy-items.component.css']
})
export class HierarchyItemsComponent implements OnInit {
  public activateClick;
  public actionClicked;
  public allHierarchies = [];
  public hierarchies = [];
  public selectedHierarchy;
  public openAddHierarchy;
  public hierarchyToAdd;
  public deleteHierarchyWindowOpen;
  public hierarchyToDelete;
  public lastHierarchyIndex = 0;
  public openMoveCopyHierarchy = false;
  public hierarchyToMoveCopy;
  public hierarchyTypeTitle = '';

  @Input() hierarchyTypes;

  @Output() listsReseted = new EventEmitter();

  @Input()
  set listOfHierarchies(listOfHierarchies) {
    this.hierarchies = listOfHierarchies.sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
  }

  @Input()
  set resetHierarchyProcessList(resetHierarchyProcessList) {
    if (resetHierarchyProcessList) {
      this.hierarchies = this.allHierarchies.filter(
        hrc => hrc.index === this.lastHierarchyIndex || (this.lastHierarchyIndex === 0 && !hrc.index)
      );
    }
  }

  @Input() hideIcons;

  @Output() hierarchyClicked = new EventEmitter();

  constructor(private processManagementService: ProcessManagementService, private sessionService: SessionService, public permissionService: PermissionService) {
    this.processManagementService.hierarchyAdded.subscribe(hierarchy => {
      this.addHierarchy(hierarchy);
    });
  }

  async ngOnInit() {
    let subProjectId = this.sessionService.getSubProject().id;
    if (this.permissionService.hasPermission('/api/Process/Browse/GetHierarchiesByParent')) {
      this.processManagementService.getHierarchiesByParent(subProjectId, Guid.empty, Guid.empty).subscribe(hierarchies => {
        if (hierarchies) {
          this.hierarchies = hierarchies.sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
          this.allHierarchies = hierarchies.sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
          if (hierarchies.length > 0) {
            this.onHierarchyClick(hierarchies[0]);
          }
        }
      });
    }
  }

  onHierarchyClick(item) {
    this.activateClick = true;
    setTimeout(() => {
      this.hierarchyClick(item);
    }, 300);
  }

  hierarchyClick(item) {
    if (this.actionClicked) {
      this.actionClicked = false;
      return;
    }

    if (this.activateClick && (!this.selectedHierarchy || !item || item.id !== this.selectedHierarchy.id)) {
      this.selectedHierarchy = item;
      this.hierarchyClicked.emit(item);
    } else if (this.activateClick && this.selectedHierarchy && item.id === this.selectedHierarchy.id) {
      this.selectedHierarchy = null;
      this.hierarchyClicked.emit(null);
    }
  }

  onHierarchyDblClick(item) {
    this.activateClick = false;
    let subProjectId = this.sessionService.getSubProject().id;
    this.processManagementService.getHierarchiesByParent(subProjectId, item.hierarchyTypeId, item.id).subscribe(subHierarchies => {
      if (subHierarchies && subHierarchies.length > 0) {
        this.lastHierarchyIndex += 1;
        this.hierarchies = subHierarchies.map(hrc => ({ ...hrc, index: this.lastHierarchyIndex }))
        .sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
        this.allHierarchies = this.allHierarchies.concat(subHierarchies.map(hrc => ({ ...hrc, index: this.lastHierarchyIndex })))
        .sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
        if (this.hierarchies.length > 0) {
          this.selectedHierarchy = this.hierarchies[0];
          this.hierarchyClicked.emit(this.hierarchies[0]);
        }
      }
    });
  }

  editHierarchy(hierarchy) {
    this.actionClicked = true;
    hierarchy.showActions = false;
    let hierarchyType = this.hierarchyTypes.find(hierarchyType => hierarchyType.id === hierarchy.hierarchyTypeId);
    this.hierarchyTypeTitle = hierarchyType ? hierarchyType.title : '';
    this.openAddHierarchy = true;
    this.hierarchyToAdd = Object.assign({}, hierarchy);
  }

  closeHierarchyDialog() {
    this.openAddHierarchy = false;
  }

  showActions(hierarchy) {
    this.hierarchies = this.hierarchies.map(item => {
      if (item.id === hierarchy.id) {
        return {...item, showActions: item.showActions ? false : true};
      }
      return {...item, showActions: false};
    });
  }

  deleteHierarchy(hierarchy) {
    this.actionClicked = true;
    hierarchy.showActions = false;
    this.deleteHierarchyWindowOpen = true;
    let hierarchyType = this.hierarchyTypes.find(hierarchyType => hierarchyType.id === hierarchy.hierarchyTypeId);
    this.hierarchyTypeTitle = hierarchyType ? hierarchyType.title : '';
    this.hierarchyToDelete = hierarchy;
  }

  moveCopyHierarchy(hierarchy) {
    this.actionClicked = true;
    hierarchy.showActions = false;
    this.openMoveCopyHierarchy = true;
    this.hierarchyToMoveCopy = hierarchy;
  }

  updateHierarchyList(hierarchy) {
    let hierarchies = [];
    let allHierarchies = [];

    if (this.hierarchies.find(hrc => hrc.id === hierarchy.id && hrc.parentId === hierarchy.parentId)) {
      hierarchies = this.hierarchies.map(hrc => {
        if (hrc.id === hierarchy.id) {
          return hierarchy;
        }
        return hrc;
      });

      allHierarchies = this.allHierarchies.map(hrc => {
        if (hrc.id === hierarchy.id) {
          return hierarchy;
        }
        return hrc;
      });
    } else {
      hierarchies = this.hierarchies.filter(hrc => hrc.id !== hierarchy.id);
      allHierarchies = this.allHierarchies.filter(hrc => hrc.id !== hierarchy.id);
    }

    this.hierarchies = hierarchies.sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
    this.allHierarchies = allHierarchies.sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
    this.closeHierarchyDialog();
  }

  addHierarchy(hierarchy) {
    if (this.hierarchies.length === 0 || this.hierarchies.find(hrc => hrc.parentId === hierarchy.parentId)) {
      this.hierarchies = this.hierarchies.concat(hierarchy)
        .sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
      this.allHierarchies = this.allHierarchies.concat({ ...hierarchy, index: this.hierarchies[0] ? this.hierarchies[0].index : null })
        .sort((h1, h2) => (h1.title.toLowerCase() > h2.title.toLowerCase()) ? 1 : -1);
    }
  }

  closeDeleteHierarchyWindow() {
    this.deleteHierarchyWindowOpen = false;
  }

  removeHierarchyFromList(hierarchy) {
    this.hierarchies = this.hierarchies.filter(hrc => hrc.id !== hierarchy.id);
    this.allHierarchies = this.allHierarchies.filter(hrc => hrc.id !== hierarchy.id);
    if (this.selectedHierarchy && this.selectedHierarchy.id === hierarchy.id) {
      this.onHierarchyClick(null);
    }
    this.closeDeleteHierarchyWindow();
  }

  closeMoveCopyHierarchy() {
    this.openMoveCopyHierarchy = false;
  }

  moveCopyHierarchyPermissions() {
    return (
      this.permissionService.hasPermission('/api/Process/Browse/AddHierarchy') &&
      this.permissionService.hasPermission('/api/Process/Browse/UpdateHierarchy') &&
      this.permissionService.hasPermission('/api/Process/Browse/GetHierarchy')
    );
  }

  showHierarchyActions() {
    return (
      this.moveCopyHierarchyPermissions ||
      this.permissionService.hasPermission('/api/Process/Browse/UpdateHierarchy') ||
      this.permissionService.hasPermission('/api/Process/Browse/DeleteHierarchy')
    );
  }
}
