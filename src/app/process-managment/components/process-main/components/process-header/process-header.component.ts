import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Guid } from 'src/app/shared/guid';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-process-header',
  templateUrl: './process-header.component.html',
  styleUrls: ['./process-header.component.css']
})
export class ProcessHeaderComponent implements OnInit {

    public header = '';
    public _hierarchyTypes = [];
    @Input() selectedHierarchy;
    @Input() searchCriteria;
    @Input() navigation;

    @Output() onClickNavigate = new EventEmitter();
    @Output() goBackClicked = new EventEmitter();
    @Output() hierarchyChanged = new EventEmitter();
    @Input() 
    set hierarchyTypes(hierarchyTypes) {
        this._hierarchyTypes = hierarchyTypes;
        if (this.header === '') {
            let parentHierarchyType = hierarchyTypes.find(hierarchyType => hierarchyType.parentId === '' || hierarchyType.parentId === Guid.empty);
            if (parentHierarchyType) {
                this.header = parentHierarchyType.title;
            }
        }
    };

    @Input() 
    set hierarchies(hierarchies) {
        if (hierarchies && hierarchies.length > 0 && this._hierarchyTypes && this.searchCriteria === "") {
            let hierarchyTypeId = hierarchies[0].hierarchyTypeId;
            let hierarchyType = this._hierarchyTypes.find(hrcType => hrcType.id === hierarchyTypeId);
            if (hierarchyType) {
                this.header = hierarchyType.title;
            }
        }
    }

    constructor(public permissionService: PermissionService) {}

    ngOnInit() {
    }

    navigateClicked(nav) {
        this.onClickNavigate.emit(nav);
    }

    goBack() {
        this.goBackClicked.emit();
    }

    onChangeHierarchies(event) {
        this.hierarchyChanged.emit(event);
    }
}
