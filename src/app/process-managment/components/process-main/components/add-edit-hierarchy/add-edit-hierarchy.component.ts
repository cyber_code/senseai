import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { SessionService } from 'src/app/core/services/session.service';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { FileInfo } from '@progress/kendo-angular-upload';
import { Guid } from 'src/app/shared/guid';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-add-edit-hierarchy',
  templateUrl: 'add-edit-hierarchy.component.html',
  styleUrls: ['add-edit-hierarchy.component.css']
})
export class AddEditHierarchyComponent implements OnInit {
  public _openAddHierarchy;
  public hierarchies = [];

  @Input() isHierarchyEdited;
  @Input() selectedHierarchy;
  @Input() hierarchyToAdd;
  @Input() hierarchyTypeTitle;

  @Input()
  set openAddHierarchy(openAddHierarchy) {
    if (openAddHierarchy) {
      if (!this.selectedHierarchy || this.selectedHierarchy.parentId === Guid.empty) {
        this.getHierarchies({ id: '', hierarchyTypeId: '', parentId: '', firstLevel: true });
      } else {
        this.processManagementService.getHierarchy(this.selectedHierarchy.parentId).subscribe(hierarchy => {
          if (hierarchy) {
            this.getHierarchies(hierarchy);
          }
        });
      }
    }

    this._openAddHierarchy = openAddHierarchy;
  }

  @Output() public closeAddHierarchyWindow = new EventEmitter<any>();
  @Output() public hierarchyListUpdated = new EventEmitter<any>();

  constructor(
    private processManagementService: ProcessManagementService,
    private sessionService: SessionService,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  close() {
    this.closeAddHierarchyWindow.emit();
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.hierarchyToAdd.title = title;
  }

  changeDescription(event) {
    let description = event.target.value.trim('');
    this.hierarchyToAdd.description = description;
  }

  disableSubmitBtn() {
    return this.hierarchyToAdd.title === '';
  }

  submitClicked() {
    if (this.isHierarchyEdited) {
      this.editHierarchy();
    } else {
      this.addHierarchy();
    }
  }

  addHierarchy() {
    let subProjectId = this.sessionService.getSubProject().id;
    this.processManagementService
      .addHierarchy(
        this.hierarchyToAdd.parentId === '' ? Guid.empty : this.hierarchyToAdd.parentId,
        subProjectId,
        this.hierarchyToAdd.hierarchyTypeId === '' ? Guid.empty : this.hierarchyToAdd.hierarchyTypeId,
        this.hierarchyToAdd.title,
        this.hierarchyToAdd.description ? this.hierarchyToAdd.description : ''
      )
      .subscribe(res => {
        this.handleHierarchyResponse(res);
      });
  }

  editHierarchy() {
    let subProjectId = this.sessionService.getSubProject().id;

    this.processManagementService
      .updateHierarchy(
        this.hierarchyToAdd.id,
        this.hierarchyToAdd.parentId === '' ? Guid.empty : this.hierarchyToAdd.parentId,
        subProjectId,
        this.hierarchyToAdd.hierarchyTypeId === '' ? Guid.empty : this.hierarchyToAdd.hierarchyTypeId,
        this.hierarchyToAdd.title,
        this.hierarchyToAdd.description ? this.hierarchyToAdd.description : ''
      )
      .subscribe(res => {
        this.handleHierarchyResponse(res);
      });
  }

  async handleHierarchyResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isHierarchyEdited
          ? this.hierarchyTypeTitle + ' was updated successfully!'
          : this.hierarchyTypeTitle + ' was added successfully',
        type: MessageType.Success
      });

      let hierarchy;

      if (this.isHierarchyEdited) {
        hierarchy = this.hierarchyToAdd;
      } else {
        hierarchy = await this.processManagementService.getHierarchy(res.id).toPromise();
      }
      this.hierarchyListUpdated.emit(hierarchy);
    } else {
      this.close();
    }
  }

  onChangeHierarchy(hierarchy) {
    this.hierarchyToAdd.parent = hierarchy ? hierarchy : {};
    this.hierarchyToAdd.parentId = hierarchy ? hierarchy.id : '';
    this.hierarchyToAdd.hierarchyTypeId = hierarchy ? hierarchy.hierarchyTypeId : '';
  }

  async getHierarchies(hierarchy) {
    if ((!this.isHierarchyEdited && !this.selectedHierarchy) || (this.isHierarchyEdited && hierarchy.firstLevel)) {
      this.hierarchyToAdd.firstLevel = true;
      return;
    }

    let subProjectId = this.sessionService.getSubProject().id;

    hierarchy.parentId = isNullOrUndefined(hierarchy.parentId) ? '' : hierarchy.parentId;
    hierarchy.hierarchyTypeId = isNullOrUndefined(hierarchy.hierarchyTypeId) ? '' : hierarchy.hierarchyTypeId;
    let processHierarchy = hierarchy;

    if (this.isHierarchyEdited && hierarchy.parentId !== '' && hierarchy.parentId !== Guid.empty) {
      processHierarchy = await this.processManagementService.getHierarchy(hierarchy.parentId).toPromise();
    }

    let hierarchyTypeId =
      this.isHierarchyEdited && (hierarchy.parentId === '' || hierarchy.parentId === Guid.empty) ? '' : processHierarchy.hierarchyTypeId;
    let parentId = this.isHierarchyEdited && (hierarchy.parentId === '' || hierarchy.parentId === Guid.empty) ? '' : processHierarchy.id;

    this.processManagementService.getHierarchiesByParent(subProjectId, hierarchyTypeId, parentId).subscribe(hierarchies => {
      if (hierarchies) {
        if (!this.isHierarchyEdited) {
          this.hierarchies = [...hierarchies];
          this.hierarchyToAdd.parent = this.selectedHierarchy;
          this.hierarchyToAdd.parentId = this.selectedHierarchy.id;
          this.hierarchyToAdd.hierarchyTypeId = this.selectedHierarchy.hierarchyTypeId;
        } else {
          this.hierarchies = [...hierarchies];
          this.hierarchyToAdd.parent = hierarchy;
        }
      }
    });
  }
}
