import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { SessionService } from 'src/app/core/services/session.service';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { DesignService } from 'src/app/design/services/design.service';
import { FileInfo } from '@progress/kendo-angular-upload';
import { Guid } from 'src/app/shared/guid';
import {ProcessType} from '../../../../models/process-type';
import { Helper } from 'src/app/shared/helper';
import { ProcessResourcesService } from 'src/app/process-managment/services/process-resources.service';
import * as _ from 'lodash';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-add-edit-process',
  templateUrl: 'add-edit-process.component.html',
  styleUrls: ['add-edit-process.component.css']
})
export class AddEditProcessComponent implements OnInit {
  public _openAddProcess;
  public hierarchies = [];
  public people = [];
  public priority: any[] = [];
  public requirementType: any[] = [];
  public typicals: any[] = [];
  public filteredtypicals: any[] = [];
  public resources = [];
  public addedResources = [];
  public deletedResources = [];
  public originalResources = [];

  @Input() isProcessEdited;
  @Input() selectedProcess;
  @Input() selectedHierarchy;

  @Input()
  set openAddProcess(openAddProcess) {
    if (openAddProcess) {
      if (!this.selectedHierarchy || this.selectedHierarchy.parentId === Guid.empty) {
        this.getHierarchies('', '');
      } else {
        this.processManagementService.getHierarchy(this.selectedHierarchy.parentId).subscribe(hierarchy => {
          if (hierarchy) {
            this.getHierarchies(hierarchy.hierarchyTypeId, hierarchy.id);
          }
        });
      }
      let subProjectId = this.sessionService.getSubProject().id;
      if (this.permissionService.hasPermission('/api/People/GetPeoples')) {
        this.processManagementService.getPeoples(subProjectId).subscribe(people => {
          if (people) {
            this.people = people;
            if (this.isProcessEdited) {
              this.selectedProcess.owner = people.find(person => person.id === this.selectedProcess.ownerId);
            }
          }
        });
      } else {
        this.people = [];
      }
      
      if (!this.isProcessEdited) {
      } else {
        this.selectedProcess.priorityObj = this.priority.find(p => p.id === this.selectedProcess.requirementPriorityId);
        this.selectedProcess.requirementTypeObj = this.requirementType.find(p => p.id === this.selectedProcess.requirementTypeId);
        if (this.selectedProcess.typicalId && this.selectedProcess.typicalId !== Guid.empty && this.permissionService.hasPermission('/api/Data/GetTypical')) {
          
          this.processManagementService.getTypical(this.selectedProcess.typicalId).subscribe((res: any) => {
            if (res) {
              this.selectedProcess.typicalObj = res;
              this.typicals = this.typicals.filter(row => row.id !== res.id).concat(res);
              this.filteredtypicals = this.filteredtypicals.filter(row => row.id !== res.id).concat(res);
            }
          });
        } else {
          this.selectedProcess.typicalObj = {};
        }
      
      }

      if (this.isProcessEdited && this.permissionService.hasPermission('/api/Process/Browse/GetProcessResources')) {
        this.processManagementService.getProcessResources(this.selectedProcess.id).subscribe((res: any) => {
          if (res && res.length) {
            this.resources = res;
            this.originalResources = _.cloneDeep(res);
          }
        });
      }
    }
    this.selectedProcess.hierarchy = this.selectedHierarchy;
    this.selectedProcess.hierarchyId = this.selectedHierarchy ? this.selectedHierarchy.id : null;
    this._openAddProcess = openAddProcess;
    this.filteredtypicals = this.typicals;
  }

  @Output() public closeAddProcessWindow = new EventEmitter<any>();
  @Output() public processListUpdated = new EventEmitter<boolean>();

  constructor(
    private processManagementService: ProcessManagementService,
    private sessionService: SessionService,
    private messageService: MessageService,
    private processResourcesService: ProcessResourcesService,
    private designService: DesignService,
    public permissionService: PermissionService
  ) {}

  ngOnInit() {
    const subProjectId = this.sessionService.getSubProject().id;
    if (this.permissionService.hasPermission('/api/Process/Configure/GetRequirementTypes')) {
      this.processManagementService.getRequirementTypes(subProjectId).subscribe(res => {
        this.requirementType = res;
      });
    }
    
    if (this.permissionService.hasPermission('/api/Process/Configure/GetRequirementPriorities')) {
      this.processManagementService.getRequirementPriorities(subProjectId).subscribe(res => {
        this.priority = res;
      });
    }
    
    this.getTypicals('');
  }

    getTypicals(value){
      let catalog = this.sessionService.getDefaultSettings().catalog;
    if (catalog && catalog.id && this.permissionService.hasPermission('/api/Design/SearchTypicals')) {
      return  this.designService.searchTypicals(value, catalog.id).subscribe((res: any[])=>{
        if (value === '')
          this.typicals = res;
        this.filteredtypicals = res;
      });
    }
  }

  handleFilter(value) {
    return this.getTypicals(value);
  }

  close() {
    this.closeAddProcessWindow.emit();
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedProcess.title = title;
  }

  onDeleteClicked(id) {
    this.resources = this.resources.filter(file => file.id !== id);
    this.addedResources = this.addedResources.filter(file => file.id !== id);
    if (this.isProcessEdited && this.originalResources.find(resource => resource.id === id)) {
      this.deletedResources = this.deletedResources.concat(id);
    }
  }

  onFileSelected(event) {
    let fileList = event.target.files;
    Object.keys(fileList).forEach(key => {
      fileList[key].fileName = fileList[key].name;
      fileList[key].id = Guid.newGuid();
      this.resources.push(fileList[key]);
      this.addedResources.push(fileList[key]);
    });
    event.target.value = '';
  }

  changeDescription(event) {
    let description = event.target.value.trim('');
    this.selectedProcess.description = description;
  }

  disableSubmitBtn() {
    return this.selectedProcess.title === '' || !this.selectedProcess.hierarchyId;
  }

  submitClicked() {
    if (this.isProcessEdited) {
      this.editProcess();
    } else {
      this.addProcess();
    }
  }

  addProcess() {
    let subProjectId = this.sessionService.getSubProject().id;
    if (!this.selectedProcess.requirementPriorityId) {
      this.selectedProcess.requirementPriorityId = Guid.empty;
    }

    if (!this.selectedProcess.requirementTypeId) {
      this.selectedProcess.requirementTypeId = Guid.empty;
    }

    this.processManagementService
      .addProcess(
        this.selectedProcess.title,
        this.selectedProcess.description ? this.selectedProcess.description : '',
        this.selectedProcess.expectedNumberOfTestCases ? this.selectedProcess.expectedNumberOfTestCases : 0,
        this.selectedProcess.hierarchyId,
        subProjectId,
        '90c39e80-9ccb-4dbf-bfd7-396b2201e01c',
        this.selectedProcess.ownerId,
        ProcessType.Aris,
        this.selectedProcess.requirementPriorityId,
        this.selectedProcess.requirementTypeId,
        this.selectedProcess.typicalId ? this.selectedProcess.typicalId : '00000000-0000-0000-0000-000000000000'
      )
      .subscribe(processRes => {
        if (this.resources && this.resources.length > 0 && this.permissionService.hasPermission('/api/Process/Browse/AddProcessResources')) {
          this.processResourcesService.addProcessResources(processRes.id, this.resources).subscribe(resourceRes => {
            this.handleProcessResponse(processRes, resourceRes);
          });
        } else {
          this.handleProcessResponse(processRes);
        }
      });
  }

  editProcess() {
    let subProjectId = this.sessionService.getSubProject().id;

    if (!this.selectedProcess.requirementPriorityId) {
      this.selectedProcess.requirementPriorityId = Guid.empty;
    }

    if (!this.selectedProcess.requirementTypeId) {
      this.selectedProcess.requirementTypeId = Guid.empty;
    }

    this.processManagementService
      .updateProcess(
        this.selectedProcess.id,
        this.selectedProcess.title,
        this.selectedProcess.description ? this.selectedProcess.description : '',
        this.selectedProcess.expectedNumberOfTestCases ? this.selectedProcess.expectedNumberOfTestCases : 0,
        this.selectedProcess.hierarchyId,
        subProjectId,
        '90c39e80-9ccb-4dbf-bfd7-396b2201e01c',
        this.selectedProcess.ownerId,
        ProcessType.Aris,
        this.selectedProcess.requirementPriorityId ? this.selectedProcess.requirementPriorityId : Guid.empty,
        this.selectedProcess.requirementTypeId ? this.selectedProcess.requirementTypeId : Guid.empty,
        this.selectedProcess.typicalId ? this.selectedProcess.typicalId : '00000000-0000-0000-0000-000000000000'
      )
      .subscribe(res => {
        if (this.addedResources && this.addedResources.length > 0 && this.permissionService.hasPermission('/api/Process/Browse/AddProcessResources')) {
          this.processResourcesService.addProcessResources(this.selectedProcess.id, this.addedResources).subscribe(() => {
            if (this.deletedResources && this.deletedResources.length > 0 && this.permissionService.hasPermission('/api/Process/Browse/DeleteProcessResources')) {
              for (let i = 0; i < this.deletedResources.length; i++) {
                this.processManagementService.deleteProcessResources(this.deletedResources[i]).subscribe(() => {
                  if (i === this.deletedResources.length - 1) {
                    this.handleProcessResponse(res);
                  }
                });
              }
            } else {
              this.handleProcessResponse(res);
            }
          });
        } else if (this.deletedResources && this.deletedResources.length > 0 && this.permissionService.hasPermission('/api/Process/Browse/DeleteProcessResources')) {
          for (let i = 0; i < this.deletedResources.length; i++) {
            this.processManagementService.deleteProcessResources(this.deletedResources[i]).subscribe(() => {
              if (i === this.deletedResources.length - 1) {
                this.handleProcessResponse(res);
              }
            });
          }
        } else {
          this.handleProcessResponse(res);
        }
      });
  }

  handleProcessResponse(processRes, resourceRes = true) {
    if (processRes && resourceRes) {
      this.messageService.sendMessage({
        text: this.isProcessEdited ? 'Requirement was updated successfully!' : 'Requirement was added successfully',
        type: MessageType.Success
      });
      this.processListUpdated.emit(this.isProcessEdited ? 
        {...this.selectedProcess, processType: ProcessType.Aris} : 
        { ...this.selectedProcess, id: processRes.id, processType: ProcessType.Aris});
    } else {
      this.close();
    }

    this.resources = [];
    this.addedResources = [];
    this.deletedResources = [];
    this.originalResources = [];
  }

  onChangeHierarchy(hierarchy) {
    this.selectedProcess.hierarchy = hierarchy;
    this.selectedProcess.hierarchyId = hierarchy ? hierarchy.id : null;
  }

  onChangePeople(person) {
    this.selectedProcess.owner = person;
    this.selectedProcess.ownerId = person ? person.id : null;
  }

  onChangePriority(priority) {
    this.selectedProcess.priorityObj = priority;
    this.selectedProcess.requirementPriorityId = priority ? priority.id : null;
  }

  onChangeRequirementType(requirementType) {
    this.selectedProcess.requirementTypeObj = requirementType;
    this.selectedProcess.requirementTypeId = requirementType ? requirementType.id : null;
  }

  onChangeTypical(typical) {
    this.selectedProcess.typicalObj = typical;
    this.selectedProcess.typicalId = typical ? typical.id : null;
  }

  onChangeExpectedNumberOfTestCases(event) {
    let numberOfTestCases = event.target.value.trim('');
    this.selectedProcess.expectedNumberOfTestCases = Number(numberOfTestCases);
  }

  arisTypeSelected() {
    this.selectedProcess.processType = ProcessType.Aris;
  }

  genericTypeSelected() {
    this.selectedProcess.processType = ProcessType.Generic;
  }

  getHierarchies(hierarchyTypeId, parentId) {
    let subProjectId = this.sessionService.getSubProject().id;
    this.processManagementService.getHierarchiesByParent(subProjectId, hierarchyTypeId, parentId).subscribe(hierarchies => {
      if (hierarchies) {
        this.hierarchies = hierarchies;
      }
    });
  }
}
