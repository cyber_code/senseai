import {Component, OnInit, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {ProcessManagementService} from '../../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Guid } from 'src/app/shared/guid';
import { HierarchyItemsComponent } from '../../../process-main/components/hierarchy-items/hierarchy-items.component';
import { isNullOrUndefined } from 'util';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-requirement-dialog',
  templateUrl: './requirement-dialog.component.html',
  styleUrls: ['./requirement-dialog.component.css']
})
export class RequirementDialogComponent implements OnInit {

    public selectedHierarchy;
    public selectedProcess;
    public searchCriteria = '';
    public listOfHierarchies = [];
    public listOfProcesses = [];
    public resetHierarchyProcessList = false;
    public processRemoved;
    public processAdded;
    public subProjectId:string;
    public navigation = [];
    public processDetail;
    public openProcessDetailsWindow = false;
    public hierarchyTypes = [];
    public isLastLevelHierarchy = false;
    public openProcessDetails = false;

    @ViewChild(HierarchyItemsComponent)
    public hierarchyItemsComponent: HierarchyItemsComponent;

    @Input() openRequirementDialog;
    @Input() shouldSelectHierarchyItem;

    @Output() public closeRequirement = new EventEmitter<any>();
    @Output() public submitRequirement = new EventEmitter<any>();
    @Output() public submitHierarchyItem = new EventEmitter<any>();

    constructor(
        private processManagementService: ProcessManagementService,
        private sessionService: SessionService,
        private permissionService: PermissionService
    ) {}

    ngOnInit() {
      const subProjectId = this.sessionService.getSubProject().id;
      this.subProjectId = subProjectId;
      if (this.permissionService.hasPermission("/api/Process/Configure/GetHierarchyTypes"))
        this.processManagementService.getHierarchyTypes(subProjectId).subscribe(hierarchyTypes => {
            if (hierarchyTypes) {
                this.hierarchyTypes = hierarchyTypes;
            }
        });
    }

    hierarchyClicked(hierarchy) {
        this.selectedHierarchy = hierarchy;
        if (hierarchy) {
            const navigation = [hierarchy];
            this.navigation = this.getNavigationRecursively(navigation);
        } else {
            const currentHierarchyLevel = this.hierarchyItemsComponent.hierarchies[0].index;
            if (isNullOrUndefined(currentHierarchyLevel)) {
                this.navigation = [];
            } else {
                const navigation = this.navigation.filter(nav => !nav.index || nav.index < currentHierarchyLevel);
                this.navigation = navigation;
                const parentEl = navigation[navigation.length - 1];
                if (parentEl) {
                    this.hierarchyItemsComponent.onHierarchyClick(parentEl);
                } 
                
            }
        }
        
    }

    getNavigationRecursively(navigation) {
        if (this.hierarchyItemsComponent && this.hierarchyItemsComponent.allHierarchies){
            const parent = this.hierarchyItemsComponent.allHierarchies.find(hierarchy => hierarchy.id === navigation[0].parentId);
            if (isNullOrUndefined(parent)) {
              return navigation;
            } else {
              navigation.unshift(parent);
              return this.getNavigationRecursively(navigation);
            }
          } else {
            return [];
          }
    }

    processClicked(process) {
        this.selectedProcess = process;

    }

    navigateClicked(hierarchy) {
        if (hierarchy.id !== this.navigation[this.navigation.length - 1].id) {
            this.hierarchyItemsComponent.lastHierarchyIndex = hierarchy.index ? hierarchy.index : 0;
            const filterIndex = this.hierarchyItemsComponent.lastHierarchyIndex;
            this.hierarchyItemsComponent.hierarchies = this.hierarchyItemsComponent.allHierarchies
                .filter(hrc => hrc.index === filterIndex ||  (filterIndex === 0 && !hrc.index));
            this.hierarchyItemsComponent.allHierarchies = this.hierarchyItemsComponent.allHierarchies
                .filter(hrc => hrc.index <= filterIndex || !hrc.index);
            this.navigation = this.navigation.filter(nav => !nav.index || nav.index <= hierarchy.index);
            this.hierarchyItemsComponent.onHierarchyClick(hierarchy);
        }
        
    }

    goBack() {
        if (this.hierarchyItemsComponent.lastHierarchyIndex > 0) {
            this.hierarchyItemsComponent.lastHierarchyIndex -= 1;
            const filterIndex = this.hierarchyItemsComponent.lastHierarchyIndex;
            this.hierarchyItemsComponent.hierarchies = this.hierarchyItemsComponent.allHierarchies
                .filter(hrc => hrc.index === filterIndex 
                    || (filterIndex === 0 && !hrc.index)
                );
            this.hierarchyItemsComponent.allHierarchies = this.hierarchyItemsComponent.allHierarchies
            .filter(hrc => hrc.index !== (filterIndex + 1));
            if (!this.selectedHierarchy || !this.hierarchyItemsComponent.hierarchies.find(hrc => hrc.id === this.selectedHierarchy.id)) {
                const currentHierarchyLevel = this.hierarchyItemsComponent.hierarchies[0].index;
                const navigation = this.navigation.filter(nav => !nav.index || nav.index <= currentHierarchyLevel);
                this.navigation = navigation;
                this.hierarchyClicked(navigation[navigation.length - 1]);
                this.hierarchyItemsComponent.onHierarchyClick(navigation[navigation.length - 1]);
            }
        }
    }

    onChangeHierarchies(event) {
        this.resetHierarchyProcessList = false;
        const searchCriteria = event.target.value.trim('');
        this.searchCriteria = searchCriteria;

        const parentId = this.selectedHierarchy ? this.selectedHierarchy.id : Guid.empty;
        if (searchCriteria !== '') {
            this.processManagementService.searchHierarchies(this.subProjectId, parentId, searchCriteria).subscribe((res: any) => {
                this.listOfHierarchies = res.listOfHierarchies;
                this.listOfProcesses = res.listOfProcesses;
            });
        } else {
            this.resetHierarchyProcessList = true;
        }
    }

    setRemovedProcess(process) {
        this.processRemoved = process;
    }

    setAddedProcess(process) {
        this.processAdded = process;
    }

    setLastLevelHierarchy(hierarchyTypeTitle) {
        this.isLastLevelHierarchy = (isNullOrUndefined(hierarchyTypeTitle) || hierarchyTypeTitle === '');
    }

    processDetailsClicked() {
        this.openProcessDetails = true;
    }

    processUpdated(process) {
        if (this.selectedProcess && this.selectedProcess.id === process.id) {
            this.selectedProcess = Object.assign({}, process);
        }
    }

    cancelClicked() {
        this.closeRequirement.emit();
    }

    submitClicked() {
        if (!this.shouldSelectHierarchyItem) {
            this.submitRequirement.emit({
                process: this.selectedProcess,
                component: this.navigation[0]
            });
        } else {
            this.submitHierarchyItem.emit(this.selectedHierarchy);
        }
    }

    disableSubmitButton() {
        return (!this.selectedProcess && !this.shouldSelectHierarchyItem) ||
        ((!this.selectedHierarchy || !this.isLastLevel(this.selectedHierarchy)) && this.shouldSelectHierarchyItem);
    }

    isLastLevel(selectedHierarchy) {
        let hierarchyType = this.hierarchyTypes.find(type => type.id === selectedHierarchy.hierarchyTypeId);
        if (!this.hierarchyTypes.find(type => type.parentId === hierarchyType.id)) {
            return true;
        }
        return false;
    }
}
