import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ProcessManagementService } from "src/app/process-managment/services/process-management.service";
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';

@Component({
  selector: "app-delete-issue",
  templateUrl: "delete-issue.component.html",
  styleUrls: ["delete-issue.component.css"]
})
export class DeleteIssueComponent implements OnInit {
    @Input() issueToDelete;
    @Input() openDeleteIssueWindow;
    @Output() closeDeleteIssueWindow = new EventEmitter();
    @Output() updateIssueList = new EventEmitter();

    constructor(
        private processManagementService: ProcessManagementService,
        private messageService: MessageService
    ) {}

    ngOnInit() {}

    close() {
        this.closeDeleteIssueWindow.emit();
    }

    submitClicked() {
        this.processManagementService.deleteIssue(this.issueToDelete.id).subscribe(res => {
            if (res) {
                this.messageService.sendMessage({text: 'Issue was deleted successfully!', type: MessageType.Success});
                this.updateIssueList.emit(this.issueToDelete);
            } else {
                this.close();
            }
        })
        
    }
} 
