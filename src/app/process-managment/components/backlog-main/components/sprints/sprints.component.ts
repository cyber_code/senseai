import { Component, OnInit, Input, Output, EventEmitter, HostListener, ViewChild } from '@angular/core';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { isNullOrUndefined } from 'util';
import { Guid } from 'src/app/shared/guid';
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import { IssuesComponent } from '../../../issues/issues.component';
import { SessionService } from 'src/app/core/services/session.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem, CdkDrag} from '@angular/cdk/drag-drop';
import {StatusType} from '../../../../models/status-type';
import { CompleteSprintComponent } from '../complete-sprint/complete-sprint.component';
import {DurationUnit} from '../../../../models/duration-unit';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-sprints',
  templateUrl: './sprints.component.html',
  styleUrls: ['./sprints.component.css']
})
export class SprintsComponent implements OnInit {

    public sprintItems = [];
    public openMoveIssueWindow = false;
    public issueToMove;
    public sprintsToMove = [];
    public selectedSprintItems = [];
    public openCompleteSprintWindow = false;
    public openIssues = [];
    public doneIssues = [];
    public openSprints = [];
    public sprintToComplete;
    public issueToDelete;
    public openDeleteIssueWindow = false;
    public openAddSprintWindow = false;
    public sprintToEdit = null;
    public sprintToDelete = null;


    @Output() backlogIssueMoved = new EventEmitter();
    @Output() sprintIssueClicked = new EventEmitter();
    @Output() sprintDroped = new EventEmitter();
    @Output() moveIssuesFromSprint = new EventEmitter();
    @Output() issueToShow = new EventEmitter();
    @Output() selectionRemoved = new EventEmitter();
    @Output() sprintIssueDeleted = new EventEmitter();

    @Input() people;
    @Input() openDetails;
    @Input()
    set sprintsIssues(sprintsIssues) {
        if (sprintsIssues) {
            this.sprintItems = sprintsIssues.map(item => {
                if (item.elType === 'Sprint' && item.started) {
                    item.expectedEndDate = this.getDuration(item);
                    return item;
                }
                return item;
            });
        }
        
    }

    @ViewChild(CompleteSprintComponent)
    private completeSprintComponent: CompleteSprintComponent;
    
    constructor(
        private processManagementService: ProcessManagementService,
        private messageService: MessageService,
        private sessionService: SessionService,
        private permissionService: PermissionService
    ) {}

    @HostListener("click", ['$event'])
    onDropdownItemClick( evt: any) {
        if (evt && evt.target && evt.ctrlKey && (evt.target.className.includes('example-box') || evt.target.className.includes('title'))) {
            if (evt.target.id && !this.selectedSprintItems.find(el => el.id === evt.target.id)) {
                let item = this.sprintItems.find(el => el.id === evt.target.id);
                if (item.elType === 'Issue') {
                    this.selectedSprintItems.push(item);
                    this.sprintIssueClicked.emit();
                }
            } else if (evt.target.id) {
                this.selectedSprintItems = this.selectedSprintItems.filter(el => el.id !== evt.target.id);
                this.selectionRemoved.emit();
            }
        } else if (evt && evt.target && (evt.target.className.includes('example-box') || evt.target.className.includes('title'))) {
            if (evt.target.id) {
                let item = this.sprintItems.find(el => el.id === evt.target.id);
                if (item.elType === 'Issue') {
                    this.selectedSprintItems = [item];
                    this.sprintIssueClicked.emit();
                    this.issueToShow.emit({issue: item, openDetails: true});
                }
            }
        }
    }

    ngOnInit() {}

    startSprint(sprint) {
        this.processManagementService.startSprint(sprint.id).subscribe(res => {
            if (res) {
                this.processManagementService.getSprint(sprint.id).subscribe((sprintItem: any) => {
                    if (sprintItem) {
                        this.sprintItems = this.sprintItems.map(item => {
                            if (item.id === sprintItem.id) {
                                return {...sprintItem, started: true, expectedEndDate: this.getDuration(sprintItem)}
                            }
                            return item;
                        });
                    }
                });
            }
        });
    }

    format(date) {
        let month = date.getUTCMonth() + 1;
        let day = date.getUTCDate();
        let year = date.getUTCFullYear();
    
        return day + "/" + month + "/" + year;
    }

    getDurationDate(sprint) {
        return this.format(new Date(sprint.start)) + " - " + this.format(new Date(sprint.expectedEndDate));
    }

    getDuration(sprint) {
        if (sprint.durationUnit === DurationUnit.Days) {
            return this.addDays(sprint.start, sprint.duration);
        } else {
            return this.addWeeks(sprint.start, sprint.duration);
        }
    }

    addDays(date, days) {
        let result = new Date(date);
        result.setDate(result.getDate() + Number(days));
        return result;
    }

    addWeeks(date, weeks) {
        let result = new Date(date);
        result.setDate(result.getDate() + Number(weeks) * 7);
        return result;
    }

    submitCompleteSprint(event) {
        this.moveIssuesFromSprint.emit({...event, openIssues: this.sprintToDelete ? this.openIssues.concat(this.doneIssues) : this.openIssues});
    }

    async completeSprint(sprint) {
        this.sprintToComplete = sprint;
        let openIssues = this.sprintItems.filter(
            iss => iss.sprintId === sprint.id && 
            (iss.issueSprint.status === StatusType.ToDo || 
            iss.issueSprint.status === StatusType.InProgress)
        );
        let doneIssues = this.sprintItems.filter(
            iss => iss.sprintId === sprint.id && 
            (iss.issueSprint.status === StatusType.Done)
        );

        this.openIssues = openIssues;
        this.doneIssues = doneIssues;
        this.openSprints = this.sprintItems.filter(sp => sp.elType === 'Sprint' && sp.id !== sprint.id);
        this.openCompleteSprintWindow = true;
        
    }

    complete(sprint) {
        this.sprintItems = this.sprintItems.filter(sp => sp.id !== sprint.id && sp.sprintId !== sprint.id);
    }

    showActions(item) {
        this.sprintItems = this.sprintItems.map(sprintItem => {
            if (sprintItem.id === item.id)
                return {...sprintItem, showActions: !item.showActions};
            return {...sprintItem, showActions: false};
        });
    }

    closeCompleteSprintWindow() {
        this.openCompleteSprintWindow = false;
        this.openIssues = [];
        this.openSprints = [];
        this.sprintToComplete = null;
        this.completeSprintComponent.selectedSprint = null;
        this.sprintToDelete = null;
        this.completeSprintComponent.backlogClicked();
    }

    moveIssueClicked(issue) {
        this.issueToMove = issue;
        this.sprintsToMove = this.sprintItems.filter(item => item.elType === 'Sprint' && item.id !== issue.sprintId);
        if (this.sprintsToMove.length === 0) {
            this.messageService.sendMessage({text: issue.sprintId ?  'There are no other open sprints!' : 'There are no open sprints!', type: MessageType.Warning});
            return;
        } else if (this.sprintsToMove.length === 1) {
            this.addToActiveSprint(issue);
        } else {
            this.openMoveIssueWindow = true;
        }
    }

    updateIssueList(deletedIssue) {
        this.sprintItems = this.sprintItems.filter(el => el.id !== deletedIssue.id);
        let sprint = this.sprintItems.find(sp => sp.id === deletedIssue.sprintId);
        this.selectedSprintItems = this.selectedSprintItems.filter(item => item.id !== deletedIssue.id);
        if (sprint.issues) 
            sprint.issues = sprint.issues.filter(iss => iss.id !== deletedIssue.id);
        this.closeDeleteIssueWindow();
        this.sprintIssueDeleted.emit(deletedIssue);
    }

    deleteIssueClicked(issue) {
        this.issueToDelete = issue;
        this.openDeleteIssueWindow = true;
    }

    hideStartBtn(sprint) {
        let activeSprint = this.sprintItems.find(item => item.started);
        return (activeSprint && sprint && activeSprint.id !== sprint.id);
    }

    shouldBeActive(item) {
        return this.selectedSprintItems.find(el => el.id === item.id);
    }

    addToActiveSprint(issue) {
        if (issue.sprintId) {
            this.processManagementService
            .updateSprintIssue(
                this.issueToMove.issueSprint.id,
                this.sprintsToMove[0].id,
                this.issueToMove.id,
                this.issueToMove.issueSprint.status
            ).subscribe(res => {
                this.handleMoveIssueResponse(res, this.sprintsToMove[0]);
            });
        } else {
            this.processManagementService
                .addSprintIssue(
                    this.sprintsToMove[0].id,
                    issue.id, 
                    issue.status
                ).subscribe(res => {
                    this.handleMoveIssueResponse(res, this.sprintsToMove[0]);
                    this.backlogIssueMoved.emit(issue);
                });
        }
    }

    closeDeleteIssueWindow() {
        this.openDeleteIssueWindow = false;
        this.issueToDelete = null;
    }

    closeMoveIssueWindow() {
        this.openMoveIssueWindow = false;
        this.sprintsToMove = [];
    }

    startDrag(item) {
        if (!this.permissionService.hasPermission("/api/Process/Sprint/DeleteSprintIssue"))
            return;
        if (this.selectedSprintItems.find(el => el.id === item.id)) {
            item.isDragged = true;
        } else {
            this.selectedSprintItems = [item];
            this.sprintIssueClicked.emit();
            item.isDragged = true;
        }
        this.issueToShow.emit({issue: item, openDetails: false});
    }
    
    endDrag(item) {
        item.isDragged = false;
    }

    resetSprintItems() {
        this.selectedSprintItems = [];
    }

    drop(event: CdkDragDrop<string[]>) {
        this.sprintDroped.emit(event);
    }

    async handleMoveIssueResponse(res, selectedSprint) {
        if (res) {
            this.messageService.sendMessage({
                text: 'Issue was moved successfully',
                type: MessageType.Success
            });

            this.updateSprintList({...this.issueToMove, sprintId: selectedSprint.id, 
                issueSprint: (this.issueToMove.issueSprint) ? 
                    {...this.issueToMove.issueSprint, sprintId: selectedSprint.id} : 
                    {sprintId: selectedSprint.id, issueId: this.issueToMove.id, status: this.issueToMove.status, id: res.id}});
            this.closeMoveIssueWindow();
        } else {
            this.closeMoveIssueWindow();
        }
    }

    updateSprintList(issue) {
        let newSprintIndex = -1;

        let oldSprint = this.sprintItems.find(sp => sp.elType === 'Sprint' && sp.id === this.issueToMove.sprintId);

        if (oldSprint) {
            this.sprintItems = this.sprintItems.filter(item => item.id !== issue.id);
            oldSprint.issues = oldSprint.issues.filter(iss => iss.id !== issue.id);
        }

        let newSprint = this.sprintItems.find((sp, i) => {
            if (sp.elType === 'Sprint' && sp.id === issue.sprintId) {
                newSprintIndex = i;
                return true;
            }
            return false;
        });

        this.sprintItems.splice(newSprintIndex + newSprint.issues.length + 1, 0, issue);
        newSprint.issues = newSprint.issues.concat(issue);
       

        if (!oldSprint) {
            this.backlogIssueMoved.emit(issue);
        }
    }

    closeAddSprintWindow() {
        this.openAddSprintWindow = false;
    }

    editSprint(sprintToEdit) {
        sprintToEdit.showActions = false;
        this.sprintToEdit = Object.assign({}, sprintToEdit);
        this.openAddSprintWindow = true;
    }

    async deleteSprintClicked(sprintToDelete) {
        sprintToDelete.showActions = false;
        let openIssues = this.sprintItems.filter(
            iss => iss.sprintId === sprintToDelete.id && 
            (iss.issueSprint.status === StatusType.ToDo || 
            iss.issueSprint.status === StatusType.InProgress)
        );

        let doneIssues = this.sprintItems.filter(
            iss => iss.sprintId === sprintToDelete.id && 
            (iss.issueSprint.status === StatusType.Done)
        );
        if (openIssues.length === 0 && doneIssues.length === 0) {
            let res = await this.processManagementService.deleteSprint(sprintToDelete.id).toPromise();
            if (res)
                this.removeSprint(sprintToDelete);
            return;
        }

        this.openIssues = openIssues;
        this.doneIssues = doneIssues;
        this.openSprints = this.sprintItems.filter(sp => sp.elType === 'Sprint' && sp.id !== sprintToDelete.id);
        this.sprintToDelete = sprintToDelete;
        this.openCompleteSprintWindow = true;
    }

    removeSprint(sprintToDelete) {
        this.sprintItems = this.sprintItems.filter(item => item.sprintId !== sprintToDelete.id && item.id !== sprintToDelete.id);
        this.messageService.sendMessage({type: MessageType.Success, text: 'Sprint was deleted successfully!'});
    }

    updateSprints(item) {
        this.sprintItems = this.sprintItems.map(sprintItem => {
            if (sprintItem.id === item.id)
                return {...item, expectedEndDate: this.getDuration(item)};
            return sprintItem;
        });
    }
}
