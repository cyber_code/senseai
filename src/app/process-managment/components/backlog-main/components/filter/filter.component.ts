import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { Guid } from 'src/app/shared/guid';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  public searchCriteria = '';
  public selectedAssignees = [];
  public show = false;
  public highlightMoreBtn = false;
  public openRequirementDialog = false;
  public selectedRequirement;

  @Input() assignees;

  @Output() filterChanged = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  public onClick(item) {
    if (!this.selectedAssignees.find(assignee => assignee.id === item.id)) {
      this.selectedAssignees.push(item);
      this.filter();
    } else {
      this.selectedAssignees = this.selectedAssignees.filter(assignee => assignee.id !== item.id);
      this.filter();
    }
  }

  filter() {
    this.filterChanged.emit({
      search: this.searchCriteria,
      assignees: this.selectedAssignees.map(assignee => assignee.id),
      requirementId: this.selectedRequirement ? this.selectedRequirement.id : Guid.empty
    });
  }

  shouldHighlight(item) {
    return (this.selectedAssignees.find(assignee => assignee.id === item.id));
  }

  onClickMoreBtn() {
    this.show = !this.show;
  }

  openRequirement(e) {
    if (e && e.target && e.target.className.includes('clearable__clear')) {
      this.clearRequirement();
    } else {
      this.openRequirementDialog = true;
    }
  }


  clearRequirement() {
    this.selectedRequirement = null;
    this.filter();
  }

  selectRequirementSubmited(requirementObj) {
    this.selectedRequirement = requirementObj.process;
    this.closeRequirementDialog();
    this.filter();
  }

  closeRequirementDialog() {
    this.openRequirementDialog = false;
  }

  onCheckboxClick(item) {
    this.onClick(item);
    if (this.assignees.slice(6).find(el => this.selectedAssignees.find(item => item.id === el.id))) {
      this.highlightMoreBtn = true;
    } else {
      this.highlightMoreBtn = false;
    }
  }

  onSearch(event) {
    this.searchCriteria = event.target.value.trim('');
    this.filter();
  }

  clear() {
    this.searchCriteria = '';
    this.filter();
  }

}
