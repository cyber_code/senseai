import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import { SessionService } from "src/app/core/services/session.service";
import {DurationUnit} from "../../../../models/duration-unit";
import { ProcessManagementService } from "src/app/process-managment/services/process-management.service";

@Component({
  selector: "app-add-sprint",
  templateUrl: "add-sprint.component.html",
  styleUrls: ["add-sprint.component.css"]
})
export class AddSprintComponent implements OnInit {

    public sprintToAdd;
    public selectedDurationUnit;
    public durationUnits = [{title: 'Weeks', type: DurationUnit.Weeks},
        {title: 'Days', type: DurationUnit.Days}];

    @Input() openAddSprintWindow;
    @Input() isSprintEdited;
    @Input()
    set sprintToEdit(sprintToEdit) {
        if (sprintToEdit) {
            this.sprintToAdd = sprintToEdit;
            this.selectedDurationUnit = this.durationUnits.find(durationUnit => durationUnit.type === sprintToEdit.durationUnit);
        } else {
            this.sprintToAdd = {
                title: '',
                description: '',
                start: '0001-01-01T00:00:00',
                end: '0001-01-01T00:00:00',
                duration: 0,
                durationUnit: DurationUnit.Weeks
            }
            this.selectedDurationUnit = this.durationUnits[0];
        }
    }
    @Output() public closeAddSprintWindow = new EventEmitter<any>();
    @Output() public sprintListUpdated = new EventEmitter<any>();


    constructor(
        private processManagementService: ProcessManagementService,
        private sessionService: SessionService,
        private messageService: MessageService
    ) {
    }

    ngOnInit() {}

    close() {
        this.resetSprintToAdd();
        this.closeAddSprintWindow.emit();
    }

    changeTitle(event) {
        let title = event.target.value.trim("");
        this.sprintToAdd.title = title;
    }

    changeDescription(event) {
        let description = event.target.value.trim("");
        this.sprintToAdd.description = description;
    }

    changeDuration(event) {
        this.sprintToAdd.duration = Number(event.target.value);
    }

    disableSubmitBtn() {
        return (
            this.sprintToAdd.title === ''
        );
    }

    submitClicked() {
        if (this.isSprintEdited) {
            this.editSprint();
        } else {
            this.addSprint();
        }
        
    }

    changeDurationUnit(event) {
        if (event) {
            this.selectedDurationUnit = event;
            this.sprintToAdd.durationUnit = event.type;
        }
    }

    addSprint() {
        let subProjectId = this.sessionService.getSubProject().id;
        this.processManagementService
            .addSprint(
                this.sprintToAdd.title,
                this.sprintToAdd.description,
                this.sprintToAdd.start,
                this.sprintToAdd.end,
                subProjectId,
                this.sprintToAdd.duration,
                this.sprintToAdd.durationUnit
            ).subscribe(res => {
                this.handleSprintResponse(res);
            });
    }

    editSprint() {
        let subProjectId = this.sessionService.getSubProject().id;
        this.processManagementService
            .updateSprint(
                this.sprintToAdd.id,
                this.sprintToAdd.title,
                this.sprintToAdd.description,
                this.sprintToAdd.start,
                this.sprintToAdd.end,
                subProjectId,
                this.sprintToAdd.duration,
                this.sprintToAdd.durationUnit
            ).subscribe(res => {
                this.handleSprintResponse(res);
            });
    }

    resetSprintToAdd() {
        this.sprintToAdd = {
            title: '',
            description: '',
            start: '0001-01-01T00:00:00',
            end: '0001-01-01T00:00:00'
        };
    }

    async handleSprintResponse(res) {
        if (res) {
            this.messageService.sendMessage({
                text: this.isSprintEdited ? 'Sprint was edited successfully!' : 'Sprint was added successfully!',
                type: MessageType.Success
            });

            this.sprintListUpdated.emit(
                this.isSprintEdited ? this.sprintToAdd : 
                {...this.sprintToAdd, id: res.id, elType: 'Sprint', started: false, issues: []}
            );
            this.close();
        } else {
            this.close();
        }
    }
} 
