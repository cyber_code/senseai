import { Component, OnInit, EventEmitter, Output, Input, HostListener } from '@angular/core';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Guid } from 'src/app/shared/guid';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-backlog',
  templateUrl: './backlog.component.html',
  styleUrls: ['./backlog.component.css']
})

export class BacklogComponent implements OnInit {
    public backlogIssues = [];
    public selectedBacklogItems = [];
    public hasItemEntered = false;
    public openDeleteIssueWindow = false;
    public issueToDelete;

    @Input() openDetails;

    @Output() public backlogIssueClicked = new EventEmitter();
    @Output() public backlogDroped = new EventEmitter();
    @Output() public issueToShow = new EventEmitter();
    @Output() public selectionRemoved = new EventEmitter();
    @Output() public backlogIssueDeleted = new EventEmitter();
    @Input() 
    set _backlogIssues(_backlogIssues) {
      this.backlogIssues = _backlogIssues;
    }

  constructor(
      private processManagementService: ProcessManagementService,
      private sessionService: SessionService,
      private permissionService: PermissionService
  ) { }

  @HostListener("click", ['$event'])
  onDropdownItemClick( evt: any) {
    if (evt && evt.target && evt.ctrlKey && (evt.target.className.includes('example-box') || evt.target.className.includes('title'))) {
      if (evt.target.id && !this.selectedBacklogItems.find(el => el.id === evt.target.id)) {
        let item = this.backlogIssues.find(el => el.id === evt.target.id);
        this.selectedBacklogItems.push(item);
        this.backlogIssueClicked.emit();
      }

      else if (evt.target.id) {
        this.selectedBacklogItems = this.selectedBacklogItems.filter(el => el.id !== evt.target.id);
        this.selectionRemoved.emit();
      }
    } else if (evt && evt.target && (evt.target.className.includes('example-box') || evt.target.className.includes('title'))) {
      if (evt.target.id) {
        let item = this.backlogIssues.find(el => el.id === evt.target.id);
        this.selectedBacklogItems = [item];
        this.issueToShow.emit({issue: item, openDetails: true});
        this.backlogIssueClicked.emit();
      }
    }
  }

  @Output() public moveIssue = new EventEmitter();
  @Input() people;

  ngOnInit() {}

  moveIssueClicked(issue) {
      this.moveIssue.emit(issue);
  }

  shouldBeActive(item) {
    return this.selectedBacklogItems.find(el => el.id === item.id);
  }

  startDrag(item) {
    if (!this.permissionService.hasPermission("/api/Process/Sprint/AddSprintIssue"))
      return;
    if (this.selectedBacklogItems.find(el => el.id === item.id)) {
      item.isDragged = true;
    } else {
      this.selectedBacklogItems = [item];
      this.backlogIssueClicked.emit();
      item.isDragged = true;
    }
    this.issueToShow.emit({issue: item, openDetails: false});
  }

  endDrag(item) {
    item.isDragged = false;
  }

  resetBacklogIssues() {
    this.selectedBacklogItems = [];
  }

  drop(event) {
    this.hasItemEntered = false;
    this.backlogDroped.emit(event);
  }

  exitItem() {
    this.hasItemEntered = false;
  }

  enteredItem() {
    this.hasItemEntered = true;
  }

  deleteIssueClicked(issue) {
    this.issueToDelete = issue;
    this.openDeleteIssueWindow = true;
  }

  updateIssueList(deletedIssue) {
    this.backlogIssues = this.backlogIssues.filter(el => el.id !== deletedIssue.id);
    this.selectedBacklogItems = this.selectedBacklogItems.filter(item => item.id !== deletedIssue.id);
    this.closeDeleteIssueWindow();
    this.backlogIssueDeleted.emit(deletedIssue);
  }

  closeDeleteIssueWindow() {
    this.openDeleteIssueWindow = false;
    this.issueToDelete = null;
  }
}
