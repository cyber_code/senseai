import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import {Priorities} from '../../../../models/priorities';
import {InvestigationStatus} from '../../../../models/investigation-status';
import {ChangeStatus} from '../../../../models/change-status';
import {Type} from '../../../../models/type';
import {Severity} from '../../../../models/severity';
import { ProcessManagementService } from '../../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Guid } from 'src/app/shared/guid';
import { DesignService } from 'src/app/design/services/design.service';
import { isNullOrUndefined } from 'util';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-add-edit-issue',
  templateUrl: './add-edit-issue.component.html',
  styleUrls: ['./add-edit-issue.component.css']
})
export class AddEditIssueComponent implements OnInit {
  public _issueWindowOpen;
  public issueTypes = [];
  public allIssueTypes = [];
  public sprints = [];
  public components = [];
  public typicals = [];
  public resources = [];

  public priorities = [{title: 'Low', type: Priorities.Low}, 
    {title: 'Medium', type: Priorities.Medium},
    {title: 'High', type: Priorities.High},
    {title: 'Critical', type: Priorities.Critical}];

  public investigationStatus = [{title: 'Identified', type: InvestigationStatus.Identified},
    {title: 'Accepted', type: InvestigationStatus.Accepted},
    {title: 'Deferred', type: InvestigationStatus.Deferred},
    {title: 'Evaluated', type: InvestigationStatus.Evaluated}];

  public changeStatus = [{title: 'Submitted', type: ChangeStatus.Submitted},
    {title: 'Studied', type: ChangeStatus.Studied},
    {title: 'Reviewed', type: ChangeStatus.Reviewed},
    {title: 'PMO Approved', type: ChangeStatus.PMOApproved},
    {title: 'Security Reviewed', type: ChangeStatus.SecurityReviewed},
    {title: 'QA Reviewed', type: ChangeStatus.QAReviewed},
    {title: 'In Development', type: ChangeStatus.InDevelopment}];   

  public types = [{title: 'Software', type: Type.Software},
    {title: 'Cosmetic', type: Type.Cosmetic},
    {title: 'Performance', type: Type.Performance},
    {title: 'Database', type: Type.Database},
    {title: 'Configuration', type: Type.Configuration},
    {title: 'Environmental', type: Type.Environmental},
    {title: 'Documentation', type: Type.Documentation}
  ];
  
  public severity = [{title: 'Low', type: Severity.Low},
    {title: 'Medium', type: Severity.Medium},
    {title: 'High', type: Severity.High},
    {title: 'Immediate', type: Severity.Immediate}
  ];

  public selectedIssueType;
  public selectedPriority;
  public selectedAssigne;
  public selectedApprover;
  public selectedSprint;
  public selectedRequirement;
  public selectedComponent;
  public selectedInvestigationStatus;
  public selectedChangeStatus;
  public selectedType;
  public selectedSeverity;
  public selectedTypical;
  public selectedAttribute;
  public workflows = [];
  public affectedWorkflows = [];
  public attributes = [];
  public issueTypeFields = [];
  public openRequirementDialog = false;
  public submittedDate: Date = new Date();
  public approvalDate: Date;
  public startDate: Date;
  public dueDate: Date;
  @Input() selectedIssue;
  @Input() people;
  @Input() set issueWindowOpen(issueWindowOpen) {
    if (issueWindowOpen) {
      const catalog = this.sessionService.getDefaultSettings().catalog;
      const subProjectId = this.sessionService.getSubProject().id;
      this.processManagment.getIssueTypes(subProjectId).subscribe(issueTypes => {
        if (issueTypes) {
          this.allIssueTypes = issueTypes;
          this.issueTypes = issueTypes;
        }
      });
      if(this.permissionService.hasPermission("/api/Process/Sprint/GetOpenSprintBySubProject"))
        this.processManagment.getOpenSprintBySubProject(subProjectId).subscribe(sprints => {
          if (sprints) {
            this.sprints = sprints;
          }
        });
      if(this.permissionService.hasPermission("/api/Process/Browse/GetHierarchiesByParent"))
        this.processManagment.getHierarchiesByParent(subProjectId, Guid.empty, Guid.empty).subscribe(components => {
          if (components) {
            this.components = components;
          }
        });

      this.designService.searchTypicals('', (catalog && catalog.id) ? catalog.id : Guid.empty).subscribe(typicals => {
        this.typicals = typicals;
      });
      this.submittedDate = new Date();
    }
    this._issueWindowOpen = issueWindowOpen;
  }

  @Output() public closeIssueWindow = new EventEmitter<any>();
  @Output() public issueListUpdated = new EventEmitter<boolean>();

  constructor(
    private processManagment: ProcessManagementService,
    private sessionService: SessionService,
    private messageService: MessageService,
    private designService: DesignService,
    private permissionService: PermissionService
  ) {}

  ngOnInit() {}

  close() {
    this.closeIssueWindow.emit();
    this.resetFields();
  }

  handleChangeIssueType(search) {
    if (!search || search.trim('') === '')
      this.issueTypes = this.allIssueTypes;
    else  
      this.issueTypes = this.allIssueTypes.filter(issue => issue.title.toLowerCase().includes(search.toLowerCase()));
  }

  changeTitle(event) {
    const title = event.target.value.trim('');
    this.selectedIssue.title = title;
  }

  changeDescription(event) {
    const description = event.target.value.trim('');
    this.selectedIssue.description = description;
  }

  changeReason(event) {
    const reason = event.target.value.trim('');
    this.selectedIssue.reason = reason;
  }

  changeImplication(event) {
    const implication = event.target.value.trim('');
    this.selectedIssue.implication = implication;
  }

  changeTestCaseId(event) {
    const testCaseId = event.target.value.trim('');
    this.selectedIssue.testCaseId = testCaseId;
  }

  changeComment(event) {
    const comment = event.target.value.trim('');
    this.selectedIssue.comment = comment;
  }

  changeIssueType(event) {
    this.selectedIssueType = event;
    this.selectedIssue.issueTypeId = event ? event.id : null;
    if (event && event.id) {
      this.processManagment.getIssueTypeFields(event.id).subscribe((res: any) => {
        if (res) {
          this.issueTypeFields = res.map(el => ({...el,  name: el.issueTypeFieldsName}));
        }
      });
    } else {
      this.issueTypeFields = [];
    }
  }

  shouldDisplay(name) {
    return (this.issueTypeFields && this.issueTypeFields.find(el => el.name === name && el.checked));
  }

  changePriority(event) {
    this.selectedPriority = event;
    this.selectedIssue.priority = event ? event.type : '';
  }

  changeSeverity(event) {
    this.selectedSeverity = event;
    this.selectedIssue.severity = event ? event.type : '';
  }

  changeType(event) {
    this.selectedType = event;
    this.selectedIssue.type = event ? event.type : '';
  }

  changeAssigne(event) {
    this.selectedAssigne = event;
    this.selectedIssue.assignedId = event ? event.id : Guid.empty;
  }

  changeApprover(event) {
    this.selectedApprover = event;
    this.selectedIssue.approvedId = event ? event.id : Guid.empty;
  }

  changeSprint(event) {
    this.selectedSprint = event;
  }

  changeEstimate(event) {
    const estimate = event.target.value.trim('');
    this.selectedIssue.estimate = estimate;
  }

  changeComponent(event) {
    this.selectedComponent = event;
    this.selectedComponent.componentId = event ? event.id : Guid.empty;
  }

  changeApprovedForInvestigation(event) {
    this.selectedIssue.approvedForInvestigation = event.target.checked;
  }

  changeChangeStatus(event) {
    this.selectedChangeStatus = event;
    this.selectedIssue.changeStatus = event ? event.type : '';
  }

  changeInvestigationStatus(event) {
    this.selectedInvestigationStatus = event;
    this.selectedIssue.investigationStatus = event ? event.type : '';
  }

  changeTypical(typical) {
    const catalog = this.sessionService.getDefaultSettings().catalog;
    this.selectedTypical = typical;
    this.selectedIssue.typicalId = typical ? typical.id : Guid.empty;
    this.selectedAttribute = null;
    this.designService.SearchAttributesByCatalogIdAndTypicalName(
      '', 
      (catalog && catalog.id) ? catalog.id : Guid.empty, 
      typical ? typical.title : ''
    ).subscribe(attributes => {
      if (attributes) {
        this.attributes = attributes;
      }
    });
  }

  handleChange(search) {
    const catalog = this.sessionService.getDefaultSettings().catalog;
    this.designService.searchTypicals(search, (catalog && catalog.id) ? catalog.id : Guid.empty).subscribe(typicals => {
      this.typicals = typicals;
    });
  }

  handleChangeAttributes(search) {
    if (!this.selectedTypical) {
      return;
    }

    const catalog = this.sessionService.getDefaultSettings().catalog;
    this.designService.SearchAttributesByCatalogIdAndTypicalName(
      search, 
      (catalog && catalog.id) ? catalog.id : Guid.empty, 
      this.selectedTypical.title
    ).subscribe(attributes => {
      if (attributes) {
        this.attributes = attributes;
      }
    });
  }

  changeAttribute(event) {
    this.selectedAttribute = event;
    this.selectedIssue.attribute = event ? event.Name : '';
  }

  disableSubmitBtn() {
    return (
      this.selectedIssue.title === '' ||
      !this.selectedIssue.issueTypeId ||
      !this.selectedIssue.assignedId ||
      this.selectedIssue.assignedId === Guid.empty
    );
  }

  submitClicked() {
      this.addIssue();
  }

  onFileSelected(event) {
    const fileList = event.target.files;
    Object.keys(fileList).forEach(key => {
      fileList[key].fileName = fileList[key].name;
      fileList[key].id = Guid.newGuid();
      this.resources.push(fileList[key]);
    });
    event.target.value = '';
  }

  onDeleteClicked(id) {
    this.resources = this.resources.filter(file => file.id !== id);
  }

  async addIssue() {
    let userIdFromAdminPanel = this.sessionService.getUser().userIdFromAdminPanel;
    const subProjectId = this.sessionService.getSubProject().id;
    let userFromAdminPanel = await this.processManagment.getPeopleByUserFromAdmin(userIdFromAdminPanel ? Number(userIdFromAdminPanel) : 0, subProjectId).toPromise() as any;
    let sprintIssueResult;
    const res = await this.processManagment
      .addIssue(
        this.selectedRequirement ? this.selectedRequirement.id : Guid.empty,
        subProjectId,
        this.selectedIssue.issueTypeId,
        this.selectedIssue.assignedId ? this.selectedIssue.assignedId : Guid.empty,
        this.selectedIssue.title,
        this.selectedIssue.description,
        !isNullOrUndefined(this.selectedIssue.status) ? this.selectedIssue.status : 0,
        this.selectedIssue.estimate ? this.selectedIssue.estimate : 0,
        this.selectedIssue.reason,
        this.selectedIssue.implication,
        this.selectedIssue.comment,
        this.selectedIssue.typicalId ? this.selectedIssue.typicalId : Guid.empty,
        this.selectedIssue.attribute ? this.selectedIssue.attribute : '',
        this.selectedIssue.approvedForInvestigation,
        !isNullOrUndefined(this.selectedIssue.investigationStatus) ? this.selectedIssue.investigationStatus : -1,
        !isNullOrUndefined(this.selectedIssue.changeStatus) ? this.selectedIssue.changeStatus : -1,
        userFromAdminPanel ? userFromAdminPanel.id : Guid.empty,
        this.selectedIssue.approvedId ? this.selectedIssue.approvedId : Guid.empty,
        this.submittedDate ? this.submittedDate.toJSON() : '0001-01-01T00:00:00',
        this.approvalDate ? this.approvalDate.toJSON() : '0001-01-01T00:00:00',
        this.startDate ? this.startDate.toJSON() : '0001-01-01T00:00:00',
        !isNullOrUndefined(this.selectedIssue.priority) ? this.selectedIssue.priority : -1,
        !isNullOrUndefined(this.selectedIssue.severity) ? this.selectedIssue.severity : -1,
        this.dueDate ? this.dueDate.toJSON() : '0001-01-01T00:00:00',
        !isNullOrUndefined(this.selectedIssue.type) ? this.selectedIssue.type : -1,
        this.selectedIssue.componentId ? this.selectedIssue.componentId : Guid.empty,
        this.selectedIssue.testCaseId ? this.selectedIssue.testCaseId : ""
      ).toPromise();

      if (res && this.selectedSprint) {
        sprintIssueResult = await this.processManagment.addSprintIssue(this.selectedSprint.id, res.id, 0).toPromise() as any;
      }

      if (res && this.resources.length > 0) {
        await this.processManagment.addIssueResources(res.id, this.resources).toPromise();
      }

      if (res && this.affectedWorkflows.length > 0) {
        for (let i = 0; i < this.affectedWorkflows.length; i++) {
          const workflow = this.affectedWorkflows[i];
          this.processManagment.addWorkflowIssue(workflow.workflowId, res.id).subscribe(result => {
            if (result && i === this.affectedWorkflows.length - 1) {
              this.handleIssueResponse(res, sprintIssueResult);
            }
          });
        }
      } else {
        this.handleIssueResponse(res, sprintIssueResult);
      }
  }

  openRequirement() {
    this.openRequirementDialog = true;
  }

  affectedWfChecked(workflow, event) {
    const isChecked = event.target.checked;
    if (isChecked) {
      this.affectedWorkflows.push(workflow);
    } else {
      this.affectedWorkflows = this.affectedWorkflows.filter(wf => wf.id !== workflow.id);
    }
  }

  isChecked(workflow) {
    return (this.affectedWorkflows.find(wf => wf.id === workflow.id));
  }

  selectRequirementSubmited(requirementObj) {
    this.selectedRequirement = requirementObj.process;
    this.selectedComponent = requirementObj.component;
    this.selectedIssue.componentId = requirementObj.component.id;
    this.closeRequirementDialog();
    this.processManagment.getProcessWorkflows(requirementObj.process.id).subscribe(issueWf => {
      if (issueWf) {
        this.workflows = issueWf;
      }
    });
  }

  closeRequirementDialog() {
    this.openRequirementDialog = false;
  }

  resetFields() {
    this.selectedIssueType = null;
    this.selectedPriority = null;
    this.selectedAssigne = null;
    this.selectedApprover = null;
    this.selectedSprint = null;
    this.selectedRequirement = null;
    this.selectedComponent = null;
    this.selectedInvestigationStatus = null;
    this.selectedChangeStatus = null;
    this.selectedType = null;
    this.selectedSeverity = null;
    this.selectedTypical = null;
    this.selectedAttribute = null;
    this.workflows = [];
    this.affectedWorkflows = [];
    this.issueTypeFields = [];
    this.attributes = [];
    this.resources = [];
    this.openRequirementDialog = false;
    this.submittedDate = null;
    this.approvalDate = null;
    this.startDate = null;
    this.dueDate = null;
  }

  async handleIssueResponse(res, sprintIssueResult) {
    if (res) {
      this.messageService.sendMessage({
        text: 'Issue was added successfully!',
        type: MessageType.Success
      });
      let issue = await this.processManagment.getIssue(res.id).toPromise();
      this.issueListUpdated.emit(
        sprintIssueResult ? 
        {
          ...issue,
          sprintId: this.selectedSprint.id,
          issueSprint: {id: sprintIssueResult.id, sprintId: this.selectedSprint.id, issueId: res.id, status: 0} 
        } : issue
      );
      this.resetFields();
    } else {
      this.close();
    }
  }
}
