import { Component, OnInit, ViewChild } from '@angular/core';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { isNullOrUndefined } from 'util';
import { Guid } from 'src/app/shared/guid';
import { SprintsComponent } from '../sprints/sprints.component';
import { BacklogComponent } from '../backlog/backlog.component';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import * as _ from 'lodash';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-backlog-main',
  templateUrl: './backlog-main.component.html',
  styleUrls: ['./backlog-main.component.css']
})
export class BacklogMainComponent implements OnInit {
  public openAddSprintWindow = false;

  public selectedIssue = {
    title: '',
    description: '',
    reason: '',
    implication: '',
    testCaseId: '',
    comment: '',
    statusType: 0,
    estimate: '',
    approvedForInvestigation: false
  };

  public issueToShow;
  public openDetails;
  public issueToClone;

  public people = [];
  public backlogIssues = [];
  public sprintsIssues = [];
  public issueTypes = [];
  public assignees = [];
  public affectedWorkflows = [];
  public linkedIssues = [];
  public resources = [];
  

  public issueWindowOpen = false;
  public openCloneIssueWindow = false;

  @ViewChild(SprintsComponent)
  private sprintsComponent: SprintsComponent;

  @ViewChild(BacklogComponent)
  private backlogComponent: BacklogComponent;

  constructor(
    private processManagementService: ProcessManagementService,
    private sessionService: SessionService,
    private permissionService: PermissionService
  ) {}

  ngOnInit() {
    let subProjectId = this.sessionService.getSubProject().id;
    if (this.permissionService.hasPermission("/api/People/GetPeople")) {
      this.processManagementService.getPeoples(subProjectId).subscribe(people => {
        this.people = people;
        this.setIssues();
      });
    } else {
      this.setIssues();
    }
      
    if (this.permissionService.hasPermission("/api/Process/Configure/GetIssueTypes"))
      this.processManagementService.getIssueTypes(subProjectId).subscribe(issueTypes => {
        if (issueTypes) {
          this.issueTypes = issueTypes;
        }
      });
  }

  setAssigneeFromIssues(allIssues) {
    let assignees = [];
    for (let i = 0; i < allIssues.length; i++) {
      let issue = allIssues[i];
      if (issue.assignedId && isNullOrUndefined(assignees.find(assignee => assignee.id === issue.assignedId))) {
        let assignee = this.people.find(person => person.id === issue.assignedId);
        if (!isNullOrUndefined(assignee)) {
          assignees.push(assignee);
        }
      }
    }
    this.assignees = assignees;
  }

  reloadAssignees(issue?) {
    let allIssues = this.sprintsComponent.sprintItems.concat(this.backlogComponent.backlogIssues);
    if (issue) {
      allIssues = allIssues.map(iss => {
        if (iss.id === issue.id) 
          return {...iss, assigne: issue.assigne, assignedId: issue.assignedId};
        return iss;
      });
    }
    this.setAssigneeFromIssues(allIssues);
  }

  closeCloneIssueWindow() {
    this.openCloneIssueWindow = false;
    this.issueToClone = null;
    this.affectedWorkflows = [];
    this.resources = [];
    this.linkedIssues = [];
  }

  cloneClicked(event) {
    if (event) {
      this.issueToClone = _.cloneDeep(event.issue);
      this.affectedWorkflows = event.issueWorkflows;
      this.resources = event.resources;
      this.linkedIssues = event.linkedIssues;
    }
    this.openCloneIssueWindow = true;
  }

  async setIssues(filterObj?) {
    let subProjectId = this.sessionService.getSubProject().id;
    let openSprints = [];
    if(this.permissionService.hasPermission("/api/Process/Sprint/GetOpenSprintBySubProject"))
      openSprints = await this.processManagementService.getOpenSprintBySubProject(subProjectId).toPromise();
      if (this.permissionService.hasPermission('/api/Process/Browse/GetIssuesForSprintPlannerWithFilters')) {
        this.processManagementService.getIssuesForSprintPlannerWithFilters(
          subProjectId, 
          filterObj ? filterObj.requirementId : Guid.empty,
          filterObj ? filterObj.search : '', 
          filterObj ? filterObj.assignees : []).subscribe((issues: any) => {
          let backlogIssues = issues ? issues.backlogIssues : [];
          let sprintsIssues = issues ? issues.sprintsIssues : [];
          this.setBacklogIssues(backlogIssues);
          this.setSprintIssues(openSprints, sprintsIssues);
          if (!filterObj) {
            this.setAssigneeFromIssues(backlogIssues.concat(sprintsIssues));
          }
        });
      }
  }

  sprintUpdated(event) {
    this.issueToShow = _.cloneDeep(event.issue);
    if (event.moveToSprint) {
      this.backlogComponent.selectedBacklogItems = this.backlogComponent.selectedBacklogItems.filter(item => item.id !== this.issueToShow.id);
      this.sprintsComponent.selectedSprintItems = this.sprintsComponent.selectedSprintItems.concat(this.issueToShow);
    } else if (event.moveToBacklog) {
      this.sprintsComponent.selectedSprintItems = this.sprintsComponent.selectedSprintItems.filter(item => item.id !== this.issueToShow.id);
      this.backlogComponent.selectedBacklogItems = this.backlogComponent.selectedBacklogItems.concat(this.issueToShow);
    }
    this.setIssues();
  }

  setBacklogIssues(issues) {
    let backlogIssues = [];
    for (let i = 0; i < issues.length; i++) {
      const backlogIssue = issues[i];
      const assigne = (backlogIssue.assignedId === Guid.empty) ? {name: '', surname: ''} : 
        this.people.find(person => person.id === backlogIssue.assignedId);
      backlogIssues.push({...backlogIssue, assigne: assigne, elType: 'Issue'});
    }
    this.backlogIssues = backlogIssues;
  }

  updateIssue(issue) {
    if (issue.sprintId) {
      this.sprintsComponent.sprintItems = this.sprintsComponent.sprintItems.map(item => {
        if (item.id === issue.id)
          return _.cloneDeep(issue);
        return item;
      });
    } else {
      this.backlogComponent.backlogIssues = this.backlogComponent.backlogIssues.map(item => {
        if (item.id === issue.id) 
          return _.cloneDeep(issue);
        return item;
      });
    }
  }

  async setSprintIssues(openSprints, sprintsIssues) {
    let sprintItems = [];
    for (let i = 0; i < openSprints.length; i++) {
     
      let issues = await this.processManagementService.getSprintIssues(openSprints[i].id).toPromise() as any;
      let sprintIss = sprintsIssues.filter(sp => sp.sprintId === openSprints[i].id);
      let allIssues = [];
      for (let j = 0; j < sprintIss.length; j++) {
        let issue = sprintIss[j];
        const assigne = (issue.assignedId === Guid.empty) ? {name: '', surname: ''} : 
        this.people.find(person => person.id === issue.assignedId);
        let issueSprint = issues.find(iss => iss.issueId === issue.id && iss.sprintId === openSprints[i].id);
        allIssues.push({...issue, assigne: assigne, elType: 'Issue', issueSprint: issueSprint});
      }

      sprintItems.push({...openSprints[i], issues: allIssues, elType: 'Sprint', started: (openSprints[i].start !== '0001-01-01T00:00:00')});
      sprintItems.push(...allIssues);
    }
    this.sprintsIssues = sprintItems;
  }

  addSprint() {
    this.openAddSprintWindow = true;
  }

  closeAddSprintWindow() {
    this.openAddSprintWindow = false;
  }

  removedSelection() {
    if (this.sprintsComponent.selectedSprintItems.length === 0  && 
      this.backlogComponent.selectedBacklogItems.length === 0) {
        this.openDetails = false;
        this.issueToShow = null;
    }
  }

  onDeleteIssue(issue) {
    if (this.issueToShow && this.issueToShow.id === issue.id) {
      this.openDetails = false;
      this.issueToShow = null;
    }
    this.reloadAssignees();
  }

  updateSprintList(newSprint) {
    if (this.sprintsComponent) {
      this.sprintsComponent.sprintItems = this.sprintsComponent.sprintItems.concat(newSprint);
    }
  }

  showIssue(event) {
    this.issueToShow = event.issue;
    if (event.openDetails) {
      this.openDetails = event.openDetails;
    }
  }

  backlogIssueMoved(issue) {
    if (this.backlogComponent) {
      this.backlogComponent.backlogIssues = this.backlogComponent.backlogIssues.filter(iss => iss.id !== issue.id);
    }
  }

  moveIssue(issue) {
    if (this.sprintsComponent) {
      this.sprintsComponent.moveIssueClicked(issue);
    }
  }

  backlogIssueClicked() {
    if (this.sprintsComponent) {
      this.sprintsComponent.resetSprintItems();
    }
  }

  closeDetails() {
    this.openDetails = false;
  }

  sprintIssueClicked() {
    if (this.backlogComponent) {
      this.backlogComponent.resetBacklogIssues();
    }
  }

  closeIssueWindow() {
    this.issueWindowOpen = false;
    this.resetIssue();
  }

  resetIssue() {
    this.selectedIssue = {
      title: '',
      description: '',
      reason: '',
      implication: '',
      testCaseId: '',
      comment: '',
      statusType: 0,
      estimate: '',
      approvedForInvestigation: false
    };
  }

  addBacklog() {
    this.issueWindowOpen = true;
  }

  updateIssueList(issue) {
    if (this.backlogComponent && !issue.sprintId) {
      const assigne = (issue.assignedId === Guid.empty) ? {name: '', surname: ''} : 
      this.people.find(person => person.id === issue.assignedId);
      this.backlogComponent.backlogIssues.push({...issue, assigne: assigne, elType: 'Issue'});
      this.reloadAssignees();
      this.closeIssueWindow();
    } else if (this.sprintsComponent) {
      const assigne = (issue.assignedId === Guid.empty) ? {name: '', surname: ''} : 
        this.people.find(person => person.id === issue.assignedId);
      let sprint = this.sprintsComponent.sprintItems.find(sp => sp.id === issue.sprintId);
      let index = this.sprintsComponent.sprintItems.indexOf(sprint);
      this.sprintsComponent.sprintItems.splice(index + sprint.issues.length + 1, 0, {...issue, elType: 'Issue', assigne: assigne});

      if (sprint.issues) {
        sprint.issues = sprint.issues.concat({...issue, elType: 'Issue', assigne: assigne});
      }
      this.reloadAssignees();
      this.closeIssueWindow();
      
    }
  }

  removeIssueFromBacklog(issue) {
    this.backlogComponent.updateIssueList(issue);
  }

  async moveIssuesFromSprint(event) {
    let backlog = event.backlog;
    let openIssues = event.openIssues;

    if (openIssues.length === 0) {
      let sprintToComplete = this.sprintsComponent.sprintToComplete;
      let res = await this.processManagementService.endSprint(sprintToComplete.id).toPromise();
      if (res)
        this.sprintsComponent.complete(sprintToComplete);
      this.sprintsComponent.closeCompleteSprintWindow();
      return;
    }

    if (backlog) {
      let backlogItems = this.backlogComponent.backlogIssues;
      let sprintToComplete = event.sprintToDelete ? event.sprintToDelete : this.sprintsComponent.sprintToComplete;
      this.sprintsComponent.sprintItems = this.sprintsComponent.sprintItems.filter(sp => sp.id !== sprintToComplete.id && sp.sprintId !== sprintToComplete.id);

      let removeOrCompleteSprintObservable = undefined;
      if (event.sprintToDelete) {
        removeOrCompleteSprintObservable = this.processManagementService.deleteSprint(event.sprintToDelete.id);
      } else {
        removeOrCompleteSprintObservable = this.processManagementService.endSprint(sprintToComplete.id);
      }

      removeOrCompleteSprintObservable.subscribe(async res => {
        for (let i = 0; i < openIssues.length; i++) {
          if (!event.sprintToDelete) {
            await this.processManagementService.deleteSprintIssue(openIssues[i].issueSprint.id).toPromise();
          }
          delete openIssues[i].sprintId;
          delete openIssues[i].issueSprint;
          if (res && i === openIssues.length - 1) {
            this.backlogComponent.backlogIssues = backlogItems.concat(openIssues);
            this.sprintsComponent.closeCompleteSprintWindow();
          }
        }
      });
    } else {
      let selectedSprint = event.selectedSprint;
      let sprintToComplete = event.sprintToDelete ? event.sprintToDelete : this.sprintsComponent.sprintToComplete;
      let sprintItems = this.sprintsComponent.sprintItems.filter(sp => sp.id !== sprintToComplete.id && sp.sprintId !== sprintToComplete.id);

      let removeOrCompleteSprintObservable = undefined;
      if (event.sprintToDelete) {
        removeOrCompleteSprintObservable = this.processManagementService.deleteSprint(event.sprintToDelete.id);
      } else {
        removeOrCompleteSprintObservable = this.processManagementService.endSprint(sprintToComplete.id);
      }

      removeOrCompleteSprintObservable.subscribe(res => {
        for (let i = 0; i < openIssues.length; i++) {
          let sprintIssue = openIssues[i].issueSprint;
          let addUpdateSprintIssueObservable = null;

          if (event.sprintToDelete) {
            addUpdateSprintIssueObservable = this.processManagementService.addSprintIssue(selectedSprint.id, sprintIssue.issueId, sprintIssue.status);
          } else {
            addUpdateSprintIssueObservable = this.processManagementService.updateSprintIssue(sprintIssue.id, selectedSprint.id, sprintIssue.issueId, sprintIssue.status);
          }

          addUpdateSprintIssueObservable.subscribe(res => {
            openIssues[i].issueSprint.id = res.id ? res.id : openIssues[i].issueSprint.id
            openIssues[i].issueSprint.sprintId = selectedSprint.id;
            openIssues[i].sprintId = selectedSprint.id;
            if (i === openIssues.length - 1) {
              
              if (event.sprintToDelete) {
                this.sprintsComponent.removeSprint(event.sprintToDelete);
              } else {
                this.sprintsComponent.complete(sprintToComplete);
              }
              selectedSprint.issues = selectedSprint.issues.concat(openIssues);
              let sprintIndex = sprintItems.indexOf(selectedSprint);
              let issuesIndex = sprintItems.filter(item => item.elType === 'Issue' && item.sprintId === selectedSprint.id).length;
              
              sprintItems.splice(sprintIndex + issuesIndex + 1, 0, ...openIssues);
              
              this.sprintsComponent.sprintItems = sprintItems;
              this.sprintsComponent.closeCompleteSprintWindow();
            }
          });
        }
      });
    }
  }

  async dropBacklog(event: CdkDragDrop<any[]>) {
    if (event.previousContainer === event.container) {
      let currentIndex = event.currentIndex;
      let backlogItems = this.backlogComponent.backlogIssues;
      let selectedBacklogItems = this.backlogComponent.selectedBacklogItems;
      backlogItems = backlogItems.filter(item => !selectedBacklogItems.find(el => el.id === item.id));
      backlogItems.splice(currentIndex, 0, ...selectedBacklogItems);
      this.backlogComponent.backlogIssues = backlogItems;
    } else {
      let sprintItems = this.sprintsComponent.sprintItems;
      let selectedSprintItems = this.sprintsComponent.selectedSprintItems;
      let backlogItems = this.backlogComponent.backlogIssues;
      this.sprintsComponent.sprintItems = sprintItems.filter(item => !selectedSprintItems.find(el => el.id === item.id));
      
      for (let i = 0; i < selectedSprintItems.length; i++) {
        let sprint = sprintItems.find(el => el.id === selectedSprintItems[i].sprintId);
        sprint.issues = sprint.issues.filter(issue => issue.id !== selectedSprintItems[i].id);
        this.processManagementService.deleteSprintIssue(selectedSprintItems[i].issueSprint.id).subscribe(res => {
          delete selectedSprintItems[i].sprintId;
          delete selectedSprintItems[i].issueSprint;
          if (res && i === selectedSprintItems.length - 1) {
            backlogItems.splice(event.currentIndex, 0, ...selectedSprintItems);
            this.backlogComponent.selectedBacklogItems = selectedSprintItems;
            this.sprintsComponent.resetSprintItems();
          }
        });
      }
    }
  }

  dropSprint(event: CdkDragDrop<any[]>) {
    event.currentIndex = event.currentIndex + 1;
    let previousItem = (event.previousIndex >= event.currentIndex || event.previousContainer !== event.container) ? 
      this.sprintsComponent.sprintItems[event.currentIndex - 1] : 
      this.sprintsComponent.sprintItems[event.currentIndex];
    if (event.previousContainer === event.container) {
      let currentIndex = event.currentIndex;
      let sprintItems = this.sprintsComponent.sprintItems;
      let selectedSprintItems = this.sprintsComponent.selectedSprintItems;
      sprintItems = sprintItems.filter(item => !selectedSprintItems.find(el => el.id === item.id));
      let newSprintId = previousItem.elType === 'Issue' ? previousItem.sprintId : previousItem.id;
      for (let i = 0; i < selectedSprintItems.length; i++) {
        let sprint = sprintItems.find(el => el.id === selectedSprintItems[i].sprintId);
        sprint.issues = sprint.issues.filter(issue => issue.id !== selectedSprintItems[i].id);
        let sprintIssue = selectedSprintItems[i].issueSprint;
        this.processManagementService.updateSprintIssue(sprintIssue.id, newSprintId, sprintIssue.issueId, sprintIssue.status).subscribe(res => {
          selectedSprintItems[i].issueSprint.sprintId = newSprintId;
          selectedSprintItems[i].sprintId = newSprintId;
        });
      }
      let newSprint = sprintItems.find(el => el.id === newSprintId);
      let index = sprintItems.indexOf(newSprint);
      newSprint.issues.splice(currentIndex - index - 1, 0, ...selectedSprintItems);
      sprintItems.splice(currentIndex, 0, ...selectedSprintItems);
      this.sprintsComponent.sprintItems = sprintItems;
    } else {
      let backlogItems = this.backlogComponent.backlogIssues;
      let selectedBacklogItems = this.backlogComponent.selectedBacklogItems;
      let sprintItems = this.sprintsComponent.sprintItems;

      this.backlogComponent.backlogIssues = backlogItems.filter(item => !selectedBacklogItems.find(el => el.id === item.id));
      let sprintId = previousItem.elType === 'Issue' ? previousItem.sprintId : previousItem.id;
      for (let i = 0; i < selectedBacklogItems.length; i++) {
        this.processManagementService.addSprintIssue(sprintId, selectedBacklogItems[i].id, selectedBacklogItems[i].status).subscribe((res: any) => {
          if (res) {
            selectedBacklogItems[i].sprintId = sprintId;
            selectedBacklogItems[i].issueSprint = {
              id: res.id, 
              sprintId: sprintId, 
              issueId: selectedBacklogItems[i].id, 
              status: selectedBacklogItems[i].status
            };
          }

          if (i === selectedBacklogItems.length - 1) {
            sprintItems.splice(event.currentIndex, 0, ...selectedBacklogItems);
            let sprint = sprintItems.find(el => el.id === sprintId);
            let index = sprintItems.indexOf(sprint);
            sprint.issues.splice(event.currentIndex - index - 1, 0, ...selectedBacklogItems);
            
            this.sprintsComponent.selectedSprintItems = selectedBacklogItems;
            this.backlogComponent.resetBacklogIssues();
          }
        });
      }

     
    }
  }

}
