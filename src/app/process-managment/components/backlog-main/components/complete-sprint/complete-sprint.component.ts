import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import { SessionService } from "src/app/core/services/session.service";
import { ProcessManagementService } from "src/app/process-managment/services/process-management.service";

@Component({
  selector: "app-complete-sprint",
  templateUrl: "complete-sprint.component.html",
  styleUrls: ["complete-sprint.component.css"]
})
export class CompleteSprintComponent implements OnInit {

    @Input() openIssues;
    @Input() doneIssues;
    @Input() openSprints;
    @Input() openCompleteSprintWindow;
    @Input() sprintToDelete;
    @Output() closeCompleteSprintWindow = new EventEmitter();
    @Output() submitCompleteSprint = new EventEmitter();

    public backlog = true;
    public newSprint = false;
    public selectedSprint;

    ngOnInit() {}

    close() {
        this.closeCompleteSprintWindow.emit();
        this.selectedSprint = null;
        this.backlogClicked();
    }

    submitClicked() {
        this.submitCompleteSprint.emit({
            backlog: this.backlog,
            selectedSprint: this.selectedSprint,
            sprintToDelete: this.sprintToDelete
        });
    }

    backlogClicked() {
        this.backlog = true;
        this.newSprint = false;
    }

    newSprintClicked() {
        this.backlog = false;
        this.newSprint = true;
    }

    disabledSubmitBtn() {
        return (this.newSprint && !this.selectedSprint);
    }
} 
