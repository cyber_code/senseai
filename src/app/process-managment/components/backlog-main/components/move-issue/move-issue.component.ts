import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import { SessionService } from "src/app/core/services/session.service";
import { ProcessManagementService } from "src/app/process-managment/services/process-management.service";

@Component({
  selector: "app-move-issue",
  templateUrl: "move-issue.component.html",
  styleUrls: ["move-issue.component.css"]
})
export class MoveIssueComponent implements OnInit {

    public selectedSprint = {title: ''} as any;
    @Input() sprints;
    @Input() issueToMove;
    @Input() openMoveIssueWindow;
    @Output() public closeMoveIssueWindow = new EventEmitter<any>();
    @Output() public sprintListUpdated = new EventEmitter<any>();

    constructor(
        private processManagementService: ProcessManagementService,
        private messageService: MessageService
    ) {
    }

    ngOnInit() {}

    close() {
        this.selectedSprint = null;
        this.closeMoveIssueWindow.emit();
    }

    disableSubmitBtn() {
        return (
            !this.selectedSprint
        );
    }

    submitClicked() {
        if (this.issueToMove.sprintId) {
            this.processManagementService
                .updateSprintIssue(
                    this.issueToMove.issueSprint.id,
                    this.selectedSprint.id,
                    this.issueToMove.id,
                    this.issueToMove.issueSprint.status
                ).subscribe(res => {
                    this.handleMoveIssueResponse(res);
                });
        } else {
            this.processManagementService
                .addSprintIssue(
                    this.selectedSprint.id,
                    this.issueToMove.id,
                    this.issueToMove.status
                ).subscribe(res => {
                    this.handleMoveIssueResponse(res);
                });
        }
        
    }

    async handleMoveIssueResponse(res) {
        if (res) {
            this.messageService.sendMessage({
                text: "Issue was moved successfully",
                type: MessageType.Success
            });

            this.sprintListUpdated.emit({...this.issueToMove, sprintId: this.selectedSprint.id, 
                issueSprint: (this.issueToMove.issueSprint) ?
                 {...this.issueToMove.issueSprint, sprintId: this.selectedSprint.id} :
                    {sprintId: this.selectedSprint.id, issueId: this.issueToMove.id, status: this.issueToMove.status, id: res.id}});
            this.close();
        } else {
            this.close();
        }
    }
} 
