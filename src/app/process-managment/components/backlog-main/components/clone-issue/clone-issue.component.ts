import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ProcessManagementService } from "src/app/process-managment/services/process-management.service";
import { SessionService } from "src/app/core/services/session.service";
import { Guid } from "src/app/shared/guid";
import { StatusType } from "../../../../models/status-type";
import { isNullOrUndefined } from "util";
import { PermissionService } from "src/app/core/services/permission.service";

@Component({
  selector: "app-clone-issue",
  templateUrl: "clone-issue.component.html",
  styleUrls: ["clone-issue.component.css"]
})
export class CloneIssueComponent implements OnInit {

    public currentIssue;
    public clonedIssue;
    public isSprintValueCloned = false;
    public areLinkedIssuesCloned = false;

    @Input() 
    set issue(issue) {
        if (issue) {
            this.currentIssue = issue;
            this.clonedIssue = {...issue, title: 'CLONE - ' + issue.title};
            this.resetClonedValues();
        }
    };

    @Input() resources;
    @Input() affectedWorkflows;
    @Input() linkedIssues;

    @Output() closeCloneIssueWindow = new EventEmitter();
    @Output() issueListUpdated = new EventEmitter();

    constructor(
        private sessionService: SessionService,
        private processManagementService: ProcessManagementService,
        private permissionService: PermissionService
    ) {}

    ngOnInit() {}

    ngAfterViewInit() {
        let title = document.getElementById('title') as any;
        if (title) {
            title.focus();
            title.select();
        }
    }

    changeTitle(event) {
        this.clonedIssue.title = event.target.value.trim('');
    }

    cancelClicked() {
        this.closeCloneIssueWindow.emit();
    }

    cloneSprintValue(event) {
        this.isSprintValueCloned = event.target.checked;
    }

    cloneLinkedIssues(event) {
        this.areLinkedIssuesCloned = event.target.checked;
    }

    resetClonedValues() {
        this.isSprintValueCloned = false;
        this.areLinkedIssuesCloned = false;
    }

    async cloneIssue() {
        let userIdFromAdminPanel = this.sessionService.getUser().userIdFromAdminPanel;
        const subProjectId = this.sessionService.getSubProject().id;
        let userFromAdminPanel = await this.processManagementService.getPeopleByUserFromAdmin(userIdFromAdminPanel ? Number(userIdFromAdminPanel) : 0, subProjectId).toPromise() as any;
        let sprintIssueResult;
        const res = await this.processManagementService
          .addIssue(
            this.clonedIssue.processId ? this.clonedIssue.processId : Guid.empty,
            subProjectId,
            this.clonedIssue.issueTypeId,
            this.clonedIssue.assignedId ? this.clonedIssue.assignedId : Guid.empty,
            this.clonedIssue.title,
            this.clonedIssue.description,
            StatusType.ToDo,
            this.clonedIssue.estimate ? this.clonedIssue.estimate : 0,
            this.clonedIssue.reason,
            this.clonedIssue.implication,
            this.clonedIssue.comment,
            this.clonedIssue.typicalId ? this.clonedIssue.typicalId : Guid.empty,
            this.clonedIssue.attribute ? this.clonedIssue.attribute : '',
            this.clonedIssue.approvedForInvestigation,
            !isNullOrUndefined(this.clonedIssue.investigationStatus) ? this.clonedIssue.investigationStatus : -1,
            !isNullOrUndefined(this.clonedIssue.changeStatus) ? this.clonedIssue.changeStatus : -1,
            userFromAdminPanel ? userFromAdminPanel.id : Guid.empty,
            this.clonedIssue.approvedId ? this.clonedIssue.approvedId : Guid.empty,
            new Date().toJSON(),
            this.clonedIssue.approvalDate ? this.clonedIssue.approvalDate : '0001-01-01T00:00:00',
            this.clonedIssue.startDate ? this.clonedIssue.startDate : '0001-01-01T00:00:00',
            !isNullOrUndefined(this.clonedIssue.priority) ? this.clonedIssue.priority : -1,
            !isNullOrUndefined(this.clonedIssue.severity) ? this.clonedIssue.severity : -1,
            this.clonedIssue.dueDate ? this.clonedIssue.dueDate : '0001-01-01T00:00:00',
            !isNullOrUndefined(this.clonedIssue.type) ? this.clonedIssue.type : -1,
            this.clonedIssue.componentId ? this.clonedIssue.componentId : Guid.empty,
            this.clonedIssue.testCaseId ? this.clonedIssue.testCaseId : ''
          ).toPromise();
    
          if (res && this.clonedIssue.sprintId && this.isSprintValueCloned) {
            sprintIssueResult = await this.processManagementService.addSprintIssue(this.clonedIssue.sprintId, res.id, StatusType.ToDo).toPromise() as any;
          }

          if (res && this.linkedIssues.length > 0 && this.areLinkedIssuesCloned) {
            for (let i = 0; i < this.linkedIssues.length; i++) {
                this.processManagementService.linkIssue(res.id, 0, this.linkedIssues[i].id).subscribe(() => {});
            }
          }
    
          if (res && this.affectedWorkflows.length > 0) {
            for (let i = 0; i < this.affectedWorkflows.length; i++) {
              const workflow = this.affectedWorkflows[i];
              this.processManagementService.addWorkflowIssue(workflow.workflowId, res.id).subscribe(result => {
                if (result && i === this.affectedWorkflows.length - 1) {
                  this.handleIssueResponse(res, sprintIssueResult);
                }
              });
            }
          } else {
            this.handleIssueResponse(res, sprintIssueResult);
          }
      }

      async handleIssueResponse(res, sprintIssueResult) {
        if (res) {
          let issue = await this.processManagementService.getIssue(res.id).toPromise();
          this.issueListUpdated.emit(
            sprintIssueResult ? 
            {
              ...issue,
              sprintId: this.clonedIssue.sprintId,
              issueSprint: {
                  id: sprintIssueResult.id, 
                  sprintId: this.clonedIssue.sprintId, 
                  issueId: res.id, 
                  status: this.clonedIssue.issueSprint.status
                } 
            } : issue
          );
          this.cancelClicked();
        } else {
          this.cancelClicked();
        }
      }

} 
