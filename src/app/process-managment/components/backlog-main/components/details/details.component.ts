import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Severity } from "src/app/process-managment/models/severity";
import { Type } from "src/app/process-managment/models/type";
import { ChangeStatus } from "src/app/process-managment/models/change-status";
import { StatusType } from "src/app/process-managment/models/status-type";
import { InvestigationStatus } from "src/app/process-managment/models/investigation-status";
import { Priorities } from "src/app/process-managment/models/priorities";
import { ProcessManagementService } from "src/app/process-managment/services/process-management.service";
import { DesignService } from "src/app/design/services/design.service";
import { SessionService } from "src/app/core/services/session.service";
import { Guid } from "src/app/shared/guid";
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import * as _ from "lodash";
import { isNullOrUndefined } from "util";
import { PermissionService } from "src/app/core/services/permission.service";

@Component({
  selector: "app-issue-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.css"]
})
export class IssueDetailsComponent implements OnInit {
  public _openDetails;
  public issue;
  public _sprints;

  @Input() people;

  @Output() detailsClosed = new EventEmitter();
  @Output() issueUpdated = new EventEmitter();
  @Output() sprintUpdated = new EventEmitter();
  @Output() assigneeReload = new EventEmitter();
  @Output() removeIssueFromBacklog = new EventEmitter();
  @Output() cloneClicked = new EventEmitter();

  public selectedPriority;
  public selectedSeverity;
  public selectedTypical;
  public selectedAttribute;
  public selectedReporter;
  public selectedAssignee;
  public selectedApprover;
  public selectedRequirement;
  public selectedComponent;
  public selectedInvestigationStatus;
  public selectedType;
  public selectedSprint;
  public submittedDate;
  public approvalDate;
  public selectedChangeStatus;
  public selectedStatusType;
  public startDate;
  public dueDate;
  public resources = [];
  public reqWorkflows = [];
  public issueWorkflows = [];
  public openRequirementDialog = false;

  public isTitleClicked = false;
  public isPriorityClicked = false;
  public isDescriptionClicked = false;
  public isSeverityClicked = false;
  public isApplicationClicked = false;
  public isAttributeClicked = false;
  public isReporterClicked = false;
  public isAssigneeClicked = false;
  public isApproverClicked = false;
  public isEstimateClicked = false;
  public isSprintClicked = false;
  public isTypeClicked = false;
  public isReasonClicked = false;
  public isInvestigationStatusClicked = false;
  public isImplicationClicked = false;
  public isTestCaseIdClicked = false;
  public isCommentClicked = false;
  public isChangeStatusClicked = false;
  public isApprovalDateClicked = false;
  public isStartDateClicked = false;
  public isDueDateClicked = false;
  public openLinkIssue = false;

  public allTcrIssues = [];
  public tcrIssuesToShow = [];
  public linkedIssues = [];
  public issuesToLink = [];

  public typicals = [];
  public attributes = [];
  public priorities = [
    { title: "Low", type: Priorities.Low },
    { title: "Medium", type: Priorities.Medium },
    { title: "High", type: Priorities.High },
    { title: "Critical", type: Priorities.Critical }
  ];

  public investigationStatus = [
    { title: "Identified", type: InvestigationStatus.Identified },
    { title: "Accepted", type: InvestigationStatus.Accepted },
    { title: "Deferred", type: InvestigationStatus.Deferred },
    { title: "Evaluated", type: InvestigationStatus.Evaluated }
  ];

  public statusTypes = [
    { title: "To Do", type: StatusType.ToDo },
    { title: "In Progress", type: StatusType.InProgress },
    { title: "Done", type: StatusType.Done }
  ];

  public changeStatus = [
    { title: "Submitted", type: ChangeStatus.Submitted },
    { title: "Studied", type: ChangeStatus.Studied },
    { title: "Reviewed", type: ChangeStatus.Reviewed },
    { title: "PMO Approved", type: ChangeStatus.PMOApproved },
    { title: "Security Reviewed", type: ChangeStatus.SecurityReviewed },
    { title: "QA Reviewed", type: ChangeStatus.QAReviewed },
    { title: "In Development", type: ChangeStatus.InDevelopment }
  ];

  public types = [
    { title: "Software", type: Type.Software },
    { title: "Cosmetic", type: Type.Cosmetic },
    { title: "Performance", type: Type.Performance },
    { title: "Database", type: Type.Database },
    { title: "Configuration", type: Type.Configuration },
    { title: "Environmental", type: Type.Environmental },
    { title: "Documentation", type: Type.Documentation }
  ];

  public severity = [
    { title: "Low", type: Severity.Low },
    { title: "Medium", type: Severity.Medium },
    { title: "High", type: Severity.High },
    { title: "Immediate", type: Severity.Immediate }
  ];

  public issueTypeFields = [];
  public issueType;

  @Input() issueTypes;

  @Input()
  set openDetails(openDetails) {
    this._openDetails = openDetails;
  }

  @Input() disableClose;

  @Input()
  set sprints(sprints) {
    this._sprints = sprints.filter(sp => sp.elType === "Sprint");
  }

  @Input()
  set _issue(_issue) {
    this.issue = _.cloneDeep(_issue);
    const catalog = this.sessionService.getDefaultSettings().catalog;
    this.selectedPriority = this.priorities.find(
      priority => priority.type === this.issue.priority
    );
    this.selectedSeverity = this.severity.find(
      sev => sev.type === this.issue.severity
    );
    if (this.issue.typicalId && this.issue.typicalId !== Guid.empty) {
      this.processManagementService
        .getTypical(this.issue.typicalId)
        .subscribe((res: any) => {
          if (res) {
            this.selectedTypical = res;
            this.designService
              .SearchAttributesByCatalogIdAndTypicalName(
                "",
                catalog && catalog.id ? catalog.id : Guid.empty,
                res.title
              )
              .subscribe(res => {
                if (res) {
                  this.attributes = res;
                }
              });
          }
        });
    } else {
      this.selectedTypical = null;
    }

    this.designService
      .searchTypicals("", catalog && catalog.id ? catalog.id : Guid.empty)
      .subscribe(typicals => {
        if (typicals) {
          this.typicals = typicals;
        }
      });

    if (
      this.issue.attribute !== "" &&
      this.issue.typicalId &&
      this.issue.typicalId !== Guid.empty
    ) {
      this.processManagementService
        .getTypicalAttribute(this.issue.typicalId, this.issue.attribute)
        .subscribe((res: any) => {
          if (res) {
            this.selectedAttribute = { ...res, Name: res.name };
          } else {
            this.selectedAttribute = null;
          }
        });
    } else {
      this.selectedAttribute = null;
    }

    if (this.issue.processId && this.issue.processId !== Guid.empty && this.permissionService.hasPermission("/api/Process/Browse/GetProcess")) {
      this.processManagementService
        .getProcess(this.issue.processId)
        .subscribe(process => {
          if (process) {
            this.selectedRequirement = process;
          }
        });
    } else {
      this.selectedRequirement = null;
    }

    if (this.issue.componentId && this.issue.componentId !== Guid.empty && this.permissionService.hasPermission("/api/Process/Browse/GetHierarchy")) {
      this.processManagementService
        .getHierarchy(this.issue.componentId)
        .subscribe(hierarchy => {
          if (hierarchy) {
            this.selectedComponent = hierarchy;
          }
        });
    } else {
      this.selectedComponent = null;
    }

    this.processManagementService
      .getIssueResources(this.issue.id)
      .subscribe((resources: any) => {
        if (resources) {
          this.resources = resources.map(resource => ({
            ...resource,
            fileName: resource.title
          }));
        }
      });

    if (this.issue.processId && this.issue.processId !== Guid.empty && this.permissionService.hasPermission("/api/Process/Browse/GetProcessWorkflows")) {
      this.processManagementService
        .getProcessWorkflows(this.issue.processId)
        .subscribe(reqWorkflows => {
          if (reqWorkflows) {
            this.reqWorkflows = reqWorkflows;
          }
        });
    } else {
      this.reqWorkflows = [];
    }
    if (this.permissionService.hasPermission("/api/Process/Browse/GetIssueWorkflows"))
    this.processManagementService
      .getIssueWorkflows(this.issue.id)
      .subscribe(res => {
        if (res && res.length) {
          this.issueWorkflows = res.map(issWf => ({
            ...issWf,
            title: issWf.workflowTitle
          }));
        } else {
          this.issueWorkflows = [];
        }
      });
    let subProjectId = this.sessionService.getSubProject().id;
    this.selectedReporter = this.people.find(
      person => person.id === this.issue.reporterId
    );
    this.selectedAssignee = this.people.find(
      person => person.id === this.issue.assignedId
    );
    this.selectedApprover = this.people.find(
      person => person.id === this.issue.approvedId
    );
    this.selectedSprint = this._sprints.find(
      sp => sp.id === this.issue.sprintId
    );
    this.selectedInvestigationStatus = this.investigationStatus.find(
      status => status.type === this.issue.investigationStatus
    );
    this.selectedType = this.types.find(type => type.type === this.issue.type);
    this.selectedChangeStatus = this.changeStatus.find(
      status => status.type === this.issue.changeStatus
    );
    this.submittedDate =
      this.issue.dateSubmitted === "0001-01-01T00:00:00"
        ? null
        : new Date(this.issue.dateSubmitted);
    this.approvalDate =
      this.issue.approvalDate === "0001-01-01T00:00:00"
        ? null
        : new Date(this.issue.approvalDate);
    this.startDate =
      this.issue.startDate === "0001-01-01T00:00:00"
        ? null
        : new Date(this.issue.startDate);
    this.dueDate =
      this.issue.dueDate === "0001-01-01T00:00:00"
        ? null
        : new Date(this.issue.dueDate);
    this.selectedStatusType = this.statusTypes.find(
      type => type.type === this.issue.status
    );
    this.issueType = this.issueTypes.find(
      type => type.id === this.issue.issueTypeId
    );

    this.processManagementService
      .getIssues(subProjectId)
      .subscribe((issues: any) => {
        if (this.permissionService.hasPermission("/api/Process/Browse/GetLinkedIssues"))
        this.processManagementService
          .getLinkedIssues(this.issue.id)
          .subscribe((linkedIssues: any) => {
            if (linkedIssues) {
              this.linkedIssues = linkedIssues.map(iss => ({
                ...iss,
                id: iss.issueId
              }));
              this.tcrIssuesToShow = issues.filter(
                iss =>
                  iss.id !== this.issue.id &&
                  !linkedIssues.find(linkedIss => linkedIss.issueId === iss.id)
              );
              this.allTcrIssues = this.tcrIssuesToShow;
            }
          });
      });
    this.openLinkIssue = false;
    this.issuesToLink = [];

    this.processManagementService
      .getIssueTypeFields(this.issue.issueTypeId)
      .subscribe((res: any) => {
        if (res) {
          this.issueTypeFields = res.map(el => ({
            ...el,
            name: el.issueTypeFieldsName
          }));
        }
      });
  }

  constructor(
    private processManagementService: ProcessManagementService,
    private designService: DesignService,
    private sessionService: SessionService,
    private messageService: MessageService,
    private permissionService: PermissionService
  ) {}

  ngOnInit() {}

  closeDetails() {
    this.detailsClosed.emit();
  }

  onClickOpenLinkIssue() {
    this.openLinkIssue = true;
  }

  shouldDisplay(name) {
    return (
      this.issueTypeFields &&
      this.issueTypeFields.find(el => el.name === name && el.checked)
    );
  }

  linkIssues() {
    for (let i = 0; i < this.issuesToLink.length; i++) {
      this.processManagementService
        .linkIssue(this.issue.id, 0, this.issuesToLink[i].id)
        .subscribe(res => {
          if (res && i === this.issuesToLink.length - 1) {
            this.linkedIssues = this.linkedIssues.concat(this.issuesToLink);
            this.tcrIssuesToShow = this.tcrIssuesToShow.filter(
              issue => !this.issuesToLink.find(iss => iss.id === issue.id)
            );
            this.allTcrIssues = this.tcrIssuesToShow;
            this.cancelLinkIssues();
          }
        });
    }
  }

  unlinkIssueClicked(issue) {
    this.processManagementService
      .unlinkIssue(this.issue.id, issue.id)
      .subscribe(res => {
        if (res) {
          this.linkedIssues = this.linkedIssues.filter(
            iss => iss.id !== issue.id
          );
          this.tcrIssuesToShow = this.tcrIssuesToShow.concat(issue);
          this.allTcrIssues = this.tcrIssuesToShow;
        }
      });
  }

  cancelLinkIssues() {
    this.openLinkIssue = false;
    this.issuesToLink = [];
  }

  changeStatusType(statusType) {
    this.selectedStatusType = statusType;
    this.issue.status = statusType.type;
    if (this.issue.issueSprint) {
      this.processManagementService
        .updateSprintIssue(
          this.issue.issueSprint.id,
          this.issue.issueSprint.sprintId,
          this.issue.issueSprint.issueId,
          statusType.type
        )
        .subscribe(res => {
          if (res) {
            this.issue.issueSprint.status = statusType.type;
            this.updateIssue();
          }
        });
    } else {
      if (statusType.type === StatusType.Done)
        this.removeIssueFromBacklog.emit(this.issue);
      this.updateIssue();
    }
  }

  format(date) {
    let month = date.getUTCMonth() + 1;
    let day = date.getUTCDate();
    let year = date.getUTCFullYear();

    return month + "/" + day + "/" + year;
  }

  titleClicked() {
    this.isTitleClicked = true;
    let titleTextarea = document.getElementsByClassName(
      "title-textarea"
    )[0] as any;
    setTimeout(() => {
      titleTextarea.focus();
    }, 150);
  }

  approvalDateClicked() {
    this.isApprovalDateClicked = true;
    let approvalDate = document.getElementsByClassName(
      "approvalDate"
    )[0] as any;
    let input = approvalDate.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  onApprovalDateFocusout() {
    this.isApprovalDateClicked = false;
    this.updateIssue();
  }

  onChangeApprovalDate(event) {
    this.approvalDate = event;
    this.issue.approvalDate = event ? event.toJSON() : "0001-01-01T00:00:00";
    this.isApprovalDateClicked = false;
    this.updateIssue();
  }

  startDateClicked() {
    this.isStartDateClicked = true;
    let startDate = document.getElementsByClassName("startDate")[0] as any;
    let input = startDate.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  onStartDateFocusout() {
    this.isStartDateClicked = false;
    this.updateIssue();
  }

  onChangeStartDate(event) {
    this.startDate = event;
    this.issue.startDate = event ? event.toJSON() : "0001-01-01T00:00:00";
    this.isStartDateClicked = false;
    this.updateIssue();
  }

  dueDateClicked() {
    this.isDueDateClicked = true;
    let dueDate = document.getElementsByClassName("dueDate")[0] as any;
    let input = dueDate.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  onDueDateFocusout() {
    this.isDueDateClicked = false;
    this.updateIssue();
  }

  onChangeDueDate(event) {
    this.dueDate = event;
    this.issue.dueDate = event ? event.toJSON() : "0001-01-01T00:00:00";
    this.isDueDateClicked = false;
    this.updateIssue();
  }

  changePriority(event) {
    this.selectedPriority = event;
    this.issue.priority = event ? event.type : "";
    this.isPriorityClicked = false;
    this.updateIssue();
  }

  changeSeverity(event) {
    this.selectedSeverity = event;
    this.issue.severity = event ? event.type : "";
    this.isSeverityClicked = false;
    this.updateIssue();
  }

  priorityClicked() {
    this.isPriorityClicked = true;
    let priorityField = document.getElementsByClassName("priority")[0] as any;
    let input = priorityField.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  typeClicked() {
    this.isTypeClicked = true;
    let typeField = document.getElementsByClassName("type")[0] as any;
    let input = typeField.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  implicationClicked() {
    this.isImplicationClicked = true;
    let implicationTextare = document.getElementsByClassName(
      "implication-textarea"
    )[0] as any;
    setTimeout(() => {
      implicationTextare.focus();
    }, 150);
  }

  testCaseIdClicked() {
    this.isTestCaseIdClicked = true;
    let textarea = document.getElementsByClassName(
      "testcaseid-textarea"
    )[0] as any;
    setTimeout(() => {
      textarea.focus();
    }, 150);
  }


  commentClicked() {
    this.isCommentClicked = true;
    let commentTextarea = document.getElementsByClassName(
      "comment-textarea"
    )[0] as any;
    setTimeout(() => {
      commentTextarea.focus();
    }, 150);
  }

  changeComment(event) {
    const comment = event.target.value.trim("");
    this.issue.comment = comment;
  }

  changeChangeStatus(event) {
    this.selectedChangeStatus = event;
    this.issue.changeStatus = event ? event.type : "";
    this.isChangeStatusClicked = false;
    this.updateIssue();
  }

  changeStatusClicked() {
    this.isChangeStatusClicked = true;
    let changeStatusField = document.getElementsByClassName(
      "change-status"
    )[0] as any;
    let input = changeStatusField.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  onChangeStatusFocusout() {
    this.isChangeStatusClicked = false;
    this.updateIssue();
  }

  changeImplication(event) {
    const implication = event.target.value.trim("");
    this.issue.implication = implication;
  }

  changeTestCaseId(event) {
    const testCaseId = event.target.value.trim("");
    this.issue.testCaseId = testCaseId;
  }

  onCommentFocusout() {
    this.isCommentClicked = false;
    this.updateIssue();
  }

  onImplicationFocusout() {
    this.isImplicationClicked = false;
    this.updateIssue();
  }

  onTestCaseIdFocusout() {
    this.isTestCaseIdClicked = false;
    this.updateIssue();
  }

  changeType(event) {
    this.selectedType = event;
    this.issue.type = event ? event.type : "";
    this.isTypeClicked = false;
    this.updateIssue();
  }

  onTypeFocusout() {
    this.isTypeClicked = false;
    this.updateIssue();
  }

  changeReason(event) {
    const reason = event.target.value.trim("");
    this.issue.reason = reason;
  }

  onReasonFocusout() {
    this.isReasonClicked = false;
    this.updateIssue();
  }

  severityClicked() {
    this.isSeverityClicked = true;
    let severityField = document.getElementsByClassName("severity")[0] as any;
    let input = severityField.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  unlinkRequirement() {
    this.processManagementService.unlinkIssueFromProcess(this.issue.id).subscribe(res => {
      if (res) {
        this.processManagementService.issueReqWorkflowUpdated.next();
        this.selectedRequirement = null;
        this.selectedComponent = null;
        this.issue.processId = Guid.empty;
        this.issue.componentId = Guid.empty;
        this.reqWorkflows = [];
        this.issueWorkflows = [];
        this.updateIssue();
      }
    });
  }

  async updateIssue() {
    let subProjectId = this.sessionService.getSubProject().id;
    if(this.permissionService.hasPermission("/api/Process/Browse/UpdateIssue")){
      const res = await this.processManagementService
      .updateIssue(
        this.issue.id,
        this.selectedRequirement ? this.selectedRequirement.id : Guid.empty,
        subProjectId,
        this.issue.issueTypeId,
        this.issue.assignedId ? this.issue.assignedId : Guid.empty,
        this.issue.title,
        this.issue.description,
        !isNullOrUndefined(this.issue.status) ? this.issue.status : 0,
        this.issue.estimate ? this.issue.estimate : 0,
        this.issue.reason,
        this.issue.implication,
        this.issue.comment,
        this.issue.typicalId ? this.issue.typicalId : Guid.empty,
        this.issue.attribute ? this.issue.attribute : "",
        this.issue.approvedForInvestigation,
        !isNullOrUndefined(this.issue.investigationStatus)
          ? this.issue.investigationStatus
          : -1,
        !isNullOrUndefined(this.issue.changeStatus)
          ? this.issue.changeStatus
          : -1,
        this.issue.reporterId ? this.issue.reporterId : Guid.empty,
        this.issue.approvedId ? this.issue.approvedId : Guid.empty,
        this.submittedDate
          ? this.submittedDate.toJSON()
          : "0001-01-01T00:00:00",
        this.approvalDate ? this.approvalDate.toJSON() : "0001-01-01T00:00:00",
        this.startDate ? this.startDate.toJSON() : "0001-01-01T00:00:00",
        !isNullOrUndefined(this.issue.priority) ? this.issue.priority : -1,
        !isNullOrUndefined(this.issue.severity) ? this.issue.severity : -1,
        this.dueDate ? this.dueDate.toJSON() : "0001-01-01T00:00:00",
        !isNullOrUndefined(this.issue.type) ? this.issue.type : -1,
        this.issue.componentId ? this.issue.componentId : Guid.empty,
        this.issue.testCaseId ? this.issue.testCaseId : ""
      )
      .toPromise();
    }
    

    this.issueUpdated.emit(this.issue);
  }

  onTitleFocusout() {
    this.isTitleClicked = false;
    this.updateIssue();
  }

  onPriorityFocusout() {
    this.isPriorityClicked = false;
    this.updateIssue();
  }

  onSeverityFocusout() {
    this.isSeverityClicked = false;
    this.updateIssue();
  }

  descriptionClicked() {
    this.isDescriptionClicked = true;
    let descriptionTextarea = document.getElementsByClassName(
      "description-textarea"
    )[0] as any;
    setTimeout(() => {
      descriptionTextarea.focus();
    }, 150);
  }

  reasonClicked() {
    this.isReasonClicked = true;
    let reasonTextarea = document.getElementsByClassName(
      "reason-textarea"
    )[0] as any;
    setTimeout(() => {
      reasonTextarea.focus();
    }, 150);
  }

  applicationClicked() {
    this.isApplicationClicked = true;
    let application = document.getElementsByClassName("application")[0] as any;
    let input = application.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  reporterClicked() {
    this.isReporterClicked = true;
    let reporter = document.getElementsByClassName("reporter")[0] as any;
    let input = reporter.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  onReporterFocusout() {
    this.isReporterClicked = false;
    this.updateIssue();
  }

  attributeClicked() {
    this.isAttributeClicked = true;
    let attribute = document.getElementsByClassName("attribute")[0] as any;
    let input = attribute.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  onApplicationFocusout() {
    this.isApplicationClicked = false;
    this.updateIssue();
  }

  onAttributeFocusout() {
    this.isAttributeClicked = false;
    this.updateIssue();
  }

  onDescriptionFocusout() {
    this.isDescriptionClicked = false;
    this.updateIssue();
  }

  changeTypical(typical) {
    const catalog = this.sessionService.getDefaultSettings().catalog;
    this.selectedTypical = typical;
    this.issue.typicalId = typical ? typical.id : Guid.empty;
    this.selectedAttribute = null;
    if (typical)
      this.designService
        .SearchAttributesByCatalogIdAndTypicalName(
          "",
          catalog && catalog.id ? catalog.id : Guid.empty,
          typical ? typical.title : ""
        )
        .subscribe(attributes => {
          if (attributes) {
            this.attributes = attributes;
          }
        });
    this.isApplicationClicked = false;
  }

  handleChange(search) {
    const catalog = this.sessionService.getDefaultSettings().catalog;
    this.designService
      .searchTypicals(search, catalog && catalog.id ? catalog.id : Guid.empty)
      .subscribe(typicals => {
        this.typicals = typicals;
        this.updateIssue();
      });
  }

  handleChangeAttributes(search) {
    if (!this.selectedTypical) {
      return;
    }

    const catalog = this.sessionService.getDefaultSettings().catalog;
    this.designService
      .SearchAttributesByCatalogIdAndTypicalName(
        search,
        catalog && catalog.id ? catalog.id : Guid.empty,
        this.selectedTypical.title
      )
      .subscribe(attributes => {
        if (attributes) {
          this.attributes = attributes;
        }
      });
  }

  changeAttribute(event) {
    this.selectedAttribute = event;
    this.issue.attribute = event ? event.Name : "";
    this.isAttributeClicked = false;
    this.updateIssue();
  }

  changeAssignee(event) {
    if (event) {
      this.selectedAssignee = event;
      this.issue.assignedId = event ? event.id : Guid.empty;
      this.issue.assigne = event;
    }
    this.isAssigneeClicked = false;
    this.updateIssue();
    this.assigneeReload.emit(this.issue);
  }

  assigneeClicked() {
    this.isAssigneeClicked = true;
    let assignee = document.getElementsByClassName("assignee")[0] as any;
    let input = assignee.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  estimateClicked() {
    this.isEstimateClicked = true;
    let estimateField = document.getElementById("estimate");
    setTimeout(() => {
      estimateField.focus();
    }, 100);
  }

  investigationStatusClicked() {
    this.isInvestigationStatusClicked = true;
    let investigationField = document.getElementsByClassName(
      "investigation"
    )[0] as any;
    let input = investigationField.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  onAssigneeFocusout() {
    this.isAssigneeClicked = false;
    this.updateIssue();
  }

  onApproverFocusout() {
    this.isApproverClicked = false;
    this.updateIssue();
  }

  changeApprover(event) {
    this.selectedApprover = event;
    this.issue.approvedId = event ? event.id : Guid.empty;
    this.isApproverClicked = false;
    this.updateIssue();
  }

  changeEstimate(event) {
    const estimate = event.target.value.trim("");
    this.issue.estimate = estimate;
  }

  onEstimateFocusout() {
    this.isEstimateClicked = false;
    this.updateIssue();
  }

  onChangeDescription(event) {
    let description = event.target.value.trim("");
    this.issue.description = description;
  }

  onChangeTitle(event) {
    let title = event.target.value.trim("");
    this.issue.title = title;
  }

  onInvestigationStatusFocusout() {
    this.isInvestigationStatusClicked = false;
    this.updateIssue();
  }

  changeApprovedForInvestigation(event) {
    this.issue.approvedForInvestigation = event.target.checked;
    this.updateIssue();
  }

  approverClicked() {
    this.isApproverClicked = true;
    let approver = document.getElementsByClassName("approver")[0] as any;
    let input = approver.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  sprintClicked() {
    this.isSprintClicked = true;
    let sprint = document.getElementsByClassName("sprint")[0] as any;
    let input = sprint.getElementsByTagName("input")[0];
    setTimeout(() => {
      input.focus();
    }, 150);
  }

  downloadResource(resource) {
    this.processManagementService.downloadIssueResource(resource.id).subscribe(
      (x: any) => {
        const newBlob = new Blob([x], { type: "application/vxml" });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement("a");
        link.href = data;
        link.download = resource.fileName;
        link.dispatchEvent(
          new MouseEvent("click", {
            bubbles: true,
            cancelable: true,
            view: window
          })
        );
        setTimeout(function() {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      },
      error => {
        this.messageService.sendMessage({
          text: "There was an error during the download of the file!",
          type: MessageType.Error
        });
      }
    );
  }

  async changeSprint(event) {
    if (!this.issue.sprintId && !event) {
      this.isSprintClicked = false;
      return;
    }

    if (!this.issue.sprintId && event) {
      if (this.permissionService.hasPermission("/api/Process/Sprint/AddSprintIssue"))
      this.processManagementService
        .addSprintIssue(event.id, this.issue.id, this.issue.status)
        .subscribe((res: any) => {
          if (res) {
            this.issue.sprintId = event.id;
            this.issue.issueSprint = {
              id: res.id,
              sprintId: event.id,
              issueId: this.issue.id,
              status: this.issue.status
            };
            this.updateSprint(event, true, false);
          }
        });
    } else if (event) {
      if (this.permissionService.hasPermission("/api/Process/Sprint/UpdateSprintIssue"))
      this.processManagementService
        .updateSprintIssue(
          this.issue.issueSprint.id,
          event.id,
          this.issue.id,
          this.issue.issueSprint.status
        )
        .subscribe(res => {
          if (res) {
            this.issue.sprintId = event.id;
            this.issue.issueSprint.sprintId = event.id;
            this.updateSprint(event, false, false);
          }
        });
    } else {
      if (this.permissionService.hasPermission("/api/Process/Sprint/DeleteSprintIssue"))
      await this.processManagementService
        .deleteSprintIssue(this.issue.issueSprint.id)
        .subscribe(res => {
          if (res) delete this.issue.sprintId;
          delete this.issue.issueSprint;
          this.updateSprint(event, false, true);
        });
    }
  }

  updateSprint(event, moveToSprint, moveToBacklog) {
    this.selectedSprint = event;
    this.isSprintClicked = false;
    this.sprintUpdated.emit({
      moveToSprint: moveToSprint,
      moveToBacklog: moveToBacklog,
      issue: this.issue
    });
  }

  onSprintFocusout() {
    this.isSprintClicked = false;
  }

  openRequirement() {
    this.openRequirementDialog = true;
  }

  selectRequirementSubmited(requirementObj) {
    this.selectedRequirement = requirementObj.process;
    this.selectedComponent = requirementObj.component;
    this.issue.processId = requirementObj.process.id;
    this.issue.componentId = requirementObj.component.id;
    this.closeRequirementDialog();
    this.processManagementService
      .getProcessWorkflows(requirementObj.process.id)
      .subscribe(issueWf => {
        if (issueWf) {
          this.reqWorkflows = issueWf;
        }
      });
    this.updateIssue();
  }

  closeRequirementDialog() {
    this.openRequirementDialog = false;
  }

  isChecked(workflow) {
    return this.issueWorkflows.find(
      wf => wf.workflowId === workflow.workflowId
    );
  }

  affectedWfChecked(workflow, event) {
    const isChecked = event.target.checked;
    if (isChecked) {
      this.processManagementService
        .addWorkflowIssue(workflow.workflowId, this.issue.id)
        .subscribe(() => {
          this.processManagementService.issueReqWorkflowUpdated.next();
          this.issueWorkflows.push(workflow);
        });
    } else {
      this.processManagementService
        .unlinkWorkflowIssue(workflow.workflowId, this.issue.id)
        .subscribe(() => {
          this.processManagementService.issueReqWorkflowUpdated.next();
          this.issueWorkflows = this.issueWorkflows.filter(
            wf => wf.workflowId !== workflow.workflowId
          );
        });
    }
  }

  changeInvestigationStatus(event) {
    this.selectedInvestigationStatus = event;
    this.issue.investigationStatus = event ? event.type : "";
    this.isInvestigationStatusClicked = false;
    this.updateIssue();
  }

  handleChangeIssueType(search) {
    if (!search || search.trim("") === "") {
      this.tcrIssuesToShow = this.allTcrIssues;
    } else {
      this.tcrIssuesToShow = this.allTcrIssues.filter(issue =>
        issue.title.toLowerCase().includes(search.trim("").toLowerCase())
      );
    }
  }

  onDeleteClicked(fileId) {
    this.processManagementService.deleteIssueResource(fileId).subscribe(res => {
      if (res) {
        this.resources = this.resources.filter(file => file.id !== fileId);
      }
    });
  }

  onFileSelected(event) {
    const fileList = event.target.files;
    Object.keys(fileList).forEach(key => {
      fileList[key].fileName = fileList[key].name;
      this.processManagementService
        .addIssueResources(this.issue.id, [fileList[key]])
        .subscribe(res => {
          fileList[key].id = res[0].id;
          this.resources.push(fileList[key]);
        });
      if (key === fileList.length) event.target.value = "";
    });
  }

  onCloneClicked() {
    this.cloneClicked.emit({
      issue: this.issue,
      resources: this.resources,
      issueWorkflows: this.issueWorkflows,
      linkedIssues: this.linkedIssues
    });
  }
}
