import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { SessionService } from 'src/app/core/services/session.service';
import { JiraIntegrationService } from 'src/app/process-managment/services/jira-integration.service';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { Guid } from 'src/app/shared/guid';

@Component({
  selector: 'app-ji-project-map',
  templateUrl: './ji-project-map.component.html',
  styleUrls: ['./ji-project-map.component.css']
})
export class JiProjectMapComponent implements OnInit, OnChanges {
  @Input() currentIndex: number;
  @Output() nextStep = new EventEmitter<boolean>();
  @Output() savedProjectMapping = new EventEmitter<{ id: string, jiraKey: string, subProjectId: string }>();
  integration: { projectId?: string, subprojectId?: string, jiraProjectKey?: string, jiraProjectName?: string } = {};

  isLoading = false;
  project: any;
  subproject: any;
  jiraProjects: any[] = [];

  hierarchyList = [];
  lastSelectedHiearchyOption: string;

  // Saved values
  savedId: string;
  savedModel: any;
  hierarchySavedValues: HierarchyModel;
  isInEditMode = false;
  savingProject = false;
  mapHierarchies = false;
  editingHierarchies = false;
  savedHierarchy: { hierarchyTypeId: string, hierarchyParentId: string };
  savedProject: string;
  constructor(private jiraService: JiraIntegrationService,
              private sessionService: SessionService,
              private processService: ProcessManagementService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const sp: SimpleChange = changes.currentIndex;
    if (sp && sp.currentValue === 1) {
      this.isInEditMode = false;
      this.editingHierarchies = false;
      this.project = this.sessionService.getProject();
      this.subproject = this.sessionService.getSubProject();
      this.getMappedProject();
      if (this.subproject && this.subproject.id) {
        this.GetHierarchyTypes(this.subproject.id);
        this.getJiraProjects(this.subproject.id);
      }
    }
  }


  getMappedProject() {
    this.isLoading = true;
    this.jiraService.GetJiraProjectMapping(this.subproject.id)
      .subscribe(
        (res) => {
          if (res) {
            this.savedModel = res;
            this.integration.jiraProjectKey = res.jiraProjectKey;
            this.savedId = res.id;
            // tslint:disable-next-line:max-line-length
            this.hierarchySavedValues = new HierarchyModel(res.mapHierarchy, res.hierarchyParentId, res.hierarchyTypeId, res.hierarchyParentName, res.hierarchyTypeName);
            this.mapHierarchies = res.mapHierarchy;
            if (res.jiraProjectKey) {
              this.savedProject = res.jiraProjectKey;
              this.isInEditMode = true;
              this.savedProjectMapping.emit(this.createConfig(res));
            }
          } else {
            this.isInEditMode = false;
          }
          this.isLoading = false;
        }
      );
  }

  getJiraProjects(subproject) {
    this.jiraService.GetJiraProjects(subproject)
      .subscribe(
        (res) => {this.jiraProjects = res || []; }
      );
  }

  saveProjectMap() {
    const lastHierarchyLevelId = this.hierarchyList && this.hierarchyList.length > 0 ?
      this.hierarchyList[this.hierarchyList.length - 1].id
      :
      null;
    this.integration.projectId = this.project.id;
    this.integration.subprojectId = this.subproject.id;
    const jiraProject = this.jiraProjects.find(x => x.projectId == this.integration.jiraProjectKey)
    this.integration.jiraProjectName = jiraProject.projectName;
    const jiraProjectId = jiraProject.projectId;
    this.jiraService.AddJiraProjectMapping(
      this.integration.projectId,
      this.integration.subprojectId,
      this.integration.jiraProjectKey,
      this.integration.jiraProjectName,
      this.mapHierarchies,
      this.lastSelectedHiearchyOption || Guid.empty,
      lastHierarchyLevelId || Guid.empty)
      .subscribe(
        (res) => {
          if (res) {
            res.jiraProjectKey = jiraProjectId;
            this.savedProjectMapping.emit(this.createConfig(res));
            this.nextStep.emit();
          }
        }
      );
  }

  editProjectMap() {
    const lastHierarchyLevelId = this.hierarchyList && this.hierarchyList.length > 0 ?
      this.hierarchyList[this.hierarchyList.length - 1].id
      :
      null;
    this.integration.projectId = this.project.id;
    this.integration.subprojectId = this.subproject.id;
    this.integration.jiraProjectName = this.jiraProjects.find(x => x.projectId == this.integration.jiraProjectKey).projectName;

    this.jiraService.UpdateJiraProjectMapping(
      this.savedId,
      this.integration.projectId,
      this.integration.subprojectId,
      this.integration.jiraProjectKey,
      this.integration.jiraProjectName,
      this.mapHierarchies,
      // tslint:disable-next-line:max-line-length
      this.editingHierarchies ? this.lastSelectedHiearchyOption || Guid.empty : this.hierarchySavedValues.hierarchyParentId,
      // tslint:disable-next-line:max-line-length
      this.editingHierarchies ? lastHierarchyLevelId || Guid.empty : this.hierarchySavedValues.hierarchyTypeId
      )
      .subscribe(
        (res) => {
          if (res) {
            const editedItem = { id: this.savedId, jiraProjectKey: this.integration.jiraProjectKey };
            this.savedProjectMapping.emit(this.createConfig(editedItem));
            this.nextStep.emit();
          }
        }
      );
  }

  async GetHierarchyTypes(subprojectId: string) {
    this.processService.getHierarchyTypes(subprojectId)
      .subscribe(
        async (res) => {
          this.hierarchyList = res || [];
          if (this.hierarchyList.length > 0) {
            this.hierarchyList[0].HirearchyOptions = await this.getHierarchiesByParent(subprojectId);
          }
        }
      );
  }

  async onGetHierarchiesByParent(hierarchyTypeId, parentId, nextHirearchy, index) {
    this.lastSelectedHiearchyOption = parentId;
    for (let i = index + 1; i < this.hierarchyList.length; i++) {
      this.hierarchyList[i].selectedOption = undefined;
      this.hierarchyList[i].HirearchyOptions = undefined;
     }
    this.hierarchyList.find(x => x.id == nextHirearchy.id).HirearchyOptions =
     await this.getHierarchiesByParent(this.subproject.id, hierarchyTypeId, parentId);
  }

  async getHierarchiesByParent(subprojectId, hierarchyTypeId = Guid.empty, parentId = Guid.empty) {
    return await this.processService.getHierarchiesByParent(subprojectId, hierarchyTypeId, parentId).toPromise();
  }


  setEditHierarchyEditModeOn() {
    this.editingHierarchies = true;
  }

  setEditHierarchyEditModeOff() {
    this.lastSelectedHiearchyOption = undefined;
    this.editingHierarchies = false;
  }

  discard() {
    // this.integration.jiraProjectKey = this.originalSavedProject;
    this.nextStep.emit();
  }

  navigateForward() {
    this.nextStep.emit();
  }


  // Create config object that will be passed to other components on wizard.
  private createConfig(response): { id: string, jiraKey: string, subProjectId: string } {
    const config: { id: string, jiraKey: string, subProjectId: string } = {
      id: response.id,
      jiraKey: response.jiraProjectKey,
      subProjectId: this.subproject.id
    };
    return config;
  }

  public get isValidToBeSaved(): boolean {
    return this.integration.jiraProjectKey !== undefined;
  }

  public get IsProjectEdited(): boolean {
    return this.savedModel.jiraProjectKey !== this.integration.jiraProjectKey;
  }

  public get validateHiearchies(): boolean {
    if (this.mapHierarchies === false) {
      return true;
    } else {
      if (this.editingHierarchies) {
        const tmp = JSON.parse(JSON.stringify(this.hierarchyList));
        tmp.pop();
        if (tmp.find(x => !x.selectedOption)) {
          return false;
        } else {
          return true;
        }
      } else {
        return true;
      }
    }
  
  }
}

class HierarchyModel {
  mapHierarchy: boolean;
  hierarchyParentId: string;
  hierarchyTypeId: string;
  hierarchyParentName: string;
  hierarchyTypeName: string;

  constructor(mapHierarchy, hierarchyParentId, hierarchyTypeId, hierarchyParentName, hierarchyTypeName) {
    this.mapHierarchy = mapHierarchy;
    this.hierarchyParentId = hierarchyParentId;
    this.hierarchyTypeId = hierarchyTypeId;
    this.hierarchyParentName = hierarchyParentName;
    this.hierarchyTypeName = hierarchyTypeName;
  }
}
