import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SessionService } from 'src/app/core/services/session.service';
import { JiraIntegrationService } from 'src/app/process-managment/services/jira-integration.service';

@Component({
  selector: 'app-ji-auth',
  templateUrl: './ji-auth.component.html',
  styleUrls: ['./ji-auth.component.css']
})
export class JiAuthComponent implements OnInit {
  @Output() authenticated = new EventEmitter<boolean>();
  private subprojectId: string;
  private username: string;
  isAuthenticating = true;
  user = {
    jiraUsername: '',
    jiraToken: '',
    jiraUrl: '',
  };
  public get isValid(): boolean {
    return this.user.jiraUsername !== '' && this.user.jiraToken !== '' && this.user.jiraUrl !== '';
  }

  constructor(private jiraService: JiraIntegrationService, private session: SessionService) { }

  ngOnInit() {
    this.subprojectId = this.session.getSubProject().id;
    this.username = this.session.getUser().name;

    this.jiraService.HasValidJiraCredentials(this.subprojectId, true)
      .subscribe(
        (res) => {
          if (res === true) {
            this.jiraService.GetJiraUserSubprojectConfigs(this.subprojectId, this.username)
              .subscribe(
                (ures: any) => {
                  if (ures) {
                    this.user = ures;
                    this.authenticated.next(true);
                  }
                  this.isAuthenticating = false;
                },
                (err) => {
                  this.isAuthenticating = false;
                }
              );
          } else {
            this.isAuthenticating = false;
          }
        }
      );
  }

  authenticate() {
    this.isAuthenticating = true;
    const user = this.session.getUser().name;
    const subproject = this.session.getSubProject().id;
    console.log(user, subproject);
    this.jiraService.SetJiraUserSubprojectConfigs(user, subproject, this.user.jiraUsername, this.user.jiraToken, this.user.jiraUrl)
      .subscribe(
        (res) => {
          console.log('RES SetJiraUserSubprojectConfigs: ', res);
          if (res === true) {
            this.authenticated.next(true);
          }
          this.isAuthenticating = false;
        },
        (err) => {
          this.isAuthenticating = false;
        }
      );
  }
}
