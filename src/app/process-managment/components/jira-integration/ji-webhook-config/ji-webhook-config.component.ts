import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { JiraIntegrationService } from 'src/app/process-managment/services/jira-integration.service';

@Component({
  selector: 'app-ji-webhook-config',
  templateUrl: './ji-webhook-config.component.html',
  styleUrls: ['./ji-webhook-config.component.css']
})
export class JiWebhookConfigComponent implements OnInit, OnChanges  {
  // Format of webhook name: {SubProjectId}-SAI-{JiraProjectKey}
  @Input() savedProject: { id: string, jiraKey: string, subProjectId: string };
  @Input() currentIndex: number;
  @Output() nextStep = new EventEmitter<boolean>();
  @Output() prevStep = new EventEmitter<boolean>();

  webhook: any = {};
  events: any[];
  projectName: '';
  isWebhookEnabled: false;
  loading = false;
  constructor(private jiraService: JiraIntegrationService) { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) {
    const sp: SimpleChange = changes.currentIndex;
    if (sp && sp.currentValue) {
      if (+sp.currentValue === 4) {
        this.webhook.name = this.formatWebhookName();
        this.getWebhook(this.savedProject.subProjectId);
        this.getJiraProjectName(this.savedProject.subProjectId, this.savedProject.jiraKey);
      }
    }
  }


  getWebhook(subprojectId) {
    this.loading = true;
    this.jiraService.GetJiraWebhooks(subprojectId)
      .subscribe(
        (res: any) => {
          this.webhook = res;
          this.getWebhookEvents(Boolean(res.self));
          this.webhook.name = res.self ? this.webhook.name : this.formatWebhookName();
          this.loading = false;
          this.isWebhookEnabled = res.enabled;
        }
      );
  }

  getJiraProjectName(subprojectId, jiraProjectKey) {
    this.jiraService.GetJiraProjects(subprojectId)
      .subscribe(
        (res) => {
          if (res) {
            this.projectName = res.find(x => x.projectId == jiraProjectKey).projectName;
          }
        }
      );
  }

  getWebhookEvents(hasSavedWebhook) {
    // TODO: Get these from API when it's ready
    const e = [
      {
        type: 'Issue',
        events: [
          { name: 'Created', key: 'jira:issue_created' },
          { name: 'Updated', key: 'jira:issue_updated' },
          { name: 'Deleted', key: 'jira:issue_deleted' }
        ]
      },
      {
        type: 'Issue Link',
        events: [
          { name: 'Created', key: 'issuelink_created' },
          { name: 'Deleted', key: 'issuelink_deleted' }
        ]
      }
    ];
    e.map((t: any) => {
      t.events = t.events.map((ev: any) => {
        ev.selected = hasSavedWebhook ? this.webhook.events.find(x => x === ev.key) ? true : false : false;
        return ev;
      });
      t.selected = t.events.find((x: any) => x.selected) ? true : false;
      return t;
    });
    this.events = e;
  }

  getSelectedEvents() {
    const events = [];
    this.events.filter((x: any) => x.selected === true)
      .forEach(x => {
        x.events.forEach(ev => {
          events.push(ev.key);
        });
      });
    return events;
  }

  prepareModel() {
    const model = JSON.parse(JSON.stringify(this.webhook));
    model.events = this.getSelectedEvents();
    model.filters = { 'issue-related-events-section': `project="${this.projectName}"` };
    model.url = '';
    return model;
  }

  getUrl(baseUrl: string, subprojectId: string, jiraUrl: string, username: string, token: string) {
    return `${baseUrl}?subprojectId=${subprojectId}&url=${jiraUrl}&username=${username}&token=${token}`;
  }

  formatWebhookName() {
    return this.savedProject ? `${this.savedProject.subProjectId}-SAI-${this.savedProject.jiraKey}-DEV` : undefined;
  }

  goBack() {
    this.prevStep.next();
  }

  goNext() {
    this.nextStep.next();
  }

  save() {
    this.loading = true;
    const w = this.prepareModel();
    this.jiraService.SaveWebhook(this.savedProject.subProjectId, w.name, w.events, w.filters, w.url)
      .subscribe(
        (res) => {
          this.loading = false;
          this.getWebhook(this.savedProject.subProjectId);
        },
        (err) => {
          this.loading = false;
        }
      );
  }

  update() {
    this.loading = true;
    const w = this.prepareModel();
    this.jiraService.UpdateWebhook(this.savedProject.subProjectId, w.name, w.events, w.filters, w.url, w.self, this.isWebhookEnabled)
      .subscribe(
        (res) => {
          this.loading = false;
        },
        (err) => {
          this.loading = false;
        }
      );
  }

  delete() {
    // TODO: Implement when API is ready
    console.error('Not implemented!');
    // this.loading = true;
    // const w = this.prepareModel();
    // this.jiraService.UpdateWebhook(w)
    //   .subscribe(
    //     (res) => {
    //       console.log(res);
    //       this.getWebhook(this.webhook.name);
    //     },
    //     (err) => {
    //       this.loading = false;
    //       console.log(err);
    //     }
    //   );
  }
}
