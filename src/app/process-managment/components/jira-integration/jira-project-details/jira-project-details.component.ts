import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/core/services/session.service';
import { JiraIntegrationService } from 'src/app/process-managment/services/jira-integration.service';

@Component({
  selector: 'app-jira-project-details',
  templateUrl: './jira-project-details.component.html',
  styleUrls: ['./jira-project-details.component.css']
})
export class JiraProjectDetailsComponent implements OnInit {
  loadingData = false;
  subproject: any;
  constructor(private jiraService: JiraIntegrationService, private session: SessionService) { }

  ngOnInit() {
    const subproject = this.session.getSubProject();
    if (subproject) {
      this.getSubprojectDetails(subproject.id);
    }
  }

  getSubprojectDetails(id) {
    this.loadingData = true;
    this.jiraService.GetSubProject(id)
      .subscribe(
        (res) => {
          if (res) {
            this.subproject = res;
          }
          this.loadingData = false;
        }
      );
  }
}
