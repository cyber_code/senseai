import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { JiraIntegrationService } from 'src/app/process-managment/services/jira-integration.service';

@Component({
  selector: 'app-ji-user-map',
  templateUrl: './ji-user-map.component.html',
  styleUrls: ['./ji-user-map.component.css']
})
export class JiUserMapComponent implements OnInit, OnChanges {
  @Input() savedProject: { id: string, jiraKey: string, subProjectId: string };
  @Output() nextStep = new EventEmitter<boolean>();
  @Output() prevStep = new EventEmitter<boolean>();
  senseUserList: any[] = [];
  jiraUserList: any[] = [];
  mappingUserList: any[] = [];
  loadingJiraUsers = false;
  loadingSenseUsers = false;
  loadingMappings = false;
  constructor(private jiraService: JiraIntegrationService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    const sp: SimpleChange = changes.savedProject;
    if (sp && sp.currentValue) {
      this.savedProject = sp.currentValue;
      if (this.savedProject.subProjectId) {
        this.getMappings();
        this.getSenseUsers();
        this.getJiraUsers();
      }
    }
  }

  public get _senseUserList(): any[] {
    if (this.senseUserList && this.mappingUserList) {
      return this.senseUserList.filter(x => (this.mappingUserList.findIndex(ml => ml.id && ml.senseaiUserId === x.id) === -1)  );
    } else {
      return this.senseUserList || [] ;
    }
  }

  public get _jiraUserList(): any[] {
    if (this.jiraUserList && this.mappingUserList) {
      return this.jiraUserList.filter(x => (this.mappingUserList.findIndex(ml => ml.id && ml.jiraUserId === x.id) === -1)  );
    } else {
      return this.jiraUserList || [];
    }
  }

  getMappings() {
    this.loadingMappings = true;
    this.jiraService.GetJiraUserMapping(this.savedProject.subProjectId)
      .subscribe(
        (res: any[]) => {
          this.loadingMappings = false;
          if (res) {
            this.mappingUserList = res;
          }
        }
      );
  }

  getJiraUsers() {
    this.loadingJiraUsers = true;
    this.jiraService.GetJiraUsers(this.savedProject.subProjectId)
      .subscribe(
        (res: any[]) => {
          this.loadingJiraUsers = false;
          this.jiraUserList = res;
        }
      );
  }

  getSenseUsers() {
    this.loadingSenseUsers = true;
    this.jiraService.GetPeoples(this.savedProject.subProjectId)
      .subscribe(
        (res: any[]) => {
          this.loadingSenseUsers = false;
          this.senseUserList = res;
        }
      );
  }

  save(userMap) {
    userMap.subprojectId = this.savedProject.subProjectId;
    this.jiraService.AddJiraUserMapping(userMap.subprojectId, userMap.jiraUserId, userMap.senseaiUserId)
      .subscribe(
        (res) => {
          this.getMappings();
        }
      );
  }

  addUserMapping() {
    this.mappingUserList.push({jiraUserId: '', senseaiUserId: ''});
  }


  // If id provided, delete on API
  // If index provided, delete on local array
  removeUserMapping(id, index) {
    if (id) {
      this.jiraService.DeleteJiraUserMapping(id)
      .subscribe(
        (res) => { this.getMappings(); }
      );
    }

    if (index) {
      this.mappingUserList.splice(index, 1);
    }
  }

  public get isAddingOneUser(): boolean {
    return this.mappingUserList.find(x => !x.id && (x.jiraUserId || x.senseaiUserId)) ? true : false;
  }

  goBack() {
    this.prevStep.next();
  }

  saveUserMappings() {
    this.nextStep.next();
  }
}
