// tslint:disable-next-line:max-line-length
import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import * as signalR from '@aspnet/signalr';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { SessionService } from 'src/app/core/services/session.service';
import { JiraIntegrationService } from 'src/app/process-managment/services/jira-integration.service';

@Component({
  selector: 'app-ji-summary',
  templateUrl: './ji-summary.component.html',
  styleUrls: ['./ji-summary.component.css']
})
export class JiSummaryComponent implements OnChanges, OnDestroy {
  summary: any;
  isStartingSync = false;
  syncIssues = true;
  isSyncing = false;
  startedToSync = false;
  private hubConnection: signalR.HubConnection;
  @Input() savedProject: { id: string, jiraKey: string, subProjectId: string };
  @Input() currentIndex: number;
  @Output() nextStep = new EventEmitter<boolean>();
  @Output() prevStep = new EventEmitter<boolean>();
  progressData = [];
  jiraIssue= ["jira:issue_updated", "jira:issue_created", "jira:issue_deleted"];
  issueLink =["issuelink_created", "issuelink_deleted"];

  constructor(private jiraService: JiraIntegrationService,
              private configuration: ConfigurationService,
              private router: Router,
              private sessionService: SessionService) { }

  ngOnChanges(changes: SimpleChanges) {
    const sp: SimpleChange = changes.currentIndex;
    if (sp && sp.currentValue) {
      if (+sp.currentValue === 5) {
        this.getSummary();
        this.hubConnection = new signalR.HubConnectionBuilder()
          .withUrl(this.configuration.serverSettings.apiUrl + 'notificationhub', {
            transport: signalR.HttpTransportType.LongPolling
          })
          .build();

        this.hubConnection
          .start()
          .then((c) => {
            this.hubConnection.invoke('JoinGroup', 'JiraGroup');
            this.hubConnection.on(this.savedProject.subProjectId, data => this.progressEvent(data));
            this.hubConnection.on(this.savedProject.subProjectId + ' completed', data => this.onSyncComplete(data));
          });
      }
    }

  }

  onSyncComplete(data) {
    this.isSyncing = false;
    console.log('Sync Completed');
  }

  getSummary() {
    this.jiraService.GetJiraIntegrationSummary(this.savedProject.id)
      .subscribe(
        (res) => {
          this.summary = res;
        }
      );
  }

  complete() {
    this.startedToSync = true;
    this.isSyncing = true;
    this.isStartingSync = true;
    const user = this.sessionService.getUser();
    this.jiraService.FirstTimeSync(this.savedProject.subProjectId, user.name, this.syncIssues)
      .subscribe(
        (res) => {
          if (!this.syncIssues) {
            this.router.navigate(['/process-main']);
          }
          // this.isStartingSync = false;
        }
      );
      console.log("subProjectId", this.savedProject.subProjectId);
  }

  finish() {
    this.router.navigate(['/process-main']);
  }

  goBack() {
    this.prevStep.emit();
  }

  progressEvent(data) {
    this.isSyncing = true;
    this.isStartingSync = true;
    // Example format of the data:
    // [{"Name":"Bug","Total":14,"Current":0},{"Name":"Story","Total":1,"Current":0}]
    const tmp: any[] = JSON.parse(data);
    this.progressData = tmp.filter(x => x.Total > 0);
  }


  ngOnDestroy() {
    if (this.hubConnection && this.hubConnection.state === signalR.HubConnectionState.Connected) {
      this.hubConnection.stop();
    }
  }
   
}
