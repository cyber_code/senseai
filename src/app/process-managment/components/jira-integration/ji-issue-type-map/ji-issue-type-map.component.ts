import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { JiraIntegrationService } from 'src/app/process-managment/services/jira-integration.service';
import { Guid } from 'src/app/shared/guid';

@Component({
  selector: 'app-ji-issue-type-map',
  templateUrl: './ji-issue-type-map.component.html',
  styleUrls: ['./ji-issue-type-map.component.css']
})
export class JiIssueTypeMapComponent implements OnInit, OnChanges {
  @Input() savedProject: { id: string, jiraKey: string, subProjectId: string };
  @Output() nextStep = new EventEmitter<boolean>();
  @Output() prevStep = new EventEmitter<boolean>();
  isInEditmode = false;
  validationMessages: any[] = [];
  isSavingOrEditing = false;

  // SenseAi Fields
  requiredOnSenseAi: any[] = [];
  senseAiIssueTypes: any[] = [];
  senseAiIssueTypesFields: any[] = [];
  senseAi_loading = false;
  senseAiAttributes_loading = false;

  // Jira Fields
  requiredOnJira: any[] = [];
  jiraIssueTypes: any[] = [];
  jiraIssueTypesFields: any[] = [];
  jira_loading = false;
  jiraAttributes_loading = false;

  mappingList: any[] = [];
  modelToSave: any = {
    AttributeMapping: []
  };
  mappingToBeDeleted: any;

  constructor(private jiraService: JiraIntegrationService) { }

  ngOnInit() { }

  ngOnChanges(changes: SimpleChanges) {
    const sp: SimpleChange = changes.savedProject;
    if (sp && sp.currentValue && sp.currentValue.id) {
      this.getListOfMappings(sp.currentValue.id);
      this.modelToSave.JiraProjectMappingId = sp.currentValue.id;
      if (sp.currentValue.jiraKey) {
        this.getJiraIssueTypes(sp.currentValue.jiraKey);
      }
      if (sp.currentValue.subProjectId) {
        this.getSenseAiIssueTypes(sp.currentValue.subProjectId);
      }
    }
  }

  discardChangesOnEdit() {
    this.isInEditmode = false;
    this.modelToSave = { JiraProjectMappingId: this.savedProject.id };
  }

  getListOfMappings(mapId: string) {
    this.jiraService.GetJiraIssueTypeListByProjectMapping(mapId)
      .subscribe(
        (res: any[]) => {
          this.mappingList = res;
        }
      );
  }

  getIssueTypeMapDetails(id) {
    this.isInEditmode = true;
    this.modelToSave.AttributeMapping = [];
    this.jiraService.GetJiraIssueTypeMapping(id)
      .subscribe(
        (res: any) => {
          if (res) {
            res.AttributeMapping = res.getAttributeMapping;
            delete res.getAttributeMapping;
            // this.modelToSave = res;
            this.modelToSave = this.toEditmodel(res);
          }
        }
      );
  }

  getJiraIssueTypes(projectKey) {
    this.jira_loading = true;
    this.jiraIssueTypes = [];
    this.jiraService.GetJiraIssueTypes(projectKey, this.savedProject.subProjectId)
      .subscribe(
        (res: any[]) => {
          this.jira_loading = false;
          this.jiraIssueTypes = res;
        }
      );
  }

  getSenseAiIssueTypes(subProjectId) {
    this.senseAi_loading = true;
    this.senseAiIssueTypes = [];
    this.jiraService.GetSenseaiIssueTypes(subProjectId)
      .subscribe(
        (res: any[]) => {
          this.senseAiIssueTypes = res;
          this.senseAi_loading = false;
        }
      );
  }

  addIssueTypeField() {
    if (!this.modelToSave.AttributeMapping) {
      this.modelToSave.AttributeMapping = [];
    }
    this.modelToSave.AttributeMapping.push({
      // FieldMapping: { },
      AttributeValuesMapping: []
    });
  }

  removeIssueTypeField(index) {
    this.modelToSave.AttributeMapping.splice(index, 1);
  }

  deleteAttributeValue(attribute, valueIndex) {
    attribute.AttributeValuesMapping.splice(valueIndex, 1);
  }

  goBack() {
    this.prevStep.next();
  }

  saveIssueMapping() {
    this.nextStep.emit();
  }

  // Helpers
  onSenseAiIssueTypeChange(e, editModel = null) {
    this.senseAiAttributes_loading = true;
    this.modelToSave.AttributeMapping = [];
    if (e) {
      // tslint:disable-next-line:max-line-length
      const command = e.title === 'Requirement' ? this.jiraService.GetIssueTypeRequirementFields(this.savedProject.subProjectId) : this.jiraService.GetSenseAiIssueTypeFields(e.id, this.savedProject.subProjectId);
      command.subscribe(
        (res: any[]) => {
          this.senseAiAttributes_loading = false;
          this.senseAiIssueTypesFields = res;
          if (editModel) {
            editModel.AttributeMapping.forEach(element => {
              element.SenseaiValue = this.senseAiIssueTypesFields.find(x => x.fieldName === element.senseaiValue);
              element.AttributeValuesMapping = element.getAttributeValuesMapping;
              element.AttributeValuesMapping.forEach(v => {
              });
            });
          }
        }
      );
    }
  }

  onJiraIssueTypeChange(e, editModel = null) {
    this.jiraAttributes_loading = true;
    this.modelToSave.AttributeMapping = [];
    this.jiraService.GetJiraIssueTypeFields(e.issueTypeName, this.savedProject.jiraKey, this.savedProject.subProjectId)
      .subscribe(
        (res: any[]) => {
          this.jiraAttributes_loading = false;
          this.jiraIssueTypesFields = res;
          if (editModel) {
            editModel.AttributeMapping.forEach(element => {
              element.JiraValue = this.jiraIssueTypesFields.find(x => x.fieldName === element.jiraValue);
              element.AttributeValuesMapping = element.getAttributeValuesMapping;
              element.AttributeValuesMapping.forEach(v => {
              });
            });
          }
        }
      );
  }

  addValueMapItem(field) {
    if (!field.AttributeValuesMapping) {
      field.AttributeValuesMapping = [];
    }
    field.AttributeValuesMapping.push({});
  }

  public toEditmodel(model): any {
    const tmp = JSON.parse(JSON.stringify(model));
    tmp.SenseaiIssueTypeField = this.senseAiIssueTypes.find(x => x.id === tmp.senseaiIssueTypeField);
    tmp.JiraIssueTypeField = this.jiraIssueTypes.find(x => x.issueTypeId === tmp.jiraIssueTypeField);
    this.onSenseAiIssueTypeChange(tmp.SenseaiIssueTypeField, tmp);
    this.onJiraIssueTypeChange(tmp.JiraIssueTypeField, tmp);
    return tmp;
  }

  public model(): any {
    const tmp = JSON.parse(JSON.stringify(this.modelToSave));
    tmp.SenseaiIssueTypeFieldId = tmp.SenseaiIssueTypeField.id;
    tmp.SenseaiIssueTypeFieldName = tmp.SenseaiIssueTypeField.title;
    delete tmp.SenseaiIssueTypeField;
    tmp.JiraIssueTypeFieldId = tmp.JiraIssueTypeField.issueTypeId;
    tmp.JiraIssueTypeFieldName = tmp.JiraIssueTypeField.issueTypeName;
    delete tmp.JiraIssueTypeField;

    for (let i = 0; i < tmp.AttributeMapping.length; i++) {
      const element = tmp.AttributeMapping[i];
      element.index = i;
      element.IsMandatory = (element.SenseaiValue.isMandatory || element.JiraValue.isMandatory);
      element.SenseaiValue = element.SenseaiValue.fieldName;
      element.JiraValue = element.JiraValue.fieldName;
    }
    return tmp;
  }

  saveMapping() {
    this.isSavingOrEditing = true;
    const item = this.model();
    this.jiraService.AddJiraIssueTypeMapping(
      this.isInEditmode ? item.id : Guid.empty,
      item.jiraProjectMappingId || item.JiraProjectMappingId,
      item.SenseaiIssueTypeFieldId,
      item.JiraIssueTypeFieldId,
      item.JiraIssueTypeFieldName,
      item.AttributeMapping,
      item.allowAttachments,
      this.isInEditmode
    ).subscribe(
      (res) => {
        if (res) {
          this.getListOfMappings(this.savedProject.id);
          this.discardChangesOnEdit();
          this.isSavingOrEditing = false;
        }
      }
    );
  }

  prepareDeleteMapping(item: any) {
    this.mappingToBeDeleted = item;
  }

  deleteMapping() {
    this.jiraService.DeleteJiraIssueTypeMapping(this.mappingToBeDeleted.id)
      .subscribe(
        (res) => {
          if (res) {
            this.getListOfMappings(this.savedProject.id);
            this.discardChangesOnEdit();
          }
        }
      );
  }

  public get senseRequired(): any[] {
    try {
      if (this.modelToSave.SenseaiIssueTypeField) {
        const required = this.senseAiIssueTypesFields
          .filter(x => x.isMandatory === true)
          .map(x => ({ fieldName: x.fieldName, filled: false }));

        const listed = [];
        if (this.modelToSave.AttributeMapping) {
          this.modelToSave.AttributeMapping.forEach(e => {
            if (e.SenseaiValue && e.SenseaiValue.fieldName) {
              listed.push(e.SenseaiValue.fieldName);
            }
          });
        }
        required.forEach(r => {
          if (listed.find(x => x === r.fieldName)) {
            r.filled = true;
          }
        });
        return required;

      } else {
        return [];
      }
    } catch (error) {
      return [];
    }
  }

  public get jiraRequired(): any[] {
    try {
      if (this.modelToSave.JiraIssueTypeField) {
        const required = this.jiraIssueTypesFields
          .filter(x => x.isMandatory === true)
          .map(x => ({ fieldName: x.fieldName, filled: false }));

        const listed = [];
        if (this.modelToSave.AttributeMapping) {
          this.modelToSave.AttributeMapping.forEach(e => {
            if (e.JiraValue && e.JiraValue.fieldName) {
              listed.push(e.JiraValue.fieldName);
            }
          });
        }
        required.forEach(r => {
          if (listed.find(x => x === r.fieldName)) {
            r.filled = true;
          }
        });
        return required;
      } else {
        return [];
      }
    } catch (error) {
      return [];
    }
  }

  public StringifyObj(obj): string {
    return JSON.stringify(obj);
  }

  public mappedAttributes(): any {
    let isValid = true;
    let fields = JSON.parse(JSON.stringify(this.modelToSave.AttributeMapping));
    const emptyFields = fields.filter(x => !x.JiraValue || !x.SenseaiValue);
    if (emptyFields.length > 0) {
      return false;
    }
    fields = fields.filter(x => x.JiraValue && x.SenseaiValue);
    // tslint:disable-next-line:max-line-length
    fields = fields.map(x => { let y = { jira: x.JiraValue.fieldName, sense: x.SenseaiValue.fieldName, values: x.AttributeValuesMapping }; return y; });

    // For each mapping row
    for (let i = 0; i < fields.length; i++) {
      const current = fields[i];
      // Check invalid attributeValues
      if (current.values && current.values.length > 0) {
        if (current.values.find(v => !v.senseaiValue || !v.jiraValue)) {
          isValid = false;
          break;
        }
      }
      // Check for duplicate mappings
      const found = fields.filter(x => x.jira === current.jira && x.sense === current.sense);
      if (found.length > 1) {
        isValid = false;
        break;
      }
    }
    return isValid;
  }

  public get isValidModel(): boolean {
    let isValid;
    isValid = this.modelToSave
            && this.modelToSave.SenseaiIssueTypeField
            // && this.modelToSave.SenseaiIssueTypeField.id
            && this.modelToSave.JiraIssueTypeField
            // && this.modelToSave.JiraIssueTypeField.title
            && this.modelToSave.AttributeMapping
            && this.modelToSave.AttributeMapping.length > 0
            && this.mappedAttributes();
    return isValid;
  }


  gatherValidationMessages() {
    this.validationMessages = [];
    if (!this.modelToSave) {
      this.validationMessages.push('Invalid model!');
      return;
    }

    if (!this.modelToSave.SenseaiIssueTypeField) {
      this.validationMessages.push('Please provide SenseAI Issue type that you want to map');
    }

    if (!this.modelToSave.JiraIssueTypeField) {
      this.validationMessages.push('Please provide Jira Issue type that you want to map');
    }

    if (this.modelToSave.AttributeMapping === undefined || this.modelToSave.AttributeMapping.length === 0) {
      this.validationMessages.push('A minimum one row of fields you want to map is required');
    }

    if (!this.mappedAttributes()) {
      this.validationMessages.push('One (or more) rows of fields are not valid');
    }
  }

}
