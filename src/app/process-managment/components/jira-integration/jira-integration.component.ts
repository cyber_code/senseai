import { Component, OnInit, ViewChild } from '@angular/core';
import { WizardComponent } from 'angular-archwizard';

@Component({
  selector: 'app-jira-integration',
  templateUrl: './jira-integration.component.html',
  styleUrls: ['./jira-integration.component.css']
})
export class JiraIntegrationComponent implements OnInit {
  @ViewChild(WizardComponent) public wizard: WizardComponent;
  savedProject: { id: string, jiraKey: string };
  isAuthenticated = false;
  constructor() {}

  ngOnInit() { }

  public get wizard_index(): number {
    return this.wizard.currentStepIndex;
  }

  authenticatedWithJira(value: boolean) {
      this.isAuthenticated = value;
      if (this.isAuthenticated) {
        this.wizard.goToNextStep();
        this.wizard.goToNextStep();
      }
  }

  navigateWizzardBack() {
    this.wizard.goToPreviousStep();
  }

  navigateWizzardForward() {
    this.wizard.goToNextStep();
  }

  getSavedProject(savedProject) {
    this.savedProject = savedProject;
  }
}
