import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-hierarchy-types',
  templateUrl: './hierarchy-types.component.html',
  styleUrls: ['./hierarchy-types.component.css']
})
export class HierarchyTypesComponent implements OnInit {
  @Input() gridData;
  @Input() state;
  @Input() hierarchyTypes = [];

  @Output() hierarchyTypeDataStateChanged = new EventEmitter<any>();
  @Output() hierarchyTypeListChanged = new EventEmitter<any>();

  public selectedHierarchyType = {
    parentId: '',
    subProjectId: '',
    title: '',
    description: ''
  };

  public hierarchyTypeWindowOpen = false;
  public deleteHierarchyTypeWindowOpen = false;
  public isHierarchyTypeEdited = false;
  public navigation = [];

  constructor(
    private processManagementService: ProcessManagementService,
    private sessionService: SessionService,
    public permissionService: PermissionService
  ) {}

  ngOnInit() {

  }

  dataStateChange(state) {
    this.hierarchyTypeDataStateChanged.emit(state);
  }

  openHierarchyTypeWindow(hierarchyType?) {
    this.hierarchyTypeWindowOpen = true;
    if (hierarchyType) {
      this.isHierarchyTypeEdited = true;
      this.selectedHierarchyType = _.cloneDeep(hierarchyType);
    } else {
      this.resetHierarchyType();
    }
  }

  close() {
    this.isHierarchyTypeEdited = false;
    this.hierarchyTypeWindowOpen = false;
    this.deleteHierarchyTypeWindowOpen = false;
    this.resetHierarchyType();
  }

  resetHierarchyType() {
    this.selectedHierarchyType = {
      parentId: '',
      subProjectId: '',
      title: '',
      description: ''
    };
  }

  updatehierarchyTypeList(hierarchyType) {
    let hierarchyTypes = [];
    if (this.isHierarchyTypeEdited) {
      hierarchyTypes = this.hierarchyTypes.map(hType => {
        if (hType.id === hierarchyType.id) {
          return hierarchyType;
        }
        return hType;
      });
    } else {
      hierarchyTypes = this.hierarchyTypes.concat(hierarchyType);
    }
    this.hierarchyTypeListChanged.emit(hierarchyTypes);

    this.close();
  }

  async removeHierarchyTypeFromList() {
    let subProjectId = this.sessionService.getSubProject().id
    if (this.permissionService.hasPermission("/api/Process/Configure/GetHierarchyTypes")){
      let hierarchyTypes = await this.processManagementService.getHierarchyTypes(subProjectId).toPromise();
      this.hierarchyTypeListChanged.emit(hierarchyTypes ? hierarchyTypes : []);
    }
    
    this.close();
  }

  openDeleteHierarchyTypeWindow(hierarchyType) {
    this.deleteHierarchyTypeWindowOpen = true;
    this.selectedHierarchyType = hierarchyType;
  }

  addHierarchyTypePermission() {
    return (
      this.permissionService.hasPermission('/api/Process/Configure/GetHierarchyTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/AddHierarchyType')
    );
  }

  editHierarchyTypePermission() {
    return (
      this.permissionService.hasPermission('/api/Process/Configure/GetHierarchyTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/UpdateHierarchyType')
    );
  }

  deleteHierarchyTypePermission() {
    return (
      this.permissionService.hasPermission('/api/Process/Configure/GetHierarchyTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/DeleteHierarchyType')
    );
  }
}
