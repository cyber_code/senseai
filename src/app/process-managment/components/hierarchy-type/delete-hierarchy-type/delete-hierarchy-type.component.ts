import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ProcessManagementService } from '../../../services/process-management.service';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';

@Component({
  selector: 'app-delete-hierarchy-type',
  templateUrl: './delete-hierarchy-type.component.html',
  styleUrls: ['./delete-hierarchy-type.component.css']
})
export class DeleteHierarchyTypeComponent implements OnInit {
  public _deleteHierarchyTypeWindowOpen;
  public message = '';
  public selectedHierarchyType;
  public deleteType = true;
  public moveType = false;
  @Input()
  set _selectedHierarchyType(selectedHierarchyType) {
    this.message = 'Are you sure you want to delete ' + selectedHierarchyType.title + ' hierarchy ?';
    this.selectedHierarchyType = selectedHierarchyType;
  }
  public deleted = false;

  @Input() hierarchyTypes;

  @Input()
  set deleteHierarchyTypeWindowOpen(deleteHierarchyTypeWindowOpen) {
    this.reset();
    this._deleteHierarchyTypeWindowOpen = deleteHierarchyTypeWindowOpen;
  }

  @Output() public closeDeleteHierarchyTypeWindow = new EventEmitter<any>();
  @Output() public removeHierarchyTypeFromList = new EventEmitter<boolean>();

  constructor(private processManagment: ProcessManagementService, private messageService: MessageService) {}

  ngOnInit() {}

  deleteHierarchyType() {
    this.processManagment.sendMessage(this.selectedHierarchyType.id);
    if (!this.deleted) {
      this.processManagment.deleteHierarchyType(this.selectedHierarchyType.id, false).subscribe(res => {
        if (!res || res.result) {
          this.handleResponse(res);
        } else {
          this.deleted = true;
          this.message = this.selectedHierarchyType.title + ' is associated with other types! You can choose to: ';
        }
      });
    } else {
      if (this.deleteType) {
        this.processManagment.deleteHierarchyType(this.selectedHierarchyType.id, true).subscribe(res => {
          this.handleResponse(res);
        });
      } else {
        this.processManagment.moveHierarchyType(this.selectedHierarchyType.id).subscribe(res => {
          this.handleResponse(res);
        });
      }
    }
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({ text: 'Hierarchy was deleted successfully!', type: MessageType.Success });
      this.removeHierarchyTypeFromList.emit(this.selectedHierarchyType);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeleteHierarchyTypeWindow.emit();
  }

  reset() {
    this.deleted = false;
    this.deleteType = true;
    this.moveType = false;
  }

  deleteTypeClicked() {
    this.deleteType = true;
    this.moveType = false;
  }

  moveTypeClicked() {
    this.moveType = true;
    this.deleteType = false;
  }
}
