import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { State, process } from '@progress/kendo-data-query';
import { SessionService } from 'src/app/core/services/session.service';
import { isNullOrUndefined } from 'util';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-hierarchies',
  templateUrl: './hierarchies.component.html',
  styleUrls: ['./hierarchies.component.css']
})
export class HierarchiesComponent implements OnInit {
  public hierarchyTypes = [];
  public hierarchyTypesGridData: any = [];

  @Output() public hierarchyTypeListChanged = new EventEmitter<boolean>();

  workContext: any;
  public hierarchyTypeState: State = {
    skip: 0,
    take: 10,
    filter: {
      logic: 'and',
      filters: []
    }
  };

  constructor(
    private processManagment: ProcessManagementService,
    private sessionService: SessionService,
    public permissionService: PermissionService
  ) { }

  async ngOnInit() {
    const hierarchyTypes = this.permissionService.hasPermission('/api/Process/Configure/GetHierarchyTypes') ? 
      await this.getHierarchyTypes() : [];
    this.hierarchyTypes = hierarchyTypes;
    this.addPathsToHierarchyTypes(hierarchyTypes);
  }

  addPathsToHierarchyTypes(hierarchyTypes) {
    let lastItem = hierarchyTypes.find(hrcType => !hierarchyTypes.find(hierarchyType => hierarchyType.parentId === hrcType.id));
    let navigation = [];

    if (lastItem) {
      navigation = this.recursivelyFindPath(hierarchyTypes, [lastItem]);
    }

    for(let i = 0; i < hierarchyTypes.length; i++) {
      const hierarchyType = hierarchyTypes[i];
      if (!navigation.find(nav => nav.id === hierarchyType.id)) {
        hierarchyType.path = '';
      } else {
        let path = '';
        for(let j = 0; i < navigation.length; j++) {
          if (navigation[j].id === hierarchyType.id) {
            path += (path === '') ? navigation[j].title : ' > ' + navigation[j].title;
            break;
          } else {
            path += (path === '') ? navigation[j].title : (' > ' + navigation[j].title);
          }
        }
        hierarchyType.path = path;
      }
    }

    this.hierarchyTypesGridData = process(hierarchyTypes, this.hierarchyTypeState);
  }

  recursivelyFindPath(hierarchyTypes, navigation) {
    if (hierarchyTypes && hierarchyTypes.length > 0){
      const parent = hierarchyTypes.find(hierarchyType => hierarchyType.id === navigation[0].parentId);
      if (isNullOrUndefined(parent)) {
        return navigation;
      } else {
        navigation.unshift(parent);
        return this.recursivelyFindPath(hierarchyTypes, navigation);
      }
    } else {
      return [];
    }
    
  }


  getHierarchyTypes() {
    this.workContext = this.sessionService.getWorkContext();
    return this.processManagment.getHierarchyTypes(this.workContext.subProject.id).toPromise();
  }

  hierarchyTypeDataStateChange(state) {
    this.hierarchyTypeState = state;
    this.hierarchyTypesGridData = process(this.hierarchyTypes, state);
  }

  hierarchyTypeListChange(hierarchyTypes) {
    this.hierarchyTypes = hierarchyTypes;
    this.addPathsToHierarchyTypes(hierarchyTypes);
  }
}
