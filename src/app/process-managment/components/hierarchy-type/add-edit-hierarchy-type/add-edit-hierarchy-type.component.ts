import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { SessionService } from 'src/app/core/services/session.service';
import { ProcessManagementService } from '../../../services/process-management.service';
import { Guid } from 'src/app/shared/guid';

@Component({
  selector: 'app-add-edit-hierarchy-type',
  templateUrl: './add-edit-hierarchy-type.component.html',
  styleUrls: ['./add-edit-hierarchy-type.component.css']
})
export class AddEditHierarchyTypeComponent implements OnInit {
  public lastHierarchyType;
  public _hierarchyTypeWindowOpen;
  public forceDelete: boolean = false;
  public confirmForceDelete: boolean = false;
  public messageForceDelete: string = '';
  public loading: boolean = false;
  @Input() isHierarchyTypeEdited;
  @Input() selectedHierarchyType;
  @Input()
  set hierarchyTypeWindowOpen(hierarchyTypeWindowOpen) {
    if (hierarchyTypeWindowOpen && !this.isHierarchyTypeEdited) {
      let subProjectId = this.sessionService.getSubProject().id;
      this.loadLastHierarchyTypeList(subProjectId);
    }
    this._hierarchyTypeWindowOpen = hierarchyTypeWindowOpen;
  }

  @Output() public closeHierarchyTypeWindow = new EventEmitter<any>();
  @Output() public hierarchyTypeListUpdated = new EventEmitter<boolean>();

  @Input() hierarchyTypes;
  lastHierarchyTypeList: any[] = [];

  constructor(
    private processManagment: ProcessManagementService,
    private messageService: MessageService,
    private sessionService: SessionService
  ) {}

  ngOnInit() {}

  loadLastHierarchyTypeList(subProjectId: any) {
    if (this.hierarchyTypes) {
      this.processManagment
        .getLastHierarchyType(subProjectId)
        .subscribe(response => {
          if (response) {
            this.lastHierarchyTypeList = [response];
            this.lastHierarchyType =
              this.hierarchyTypes.length > 0 ? response : null;
          } else {
            this.lastHierarchyTypeList = [];
            this.lastHierarchyType = null;
          }
        });
    }
  }

  close() {
    this.closeHierarchyTypeWindow.emit();
  }

  changeParentId(event) {
    const parentId = event.target.value.trim('');
    this.selectedHierarchyType.parentId = parentId;
  }

  changeSubProjectId(event) {
    const subProjectId = event.target.value.trim('');
    this.selectedHierarchyType.subProjectId = subProjectId;
  }

  changeDescription(event) {
    const description = event.target.value.trim('');
    this.selectedHierarchyType.description = description;
  }

  disableSubmitBtn() {
    return this.selectedHierarchyType.title.trim('') === '';
  }

  submitClicked() {
    if (this.isHierarchyTypeEdited) {
      this.editHierarchyType();
    } else {
      this.addHierarchyType();
    }
  }

  addHierarchyType() {
    this.loading = true;
    let subProjectId = this.sessionService.getSubProject().id;
    this.processManagment
      .addHierarchyType(
        this.lastHierarchyType ? this.lastHierarchyType.id : Guid.empty,
        subProjectId,
        this.selectedHierarchyType.title.trim(''),
        this.selectedHierarchyType.description.trim(''),
        this.confirmForceDelete ? true : false
      )
      .subscribe(res => {
        this.loading = false;
        if (
          res.response &&
          res.response.messageType === 3 &&
          res.result &&
          res.result.id === Guid.empty
        ) {
          this.forceDelete = true;
          this.messageForceDelete = res.response.message;
        } else {
          this.forceDelete = false;
          this.confirmForceDelete = false;
          this.handleHierarchyTypeResponse(res.result);
        }
      });
  }

  editHierarchyType() {
    this.processManagment
      .updateHierarchyType(
        this.selectedHierarchyType.id,
        this.selectedHierarchyType.parentId === ''
          ? Guid.empty
          : this.selectedHierarchyType.parentId,
        this.sessionService.getSubProject().id,
        this.selectedHierarchyType.title.trim(''),
        this.selectedHierarchyType.description.trim('')
      )
      .subscribe(res => {
        this.handleHierarchyTypeResponse(res);
      });
  }

  approveConfirm() {
    if (!this.confirmForceDelete) return (this.confirmForceDelete = true);
    this.addHierarchyType();
  }

  closeWindow() {
    this.forceDelete = false;
    this.confirmForceDelete = false;
  }

  handleHierarchyTypeResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isHierarchyTypeEdited
          ? 'Hierarchy was updated successfully!'
          : 'Hierarchy was added successfully!',
        type: MessageType.Success
      });

      this.hierarchyTypeListUpdated.emit(
        this.isHierarchyTypeEdited
          ? this.selectedHierarchyType
          : {
              ...this.selectedHierarchyType,
              id: res.id,
              parentId: this.lastHierarchyType
                ? this.lastHierarchyType.id
                : Guid.empty
            }
      );
    }

    this.close();
  }

  setParent(event: any) {
    if (event) {
      this.selectedHierarchyType = event;
    }
  }
}
