import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {KitchenSinkService} from '../../../design/services/rapid-services/kitchensink-service';
import {StencilService} from '../../../design/services/rapid-services/stencil-service';
import {ToolbarService} from '../../../design/services/rapid-services/toolbar-service';
import {InspectorService} from '../../../design/services/rapid-services/inspector-service';
import {HaloService} from '../../../design/services/rapid-services/halo-service';
import {KeyboardService} from '../../../design/services/rapid-services/keyboard-service';
import {ProcessManagementService} from '../../services/process-management.service';
import {WindowService} from '@progress/kendo-angular-dialog';
import {WorkflowItemService} from '../../../design/services/workflow-item.service';
import {RapidEventsService} from '../../../design/services/rapid-events.service';
import {SessionService} from '../../../core/services/session.service';
import {takeWhileAlive} from 'take-while-alive';

@Component({
  selector: 'app-view-workflow',
  templateUrl: './view-workflow.component.html',
  styleUrls: ['./view-workflow.component.css']
})
export class ViewWorkflowComponent implements OnInit {
  public rappid: KitchenSinkService;
  @Input() public workflowId: string;
  public linkedWorkflows: any = [];
  constructor(private processManagementService: ProcessManagementService,
              private windowService: WindowService,
              private workflowItemServie: WorkflowItemService,
              private element: ElementRef,
              private rapidEventsService: RapidEventsService,
              private sessionService: SessionService
  ) {
    rapidEventsService.itemSelected$.pipe(takeWhileAlive(this)).subscribe(result => {
    });
  }
  ngOnInit() {
    this.rappid = new KitchenSinkService(
      this.element.nativeElement as any,
      new StencilService(),
      new ToolbarService(),
      new InspectorService(),
      new HaloService(),
      new KeyboardService(),
      this.rapidEventsService,
      true,
      this.sessionService);
    this.rappid.startRappid();
    this.loadWorkflow();
  }

  private loadWorkflow() {
    // this.isWorkflowChanged = false;
    this.processManagementService.getWorkflow(this.workflowId).subscribe(result => {
      let jsonParsed;
      // if (!result.isParsed) {
      //   jsonParsed = result ? this.getParsedGraph(JSON.parse(result.designerJson)) : {cells: []};
      // } else {
      jsonParsed = (result && result.designerJson && JSON.parse(result.designerJson)) || [];
      // }
      let pathJson = (result && result.pathsJson && JSON.parse(result.pathsJson)) || {paths: []};
      this.linkedWorkflows = (result && result.linkedWorkflows) || [];
      if (jsonParsed.length === 0 || (jsonParsed.cells && jsonParsed.cells.length === 0)) {
        this.clearGraph();
      } else {
        this.rappid.loadGraphJson(jsonParsed);
        this.rappid.layoutGraph(false);
        if (result && !result.isParsed) {
          pathJson = {paths: pathJson.paths.map((path, i) => ({...path, number: i}))};
        }
        // it should be called after loading graph, otherwise it cannot highlight anything
        this.workflowItemServie.worklfowPathsLoaded(pathJson.paths);
      }
    });
    this.rappid.undoStack = [];
    this.rappid.redoStack = [];
  }

  private clearGraph() {
    this.rappid.loadGraphJson({cells: []});
    this.workflowItemServie.worklfowPathsLoaded([]);
  }


}
