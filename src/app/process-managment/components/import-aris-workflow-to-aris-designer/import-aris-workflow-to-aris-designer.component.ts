import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ArisTreeViewItem } from 'src/app/shared/aris-tree-view-item';
import { ImportArisWfService } from '../../services/import-aris-workflow.service';


@Component({
  selector: 'app-import-aris-workflow-to-aris-designer',
  templateUrl: './import-aris-workflow-to-aris-designer.component.html',
  styleUrls: ['./import-aris-workflow-to-aris-designer.component.css']
})
export class ImportArisWorkflowToArisDesignerComponent implements OnInit {
  public file: File;
  private selectedTreeViewItem: ArisTreeViewItem;
  openedImportArisWorkFlowDialogBox = true;

  @Output() public reloadFolder = new EventEmitter<ArisTreeViewItem>();
  @Output() public treeViewItemCreated = new EventEmitter<ArisTreeViewItem>();
  @Output() public treeViewItemDeleted = new EventEmitter<ArisTreeViewItem>();

  constructor(private importArisWorkflowService: ImportArisWfService) {
  }

  ngOnInit() {
  }

  importArisWorkflowSettings() {
    const formData = new FormData();
    formData.append('file', this.file);
    this.importArisWorkflowService.importArisSettings('027BA59C-6FA8-4B9F-827F-89B54F1E7609', formData).subscribe(res => {
      if (res) {
        this.reloadFolder.emit(this.selectedTreeViewItem);
        this.closeImportArisWorkFlowDialogBox();
      }
    })
  }

  fileChanged(event) {
    const file = event.target.files[0];
    if (file) {
      this.file = file;
    }
  }

  submit() {
    this.importArisWorkflowSettings();
  }

  closeImportArisWorkFlowDialogBox() {
    this.openedImportArisWorkFlowDialogBox = false;
  }

}
