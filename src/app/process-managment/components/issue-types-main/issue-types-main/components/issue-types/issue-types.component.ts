import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import * as _ from 'lodash';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-issue-types',
  templateUrl: './issue-types.component.html',
  styleUrls: ['./issue-types.component.css']
})
export class IssueTypesComponent implements OnInit {
  @Input() gridData;
  @Input() state;
  @Input() issueTypes;

  @Output() issueTypeDataStateChanged = new EventEmitter<any>();
  @Output() issueTypeListChanged = new EventEmitter<any>();

  public selectedIssueType = {
    subProjectId: '',
    title: '',
    description: '',
    unit: ''
  };

  public issueTypeWindowOpen = false;
  public deleteIssueTypeWindowOpen = false;
  public isIssueTypeEdited = false;

  constructor(public permissionService: PermissionService) {}

  ngOnInit() {}

  dataStateChange(state) {
    this.issueTypeDataStateChanged.emit(state);
  }

  openIssueTypeWindow(issueType?) {
    this.issueTypeWindowOpen = true;
    if (issueType) {
      this.isIssueTypeEdited = true;
      this.selectedIssueType = _.cloneDeep(issueType);
    } else {
      this.resetIssueType();
    }
  }

  close() {
    this.issueTypeWindowOpen = false;
    this.deleteIssueTypeWindowOpen = false;
    this.resetIssueType();
  }
  
  resetIssueType() {
    this.isIssueTypeEdited = false;
    this.selectedIssueType = {
      subProjectId: '',
      title: '',
      description: '',
      unit: ''
    };
  }

  updateIssueTypeList(issueType) {
    let issueTypes = [];
    if (this.isIssueTypeEdited) {
      issueTypes = this.issueTypes.map(iType => {
        if (iType.id === issueType.id) {
          return issueType;
        }
        return iType;
      });
    } else {
      issueTypes = this.issueTypes.concat(issueType);
    }
    this.issueTypeListChanged.emit(issueTypes);
    this.close();
  }

  removeIssueTypeFromList(issueType) {
    let issueTypes = this.issueTypes.filter(iType => iType.id !== issueType.id);
    this.issueTypeListChanged.emit(issueTypes);
    this.close();
  }

  openDeleteIssueTypeWindow(issueType) {
    this.deleteIssueTypeWindowOpen = true;
    this.selectedIssueType = issueType;
  }

  addIssueTypePermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypeFields') &&
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypeFieldsNames') &&
      this.permissionService.hasPermission('/api/Process/Configure/AddIssueType')
    );
  }

  editIssueTypePermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypeFields') &&
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypeFieldsNames') &&
      this.permissionService.hasPermission('/api/Process/Configure/UpdateIssueType')
    );
  }

  deleteIssueTypePermission() {
    return (
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/DeleteIssueType')
    );
  }
}
