import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from '../../../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-add-edit-issue-type',
  templateUrl: './add-edit-issue-type.component.html',
  styleUrls: ['./add-edit-issue-type.component.css']
})
export class AddEditIssueTypeComponent implements OnInit {
  @Input() isIssueTypeEdited;
  @Input() selectedIssueType;
  public _issueTypeWindowOpen;
  public issueTypeFields = [];

  @Input() 
  set issueTypeWindowOpen(issueTypeWindowOpen) {
    this._issueTypeWindowOpen = issueTypeWindowOpen;
    if (issueTypeWindowOpen && this.isIssueTypeEdited) {
      this.processManagment.getIssueTypeFields(this.selectedIssueType.id).subscribe((res: any) => {
        if (res && res.length > 0) {
          this.issueTypeFields = res.map(el => ({...el,  name: el.issueTypeFieldsName}));
        } else {
          this.processManagment.getIssueTypeFieldsNames().subscribe((res: any) => {
            if (res) {
             this.issueTypeFields = res.map(el => ({...el, issueTypeFieldsNameId: el.id, checked: false}));
           }
         });
        }
      });
    }

    else if (issueTypeWindowOpen) {
      this.processManagment.getIssueTypeFieldsNames().subscribe((res: any) => {
         if (res) {
          this.issueTypeFields = res.map(el => ({...el, issueTypeFieldsNameId: el.id, checked: false}));
        }
      });
    }
  };

  @Output() public closeIssueTypeWindow = new EventEmitter<any>();
  @Output() public issueTypeListUpdated = new EventEmitter<boolean>();

  public configureFieldWindowOpen = false;
  public workContext: any;

  public units = ['Days', 'Weeks', 'Months', 'Story Points'];
  

  constructor(
    private processManagment: ProcessManagementService,
    private messageService: MessageService,
    private sessionService: SessionService
  ) { }

  ngOnInit() {
    this.workContext = this.sessionService.getWorkContext();
  }

  close() {
    this.closeIssueTypeWindow.emit();
    this.resetIssueTypeFields();
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedIssueType.title = title;
  }

  changeDescription(event) {
    let description = event.target.value.trim('');
    this.selectedIssueType.description = description;
  }

  changeUnit(event) {
    this.selectedIssueType.unit = event ? event : '';
  }

  disableSubmitBtn() {
    return (
      this.selectedIssueType.title === ''
    );
  }

  submitClicked() {
    if (this.isIssueTypeEdited) {
      this.editIssueType();
    } else {
      this.addIssueType();
    }
  }

  addIssueType() {
    this.processManagment
      .addIssueType(
        this.workContext.subProject.id, 
        this.selectedIssueType.title, 
        this.selectedIssueType.description,
        this.selectedIssueType.unit ? this.selectedIssueType.unit : '',
        false,
        this.issueTypeFields
      ).subscribe(res => {
        this.handleIssueTypeResponse(res);
      });
  }

  editIssueType() {
    let issueTypeFields = this.issueTypeFields.map(field => ({issueTypeFieldsNameId: field.issueTypeFieldsNameId, checked: field.checked}));
    this.processManagment
      .updateIssueType(
        this.selectedIssueType.id,
        this.workContext.subProject.id,
        this.selectedIssueType.title,
        this.selectedIssueType.description,
        this.selectedIssueType.unit ? this.selectedIssueType.unit : '',
        this.selectedIssueType.default,
        issueTypeFields
      ).subscribe(res => {
        this.handleIssueTypeResponse(res);
      });
  }

  resetIssueTypeFields() {
    this.issueTypeFields = [];
  }

  handleIssueTypeResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isIssueTypeEdited
          ? 'IssueType was updated successfully!'
          : 'IssueType was added successfully!',
        type: MessageType.Success
      });
      this.issueTypeListUpdated.emit(
        this.isIssueTypeEdited
          ? this.selectedIssueType
          : { ...this.selectedIssueType, id: res.id }
      );
    } else {
      this.close();
    }
  }
}
