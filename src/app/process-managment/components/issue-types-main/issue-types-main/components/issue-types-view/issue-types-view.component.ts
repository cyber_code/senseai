import { Component, OnInit } from '@angular/core';
import { State, process } from '@progress/kendo-data-query';
import { SessionService } from 'src/app/core/services/session.service';
import { DesignService } from 'src/app/design/services/design.service';
import { ProcessManagementService } from '../../../../../services/process-management.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-issue-types-view',
  templateUrl: './issue-types-view.component.html',
  styleUrls: ['./issue-types-view.component.css']
})
export class IssueTypesViewComponent implements OnInit {
  public issueTypes = [];
  public issueTypesGridData: any = [];

  workContext: any;
  public issueTypeState: State = {
    skip: 0,
    take: 10,
    filter: {
      logic: 'and',
      filters: []
    }
  };

  constructor(
    private processManagment: ProcessManagementService,
    private sessionService: SessionService,
    public permissionService: PermissionService
  ) {}

  async ngOnInit() {
    const issueTypes = this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypes') ? await this.getIssueTypes() : [];
    this.issueTypes = issueTypes;
    this.issueTypesGridData = process(issueTypes, this.issueTypeState);
  }

  getIssueTypes() {
    this.workContext = this.sessionService.getWorkContext();
    return this.processManagment
      .getIssueTypes(this.workContext.subProject.id)
      .toPromise();
  }

  issueTypeDataStateChange(state) {
    this.issueTypeState = state;
    this.issueTypesGridData = process(this.issueTypes, state);
  }

  issueTypeListChange(issueTypes) {
    this.issueTypes = issueTypes;
    this.issueTypesGridData = process(issueTypes, this.issueTypeState);
  }
}
