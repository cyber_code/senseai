import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from '../../../../../services/process-management.service';

@Component({
    selector: 'app-delete-issue-type',
    templateUrl: './delete-issue-type.component.html',
    styleUrls: ['./delete-issue-type.component.css']
})
export class DeleteIssueTypeComponent implements OnInit {
    @Input() deleteIssueTypeWindowOpen;
    @Input() selectedIssueType;

    @Output() public closeIssueTypeWindow = new EventEmitter<any>();
    @Output() public removeIssueTypeFromList = new EventEmitter<boolean>();

    constructor(
        private processManagment: ProcessManagementService,
        private messageService: MessageService
    ) { }

    ngOnInit() { }

    deleteIssueType() {
        this.processManagment.deleteIssueType(this.selectedIssueType.id, true).subscribe(res => {
            this.handleResponse(res);
        });
    }

    handleResponse(res) {
        if (res) {
            this.messageService.sendMessage({ text: 'Issue Type was deleted successfully!', type: MessageType.Success });
            this.removeIssueTypeFromList.emit(this.selectedIssueType);
        } else {
            this.close();
        }
    }

    close() {
        this.closeIssueTypeWindow.emit();
    }

}
