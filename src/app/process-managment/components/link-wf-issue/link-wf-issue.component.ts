import { Component, EventEmitter, OnInit, Output, ViewChild, Input } from '@angular/core';
import { DataManagementService } from '../../../data-management/services/data-management.service';
import { isNullOrUndefined } from 'util';
import { Type } from '../../models/type';
import { ProcessManagementService } from '../../services/process-management.service';
import { MessageType } from '../../../core/models/message.model';
import { Severity } from '../../models/severity';
import { Priorities } from '../../models/priorities';
import { MessageService } from '../../../core/services/message.service';
import { SessionService } from '../../../core/services/session.service';
import { thresholdSturges } from 'd3';
import { Guid } from 'src/app/shared/guid';

@Component({
  selector: 'app-link-wf-issue',
  templateUrl: './link-wf-issue.component.html',
  styleUrls: ['./link-wf-issue.component.css']
})
export class LinkWfIssueComponent implements OnInit {
  public processWorkflows: any[] = [];
  public issueTypes: any[] = [];
  public selectedWorkflow: any;
  public selectedIssue: any;
  public selectedIssueType: any;
  public openIssueModal: boolean = false;
  public openDefectIssue: boolean = false;
  public issuesByType: any[] = [];

  public folderId = '4f34555b-38d7-4d81-86b9-1612f5158b62';
  // public processId = '2146d23f-b8db-4ce0-b7d1-466214976c9b';
  public subProjectId: any;
  @Input() allIssues;
  public unLinkIssue: boolean = false;
  public types = [
    { title: 'Software', type: Type.Software },
    { title: 'Cosmetic', type: Type.Cosmetic },
    { title: 'Performance', type: Type.Performance },
    { title: 'Database', type: Type.Database },
    { title: 'Configuration', type: Type.Configuration },
    { title: 'Environmental', type: Type.Environmental },
    { title: 'Documentation', type: Type.Documentation }
  ];
  public priorities = [
    { title: 'Low', type: Priorities.Low },
    { title: 'Medium', type: Priorities.Medium },
    { title: 'High', type: Priorities.High },
    { title: 'Critical', type: Priorities.Critical }
  ];
  public severity = [
    { title: 'Low', type: Severity.Low },
    { title: 'Medium', type: Severity.Medium },
    { title: 'High', type: Severity.High },
    { title: 'Immediate', type: Severity.Immediate }
  ];
  @Input() public processDetail: any;
  @Output() public closeIssuePopupBool = new EventEmitter();
  @Output() public isDefectType = new EventEmitter();
  constructor(
    private dataManagementService: DataManagementService,
    private processManagmentService: ProcessManagementService,
    private messageService: MessageService,
    private sessionService: SessionService
  ) {}

  ngOnInit() {
    this.subProjectId = this.sessionService.getWorkContext().subProject.id;
    this.loadProcessWorkflows(this.processDetail.id);
    this.loadIssueTypes(this.subProjectId);
    this.isDefectType.emit(false);
  }

  private loadProcessWorkflows(processId) {
    if (isNullOrUndefined(processId)) {
      this.processWorkflows = [];
    } else {
      this.processManagmentService.getProcessWorkflows(processId).subscribe(res => {
        this.processWorkflows = res;
      });
    }
  }

  loadIssueTypes(subProjectId) {
    this.processManagmentService.getIssueTypes(subProjectId).subscribe((result: any) => {
      if (result) {
        let issueTypes = [];
        result.map(async type => {
          let fields = await this.processManagmentService.getIssueTypeFields(type.id).toPromise() as any;
          let workflow = fields.find(field => field.issueTypeFieldsName === 'Workflow');
          if (workflow && workflow.checked) {
            let issues = this.allIssues ? this.allIssues.filter(issue => issue.issueTypeId === type.id) : [];
            issueTypes.push({...type, issueNumber: issues.length, issues: issues});
          } else {
            let issues = this.allIssues ? 
            this.allIssues.filter(issue => issue.issueTypeId === type.id && 
              (issue.processId === Guid.empty || issue.processId === this.processDetail.id)) : [];
            issueTypes.push({...type, issueNumber: issues.length, issues: issues});
          }
        });
        this.issueTypes = issueTypes;
      }
    });
  }

  goBack() {
    this.openIssueModal = false;
    this.openDefectIssue = false;
    this.selectedIssue = null;
    this.selectedWorkflow = null;
    this.unLinkIssue = false;
    this.isDefectType.emit(false);
  }

  handleWorkflowSelection(event) {
    this.selectedWorkflow = event.selectedRows[0].dataItem;
    this.linkOrUnLinkIssue();
  }

  handleIssueSelection(event) {
    this.selectedIssue = event.selectedRows[0].dataItem;
    if (this.openDefectIssue) return this.linkOrUnLinkIssue();
    return this.linkOrUnLinkRequirementIssue();
  }

  handleIssueTypeSelection(event) {
    this.selectedIssueType = event.selectedRows[0].dataItem;
    this.processManagmentService.getIssueTypeFields(this.selectedIssueType.id).subscribe((fields: any) => {
      let workflow = fields.find(field => field.issueTypeFieldsName === 'Workflow');
      if (workflow && workflow.checked) {
        this.openDefectIssue = true;
        this.isDefectType.emit(true);
        this.getIssuesByIssueType();
      } else {
        this.openIssueModal = true;
        this.isDefectType.emit(false);
        this.loadProcessIssues();
      }
    });
  }

  getIssuesByIssueType() {
    this.issuesByType = this.loadFormatedIssue(this.selectedIssueType.issues);
  }

  loadProcessIssues() {
    if (isNullOrUndefined(this.processDetail.id)) {
      this.processWorkflows = [];
    } else {
        this.issuesByType = this.loadFormatedIssue(this.selectedIssueType.issues);
    }
  }

  loadFormatedIssue(result) {
    return result.map(issue => {
      return {
        ...issue,
        type: this.types[issue.type] ? this.types[issue.type].title : null,
        priority: this.priorities[issue.priority] ? this.priorities[issue.priority].title : null,
        severity: this.severity[issue.severity] ? this.severity[issue.severity].title : null,
        dueDate: issue.dueDate != '0001-01-01T00:00:00' ? new Date(issue.dueDate).toLocaleString() : null
      };
    });
  }

  linkOrUnLinkIssue() {
    if (this.selectedWorkflow && this.selectedIssue) {
      this.processManagmentService.getWorkflowIssues(this.selectedWorkflow.workflowId).subscribe(res => {
        if (res) {
          if (res.find(workflow => workflow.issueId === this.selectedIssue.id)) return (this.unLinkIssue = true);
          this.unLinkIssue = false;
        }
        this.unLinkIssue = false;
      });
    }
  }

  linkOrUnLinkRequirementIssue() {
    if (this.selectedIssue && this.selectedIssue.processId !== this.processDetail.id) return (this.unLinkIssue = false);
    return (this.unLinkIssue = true);
  }

  linkIssueRequirement() {
    this.isDefectType.emit(false);
    this.closeIssuePopupBool.emit();
    this.processManagmentService.linkIssueToProcess(this.selectedIssue.id, this.processDetail.id).subscribe(res => {
      this.allIssues.find(x => x.id === this.selectedIssue.id).processId = this.processDetail.id;
      this.messageService.sendMessage({
        text: 'Process and issue are linked!',
        type: MessageType.Success
      });
    });
  }

  unLinkIssueRequirement() {
    this.isDefectType.emit(false);
    this.closeIssuePopupBool.emit();
    this.processManagmentService.unlinkIssueFromProcess(this.selectedIssue.id).subscribe(res => {
      this.allIssues.find(x => x.id === this.selectedIssue.id).processId = '00000000-0000-0000-0000-000000000000';
      this.messageService.sendMessage({
        text: 'Process and issue unlinked!',
        type: MessageType.Success
      });
    });
  }

  linkWorkflowIssue() {
    this.isDefectType.emit(false);
    this.closeIssuePopupBool.emit();
    this.processManagmentService.addWorkflowIssue(this.selectedWorkflow.workflowId, this.selectedIssue.id).subscribe(res => {
      this.messageService.sendMessage({
        text: 'Workflow and issue are linked!',
        type: MessageType.Success
      });
    });
  }

  unLinkWorkflowIssue() {
    this.isDefectType.emit(false);
    this.closeIssuePopupBool.emit();
    this.processManagmentService.unlinkWorkflowIssue(this.selectedWorkflow.workflowId, this.selectedIssue.id).subscribe(res => {
      this.messageService.sendMessage({
        text: 'Workflow and issue unlinked!',
        type: MessageType.Success
      });
    });
  }
}
