import { Component, OnInit } from '@angular/core';
import { State, process } from '@progress/kendo-data-query';
import { SessionService } from 'src/app/core/services/session.service';
import { DesignService } from 'src/app/design/services/design.service';
import { ProcessManagementService } from '../../../services/process-management.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-roles-view',
  templateUrl: './roles-view.component.html',
  styleUrls: ['./roles-view.component.css']
})
export class RolesViewComponent implements OnInit {
  public roles = [];
  public rolesGridData: any = [];

  workContext: any;
  public roleState: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };

  constructor(
    private processManagment: ProcessManagementService, 
    private sessionService: SessionService,
    public permissionService: PermissionService
  ) {}

  async ngOnInit() {
    const roles = this.permissionService.hasPermission('/api/People/GetRoles') ? await this.getRoles() : [];
    this.roles = roles;
    this.rolesGridData = process(roles, this.roleState);
  }

  getRoles() {
    this.workContext = this.sessionService.getWorkContext();
    return this.processManagment.getRoles(this.workContext.subProject.id).toPromise();
  }

  roleDataStateChange(state) {
    this.roleState = state;
    this.rolesGridData = process(this.roles, state);
  }

  roleListChange(roles) {
    this.roles = roles;
    this.rolesGridData = process(roles, this.roleState);
  }
}
