import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from '../../../services/process-management.service';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-add-edit-role',
  templateUrl: './add-edit-role.component.html',
  styleUrls: ['./add-edit-role.component.css']
})
export class AddEditRoleComponent implements OnInit {
  @Input() isRoleEdited;
  @Input() selectedRole;
  @Input() roleWindowOpen;

  @Output() public closeRoleWindow = new EventEmitter<any>();
  @Output() public roleListUpdated = new EventEmitter<boolean>();
  workContext: any;
  subProjectId: string = '';
  constructor(
    private processManagment: ProcessManagementService,
    private messageService: MessageService,
    private sessionService: SessionService
  ) {}

  ngOnInit() {
    this.workContext = this.sessionService.getWorkContext();
    this.subProjectId = this.sessionService.getWorkContext().subProject.id;
  }

  close() {
    this.closeRoleWindow.emit();
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedRole.title = title;
  }

  changeDescription(event) {
    let description = event.target.value.trim('');
    this.selectedRole.description = description;
  }

  disableSubmitBtn() {
    return this.selectedRole.title === '';
  }

  submitClicked() {
    if (this.isRoleEdited) {
      this.editRole();
    } else {
      this.addRole();
    }
  }

  addRole() {
    this.processManagment.addRole(this.selectedRole.title, this.selectedRole.description, this.subProjectId).subscribe(res => {
      this.handleRoleResponse(res);
    });
  }

  editRole() {
    this.processManagment.updateRole(this.selectedRole.id, this.selectedRole.title, this.selectedRole.description, this.subProjectId).subscribe(res => {
      this.handleRoleResponse(res);
    });
  }

  handleRoleResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isRoleEdited ? 'Role was updated successfully!' : 'Role was added successfully!',
        type: MessageType.Success
      });
      this.roleListUpdated.emit(this.isRoleEdited ? this.selectedRole : { ...this.selectedRole, id: res.id });
    } else {
      this.close();
    }
  }
}
