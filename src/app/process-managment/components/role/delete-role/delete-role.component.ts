import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from '../../../services/process-management.service';

@Component({
  selector: 'app-delete-role',
  templateUrl: './delete-role.component.html',
  styleUrls: ['./delete-role.component.css']
})
export class DeleteRoleComponent implements OnInit {
  @Input() deleteRoleWindowOpen;
  @Input() selectedRole;

  @Output() public closeDeleteRoleWindow = new EventEmitter<any>();
  @Output() public removeRoleFromList = new EventEmitter<boolean>();

  constructor(private processManagment: ProcessManagementService, private messageService: MessageService) {}

  ngOnInit() {}

  deleteRole() {
    this.processManagment.deleteRole(this.selectedRole.id).subscribe(res => {
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({ text: 'Role was deleted successfully!', type: MessageType.Success });
      this.removeRoleFromList.emit(this.selectedRole);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeleteRoleWindow.emit();
  }
}
