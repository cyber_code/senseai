import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import * as _ from 'lodash';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  @Input() gridData;
  @Input() state;
  @Input() roles;

  @Output() roleDataStateChanged = new EventEmitter<any>();
  @Output() roleListChanged = new EventEmitter<any>();

  public selectedRole = {
    subProjectId: '',
    title: '',
    description: ''
  };

  public roleWindowOpen = false;
  public deleteRoleWindowOpen = false;
  public isRoleEdited = false;

  constructor(public permissionService: PermissionService) {}

  ngOnInit() {}

  dataStateChange(state) {
    this.roleDataStateChanged.emit(state);
  }

  openRoleWindow(role?) {
    this.roleWindowOpen = true;
    if (role) {
      this.isRoleEdited = true;
      this.selectedRole = _.cloneDeep(role);
    } else {
      this.resetRole();
    }
  }

  close() {
    this.roleWindowOpen = false;
    this.deleteRoleWindowOpen = false;
    this.resetRole();
  }

  resetRole() {
    this.isRoleEdited = false;
    this.selectedRole = {
      subProjectId: '',
      title: '',
      description: ''
    };
  }

  updateRoleList(role) {
    let roles = [];
    if (this.isRoleEdited) {
      roles = this.roles.map(iType => {
        if (iType.id === role.id) {
          return role;
        }
        return iType;
      });
    } else {
      roles = this.roles.concat(role);
    }
    this.roleListChanged.emit(roles);
    this.close();
  }

  removeRoleFromList(role) {
    let roles = this.roles.filter(iType => iType.id !== role.id);
    this.roleListChanged.emit(roles);
    this.close();
  }

  openDeleteRoleWindow(role) {
    this.deleteRoleWindowOpen = true;
    this.selectedRole = role;
  }

  addRolePermission() {
    return(
      this.permissionService.hasPermission('/api/People/GetRoles') &&
      this.permissionService.hasPermission('/api/People/AddRole')
    );
  }

  editRolePermission() {
    return(
      this.permissionService.hasPermission('/api/People/GetRoles') &&
      this.permissionService.hasPermission('/api/People/UpdateRole')
    );
  }

  deleteRolePermission() {
    return(
      this.permissionService.hasPermission('/api/People/GetRoles') &&
      this.permissionService.hasPermission('/api/People/DeleteRole')
    );
  }
}
