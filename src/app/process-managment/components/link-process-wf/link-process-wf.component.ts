import { Component, OnInit } from '@angular/core';
import { DataManagementService } from '../../../data-management/services/data-management.service';
import { ProcessManagementService } from '../../services/process-management.service';
import { MessageService } from '../../../core/services/message.service';
import { isNullOrUndefined } from 'util';
import { MessageType } from '../../../core/models/message.model';

@Component({
  selector: 'app-link-process-wf',
  templateUrl: './link-process-wf.component.html',
  styleUrls: ['./link-process-wf.component.css']
})
export class LinkProcessWfComponent implements OnInit {
  public processWorkflows: any[] = [];
  public processes: any[] = [];

  public selectedWorkflow: any;
  public selectedProcess: any;

  public folderId = '4f34555b-38d7-4d81-86b9-1612f5158b62';
  public processId = '2146d23f-b8db-4ce0-b7d1-466214976c9b';
  public subProjectId = '3adf6db6-b640-439d-b6d3-1aaa9d9fd626';

  constructor(
    private dataManagementService: DataManagementService,
    private processManagmentService: ProcessManagementService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.loadProcessWorkflows(this.processId);
    this.loadProcesses(this.subProjectId);
  }

  private loadProcessWorkflows(processId) {
    if (isNullOrUndefined(processId)) {
      this.processWorkflows = [];
    } else {
      this.processManagmentService.getProcessWorkflows(processId).subscribe(res => {
        this.processWorkflows = res;
      });
    }
  }

  private loadProcesses(subProjectId) {
    if (isNullOrUndefined(subProjectId)) {
      this.processWorkflows = [];
    } else {
      this.processManagmentService.getProcessesBySubProject(subProjectId).subscribe(res => {
        this.processes = res;
      });
    }
  }

  handleProcessSelection(event) {
    this.selectedProcess = event.selectedRows[0].dataItem;
  }

  handleWorkflowSelection(event) {
    this.selectedWorkflow = event.selectedRows[0].dataItem;
  }

  linkProcessWorkflow() {
    this.processManagmentService.addProcessWorkflow(this.selectedWorkflow.workflowId, this.selectedProcess.id).subscribe(res => {
      this.messageService.sendMessage({
        text: 'Requirement and Workflow are linked!',
        type: MessageType.Success
      });
    });
  }
}
