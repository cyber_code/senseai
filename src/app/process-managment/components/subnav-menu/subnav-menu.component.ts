import { Component, OnInit, Input, TemplateRef, ViewChild, EventEmitter, Output, Inject } from '@angular/core';
import { ProcessManagementService } from '../../services/process-management.service';
import { Guid } from 'src/app/shared/guid';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { WindowService } from '@progress/kendo-angular-dialog';
import { SessionService } from 'src/app/core/services/session.service';
import { Router } from '@angular/router';
import {ProcessType} from '../../models/process-type';
import { DOCUMENT } from '@angular/common';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-subnav-menu',
  templateUrl: './subnav-menu.component.html',
  styleUrls: ['./subnav-menu.component.css']
})
export class SubnavMenuComponent implements OnInit {
  public license = Licenses;
  public hierarchyTypeTitle = '';
  public _selectedHierarchy;

  public openAddProcess = false;
  public openAddHierarchy = false;
  public openDocumentGeneration = false;
  public copyCutProcesses = [];
  public hierarchyTypes = [];
  public processToAdd = {
    title: '',
    description: '',
    hierarchyId: '',
    ownerId: Guid.empty,
    processType: ProcessType.Aris
  };

  public hierarchyToAdd = {
    title: '',
    description: '',
    hierarchyTypeId: '',
    parentId: '',
    parent: {}
  };

  public loading = false;
  public openRequirementDialog = false;

  @Output() hierarchyTypeTitleChanged = new EventEmitter();
  @Output() processDetailsClicked = new EventEmitter();
  @Output() requirementsMoved = new EventEmitter();

  @Input() displayHierarchy;
  @Input() selectedProcess;
  @Input() hierarchies;
  @Input() searchCriteria;
  @Input()
  set selectedHierarchy(selectedHierarchy) {
    if (selectedHierarchy && this.hierarchyTypes) {
      let child = this.hierarchyTypes.find(hrType => hrType.parentId === selectedHierarchy.hierarchyTypeId);
      if (child && this.permissionService.hasPermission('/api/Process/Configure/GetHierarchyType')) {
        this.processManagementService.getHierarchyType(child.id).subscribe(hierarchyType => {
          if (hierarchyType) {
            this.hierarchyTypeTitle = hierarchyType.title;
            this.hierarchyTypeTitleChanged.emit(hierarchyType.title);
          }
        });
      } else {
        this.hierarchyTypeTitle = null;
        this.hierarchyTypeTitleChanged.emit(null);
      }
    } else if (this.hierarchyTypes) {
      this.selectedProcess = null;
      if (!this.hierarchies[0]) {
        this.setParentHierarchyType(this.hierarchyTypes);
      } else if (!this.searchCriteria || this.searchCriteria === '') {
        let hierarchyType = this.hierarchyTypes.find(el => el.id === this.hierarchies[0].hierarchyTypeId);
        if (hierarchyType) {
          this.hierarchyTypeTitle = hierarchyType.title;
          this.hierarchyTypeTitleChanged.emit(hierarchyType.title);
        }
      }
    }
    this._selectedHierarchy = selectedHierarchy;
  }

  @Input()
  set _hierarchyTypes(_hierarchyTypes) {
    this.setParentHierarchyType(_hierarchyTypes);
  }

  @Input() selectedProcesses;

  constructor(
    private processManagementService: ProcessManagementService,
    private messageService: MessageService,
    private windowService: WindowService,
    private sessionService: SessionService,
    public licenseService: LicenseService,
    private router: Router,
    public permissionService: PermissionService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit() {}

  setParentHierarchyType(hierarchyTypes) {
    let parentHierarchy = hierarchyTypes.find(hrc => hrc.parentId === '' || hrc.parentId === Guid.empty);
    if (parentHierarchy) {
      this.hierarchyTypeTitle = parentHierarchy.title;
    } else {
      this.hierarchyTypeTitle = null;
    }
    this.hierarchyTypeTitleChanged.emit(this.hierarchyTypeTitle);
    this.hierarchyTypes = hierarchyTypes;
  }

  addProcessClicked() {
    this.openAddProcess = true;
  }

  closeProcessDialog() {
    this.openAddProcess = false;
    this.resetProcessToAdd();
  }

   closeDocumentGenerationDialog() {
     this.openDocumentGeneration = false;
   }

  addHierarchyClicked() {
    this.openAddHierarchy = true;
  }

  resetProcessToAdd() {
    this.processToAdd = {
      title: '',
      description: '',
      hierarchyId: '',
      ownerId: Guid.empty,
      processType: ProcessType.Aris
    };
  }

  resetHierarchyToAdd() {
    this.hierarchyToAdd = {
      title: '',
      description: '',
      hierarchyTypeId: '',
      parentId: '',
      parent: {}
    };
  }

  closeHierarchyDialog() {
    this.openAddHierarchy = false;
    this.resetHierarchyToAdd();
  }

  updateHierarchyList(hierarchy) {
    this.processManagementService.hierarchyAdded.next(hierarchy);
    this.closeHierarchyDialog();
  }

  updateProcessList(process) {
    this.processManagementService.processAdded.next(process);
    this.closeProcessDialog();
  }

  copyProcess() {
    this.copyCutProcesses = this.selectedProcesses.map(process => {
      return Object.assign({}, { ...process, type: 0 });
    });
  }

  cutProcess() {
    this.copyCutProcesses = this.selectedProcesses.map(process => {
      return Object.assign({}, { ...process, type: 1 });
    });
  }

  pasteProcess() {
    for (let i = 0; i < this.copyCutProcesses.length; i++) {
      let copyCutProcess = this.copyCutProcesses[i];
      this.processManagementService
      .pasteProcess(copyCutProcess.id, this._selectedHierarchy.id, copyCutProcess.type)
      .subscribe(res => {
        if (copyCutProcess.type === 0) {
          this.processManagementService.processAdded.next({
            ...copyCutProcess,
            id: res.processId,
            title: res.processName,
            hierarchyId: this._selectedHierarchy.id
          });
        } else {
          this.processManagementService.processRemoved.next(copyCutProcess);
          this.processManagementService.processAdded.next({
            ...copyCutProcess,
            id: res.processId,
            title: res.processName,
            hierarchyId: this._selectedHierarchy.id
          });
        }
        if (i === this.copyCutProcesses.length - 1) {
          this.messageService.sendMessage({
            text: (this.copyCutProcesses.length > 1) ?  'Requirements was pasted successfully!' : 'Requirement was pasted successfully!',
            type: MessageType.Success
          });
          this.requirementsMoved.emit();
          this.copyCutProcesses = [];
        }
      });
    }
  }

  openProcessDetails() {
    this.processDetailsClicked.emit();
  }

  generateDocumentation() {
    this.openDocumentGeneration = true;
    // this.loading = true;
    //   if (this.selectedProcess === null ) {
    //       this.generateHierarchyDocs(this._selectedHierarchy.id, this.document.location.href);
    //   }
    //   else if (this.selectedProcess !== null ) {
    //       this.generateRequirementDocs(this.selectedProcess.id, this.document.location.href);
    //   }
  }

  moveRequirementClicked() {
    this.openRequirementDialog = true;
  }

  closeRequirementDialog() {
    this.openRequirementDialog = false;
  }

  hierarchyItemSelected(hierarchyItem) {
    for (let i = 0; i < this.selectedProcesses.length; i++) {
      let process = this.selectedProcesses[i];
      this.processManagementService
      .pasteProcess(process.id, hierarchyItem.id, 1)
      .subscribe(res => {
          this.processManagementService.processRemoved.next(process);
          this.processManagementService.processAdded.next({
            ...process,
            id: res.processId,
            title: res.processName,
            hierarchyId: hierarchyItem.id
          });
          if (i === this.selectedProcesses.length - 1) {
            this.closeRequirementDialog();
            this.messageService.sendMessage({
              text: 'Requirements were moved successfully!',
              type: MessageType.Success
            });
            this.requirementsMoved.emit();
          }
      });
    }
  }

  generateHierarchyDocs(id, uiURL) {
    this.processManagementService.GenerateHierarchyDocs(id, uiURL).subscribe(
      (x: any) => {
        const newBlob = new Blob([x], { type: 'application/vxml' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = 'HierarchyDocumentation.zip';
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
        this.loading = false;
        setTimeout(function() {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      },
      error => {
        this.loading = false;
        this.messageService.sendMessage({ text: 'There was an error during the download of the file!', type: MessageType.Error });
      }
    );
  }

  generateRequirementDocs(id: string, uiURL: string) {
    this.processManagementService.GenerateRequirementDocs(id, uiURL).subscribe(
      (x: any) => {
        const newBlob = new Blob([x], { type: 'application/vxml' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = 'RequirementDocumentation.zip';
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
        this.loading = false;
        setTimeout(function() {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      },
      error => {
        this.loading = false;
        this.messageService.sendMessage({ text: 'There was an error during the download of the file!', type: MessageType.Error });
      }
    );
  }

  hasHierarchyPermission() {
    return (
      this.permissionService.hasPermission('/api/Process/Browse/GetHierarchiesByParent') &&
      this.permissionService.hasPermission('/api/Process/Browse/AddHierarchy') &&
      this.permissionService.hasPermission('/api/Process/Browse/GetHierarchy')
    );
  }

  hasGenerateDocumentationPermission() {
    if (this.selectedProcess) {
      return this.permissionService.hasPermission('/api/Process/Browse/GenerateRequirementDocs');
    }
    else if (this._selectedHierarchy) {
      return this.permissionService.hasPermission('/api/Process/Browse/GenerateHierarchyDocs');
    }
  }

  moveRequirementPermissions() {
    return (
      this.permissionService.hasPermission('/api/Process/Browse/GetHierarchiesByParent') &&
      this.permissionService.hasPermission('/api/Process/Browse/GetProcesses') &&
      this.permissionService.hasPermission('/api/Process/Browse/PasteProcess')
    );
  }

  requirementPermissions() {
    return (
      this.permissionService.hasPermission('/api/Process/Browse/GetHierarchiesByParent') &&
      this.permissionService.hasPermission('/api/Process/Browse/AddProcess')
    );
  }

  showNavbar() {
    return (
      this.selectedProcess ||
      (this.hasHierarchyPermission() ||
      (this._selectedHierarchy && this.hasGenerateDocumentationPermission()) ||
      (this._selectedHierarchy && (!this.hierarchyTypeTitle || 
        this.hierarchyTypeTitle === '') && 
        this.requirementPermissions()))
    );
  }
}
