import { Component, OnInit } from '@angular/core';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public license = Licenses;

  constructor(public licenseService: LicenseService) {}

  ngOnInit() {}

}
