import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ImportHierarchyService } from '../../services/import-hierarchy.service';
import { MessageService } from "src/app/core/services/message.service";
import { MessageType } from "src/app/core/models/message.model";
import { SessionService } from 'src/app/core/services/session.service';
import { of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-import-hierarchy',
  templateUrl: './import-hierarchy.component.html',
  styleUrls: ['./import-hierarchy.component.css']
})

export class ImportHierarchyComponent implements OnInit {
    public loading = false;
    public file = null;
    public confirmForceDelete: boolean = false;
    public forceDelete: boolean= false;

    @ViewChild('fileInput') fileInput: ElementRef;

    constructor(
        private importHierarchyService: ImportHierarchyService,
        private messageService: MessageService,
        private sessionService: SessionService,
        private authenticationService: AuthenticationService,
        private router: Router
    ) {}

    ngOnInit() {}

    openOrCloseConfirmDialog(){
        this.confirmForceDelete = !this.confirmForceDelete;
    }

    submitForceDelete(){
        this.forceDelete = true;
        this.onSubmit();
    }

    onSubmit() {
        if (!this.file) {
            this.messageService.sendMessage({text: 'Please, select a file to upload!', type: MessageType.Warning});
            return; 
        }
        let subProjectId = this.sessionService.getSubProject().id;
        const formModel = new FormData();
        formModel.append('file', this.file);
        formModel.append('subProjectId', subProjectId);
        formModel.append('ForceDelete', JSON.stringify(this.forceDelete));
        this.loading = true;
        this.importHierarchyService
        .importHierarchy(formModel)
        .pipe(
            catchError(err => {
                this.errorCatched(err);
                return of(null);
            })
        )
        .subscribe(event => {
            if (event.type === 4) {
                this.loading = false;
                if (event.status === 200) {
                    if (event.body.result) {
                        this.messageService.sendMessage({text: 'File was uploaded successfully!', type: MessageType.Success});
                    } else {
                        if (event && event.body && event.body.hasResult && event.body.response.messageType === 3)
                            this.openOrCloseConfirmDialog();
                        else 
                            this.messageService.sendMessage({text: 'There was an error during the upload of the file!', type: MessageType.Error});
                    }
                }
                if (this.forceDelete){
                    this.forceDelete = false;
                    this.openOrCloseConfirmDialog();
                }
            }
        });
    }

    errorCatched(err) {
        if (err.status === 401) {
          this.authenticationService.logout();
          this.router.navigateByUrl('/login');
          return;
        }
        this.messageService.sendMessage({text: 'There was an error during the upload of the file!', type: MessageType.Error});
        this.loading = false;
      }

    onFileChange(event) {
        let file = event.target.files[0];
        if (file) {
            this.file = file;
        }
    }

    clearFile() {
        this.fileInput.nativeElement.value = '';
        this.file = undefined;
        this.loading = false;
    }

}
