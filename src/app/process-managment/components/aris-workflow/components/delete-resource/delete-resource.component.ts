import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';

@Component({
  selector: 'app-delete-process-resource',
  templateUrl: 'delete-resource.component.html',
  styleUrls: ['delete-resource.component.css']
})
export class DeleteProcessResourceComponent implements OnInit {
  public _deleteProcessResourceWindowOpen;
  public message = 'Are you sure you want to delete this attachment?';

  @Input() selectedProcessResource;

  @Input()
  set deleteProcessResourceWindowOpen(deleteProcessResourceWindowOpen) {
    this._deleteProcessResourceWindowOpen = deleteProcessResourceWindowOpen;
  }

  @Output() public closeDeleteProcessResourceWindow = new EventEmitter<any>();
  @Output() public removeProcessResourceFromList = new EventEmitter<any>();

  constructor(private processManagementService: ProcessManagementService, private messageService: MessageService) {}

  ngOnInit() {}

  deleteProcessResource() {
    this.processManagementService.deleteProcessResources(this.selectedProcessResource.id).subscribe((res: any) => {
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({ text: 'Attachment was deleted successfully!', type: MessageType.Success });
      this.removeProcessResourceFromList.emit(this.selectedProcessResource);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeleteProcessResourceWindow.emit();
  }
}
