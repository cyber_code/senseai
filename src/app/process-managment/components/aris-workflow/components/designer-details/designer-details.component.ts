import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { WorkFlowComponent } from '../work-flow/work-flow.component';
import { ActivatedRoute } from '@angular/router';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { DetailsComponent } from '../details/details.component';
import { PermissionService } from 'src/app/core/services/permission.service';
import { ProcessType } from 'src/app/process-managment/models/process-type';

@Component({
  selector: 'app-designer-details',
  templateUrl: './designer-details.component.html',
  styleUrls: ['./designer-details.component.css']
})
export class DesignerDetailsComponent implements OnInit {

  @Input() public processDetail;
  @Input() allIssues;

  @Output() public goBackClicked = new EventEmitter();
  @Output() public ImportAris = new EventEmitter();

  @ViewChild(WorkFlowComponent)
  private workflowComponent: WorkFlowComponent;

  public showTimeVariance = false;

  constructor(
    private processManagementService: ProcessManagementService,
    public permissionService: PermissionService
  ) {}

  ngOnInit() {

  }

  ngAfterViewInit() {
    if (this.processDetail && 
        (this.processDetail.processType === ProcessType.Aris && 
          this.permissionService.hasPermission('/api/Process/ArisWorkflow/GetArisWorkflowVersionsByDate')) ||
        (this.processDetail.processType === ProcessType.Generic && 
          this.permissionService.hasPermission('/api/Process/GenericWorkflow/GetGenericWorkflowVersionsByDate'))
      ) {
        var ul = document.getElementsByClassName("k-tabstrip-items")[0];
        var li = document.createElement("li");
        li.appendChild(document.createTextNode("Time Variance"));
        li.className = 'time-variance';
        li.style.marginTop = '0.4rem';
        li.style.position = 'absolute';
        li.style.right = '5rem';
        li.style.color = '#007bff';
        li.style.cursor = 'pointer';
        li.onclick = () => {
          this.timeVarianceClicked();
        }
        ul.appendChild(li);
      }
  }


  saveClicked() {
    if (this.workflowComponent) {
      this.workflowComponent.saveClicked();
    }
  }

  issueClicked() {
    if (this.workflowComponent) {
      this.workflowComponent.isIssueOpen = true;
    }
  }

  generateClicked() {
    if (this.workflowComponent) {
      this.workflowComponent.generateAris();
    }
  }

  importClicked() {
    if (this.workflowComponent) {
      this.workflowComponent.isImportOpen = true;
    }
  }

  backClicked(event) {
    this.goBackClicked.emit(event);
  }

  timeVarianceClicked() {
    this.showTimeVariance = true;
  }

  closeTimeVariance() {
    this.showTimeVariance = false;
  }

  pageChanges(event) {
    let element = document.getElementsByClassName('time-variance')[0] as any;
    if (event && event.index === 1) {
      if (element) {
        element.style.display = 'none';
      }
    } else {
      if (element) {
        element.style.display = 'block';
      }
    }
  }
}
