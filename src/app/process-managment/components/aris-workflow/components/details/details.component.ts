import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { WorkflowIssuesDataService } from 'src/app/process-managment/services/workflow-issues-data.service';
import { ProcessResourcesService } from 'src/app/process-managment/services/process-resources.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  public _processDetail;
  public addedResources = [];

  @Input() allIssues;

  @Input()
  set processDetail(processDetail) {
    if (processDetail) {
      this._processDetail = processDetail;
    }
  }

  constructor(
    private processManagementService: ProcessManagementService,
    private workflowIssuesDataService: WorkflowIssuesDataService,
    private processResourcesService: ProcessResourcesService,
    private messageService: MessageService,
    public permissionSerivce: PermissionService
  ) {}

  ngOnInit() {
  }

  onFileSelected(event) {
    let fileList = event.target.files;
    Object.keys(fileList).forEach(key => {
      fileList[key].fileName = fileList[key].name;
    });
    if (fileList && fileList.length > 0) {
      this.processResourcesService.addProcessResources(this._processDetail.id, fileList).subscribe(res => {
        if (res) {
          this.messageService.sendMessage({ text: 'Attachments were added successfully!', type: MessageType.Success });
          this.addedResources = fileList;
        } else {
          this.addedResources = [];
        }
      });
    }
  }
}
