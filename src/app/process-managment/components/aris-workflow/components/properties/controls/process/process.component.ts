import {ArisWorkflowItemCustomData} from '../../../../../../models/aris-workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from '../../../../../../services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-process',
  templateUrl: './process.component.html',
  styleUrls: ['./process.component.css']
})
@AutoUnsubscribe()
export class ProcessComponent implements OnInit {
  public workflowItemData: ArisWorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected ? 
        this.workflowItemService.workflowItemSelected.customData : {} as ArisWorkflowItemCustomData;
  }

  ngOnInit() {}

  processChanged(process) {
    this.workflowItemService.workflowItemSelectedModified.next(process);
  }
} 

