import {ArisWorkflowItemCustomData} from '../../../../../../models/aris-workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from '../../../../../../services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-file-type',
  templateUrl: './file-type.component.html',
  styleUrls: ['./file-type.component.css']
})
@AutoUnsubscribe()
export class FileTypeComponent implements OnInit {
  public workflowItemData: ArisWorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected ? 
        this.workflowItemService.workflowItemSelected.customData : {} as ArisWorkflowItemCustomData;
  }

  ngOnInit() {}

  fileTypeChanged(fileType) {
    this.workflowItemData.FileType = fileType;
  }
} 
