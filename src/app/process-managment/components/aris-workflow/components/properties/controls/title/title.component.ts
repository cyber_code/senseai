import {ArisWorkflowItemCustomData} from '../../../../../../models/aris-workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from '../../../../../../services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import { GenericWorkflowItemCustomData } from 'src/app/process-managment/models/generic-workflow-item-custom-data.model';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
@AutoUnsubscribe()
export class TitleComponent implements OnInit {
  public workflowItemData: ArisWorkflowItemCustomData | GenericWorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
      this.workflowItemData = this.workflowItemService.workflowItemSelected ? 
        this.workflowItemService.workflowItemSelected.customData : {} as ArisWorkflowItemCustomData | GenericWorkflowItemCustomData;
  }

  ngOnInit() {}

  titleChanged(newValue: string) {
    this.workflowItemService.workflowItemSelectedModified.next(newValue);
  }
}
