import {ArisWorkflowItemCustomData} from '../../../../../../models/aris-workflow-item-custom-data.model';
import {GenericWorkflowItemCustomData} from '../../../../../../models/generic-workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from '../../../../../../services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
@AutoUnsubscribe()
export class DescriptionComponent implements OnInit {
  public workflowItemData: ArisWorkflowItemCustomData | GenericWorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected ?
        this.workflowItemService.workflowItemSelected.customData : {} as ArisWorkflowItemCustomData | GenericWorkflowItemCustomData;
  }

  ngOnInit() {}

  changeDescription(description) {
    this.workflowItemData.Description = description;
  }
}
