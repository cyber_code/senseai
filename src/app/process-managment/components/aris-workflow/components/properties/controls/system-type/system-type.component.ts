import {ArisWorkflowItemCustomData} from '../../../../../../models/aris-workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from '../../../../../../services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-system-type',
  templateUrl: './system-type.component.html',
  styleUrls: ['./system-type.component.css']
})
@AutoUnsubscribe()
export class SystemTypeComponent implements OnInit {
  public workflowItemData: ArisWorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected ? 
        this.workflowItemService.workflowItemSelected.customData : {} as ArisWorkflowItemCustomData;
  }

  ngOnInit() {}

  systemTypeChanged(systemType) {
    this.workflowItemService.workflowItemSelectedModified.next(systemType);
  }
} 

