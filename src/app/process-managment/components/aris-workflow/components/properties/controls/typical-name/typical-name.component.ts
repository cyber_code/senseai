import {ArisWorkflowItemCustomData} from '../../../../../../models/aris-workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from '../../../../../../services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-typical-name',
  templateUrl: './typical-name.component.html',
  styleUrls: ['./typical-name.component.css']
})
@AutoUnsubscribe()
export class TypicalNameComponent implements OnInit {
  public workflowItemData: ArisWorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected ? 
        this.workflowItemService.workflowItemSelected.customData : {} as ArisWorkflowItemCustomData;
  }

  ngOnInit() {}

  typicalNameChanged(typicalName) {
    this.workflowItemData.TypicalName = typicalName;
  }
} 
