import {ArisWorkflowItemCustomData} from '../../../../../../models/aris-workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from '../../../../../../services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-screen',
  templateUrl: './screen.component.html',
  styleUrls: ['./screen.component.css']
})
@AutoUnsubscribe()
export class ScreenComponent implements OnInit {
  public workflowItemData: ArisWorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected ? 
        this.workflowItemService.workflowItemSelected.customData : {} as ArisWorkflowItemCustomData;
  }

  ngOnInit() {}

  screenChanged(screen) {
    this.workflowItemData.Screen = screen;
  }
} 
