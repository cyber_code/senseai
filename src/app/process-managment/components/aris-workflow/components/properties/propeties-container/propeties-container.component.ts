import {Component, OnInit, Input} from '@angular/core';
import {ArisWorkflowType} from '../../../../../../core/models/aris-workflow-item-type';
import {GenericWorkflowType} from '../../../../../../core/models/generic-workflow-item-type';

@Component({
  selector: 'app-propeties-container',
  templateUrl: './propeties-container.component.html',
  styleUrls: ['./propeties-container.component.css']
})
export class PropetiesContainerComponent implements OnInit {

  @Input() type: ArisWorkflowType | GenericWorkflowType;
  @Input() isArisWorkflow: Boolean;
  
  public ArisWorkflowType = ArisWorkflowType;
  public GenericWorkflowType = GenericWorkflowType;

  constructor() {
  }

  ngOnInit() {
  }
}
