import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {WorkflowItemCustomData} from 'src/app/design/models/workflow-item-custom-data.model';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-activity-item',
  templateUrl: './activity-item.component.html',
  styleUrls: ['./activity-item.component.css']
})

@AutoUnsubscribe()
export class ActivityItemComponent implements OnInit {

  public workflowItemData: WorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    if (this.workflowItemService.workflowItemSelected) {
        this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    }
  }

  ngOnInit() {}

}
