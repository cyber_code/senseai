import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {WorkflowItemCustomData} from 'src/app/design/models/workflow-item-custom-data.model';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-resource-item',
  templateUrl: './resource-item.component.html',
  styleUrls: ['./resource-item.component.css']
})

@AutoUnsubscribe()
export class ResourceItemComponent implements OnInit {

  public workflowItemData: WorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    if (this.workflowItemService.workflowItemSelected) {
        this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    }
  }

  ngOnInit() {}

}
