import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {WorkflowItemCustomData} from 'src/app/design/models/workflow-item-custom-data.model';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-event-item',
  templateUrl: './event-item.component.html',
  styleUrls: ['./event-item.component.css']
})

@AutoUnsubscribe()
export class EventItemComponent implements OnInit {

  public workflowItemData: WorkflowItemCustomData;

  constructor(private workflowItemService: WorkflowItemService) {
    if (this.workflowItemService.workflowItemSelected) {
        this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    }
  }

  ngOnInit() {}

}
