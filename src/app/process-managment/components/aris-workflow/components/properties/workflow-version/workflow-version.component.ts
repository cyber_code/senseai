import {Router} from '@angular/router';
import {Component, OnInit, Input} from '@angular/core';
import {WorkflowItemService} from '../../../../../services/workflow-item.service';
import {isNullOrUndefined} from 'util';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';

@Component({
  selector: 'app-workflow-version',
  templateUrl: './workflow-version.component.html',
  styleUrls: ['./workflow-version.component.css']
})
@AutoUnsubscribe()
export class WorkflowVersionComponent implements OnInit {
  public workflowVersions = [];
  public selectedWorkflowVersion = {} as any;
  public previousWorkflowVersion = {};
  public rollbackDialogOpen = false;
  public versionsDisabled = false;
  public 
  @Input() isArisWorkflow;
  @Input()
  set workflowId(workflowId) {
    this.selectedWorkflowVersion = {};
    this.loadWorkflowVersions(workflowId);
  }

  constructor(
    private processManagementService: ProcessManagementService, 
    private router: Router, 
    private workflowService: WorkflowItemService
  ) {

    this.workflowService.workflowSelectionChanged$.pipe(takeWhileAlive(this)).subscribe(workflowId => {
      this.loadWorkflowVersions(workflowId);
    });
  }

  ngOnInit() {
  }

  rollback() {
    if (this.isArisWorkflow) {
      this.processManagementService.rollbackArisWorkflow(this.selectedWorkflowVersion.arisWorkflowId, this.selectedWorkflowVersion.version).subscribe(() => {
        this.workflowService.workflowVersionChanged.next(this.selectedWorkflowVersion);
        this.loadWorkflowVersions(this.selectedWorkflowVersion.arisWorkflowId);
      });
    } else {
      this.processManagementService.rollbackGenericWorkflow(this.selectedWorkflowVersion.genericWorkflowId, this.selectedWorkflowVersion.version).subscribe(() => {
        this.workflowService.workflowVersionChanged.next(this.selectedWorkflowVersion);
        this.loadWorkflowVersions(this.selectedWorkflowVersion.genericWorkflowId);
      });
    }
    this.rollbackDialogOpen = false;
  }

  open(version) {
    this.previousWorkflowVersion = this.selectedWorkflowVersion;
    this.selectedWorkflowVersion = version;
    this.rollbackDialogOpen = true;
  }

  close() {
    this.rollbackDialogOpen = false;
    this.selectedWorkflowVersion = this.previousWorkflowVersion;
  }
  
  loadWorkflowVersions(workflowId: string) {
    if (isNullOrUndefined(workflowId)) {
      this.workflowVersions = [];
      this.versionsDisabled = true;
    } else {
      if (this.isArisWorkflow) {
        this.processManagementService.getArisWorkflowVersions(workflowId).subscribe(result => {
          if (result) {
            result.map(x => {
              x['versionText'] = `Version : ${x.version}`;
            });
            this.workflowVersions = result;
            this.versionsDisabled = false;
          }
        });
      } else {
        this.processManagementService.getGenericWorkflowVersions(workflowId).subscribe(result => {
          if (result) {
            result.map(x => {
              x['versionText'] = `Version : ${x.version}`;
            });
            this.workflowVersions = result;
            this.versionsDisabled = false;
          }
        });
      }
    }
  }
}
