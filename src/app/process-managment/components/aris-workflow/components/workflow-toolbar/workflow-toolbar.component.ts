import {Component, EventEmitter, OnInit, Output, ViewChild, Input} from '@angular/core';
import {SubProject} from 'src/app/core/models/sub-project.model';
import {AdministrationService} from 'src/app/core/services/administration.service';
import {SessionService} from 'src/app/core/services/session.service';
import {ArisNodeType} from 'src/app/shared/aris-node-type';
import {TreeViewItem} from 'src/app/shared/tree-view-item';
import {MenuAction} from '../../../../models/menu-action';
import {ProcessManagementService} from '../../../../services/process-management.service';
import {MessageService} from '../../../../../core/services/message.service';
import {Router} from '@angular/router';
import {MenuItem} from '../../../../models/menu-item.model';
import {MenuComponent} from '@progress/kendo-angular-menu';
import {ArisTreeViewItem} from 'src/app/shared/aris-tree-view-item';
import {Guid} from 'src/app/shared/guid';
import {MessageType} from 'src/app/core/models/message.model';
import {ImportArisWfService} from '../../../../services/import-aris-workflow.service';
import {isNullOrUndefined} from '@syncfusion/ej2-base';
import {SortDescriptor} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridComponent} from '@progress/kendo-angular-grid';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-workflow-toolbar',
  templateUrl: './workflow-toolbar.component.html',
  styleUrls: ['./workflow-toolbar.component.css']
})


export class WorkflowToolbarComponent implements OnInit {
  constructor(
    private router: Router,
    private session: SessionService,
    private processManagementService: ProcessManagementService,
    private messageService: MessageService,
    private importArisWorkflowService: ImportArisWfService,
    public permissionService: PermissionService
  ) {
  }
  @Output() public saveClicked = new EventEmitter<ArisTreeViewItem>(); //
  @Output() public issueClicked = new EventEmitter();  //
  @Output() public generateClicked = new EventEmitter();
  @Output() public importClicked = new EventEmitter(); //
  @Output() public backClicked = new EventEmitter();
  @ViewChild(GridComponent) grid: GridComponent;

  public _processDetail;
  public processStatistics = {
    nrWorkflows: 0,
    nrTotalIssues: 0,
    nrOpenIssues: 0
  };

  public openWfPopup: boolean = false;
  public openIssuePopup: boolean = false;
  public processWorkflows: any[] = [];
  public folderWorkflows: any[] = [];
  public subProjectFolders: any;
  public processIssues: any[] = [];
  public sort: Array<SortDescriptor> = [];
  public pageSize = 10;
  public skip = 0;
  public selectedFolder:any;
  public openMoveProcessWindow = false;
  public descriptionWidth = '';
  public isDefect = false;
  @Input() allIssues;
  @Input()
  set processDetail(processDetail) {
    if (processDetail) {
      this._processDetail = processDetail;
      if (this.permissionService.hasPermission('/api/Process/Browse/GetProcessStatistics')) {
        this.processManagementService.getProcessStatistics(processDetail.id).subscribe(processStatistics => {
          if (processStatistics) {
            this.processStatistics = processStatistics;
          }
        });
      }
    }
  };


  ngOnInit() {
  }


  public dataStateChange({ skip, take, sort }: DataStateChangeEvent): void {

    // Save the current state of the Grid component
    this.skip = skip;
    this.pageSize = take;
    this.sort = sort;
  }

  public decreaseWorkflowStatistic() {
    this.processStatistics.nrWorkflows -= 1;
  }

  public increaseWorkflowStatistic() {
    this.processStatistics.nrWorkflows += 1;
  }

  isDefectType(isDefect){
    this.isDefect = isDefect;
  }

  public loadFolderWorkflows(folderId){
    if (isNullOrUndefined(folderId)) {
      this.folderWorkflows = [];
    } else {
      this.processManagementService.getWorkflows(folderId).subscribe(res => {
        this.folderWorkflows = res;
      });
    }
  }

  closeIssuePopupBool(){
    this.openIssuePopup = false;
    this.isDefect=false;
  }
  closeWfPopup() {
    this.openWfPopup = false;
  }

  private loadFolders() {
    let subProjectId = this.session.getSubProject().id;
    if (isNullOrUndefined(subProjectId)) {
      this.subProjectFolders = [];
    } else {
      this.processManagementService.getFolders(subProjectId).subscribe(res => {
        this.subProjectFolders = res;
      });
    }
  }

  private loadWorkflows(processId) {
    if (isNullOrUndefined(processId)) {
      this.processWorkflows = [];
    } else {
      this.processManagementService.getProcessWorkflows(processId).subscribe(res => {
        this.processWorkflows = res;
      });
    }
  }
  private loadIssues(processId) {
    if (isNullOrUndefined(processId)) {
      this.processIssues = [];
    } else {
      this.processIssues = this.allIssues.filter(issue => issue.processId === processId);
    }
  }

  saveClick() {
    this.saveClicked.emit();
  }

  issueClick() {
    this.issueClicked.emit();
  }

  generateClick() {
    this.generateClicked.emit();
  }

  importClick() {
    this.importClicked.emit();
  }

  goBack() {
    this.backClicked.emit(this._processDetail);
  }


  linkIssuePopup(processDetails) {
    this.loadWorkflows(processDetails.id);
    this.loadIssues(processDetails.id);
    this.openIssuePopup = true;
  }

  closeMoveProcessWindow() {
    this.openMoveProcessWindow = false;
  }

  moveProcessClicked() {
    this.openMoveProcessWindow = true;
  }

  onResizeEnd(event) {
    this.descriptionWidth = event.rectangle.width + 'px';
  }

  closeIssuePopup() {
    this.openIssuePopup = false;
  }

  linkWfPopup(processDetails) {
    this.loadFolders();
    this.loadWorkflows(processDetails.id);
    this.openWfPopup = true;
  }

  linkIssue(issueId){
    this.openIssuePopup = false;
    this.processManagementService.addWorkflowIssue('0000-0000-0000-0000', issueId).subscribe(res => {
      this.messageService.sendMessage({
        text: 'Workflow and issue linked!',
        type: MessageType.Success
      });
    });
  }

  processUpdated(process) {
    this._processDetail = process;
  }

  linkWorkflowProcessPermission() {
    return(
      this.permissionService.hasPermission('/api/Design/GetFolders') &&
      this.permissionService.hasPermission('/api/Process/Browse/GetProcessWorkflows') &&
      this.permissionService.hasPermission('/api/Process/Browse/AddProcessWorkflow') &&
      this.permissionService.hasPermission('/api/Process/Browse/DeleteProcessWorkflow')
    );
  }

  linkIssueProcessPermission() {
    return(
      this.permissionService.hasPermission('/api/Process/Browse/GetProcessWorkflows') &&
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypes') &&
      this.permissionService.hasPermission('/api/Process/Configure/GetIssueTypeFields') &&
      this.permissionService.hasPermission('/api/Process/Browse/GetWorkflowIssues') &&
      this.permissionService.hasPermission('/api/Process/Browse/LinkIssueToProcess') &&
      this.permissionService.hasPermission('/api/Process/Browse/UnlinkIssueFromProcess') &&
      this.permissionService.hasPermission('/api/Process/Browse/AddWorkflowIssue') &&
      this.permissionService.hasPermission('/api/Process/Browse/UnlinkWorkflowIssue')
    );
  }
}
