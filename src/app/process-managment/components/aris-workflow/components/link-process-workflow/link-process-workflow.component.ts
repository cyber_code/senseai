import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProcessManagementService } from '../../../../services/process-management.service';
import { isNullOrUndefined } from '@syncfusion/ej2-base';
import { SortDescriptor } from '@progress/kendo-data-query';
import { DataStateChangeEvent, PageChangeEvent } from '@progress/kendo-angular-grid';
import { MessageType } from '../../../../../core/models/message.model';
import { MessageService } from '../../../../../core/services/message.service';

@Component({
  selector: 'app-link-process-workflow',
  templateUrl: './link-process-workflow.component.html',
  styleUrls: ['./link-process-workflow.component.css']
})
export class LinkProcessWorkflowComponent implements OnInit {
  @Input() public folder: any;
  @Input() public processDetail: any;
  public processWorkflows: any[] = [];
  public folderWorkflows: any[] = [];
  public sort: Array<SortDescriptor> = [];
  public skip = 0;
  public pageSize = 5;
  public latestSelectedWorkflowId: any;

  constructor(private processManagementService: ProcessManagementService, private messageService: MessageService) {}

  @Output() public closeWfPopupBool = new EventEmitter();
  @Output() public decreaseWorkflowStatistic = new EventEmitter();
  @Output() public increaseWorkflowStatistic = new EventEmitter();

  public ngOnInit(): void {
    this.loadFolderWorkflows();
  }

  public loadFolderWorkflows() {
    if (isNullOrUndefined(this.folder)) {
      this.folderWorkflows = [];
    } else {
      this.processManagementService.getWorkflows(this.folder.id).subscribe(res => {
        this.folderWorkflows = res;
        this.processManagementService.getProcessWorkflows(this.processDetail.id).subscribe(res => {
          this.processWorkflows = res;
          this.folderWorkflows.forEach(wf => {
            wf.linkStatus = 0;
            wf.linkProcessWorkflowId = null;
            this.processWorkflows.forEach(pw => {
              if (wf.id == pw.workflowId) {
                wf.linkStatus = 1;
                wf.linkProcessWorkflowId = pw.id;
              }
            });
          });
        });
      });
    }
  }

  public dataStateChange({ skip, take, sort }: DataStateChangeEvent): void {
    this.skip = skip;
    this.pageSize = take;
    this.sort = sort;
  }

  linkWf(workflowId) {
    this.latestSelectedWorkflowId = workflowId;
    this.processManagementService.addProcessWorkflow(this.processDetail.id, workflowId).subscribe(
      res => {
        if (res) {
          this.increaseWorkflowStatistic.emit();
          this.messageService.sendMessage({
            text: 'Requirement and Workflow are linked!',
            type: MessageType.Success
          });
        }
        this.closeWfPopupBool.emit();
      },
      err => {
        this.closeWfPopupBool.emit();
      }
    );
  }

  unLinkWf(linkProcessWorkflowId) {
    this.processManagementService.deleteProcessWorkflow(linkProcessWorkflowId).subscribe(
      res => {
        if (res) {
          this.decreaseWorkflowStatistic.emit();
          this.messageService.sendMessage({
            text: 'Workflow is unlinked!',
            type: MessageType.Success
          });
        }
        this.closeWfPopupBool.emit();
      },
      err => {
        this.closeWfPopupBool.emit();
      }
    );
  }
}
