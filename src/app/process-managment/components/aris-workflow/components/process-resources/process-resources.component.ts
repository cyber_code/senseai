import { Component, OnInit, Input } from '@angular/core';
import { MessageType } from "src/app/core/models/message.model";
import { MessageService } from "src/app/core/services/message.service";
import { ProcessResourcesService } from 'src/app/process-managment/services/process-resources.service';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import * as _ from 'lodash';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-process-resources',
  templateUrl: './process-resources.component.html',
  styleUrls: ['./process-resources.component.css']
})
export class ProcessResourcesComponent implements OnInit {

  @Input() processDetail;
  public resources = [];
  public deleteProcessResourceWindowOpen = false;
  public openResourceWindow = false;
  public resourceToDelete;
  public resourceToEdit;

  @Input() 
  set addedResources(addedResources) {
      if (addedResources && addedResources.length > 0 && this.permissionService.hasPermission('/api/Process/Browse/GetProcessResources')) {
        this.processManagementService.getProcessResources(this.processDetail.id).subscribe((res: any) => {
            if (res) {
                this.resources = res;
            }
        });
      }
  }

  constructor(
    private processManagementService: ProcessManagementService,
    private processResourcesService: ProcessResourcesService,
    private messageService: MessageService,
    public permissionService: PermissionService
  ) {
  }

  ngOnInit() {
    if (this.permissionService.hasPermission('/api/Process/Browse/GetProcessResources')) {
      this.processManagementService.getProcessResources(this.processDetail.id).subscribe((res: any) => {
        if (res) {
            this.resources = res;
        }
      });
    }
  }

  deleteResourceClicked(resource) {
      this.resourceToDelete = resource;
      this.deleteProcessResourceWindowOpen = true;
  }

  closeDeleteProcessResourceWindow() {
      this.deleteProcessResourceWindowOpen = false;
  }

  removeProcessResourceFromList(resourceToDelete) {
      this.resources = this.resources.filter(resource => resource.id !== resourceToDelete.id);
      this.closeDeleteProcessResourceWindow();
  }

  editResourceClicked(resource) {
    this.resourceToEdit = Object.assign({}, resource);
    this.openResourceWindow = true;
  }

  closeResourceWindow() {
    this.openResourceWindow = false;
  }

  resourceListUpdated(resourceToUpdate) {
    let resources = this.resources.map(resource => {
        if (resource.id === resourceToUpdate.id) {
            return resourceToUpdate;
        }
        return resource;
    });
    this.resources = resources;
    this.closeResourceWindow();
  }

  downloadResource(resource) {
    this.processResourcesService.downloadProcessResource(resource.id).subscribe(
      (x: any) => {
        const newBlob = new Blob([x], {type: 'application/vxml'});
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = resource.fileName;
        link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      },
      error => {
        this.messageService.sendMessage({text: 'There was an error during the download of the file!', type: MessageType.Error});
      }
    );
  }
}
