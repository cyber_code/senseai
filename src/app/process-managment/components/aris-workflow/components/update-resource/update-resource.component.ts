import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { ProcessResourcesService } from 'src/app/process-managment/services/process-resources.service';

@Component({
  selector: 'app-update-resource',
  templateUrl: 'update-resource.component.html',
  styleUrls: ['update-resource.component.css']
})
export class UpdateResourceComponent implements OnInit {
  @Input() processDetail;
  public _openResourceWindow;
  @Input() selectedResource;
  @Input()
  set openResourceWindow(openResourceWindow) {
    this._openResourceWindow = openResourceWindow;
  }

  @Output() public closeResourceWindow = new EventEmitter<any>();
  @Output() public resourceListUpdated = new EventEmitter<any>();
  public file;

  constructor(private messageService: MessageService, private processResourceService: ProcessResourcesService) {}

  ngOnInit() {}

  close() {
    this.closeResourceWindow.emit();
  }

  submitClicked() {
    if (!this.file) {
      this.messageService.sendMessage({
        text: 'Attachment was updated successfully!',
        type: MessageType.Success
      });
      this.close();
      return;
    }

    this.processResourceService
      .updateProcessResource(this.selectedResource.id, this.file.name, this.processDetail.id, this.file)
      .subscribe(res => {
        if (res) {
          this.handleResourceResponse(res);
        }
      });
  }

  fileSelected(event) {
    let file = event.target.files[0];
    if (file) {
      this.file = file;
    }
  }

  async handleResourceResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: 'Attachment was updated successfully!',
        type: MessageType.Success
      });
      this.resourceListUpdated.emit({ ...this.file, fileName: this.file.name, id: this.selectedResource.id });
      this.file = null;
    } else {
      this.close();
      this.file = null;
    }
  }
}
