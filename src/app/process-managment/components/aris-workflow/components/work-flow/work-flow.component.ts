import { Guid } from 'src/app/shared/guid';
import * as Wft from '../../../../../core/models/workflow-item-type';

import { MessageService } from './../../../../../core/services/message.service';
import { isNullOrUndefined } from 'util';
import { Component, ElementRef, OnInit, AfterViewInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { WorkflowToolbarComponent } from '../workflow-toolbar/workflow-toolbar.component';
import { ProcessManagementService } from '../../../../services/process-management.service';
import { StencilService } from '../../../../services/rapid-services/stencil-service';
import { ToolbarService } from '../../../../services/rapid-services/toolbar-service';
import { InspectorService } from '../../../../services/rapid-services/inspector-service';
import { HaloService } from '../../../../services/rapid-services/halo-service';
import { KeyboardService } from '../../../../services/rapid-services/keyboard-service';
import { KitchenSinkService } from '../../../../services/rapid-services/kitchensink-service';
import { ArisNodeType } from 'src/app/shared/aris-node-type';
import { MessageType } from 'src/app/core/models/message.model';
import { MenuAction } from '../../../../models/menu-action';
import { AppArisTreViewComponent } from 'src/app/core/components/aris-tree-view/aris-tree-view.component';
import { RapidEventsService } from '../../../../services/rapid-events.service';
import { ArisWorkflowItem } from '../../../../models/aris-workflow-item';
import { WorkflowItemService } from '../../../../services/workflow-item.service';
import { ArisWorkflowItemCustomData } from '../../../../models/aris-workflow-item-custom-data.model';
import { ArisWorkflowType } from '../../../../../core/models/aris-workflow-item-type';
import { AutoUnsubscribe, takeWhileAlive } from 'take-while-alive';
import * as _ from 'lodash';
import { ArisTreeViewItem } from 'src/app/shared/aris-tree-view-item';
import { GenericWorkflowItem } from 'src/app/process-managment/models/generic-workflow-item';
import { GenericWorkflowType } from 'src/app/core/models/generic-workflow-item-type';
import { GenericWorkflowItemCustomData } from 'src/app/process-managment/models/generic-workflow-item-custom-data.model';
import { SessionService } from 'src/app/core/services/session.service';
import { ImportArisWfService } from 'src/app/process-managment/services/import-aris-workflow.service';
import { State, process } from '@progress/kendo-data-query';
import { SelectionEvent } from '@progress/kendo-angular-grid';
import {ProcessType} from '../../../../models/process-type';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-work-flow',
  templateUrl: './work-flow.component.html',
  styleUrls: ['./work-flow.component.css']
})
@AutoUnsubscribe()
export class WorkFlowComponent implements OnInit, AfterViewInit {

  private rappid: KitchenSinkService;
  public propertiesCollapsed = false;
  public loading = false;
  public selectedTreeViewItem;
  public selectedWorkflowItem: ArisWorkflowItem | GenericWorkflowItem;
  public toggleConnectorText = 'Hide';
  public connectorShow = false;
  public offset = { left: 0, top: 0 };
  public linkHolder;
  public _processDetail;
  public newVersion = true;
  public isIssueOpen = false;
  public isImportOpen = false;
  public file = null;
  public showLoadingMessage = false;
  public isSelectWfOpen = false;
  public generatedWorkflows = [];
  public gridData: any = [];
  public selectedWorkflows = [];
  public selectedDataSetsIds = [];
  public selectableSettings = {
    checkboxOnly: false,
    mode: 'single'
  };
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 5,
    filter: {
      logic: 'and',
      filters: []
    }
  };

  @Input()
  set processDetail(processDetail) {
    if (processDetail) {
      if (!this._processDetail) {
        this._processDetail = processDetail;
        this.initializeRapid(processDetail);
      }
      if (processDetail.processType === ProcessType.Aris) {
        if (this.permissionService.hasPermission('/api/Process/ArisWorkflow/GetArisWorkflows')) {
          this.processManagementService.getArisWorkflows(processDetail.id).subscribe((res: any) => {
            if (isNullOrUndefined(res)) {
              if (this.permissionService.hasPermission('/api/Process/ArisWorkflow/AddArisWorkflow')) {
                this.processManagementService
                .addArisWorkflow(processDetail.id, processDetail.title, processDetail.description)
                .subscribe((aris: any) => {
                  if (!isNullOrUndefined(aris)) {
                    let selectedTreeViewItem = {
                      title: processDetail.title,
                      description: processDetail.description,
                      id: aris.id,
                      processId: processDetail.id,
                      type: 0
                    };
                    this.selectedTreeViewItem = selectedTreeViewItem;
                    this.loadWorkflow(selectedTreeViewItem, processDetail.processType);
                  }
                });
              }
            } else {
              let selectedTreeViewItem = { title: res.title, description: res.description, id: res.id, processId: res.processId, type: 0 };
              this.selectedTreeViewItem = selectedTreeViewItem;
              this.loadWorkflow(selectedTreeViewItem, processDetail.processType);
            }
          });
        }
        
      } else {
        if (this.permissionService.hasPermission('/api/Process/GenericWorkflow/GetGenericWorkflows')) {
          this.processManagementService.getGenericWorkflows(processDetail.id).subscribe((res: any) => {
            if (isNullOrUndefined(res)) {
              if (this.permissionService.hasPermission('/api/Process/GenericWorkflow/AddGenericWorkflow')) {
                this.processManagementService
                .addGenericWorkflow(processDetail.id, processDetail.title, processDetail.description)
                .subscribe((generic: any) => {
                  if (!isNullOrUndefined(generic)) {
                    let selectedTreeViewItem = {
                      title: processDetail.title,
                      description: processDetail.description,
                      id: generic.id,
                      processId: processDetail.id,
                      type: 0
                    };
                    this.selectedTreeViewItem = selectedTreeViewItem;
                    this.loadWorkflow(selectedTreeViewItem, processDetail.processType);
                  }
                });
              }
            } else {
              let selectedTreeViewItem = { title: res.title, description: res.description, id: res.id, processId: res.processId, type: 0 };
              this.selectedTreeViewItem = selectedTreeViewItem;
              this.loadWorkflow(selectedTreeViewItem, processDetail.processType);
            }
          });
        }
      }
    }
  }

  @ViewChild(WorkflowToolbarComponent)
  private workflowToolbarComponent: WorkflowToolbarComponent;
  public type: ArisWorkflowType | GenericWorkflowType;
  public isArisWorkflow: Boolean = false;
  public message: string;
  public messageType: string;

  constructor(
    private element: ElementRef,
    private processManagementService: ProcessManagementService,
    private messageService: MessageService,
    private rapidEventsService: RapidEventsService,
    private workflowItemServie: WorkflowItemService,
    private sessionService: SessionService,
    private importArisService: ImportArisWfService,
    public permissionService: PermissionService
  ) {
    this.selectedWorkflowItem = { customData: {} } as ArisWorkflowItem | GenericWorkflowItem;
    rapidEventsService.itemSelected$.pipe(takeWhileAlive(this)).subscribe(result => {
      this.type = null;
      // this is a trick in order to re initialize child components, when ActionType doesn't change
      setTimeout(() => {
        this.updateWorkflowItemPropertiesPanel(result);
      }, 0);
    });

    this.workflowItemServie.worfklowItemSelectedModified$.pipe(takeWhileAlive(this)).subscribe(result => {
      this.updateWorkflowItem(result);
    });

    rapidEventsService.newElementAdded$.pipe(takeWhileAlive(this)).subscribe(result => {
      const currentCell = this.rappid.graph.getCell(result.cell.id);

      if (!isNullOrUndefined(currentCell)) {
        let graph = this.rappid.getGraphJson();
        graph.cells = graph.cells.filter(cell => cell.id !== currentCell.id);
        this.addToUndoStack(graph.cells);
      }

      if (result.cell.attributes.type === 'fsa.State') {
        if (!isNullOrUndefined(result.collection.models.find(x => x.attributes.type === 'fsa.State' && x.cid !== result.cell.cid))) {
          currentCell.attributes.attrs.text.text = 'Switch\nSystem';
          currentCell.attr('text/font-size', 10);
        } else {
          currentCell.attr('text/font-size', 14);
          currentCell.attributes.size.width = 50;
          currentCell.attributes.size.height = 50;
        }
      }
    });

    this.workflowItemServie.workflowVersionChanged$.pipe(takeWhileAlive(this)).subscribe((selectedVersion: any) => {
      if (this.isArisWorkflow) {
        if (!isNullOrUndefined(selectedVersion.arisWorkflowId) && !isNullOrUndefined(selectedVersion.version)) {
          this.processManagementService.getArisWorkflow(selectedVersion.arisWorkflowId).subscribe(res => {
            let jsonParsed = (res && res.designerJson && JSON.parse(res.designerJson)) || [];
            if (jsonParsed.cells && jsonParsed.cells.length === 0) {
              this.clearGraph();
              return;
            }
            this.rappid.loadGraphJson(jsonParsed);
          });
        }
      } else {
        if (!isNullOrUndefined(selectedVersion.genericWorkflowId) && !isNullOrUndefined(selectedVersion.version)) {
          this.processManagementService.getGenericWorkflow(selectedVersion.genericWorkflowId).subscribe((res: any) => {
            let jsonParsed = (res && res.designerJson && JSON.parse(res.designerJson)) || [];
            if (jsonParsed.cells && jsonParsed.cells.length === 0) {
              this.clearGraph();
              return;
            }
            this.rappid.loadGraphJson(jsonParsed);
          });
        }
      }
    });

    this.workflowItemServie.getGraphJson$.pipe(takeWhileAlive(this)).subscribe(neededBy => {
      const json = this.rappid.getGraphJson();
      const graphJson = JSON.stringify(json);
      this.workflowItemServie.sendGraphJson(graphJson, neededBy);
    });
  }
//
@Output() public importAris = new EventEmitter();
//
  private clearGraph() {
    this.rappid.loadGraphJson({ cells: [] });
  }

  private updateWorkflowItem(title: any) {
    let currentCell = this.rappid.graph.getCell(this.selectedWorkflowItem.id);

    if (title) {
      let elementTitle = title.length > 30 ? title.slice(0, 29) + '...' : title;
      currentCell.attr('label/text', elementTitle);
      currentCell.attr('root/dataTooltip', title);
      currentCell.resize(250, 40);
      this.selectedWorkflowItem.customData.Title = title;
    }
  }

  private getCustomData(workflowItemModel: any) {
    workflowItemModel.attributes.customData = Object.assign(
      this.isArisWorkflow ? new ArisWorkflowItemCustomData() : new GenericWorkflowItemCustomData(),
      workflowItemModel.attributes.customData
    );
    return workflowItemModel.attributes.customData;
  }

  private updateWorkflowItemPropertiesPanel(result: any) {
    if (isNullOrUndefined(result)) {
      this.type = null;
      return;
    }
    const id = result.id;
    const label = result.attributes.attrs.label || result.attributes.attrs.text;
    const displayText = label.text;
    const customData = this.getCustomData(result);
    let previousCustomData = null;
    this.selectedWorkflowItem = this.isArisWorkflow
      ? new ArisWorkflowItem(id, displayText, customData, previousCustomData)
      : new GenericWorkflowItem(id, displayText, customData, previousCustomData);
    this.propertiesCollapsed = false;
    this.type = this.selectedWorkflowItem.customData.Type;
    this.workflowItemServie.itemUpdated.next(this.selectedWorkflowItem);
  }

  private loadWorkflow(treeViewItem: any, processType) {
    if (processType === ProcessType.Aris) {
      this.loadArisWorkflow(treeViewItem);
    } else {
      this.loadGenericWorkflow(treeViewItem);
    }
  }

  public loadArisWorkflow(treeViewItem) {
    if (this.permissionService.hasPermission('/api/Process/ArisWorkflow/GetArisWorkflow')) {
      this.processManagementService.getArisWorkflow(treeViewItem.id).subscribe(async result => {
        let jsonParsed = (result && result.designerJson && JSON.parse(result.designerJson)) || [];
        if (result && !result.isparsed) {
          jsonParsed = this.getParsedGraph(jsonParsed.Items, jsonParsed.ItemLinks);
        }
        if (jsonParsed.length === 0 || (jsonParsed.cells && jsonParsed.cells.length === 0)) {
          this.clearGraph();
        } else {
          if (result && !result.isparsed && this.permissionService.hasPermission('/api/Process/ArisWorkflow/SaveArisWorkflow')) {
            await this.processManagementService.saveArisWorkflow(this.selectedTreeViewItem.id, JSON.stringify(jsonParsed)).toPromise();
          }
          this.rappid.loadGraphJson(jsonParsed);
        }
      });
    }
    this.rappid.undoStack = [];
    this.rappid.redoStack = [];
  }

  public loadGenericWorkflow(treeViewItem) {
    if (this.permissionService.hasPermission('/api/Process/GenericWorkflow/GetGenericWorkflow')) {
      this.processManagementService.getGenericWorkflow(treeViewItem.id).subscribe(async (result: any) => {
        let jsonParsed = (result && result.designerJson && JSON.parse(result.designerJson)) || [];
        if (jsonParsed.length === 0 || (jsonParsed.cells && jsonParsed.cells.length === 0)) {
          this.clearGraph();
        } else {
          this.rappid.loadGraphJson(jsonParsed);
        }
      });
    }
    
    this.rappid.undoStack = [];
    this.rappid.redoStack = [];
  }

  getParsedGraph(items, links) {
    let rapidItems = items.map(item => {
      return this.rappid.createRapidElement({ ...item, PreviousId: item.Id });
    });

    let rapidLinks = links.map(link => {
      let rapidLink = this.rappid.createLink();
      let sourceElement = rapidItems.find(item => item.attributes.customData.PreviousId === link.FromId);
      let targetElement = rapidItems.find(item => item.attributes.customData.PreviousId === link.ToId);

      if (sourceElement) {
        rapidLink.source(sourceElement);
      }

      if (targetElement) {
        rapidLink.target(targetElement);
      }

      return rapidLink;
    });

    return { cells: rapidItems.concat(rapidLinks) };
  }

  // position of popUP
  private addTextConnectorsPosition(cellView) {
    this.linkHolder = cellView;
    const link = document.querySelector(`[model-id="${cellView.model.id}"]`);
    this.offset.top = link.getBoundingClientRect().top + link.getBoundingClientRect().height / 2;
    this.offset.left = link.getBoundingClientRect().left + link.getBoundingClientRect().width / 2;
    this.onToggle();
  }

  ngOnInit() {}

  ngAfterViewInit(): void {
    if (this._processDetail) {
      this.initializeRapid(this._processDetail);
    }
  }

  initializeRapid(processDetail) {
    this.rappid = new KitchenSinkService(
      this.element.nativeElement as any,
      new StencilService(processDetail && processDetail.processType === ProcessType.Aris),
      new ToolbarService(),
      new InspectorService(),
      new HaloService(),
      new KeyboardService(),
      this.rapidEventsService
    );
    this.rappid.startRappid();
    this.isArisWorkflow = processDetail && processDetail.processType === ProcessType.Aris;
    const that = this;

    this.rappid.paper.on('link:pointerdblclick', function(cellView) {
      that.connectorShow = false;
      const conditionItem = that.rappid.graph.getCell(cellView.model.attributes.source.id);
      if (conditionItem.attributes.type === 'standard.Polygon') {
        that.addTextConnectorsPosition(cellView);
      }
    });

    this.rappid.graph.on('remove', (cell, collection, opt) => {
      if (opt.removeLinks) {
        if (cell.lastLink) {
          let removedLinks = this.rappid.removedLinks.concat(cell);
          this.addToUndoStack(this.rappid.getGraphJson().cells.concat(removedLinks));
          this.rappid.removedLinks = [];
        } else {
          this.rappid.removedLinks.push(cell);
        }
      } else if (opt.selection) {
        if (this.rappid.selectedItems.length > 0) {
          let selectedItems = this.rappid.selectedItems.map(el => el.attributes);
          this.rappid.selectedItems = [];
          let graph = this.rappid.getGraphJson();
          this.addToUndoStack(graph.cells.concat(cell));
          let sourceElements = graph.cells.filter(
            el => el.source && selectedItems.find(cell => el.source.id === cell.id) && !selectedItems.find(cell => el.id === cell.id)
          );
          let targetElements = graph.cells.filter(
            el => el.target && selectedItems.find(cell => el.target.id === cell.id) && !selectedItems.find(cell => cell.id === el.id)
          );
          let elementsToBeRemoved = [...sourceElements, ...targetElements, ...selectedItems];
          graph.cells = graph.cells.filter(cell => !elementsToBeRemoved.find(el => el.id === cell.id));
          this.rappid.loadGraphJson(graph);
        }
      } else if (!cell.isLink()) {
        let graph = this.rappid.getGraphJson();
        this.addToUndoStack(graph.cells.concat(cell));
        let sourceElements = graph.cells.filter(el => el.source && el.source.id === cell.attributes.id);
        let targetElements = graph.cells.filter(el => el.target && el.target.id === cell.attributes.id);
        graph.cells = graph.cells.filter(
          cell => !targetElements.find(el => el.id === cell.id) && !sourceElements.find(el => el.id === cell.id)
        );
        this.rappid.loadGraphJson(graph);
        this.updateWorkflowItemPropertiesPanel(null);
      } else {
        let graph = this.rappid.getGraphJson();
        graph.cells = graph.cells.concat(cell);
        this.addToUndoStack(graph.cells);
      }
    });
  }

  addToUndoStack(cells) {
    this.rappid.undoStack.push({
      graph: { cells: cells }
    });
  }

  saveClicked() {
    this.saveWorkflow();
  }

  saveWorkflow() {
    const graphJson = JSON.stringify(this.rappid.getGraphJson());
    if (this._processDetail.processType === ProcessType.Aris) {
      this.saveArisWorkflow(graphJson);
    } else {
      this.saveGenericWorkflow(graphJson);
    }
  }

  saveArisWorkflow(graphJson) {
    this.processManagementService.saveArisWorkflow(this.selectedTreeViewItem.id, graphJson).subscribe(res => {
      if (res) {
        this.messageService.sendMessage({
          type: MessageType.Success,
          text: `Requirement ${this.selectedTreeViewItem.title} is saved!`
        });
      }
    });
  }

  saveGenericWorkflow(graphJson) {
    this.processManagementService.saveGenericWorkflow(this.selectedTreeViewItem.id, graphJson).subscribe(res => {
      if (res) {
        this.messageService.sendMessage({ text: `Requirement ${this.selectedTreeViewItem.title} is saved!`, type: MessageType.Success });
      }
    });
  }

  onToggle(state?): void {
    this.rappid.addTextToConnector(this.linkHolder, state);
    this.connectorShow = !this.connectorShow;
    this.toggleConnectorText = this.connectorShow ? 'Hidе' : 'Show';
  }

  issueClicked() {
    this.rappid.paper.toPNG((dataURL: string) => {
      if (this.isArisWorkflow) {
        this.processManagementService.issueArisWorkflow(this.selectedTreeViewItem.id, dataURL, this.newVersion).subscribe(res => {
          if (res) {
            this.workflowItemServie.workflowSelectionChanged.next(this.selectedTreeViewItem.id);
            this.messageService.sendMessage({ type: MessageType.Info, text: `Workflow ${this.selectedTreeViewItem.title} issued !` });
          }
          this.closeIssue();
        });
      } else {
        this.processManagementService.issueGenericWorkflow(this.selectedTreeViewItem.id, dataURL, this.newVersion).subscribe((res: any) => {
          if (res) {
            this.workflowItemServie.workflowSelectionChanged.next(this.selectedTreeViewItem.id);
            this.messageService.sendMessage({ type: MessageType.Info, text: `Workflow ${this.selectedTreeViewItem.title} issued !` });
          }
          this.closeIssue();
        });
      }
    });
  }

  newClicked() {
    this.newVersion = true;
  }

  updatedClicked() {
    this.newVersion = false;
  }

  closeIssue() {
    this.newVersion = true;
    this.isIssueOpen = false;
  }

  fileChanged(event) {
    let file = event.target.files[0];
    if (file) {
      this.file = file;
    }
  }
openImport(){
  this.isImportOpen = true;
}
  closeImport() {
    this.isImportOpen = false;
    this.file = null;
  }

  import() {
    if (!this.file) {
      this.messageService.sendMessage({ text: 'Please select a file!', type: MessageType.Warning });
      return;
    }
    let formData = new FormData();
    formData.append('file', this.file);
    if (!this.showLoadingMessage) {
      this.showLoadingMessage = true;
      this.importArisService.importAris(formData, this._processDetail.id).subscribe(res => {
        this.showLoadingMessage = false;

        if (res) {
          this.gridData = process(res, this.gridState);
          this.generatedWorkflows = res;
          if (res.length > 0) {
            this.isSelectWfOpen = true;
          }
        }
        this.closeImport();
      });
    }
  }

  closeSelectWf() {
    this.isSelectWfOpen = false;
    this.generatedWorkflows = [];
    this.selectedWorkflows = [];
    this.gridData = [];
  }

  onStateChange(state: State) {
    this.gridState = state;
    this.gridData = process(this.generatedWorkflows, state);
  }

  selectionChanged($event: SelectionEvent) {
    let selectedWorkflows = [];
    if ($event.selectedRows.length > 0) {
      selectedWorkflows = $event.selectedRows.map(row => row.dataItem);
    } else if ($event.deselectedRows.length > 0) {
      selectedWorkflows = this.selectedWorkflows.filter(workflow => !$event.deselectedRows.find(row => row.dataItem.id === workflow.id));
    }
    this.selectedDataSetsIds = selectedWorkflows.map(row => row.id);
    this.selectedWorkflows = selectedWorkflows;
  }

  createWorkflow() {
    let workflow = this.selectedWorkflows[0];
    let jsonParsed = (workflow && workflow.desingerJson && JSON.parse(workflow.desingerJson)) || [];
    jsonParsed = this.getParsedGraph(jsonParsed.Items, jsonParsed.ItemLinks);
    this.processManagementService.saveArisWorkflow(this.selectedTreeViewItem.id, JSON.stringify(jsonParsed)).subscribe(res => {
      if (res) {
        this.closeSelectWf();
        this.rappid.loadGraphJson(jsonParsed);
        this.messageService.sendMessage({ text: 'Workflow was imported successfully!', type: MessageType.Success });
      } else {
        this.closeSelectWf();
      }
    });
  }

  disableCreateBtn() {
    return !this.selectedWorkflows || this.selectedWorkflows.length !== 1;
  }

  generateAris() {
    let subProjectId = this.sessionService.getSubProject().id;
    let catalog = this.sessionService.getDefaultSettings().catalog;
    let system = this.sessionService.getDefaultSettings().system;
    if (!system || !system.id) {
      this.messageService.sendMessage({ text: 'There is no default system selected!', type: MessageType.Warning });
    } else if (!catalog || !catalog.id) {
      this.messageService.sendMessage({ text: 'There is no default catalog selected!', type: MessageType.Warning });
    } else {
      this.importArisService.exportAris(subProjectId, system.id, catalog.id, this.selectedTreeViewItem.id).subscribe(res => {
        if (res) {
          this.messageService.sendMessage({ text: 'Workflow was exported successfully!', type: MessageType.Success });
        }
      });
    }
  }

  hasSavePermission() {
    return (
      this._processDetail &&
      (this._processDetail.processType === ProcessType.Aris && this.permissionService.hasPermission('/api/Process/ArisWorkflow/SaveArisWorkflow')) ||
      (this._processDetail.processType === ProcessType.Generic && this.permissionService.hasPermission('/api/Process/GenericWorkflow/SaveGenericWorkflow'))
    );
  }

  hasIssuePermission() {
    return (
      this._processDetail &&
      ((this._processDetail.processType === ProcessType.Aris && this.permissionService.hasPermission('/api/Process/ArisWorkflow/IssueArisWorkflow')) ||
      (this._processDetail.processType === ProcessType.Generic && this.permissionService.hasPermission('/api/Process/GenericWorkflow/IssueGenericWorkflow')))
    );
  }

  hasImportPermission() {
    return (
      this._processDetail &&
      this._processDetail.processType === ProcessType.Aris &&
      this.permissionService.hasPermission('/api/Process/ArisWorkflow/ImportArisWorkflow2ArisDesigner') &&
      this.permissionService.hasPermission('/api/Process/ArisWorkflow/SaveArisWorkflow')
    )
  }

  showActions() {
    return (
      this.hasSavePermission() ||
      this.hasIssuePermission() ||
      this.hasImportPermission()
    );
  }

  issueworkflow() {
    this.isIssueOpen = true;
  }

  hasVersionPermission() {
    return(
      this._processDetail &&
      (
        this._processDetail.processType === ProcessType.Aris && 
        this.permissionService.hasPermission('/api/Process/ArisWorkflow/GetArisWorkflowVersions') && 
        this.permissionService.hasPermission('/api/Process/ArisWorkflow/RollbackArisWorkflow')
      ) ||
      (
        this._processDetail.processType === ProcessType.Generic &&
        this.permissionService.hasPermission('/api/Process/GenericWorkflow/GetGenericWorkflowVersions') &&
        this.permissionService.hasPermission('/api/Process/GenericWorkflow/RollbackGenericWorkflow')
      )
    );
  }
}
