import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MessageService } from 'src/app/core/services/message.service';
import { MessageType } from 'src/app/core/models/message.model';
import { SessionService } from 'src/app/core/services/session.service';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { Guid } from 'src/app/shared/guid';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-select-hierarchy',
  templateUrl: 'select-hierarchy.component.html',
  styleUrls: ['select-hierarchy.component.css']
})
export class SelectHierarchyComponent implements OnInit {
  public hierarchy = { title: '' } as any;
  public hierarchies = [];
  public _processDetail;
  @Input() openMoveProcessWindow;
  @Input()
  set processDetail(processDetail) {
    if (processDetail) {
      this.processManagementService.getHierarchy(processDetail.hierarchyId).subscribe(hierarchy => {
        if (hierarchy.parentId === Guid.empty) {
          this.getHierarchies({ id: Guid.empty, hierarchyTypeId: Guid.empty }, hierarchy);
        } else {
          this.processManagementService.getHierarchy(hierarchy.parentId).subscribe(parentHierarchy => {
            if (parentHierarchy) {
              this.getHierarchies(parentHierarchy, hierarchy);
            }
          });
        }
      });
      this._processDetail = processDetail;
    }
  }
  @Output() public closeMoveProcessWindow = new EventEmitter<any>();
  @Output() public processHierarchyUpdated = new EventEmitter<any>();

  constructor(
    private processManagementService: ProcessManagementService,
    private messageService: MessageService,
    private session: SessionService
  ) {}

  ngOnInit() {}

  close() {
    this.closeMoveProcessWindow.emit();
  }

  disableSubmitBtn() {
    return !this.hierarchy;
  }

  submitClicked() {
    if (!isNullOrUndefined(this.hierarchy)) {
      let subProjectId = this.session.getSubProject().id;
      this.processManagementService
        .updateProcess(
          this._processDetail.id,
          this._processDetail.title,
          this._processDetail.description,
          this._processDetail.expectedNumberOfTestCases,
          this.hierarchy.id,
          subProjectId,
          this._processDetail.createdId,
          this._processDetail.ownerId,
          this._processDetail.processType,
          this._processDetail.priority,
          this._processDetail.requirementType,
          this._processDetail.typicalId
        )
        .subscribe(res => {
          if (res) {
            this.messageService.sendMessage({ text: 'Requirement was moved successfully!', type: MessageType.Success });
            this.processHierarchyUpdated.emit({ ...this._processDetail, hierarchyId: this.hierarchy.id });
          }
          this.close();
        });
    }
  }

  async getHierarchies(parentHierarchy, processHierarchy) {
    let subProjectId = this.session.getSubProject().id;
    parentHierarchy.hierarchyTypeId =
      isNullOrUndefined(parentHierarchy.hierarchyTypeId) || parentHierarchy.hierarchyTypeId === ''
        ? Guid.empty
        : parentHierarchy.hierarchyTypeId;

    this.processManagementService
      .getHierarchiesByParent(subProjectId, parentHierarchy.hierarchyTypeId, parentHierarchy.id)
      .subscribe(hierarchies => {
        if (hierarchies) {
          this.hierarchies = [...hierarchies];
          this.hierarchy = processHierarchy;
        }
      });
  }
}
