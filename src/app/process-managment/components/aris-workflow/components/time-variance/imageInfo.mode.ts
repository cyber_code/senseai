export interface ImageInfo {
  fileExtension: string;
  base64EncodedContent: string;
}
