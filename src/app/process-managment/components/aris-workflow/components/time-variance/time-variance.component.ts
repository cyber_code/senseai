
import {Component, OnInit, ChangeDetectionStrategy, AfterViewInit, Input, Output, EventEmitter} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {WorkflowVersion} from 'src/app/design/models/workflow-version.model';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import {ProcessType} from '../../../../models/process-type';

@Component({
  selector: 'app-time-variance',
  templateUrl: './time-variance.component.html',
  styleUrls: ['./time-variance.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimeVarianceComponent implements OnInit, AfterViewInit {
  @Output() public closeTimeVariance = new EventEmitter<any>();
  @Input() workflow;
  @Input() processDetail;
  public dateTimeRange: Date[];
  public workflowVersions: WorkflowVersion[] = [];

  constructor(
    private domSanitizer: DomSanitizer,
    private processManagementsService: ProcessManagementService
  ) {
  }

  ngOnInit() {}

  ngAfterViewInit(): void {}

  goBack() {
    this.closeTimeVariance.emit();
  }

  openZoom(event: any) {
    event.preventDefault();
  }

  pickerClosed() {
    if (this.dateTimeRange && this.dateTimeRange.length > 0) {
      const fromDate = this.dateTimeRange[0].toJSON();
      const toDate = this.dateTimeRange[1] && this.dateTimeRange[1].toJSON();
      if (this.processDetail.processType === ProcessType.Aris) {
        this.processManagementsService.getArisWorkflowVersionsByDate(this.workflow.id, fromDate, toDate).subscribe((result: any) => {
          if (result) {
            result.map(x => {
              x.workflowImage = this.domSanitizer.bypassSecurityTrustUrl(x.workflowImage) as string;
              return x;
            });
            
          }
          this.workflowVersions = result;
        });
      } else {
        this.processManagementsService.getGenericWorkflowVersionsByDate(this.workflow.id, fromDate, toDate).subscribe((result: any) => {
          if (result) {
            result.map(x => {
              x.workflowImage = this.domSanitizer.bypassSecurityTrustUrl(x.workflowImage) as string;
              return x;
            });
          }
          this.workflowVersions = result;
        });
      }
      
    }
  }
}
