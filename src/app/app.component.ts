import { SubProject } from "src/app/core/models/sub-project.model";
import { DesignService } from "./design/services/design.service";
import {
  DefaultSettings,
  Settings
} from "./core/models/default-settings.model";
import { SessionService } from "./core/services/session.service";
import { MessageService } from "./core/services/message.service";
import { ModelChangesService } from "./core/components/model-changes/model-changes.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { ConfigurationService } from "./core/services/configuration.service";
import { AuthenticationService } from "./core/services/authentication.service";
import { ProjectSelectionService } from "./core/components/project-selection/project-selection.service";
import { NotificationService } from "@progress/kendo-angular-notification";
import { zip } from "rxjs";
import { MessageType, RpaMessage } from "./core/models/message.model";
import { Guid } from "./shared/guid";
import { Router } from "@angular/router";
import { AdministrationService } from "./core/services/administration.service";
import { Catalog } from "./core/models/catalog.model";
import { System } from "./core/models/system.model";
import { AutoUnsubscribe, takeWhileAlive } from "take-while-alive";
import { globalCacheBusterNotifier } from "ngx-cacheable";
import { Location } from "@angular/common";
import { environment } from "../environments/environment";
import { VersionService } from "./core/services/version.service";
import { Project } from "./core/models/project.model";
import { PermissionService } from "./core/services/permission.service";

@Component({
  templateUrl: "./app.component.html",
  selector: "app-root",
  styleUrls: ["./app.component.css"]
})
@AutoUnsubscribe()
export class AppComponent implements OnInit {
  public title = "Validata Sense.ai";
  public showNavbar: Boolean = false;
  public numberOfTypicalChanges = 0;
  public uiVersion: string = "0.0.0.0";
  public apiVersion: string = "0.0.0.0";
  public mlVersion: string = "0.0.0.0";
  public defaultSettingsOpened = false;
  public userProfileOpened = false;
  public changePasswordOpened = false;
  public changeUsernameOpened = false;
  public changeEmailOpened = false;

  constructor(
    public authenticationService: AuthenticationService,
    private projectSelectionService: ProjectSelectionService,
    private notificationService: NotificationService,
    private messageService: MessageService,
    private sessionService: SessionService,
    private administrationService: AdministrationService,
    private designService: DesignService,
    private modelChangesService: ModelChangesService,
    private versionService: VersionService,
    private router: Router,
    public location: Location,
    public permissionService: PermissionService
  ) {
    // closure
    // subscribe for project selection changes
    this.projectSelectionService.subProjectSelected$
      .pipe(takeWhileAlive(this))
      .subscribe(res => {
        if (res.hasValue) {
          const project = this.sessionService.getProject();
          this.checkDefaultSettings(project, res.selectedSubProject);
        }
        // clear cache
        globalCacheBusterNotifier.next();
      });
    // subscribe for messages sent by different components
    this.messageService
      .getMessage()
      .pipe(takeWhileAlive(this))
      .subscribe(message => {
        switch (message.type) {
          case MessageType.Default:
            this.showDefault(message.text);
            break;
          case MessageType.Success:
            this.showSuccess(message.text);
            break;
          case MessageType.Info:
            this.showInfo(message.text);
            break;
          case MessageType.Warning:
            this.showWarning(message.text);
            break;
          case MessageType.Error:
            this.showError(message);
            break;
        }
      });
    // subscribe for authentication notification
    this.authenticationService.authenticationChallange$.pipe(takeWhileAlive(this)).subscribe(x => {
      this.showNavbar = x;
      if (x) {
        this.modelChangesService.GetNumberOfTypicalChanges().subscribe(number => {
          this.numberOfTypicalChanges = number;
        });
        
      }
    });

    this.modelChangesService.changesApproved.subscribe(res => {
      if (res) {
        this.modelChangesService
          .GetNumberOfTypicalChanges()
          .subscribe(number => {
            this.numberOfTypicalChanges = number;
          });
      }
    });

    this.uiVersion = environment.version;
    this.versionService.getApiVersion().subscribe((res: any) => {
      this.apiVersion = res.version;
    });
    this.versionService.getMLVersion().subscribe((res: any) => {
      console.log(res)
      this.mlVersion = res.version;
    });
  }

  private checkDefaultSettings(project: Project, subProject: SubProject) {
    const catalogs$ = this.designService.getTenantCatalogs(
      project.id,
      subProject.id
    );
    const systems$ = this.designService.getSystems();
    const settings$ = this.administrationService.getSettings(
      subProject.projectId,
      subProject.id
    );
    zip(catalogs$, systems$, settings$).subscribe(result => {
      const catalogs = result[0] || [];
      const systems = result[1] || [];
      const settings = result[2] || new Settings();
      if (
        !settings ||
        Guid.isEmpty(settings.catalogId) ||
        Guid.isEmpty(settings.systemId)
      ) {
        this.sessionService.setDefaultSettings(
          new DefaultSettings(new Catalog(), new System())
        );
        this.defaultSettingsOpened = true;
      } else {
        const catalog =
          catalogs.find(c => c.id === settings.catalogId) || new Catalog();
        const system =
          systems.find(c => c.id === settings.systemId) || new System();
        const defaultSettings = new DefaultSettings(catalog, system);
        this.sessionService.setDefaultSettings(defaultSettings);
      }
    });
  }

  ngOnInit() {
    this.projectSelectionService.load();
  }

  public showDefault(message: string): void {
    this.notificationService.show({
      content: message,
      hideAfter: 600,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 400 },
      type: { style: "none", icon: false }
    });
  }

  public showSuccess(message: string): void {
    this.notificationService.show({
      content: message,
      hideAfter: 1000,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 600 },
      type: { style: "success", icon: true }
    });
  }

  public showWarning(message: string): void {
    this.notificationService.show({
      content: message,
      hideAfter: 2000,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 600 },
      type: { style: "warning", icon: true }
    });
  }

  public showInfo(message: string): void {
    this.notificationService.show({
      content: message,
      hideAfter: 2000,
      position: { horizontal: "center", vertical: "top" },
      animation: { type: "fade", duration: 600 },
      type: { style: "info", icon: true }
    });
  }

  public showError(message: RpaMessage): void {
    if (message.appendTo) {
      this.notificationService.show({
        content: message.text,
        hideAfter: 2000,
        position: { horizontal: "center", vertical: "top" },
        animation: { type: "fade", duration: 600 },
        type: { style: "error", icon: true },
        appendTo: message.appendTo
      });
    } else {
      this.notificationService.show({
        content: message.text,
        hideAfter: 2000,
        position: { horizontal: "center", vertical: "top" },
        animation: { type: "fade", duration: 600 },
        type: { style: "error", icon: true }
      });
    }
  }
  ngOnDestroy(): void {
    this.classHide();
    this.idHide();
    if (
      document.getElementById("botDiv").children[0].parentNode["className"] ===
      "chat-container is-visible"
    ) {
      this.clickBot();
    }
  }
  clickBot() {
    const element: HTMLElement = document.getElementsByClassName(
      "btn-chat btn-sonar"
    )[0] as HTMLElement;
    element.click();
  }

  classShow() {
    const botDiv = document.getElementById("botChat");
    if (botDiv) {
      botDiv.style.opacity = "1";
    }
  }

  idShow() {
    const chatBtn = document.getElementsByClassName("btn-chat btn-sonar");
    if (chatBtn && chatBtn.length) {
      for (let i = 0; i < chatBtn.length; i++) {
        chatBtn[i]["style"].opacity = "1";
      }
    }
  }

  classHide() {
    const botDiv = document.getElementById("botChat");
    if (botDiv) {
      botDiv.style.opacity = "0";
    }
  }

  idHide() {
    const chatBtn = document.getElementsByClassName("btn-chat btn-sonar");
    if (chatBtn && chatBtn.length) {
      for (let i = 0; i < chatBtn.length; i++) {
        chatBtn[i]["style"].opacity = "0";
      }
    }
  }

  closePopups() {
    this.defaultSettingsOpened = false;
    this.userProfileOpened = false;
    this.changePasswordOpened = false;
    this.changeUsernameOpened = false;
    this.changeEmailOpened = false;
  }

  closeDefaultSettings() {
    this.defaultSettingsOpened = false;
  }

  openDefaultSettings() {
    this.defaultSettingsOpened = true;
    this.userProfileOpened = false;
    this.changePasswordOpened = false;
    this.changeUsernameOpened = false;
    this.changeEmailOpened = false;
  }

  closeUserProfile() {
    this.userProfileOpened = false;
  }

  closeChangePassword() {
    this.changePasswordOpened = false;
  }

  closeChangeUsername() {
    this.changeUsernameOpened = false;
  }

  openUserProfile() {
    this.userProfileOpened = true;
    this.defaultSettingsOpened = false;
    this.changePasswordOpened = false;
    this.changeEmailOpened = false;
    this.changeUsernameOpened = false;
  }

  openChangePassword() {
    this.userProfileOpened = false;
    this.defaultSettingsOpened = false;
    this.changePasswordOpened = true;
    this.changeEmailOpened = false;
    this.changeUsernameOpened = false;
  }

  openChangeUsername() {
    this.userProfileOpened = false;
    this.defaultSettingsOpened = false;
    this.changePasswordOpened = false;
    this.changeUsernameOpened = true;
    this.changeEmailOpened = false;
  }

  openChangeEmail() {
    this.userProfileOpened = false;
    this.defaultSettingsOpened = false;
    this.changePasswordOpened = false;
    this.changeUsernameOpened = false;
    this.changeEmailOpened = true;
  }

  closeChangeEmail() {
    this.changeEmailOpened = false;
  }
}
