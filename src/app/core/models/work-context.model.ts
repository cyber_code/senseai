import { DefaultSettings } from './default-settings.model';
import { User } from './user.model';
import { Project } from './project.model';
import { SubProject } from './sub-project.model';

export class WorkContext {
  user: User;
  identityUrl: string;
  project: Project;
  subProject: SubProject;
  defaultSettings: DefaultSettings;
  staticHashId: string;
}
