export class User {
  id: string;
  username: string;
  name: string;
  lastName: string;
  email: string;
  role: string;
  tenantId: string;
  token?: string;
  permissions?:any;
  userIdFromAdminPanel: number;
  licenseId?: string;
  licenses?: any;
}
