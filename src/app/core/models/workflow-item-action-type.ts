export enum WorkflowItemActionType {
  Menu,
  See,
  Delete,
  Authorise,
  Reverse,
  SingOff,
  Tab,
  Input,
  EnquiryAction,
  EnquiryResult,
  Verify,
  Validate
}

// ACTION : Navigation
// Parameters: Tab-Home/customer/details
//
