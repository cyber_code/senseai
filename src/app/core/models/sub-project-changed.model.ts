import { Project } from './project.model';
import { SubProject } from './sub-project.model';
import { isNullOrUndefined } from 'util';

export class SubProjectChanged {
  selectedProject: Project;
  selectedSubProject: SubProject;
  hasValue: Boolean;
  constructor(project: Project, subProject: SubProject) {
    this.selectedProject = (!isNullOrUndefined(project) && project.id !== '' && project) || null;
    this.selectedSubProject = (!isNullOrUndefined(subProject) && subProject.id !== '' && subProject) || null;
    this.hasValue = this.selectedProject != null && this.selectedSubProject != null;
  }
}
