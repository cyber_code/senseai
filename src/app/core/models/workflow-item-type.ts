export enum WorkflowItemType {
  Start,
  Action,
  Condition, 
  Linked,
  Manual
}
