export enum Licenses {
    Requirements_intelligence = 1,
    Sprint_planning_and_management = 2,
    Requirements_and_hierarchies = 3,
    Requirements_Insights = 4,
    Documentation_and_Video_generation = 5,
    Prebuild_processed = 6,
    Flow_intelligence = 7,
    Content_License_Copy_right = 8,
    Computer_Assisted_Computer_generated = 9,
    Recording = 10,
    Test_data_intelligence = 11,
    AI_recommended_Data = 12,
    Synthetic_data = 13,
    Test_case_intelligence = 14,
    Test_case_export_to_ATS_for_execution = 15,
    Test_case_export_in_other_format = 16,
    Model_association_exposure = 17,
    Defect_intelligence = 18,
    Defect_Import = 19,
    Defect_Recommendations = 20,
    Defect_Mining_graph = 21,
    Reports_and_dashboards = 22,
    Test_analysis = 23,
    Requirement_Prioritization = 24,
    Requirements_traceability = 25,
    Test_coverage_module = 26,
    Workflow_coverage = 27,
    Test_coverage = 28,
    Defect = 29,
    Defect_Insights = 30,
    Defect_Replay = 31,
    Project_and_Risk = 32,
    Risk_coverage = 33,
    Risk_optimize = 34,
    Release_summary = 35,
    Integrations = 36,
    Jira = 37,
    Slack = 38,
    Data_extraction_tool = 39,
    Planning = 40,
    QA_plan = 41,
    Dev_Plan = 42,
    Live_Impact_Analysis = 43
}