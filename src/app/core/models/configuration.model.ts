export class Configuration {
  identityUrl: string;
  apiUrl: string;
  reportsUrl: string;
  useStaticHashId: boolean;
  hashId: string;
  projectId: string;
  subProjectId: string;
  negativeTrend: number;
  positiveTrend: number;
}
