export  class RpaMessage {
  text: string;
  type: MessageType;
  timeout?: Number;
  appendTo?: any;
}

export enum MessageType {
  Info,
  Success,
  Warning,
  Error,
  Default
}

