import { Suggestion } from './suggestion.model';

export class ListSuggestions {
  error: {
    exceptionObject?: {
      Message? :string
    }
  };
  hasResult: boolean;
  result: ListSuggestionsItem;
  successful: boolean
}

export class ListSuggestionsItem {
  preItems: Suggestion[];
  postItems: Suggestion[];
}