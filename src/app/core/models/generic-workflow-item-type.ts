export enum GenericWorkflowType {
  Start,
  Activity,
  Conditional
}