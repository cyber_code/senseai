import { WorkflowItemCustomData } from 'src/app/design/models/workflow-item-custom-data.model';

export class Suggestion {
  workflowItem: WorkflowItemCustomData;
  worklfowItemLink: WorkflowItemLink;
}
export class WorkflowItemLink {
  sourceId: string;
  targetId: string;
  type: number;
}
export class WorkflowItem {
  id: string;
  title: string;
  description: string;
  catalogId: string;
  catalogName: string;
  typicalId: string;
  systemId: string;
  typicalName: string;
  actionType: string;
  parameters: string;
  dynamicData: any;
}
