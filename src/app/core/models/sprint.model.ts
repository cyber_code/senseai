export class Sprint {
  id: string;
  title: string;
  description: string;
  start: Date;
  end: Date;
  subProjectId: string;
}
