import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { User } from '../models/user.model';
import { Guid } from 'src/app/shared/guid';

@Injectable()
export class ConfigurationInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const users: User[] = [
      {
        id: '1',
        username: 'admin',
        name: 'Validata',
        role: 'Admin',
        lastName: 'Group',
        email: 'info@validata-software.com',
        tenantId: Guid.newGuid(),
        userIdFromAdminPanel: 0
      }
    ];

    const authHeader = request.headers.get('Authorization');
    const isLoggedIn =
      authHeader && authHeader.startsWith('Bearer fake-jwt-token');

    return of(null)
      .pipe(
        mergeMap(() => {
          if (request.url.includes('/Configuration')) {
            return ok({
              identityUrl: 'http://localhost:5001/api/identity/signin'
            });
          }
          return next.handle(request);
        })
      )
      .pipe(materialize())
      .pipe(dematerialize());

    function ok(body) {
      return of(new HttpResponse({ status: 200, body }));
    }

    function unauthorised() {
      return throwError({ status: 401, error: { message: 'Unauthorised' } });
    }

    function error(message) {
      return throwError({ status: 400, error: { message } });
    }
  }
}

export let ConfigurationMockProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: ConfigurationInterceptor,
  multi: true
};
