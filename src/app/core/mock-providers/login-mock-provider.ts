import { Guid } from './../../shared/guid';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { User } from '../models/user.model';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const users: User[] = [
      {
        id: '1',
        username: 'admin',
        name: 'Validata',
        lastName: 'Group',
        email: 'info@validata-software.com',
        role: 'Admin',
        tenantId: Guid.newGuid(),
        userIdFromAdminPanel: 0
      }
    ];

    const authHeader = request.headers.get('Authorization');
    const isLoggedIn =
      authHeader && authHeader.startsWith('Bearer fake-jwt-token');

    return of(null)
      .pipe(
        mergeMap(() => {
          if (
            request.url.endsWith('/identity/signin') &&
            request.method === 'POST'
          ) {
            const user = users.find(x => x.username === request.body.username);
            if (!user) {
              return error('Username or password is incorrect');
            }
            return ok({
              id: user.id,
              username: user.username,
              name: user.name,
              lastName: user.lastName,
              // tslint:disable-next-line:max-line-length
              tenantId: Guid.newGuid(),
              // tslint:disable-next-line: max-line-length
              access_token: `eyJhbGciOiJSUzI1NiIsImtpZCI6ImJhZWUwZDNkNmU2OTdhYmVhMzhmYjIwNGE5OGQ1MzI4IiwidHlwIjoiSldUIn0.eyJuYmYiOjE1NTAwMDA5NjAsImV4cCI6MTU1MDAwNDU2MCwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo1MDAwIiwiYXVkIjpbImh0dHA6Ly9sb2NhbGhvc3Q6NTAwMC9yZXNvdXJjZXMiLCJSUEFXZWJBcGkiLCJVc2VyUHJvZmlsZSJdLCJjbGllbnRfaWQiOiJSUEEuV2ViQXBpIiwic3ViIjoiMSIsImF1dGhfdGltZSI6MTU1MDAwMDk2MCwiaWRwIjoibG9jYWwiLCJ1c2VyX2lkIjoiMSIsIm5hbWUiOiJhZG1pbiIsInByZWZlcnJlZF91c2VybmFtZSI6ImFkbWluIiwiZW1haWwiOiJhZG1pbkB2YWxpZGF0YS1zb2Z0d2FyZS5jb20iLCJyb2xlIjoiQWRtaW5pc3RyYXRvciIsInNjb3BlIjpbIlJQQVdlYkFwaSIsIlVzZXJQcm9maWxlIl0sImFtciI6WyJjdXN0b20iXX0.iXuxTUO1G2O7TZbjkGsqAXXEJsm9jnujU9isnuMcacgeyvTs6mjVZsD_NzEMs8QZobFpuRkfA4Yn9Y5BZvlPioM3An-x7v51xTNpwBzzhjA2cPYYOowJpRKBrpRtTi95nshztUWQr4sFWPfMcKtWI2iYvkx4v0w6c5zSan-Q33CnEn3XUOO7TYYU6mxYvBunpl2Rd5SOEu32gnQ7AtHUYP8MiCLn3fsHKeNMYuOBl_jI6dcB7ePR45QiGAr90HhEFxiWJ65V1Z3FGYC-s7zXeKVCTo1AjF5V3lBUmFHc6InYLEF888ak0j7s1eOAFo_9oQQH7lvIFsaonz49IBp-BA`
            });
          }
          if (request.url.includes('/Configuration')) {
            return ok({
              identityUrl: 'http://localhost:5001/api/identity/signin'
            });
          }
          return next.handle(request);
        })
      )
      .pipe(materialize())
      .pipe(dematerialize());

    function ok(body) {
      return of(new HttpResponse({ status: 200, body }));
    }

    function unauthorised() {
      return throwError({ status: 401, error: { message: 'Unauthorised' } });
    }

    function error(message) {
      return throwError({ status: 400, error: { message } });
    }
  }
}

export let LoginMockProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
