import { Helper } from 'src/app/shared/helper';
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable, of, throwError, from } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';
import { Project } from '../models/project.model';
import { Guid } from 'src/app/shared/guid';
import { SubProject } from '../models/sub-project.model';

@Injectable()
export class ProjectSelectionInterceptor implements HttpInterceptor {
  public projects: Project[];
  public SubProjects: SubProject[];

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.projects == null) {
      this.projects = [
        { id: Guid.newGuid(), title: 'Mr. Nice' },
        { id: Guid.newGuid(), title: 'Narco' },
        { id: Guid.newGuid(), title: 'Bombasto' },
        { id: Guid.newGuid(), title: 'Celeritas' },
        { id: Guid.newGuid(), title: 'Magneta' },
        { id: Guid.newGuid(), title: 'RubberMan' },
        { id: Guid.newGuid(), title: 'Dynama' },
        { id: Guid.newGuid(), title: 'Dr IQ' },
        { id: Guid.newGuid(), title: 'Magma' },
        { id: Guid.newGuid(), title: 'Tornado' }
      ];
      this.SubProjects = [
        {
          id: Guid.newGuid(),
          projectId: this.projects[0].id,
          title: 'Florida'
        },
        { id: Guid.newGuid(), projectId: this.projects[0].id, title: 'Athens' },
        { id: Guid.newGuid(), projectId: this.projects[2].id, title: 'LA' },
        {
          id: Guid.newGuid(),
          projectId: this.projects[2].id,
          title: 'New York'
        },
        { id: Guid.newGuid(), projectId: this.projects[4].id, title: 'Mexico' }
      ];
    }

    return of(null).pipe(
      mergeMap(() => {
        if (request.url.includes('/GetAllProjects')) {
          return ok(this.projects);
        }
        if (request.url.includes('/GetAllSubProjects')) {
          const projectId = Helper.getValueFromQueryString(
            request.urlWithParams,
            'projectId'
          );
          const a = this.SubProjects.filter(
            elem => elem.projectId === projectId
          );
          return ok(a); // subprojects
        }
        if (request.url.includes('/AddProject')) {
          const newProject = {
            id: Guid.newGuid(),
            title: request.body['title'],
            description: request.body['description']
          } as Project;
          this.projects.push(newProject);
          return ok(newProject);
        }
        if (request.url.includes('/DeleteProject')) {
          const projectId = Helper.getValueFromQueryString(
            request.urlWithParams,
            'id'
          );
          this.projects = this.projects.filter(x => x.id !== projectId);
          return ok({});
        }
        return next.handle(request);
      })
    );

    function ok(body) {
      return of(new HttpResponse({ status: 200, body }));
    }
  }
}

export let ProjectSelectionMockProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: ProjectSelectionInterceptor,
  multi: true
};
