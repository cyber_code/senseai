import { ThemeComponent } from './components/theme/theme.component';
import { HelpComponent } from './components/help/help.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {ProjectSelectionComponent} from './components/project-selection/project-selection.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ChangeProfileComponent} from './components/change-profile/change-profile.component';
import {ChangePasswordComponent} from './components/change-profile/change-password/change-password.component';
import {ChangeEmailComponent} from './components/change-profile/change-email/change-email.component';
import {ChangeUserNameComponent} from './components/change-profile/change-user-name/change-user-name.component';
import {MonitoringPageComponent} from './components/monitoring-page/monitoring-page.component';
import {AuthGuard} from '../shared/auth.guards';
import {ProjectSelectionGuard} from '../shared/project-selection.guards';
import {ModelChangesComponent} from './components/model-changes/model-changes.component';
import { SystemsCatalogsComponent } from './components/systems-catalogs/systems-catalogs.component';
import { JiraUserAuthComponent } from './components/navbar/components/jira-user-auth/jira-user-auth.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'not-found', component: NotFoundComponent},
  {path: 'project-selection', component: ProjectSelectionComponent, canActivate: [AuthGuard]},
  {path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'change-email', component: ChangeEmailComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'change-user-name', component: ChangeUserNameComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  {path: 'monitoring-page', component: MonitoringPageComponent},
  {path: 'model-changes', component: ModelChangesComponent},
  {path: 'systems-catalogs', component: SystemsCatalogsComponent, canActivate: [AuthGuard, ProjectSelectionGuard]},
  { path: 'jira-auth', component: JiraUserAuthComponent },
  {path: 'themes', component: ThemeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule {
}
