import {Component, OnInit, Inject, Input} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {DesignService} from '../../../design/services/design.service';
import {State, process} from '@progress/kendo-data-query';
import {MessageType} from '../../models/message.model';
import {MessageService} from '../../services/message.service';
import { isNullOrUndefined } from 'util';
import { PermissionService } from '../../services/permission.service';


@Component({
  selector: 'app-typical-attributes',
  templateUrl: './typical-attributes.component.html',
  styleUrls: ['./typical-attributes.component.css']
})

export class TypicalAttributesComponent implements OnInit {
  public gridData: any = [];
  public formGroup: FormGroup;
  private editedRowIndex: number;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor

    filter: {
      logic: 'and',
      filters: []
    }
  };
  @Input() typicalId: any;
  typicalAttributes: any;
  public deleteTypicalAttributeWindowOpen;
  public selectedTypicalAttribute;
  public editedTypical;
  public typicalTitle;

  constructor(private designService: DesignService, private messageService: MessageService, private permissionService: PermissionService) {
  }

  async ngOnInit() {

    let typicalAttributes = await this.getTypicalAttributes();
    this.typicalTitle = typicalAttributes.title;
    typicalAttributes = typicalAttributes['attributes'];
    this.typicalAttributes = typicalAttributes;
    this.gridData = process(typicalAttributes, this.state);
  }

  public addHandler({sender}) {
    this.closeEditor(sender);

    this.formGroup = new FormGroup({
      'name': new FormControl('', Validators.required),
      'typicalId': new FormControl(this.typicalId, Validators.required)
    });

    sender.addRow(this.formGroup);
  }

  public editHandler({sender, rowIndex, dataItem}) {
    this.closeEditor(sender);
    this.editedTypical = Object.assign({}, dataItem);
    this.formGroup = new FormGroup({
      'name': new FormControl(dataItem.name, Validators.required),
      'typicalId': new FormControl(this.typicalId, Validators.required)
    });
    this.editedRowIndex = rowIndex;

    sender.editRow(rowIndex, this.formGroup);
  }

  public cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({sender, rowIndex, formGroup, isNew}): void {
    const attribute = formGroup.value;

    if (!isNullOrUndefined(this.typicalAttributes)) {
      if (this.typicalAttributes.find(attr => attr.name === attribute.name) && attribute.name !== this.editedTypical.name) {
        this.messageService.sendMessage({type: MessageType.Error, text: 'There is already an attribute with ' + attribute.name + ' name'});
        sender.closeRow(rowIndex);
        return;
      }
    }
    if (!isNew) {
      // edit
      this.designService.updateTypicalAttributeManually(this.editedTypical.name, attribute.typicalId, attribute.name).subscribe(res => {
        if (res) {
          let attributes = this.typicalAttributes.map(attr => {
            if (attr.name === this.editedTypical.name)
              return {...attr, name: attribute.name };
            return attr;
          });
          this.typicalAttributes = attributes;
          this.gridData = process(this.typicalAttributes, this.state);
          sender.closeRow(rowIndex);
        }
      });
    } else {
      // add
      this.designService.addTypicalAttributeManually(attribute.name, attribute.typicalId).subscribe(res => {
        if (res) {
          if (isNullOrUndefined(this.typicalAttributes)) {
             // tslint:disable-next-line: max-line-length
             //this.typicalAttributes = {'id': attribute.typicalId , 'title': this.typicalTitle, 'attributes': [{'name' : attribute.name, 'isReqired': false}]} ;
             let typicalAtri = {'id': attribute.typicalId , 'title': this.typicalTitle, 'attributes': [{'name' : attribute.name, 'isReqired': false}]} ;
             this.typicalAttributes = typicalAtri.attributes;
             this.gridData = process(typicalAtri.attributes, this.state);
             sender.closeRow(rowIndex);
          } else {
          let attributes = this.typicalAttributes.concat(attribute);
          this.typicalAttributes = attributes;
          this.gridData = process(this.typicalAttributes, this.state);
          sender.closeRow(rowIndex);
          }
        }
      });
    }
  }

  public removeHandler({dataItem}): void {
    this.selectedTypicalAttribute = dataItem;
    this.deleteTypicalAttributeWindowOpen = true;
  }

  removeTypicalAttribute(deletedAttribute) {
    let attributes = this.typicalAttributes.filter(attribute => attribute.name !== deletedAttribute.name);
    this.typicalAttributes = attributes;
    this.gridData = process(this.typicalAttributes, this.state);
    this.closeDeleteAttribute();
  }

  closeDeleteAttribute() {
    this.deleteTypicalAttributeWindowOpen = false;
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.editedRowIndex = undefined;
    this.formGroup = undefined;
  }

  dataStateChange(state) {
    this.state = state;
    this.gridData = process(this.typicalAttributes, this.state);
  }

  listChange(typicalAttributes) {
    this.typicalAttributes = typicalAttributes;
    this.gridData = process(typicalAttributes, this.state);
  }

  public getTypicalAttributes() {
    return this.designService.getTypicalAttributes(this.typicalId).toPromise();
  }

  resetGridData() {
    this.designService.getTypicalAttributes(this.typicalId).subscribe(res => {
      this.gridData = process(res['attributes'], this.state);
    });
  }
}
