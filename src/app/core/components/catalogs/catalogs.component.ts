import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { State, process } from '@progress/kendo-data-query';
import { DesignService } from 'src/app/design/services/design.service';
import { SessionService } from '../../services/session.service';
import { Guid } from 'src/app/shared/guid';
import * as _ from 'lodash';
import { PermissionService } from '../../services/permission.service';


@Component({
  selector: 'app-catalogs',
  templateUrl: 'catalogs.component.html',
  styleUrls: ['catalogs.component.css']
})
export class CatalogsComponent implements OnInit {
    @Input() gridData;
    @Input() catalogs;
    @Input() systems;
    @Input() state;

    @Output() catalogDataStateChanged = new EventEmitter<any>();
    @Output() catalogListChanged = new EventEmitter<any>();

    public selectedCatalog = {
        title: '',
        description: '',
        level: '',
        type: '0'
    }

    public catalogWindowOpen = false;
    public deleteCatalogWindowOpen = false;
    public selectSystemWindowOpen = false;
    public isCatalogEdited = false;

    constructor(
        private designService: DesignService,
        private sessionService: SessionService,
        private permissionService: PermissionService
    ){}

    ngOnInit() {}

    dataStateChange(state) {
        this.catalogDataStateChanged.emit(state);
    }

    openCatalogWindow(catalog?) {
        this.catalogWindowOpen = true;
        if (catalog) {
            this.isCatalogEdited = true;
            this.selectedCatalog = _.cloneDeep(catalog);
        } else {
            this.resetCatalog();
        }
    }

    close() {
        this.catalogWindowOpen = false;
        this.deleteCatalogWindowOpen = false;
        this.selectSystemWindowOpen = false;
        this.resetCatalog();
    }

    resetCatalog() {
        this.isCatalogEdited = false;
        this.selectedCatalog = {
            title: '',
            description: '',
            level: '',
            type: '0'
        };
    }

    updateCatalogList(catalog) {
        let catalogs = [];
        if (this.isCatalogEdited) {
            catalogs = this.catalogs.map(cat => {
                if (cat.id === catalog.id) {
                    return catalog;
                }
                return cat;
            });
        } else {
            catalogs = this.catalogs.concat(catalog);
        }
        this.catalogListChanged.emit(catalogs);
        this.close();
    }

    removeCatalogFromList(catalog) {
        let catalogs = this.catalogs.filter(cat => cat.id !== catalog.id);
        this.catalogListChanged.emit(catalogs);
        this.close();
    }

    openDeleteCatalogWindow(catalog) {
        this.deleteCatalogWindowOpen = true;
        this.selectedCatalog = catalog;
    }

    async openSelectSystemWindow(catalog) {
        let subProject = this.sessionService.getSubProject();
        this.designService.getCatalogSystem(subProject.id, catalog.id).subscribe(res => {
            this.selectSystemWindowOpen = true;
            if (res) {
                let system = this.systems.find(sys => sys.id === res.systemId);
                this.selectedCatalog = _.cloneDeep({...catalog, system: system, systemCatalogId: res.id});
            } else {
                this.selectedCatalog = _.cloneDeep(catalog);
            }
        });
    }
}
