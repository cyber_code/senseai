import {Component, OnInit} from '@angular/core';
import {BooleanFilterCellComponent} from '@progress/kendo-angular-grid/dist/es2015/filtering/cell/boolean-filter-cell.component';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  alertTitle: string;
  alertMessage: string;
  showAlert: boolean = false;
  alertType: string;
  bgColor: string;

  constructor() {
  }

  ngOnInit() {
  }

  displayAlert(alertTitle: string, alertMessage: string, alertType: string) {
    this.showAlert = true;
    this.alertTitle = alertTitle;
    this.alertMessage = alertMessage;
    this.bgColor = (alertType === 'error') ? '#fc5c65' : '#227093';
  }
}
