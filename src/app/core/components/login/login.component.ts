import { ProjectSelectionService } from 'src/app/core/components/project-selection/project-selection.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticationService } from '../../services/authentication.service';
import { SubProjectChanged } from '../../models/sub-project-changed.model';
import { MessageComponent } from '../message/message.component';
import { BuilderService } from 'src/app/product/services/builder.service';
import { environment } from '../../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { ConfigurationService } from '../../services/configuration.service';
import { VersionService } from '../../services/version.service';
import { SessionService } from '../../services/session.service';
import {Licenses} from 'src/app/core/models/licenses.model';
import { LicenseService } from '../../services/licenses.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild(MessageComponent) message: MessageComponent;
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  uiVersion: string = '0.0.0.0';
  apiVersion: string = '0.0.0.0';
  mlVersion: string = '0.0.0.0';
  public currentYear: Number;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private projectSelectionService: ProjectSelectionService,
    private builderService: BuilderService,
    private http: HttpClient,
    private configurationService: ConfigurationService,
    private versionService: VersionService,
    private sessionService: SessionService,
    private licenseService: LicenseService
  ) {}

  ngOnInit() {
    if (this.authenticationService.isAuthenticated()) {
      this.licenseService.redirectToModuleByLicense();
    }
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.currentYear = new Date().getFullYear();
    this.uiVersion = environment.version;
    this.versionService.getApiVersion().subscribe((res: any) => {
      this.apiVersion = res.version;
    });
    this.versionService.getMLVersion().subscribe((res:any)=>{
      this.mlVersion = res.version;
    })
  }

  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      if (this.f.username.value.trim('') === '') {
        this.message.displayMessage(
          'Username must not be empty!',
          'Error',
          'danger'
        );
      } else if (this.f.password.value.trim('') === '') {
        this.message.displayMessage(
          'Password must not be empty!',
          'Error',
          'danger'
        );
      }
      return;
    }
    this.loading = true;
    this.authenticationService
      .login(this.f.username.value, this.f.password.value)
      .then(
        data => {
          this.builderService.removeBotSession();
          
          this.projectSelectionService.changeSelection(
            new SubProjectChanged(null, null)
          );
          this.authenticationService.authenticationChallange.next(true);
          this.authenticationService._isAuthenticated = true;
          this.router.navigateByUrl('/project-selection');
        },
        error => {
          this.message.displayMessage(
            'Error username or password!',
            'Authentication Error',
            'danger'
          );
          this.loading = false;
        }
      );
  }
}
