import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  @Output() public itemsToShow = new EventEmitter<any>();
  pager: any = {};
  showingFrom: number;
  showingTo: number;
  total: number;
  allItems: any = [];

  constructor() {}

  ngOnInit() {}

  getPager(totalItems: number, currentPage: number = 1, pageSize: number = 10) {
    let totalPages = Math.ceil(totalItems / pageSize);
    if (currentPage < 1) {
      currentPage = 1;
    } else if (currentPage > totalPages) {
      currentPage = totalPages;
    }

    let startPage: number, endPage: number;
    if (totalPages <= 10) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }
    let startIndex = (currentPage - 1) * pageSize;
    let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);
    let pages = Array.from(Array(endPage + 1 - startPage).keys()).map(
      i => startPage + i
    );

    return {
      totalItems: totalItems,
      currentPage: currentPage,
      pageSize: pageSize,
      totalPages: totalPages,
      startPage: startPage,
      endPage: endPage,
      startIndex: startIndex,
      endIndex: endIndex,
      pages: pages
    };
  }

  setPage(allItems: any, page: number) {
    if (!allItems) {
      allItems = this.allItems;
    } else {
      this.allItems = allItems;
    }
    this.pager = this.getPager(allItems.length, page);
    this.showingFrom =
      this.pager.currentPage * this.pager.pageSize - (this.pager.pageSize - 1);
    this.showingTo =
      allItems.length < this.pager.currentPage * this.pager.pageSize
        ? allItems.length
        : this.pager.currentPage * this.pager.pageSize;
    this.total = allItems.length;
    const pagedItems = allItems.slice(
      this.pager.startIndex,
      this.pager.endIndex + 1
    );
    this.itemsToShow.emit(pagedItems);
  }
}
