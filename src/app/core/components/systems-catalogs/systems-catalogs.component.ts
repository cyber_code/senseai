import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { DesignService } from 'src/app/design/services/design.service';
import { State, process } from '@progress/kendo-data-query';
import { System } from '../../models/system.model';
import {Guid} from 'src/app/shared/guid';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-systems-catalogs',
  templateUrl: 'systems-catalogs.component.html',
  styleUrls: ['systems-catalogs.component.css']
})
export class SystemsCatalogsComponent implements OnInit {
  public systems = [];
  public catalogs = [];
  public typicals = [];
  public attributes = [];
  public systemTags = ([] = []);

  public systemsGridData: any = [];
  public systemTagsGridData: any = ([] = []);
  public catalogsGridData: any = [];
  public typicalsGridData: any = [];
  public attributesGridData: any = [];
  getSystemId: string = '';
  getTypicalId: string = '';
  public systemState: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public systemTagState: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public catalogState: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public typicalState: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public attributeState: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };

  constructor(
    private sessionService: SessionService,
    private designService: DesignService
  ) {}

  async ngOnInit() {
    const systems = await this.getSystems();
    const catalogs = await this.getCatalogs();
    const typicals = await this.getTypicals();
    const systemTags = await this.getSystemTags();
    const attributes = await this.getAttributes();
    this.catalogs = catalogs;
    this.systems = systems;
    this.systemTags = systemTags;
    this.typicals = typicals;
    this.attributes = attributes;
    this.systemsGridData = process(systems, this.systemState);
    this.catalogsGridData = process(catalogs, this.catalogState);
    this.systemTagsGridData = process(systemTags, this.systemTagState);
    this.typicalsGridData = process(typicals, this.typicalState);
    this.attributesGridData = process(attributes, this.attributeState);
  }

  getCatalogs() {
    const subProject = this.sessionService.getSubProject();
    const project = this.sessionService.getProject();
    return this.designService.getTenantCatalogs(project.id, subProject.id).toPromise();
  }

  getSystems() {
    return this.designService.getSystems().toPromise();
  }

  getTypicals(event?: any) {
    if (!isNullOrUndefined(event)) {
      return this.designService.getTypicals(event).toPromise();
    } 
    return [];
  }
  getAttributes() {
    const workContext = this.sessionService.getWorkContext();
    const catalogId = workContext.defaultSettings.catalog.id;
    if (catalogId) {
      return this.designService.getTypicals(catalogId).toPromise();
    }
    return [];
  }

  getSystemTags(event?: any) {
    if (!isNullOrUndefined(event)) {
      return this.designService.getSystemTags(event).toPromise();
    } 
    return [];
  }

  systemDataStateChange(state) {
    this.systemState = state;
    this.systemsGridData = process(this.systems, state);
  }

  systemListChange(systems) {
    this.systems = systems;
    this.systemsGridData = process(systems, this.systemState);
  }

  catalogDataStateChange(state) {
    this.catalogState = state;
    this.catalogsGridData = process(this.catalogs, state);
  }

  catalogListChange(catalogs) {
    this.catalogs = catalogs;
    this.catalogsGridData = process(catalogs, this.catalogState);
  }

  typicalDataStateChange(state) {
    this.typicalState = state;
    this.typicalsGridData = process(this.typicals, state);
  }

  typicalListChange(typicals) {
    this.typicals = typicals;
    this.typicalsGridData = process(typicals, this.typicalState);
  }

  systemTagDataStateChange(state) {
    this.systemTagState = state;
    this.systemTagsGridData = process(this.systemTags, state);
  }

  async getIdOfSystem(event: any) {
    const systemTags = await this.getSystemTags(event);
    this.systemTags = systemTags;
    this.systemTagsGridData = process(systemTags, this.systemTagState);
  }

  systemIdChanged(state) {
    this.getSystemId = state;
  }
  typicalIdChanged(state){
    this.getTypicalId = state;
  }

  async getIdOfTypical(event: any) {
    const typicals = await this.getTypicals(event);
    this.typicals = typicals;
    this.typicalsGridData = process(typicals, this.typicalState);
  }

  systemTagsListChange(systemTags) {
    this.systemTags = systemTags;
    this.systemTagsGridData = process(systemTags, this.systemTagState);
  }


}
