import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { VersionService } from '../../services/version.service';
import { ProjectSelectionService } from '../project-selection/project-selection.service';
import { Location } from '@angular/common';
import { GetNumberOfTypicalChanges } from '../../services/command-queries/command-queries';
import { BuilderService } from 'src/app/product/services/builder.service';
import { SessionService } from '../../services/session.service';
import { LicenseService } from '../../services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public jiraAuthWindowOpen = false;
  public selectedProjectTitle: string;
  public selectedSubProjectTitle: string;
  public licenses = Licenses;
  @Input() uiVersion: string;
  @Input() apiVersion: string;
  @Input() mlVersion: string;
  @Input() numberOfTypicalChanges: number;
  @Output() defaultSettingsClicked = new EventEmitter<any>();
  @Output() userProfileClicked = new EventEmitter();
  @Output() closePopup = new EventEmitter();
  @Output() changePasswordClicked = new EventEmitter();

  constructor(
    public router: Router,
    private locationService: Location,
    private authenticationService: AuthenticationService,
    private projectSelectionService: ProjectSelectionService,
    private sessionService: SessionService,
    private builderService: BuilderService,
    public licenseService: LicenseService,
    public permissionService: PermissionService
  ) {
    this.projectSelectionService.subProjectSelected$.subscribe(res => {
      if (res.hasValue) {
        this.selectedProjectTitle = res.selectedProject.title;
        this.selectedSubProjectTitle = res.selectedSubProject.title;
      }
    });
  }

  ngOnInit() {
  }

  notification() {
    this.router.navigate(['/model-changes']);
    this.closePopups();
  }

  logOut() {
    this.builderService.removeBotSession();
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.closePopups();
  }

  userProfile() {
    this.userProfileClicked.emit();
  }

  closePopups() {
    this.closePopup.emit();
  }

  changePassword() {
    this.changePasswordClicked.emit();
  }

  select() {
    this.router.navigate(['/project-selection']);
    this.closePopups();
  }

  defaultSettings() {
    this.defaultSettingsClicked.emit();
  }

  goToSystemsCatalogs() {
    this.router.navigate(['/systems-catalogs']);
    this.closePopups();
  }
  themes() {
    this.router.navigate(['/theme']);
    this.closePopups();
  }

  checkJira() {
    this.router.navigate(['/jira-auth']);
    this.closePopups();
    // this.jiraAuthWindowOpen = true;
  }

  onJiraAuthDialogClose() {
    this.jiraAuthWindowOpen = false;
  }
}
