import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionService } from 'src/app/core/services/session.service';
import { JiraIntegrationService } from 'src/app/process-managment/services/jira-integration.service';

@Component({
  selector: 'app-jira-user-auth',
  templateUrl: './jira-user-auth.component.html',
  styleUrls: ['./jira-user-auth.component.css']
})
export class JiraUserAuthComponent implements OnInit {
  showModal = false;
  selectedJiraModelType = 2;
  selectedInstructionStep: any;
  instructionData = [
    {
      stepName: 'Step 1',
      imgSrc: 'assets/images/jira_integration/click_generate_token.jpg',
      description: 'Click <i>"Generate token"</i> button below token field'
    },
    {
      stepName: 'Step 2',
      imgSrc: 'assets/images/jira_integration/click_create_api_token.jpg',
      description: 'On Jira (Log in if needed) find and click <i> "Create API token" </i> button'
    },
    {
      stepName: 'Step 3',
      imgSrc: 'assets/images/jira_integration/name_and_click_create.jpg',
      description: 'Write a name (any name) on <i>Label</i> field and click on <i> "Create" </i> button'
    },
    {
      stepName: 'Step 4',
      imgSrc: 'assets/images/jira_integration/copy_token.jpg',
      description: 'A token will be generated for you. Click <i>"Copy to clipboard"</i> to copy your token.',
      extraText: 'PS: Please treat this token like you treat your password. Keep it secure and don\'t share it with anybody! '
    },
    {
      stepName: 'Login',
      imgSrc: 'assets/images/jira_integration/paste_your_token.jpg',
      description: 'Paste your generated token on token field. Fill other required fields and click "Continue" to finish login process.'
    }
  ];

  onPremiseInstructionData: any[] = [
    {
      stepName: 'Login',
      imgSrc: 'assets/images/jira_integration/JiraOnPremiseLogIn.jpg',
      description: 'Use your <i> username </i> and <i> password </i> to authenticate with Jira OnPremise'
    }
  ];


  returnUrl: string;
  jiraUsername: string;
  jiraToken: string;
  constructor(
    private jiraService: JiraIntegrationService,
    private session: SessionService,
    private router: Router,
    private route: ActivatedRoute) {
      this.route.queryParams.subscribe(params => {
        this.returnUrl = params['returnUrl'];
    });
    }

  ngOnInit() {
  }

  login() {
    const user = this.session.getUser().name;
    const subproject = this.session.getSubProject().id;
    this.jiraService.SetJiraUserSubprojectConfigs(user, subproject, this.jiraUsername, this.jiraToken, null)
      .subscribe(
        (res) => {
          if (res === true) {
            this.router.navigate([this.returnUrl]);
          }
        }
      );
  }

  selectJiraModel(e) {
    this.selectedJiraModelType = +e;
  }

  enlargeInstruction(instruction) {
    this.selectedInstructionStep = instruction;
  }
}
