import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as _ from 'lodash';
import { DataSet } from '../../../data-management/models/path-dataset.model';
import { WindowService } from '@progress/kendo-angular-dialog';
import { PermissionService } from '../../services/permission.service';
import { DesignService } from 'src/app/design/services/design.service';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-typicals',
  templateUrl: './typicals.component.html',
  styleUrls: ['./typicals.component.css']
})
export class TypicalsComponent implements OnInit {
  @Input() gridData;
  @Input() state;
  @Input() typicals;

  @Output() typicalDataStateChanged = new EventEmitter<any>();
  @Output() typicalListChanged = new EventEmitter<any>();
  @Output() typicalIdChanged = new EventEmitter<any>();

  public selectedTypical = {
    title: ''
  };
  public selectedTypicalId: any;
  public typicalWindowOpen = false;
  public deleteTypicalWindowOpen = false;
  public isTypicalEdited = false;
  catalogList: any[] = [];
  selectedCatalogIdFromKendoCmb: string = '';
  public selectedCatalog: string = '';

  constructor(private windowService: WindowService, private permissionService: PermissionService,
    private designService: DesignService, private sessionService: SessionService, ) { }

  ngOnInit() {
    this.getCatalogs();
  }

  dataStateChange(state) {
    this.typicalDataStateChanged.emit(state);
  }

  openTypicalWindow(catalog?) {
    this.typicalWindowOpen = true;
    if (catalog) {
      this.isTypicalEdited = true;
      this.selectedTypical = _.cloneDeep(catalog);
    } else {
      this.resetTypical();
    }
  }

  close() {
    this.typicalWindowOpen = false;
    this.deleteTypicalWindowOpen = false;
    this.resetTypical();
  }

  resetTypical() {
    this.isTypicalEdited = false;
    this.selectedTypical = {
      title: ''
    };
  }

  updateTypicalList(system) {
    let typicals = [];
    if (this.isTypicalEdited) {
      typicals = this.typicals.map(sys => {
        if (sys.id === system.id) {
          return system;
        }
        return sys;
      });
    } else {
      typicals = this.typicals.concat(system);
    }
    this.typicalListChanged.emit(typicals);
    this.close();
  }

  removeSystemFromList(system) {
    let typicals = this.typicals.filter(sys => sys.id !== system.id);
    this.typicalListChanged.emit(typicals);
    this.close();
  }

  openDeleteTypicalWindow(system) {
    this.deleteTypicalWindowOpen = true;
    this.selectedTypical = system;
  }

  viewRows(item, rowsTemplate) {
    this.selectedTypicalId = item.id;
    const window = this.windowService.open({
      title: `Attributes`,
      content: rowsTemplate,
      width: 920,
      height: 800
    });
  }

  systemIdChanged() {
    this.typicalIdChanged.emit(this.selectedCatalogIdFromKendoCmb);
  }


  getCatalogs() {
    const subProject = this.sessionService.getSubProject();
    const project = this.sessionService.getProject();
    return this.designService.getTenantCatalogs(project.id, subProject.id).subscribe(response => {
      this.catalogList = response;
    });
  }

  getTypicalsByCatalog(event: any) {
    if (event != undefined) {
      this.selectedCatalogIdFromKendoCmb = event.id;
      this.typicalIdChanged.emit(this.selectedCatalogIdFromKendoCmb);
    }
  }

}
