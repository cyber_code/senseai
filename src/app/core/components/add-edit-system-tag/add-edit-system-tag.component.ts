import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { MessageType } from '../../models/message.model';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageService } from '../../services/message.service';

@Component({
  selector: 'app-add-edit-system-tag',
  templateUrl: './add-edit-system-tag.component.html',
  styleUrls: ['./add-edit-system-tag.component.css']
})
export class AddEditSystemTagComponent implements OnInit {
  @Input() systemTagWindowOpen;
  @Input() selectedSystemTag;
  @Input() isSystemTagEdited;
  @Output() public closeSystemWindow = new EventEmitter<any>();
  @Output() public systemTagListUpdated = new EventEmitter<boolean>();
  constructor(
    private designService: DesignService,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  close() {
    this.closeSystemWindow.emit();
  }

  disableSubmitBtn() {
    return (
      this.selectedSystemTag.systemid === '' ||
      this.selectedSystemTag.description === '' ||
      this.selectedSystemTag.title === ''
    );
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedSystemTag.title = title;
  }

  changeDescription(event) {
    let title = event.target.value.trim('');
    this.selectedSystemTag.description = title;
  }

  submitClicked() {
    this.isSystemTagEdited = this.selectedSystemTag.id !== undefined;
    if (this.isSystemTagEdited) {
      this.editSystemTag();
    } else {
      this.addSystemTag();
    }
  }

  addSystemTag() {
    this.designService
      .addSystemTag(
        this.selectedSystemTag.systemId,
        this.selectedSystemTag.title,
        this.selectedSystemTag.description
      )
      .subscribe(res => {
        this.handleSystemResponse(res);
      });
  }

  editSystemTag() {
    this.designService
      .updateSystemTag(
        this.selectedSystemTag.id,
        this.selectedSystemTag.systemId,
        this.selectedSystemTag.title,
        this.selectedSystemTag.description
      )
      .subscribe(res => {
        this.handleSystemResponse(res);
      });
  }

  handleSystemResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isSystemTagEdited
          ? 'SystemTag was updated successfully!'
          : 'SystemTag was added successfully!',
        type: MessageType.Success
      });
      this.systemTagListUpdated.emit(
        this.isSystemTagEdited
          ? this.selectedSystemTag
          : { ...this.selectedSystemTag, id: res.id }
      );
    } else {
    }
    this.close();
  }
}
