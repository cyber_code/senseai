import {
  GetTypicalChanges,
  GetAcceptedTypicalChanges,
  GetNumberOfTypicalChanges,
  GetTypicalAttributeChangesById,
  GetAcceptedTypicalAttributeChangesById,
  AcceptChanges
} from '../../services/command-queries/command-queries';
import { Injectable } from '@angular/core';
import { ConfigurationService } from '../../services/configuration.service';
import { Observable, Subject } from 'rxjs';
import { HttpExecutorService } from '../../services/http-executor.service';
import { CommandMethod } from 'src/app/shared/command-method';

@Injectable()
export class ModelChangesService {
  private apiUrl: string;

  constructor(
    private configuration: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.apiUrl = `${configuration.serverSettings.apiUrl}Data/`;
  }

  changesApproved: Subject<boolean> = new Subject<boolean>();
  changesApproved$ = this.changesApproved.asObservable();

  GetTypicalChanges(): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.apiUrl,
      new GetTypicalChanges(),
      CommandMethod.GET
    );
  }

  GetAcceptedTypicalChanges(): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.apiUrl,
      new GetAcceptedTypicalChanges(),
      CommandMethod.GET
    );
  }

  GetNumberOfTypicalChanges(): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.apiUrl,
      new GetNumberOfTypicalChanges(),
      CommandMethod.GET
    );
  }

  GetTypicalAttributeChangesById(typicalId: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.apiUrl,
      new GetTypicalAttributeChangesById(typicalId),
      CommandMethod.GET
    );
  }

  AcceptChanges(typicalId: string[], markAsInvalid: boolean): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.apiUrl,
      new AcceptChanges(typicalId, markAsInvalid),
      CommandMethod.POST
    );
  }

  GetAcceptedTypicalAttributeChangesById(typicalId: any): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.apiUrl,
      new GetAcceptedTypicalAttributeChangesById(typicalId),
      CommandMethod.GET
    );
  }
}
