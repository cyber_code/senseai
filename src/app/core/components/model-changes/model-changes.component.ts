import { Component, OnInit, ViewChild } from '@angular/core';
import { ModelChangesService } from './model-changes.service';
import { PaginationComponent } from '../pagination/pagination.component';
import { MessageType } from 'src/app/core/models/message.model';
import { MessageService } from '../../services/message.service';
import { DataResult, process, State } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent, RowArgs, SelectAllCheckboxState } from '@progress/kendo-angular-grid/dist/es2015';
import { SelectableSettings } from '@progress/kendo-angular-grid';
import { forkJoin } from 'rxjs';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-model-changes',
  templateUrl: './model-changes.component.html',
  styleUrls: ['./model-changes.component.css']
})
export class ModelChangesComponent implements OnInit {
  pagedTypicals: any = [];
  typicals: any = [];
  acceptedTypicals: any = [];
  pagedAcceptedTypicals: any = [];
  allItems: any = [];
  newTypicalAttributeChanges: any = [];
  deletedTypicalAttributeChanges: any = [];
  changedTypicalAttributeChanges: any = [];
  listOfWorkflowTypicalAttributeChanges: any = [];
  selectedTypicalId: string;
  acceptedTypicalId: string;
  markAsInvalid = false;
  isCollapsedNewAttributes = true;
  isCollapsedDeletedAttributes = true;
  isCollapsedChangedAttributes = true;
  markAsInvalidMultiRows: boolean = false;
  isPopUpOpen: boolean = false;
  loading: boolean = false;
  public selectableSettings: SelectableSettings;
  public selectAllState: SelectAllCheckboxState = 'unchecked';

  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public gridData: DataResult;
  public stateapprove: State = {
    skip: 0,
    take: 10,
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public gridDataapprove: GridDataResult;
  public windowOpened = false;
  public windowOpenedAccept = false;
  public selectedTab = 'Actual Changes';

  constructor(private modelChangesService: ModelChangesService, private messageService: MessageService, private permissionService: PermissionService) {
    this.selectableSettings = {
      checkboxOnly: true,
      mode: 'multiple'
    };
  }

  public mySelection: string[] = [];
  public mySelectionKey(context: RowArgs): string {
    return context.dataItem.id;
  }

  public onSelectedKeysChange(e) {
    const len = this.mySelection.length;

    if (len === 0) {
      this.selectAllState = 'unchecked';
    } else if (len > 0 && len < this.typicals.length) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = 'checked';
    }
  }

  public onSelectAllChange(checkedState: SelectAllCheckboxState) {
    if (checkedState === 'checked') {
      this.mySelection = this.typicals.map((typical) => typical.id);
      this.selectAllState = 'checked';
    } else {
      this.mySelection = [];
      this.selectAllState = 'unchecked';
    }
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gridData = process(this.typicals, this.state);
    this.loadData();
  }

  public dataStateChangeapprove(state: DataStateChangeEvent): void {
    this.stateapprove = state;
    this.gridDataapprove = process(this.acceptedTypicals, this.state);
    this.loadData();
  }



  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.modelChangesService.GetTypicalChanges().subscribe(res => {
      this.typicals = res;
      this.gridData = process(this.typicals, this.state);
    });
    this.modelChangesService.GetAcceptedTypicalChanges().subscribe(res => {
      this.acceptedTypicals = res;
      this.gridDataapprove = process(this.acceptedTypicals, this.stateapprove);
    });
  }

  public close(component) {
    this[component + 'Opened'] = false;

    this.isCollapsedNewAttributes = true;
    this.isCollapsedDeletedAttributes = true;
    this.isCollapsedChangedAttributes = true;
  }

  public closeAccepted(component) {
    this[component + 'OpenedAccept'] = false;

    this.isCollapsedNewAttributes = true;
    this.isCollapsedDeletedAttributes = true;
    this.isCollapsedChangedAttributes = true;
  }

  public open(component, typicalId) {
    this.selectedTypicalId = typicalId;
    this.modelChangesService.GetTypicalAttributeChangesById(typicalId).subscribe(res => {
      if (res) {
        this.newTypicalAttributeChanges = res.newAttributes;
        this.deletedTypicalAttributeChanges = res.deletedAttributes;
        this.changedTypicalAttributeChanges = res.changedAttributes;
        this.listOfWorkflowTypicalAttributeChanges = res.listOfAffectedWorkflows;
        this[component + 'Opened'] = true;
      } else {
        this.newTypicalAttributeChanges = [];
        this.messageService.sendMessage({ text: 'There are no attribute changes for this typical!', type: MessageType.Error });
      }
    });
  }


  public openAccepted(component, typicalId) {
    this.acceptedTypicalId = typicalId;
    this.modelChangesService.GetAcceptedTypicalAttributeChangesById(typicalId).subscribe(res => {
      if (res) {
        this.newTypicalAttributeChanges = res.newAttributes;
        this.deletedTypicalAttributeChanges = res.deletedAttributes;
        this.changedTypicalAttributeChanges = res.changedAttributes;
        // this.listOfWorkflowTypicalAttributeChanges = res.listOfAffectedWorkflows;
        this[component + 'OpenedAccept'] = true;
      } else {
        this.newTypicalAttributeChanges = [];
        this.messageService.sendMessage({ text: 'There are no attribute changes for this typical!', type: MessageType.Error });
      }
    });
  }

  public setPagedTypicals(typicals) {
    if (this.selectedTab === 'Actual Changes') {
      this.pagedTypicals = typicals;
    }
  }

  public setPagedAcceptedTypicals(acceptedTypicals) {
    if (this.selectedTab === 'Approved Changes') {
      this.pagedAcceptedTypicals = acceptedTypicals;
    }
  }

  pageChanges(evt) {
    this.selectedTab = evt.title;
    this.loadData();
  }

  acceptChanges() {
    this.modelChangesService.AcceptChanges([this.selectedTypicalId], this.markAsInvalid).subscribe(res => {
      this.modelChangesService.changesApproved.next(true);
      this.loadData();
    });
    this.close('window');
  }

  acceptMultiChanges() {
    this.isPopUpOpen = true;
  }

  markWorkflow(evt) {
    this.markAsInvalid = evt.target.checked;
  }
  markMultiWorkflows(evt) {
    this.markAsInvalidMultiRows = evt.target.checked;
  }

  closeWindow() {
    this.isPopUpOpen = false;
  }

  approveConfirm() {
    this.loading = true;
    const typicalsToApprove = this.typicals.filter(typical =>
      this.mySelection.find(selection => selection === typical.id)
    ).map(x => x.id);

    // .map(res =>{
    return this.modelChangesService.AcceptChanges(typicalsToApprove, this.markAsInvalidMultiRows).
      // });
      // forkJoin(typicalsToApprove).
      subscribe(result => {
        this.loading = false;
        this.loadData();
        this.mySelection = [];
        this.isPopUpOpen = false;
        this.modelChangesService.changesApproved.next(true);
      },
        (err) => {
          this.loading = false;
        });

  }
}
