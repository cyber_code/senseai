import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageType } from '../../models/message.model';
import { MessageService } from 'src/app/core/services/message.service';
import { SessionService } from '../../services/session.service';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-select-system',
  templateUrl: 'select-system.component.html',
  styleUrls: ['select-system.component.css']
})
export class SelectSystemComponent implements OnInit {
  @Input() selectSystemWindowOpen;
  @Input() systems;
  @Input() typicals;
  @Input() selectedCatalog;

  @Output() closeSelectSystemWindow = new EventEmitter<any>();

  constructor(
    private designService: DesignService,
    private sessionService: SessionService,
    private messageService: MessageService,
    private permissionService: PermissionService
  ) {}

  ngOnInit() {}

  close() {
    this.closeSelectSystemWindow.emit();
  }

  changeCatalogSystem(system) {
    this.selectedCatalog.system = system;
  }

  addSystemToCatalog() {
    let subProject = this.sessionService.getSubProject();
    if (this.selectedCatalog.systemCatalogId) {
      this.updateSystemCatalog(subProject);
    } else {
      this.addSystemCatalog(subProject);
    }
  }

  updateSystemCatalog(subProject) {
    this.designService
      .updateSystemCatalog(
        this.selectedCatalog.systemCatalogId,
        subProject.id,
        this.selectedCatalog.system.id,
        this.selectedCatalog.id
      )
      .subscribe(res => {
        this.handleResponse(res);
      });
  }

  addSystemCatalog(subProject) {
    this.designService
      .addSystemCatalog(
        subProject.id,
        this.selectedCatalog.system.id,
        this.selectedCatalog.id
      )
      .subscribe(res => {
        this.handleResponse(res);
      });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text:
          'Catalog ' +
          this.selectedCatalog.title +
          ' was added to system ' +
          this.selectedCatalog.system.title,
        type: MessageType.Success
      });
      this.close();
    } else {
      this.close();
    }
  }

  disableSubmitButton() {
    return !this.selectedCatalog || !this.selectedCatalog.system;
  }
}
