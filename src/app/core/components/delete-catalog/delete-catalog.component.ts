import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageType } from '../../models/message.model';
import {MessageService} from 'src/app/core/services/message.service';


@Component({
  selector: 'app-delete-catalog',
  templateUrl: 'delete-catalog.component.html',
  styleUrls: ['delete-catalog.component.css']
})
export class DeleteCatalogComponent implements OnInit {
    @Input() deleteCatalogWindowOpen;
    @Input() selectedCatalog;

    @Output() public closeDeleteCatalogWindow = new EventEmitter<any>();
    @Output() public removeCatalogFromList = new EventEmitter<boolean>();

    constructor(
        private designService: DesignService,
        private messageService: MessageService
    ) {}

    ngOnInit() {}

    deleteCatalog() {
        this.designService.deleteCatalog(this.selectedCatalog.id).subscribe(res => {
            this.handleResponse(res);
        });
    }

    handleResponse(res) {
        if (res) {
            this.messageService.sendMessage({text: 'Catalog was deleted successfully!', type: MessageType.Success});
            this.removeCatalogFromList.emit(this.selectedCatalog);
        } else {
            this.close();
        }
    }

    close() {
        this.closeDeleteCatalogWindow.emit();
    }

}
