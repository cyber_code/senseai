import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageType } from '../../models/message.model';
import {MessageService} from 'src/app/core/services/message.service';


@Component({
  selector: 'app-delete-system',
  templateUrl: 'delete-system.component.html',
  styleUrls: ['delete-system.component.css']
})
export class DeleteSystemComponent implements OnInit {
    @Input() deleteSystemWindowOpen;
    @Input() selectedSystem;

    @Output() public closeDeleteSystemWindow = new EventEmitter<any>();
    @Output() public removeSystemFromList = new EventEmitter<boolean>();

    constructor(
        private designService: DesignService,
        private messageService: MessageService
    ) {}

    ngOnInit() {}

    deleteSystem() {
        this.designService.deleteSystem(this.selectedSystem.id).subscribe(res => {
            this.handleResponse(res);
        });
    }

    handleResponse(res) {
        if (res) {
            this.messageService.sendMessage({text: 'System was deleted successfully!', type: MessageType.Success});
            this.removeSystemFromList.emit(this.selectedSystem);
        } else {
            this.close();
        }
    }

    close() {
        this.closeDeleteSystemWindow.emit();
    }

}
