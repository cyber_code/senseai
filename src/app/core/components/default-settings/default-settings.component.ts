import {AdministrationService} from 'src/app/core/services/administration.service';
import {DefaultSettings, Settings} from './../../models/default-settings.model';
import {System} from './../../../design/models/system.model';
import {DesignService} from './../../../design/services/design.service';
import {Component, OnInit, AfterViewInit, ViewChild, EventEmitter, Output} from '@angular/core';
import {SessionService} from '../../services/session.service';
import {DefaultSettingsService} from './default-settings.service';
import {MessageService} from 'src/app/core/services/message.service';
import {MessageType} from 'src/app/core/models/message.model';
import {Catalog} from 'src/app/core/models/catalog.model';
import {switchMap, tap, map} from 'rxjs/operators';
import {from, Subject, zip} from 'rxjs';
import {Project} from '../../models/project.model';
import {SubProject} from '../../models/sub-project.model';
import {Guid} from 'src/app/shared/guid';
import { isNullOrUndefined } from 'util';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-default-settings',
  templateUrl: './default-settings.component.html',
  styleUrls: ['./default-settings.component.css']
})
export class ChangeDefaultSettingsComponent implements OnInit, AfterViewInit {
  public defaultSettings: DefaultSettings = {catalog: new Catalog(), system: new System()} as DefaultSettings;
  public applicationCatalogs: Catalog[];
  public systems: System[];
  public project: Project;
  public subProject: SubProject;
  // this is for now, because later we can use server side filtering in order to not load all the typicals
  public allCatalogs: Catalog[];
  public allSystems: System[];
  @ViewChild('cmbAppilcationCatalogs') cmbAppilcationCatalogs;
  @ViewChild('cmbAppilcationSystems') cmbAppilcationSystems;

  private settingsDataLoaded = new Subject<string>();
  private settingsDataLoaded$ = this.settingsDataLoaded.asObservable();
  private loadedData: string[] = [];
  @Output() closeComponent = new EventEmitter<any>();

  constructor(
    private sessionService: SessionService,
    public changeDefaultSettingsService: DefaultSettingsService,
    private messageService: MessageService,
    private designService: DesignService,
    private administrationService: AdministrationService,
    private permissionService: PermissionService
  ) {
  }

  public loadData() {
    this.project = this.sessionService.getWorkContext().project;
    this.subProject = this.sessionService.getWorkContext().subProject;
    this.defaultSettings = this.sessionService.getDefaultSettings();

    const catalogs$ = this.designService.getTenantCatalogs(this.project.id, this.subProject.id);
    const systems$ = this.designService.getSystems();
    zip(catalogs$, systems$).subscribe(result => {
      this.allCatalogs = result[0] || [];
      this.allSystems = result[1] || [];

      this.applicationCatalogs = [...this.allCatalogs];
      this.systems = [...this.allSystems];
      if (!Guid.isEmpty(this.defaultSettings.catalog.id)) {
        this.defaultSettings.catalog = this.applicationCatalogs.find(x => x.id === this.defaultSettings.catalog.id);
      }
      if (!Guid.isEmpty(this.defaultSettings.system.id)) {
        this.defaultSettings.system = this.systems.find(x => x.id === this.defaultSettings.system.id);
      }
    });
  }

  ngOnInit() {
    this.loadData();
  }

  ngAfterViewInit() {
    this.cmbAppilcationCatalogs.filterChange
      .asObservable()
      .pipe(
        switchMap(value =>
          from([this.allCatalogs]).pipe(
            tap(() => (this.cmbAppilcationCatalogs.loading = true)),
            map(data => {
              return data.filter(x => x.title.indexOf(value as string) > -1);
            })
          )
        )
      )
      .subscribe(filtered => {
        this.applicationCatalogs = filtered;
        this.cmbAppilcationCatalogs.loading = false;
      });
    this.cmbAppilcationSystems.filterChange
      .asObservable()
      .pipe(
        switchMap(value =>
          from([this.allSystems]).pipe(
            tap(() => (this.cmbAppilcationSystems.loading = true)),
            map(data => {
              return data.filter(x => x.title.indexOf(value as string) > -1);
            })
          )
        )
      )
      .subscribe(filtered => {
        this.systems = filtered;
        this.cmbAppilcationSystems.loading = false;
      });
  }

  applicationCatalogChanged(newValue: Catalog) {
    this.defaultSettings.catalog = newValue || new Catalog();
  }

  systemChanged(newValue: System) {
    this.defaultSettings.system = newValue || new System();
  }

  back() {
    this.closeComponent.emit();
  }

  private isDefaultSettingsSelected() {
    return (
      this.defaultSettings.catalog &&
      !Guid.isEmpty(this.defaultSettings.catalog.id) &&
      this.defaultSettings.system &&
      !Guid.isEmpty(this.defaultSettings.system.id)
    );
  }

  confirm() {
    if (!this.isDefaultSettingsSelected()) {
      return;
      // TODO ASTRIT WITH VALIDATION
    }
    this.sessionService.setDefaultSettings(this.defaultSettings);
    if (this.permissionService.hasPermission("/api/Administration/GetSetting")){
      this.administrationService.getSettings(this.project.id, this.subProject.id).subscribe(result3 => {
        if (isNullOrUndefined(result3) || !result3.id) {
          if (this.permissionService.hasPermission("/api/Administration/AddSettings"))
            this.changeDefaultSettingsService
              .addDefaultSettings(this.project.id, this.subProject.id, this.defaultSettings.system.id, this.defaultSettings.catalog.id)
              .subscribe(result => {
                if (result.error) {
                  this.messageService.sendMessage({text: result.error, type: MessageType.Error});
                } else {
                  this.messageService.sendMessage({text: 'Default Settings Saved', type: MessageType.Info});
                  this.back();
                }
              });
        } else {
          if (this.permissionService.hasPermission("/api/Administration/UpdateSettings"))
            this.changeDefaultSettingsService
              .changeDefaultSettings(
                result3.id,
                this.project.id,
                this.subProject.id,
                this.defaultSettings.system.id,
                this.defaultSettings.catalog.id
              )
              .subscribe(result => {
                if (result !== null && result.error) {
                  this.messageService.sendMessage({text: result.error, type: MessageType.Error});
                } else {
                  this.messageService.sendMessage({text: 'Default Settings Saved', type: MessageType.Info});
                  this.back();
                }
              });
        }
      });
    }

  }

}
