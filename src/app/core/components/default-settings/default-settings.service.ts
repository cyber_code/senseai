import {
  AddDefaultSettings,
  ChangeDefaultSettings
} from './../../services/command-queries/command-queries';
import {Injectable} from '@angular/core';
import {ConfigurationService} from '../../services/configuration.service';
import {Observable} from 'rxjs';
import {HttpExecutorService} from '../../services/http-executor.service';
import {CommandMethod} from 'src/app/shared/command-method';

@Injectable()
export class DefaultSettingsService {
  private apiUrl: string;

  constructor(
    private configuration: ConfigurationService,
    private httpExecutor: HttpExecutorService
  ) {
    this.apiUrl = `${configuration.serverSettings.apiUrl}Administration/`;
  }

  changeDefaultSettings(
    id: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.apiUrl,
      new ChangeDefaultSettings(
        id,
        projectId,
        subProjectId,
        systemId,
        catalogId
      ),
      CommandMethod.PUT
    );
  }

  addDefaultSettings(
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.apiUrl,
      new AddDefaultSettings(projectId, subProjectId, systemId, catalogId),
      CommandMethod.POST
    );
  }
}
