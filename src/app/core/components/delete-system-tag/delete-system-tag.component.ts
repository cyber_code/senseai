import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageService } from '../../services/message.service';
import { MessageType } from '../../models/message.model';

@Component({
    selector: 'app-delete-system-tag',
    templateUrl: './delete-system-tag.component.html',
    styleUrls: ['./delete-system-tag.component.css']
})
export class DeleteSystemTagComponent implements OnInit {
    @Input() deleteSystemTagWindowOpen;
    @Input() selectedSystemTag;

    @Output() public closeDeleteSystemTagWindow = new EventEmitter<any>();
    @Output() public removeSystemTagFromList = new EventEmitter<boolean>();

    constructor(
        private designService: DesignService,
        private messageService: MessageService
    ) { }

    ngOnInit() { }

    deleteSystemTag() {
        this.designService.deleteSystemTag(this.selectedSystemTag.id).subscribe(res => {
            this.handleResponse(res);
        });
    }
    handleResponse(res) {
        if (res) {
            this.messageService.sendMessage({ text: 'System Tag was deleted successfully!', type: MessageType.Success });
            this.removeSystemTagFromList.emit(this.selectedSystemTag);
        } else {
            this.close();
        }
    }
    close() {
        this.closeDeleteSystemTagWindow.emit();
    }

}
