import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TypicalType} from '../../models/typical-type.model';
import {DesignService} from '../../../design/services/design.service';
import {MessageService} from '../../services/message.service';
import {SessionService} from '../../services/session.service';
import {MessageType} from '../../models/message.model';

@Component({
  selector: 'app-add-edit-attributes',
  templateUrl: './add-edit-attributes.component.html',
  styleUrls: ['./add-edit-attributes.component.css']
})
export class AddEditAttributesComponent implements OnInit {
  @Input() isTypicalEdited;
  @Input() selectedTypical;
  @Input() typicalWindowOpen;

  @Output() public closeTypicalWindow = new EventEmitter<any>();
  @Output() public typicalListUpdated = new EventEmitter<boolean>();

  public typicalTypes = [{title: 'Application', type: TypicalType.Application},
    {title: 'Version', type: TypicalType.Version},
    {title: 'AA Application', type: TypicalType.AAAplication},
    {title: 'Enquiry', type: TypicalType.Enquiry},
    {title: 'Project Resourcing', type: TypicalType.ProjectResourcing}];

  public selectedTypicalType;

  workContext: any;

  constructor(private designService: DesignService, private messageService: MessageService, private sessionService: SessionService) {}

  ngOnInit() {
    this.workContext = this.sessionService.getWorkContext();
  }

  close() {
    this.closeTypicalWindow.emit();
    this.selectedTypicalType = null;
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedTypical.title = title;
  }

  disableSubmitBtn() {
    return (
      (!this.selectedTypicalType &&
        !this.isTypicalEdited) ||
      this.selectedTypical.title === ''
    );
  }

  submitClicked() {
    if (this.isTypicalEdited) {
      this.editTypicalManually();
    } else {
      this.addTypicalManually();
    }
  }

  addTypicalManually() {
    this.designService
      .addTypicalManually(
        this.workContext.project.id,
        this.workContext.subProject.id,
        this.workContext.defaultSettings.system.id,
        this.workContext.defaultSettings.catalog.id,
        this.selectedTypical.title,
        this.selectedTypical.type,
        this.workContext.user.tenantId
      )
      .subscribe(res => {
        this.handleSystemResponse(res);
      });
  }

  editTypicalManually() {
    this.designService.updateTypicalManually(this.selectedTypical.id, this.selectedTypical.title).subscribe(res => {
      this.handleSystemResponse(res);
    });
  }

  handleSystemResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isTypicalEdited ? 'Typical was updated successfully!' : 'Typical was added successfully!',
        type: MessageType.Success
      });
      this.typicalListUpdated.emit(this.isTypicalEdited ? this.selectedTypical : { ...this.selectedTypical, id: res.id });
      this.selectedTypicalType = null;
    } else {
      this.close();
    }
  }

  changeTypicalType(typicalType) {
    this.selectedTypicalType = typicalType;
    this.selectedTypical.type = typicalType ? typicalType.type : null;
  }

}
