import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {
  map,
  debounceTime,
  switchMap,
  distinctUntilChanged
} from 'rxjs/operators';
import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  AfterViewInit,
  Input,
  ViewChildren,
  QueryList
} from '@angular/core';
import { of } from 'rxjs/observable/of';

import { isNullOrUndefined } from 'util';

import { ArisNodeType } from 'src/app/shared/aris-node-type';
import { ArisWorkflow } from 'src/app/process-managment/models/aris-workflow.model';
import { TreeViewItem } from 'src/app/shared/tree-view-item';
import { SubProject } from 'src/app/core/models/sub-project.model';
import { ProjectSelectionService } from 'src/app/core/components/project-selection/project-selection.service';
import { SessionService } from 'src/app/core/services/session.service';
import { Guid } from 'src/app/shared/guid';
import { HelperService } from '../../../shared/services/helper.service';
import { ProcessManagementService } from 'src/app/process-managment/services/process-management.service';
import { ArisTreeViewItem } from 'src/app/shared/aris-tree-view-item';
import { Project } from '../../models/project.model';

@Component({
  selector: 'app-aris-tree-view',
  templateUrl: './aris-tree-view.component.html',
  styleUrls: ['./aris-tree-view.component.css']
})
export class AppArisTreViewComponent implements OnInit, AfterViewInit {
  private data: ArisTreeViewItem[] = [];
  private _subProject: SubProject;

  public filterdData: ArisTreeViewItem[] = [];
  public selectedKeys: String[] = [];
  public expandedKeys: String[] = [];
  public disabledKeys: String[] = [];
  public searchTerm: String = '';
  public draggedItem: string;
  public editModeState = false;
  private _helper: HelperService;
  @ViewChildren('checkPermission') checkPermission: QueryList<any>;

  public selectedItem: ArisTreeViewItem;
  @Output() public treeViewItemSelected = new EventEmitter<ArisTreeViewItem>();
  private newSearchTermTyped = new BehaviorSubject<string>('');

  private firstLoad: Boolean = true;

  constructor(
    private processManagementService: ProcessManagementService,
    private projectSelectionService: ProjectSelectionService,
    private session: SessionService
  ) {
    this.newSearchTermTyped
      .pipe(
        // wait 300ms after each keystroke before considering the term
        debounceTime(300),
        // ignore new term if same as previous term
        distinctUntilChanged(),
        // switch to new search observable each time the term changes
        switchMap((term: string) =>
          term
            ? this.processManagementService.searchArisWorkflows(
                term,
                this.getSubProject().id
              )
            : of([])
        ),
        // map workflow to TreeViewItem
        map((workflows: ArisWorkflow[]) =>
          workflows.map(
            item =>
              new ArisTreeViewItem(
                item.id,
                item.title,
                ArisNodeType.ArisWorkflow,
                [],
                item.processId
              )
          )
        )
      )
      .subscribe(result => {
        if (this.firstLoad) {
          this.firstLoad = false;
          return;
        }
        const currentValue = this.newSearchTermTyped.getValue();
        if (currentValue === '') {
          this.getProcesses();
        } else {
          this.populateDataSource(result);
          this.expandRootFolder();
        }
      });
  }

  private getProcesses() {
    const subProject = this.getSubProject();
    if (isNullOrUndefined(subProject) || Guid.isEmpty(subProject.id)) {
      return;
    }
    this.processManagementService
      .getProcesses('f5c6e4ad-ad2c-4225-8c3b-d0c52789b727', subProject.id)
      .subscribe(result => {
        if (result) {
          this.data = result.map(
            item =>
              new ArisTreeViewItem(item.id, item.title, ArisNodeType.Process, [])
          );
        }
        this.filterdData = [
          new ArisTreeViewItem(
            subProject.id,
            subProject.title,
            ArisNodeType.Root,
            this.data
          )
        ];
        this.expandRootFolder();
      });
  }

  ngAfterViewInit() {}

  ngOnInit() {
    // this will be call the subscriber because it's the first time
    this.projectSelectionService.subProjectSelected$.subscribe(p => {
      if (p.hasValue) {
        this._subProject = p.selectedSubProject;
        this.searchTerm = '';
        this.getProcesses();
      }
    });
  }

  onDragStart(event) {
    this.draggedItem = event.target.id;
  }

  public onkeyup(value: string): void {
    this.newSearchTermTyped.next(value);
  }

  public fetchChildren = (dataItem: ArisTreeViewItem) =>
    // tslint:disable-next-line:semicolon
    dataItem.type === ArisNodeType.Root
      ? of(this.filterdData[0].items)
      : this.processManagementService.fetchArisWorkflows(dataItem.id);

  // store children in component data source
  public childrenLoaded($event: any) {
    if ($event !== null && $event.item.dataItem.type !== ArisNodeType.Root) {
      const folder = this.data.find(x => x.id === $event.item.dataItem.id);
      folder.items = $event.children.map(child => child.dataItem);
    }
  }

  reloadFolderItems(dataItem: ArisTreeViewItem) {
    this.processManagementService
      .fetchArisWorkflows(dataItem.id)
      .subscribe(workflows => {
        if (workflows) {
          this.selectedItem.items = workflows;
          this.populateDataSource(this.data);
        }
      });
  }

  public hasChildren = (dataitem: any): boolean =>
    dataitem.type !== ArisNodeType.ArisWorkflow;

  public iconClass(data: any): any {
    if (data.type === ArisNodeType.ArisWorkflow) {
      return 'k-icon k-i-share';
    } else if (data.type === ArisNodeType.Process) {
      return 'k-icon k-i-folder';
    } else {
      return 'k-icon k-i-folder-more';
    }
  }

  public disable(disable: boolean) {
    this.editModeState = disable;
  }

  public handleSelection($event: any): void {
    if (this.editModeState === true) {
      return;
    }
    this.selectedItem = $event.dataItem;
    this.treeViewItemSelected.emit(this.selectedItem);
  }

  public addTreeViewElement(treeViewItem: ArisTreeViewItem): void {
    if (!this.selectedItem.items) {
      this.selectedItem.items = [];
    }
    this.selectedItem.items.push(treeViewItem);
    this.populateDataSource(this.data);
    if (treeViewItem.type === ArisNodeType.ArisWorkflow) {
      this.selectedItem = treeViewItem;
      this.treeViewItemSelected.emit(this.selectedItem);
      this.selectedKeys = [this.selectedItem.id];
    }
  }

  public cutTreeViewElement(treeViewItem: ArisTreeViewItem) {
    const newData = this.data.filter(item => item.id !== treeViewItem.id);
    this.data = newData;
    this.populateDataSource(newData);
  }

  public deleteTreeViewElement(treeViewItem: ArisTreeViewItem): void {
    const newData = this.data.filter(item => item.id !== treeViewItem.id);
    this.data = newData;
    this.populateDataSource(newData);
  }

  public selectWfItemAndExpand(workflowItemId: string) {
    this.selectedKeys = [workflowItemId];
  }

  //#region  private methods
  private expandRootFolder() {
    this.expandedKeys = [this.getSubProject().id];
    this.selectedItem = this.filterdData.length > 0 && this.filterdData[0];
    this.treeViewItemSelected.emit(this.selectedItem);
  }

  private populateDataSource(items: ArisTreeViewItem[]) {
    this.filterdData = [
      new ArisTreeViewItem(
        this.getSubProject().id,
        this.getSubProject().title,
        ArisNodeType.Root,
        items
      )
    ];
  }

  private getSubProject(): SubProject {
    if (isNullOrUndefined(this._subProject)) {
      this._subProject = this.session.getWorkContext().subProject;
    }
    return this._subProject;
  }
}
