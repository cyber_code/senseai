import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { State, process } from '@progress/kendo-data-query';
import { DesignService } from 'src/app/design/services/design.service';
import { SessionService } from '../../services/session.service';
import { Guid } from 'src/app/shared/guid';
import * as _ from 'lodash';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-systems',
  templateUrl: 'systems.component.html',
  styleUrls: ['systems.component.css']
})
export class SystemsComponent implements OnInit {
  @Input() gridData;
  @Input() state;
  @Input() systems;

  @Output() systemDataStateChanged = new EventEmitter<any>();
  @Output() systemListChanged = new EventEmitter<any>();

  public selectedSystem = {
    title: '',
    adapterName: ''
  };

  public systemWindowOpen = false;
  public deleteSystemWindowOpen = false;
  public isSystemEdited = false;

  constructor(private permissionService: PermissionService) {}

  ngOnInit() {}

  dataStateChange(state) {
    this.systemDataStateChanged.emit(state);
  }

  openSystemWindow(catalog?) {
    this.systemWindowOpen = true;
    if (catalog) {
      this.isSystemEdited = true;
      this.selectedSystem = _.cloneDeep(catalog);
    } else {
      this.resetSystem();
    }
  }

  close() {
    this.systemWindowOpen = false;
    this.deleteSystemWindowOpen = false;
    this.resetSystem();
  }

  resetSystem() {
    this.isSystemEdited = false;
    this.selectedSystem = {
      title: '',
      adapterName: ''
    };
  }

  updateSystemList(system) {
    let systems = [];
    if (this.isSystemEdited) {
      systems = this.systems.map(sys => {
        if (sys.id === system.id) {
          return system;
        }
        return sys;
      });
    } else {
      systems = this.systems.concat(system);
    }
    this.systemListChanged.emit(systems);
    this.close();
  }

  removeSystemFromList(system) {
    let systems = this.systems.filter(sys => sys.id !== system.id);
    this.systemListChanged.emit(systems);
    this.close();
  }

  openDeleteSystemWindow(system) {
    this.deleteSystemWindowOpen = true;
    this.selectedSystem = system;
  }
}
