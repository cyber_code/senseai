import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Route, Router, RouterLink} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {first} from 'rxjs/operators';
import {User} from '../../models/user.model';
import {SessionService} from '../../services/session.service';
import {Location} from '@angular/common';
import { BuilderService } from 'src/app/product/services/builder.service';

@Component({
  selector: 'app-change-profile',
  templateUrl: './change-profile.component.html',
  styleUrls: ['./change-profile.component.css']
})
export class ChangeProfileComponent implements OnInit {
  currentUser: User = new User();
  @Output() 
  public userProfileClosed = new EventEmitter();
  @Output() 
  public changePasswordClicked = new EventEmitter();
  @Output()
  public changeUsernameClicked = new EventEmitter();
  @Output()
  public changeEmailClicked = new EventEmitter();

  constructor(
    private router: Router,
    private location: Location,
    private sessionService: SessionService,
    private authenticationService: AuthenticationService,
    private builderService: BuilderService
  ) {
    this.authenticationService.authenticationChallange$.subscribe(isAuthenticated => {
      if (isAuthenticated) {
        this.currentUser = this.sessionService.getUser();
      }
    });
  }

  ngOnInit() {
  }

  back() {
    this.userProfileClosed.emit();
  }

  info() {
  }

  onPassword() {
    this.changePasswordClicked.emit();
  }

  onUser() {
    this.changeUsernameClicked.emit();
  }

  onEmail() {
    this.changeEmailClicked.emit();
  }

  logOut() {
    this.builderService.removeBotSession();
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.back();
  }
}
