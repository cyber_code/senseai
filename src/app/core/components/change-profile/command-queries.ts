import {ApiAction} from './../../services/command-queries/api-action';

export class ChangePassword extends ApiAction {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;

  constructor(
    oldPassword: string,
    newPassword: string,
    confirmPassword: string
  ) {
    super('ChangePassword');
    this.oldPassword = oldPassword;
    this.newPassword = newPassword;
    this.confirmPassword = confirmPassword;
  }
}

export class ChangeEmail extends ApiAction {
  newEmail: string;
  confirmEmail: string;

  constructor(newEmail: string, confirmEmail: string) {
    super('ChangeEmail');
    this.newEmail = newEmail;
    this.confirmEmail = confirmEmail;
  }
}

export class ChangeProfileName extends ApiAction {
  newProfileName: string;

  constructor(newProfileName: string) {
    super('ChangeProfileName');
    this.newProfileName = newProfileName;
  }
}

