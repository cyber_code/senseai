import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Router} from '@angular/router';
import {SessionService} from '../../../services/session.service';
import {ChangeProfileService} from '../change-profile.service';
import {User} from 'src/app/core/models/user.model';
import {AuthenticationService} from 'src/app/core/services/authentication.service';
import { BuilderService } from 'src/app/product/services/builder.service';

@Component({
  selector: 'app-change-user-name',
  templateUrl: './change-user-name.component.html',
  styleUrls: ['./change-user-name.component.css']
})
export class ChangeUserNameComponent implements OnInit {
  currentUser: User = new User();
  changeUserNameForm = {newProfileName: ''};

  @Output() 
  public redirectToChangeProfile = new EventEmitter();

  @Output() 
  public changePasswordClicked = new EventEmitter();

  @Output()
  public changeUsernameClosed = new EventEmitter();

  @Output()
  public changeEmailClicked = new EventEmitter();

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private authenticationService: AuthenticationService,
    public changeProfileService: ChangeProfileService,
    private builderService: BuilderService
  ) {
    this.authenticationService.authenticationChallange$.subscribe(isAuthenticated => {
      if (isAuthenticated) {
        this.currentUser = this.sessionService.getUser();
      }
    });
  }

  ngOnInit() {}

  back() {
    this.changeUsernameClosed.emit();
  }

  onEmail() {
    this.changeEmailClicked.emit();
  }

  logOut() {
    this.builderService.removeBotSession();
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.back();
  }

  onPassword() {
    this.changePasswordClicked.emit();
  }

  confirm() {
    this.changeProfileService.ChangeProfileName(this.changeUserNameForm.newProfileName).subscribe(result => {
      this.changeUserNameForm.newProfileName = '';
      this.builderService.removeBotSession();
      this.authenticationService.logout();
    });

  }
}
