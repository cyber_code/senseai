import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../services/authentication.service';
import {User} from '../../../models/user.model';
import {SessionService} from '../../../services/session.service';
import {ChangeProfileService} from '../change-profile.service';
import {MessageService} from 'src/app/core/services/message.service';
import {MessageType} from 'src/app/core/models/message.model';
import {FormControl, Validators} from '@angular/forms';
import {CustomValidators} from 'ng2-validation';
import { BuilderService } from 'src/app/product/services/builder.service';


@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.css']
})
export class ChangeEmailComponent implements OnInit {
  currentUser: User = new User();
  changeEmailForm = {newEmail: '', confirmEmail: ''};

  email = new FormControl('', CustomValidators.email);

  @Output()
  public changeEmailClosed = new EventEmitter();

  @Output()
  public changePasswordClicked = new EventEmitter();

  @Output()
  public changeUsernameClicked = new EventEmitter();

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private authenticationService: AuthenticationService,
    public changeProfileService: ChangeProfileService,
    private messageService: MessageService,
    private builderService: BuilderService
  ) {
    this.authenticationService.authenticationChallange$.subscribe(isAuthenticated => {
      if (isAuthenticated) {
        this.currentUser = this.sessionService.getUser();
      }
    });
  }


  ngOnInit() {
  }


  back() {
    this.changeEmailClosed.emit();
  }

  onPassword() {
    this.changePasswordClicked.emit();
  }

  onUser() {
    this.changeUsernameClicked.emit();
  }

  logout() {
    this.builderService.removeBotSession();
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.back();
  }

  confirm() {
    this.changeProfileService.ChangeEmail(this.changeEmailForm.newEmail, this.changeEmailForm.confirmEmail).subscribe(result => {
      if (result.error) {
        this.messageService.sendMessage({text: result.error, type: MessageType.Error});
      } else {
        this.authenticationService.getUerInfo().subscribe(user => {
          this.currentUser = user;
          this.sessionService.setUser(user);
        });

        this.messageService.sendMessage({text: result.Message, type: MessageType.Info});
      }
      this.resetForm();
    });
  }

  checkEmail() {
    this.changeEmailForm.confirmEmail = this.changeEmailForm.newEmail;
  }

  resetForm() {
    this.changeEmailForm.newEmail = '';
    this.changeEmailForm.confirmEmail = '';
  }
}
