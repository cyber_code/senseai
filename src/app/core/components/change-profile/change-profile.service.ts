import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {ConfigurationService} from '../../services/configuration.service';
import {Observable} from 'rxjs';
import {HttpExecutorService} from '../../services/http-executor.service';
import {CommandMethod} from 'src/app/shared/command-method';
import {ChangePassword, ChangeEmail, ChangeProfileName} from './command-queries';

@Injectable({
  providedIn: 'root'
})
export class ChangeProfileService {
  private identityUrl = '';
  public authenticationChallange: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public authenticationChallange$ = this.authenticationChallange.asObservable();

  constructor(private configurationService: ConfigurationService, private httpExecutor: HttpExecutorService) {
    this.identityUrl = `${configurationService.serverSettings.identityUrl}User/`;
  }

  ChangePassword(oldPassword: string, newPassword: string, confirmPassword: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.identityUrl,
      new ChangePassword(oldPassword, newPassword, confirmPassword),
      CommandMethod.POST
    );
  }

  ChangeEmail(newEmail: string, confirmEmail: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.identityUrl, new ChangeEmail(newEmail, confirmEmail), CommandMethod.POST);
  }

  ChangeProfileName(newProfileName: string): Observable<any> {
    return this.httpExecutor.executeCommand(this.identityUrl, new ChangeProfileName(newProfileName), CommandMethod.POST);
  }
}
