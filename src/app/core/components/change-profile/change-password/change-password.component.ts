import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Route, Router, RouterLink } from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import { User } from '../../../models/user.model';
import { SessionService } from '../../../services/session.service';
import { ChangeProfileService } from '../change-profile.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from './custom-validators';
import { BuilderService } from 'src/app/product/services/builder.service';
import { MessageType } from 'src/app/core/models/message.model';
import { MessageService } from 'src/app/core/services/message.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public currentUser: User = new User();
  public frmSignup: FormGroup;
  public isSubmitButtonDisabled = true;
  public oldPassword: string = '';

  @Output()
  public changePasswordClosed = new EventEmitter();
  @Output()
  public changeUsernameClicked = new EventEmitter();
  @Output()
  public changeEmailClicked = new EventEmitter();

  constructor(
    private router: Router,
    private sessionService: SessionService,
    private authenticationService: AuthenticationService,
    public changeProfileService: ChangeProfileService,
    private fb: FormBuilder,
    private builderService: BuilderService,
    private messageService: MessageService
  ) {
    this.authenticationService.authenticationChallange$.subscribe(
      isAuthenticated => {
        if (isAuthenticated) {
          this.currentUser = this.sessionService.getUser();
        }
      }
    );

    this.frmSignup = this.createSignupForm();
  }

  ngOnInit() {}

  createSignupForm(): FormGroup {
    return this.fb.group(
      {
        password: [
          '',
          Validators.compose([
            Validators.required,
            CustomValidators.patternValidator(/\d/, {
              hasNumber: true
            }),
            CustomValidators.patternValidator(/[A-Z]/, {
              hasCapitalCase: true
            }),
            CustomValidators.patternValidator(/[a-z]/, {
              hasSmallCase: true
            }),
            CustomValidators.patternValidator(
              /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
              {
                hasSpecialCharacters: true
              }
            ),
            Validators.minLength(8)
          ])
        ],
        confirmPassword: ['', Validators.compose([Validators.required])]
      },
      {
        validator: CustomValidators.passwordMatchValidator
      }
    );
  }

  back() {
    this.changePasswordClosed.emit();
  }

  onEmail() {
    this.changeEmailClicked.emit();
  }

  onUser() {
    this.changeUsernameClicked.emit();
  }

  logOut() {
    this.builderService.removeBotSession();
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    this.back();
  }

  confirm() {
    this.changeProfileService
      .ChangePassword(
        this.oldPassword,
        this.frmSignup.controls['password'].value,
        this.frmSignup.controls['confirmPassword'].value
      )
      .subscribe(result => {
        this.messageService.sendMessage({
          text: result.message,
          type: MessageType.Success
        });
        this.resetForm();
      });
  }

  resetForm() {
    this.oldPassword = '';
    this.frmSignup.reset();
  }

  isSubmitBtnDisabled() {
    return (
      !this.oldPassword ||
      this.oldPassword.trim() === '' ||
      this.frmSignup.controls['password'].invalid ||
      this.frmSignup.controls['confirmPassword'].invalid
    );
  }
}
