import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-attributes',
  templateUrl: './attributes.component.html',
  styleUrls: ['./attributes.component.css']
})
export class AttributesComponent implements OnInit {

  @Input() gridData;
  @Input() state;
  @Input() typicals;

  @Output() typicalDataStateChanged = new EventEmitter<any>();
  @Output() typicalListChanged = new EventEmitter<any>();

  public selectedTypical = {
    title: ''
  };

  public typicalWindowOpen = false;
  public deleteTypicalWindowOpen = false;
  public isTypicalEdited = false;

  constructor() {}

  ngOnInit() {}

  dataStateChange(state) {
    this.typicalDataStateChanged.emit(state);
  }

  openTypicalWindow(catalog?) {
    this.typicalWindowOpen = true;
    if (catalog) {
      this.isTypicalEdited = true;
      this.selectedTypical = _.cloneDeep(catalog);
    } else {
      this.resetTypical();
    }
  }

  close() {
    this.typicalWindowOpen = false;
    this.deleteTypicalWindowOpen = false;
    this.resetTypical();
  }

  resetTypical() {
    this.isTypicalEdited = false;
    this.selectedTypical = {
      title: ''
    };
  }

  updateTypicalList(system) {
    let typicals = [];
    if (this.isTypicalEdited) {
      typicals = this.typicals.map(sys => {
        if (sys.id === system.id) {
          return system;
        }
        return sys;
      });
    } else {
      typicals = this.typicals.concat(system);
    }
    this.typicalListChanged.emit(typicals);
    this.close();
  }

  removeSystemFromList(system) {
    let typicals = this.typicals.filter(sys => sys.id !== system.id);
    this.typicalListChanged.emit(typicals);
    this.close();
  }

  openDeleteTypicalWindow(system) {
    this.deleteTypicalWindowOpen = true;
    this.selectedTypical = system;
  }

}
