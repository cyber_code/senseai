import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { DesignService } from "src/app/design/services/design.service";
import { SessionService } from "../../services/session.service";
import { MessageType } from "../../models/message.model";
import { MessageService } from "src/app/core/services/message.service";

@Component({
  selector: "app-add-edit-catalog",
  templateUrl: "add-edit-catalog.component.html",
  styleUrls: ["add-edit-catalog.component.css"]
})
export class AddEditCatalogComponent implements OnInit {
  @Input() isCatalogEdited;
  @Input() selectedCatalog;
  @Input() catalogWindowOpen;

  @Output() public closeCatalogWindow = new EventEmitter<any>();
  @Output() public catalogListUpdated = new EventEmitter<boolean>();

  public catalogLevels = ["Tenant Level", "Project Level", "SubProject Level"];

  constructor(
    private designService: DesignService,
    private sessionService: SessionService,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  close() {
    this.closeCatalogWindow.emit();
  }

  changeTitle(event) {
    let title = event.target.value.trim("");
    this.selectedCatalog.title = title;
  }

  changeDescription(event) {
    let description = event.target.value.trim("");
    this.selectedCatalog.description = description;
  }

  changeCatalogLevel(catalogLevel) {
    this.selectedCatalog.level = catalogLevel;
  }

  disableSubmitBtn() {
    return (
      (!this.isCatalogEdited &&
        (!this.selectedCatalog.level || this.selectedCatalog.level === "")) ||
      this.selectedCatalog.title === ""
    );
  }

  submitClicked() {
    if (this.isCatalogEdited) {
      this.editCatalog();
    } else {
      this.addCatalog();
    }
  }

  addCatalog() {
    if (this.selectedCatalog.level === "Tenant Level") {
      this.designService
        .addTenantCatalog(
          this.selectedCatalog.title,
          this.selectedCatalog.description,
          "0"
        )
        .subscribe(res => {
          this.handleCatalogResponse(res);
        });
    } else if (this.selectedCatalog.level === "Project Level") {
      let project = this.sessionService.getProject();
      this.designService
        .addProjectCatalog(
          project.id,
          this.selectedCatalog.title,
          this.selectedCatalog.description,
          "0"
        )
        .subscribe(res => {
          this.handleCatalogResponse(res);
        });
    } else {
      let subProject = this.sessionService.getSubProject();
      let project = this.sessionService.getProject();
      this.designService
        .addSubProjectCatalog(
          project.id,
          subProject.id,
          this.selectedCatalog.title,
          this.selectedCatalog.description,
          "0"
        )
        .subscribe(res => {
          this.handleCatalogResponse(res);
        });
    }
  }

  editCatalog() {
    this.designService
      .updateCatalog(
        this.selectedCatalog.id,
        this.selectedCatalog.title,
        this.selectedCatalog.description,
        this.selectedCatalog.type
      )
      .subscribe(res => {
        this.handleCatalogResponse(res);
      });
  }

  handleCatalogResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isCatalogEdited
          ? "Catalog was updated successfully!"
          : "Catalog was added successfully",
        type: MessageType.Success
      });
      this.catalogListUpdated.emit(
        this.isCatalogEdited
          ? this.selectedCatalog
          : { ...this.selectedCatalog, id: res.id }
      );
    } else {
      this.close();
    }
  }
}
