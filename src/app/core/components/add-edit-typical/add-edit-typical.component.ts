import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DesignService } from '../../../design/services/design.service';
import { MessageService } from '../../services/message.service';
import { MessageType } from '../../models/message.model';
import { TypicalType } from '../../models/typical-type.model';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'app-add-edit-typical',
  templateUrl: './add-edit-typical.component.html',
  styleUrls: ['./add-edit-typical.component.css']
})
export class AddEditTypicalComponent implements OnInit {
  @Input() isTypicalEdited;
  @Input() selectedTypical;
  @Input() typicalWindowOpen;
  @Input() selectedCatalog;
  @Input() selectedCatalogFromDropDown;

  @Output() public closeTypicalWindow = new EventEmitter<any>();
  @Output() public typicalListUpdated = new EventEmitter<boolean>();

  public typicalTypes = [{ title: 'Application', type: TypicalType.Application },
  { title: 'Version', type: TypicalType.Version },
  { title: 'AA Application', type: TypicalType.AAAplication },
  { title: 'Enquiry', type: TypicalType.Enquiry },
  { title: 'Project Resourcing', type: TypicalType.ProjectResourcing }];

  public selectedTypicalType;

  workContext: any;
  catalogList: any[] = [];

  constructor(private designService: DesignService, private messageService: MessageService, private sessionService: SessionService) { }

  ngOnInit() {
    this.workContext = this.sessionService.getWorkContext();
    this.getCatalogs();
  }

  close() {
    this.closeTypicalWindow.emit();
    this.selectedTypicalType = null;
    this.selectedCatalog = null;
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedTypical.title = title;
  }

  disableSubmitBtn() {
    return (
      (!this.selectedTypicalType &&
        !this.isTypicalEdited)
      ||
      this.selectedTypical.title === '' 
    );
  }

  submitClicked() {
    if (this.isTypicalEdited) {
      this.editTypicalManually();
    }
    else {
      if (this.selectedCatalog != undefined) {
        this.addTypicalManually();
       this.selectedCatalog = null;
       this.selectedCatalogFromDropDown = null;
      }
      else{
        this.selectedCatalog=null;
        this.addTypicalWithselectedCatalogFromDropDown()
      }
    }
  }

  addTypicalManually() {
    this.designService
      .addTypicalManually(
        this.workContext.project.id,
        this.workContext.subProject.id,
        this.workContext.defaultSettings.system.id,
        this.selectedCatalog.id,
        this.selectedTypical.title,
        this.selectedTypical.type,
        this.workContext.user.tenantId
      )
      .subscribe(res => {
        this.handleSystemResponse(res);
      });
  }

  addTypicalWithselectedCatalogFromDropDown() {
    this.designService
      .addTypicalManually(
        this.workContext.project.id,
        this.workContext.subProject.id,
        this.workContext.defaultSettings.system.id,
        this.selectedCatalogFromDropDown.id,
        this.selectedTypical.title,
        this.selectedTypical.type,
        this.workContext.user.tenantId
      )
      .subscribe(res => {
        this.handleSystemResponse(res);
      });
  }

  editTypicalManually() {
    this.designService.updateTypicalManually(this.selectedTypical.id, this.selectedTypical.title).subscribe(res => {
      this.handleSystemResponse(res);
    });
  }

  handleSystemResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isTypicalEdited ? 'Typical was updated successfully!' : 'Typical was added successfully!',
        type: MessageType.Success
      });
      this.typicalListUpdated.emit(this.isTypicalEdited ? this.selectedTypical : { ...this.selectedTypical, id: res.id });
      this.selectedTypicalType = null;
    } else {
      this.close();
    }
  }

  changeTypicalType(typicalType) {
    this.selectedTypicalType = typicalType;
    this.selectedTypical.type = typicalType ? typicalType.type : null;
  }

  getCatalogs() {
    const subProject = this.sessionService.getSubProject();
    const project = this.sessionService.getProject();
    return this.designService.getTenantCatalogs(project.id, subProject.id).subscribe(response => {
      this.catalogList = response;
    });
  }

  changeCatalog(catalog) {
    this.selectedCatalog = catalog;
    this.selectedCatalogFromDropDown = catalog;
  }
}
