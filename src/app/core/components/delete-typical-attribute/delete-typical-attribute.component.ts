import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DesignService} from '../../../design/services/design.service';
import {MessageService} from '../../services/message.service';
import {MessageType} from '../../models/message.model';

@Component({
  selector: 'app-delete-typical-attribute',
  templateUrl: './delete-typical-attribute.component.html',
  styleUrls: ['./delete-typical-attribute.component.css']
})
export class DeleteTypicalAttributeComponent implements OnInit {
  @Input() deleteTypicalAttributeWindowOpen;
  @Input() selectedTypicalAttribute;
  @Input() typicalId;
;
  @Output() public removeTypicalAttributeFromList = new EventEmitter();
  @Output() public closeAttributeWindow = new EventEmitter();

  constructor(
    private designService: DesignService,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  deleteTypicalAttribute() {
    this.designService.deleteTypicalAttributeManually(this.selectedTypicalAttribute.name, this.typicalId, false).subscribe(res => {
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({text: 'Attribute ' + this.selectedTypicalAttribute.name +  ' was deleted successfully!', type: MessageType.Success});
      this.removeTypicalAttributeFromList.emit(this.selectedTypicalAttribute);
    } else {
      this.close();
    }
  }

  close() {
    this.closeAttributeWindow.emit();
  }
}
