import { isEmpty } from "rxjs/operators";
import { DesignService } from "./../../../design/services/design.service";
import { SessionService } from "src/app/core/services/session.service";
import { Project } from "../../models/project.model";
import { ConfigurationService } from "../../services/configuration.service";
import { Router } from "@angular/router";
import { AuthenticationService } from "../../services/authentication.service";
import {
  AfterViewInit,
  Component,
  OnInit,
  ViewChild,
  QueryList,
  ViewChildren,
  ElementRef
} from "@angular/core";
import { ProjectSelectionService } from "src/app/core/components/project-selection/project-selection.service";
import { isNullOrUndefined } from "util";
import { SubProject } from "../../models/sub-project.model";
import { AdministrationService } from "src/app/core/services/administration.service";
import { SubProjectChanged } from "../../models/sub-project-changed.model";
import { Guid } from "src/app/shared/guid";
import { BuilderService } from "src/app/product/services/builder.service";
import { HelperService } from "../../../shared/services/helper.service";
import { LicenseService } from "../../services/licenses.service";
import { PermissionService } from "../../services/permission.service";

@Component({
  selector: "app-project-selection",
  templateUrl: "./project-selection.component.html",
  styleUrls: ["./project-selection.component.css"]
})
export class ProjectSelectionComponent implements OnInit, AfterViewInit {
  public projects: Project[];
  public subProjects: SubProject[];

  public selectedProject?: Project;
  public selectedSubProject?: SubProject;
  public isPopupOpen: Boolean;
  public isDialogOpen: Boolean;
  public inProcessModel: InProcessModel;
  public display: String = "none";
  // @ViewChild('AddProject') AddProject;
  @ViewChildren("checkPermission") checkPermission: QueryList<any>;

  constructor(
    public router: Router,
    private authenticationService: AuthenticationService,
    public projectSelectionService: ProjectSelectionService,
    private administrationService: AdministrationService,
    private session: SessionService,
    private builderService: BuilderService,
    private _helper: HelperService,
    private licenseService: LicenseService,
    private permissionService: PermissionService
  ) {
    this.inProcessModel = new InProcessModel();
    this.isPopupOpen = false;
    this.isDialogOpen = false;
    this.projects = [];
    this.subProjects = [];
  }

  ngOnInit() {
    this.loadData();
  }

  ngAfterViewInit() {}

  public loadData() {
    if (this.permissionService.hasPermission("/api/administration/GetProjects"))
      this.administrationService.getProjects().subscribe(result => {
        this.projects = result;
        this.selectedProject = this.session.getProject();
        this.loadSubProjects();
      });
    
  }

  selectProject(projectId: string) {
    if (!this.selectedProject || this.selectedProject.id !== projectId) {
      this.selectedProject = this.projects.find(p => p.id === projectId);
      this.loadSubProjects();
    }
  }

  loadSubProjects() {
    if (!this.selectedProject) {
      this.subProjects = [];
    } else {
      if (this.permissionService.hasPermission("/api/Administration/GetSubProject"))
      this.administrationService
        .getSubProjects(this.selectedProject.id)
        .subscribe(result => {
          this.subProjects = result;
          this.selectedSubProject = this.session.getSubProject();
        });
    }
  }

  selectSubProject(subProjectId: string) {
    this.selectedSubProject = this.subProjects.find(
      sp => sp.id === subProjectId
    );
  }

  deleteProject() {
    if (this.selectedProject == null) {
      return;
    } else {
      this.administrationService
        .deleteProject(this.selectedProject.id)
        .subscribe(x => {
          this.projects = this.projects.filter(
            p => p.id !== this.selectedProject.id
          );
          this.subProjects = [];
          if (
            this.session.getProject() &&
            this.session.getProject().id === this.selectedProject.id
          ) {
            this.clearProjectSelection();
            this.clearSubProjectSelection();
          }
          this.close();
        });
    }
  }

  deleteSubProject() {
    this.administrationService
      .deleteSubProject(this.selectedSubProject.id)
      .subscribe(x => {
        this.subProjects = this.subProjects.filter(
          sp => sp.id !== this.selectedSubProject.id
        );
        this.clearSubProjectSelection();
        this.close();
      });
  }

  createProject(title: string) {
    this.administrationService
      .createProject({ title: title } as Project)
      .subscribe(x => {
        this.projects.push(x);
        this.close();
      });
  }

  createSubProject(title: string) {
    this.administrationService
      .createSubProject({
        projectId: this.selectedProject.id,
        title: title
      } as SubProject)
      .subscribe(x => {
        this.subProjects.push(x);
        this.close();
      });
  }

  editProject(text: string): any {
    this.administrationService
      .editProject({ id: this.selectedProject.id, title: text } as Project)
      .subscribe(x => {
        this.selectedProject.title = text;
        this.close();
      });
  }

  editSubProject(text: string): any {
    this.administrationService
      .editSubProject({
        id: this.selectedSubProject.id,
        title: text,
        description: "",
        projectId: this.selectedProject.id
      } as SubProject)
      .subscribe(x => {
        this.selectedSubProject.title = text;
        this.close();
      });
  }

  clearProjectSelection() {
    this.selectedProject = {} as Project;
    this.session.setProject(this.selectedProject);
  }

  clearSubProjectSelection() {
    this.selectedSubProject = {} as SubProject;
    this.session.setSubProject(this.selectedSubProject);
  }

  open(entityType: string, action: string) {
    if (this.isPopupOpen || this.isDialogOpen) {
      return;
    }
    this.inProcessModel.actionType = action;
    this.inProcessModel.entityType = entityType;
    this.inProcessModel.entityTypeId = this.camelize(entityType);

    if (action === "create") {
      this.inProcessModel.popupTitle = `Create new  ${entityType}`;
      this.inProcessModel.text = "";
      this.isPopupOpen = true;
    } else if (action === "edit") {
      this.inProcessModel.popupTitle = `Edit ${entityType}`;
      if (entityType === "project") {
        this.isPopupOpen = !isNullOrUndefined(this.selectedProject);
        this.inProcessModel.text =
          this.isPopupOpen && this.selectedProject.title;
      } else if (entityType === "Sub project") {
        this.isPopupOpen = !isNullOrUndefined(this.selectedSubProject);
        this.inProcessModel.text =
          this.isPopupOpen && this.selectedSubProject.title;
      }
    } else if (action === "delete") {
      if (entityType === "project") {
        this.isDialogOpen =
          !isNullOrUndefined(this.selectedProject) &&
          !Guid.isEmpty(this.selectedProject.id);
        this.inProcessModel.popupTitle =
          this.isDialogOpen &&
          `Are you sure you want to delete project : ${this.selectedProject.title} and its sub projects`;
      } else if (entityType === "Sub project") {
        this.isDialogOpen =
          !isNullOrUndefined(this.selectedSubProject) &&
          !Guid.isEmpty(this.selectedSubProject.id);
        this.inProcessModel.popupTitle =
          this.isDialogOpen &&
          `Are you sure you want to delete sub project : ${this.selectedSubProject.title}`;
      }
    }
  }

  submit() {
    if (this.inProcessModel.entityType === "project") {
      if (this.inProcessModel.actionType === "create") {
        this.createProject(this.inProcessModel.text);
      } else if (this.inProcessModel.actionType === "edit") {
        this.editProject(this.inProcessModel.text);
      }
    } else if (this.inProcessModel.entityType === "Sub project") {
      if (this.inProcessModel.actionType === "create") {
        this.createSubProject(this.inProcessModel.text);
      } else if (this.inProcessModel.actionType === "edit") {
        this.editSubProject(this.inProcessModel.text);
      }
    }
  }

  delete(action: string) {
    if (action === "cancel") {
      this.isDialogOpen = false;
    } else if (action === "no") {
      this.isDialogOpen = false;
    } else if (action === "yes") {
      if (this.inProcessModel.entityType === "project") {
        this.deleteProject();
      } else if (this.inProcessModel.entityType === "Sub project") {
        this.deleteSubProject();
      }
    }
  }

  close() {
    this.inProcessModel = new InProcessModel();
    this.isPopupOpen = false;
    this.isDialogOpen = false;
  }

  okClicked() {
    if (
      this.selectedSubProject &&
      !Guid.isEmpty(this.selectedSubProject.id) &&
      this.selectedProject.id === this.selectedSubProject.projectId
    ) {
      this.projectSelectionService.changeSelection(
        new SubProjectChanged(this.selectedProject, this.selectedSubProject)
      );
      this.licenseService.redirectToModuleByLicense();
    }
  }

  closeClicked() {
    const workContext = this.session.getWorkContext();
    if (
      workContext.project &&
      workContext.project.id &&
      workContext.subProject &&
      workContext.subProject.id
    ) {
      this.projectSelectionService.changeSelection(
        new SubProjectChanged(workContext.project, workContext.subProject)
      );
      this.licenseService.redirectToModuleByLicense();
    } else {
      this.builderService.removeBotSession();
      this.authenticationService.logout();
      this.router.navigate(["/login"]);
    }
  }

  info() {
    this.display = "block";
  }

  camelize(str) {
    return str
      .replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
        return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
      })
      .replace(/\s+/g, "");
  }
}

class InProcessModel {
  actionType: string;
  text: string;
  entityType: string;
  popupTitle: string;
  entityTypeId: string;
}
