import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { SessionService } from '../../services/session.service';
import { SubProjectChanged } from '../../models/sub-project-changed.model';

@Injectable({ providedIn: 'root' })
export class ProjectSelectionService {
  private subProjectSelected = new BehaviorSubject<SubProjectChanged>(
    new SubProjectChanged(null, null)
  );
  public subProjectSelected$ = this.subProjectSelected.asObservable();

  constructor(private sessionService: SessionService) {
    this.subProjectSelected$.subscribe(x => {
      if (x.hasValue) {
        this.sessionService.setProject(x.selectedProject);
        this.sessionService.setSubProject(x.selectedSubProject);
      }
    });
  }

  load() {
    const context = this.sessionService.getWorkContext();
    this.changeSelection(
      new SubProjectChanged(context.project, context.subProject)
    );
  }

  public changeSelection(subProjectChanged: SubProjectChanged) {
    this.subProjectSelected.next(subProjectChanged);
  }
}
