import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {
  map,
  debounceTime,
  switchMap,
  distinctUntilChanged
} from 'rxjs/operators';
import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  AfterViewInit,
  Input,
  ViewChildren,
  QueryList
} from '@angular/core';
import {of} from 'rxjs/observable/of';

import {isNullOrUndefined} from 'util';

import {NodeType} from 'src/app/shared/node-type';
import {Workflow} from 'src/app/design/models/workflow.model';
import {DesignService} from 'src/app/design/services/design.service';
import {TreeViewItem} from 'src/app/shared/tree-view-item';
import {SubProject} from 'src/app/core/models/sub-project.model';
import {ProjectSelectionService} from 'src/app/core/components/project-selection/project-selection.service';
import {SessionService} from 'src/app/core/services/session.service';
import {Guid} from 'src/app/shared/guid';
import {HelperService} from '../../../shared/services/helper.service';
import { ActivatedRoute } from '@angular/router';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: ' app-tree-view',
  templateUrl: './app-tree-view.component.html',
  styleUrls: ['./app-tree-view.component.css']
})
export class AppTreViewComponent implements OnInit, AfterViewInit {
  private data: TreeViewItem[] = [];
  private _subProject: SubProject;

  public filterdData: TreeViewItem[] = [];
  public selectedKeys: String[] = [];
  public expandedKeys: String[] = [];
  public disabledKeys: String[] = [];
  public searchTerm: String = '';
  public draggedItem: string;
  public editModeState = false;
  private _helper: HelperService;
  @ViewChildren('checkPermission') checkPermission: QueryList<any>;
  public selectedTreeViewItemInput: any;
  public selectedItem: TreeViewItem;
  @Output() public treeViewItemSelected = new EventEmitter<TreeViewItem>();
  @Output() public workflowToLoad = new EventEmitter();
  @Input() isIssued: boolean;
  @Input() generatedTestCases: boolean;
  private newSearchTermTyped = new BehaviorSubject<string>('');

  private firstLoad: Boolean = true;

  constructor(
    private designService: DesignService,
    private projectSelectionService: ProjectSelectionService,
    private session: SessionService,
    private activatedRoute: ActivatedRoute,
    public permissionService: PermissionService
  ) {
    this.newSearchTermTyped
      .pipe(
        // wait 300ms after each keystroke before considering the term
        debounceTime(300),
        // ignore new term if same as previous term
        distinctUntilChanged(),
        // switch to new search observable each time the term changes
        switchMap((term: string) =>
          term
            ? this.designService.searchWorkflows(term, this.getSubProject().id)
            : of([])
        ),
        // map workflow to TreeViewItem
        map((workflows: Workflow[]) =>
          workflows.map(
            item => new TreeViewItem(item.id, item.title, NodeType.Workflow, [])
          )
        )
      )
      .subscribe(result => {
        if (this.firstLoad) {
          this.firstLoad = false;
          return;
        }
        const currentValue = this.newSearchTermTyped.getValue();
        if (currentValue === '') {
          this.getAllFolders();
        } else {
          this.populateDataSource(result);
          this.expandRootFolder();
        }
      });
  }

  private getAllFolders() {
    const subProject = this.getSubProject();
    if (isNullOrUndefined(subProject) || Guid.isEmpty(subProject.id)) {
      return;
    }
    this.designService.getAllFolders(subProject.id).subscribe(result => {
      this.data = result.map(
        item => new TreeViewItem(item.id, item.title, NodeType.Folder, [])
      );
      this.filterdData = [
        new TreeViewItem(
          subProject.id,
          subProject.title,
          NodeType.Root,
          this.data
        )
      ];
      if (this.selectedTreeViewItemInput && this.selectedTreeViewItemInput.folderId) {
        this.expandedKeys = [this.getSubProject().id, this.selectedTreeViewItemInput.folderId];
        this.selectedItem = this.selectedTreeViewItemInput;
        this.treeViewItemSelected.emit(this.selectedItem);
        this.selectedKeys = [this.selectedTreeViewItemInput.id];
      } else {
        this.expandRootFolder();
      }
    });
  }

  ngAfterViewInit() {}
    

  ngOnInit() {
    // this will be call the subscriber because it's the first time
    this.activatedRoute.queryParams.subscribe(params => {
      if (params && params.id) {
        this.designService.getWf(params.id).subscribe(result => {
          const treeViewParams = {
            id: params.id,
            title: '',
            type: 2,
            items: []
          };
          this.selectedTreeViewItemInput = {
            folderId: result.folderId,
            id: params.id,
            title: '',
            type: 2,
            items: []
          };
          this.loadFolders();
          this.workflowToLoad.emit(treeViewParams);
        });
      }else {
        this.loadFolders();
      }
    });
    
  }

  loadFolders() {
    this.projectSelectionService.subProjectSelected$.subscribe(p => {
      if (p.hasValue) {
        this._subProject = p.selectedSubProject;
        this.searchTerm = '';
        this.getAllFolders();
      }
    });
  }



  onDragStart(event) {
    this.draggedItem = event.target.id;
  }

  public onkeyup(value: string): void {
    this.newSearchTermTyped.next(value);
  }

  public fetchChildren = (dataItem: TreeViewItem) =>{
    // tslint:disable-next-line:semicolon
    return dataItem.type === NodeType.Root
      ? of(this.filterdData[0].items)
      : this.designService.fetchWorkflows(dataItem.id, this.isIssued, this.generatedTestCases);
  }
    

  // store children in component data source
  public childrenLoaded($event: any) {
    if ($event !== null && $event.item.dataItem.type !== NodeType.Root) {
      const folder = this.data.find(x => x.id === $event.item.dataItem.id);
      folder.items = $event.children.map(child => child.dataItem);
    }
  }

  reloadFolderItems(dataItem: TreeViewItem) {
    this.designService.fetchWorkflows(dataItem.id, this.isIssued, this.generatedTestCases).subscribe(workflows => {
      if (workflows) {
        this.selectedItem.items = workflows;
        this.populateDataSource(this.data);
      }
    });
  }

  public hasChildren = (dataitem: any): boolean =>
    dataitem.type !== NodeType.Workflow;

  public iconClass(data: any): any {
    if (data.type === NodeType.Workflow) {
      return 'k-icon k-i-share';
    } else if (data.type === NodeType.Folder) {
      return 'k-icon k-i-folder';
    } else {
      return 'k-icon k-i-folder-more';
    }
  }

  public disable(disable: boolean) {
    this.editModeState = disable;
  }

  public handleSelection($event: any): void {
    if (this.editModeState === true) {
      return;
    }
    this.selectedItem = $event.dataItem;
    this.treeViewItemSelected.emit(this.selectedItem);
  }

  public addTreeViewElement(treeViewItem: TreeViewItem): void {
    if (!this.selectedItem.items) {
      this.selectedItem.items = [];
    }
    let cutCopy = treeViewItem.cutCopy;
    this.selectedItem.items.push({...treeViewItem, cutCopy: false});
    this.populateDataSource(this.data);
    if (treeViewItem.type === NodeType.Workflow) {
      this.selectedItem = {...treeViewItem, cutCopy: false};
      this.treeViewItemSelected.emit({...this.selectedItem, cutCopy: cutCopy});
      this.selectedKeys = [this.selectedItem.id];
    }
  }

  public cutTreeViewElement(treeViewItem: TreeViewItem) {
    const newData = this.data.filter(item => item.id !== treeViewItem.id);
    this.data = newData;
    this.populateDataSource(newData);
  }

  public deleteTreeViewElement(treeViewItem: TreeViewItem): void {
    const newData = this.data.filter(item => item.id !== treeViewItem.id);
    this.data = newData;
    this.populateDataSource(newData);
  }

  public renameTreeViewElement(name: string): void {
    this.selectedItem.title = name;
    let newData = this.data.map(item => {
      if (item.id === this.selectedItem.id) {
        return {...this.selectedItem, title: name};
      }
      return item;
    });
    this.populateDataSource(newData);
    this.treeViewItemSelected.emit(this.selectedItem);
  }

  public selectWfItemAndExpand(workflowItemId: string) {
    this.selectedKeys = [workflowItemId];
  }

  private findParent(id: string): TreeViewItem {
    for (const folder of this.data) {
      if (folder.items === null) {
        continue;
      }
      for (const wfItem of folder.items) {
        if (wfItem.id === id) {
          return folder;
        }
      }
    }
    return null;
  }

  //#region  private methods
  private expandRootFolder() {
    this.expandedKeys = [this.getSubProject().id];
    this.selectedItem = this.filterdData.length > 0 && this.filterdData[0];
    this.treeViewItemSelected.emit(this.selectedItem);
  }

  private populateDataSource(items: TreeViewItem[]) {
    this.filterdData = [
      new TreeViewItem(
        this.getSubProject().id,
        this.getSubProject().title,
        NodeType.Root,
        items
      )
    ];
  }

  private getSubProject(): SubProject {
    if (isNullOrUndefined(this._subProject)) {
      this._subProject = this.session.getWorkContext().subProject;
    }
    return this._subProject;
  }
}
