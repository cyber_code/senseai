import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { MessageType } from '../../models/message.model';
import { MessageService } from 'src/app/core/services/message.service';
import { Adapter } from 'src/app/design/models/adapter.model';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-add-edit-system',
  templateUrl: 'add-edit-system.component.html',
  styleUrls: ['add-edit-system.component.css']
})
export class AddEditSystemComponent implements OnInit {
  @Input() isSystemEdited;
  @Input() selectedSystem;
  @Input() systemWindowOpen;

  @Output() public closeSystemWindow = new EventEmitter<any>();
  @Output() public systemListUpdated = new EventEmitter<boolean>();

  adapters: Adapter[] = [];

  constructor(
    private designService: DesignService,
    private messageService: MessageService,
    private permissionService: PermissionService
  ) {
  }

  ngOnInit() {
    if (this.permissionService.hasPermission("/api/Administration/GetAdapters"))
      this.designService.getAdapters().subscribe((result: any) => {
        result = result ? result.map(x => x.description) : [];
        this.adapters = result;
      });
  }

  close() {
    this.closeSystemWindow.emit();
  }

  changeTitle(event) {
    let title = event.target.value.trim('');
    this.selectedSystem.title = title;
  }

  changeAdapterName(event) {
    let adapterName = event;
    this.selectedSystem.adapterName = adapterName;
  }

  get disableSubmitBtn() {
    return (
      this.selectedSystem.adapterName === '' || this.selectedSystem.title === ''
    );
  }

  submitClicked() {
    if (this.isSystemEdited) {
      this.editSystem();
    } else {
      this.addSystem();
    }
  }

  addSystem() {
    this.designService
      .addSystem(this.selectedSystem.title, this.selectedSystem.adapterName)
      .subscribe(res => {
        this.handleSystemResponse(res);
      });
  }

  editSystem() {
    this.designService
      .updateSystem(
        this.selectedSystem.id,
        this.selectedSystem.title,
        this.selectedSystem.adapterName
      )
      .subscribe(res => {
        this.handleSystemResponse(res);
      });
  }

  handleSystemResponse(res) {
    if (res) {
      this.messageService.sendMessage({
        text: this.isSystemEdited
          ? 'System was updated successfully!'
          : 'System was added successfully!',
        type: MessageType.Success
      });
      this.systemListUpdated.emit(
        this.isSystemEdited
          ? this.selectedSystem
          : { ...this.selectedSystem, id: res.id }
      );
    } else {
      this.close();
    }
  }
}
