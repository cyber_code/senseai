import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DesignService} from '../../../design/services/design.service';
import {MessageService} from '../../services/message.service';
import {MessageType} from '../../models/message.model';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-delete-typical',
  templateUrl: './delete-typical.component.html',
  styleUrls: ['./delete-typical.component.css']
})
export class DeleteTypicalComponent implements OnInit {

  @Input() deleteTypicalWindowOpen;
  @Input() selectedTypical;

  @Output() public closeDeleteTypicalWindow = new EventEmitter<any>();
  @Output() public removeSystemFromList = new EventEmitter<boolean>();

  constructor(
    private designService: DesignService,
    private messageService: MessageService,
    private permissionService: PermissionService
  ) {}

  ngOnInit() {}

  deleteTypicalManually() {
    this.designService.deleteTypicalManually(this.selectedTypical.id).subscribe(res => {
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res) {
      this.messageService.sendMessage({text: 'Typical was deleted successfully!', type: MessageType.Success});
      this.removeSystemFromList.emit(this.selectedTypical);
    } else {
      this.close();
    }
  }

  close() {
    this.closeDeleteTypicalWindow.emit();
  }

}
