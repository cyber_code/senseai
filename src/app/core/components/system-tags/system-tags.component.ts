import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { SessionService } from '../../services/session.service';
import * as _ from 'lodash';
import { System } from '../../models/system.model';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-system-tags',
  templateUrl: './system-tags.component.html',
  styleUrls: ['./system-tags.component.css']
})
export class SystemTagsComponent implements OnInit {
  @Input() gridData;
  @Input() state;
  @Input() systemTags = [];

  @Output() systemTagListChanged = new EventEmitter<any>();
  @Output() systemTagsListChanged = new EventEmitter<any>();

  @Output() systemTagDataStateChanged = new EventEmitter<any>();
  @Output() systemTagIdChanged = new EventEmitter<any>();
  @Output() isEditableItem = new EventEmitter<boolean>();

  public selectedSystemTag = {
    systemId: '',
    title: '',
    description: ''
  };

  selectedSystemIdFromKendoCmb: string = '';
  public systemTagWindowOpen = false;
  public isSystemTagEdited = false;
  public deleteSystemTagWindowOpen = false;

  systemList: any[] = [];

  constructor(private designService: DesignService, private permissionService: PermissionService) {}

  ngOnInit() {
    this.designService.getSystems().subscribe(response => {
      this.systemList = response;
    });
  }

  systemIdChanged() {
    this.systemTagIdChanged.emit(this.selectedSystemIdFromKendoCmb);
  }

  dataStateChange(state) {
    this.systemTagDataStateChanged.emit(state);
  }

  openSystemTagPopUp(item?: any) {
    this.systemTagWindowOpen = true;
    if (item) {
      this.isSystemTagEdited = true;
      this.selectedSystemTag = _.cloneDeep(item);
      this.isEditableItem.emit(true);
    } else {
      this.isEditableItem.emit(false);
      this.resetSystemTag();
    }
  }

  resetSystemTag() {
    this.selectedSystemTag = {
      systemId: this.selectedSystemIdFromKendoCmb,
      title: '',
      description: ''
    };
  }

  getTagsBySystem(event: any) {
    this.selectedSystemIdFromKendoCmb = event.id;
    this.systemTagIdChanged.emit(this.selectedSystemIdFromKendoCmb);
  }

  close() {
    this.systemTagWindowOpen = false;
    this.deleteSystemTagWindowOpen = false;
    this.resetSystemTag();
  }

  updateSystemTagList(systemTag) {
    //debugger;
    let systemTags = ([] = []);
    if (this.isSystemTagEdited) {
      systemTags = this.systemTags.map(sysTag => {
        if (sysTag.id === systemTag.id) {
          return systemTag;
        }
        return sysTag;
      });
    } else {
      systemTags = this.systemTags.concat(systemTag);
    }
    this.systemTagsListChanged.emit(systemTags);
    this.close();
  }

  removeSystemTagFromList(systemTag) {
    let systemTags = this.systemTags.filter(sys => sys.id !== systemTag.id);
    this.systemTagsListChanged.emit(systemTags);
    this.close();
  }

  openDeleteSystemTagWindow(systemTag) {
    this.deleteSystemTagWindowOpen = true;
    this.selectedSystemTag = systemTag;
  }

  openSystemTagWindow(systemTag, isEdited?: boolean) {
    this.isEditableItem.emit(isEdited);
    this.systemTagWindowOpen = true;
    this.isSystemTagEdited = isEdited;
    if (systemTag) {
      this.isSystemTagEdited = true;
      this.selectedSystemTag = _.cloneDeep(systemTag);
    } else {
      this.resetSystemTag();
    }
  }
}
