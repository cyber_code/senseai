import { DefaultSettingsService } from './components/default-settings/default-settings.service';
import { ChangeDefaultSettingsComponent } from './components/default-settings/default-settings.component';
import { AdministrationService } from './services/administration.service';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MessageComponent } from './components/message/message.component';
import { RouterModule } from '@angular/router';
import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { BrowserModule } from '@angular/platform-browser';
import { WindowModule, DialogModule } from '@progress/kendo-angular-dialog';
import { ButtonsModule, ButtonGroupModule } from '@progress/kendo-angular-buttons';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ProjectSelectionComponent } from './components/project-selection/project-selection.component';
import { CoreRoutingModule } from './core-routing.module';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { PopupModule } from '@progress/kendo-angular-popup';
import { ProjectSelectionService } from './components/project-selection/project-selection.service';
import { ConfigurationMockProvider } from './mock-providers/configuration-mock-provider';
import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { ChangeProfileComponent } from './components/change-profile/change-profile.component';
import { AvatarModule } from 'ngx-avatar';
import { ChangePasswordComponent } from './components/change-profile/change-password/change-password.component';
import { ChangeEmailComponent } from './components/change-profile/change-email/change-email.component';
import { ChangeUserNameComponent } from './components/change-profile/change-user-name/change-user-name.component';
import { AppTreViewComponent } from './components/tree-view/app-tree-view.component';
import { AppArisTreViewComponent } from './components/aris-tree-view/aris-tree-view.component';
import { NgDragDropModule } from 'ng-drag-drop';
import { MonitoringPageComponent } from './components/monitoring-page/monitoring-page.component';
import { GridModule, SharedModule } from '@progress/kendo-angular-grid';
import { MonitoringService } from './services/monitoring.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertComponent } from './components/alert/alert.component';
import { ModelChangesComponent } from './components/model-changes/model-changes.component';
import { LayoutModule } from '@progress/kendo-angular-layout';
import { PaginationComponent } from './components/pagination/pagination.component';
import { ModelChangesService } from './components/model-changes/model-changes.service';
import { FilterPipe } from './components/model-changes/model-changes.pipe';
import { CatalogsComponent } from './components/catalogs/catalogs.component';
import { AddEditCatalogComponent } from './components/add-edit-catalog/add-edit-catalog.component';
import { DeleteCatalogComponent } from './components/delete-catalog/delete-catalog.component';
import { SelectSystemComponent } from './components/select-system/select-system.component';
import { SystemsCatalogsComponent } from './components/systems-catalogs/systems-catalogs.component';
import { SystemsComponent } from './components/systems/systems.component';
import { AddEditSystemComponent } from './components/add-edit-system/add-edit-system.component';
import { DeleteSystemComponent } from './components/delete-system/delete-system.component';
import { TypicalsComponent } from './components/typicals/typicals.component';
import { AddEditTypicalComponent } from './components/add-edit-typical/add-edit-typical.component';
import { DeleteTypicalComponent } from './components/delete-typical/delete-typical.component';
import { AddEditSystemTagComponent } from './components/add-edit-system-tag/add-edit-system-tag.component';
import { SystemTagsComponent } from './components/system-tags/system-tags.component';
import { DeleteSystemTagComponent } from './components/delete-system-tag/delete-system-tag.component';
import { AttributesComponent } from './components/attributes/attributes.component';
import { AddEditAttributesComponent } from './components/add-edit-attributes/add-edit-attributes.component';
import { DeleteAttributeComponent } from './components/delete-attribute/delete-attribute.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TypicalAttributesComponent } from './components/typical-attributes/typical-attributes.component';
import { JiraUserAuthComponent } from './components/navbar/components/jira-user-auth/jira-user-auth.component';
import { DeleteTypicalAttributeComponent } from './components/delete-typical-attribute/delete-typical-attribute.component';
import { ThemeComponent } from './components/theme/theme.component';
import { HelpComponent } from './components/help/help.component';

@NgModule({
  declarations: [
    FilterPipe,
    LoginComponent,
    NavbarComponent,
    ProjectSelectionComponent,
    NotFoundComponent,
    ChangeProfileComponent,
    ChangePasswordComponent,
    ChangeEmailComponent,
    ChangeUserNameComponent,
    AppTreViewComponent,
    AppArisTreViewComponent,
    ChangeDefaultSettingsComponent,
    AddEditCatalogComponent,
    AddEditSystemComponent,
    DeleteCatalogComponent,
    DeleteSystemComponent,
    SelectSystemComponent,
    SystemsCatalogsComponent,
    SystemsComponent,
    MonitoringPageComponent,
    MessageComponent,
    AlertComponent,
    ModelChangesComponent,
    PaginationComponent,
    CatalogsComponent,
    TypicalsComponent,
    AddEditTypicalComponent,
    DeleteTypicalComponent,
    AddEditSystemTagComponent,
    SystemTagsComponent,
    DeleteSystemTagComponent,
    AttributesComponent,
    AddEditAttributesComponent,
    DeleteAttributeComponent,
    TypicalAttributesComponent,
    JiraUserAuthComponent,
    DeleteTypicalAttributeComponent,
    HelpComponent,
    ThemeComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    CoreRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    WindowModule,
    DialogModule,
    TreeViewModule,
    ButtonsModule,
    ButtonGroupModule,
    PopupModule,
    AvatarModule,
    NgDragDropModule.forRoot(),
    DropDownsModule,
    GridModule,
    HttpClientModule,
    LayoutModule,
    SharedModule,
    NgbModule
  ],
  providers: [
    ProjectSelectionService,
    AdministrationService,
    DefaultSettingsService,
    MonitoringService,
    DatePipe,
    ConfigurationMockProvider,
    ModelChangesService
  ],
  exports: [
    LoginComponent,
    NavbarComponent,
    MessageComponent,
    AlertComponent,
    ProjectSelectionComponent,
    NotFoundComponent,
    AppTreViewComponent,
    AppArisTreViewComponent,
    ChangeDefaultSettingsComponent,
    ChangeProfileComponent,
    ChangePasswordComponent,
    ChangeUserNameComponent,
    ChangeEmailComponent
  ]
})
export class CoreModule { }
