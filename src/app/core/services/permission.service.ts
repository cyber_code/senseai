import {ConfigurationService} from './configuration.service';
import {Injectable} from '@angular/core';
import { HttpExecutorService } from './http-executor.service';
import { GetAllPermissions } from './command-queries/command-queries';
import { isNullOrUndefined } from 'util';
import { SessionService } from './session.service';

@Injectable({providedIn: 'root'})
export class PermissionService {

    private identityUrl: string;
    public permissions;

    constructor(
        private configurationService: ConfigurationService,
        private httpExecutor: HttpExecutorService,
        private sessionService: SessionService
    ) {
        this.identityUrl = configurationService.serverSettings.apiUrl + 'Identity/';
    }
    
    hasPermission(action) {
        let user = this.sessionService.getUser();
        let permissions = user ? user.permissions : [];
        if (!isNullOrUndefined(permissions.find(permission => permission.trim() === action.trim()))) {
            return true;
        }
        return false;
    }
}
