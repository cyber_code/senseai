import {ConfigurationService} from './configuration.service';
import {Injectable} from '@angular/core';
import { HttpExecutorService } from './http-executor.service';
import { GetAllPermissions } from './command-queries/command-queries';
import { isNullOrUndefined } from 'util';
import { SessionService } from './session.service';
import { Router } from '@angular/router';
import {Licenses} from '../models/licenses.model';
import { AuthenticationService } from './authentication.service';

@Injectable({providedIn: 'root'})
export class LicenseService {

    private identityUrl: string;

    constructor(
        private configurationService: ConfigurationService,
        private httpExecutor: HttpExecutorService,
        private sessionService: SessionService,
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        this.identityUrl = configurationService.serverSettings.apiUrl + 'Identity/';
    }

    
    hasLicense(action) {
        let user = this.sessionService.getUser();
        let licenses = user ? user.licenses : [];
        if (!isNullOrUndefined(licenses.find(license => license.toString() === action.toString()))) {
            return true;
        }
        return false;
    }

    redirectToModuleByLicense() {
        if (this.hasLicenseForRequirement()) {
            this.router.navigateByUrl('/process-main');
        }
        else if (this.hasLicenseForFlowDesigner()) {
            this.router.navigateByUrl('/workflow-designer');
        }
        else if (this.hasLicenseForDataIntelligence()) {
            this.router.navigateByUrl('/data-management');
        }
        else if (this.hasLicenseForTestIntelligence()) {
            this.router.navigateByUrl('/test-generation');
        }
        else if (this.hasLicenseForDefectIntelligence()) {
            this.router.navigateByUrl('/reports');
        }
        else if (this.hasLicenseForAIAnalytics()){
            this.router.navigateByUrl('/requirement-reports');
        } else {
            this.authenticationService.logout();
            this.router.navigateByUrl('/login');
        }
        
    }

    hasLicenseForRequirement() {
        return (this.hasLicense(Licenses.Sprint_planning_and_management) ||
        this.hasLicense(Licenses.Requirements_and_hierarchies) ||
        this.hasLicense(Licenses.Prebuild_processed));
    }

    hasLicenseForFlowDesigner() {
        return (this.hasLicense(Licenses.Content_License_Copy_right) ||
        this.hasLicense(Licenses.Computer_Assisted_Computer_generated) ||
        this.hasLicense(Licenses.Recording));
    }

    hasLicenseForDataIntelligence() {
        return (this.hasLicense(Licenses.AI_recommended_Data) || 
        this.hasLicense(Licenses.Synthetic_data));
    }

    hasLicenseForTestIntelligence() {
        return (this.hasLicense(Licenses.Test_case_export_to_ATS_for_execution) ||
        this.hasLicense(Licenses.Test_case_export_in_other_format) ||
        this.hasLicense(Licenses.Model_association_exposure));
    }

    hasLicenseForDefectIntelligence() {
        return (this.hasLicense(Licenses.Defect_Import) ||
        this.hasLicense(Licenses.Defect_Recommendations) ||
        this.hasLicense(Licenses.QA_plan) ||
        this.hasLicense(Licenses.Dev_Plan));
    }

    hasLicenseForAIAnalytics() {
        return (this.hasLicense(Licenses.Defect_Mining_graph) ||
        this.hasLicense(Licenses.Requirement_Prioritization) ||
        this.hasLicense(Licenses.Requirements_traceability) ||
        this.hasLicense(Licenses.Workflow_coverage) ||
        this.hasLicense(Licenses.Test_coverage) ||
        this.hasLicense(Licenses.Defect_Insights) ||
        this.hasLicense(Licenses.Defect_Replay) ||
        this.hasLicense(Licenses.Risk_coverage) ||
        this.hasLicense(Licenses.Risk_optimize) ||
        this.hasLicense(Licenses.Release_summary) ||
        this.hasLicense(Licenses.Requirements_Insights));
    }
}
