﻿import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {RpaMessage} from '../models/message.model';

@Injectable({providedIn: 'root'})
export class MessageService {
  private subject = new Subject<RpaMessage>();

  sendMessage(message: RpaMessage) {
    this.subject.next(message);
  }

  getMessage(): Observable<RpaMessage> {
    return this.subject.asObservable();
  }
}
