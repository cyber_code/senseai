import { ConfigurationService } from './configuration.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MessageService } from './message.service';
import { MessageType } from '../models/message.model';
import {Observable, of} from 'rxjs';

@Injectable({ providedIn: 'root' })
export class VersionService {

    constructor(
        private http: HttpClient,
        private configurationService: ConfigurationService,
        private messageService: MessageService
    ) {
    }

    getApiVersion() {
       return this.http.get(this.configurationService.serverSettings.apiUrl + 'Services/GetVersion').pipe(
        map((res: any) => {
          if (res && res.hasResult && res.successful) {
            return res.result;
          } else if (res && res.error && res.error.exceptionObject && res.error.exceptionObject.Message) {
            this.messageService.sendMessage({type: MessageType.Error, text: res.error.exceptionObject.Message});
            return;
          } 
        })
      );
    }

    getMLVersion(): Observable<any> {
      const url = this.configurationService.serverSettings.reportsUrl + 'version';
      const res = this.http.get(url);
      return res;
    }

}
