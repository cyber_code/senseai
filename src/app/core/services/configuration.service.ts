﻿import { Injectable } from '@angular/core';
import { Configuration } from '../models/configuration.model';
import { SessionService } from './session.service';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class ConfigurationService {
    
    public serverSettings: Configuration;

    constructor(private sessionService: SessionService, private http: HttpClient) {
    }

    load(): Promise<Configuration> {

        return this.http.get('./config.json')
            .pipe(
                tap((config: Configuration) => {
                    this.serverSettings = config;
                    this.sessionService.setAuthorityUrl(this.serverSettings.identityUrl);
                    if(config.useStaticHashId === true && config.hashId != null) {
                        this.sessionService.setStaticHashId(config.hashId);
                    } else {
                        this.sessionService.setStaticHashId(null);
                    }
                })
            )
            .toPromise<Configuration>();
    }
}