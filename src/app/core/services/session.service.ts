import {Helper} from 'src/app/shared/helper';
import {SubProject} from '../models/sub-project.model';
import {Injectable, Inject} from '@angular/core';
import {LOCAL_STORAGE, StorageService} from 'angular-webstorage-service';
import {User} from '../models/user.model';
import {WorkContext} from '../models/work-context.model';
import {Project} from '../models/project.model';
import {DefaultSettings} from '../models/default-settings.model';
import {Catalog} from '../models/catalog.model';
import {System} from '../models/system.model';

const STORAGE_KEY = 'RAP_SESSION_KEY';

@Injectable({providedIn: 'root'})
export class SessionService {
  staticHashId: string;
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
  }

  setAuthorityUrl(authorityUrl: string) {
    const context = this.getWorkContext();
    context.identityUrl = authorityUrl;
    this.storage.set(STORAGE_KEY, context);
  }

  setUser(user: User) {
    const context = this.getWorkContext();
    context.user = user;
    this.storage.set(STORAGE_KEY, context);
  }

  getUser(): User {
    const context = this.getWorkContext();
    return context.user;
  }

  getToken(): string {
    const user = this.getUser();
    if (user == null) {
      return null;
    }
    return this.getUser().token;
  }

  setProject(project: Project) {
    const context = this.getWorkContext();
    context.project = project;
    this.storage.set(STORAGE_KEY, context);
  }

  setSubProject(subProject: SubProject) {
    const context = this.getWorkContext();
    context.subProject = subProject;
    this.storage.set(STORAGE_KEY, context);
  }

  getProject(): Project {
    const context = this.getWorkContext();
    return context.project;
  }

  getSubProject(): SubProject {
    const context = this.getWorkContext();
    return context.subProject;
  }

  clearSession() {
    this.storage.remove(STORAGE_KEY);
  }

  getWorkContext(): WorkContext {
    let context = this.storage.get(STORAGE_KEY) as WorkContext;
    if (context == null) {
      context = new WorkContext();

      context.defaultSettings = new DefaultSettings(new Catalog(), new System());
      this.storage.set(STORAGE_KEY, context);
    }
    return context;
  }

  getDefaultSettings(): DefaultSettings {
    const context = this.getWorkContext();
    return context.defaultSettings;
  }

  setDefaultSettings(defaultsettings: DefaultSettings) {
    const context = this.getWorkContext();
    context.defaultSettings = defaultsettings;
    this.storage.set(STORAGE_KEY, context);
  }

  setStaticHashId(staticHashId: string) {
    const context = this.getWorkContext();
    context.staticHashId = staticHashId;
    this.storage.set(STORAGE_KEY, context);
  }

  getHashId() {
    const context = this.getWorkContext();

    if(context.staticHashId != null)
      return context.staticHashId;

    // ledio: for some reason the systemId is hardcoded in RPA when the hashId is calculated
    //const systemId = context.defaultSettings.system.id;
    const systemId = "b5bcc43f-afc8-4f67-8eb1-b633994ce363";
    return Helper.generateHashId(
      context.user.tenantId,
      context.project.id,
      context.subProject.id,
      systemId,
      context.defaultSettings.catalog.id
    );
  }
}
