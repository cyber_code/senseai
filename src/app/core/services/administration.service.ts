import {DefaultSettings, Settings} from './../models/default-settings.model';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ConfigurationService} from './configuration.service';
import {HttpExecutorService} from './http-executor.service';
import {Observable, of, Subject} from 'rxjs';
import {Cacheable, CacheBuster} from 'ngx-cacheable';
import {SessionService} from 'src/app/core/services/session.service';
import {Project} from '../models/project.model';
import {
  GetProjects,
  GetSubProjects,
  AddProject,
  DeleteProject,
  AddSubProject,
  DeleteSubProject,
  UpdateProject,
  UpdateSubProject,
  AddFolder,
  DeleteFolder,
  UpdateFolder,
  GetSetting
} from './command-queries/command-queries';
import {SubProject} from '../models/sub-project.model';
import {CommandMethod} from 'src/app/shared/command-method';
import {Folder} from '../models/folder.model';

const projectNotifier = new Subject<void>();
const subProjectNotifier = new Subject<void>();

@Injectable()
export class AdministrationService {
  private apiUrl: string;

  constructor(private configuration: ConfigurationService, private httpExecutor: HttpExecutorService) {
      this.apiUrl = `${configuration.serverSettings.apiUrl}Administration/`;
  }

  addFolder(folder: Folder): Observable<Folder> {
    return this.httpExecutor.executeCommand<Folder>(this.apiUrl, new AddFolder(folder.subProjectId, folder.title), CommandMethod.POST).pipe(
      map(folderCreated => {
        return {
          id: folderCreated['id'],
          title: folder.title,
          description: folder.description,
          subProjectId: folder.subProjectId
        } as Folder;
      })
    );
  }

  @Cacheable({
    cacheBusterObserver: projectNotifier
  })
  getProjects(): Observable<Project[]> {
    return this.httpExecutor.executeQuery<Project[]>(this.apiUrl, new GetProjects());
  }

  @Cacheable({
    cacheBusterObserver: subProjectNotifier
  })
  getSubProjects(projectId: string): Observable<SubProject[]> {
    return this.httpExecutor.executeQuery<SubProject[]>(this.apiUrl, new GetSubProjects(projectId));
  }

  @CacheBuster({
    cacheBusterNotifier: projectNotifier
  })
  createProject(project: Project): Observable<Project> {
    return this.httpExecutor.executeCommand<Response>(this.apiUrl, new AddProject(project.title), CommandMethod.POST).pipe(
      map(res => {
        return {id: res['id'], title: project.title, description: project.description} as Project;
      })
    );
  }

  @CacheBuster({
    cacheBusterNotifier: projectNotifier
  })
  deleteProject(projectId: string): Observable<any> {
    return this.httpExecutor.executeCommand<any>(this.apiUrl, new DeleteProject(projectId), CommandMethod.DELETE);
  }

  @CacheBuster({
    cacheBusterNotifier: subProjectNotifier
  })
  createSubProject(subProject: SubProject): Observable<SubProject> {
    return this.httpExecutor
      .executeCommand<Response>(
        this.apiUrl,
        new AddSubProject(subProject.projectId, subProject.title, subProject.description),
        CommandMethod.POST
      )
      .pipe(
        map(res => {
          return {
            id: res['id'],
            projectId: subProject.projectId,
            title: subProject.title,
            description: subProject.description
          } as SubProject;
        })
      );
  }

  @CacheBuster({
    cacheBusterNotifier: subProjectNotifier
  })
  deleteSubProject(subProjectId: string): Observable<any> {
    return this.httpExecutor.executeCommand<any>(this.apiUrl, new DeleteSubProject(subProjectId), CommandMethod.DELETE);
  }

  deleteFolder(id: string): Observable<any> {
    return this.httpExecutor.executeCommand<Folder>(this.apiUrl, new DeleteFolder(id), CommandMethod.DELETE);
  }

  editProject(project: Project): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.apiUrl,
      new UpdateProject(project.id, project.title, project.description),
      CommandMethod.PUT
    );
  }

  editSubProject(subProject: SubProject): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.apiUrl,
      new UpdateSubProject(subProject.id, subProject.projectId, subProject.title, subProject.description),
      CommandMethod.PUT
    );
  }

  updateFolder(title: string, id: string): Observable<Folder> {
    const description = '';
    return this.httpExecutor.executeCommand<Folder>(this.apiUrl, new UpdateFolder(title, id, description), CommandMethod.PUT);
  }

  getSettings(projectId: string, subProjectId: string): Observable<Settings> {
    return this.httpExecutor.executeQuery(this.apiUrl, new GetSetting(projectId, subProjectId));
  }
}
