import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { SessionService } from './core/services/session.service';

let routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
