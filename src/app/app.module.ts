import {BrowserModule} from '@angular/platform-browser';
import {NgModule, APP_INITIALIZER} from '@angular/core';
import {StorageServiceModule} from 'angular-webstorage-service';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DataManagementModule} from './data-management/data-management.module';
import {TestGenerationModule} from './test-generation/test-generation.module';
import {TreeViewModule} from '@progress/kendo-angular-treeview';
import {CoreModule} from './core/core.module';
import {DialogsModule} from '@progress/kendo-angular-dialog';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {LayoutModule} from '@progress/kendo-angular-layout';
import {PopupModule} from '@progress/kendo-angular-popup';
import {NotificationModule} from '@progress/kendo-angular-notification';
import {ToolBarModule} from '@progress/kendo-angular-toolbar';
import {MenuModule} from '@progress/kendo-angular-menu';
import {DesignModule} from './design/design.module';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {GridModule, ExcelModule } from '@progress/kendo-angular-grid';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {ImageZoomModule} from 'angular2-image-zoom';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
import {ReportsModule} from './reports/reports.module';
import {HttpClientModule} from '@angular/common/http';
import {DataTablesModule} from 'angular-datatables';
import {ReactiveFormsModule} from '@angular/forms';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {Ng5SliderModule} from 'ng5-slider';
import {ConfigurationService} from './core/services/configuration.service';
import {ProductModule} from './product/product.module';
import {ProductComponent} from './product/components/product.component';
import { ProcessManagmentModule } from './process-managment/process-managment.module';
import { TreeModule } from 'angular-tree-component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AiAnalyticsModule } from './ai-analytics/ai-analytics.module';
import { ModalDataCoverageModule } from './ai-analytics/components/requirement-reports/workflows/modal-data-coverage/modal-data-coverage.module';

const appInitializerFn = (appConfig: ConfigurationService) => {
  return () => {
    return appConfig.load();
  };
};

@NgModule({
  declarations: [AppComponent, ProductComponent],
  imports: [
    BrowserModule,
    StorageServiceModule,
    AppRoutingModule,
    DataManagementModule,
    TestGenerationModule,
    ReportsModule,
    TreeViewModule,
    BrowserAnimationsModule,
    CoreModule,
    AiAnalyticsModule,
    DialogsModule,
    ButtonsModule,
    LayoutModule,
    PopupModule,
    NotificationModule,
    ToolBarModule,
    MenuModule,
    DesignModule,
    ProcessManagmentModule,
    InputsModule,
    DropDownsModule,
    GridModule,
    ExcelModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ImageZoomModule,
    DataTablesModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    ModalDataCoverageModule,
    Ng5SliderModule,
    HttpClientModule,
    ProductModule,
    DragDropModule,
    TabsModule.forRoot()
  ],
  providers: [
    ConfigurationService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFn,
      multi: true,
      deps: [ConfigurationService]
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
