import {RapidEventsService} from './services/rapid-events.service';
import {CoreModule} from '../core/core.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TreeViewModule} from '@progress/kendo-angular-treeview';
import {HttpClientModule} from '@angular/common/http';
import {LayoutModule} from '@progress/kendo-angular-layout';
import {PopupModule} from '@progress/kendo-angular-popup';
import {WindowModule, DialogModule} from '@progress/kendo-angular-dialog';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {MenuModule} from '@progress/kendo-angular-menu';
import {WorkFlowComponent} from './components/work-flow/work-flow.component';
import {WorkflowToolbarComponent} from './components/workflow-toolbar/workflow-toolbar.component';
import {DesignRoutingModule} from './design-routing.module';
import {DesignService} from './services/design.service';
import {WorkflowService} from './services/workflow.service';
import {ApplicationStepComponent} from './components/properties/workflow-items/application-step/application-step.component';
import {TitleComponent} from './components/properties/controls/title/title.component';
import {DescriptionComponent} from './components/properties/controls/description/description.component';
import {CatalogTypicalComponent} from './components/properties/controls/catalog-typical/catalog-typical.component';
import {EnquiryActionStepComponent} from './components/properties/workflow-items/enquiry-action-step/enquiry-action-step.component';
import {CheckEnquiryStepComponent} from './components/properties/workflow-items/check-enquiry-step/check-enquiry-step.component';
import {NavigationStepComponent} from './components/properties/workflow-items/navigation/navigation-step.component';
import {InputsModule} from '@progress/kendo-angular-inputs';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {StartItemComponent} from './components/properties/workflow-items/start-item/start-item.component';
import {ConditionComponent} from './components/properties/workflow-items/condition/condition.component';
import {GridModule} from '@progress/kendo-angular-grid';
import {WorkflowItemService} from './services/workflow-item.service';
import {NgDragDropModule} from 'ng-drag-drop';
import {TimeVarianceComponent} from './components/time-variance/time-variance.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';
import {WorkflowVersionComponent} from './components/properties/workflow-version/workflow-version.component';
import {WorkflowPathsComponent} from './components/properties/workflow-paths/workflow-paths.component';
import {SortableModule} from '@progress/kendo-angular-sortable';
import {AngularDraggableModule} from 'angular2-draggable';
import {DialogsModule} from '@progress/kendo-angular-dialog';
import {PropetiesContainerComponent} from './components/properties/propeties-container/propeties-container.component';
import {IsNegativeStepComponent} from './components/properties/controls/isnegativestep/isnegativestep.component';
import {FiltersPostFiltersComponent} from './components/properties/controls/filters-postfilters/filters-postfilters.component';
import {ActionComponent} from './components/properties/controls/action/action.component';
import {DynamicDataComponent} from './components/properties/controls/dynamicdata/dynamicdata.component';
import {DynamicDataPopupComponent} from './components/properties/controls/dynamicdatapopup/dynamicdatapopup.component';
import {DynamicDataService} from './components/properties/controls/dynamicdatapopup/dynamic-data.service';
import {PerformActionStepComponent} from './components/properties/workflow-items/perform-action-step/perform-action-step.component';
import {EnquiryActionsComponent} from './components/properties/controls/enquiry-actions/enquiry-actions.component';
import { PreviewWorkflowComponent } from './components/preview-workflow/preview-workflow.component';
import { ManualStepComponent } from './components/properties/workflow-items/manual-step/manual-step.component';
import { RoleComponent } from './components/properties/controls/role/role.component';
import { ManualActionComponent } from './components/properties/controls/manual-action/manual-action.component';
import { ImportArisService } from './services/import-aris.service';
import { PopupContentComponent } from './components/properties/popup-content/popup-content.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    TreeViewModule,
    HttpClientModule,
    FormsModule,
    DesignRoutingModule,
    CoreModule,
    LayoutModule,
    WindowModule,
    ButtonsModule,
    DialogModule,
    ReactiveFormsModule,
    PopupModule,
    MenuModule,
    InputsModule,
    DropDownsModule,
    GridModule,
    NgDragDropModule.forRoot(),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    SortableModule,
    AngularDraggableModule,
    DialogsModule
  ],
  providers: [
    DesignService,
    RapidEventsService,
    WorkflowItemService,
    WorkflowService,
    DynamicDataService,
    DynamicDataComponent,
    ImportArisService
  ],
  declarations: [
    WorkFlowComponent,
    WorkflowToolbarComponent,
    ApplicationStepComponent,
    TitleComponent,
    CatalogTypicalComponent,
    DescriptionComponent,
    IsNegativeStepComponent,
    FiltersPostFiltersComponent,
    PreviewWorkflowComponent,
    ActionComponent,
    EnquiryActionsComponent,
    DynamicDataComponent,
    DynamicDataPopupComponent,
    EnquiryActionStepComponent,
    CheckEnquiryStepComponent,
    NavigationStepComponent,
    PerformActionStepComponent,
    StartItemComponent,
    ConditionComponent,
    ManualStepComponent,
    TimeVarianceComponent,
    WorkflowVersionComponent,
    WorkflowPathsComponent,
    PropetiesContainerComponent,
    ManualActionComponent,
    RoleComponent,
    PopupContentComponent
  ],
  exports: [WorkFlowComponent]
})
export class DesignModule {
}
