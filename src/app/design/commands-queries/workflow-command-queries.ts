import { ApiAction } from 'src/app/core/services/command-queries/api-action';

export class AddWorkflow extends ApiAction {
  constructor(title: string, folderId: string, description?: string) {
    super('AddWorkflow');
    this.title = title;
    this.description = description;
    this.folderId = folderId;
  }

  title: string;
  description: string;
  folderId: string;
}

export class RenameWorkflow extends ApiAction {
  constructor(title: string, workflowId: string) {
    super('RenameWorkflow');
    this.title = title;
    this.workflowId = workflowId;
  }

  title: string;
  workflowId: string;
}

export class DeleteWorkflow extends ApiAction {
  constructor(workflowId: string) {
    super('DeleteWorkflow');
    this.id = workflowId;
  }

  id: string;
}

export class PasteWorkflow extends ApiAction {
  constructor(workflowId: string, folderId: string, type: number) {
    super('PasteWorkflow');
    this.workflowId = workflowId;
    this.folderId = folderId;
    this.type = type;
  }

  workflowId: string;
  folderId: string;
  type: number;
}

export class CutWorkflow extends ApiAction {
  constructor(workflowId: string) {
    super('CutWorkflow');
    this.workflowId = workflowId;
  }

  workflowId: string;
}

export class IssueWorkflow extends ApiAction {
  constructor(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string,
    workflowId: string, workflowImage: string, newVersion: boolean, skipStatus: boolean, versionStatus: number = 0,
    currentVersionId: number = 0) {
    super('IssueWorkflow');
    this.tenantId = tenantId;
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
    this.workflowId = workflowId;
    this.workflowImage = workflowImage;
    this.newVersion = newVersion;
    this.skipStatus = skipStatus;
    this.versionStatus = versionStatus;
    this.currentVersionId = currentVersionId;
  }

  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
  newVersion: boolean;
  workflowId: string;
  workflowImage: string;
  skipStatus: boolean;
  versionStatus: number;
  currentVersionId: number;
}

export class PostIssueWorkflow extends ApiAction {
  constructor(workflowId: string, tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string) {
    super('PostIssueWorkflow');
    this.workflowId = workflowId;
    this.tenantId = tenantId;
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
  }

  workflowId: string;
  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
}

export class SaveWorkflow extends ApiAction {
  constructor(workflowId: string, json: string, workflowPathsJson: string) {
    super('SaveWorkflow');
    this.id = workflowId;
    this.json = json;
    this.workflowPathsJson = workflowPathsJson;
  }

  id: string;
  json: string;
  workflowPathsJson: string;
}

export class GetWorkflow extends ApiAction {
  constructor(workflowId: string) {
    super('GetWorkflow');
    this.workflowId = workflowId;
  }

  workflowId: string;
}

export class GetWorkflowByVersion extends ApiAction {
  constructor(workflowId: string, versionId: number) {
    super('GetWorkflowByVersion');
    this.workflowId = workflowId;
    this.versionId = versionId;
  }

  workflowId: string;
  versionId: number;
}

export class GetDynamicDataSuggestions extends ApiAction {
  SourceTypicalName: string;
  TargetTypicalName: string;

  constructor(SourceTypicalName: string, TargetTypicalName: string) {
    super('GetDynamicDataSuggestions');
    this.SourceTypicalName = SourceTypicalName;
    this.TargetTypicalName = TargetTypicalName;
  }
}

export class SaveDynamicDataSuggestions extends ApiAction {
  SourceTypicalName: string;
  TargetTypicalName: string;
  SourceAttributeName: string;
  TargetAttributeName: string;

  constructor(SourceTypicalName: string, SourceAttributeName: string, TargetTypicalName: string, TargetAttributeName: string) {
    super('SaveDynamicDataSuggestions');
    this.SourceTypicalName = SourceTypicalName;
    this.SourceAttributeName = SourceAttributeName;

    this.TargetTypicalName = TargetTypicalName;
    this.TargetAttributeName = TargetAttributeName;
  }
}

export class GetWorkflows extends ApiAction {
  folderId: string;

  constructor(folderId: string) {
    super('GetWorkflows');
    this.folderId = folderId;
  }
}

export class GetWorkflowVersionsByDate extends ApiAction {
  WorkflowId: string;
  DateFrom: string;
  DateTo: string;

  constructor(workflowId: string, fromDate: string, toDate: string) {
    super('GetWorkflowVersionsByDate');
    this.DateFrom = fromDate;
    this.DateTo = toDate;
    this.WorkflowId = workflowId;
  }
}

// export class GetWorkflowVersions  extends ApiAction{
//   folderId: string;
//   constructor(folderId: string) {
//   super('GetWorkflowVersions');
//   this.folderId = folderId
//   }
// }

export class SearchWorkflows extends ApiAction {
  searchString: string;

  constructor(searchString: string, subProjectId: string) {
    super('SearchWorkflows');
    this.subProjectId = subProjectId;
    this.searchString = searchString;
  }

  subProjectId: string;
}

export class GetSystems extends ApiAction {
  constructor() {
    super('GetSystems');
  }
}

export class GetSystem extends ApiAction {
  Id: string;

  constructor(Id: string) {
    super('GetSystem');
    this.Id = Id;
  }
}

export class AddSystem extends ApiAction {
  title: string;
  adapterName: string;

  constructor(title: string, adapterName: string) {
    super('AddSystem');
    this.title = title;
    this.adapterName = adapterName;
  }
}

export class UpdateSystem extends ApiAction {
  id: string;
  title: string;
  adapterName: string;

  constructor(id: string, title: string, adapterName: string) {
    super('UpdateSystem');
    this.id = id;
    this.title = title;
    this.adapterName = adapterName;
  }
}

export class DeleteSystem extends ApiAction {
  Id: string;

  constructor(id: string) {
    super('DeleteSystem');
    this.Id = id;
  }
}

export class GetCatalogs extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetCatalogs');
    this.subProjectId = subProjectId;
  }
}

export class GetCatalogSystem extends ApiAction {
  subProjectId: string;
  catalogId: string;

  constructor(subProjectId: string, catalogId: string) {
    super('GetCatalogSystem');
    this.subProjectId = subProjectId;
    this.catalogId = catalogId;
  }
}

export class GetSystemCatalog extends ApiAction {
  subProjectId: string;
  systemId: string;
  catalogId: string;

  constructor(subProjectId: string, systemId: string, catalogId: string) {
    super('GetSystemCatalog');
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
  }
}

export class UpdateSystemCatalog extends ApiAction {
  id: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;

  constructor(id: string, subProjectId: string, systemId: string, catalogId: string) {
    super('UpdateSystemCatalog');
    this.id = id;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
  }
}

export class AddSystemCatalog extends ApiAction {
  subProjectId: string;
  systemId: string;
  catalogId: string;

  constructor(subProjectId: string, systemId: string, catalogId: string) {
    super('AddSystemCatalog');
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
  }
}

export class AddTenantCatalog extends ApiAction {
  title: string;
  description: string;
  type: string;

  constructor(title: string, description: string, type: string) {
    super('AddTenantCatalog');
    this.title = title;
    this.description = description;
    this.type = type;
  }
}

export class AddProjectCatalog extends ApiAction {
  projectId: string;
  title: string;
  description: string;
  type: string;

  constructor(projectId: string, title: string, description: string, type: string) {
    super('AddProjectCatalog');
    this.projectId = projectId;
    this.title = title;
    this.description = description;
    this.type = type;
  }
}

export class AddSubProjectCatalog extends ApiAction {
  projectId: string;
  subProjectId: string;
  title: string;
  description: string;
  type: string;

  constructor(projectId: string, subProjectId: string, title: string, description: string, type: string) {
    super('AddSubProjectCatalog');
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.title = title;
    this.description = description;
    this.type = type;
  }
}

export class UpdateCatalog extends ApiAction {
  id: string;
  title: string;
  description: string;
  type: string;

  constructor(id: string, title: string, description: string, type: string) {
    super('UpdateCatalog');
    this.id = id;
    this.title = title;
    this.description = description;
    this.type = type;
  }
}

export class DeleteCatalog extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteCatalog');
    this.id = id;
  }
}

export class GetTenantCatalogs extends ApiAction {
  subProjectId: string;
  projectId: string;

  constructor(projectId: string, subProjectId: string) {
    super('GetTenantCatalogs');
    this.projectId = projectId;
    this.subProjectId = subProjectId;
  }
}

export class GetAllNavigations extends ApiAction {
  subProjectId: string;
  systemId: string;

  constructor(subProjectId: string, systemId: string) {
    super('GetAllNavigations');
    this.subProjectId = subProjectId;
    this.systemId = systemId;
  }
}

export class GetCatalog extends ApiAction {
  catalogId: string;

  constructor(catalogId: string) {
    super('GetCatalog');
    this.catalogId = catalogId;
  }
}

export class GetEnquiryActions extends ApiAction {
  catalogId: string;
  typicalName: string;

  constructor(typicalName: string, catalogId: string) {
    super('GetEnquiryActions');
    this.catalogId = catalogId;
    this.typicalName = typicalName;
  }
}

export class GetActionItem extends ApiAction {
  adapterName: string;

  constructor(adapterName: string) {
    super('GetActionItem');
    this.adapterName = adapterName;
  }
}

export class GetConditionItem extends ApiAction {
  adapterName: string;

  constructor(adapterName: string) {
    super('GetConditionItem');
    this.adapterName = adapterName;
  }
}

export class SearchAttributes extends ApiAction {
  searchString: string;
  typicalId: string;

  constructor(searchString: string, typicalId: string) {
    super('SearchAttributes');
    this.searchString = searchString;
    this.typicalId = typicalId;
  }
}

export class SearchAttributesByCatalogIdAndTypicalName extends ApiAction {
  searchString: string;
  catalogId: string;
  typicalName: string;

  constructor(searchString: string, catalogId: string, typicalName: string) {
    super('SearchAttributesByCatalogIdAndTypicalName');
    this.searchString = searchString;
    this.catalogId = catalogId;
    this.typicalName = typicalName;
  }
}

export class SearchTypicals extends ApiAction {
  searchString: string;
  catalogId: string;

  constructor(searchString: string, catalogId: string) {
    super('SearchTypicals');
    this.searchString = searchString;
    this.catalogId = catalogId;
  }
}

export class GetTypicals extends ApiAction {
  catalogId: string;

  constructor(catalogId: string) {
    super('GetTypicals');
    this.catalogId = catalogId;
  }
}


export class AddTypicalManually extends ApiAction {
  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
  title: string;
  type: number;
  hasAutomaticId: boolean = true;
  isConfiguration: boolean = true;

  constructor(projectId: string, subProjectId: string, systemId: string, catalogId: string, title: string, type: number, tenantId: string, hasAutomaticId?: boolean, isConfiguration?: boolean) {
    super('AddTypicalManually');
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
    this.title = title;
    this.type = type;
    this.tenantId = tenantId;
    this.hasAutomaticId = hasAutomaticId;
    this.isConfiguration = isConfiguration;
  }
}

export class GetComputerGeneratedWorkflows extends ApiAction {
  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
  workflowTitle: string;
  typicalId: string;
  typicalTitle: string;

  constructor(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string, workflowTitle: string, typicalId: string, typicalTitle: string) {
    super('GetComputerGeneratedWorkflows');
    this.tenantId = tenantId;
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
    this.workflowTitle = workflowTitle;
    this.typicalId = typicalId;
    this.typicalTitle = typicalTitle;
  }
}


export class UpdateTypicalManually extends ApiAction {
  id: string;
  title: string;

  constructor(id: string, title: string) {
    super('UpdateTypicalManually');
    this.id = id;
    this.title = title;
  }
}

export class DeleteTypicalManually extends ApiAction {
  Id: string;

  constructor(Id: string) {
    super('DeleteTypicalManually');
    this.Id = Id;
  }
}

export class SaveComputerGeneratedWorkflows extends ApiAction {
  workflows: any;

  constructor(workflows: any) {
    super('SaveComputerGeneratedWorkflows');
    this.workflows = workflows;
  }
}


export class RollbackWorkflow extends ApiAction {
  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
  workflowId: string;
  version: number;
  skipStatus: boolean;

  constructor(tenantId: string,  projectId: string, subProjectId: string, systemId: string,
    catalogId: string, workflowId: string, version: number, skipStatus: boolean) {
    super('RollbackWorkflow');
    this.tenantId = tenantId;
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.catalogId = catalogId;
    this.systemId = systemId;
    this.workflowId = workflowId;
    this.version = version;
    this.skipStatus = skipStatus;
  }
}

export class GetWorkflowVersion extends ApiAction {
  workflowId: string;
  version: number;

  constructor(workflowId: string, version: number) {
    super('GetWorkflowVersion');
    this.workflowId = workflowId;
    this.version = version;
  }
}

export class GetPreviousWorkflowItems extends ApiAction {
  currentId: string;
  designerJson: string;
  itemIds: string[];
  parenId: string;

  constructor(currentId: string, designerJson: string, parenId: string, selectedPathItemsIds: string[]) {
    super('GetPreviousWorkflowItems');
    this.currentId = currentId;
    this.designerJson = designerJson;
    this.itemIds = selectedPathItemsIds;
    this.parenId = parenId;
  }
}

export class GetWorkflowVersions extends ApiAction {
  workflowId: string;

  constructor(workflowId: string) {
    super('GetWorkflowVersions');
    this.workflowId = workflowId;
  }
}

export class HasVideo extends ApiAction {
  workflowId: string;

  constructor(workflowId: string) {
    super('HasVideo');
    this.workflowId = workflowId;
  }
}

export class GetSuggestions extends ApiAction {
  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
  currentItemId: string;
  json: string;

  constructor(
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    currentItemId: string,
    json: string
  ) {
    super('GetSuggestions');
    this.currentItemId = currentItemId;
    this.json = json;
    this.projectId = projectId;
    this.tenantId = tenantId;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
  }
}

export class GetWorkflowCoverage extends ApiAction {
  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
  workflowId: string;

  constructor(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string, workflowId: string) {
    super('GetWorkflowCoverage');
    this.projectId = projectId;
    this.tenantId = tenantId;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
    this.catalogId = catalogId;
    this.workflowId = workflowId;
  }
}

export class GetPaths extends ApiAction {
  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
  json: string;

  constructor(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string, json: string) {
    super('GetPaths');
    this.catalogId = catalogId;
    this.tenantId = tenantId;
    this.json = json;
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
  }
}

export class GetWorkflowByTitle extends ApiAction {
  tenantId: string;
  projectId: string;
  subProjectId: string;
  systemId: string;
  catalogId: string;
  title: string;

  constructor(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string, title: string) {
    super('GetWorkflowByTitle');
    this.catalogId = catalogId;
    this.tenantId = tenantId;
    this.title = title;
    this.projectId = projectId;
    this.subProjectId = subProjectId;
    this.systemId = systemId;
  }
}

export class GetSystemTags extends ApiAction {
  systemId: string;

  constructor(systemId: string) {
    super('GetSystemTags');
    this.systemId = systemId;
  }
}

export class AddSystemTag extends ApiAction {
  systemId: string;
  title: string;
  description: string;

  constructor(systemId: string, title: string, description: string) {
    super('AddSystemTag');
    this.systemId = systemId;
    this.title = title;
    this.description = description;
  }
}

export class UpdateSystemTag extends ApiAction {
  id: string;
  systemId: string;
  title: string;
  description: string;

  constructor(id: string, systemId: string, title: string, description: string) {
    super('UpdateSystemTag');
    this.id = id;
    this.systemId = systemId;
    this.title = title;
    this.description = description;
  }
}

export class DeleteSystemTag extends ApiAction {
  Id: string;

  constructor(id: string) {
    super('DeleteSystemTag');
    this.Id = id;
  }
}


export class AddHierarchyType extends ApiAction {
  parentId: string;
  subProjectId: string;
  title: string;
  description: string;

  constructor(parentId: string, subProjectId: string, title: string, description: string) {
    super('AddHierarchyType');
    this.parentId = parentId;
    this.subProjectId = subProjectId;
    this.title = title;
    this.description = description;
  }
}

export class UpdateHierarchyType extends ApiAction {
  id: string;
  parentId: string;
  subProjectId: string;
  title: string;
  description: string;

  constructor(id: string, parentId: string, subProjectId: string, title: string, description: string) {
    super('UpdateHierarchyType');
    this.id = id;
    this.parentId = parentId;
    this.subProjectId = subProjectId;
    this.title = title;
    this.description = description;
  }
}

export class GetHierarchyTypes extends ApiAction {
  subProjectId: string;

  constructor(subProjectId: string) {
    super('GetHierarchyTypes');
    this.subProjectId = subProjectId;
  }
}

export class DeleteHierarchyType extends ApiAction {
  id: string;

  constructor(id: string) {
    super('DeleteHierarchyType');
    this.id = id;
  }
}

export class GetAdapters extends ApiAction {
  constructor() {
    super('GetAdapters');
  }
}


export class GetTypicalAttributes extends ApiAction {
  typicalId: string;

  constructor(typicalId: string) {
    super('GetTypicalAttributes');
    this.typicalId = typicalId;
  }
}


export class AddTypicalAttributeManually extends ApiAction {
  name: string;
  typicalId: string;

  constructor(name: string, typicalId: string) {
    super('AddTypicalAttributeManually');
    this.name = name;
    this.typicalId = typicalId;
  }
}

export class UpdateTypicalAttributeManually extends ApiAction {
  name: string;
  typicalId: string;
  newName: string;

  constructor(name: string, typicalId: string, newName: string) {
    super('UpdateTypicalAttributeManually');
    this.name = name;
    this.typicalId = typicalId;
    this.newName = newName;
  }
}


export class DeleteTypicalAttributeManually extends ApiAction {
  name: string;
  typicalId: string;
  isTypicalChange: boolean;

  constructor(name: string, typicalId: string, isTypicalChange: boolean) {
    super('DeleteTypicalAttributeManually');
    this.name = name;
    this.typicalId = typicalId;
    this.isTypicalChange = isTypicalChange;
  }
}


