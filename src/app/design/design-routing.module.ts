import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {WorkflowToolbarComponent} from './components/workflow-toolbar/workflow-toolbar.component';
import {WorkFlowComponent} from './components/work-flow/work-flow.component';
import {AuthGuard} from '../shared/auth.guards';
import {ProjectSelectionGuard} from '../shared/project-selection.guards';

const routes: Routes = [
  {
    path: 'workflow-designer',
    component: WorkFlowComponent,
    canActivate: [AuthGuard, ProjectSelectionGuard]
  },
  {path: 'app-workflow-toolbar', component: WorkflowToolbarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DesignRoutingModule {
}
