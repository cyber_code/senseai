import {Attribute} from './attribute.model';

export class Typical {
  id: string;
  title: string;
  attributes: Attribute[];

  /**
   *
   */
  constructor(id: string, title: string, attributes: Attribute[]) {
    this.id = id;
    this.title = title;
    this.attributes = attributes;
  }
}
