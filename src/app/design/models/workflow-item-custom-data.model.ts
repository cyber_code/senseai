import {Typical} from './typical.model';
import {WorkflowItemActionType} from '../../core/models/workflow-item-action-type';
import {WorkflowItemType} from '../../core/models/workflow-item-type';
import {Catalog} from './catalog.model';
import {Guid} from 'src/app/shared/guid';
import {System} from 'src/app/core/models/system.model';

export class WorkflowItemCustomData {
  Type: WorkflowItemType; // action, start, link
  Id: string;
  SystemId: string;
  AdapterName: string;
  Title: string;
  IsTitleChanged: boolean;
  Description: string;
  CatalogId: string;
  CatalogName: string;
  TypicalId: string;
  TypicalName: string;
  TypicalType: string;
  ActionType: WorkflowItemActionType; // application, enquiry, navigatin etc etc
  Parameters: string;
  DataSetsItemIds: string[];
  DynamicDatas: DynamicData[];
  Constraints: Constraint[];
  PostConstraints: Constraint[];
  IsNegativeStep: boolean;
  ExpectedError: string;
  isExpanded: Boolean;
  WorkflowId: string;
  refId: string;
  linkedParentId: string;
  parentIds: any;
  Role?: string;
  Action: string;


  setSystem(system: System) {
    this.SystemId = system.id || Guid.empty;
    this.AdapterName = system.adapterName;
  }

  getSystem() {
    return {
      id: this.SystemId || Guid.empty,
      adapterName: this.AdapterName
    } as System;
  }

  getCatalog() {
    return {
      id: this.CatalogId,
      title: this.CatalogName
    } as Catalog;
  }

  setCatalog(catalog: Catalog) {
    this.CatalogId = catalog.id || Guid.empty;
    this.CatalogName = catalog.title;
    this.TypicalType = catalog.type;
  }

  setTypical(typical: Typical) {
    this.TypicalId = typical.id || Guid.empty;
    this.TypicalName = typical.title;
  }

  getTypical() {
    return {
      id: this.TypicalId,
      title: this.TypicalName
    } as Typical;
  }
}

export class Constraint {
  AttributeName: string;
  Operator: string;
  AttributeValue: string;
  IsPostFilter: boolean;
  DynamicData: DynamicData;
}

export class DynamicData {
  SourceWorkflowItemId: string;
  SourceAttributeName: string;
  TargetAttributeName: string;
  FormulaText: string;
  Path: string;
  PathName: string;
}

export class DynamicDataSuggested {
  SourceTypicalName: string;
  SourceAttributeName: string;
  TargetTypicalName: string;
  TargetAttributeName: string;
}

