export class Attribute {
  Name: string;
  DataType: string;
  Status: boolean;
  DefValue: string;
  MinValue: string;
  MaxValue: string;
  Length: string;
}
