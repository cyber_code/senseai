export interface RappidConnector {
  attrs: Attrs;
  connector: ConnectorOrRouter;
  id: string;
  router: ConnectorOrRouter;
  source: SourceOrTarget;
  target: SourceOrTarget;
  type: string;
  z: number;
}

export interface Attrs {
  line: Line;
}

export interface Line {
  fill: string;
  sourceMarker: SourceMarkerOrTargetMarker;
  stroke: string;
  strokeDasharray: string;
  strokeWidth: number;
  targetMarker: SourceMarkerOrTargetMarker;
}

export interface SourceMarkerOrTargetMarker {
  d: string;
  stroke: string;
  type: string;
}

export interface ConnectorOrRouter {
  name: string;
}

export interface SourceOrTarget {
  id: string;
}

const x = {
  attrs: {
    line: {
      fill: 'none',
      sourceMarker: {
        d: 'M 0 0 0 0',
        stroke: 'none',
        type: 'path'
      },
      stroke: '#8f8f8f',
      strokeDasharray: '0',
      strokeWidth: 2,
      targetMarker: {
        d: 'M 0 -5 -10 0 0 5 z',
        stroke: 'none',
        type: 'path'
      }
    }
  },
  connector: {
    name: 'rounded'
  },
  id: '28435b23-17e3-47a9-84a6-7e100f433b42',
  router: {
    name: 'normal'
  },
  source: {
    id: '74d4cb4f-2277-499f-9007-3b31320752d7'
  },
  target: {
    id: '99d576ef-71dc-4b5c-963d-05d21fa1b583'
  },
  type: 'link',
  z: 9
};

const newConnector = Object.assign({}, x, {} as RappidConnector);
