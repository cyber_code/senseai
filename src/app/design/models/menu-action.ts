export enum MenuAction {
  NewFolder,
  NewWorkflow,
  Edit,
  Rename,
  Copy,
  Paste,
  Cut,
  Delete,
  Save,
  Cancel,
  Issue,
  Versions,
  ViewCoverage,
  DownloadVideo
}
