import {NodeType} from 'src/app/shared/node-type';
import {MenuAction} from './menu-action';

export class MenuItem {
  types: NodeType[];
  menuAction: MenuAction;
  text: string;
  isActive: boolean;

  constructor(types: NodeType[], menuAction: MenuAction, isActive: boolean) {
    this.types = types;
    this.menuAction = menuAction;
    this.text = MenuAction[menuAction].replace(/([A-Z])/g, ' $1').trim();
    this.isActive = isActive;
  }
}
