export class Workflow {
  id: string;
  title: string;
  designerJson: string;
  pathsJson: string;
  linkedWorkflows: Object;
  isParsed: boolean;
  status: number;
  isIssued: boolean;
  generatedTestCases: boolean;
  versionId: number;
}
