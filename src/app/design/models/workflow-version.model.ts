export class WorkflowVersion {
  workflowId: string;
  version: number;
  dateCreated: string;
  title: string;
  description: string;
  isLast: boolean;
  designerJson: string;
  pathsJson: string;
  workflowImage: string;
}
