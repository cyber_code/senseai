import {WorkflowItemCustomData} from './workflow-item-custom-data.model';
import {WorkflowItemActionType} from '../../core/models/workflow-item-action-type';

export class WorkflowItem {
  id: string;
  text: string;
  customData: WorkflowItemCustomData;
  workflowItemActionType: WorkflowItemActionType;
  startWorkflowItemCustomData: WorkflowItemCustomData;
  previousWorkflowItemCustomData: WorkflowItemCustomData;

  constructor(
    id: string,
    text: string,
    customData: WorkflowItemCustomData,
    startWorkflowItemCustomData: WorkflowItemCustomData,
    previousWorkflowItemCustomData?: WorkflowItemCustomData
  ) {
    this.id = id;
    this.text = text;
    this.customData = customData;
    this.workflowItemActionType = customData.ActionType;
    if (customData.Title === '') {
      customData.Title = text;
    }
    this.customData.Id = id;
    this.startWorkflowItemCustomData = startWorkflowItemCustomData;
    this.previousWorkflowItemCustomData = previousWorkflowItemCustomData;
  }
}
