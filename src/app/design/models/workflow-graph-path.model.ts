export class WorkflowGraphPath {
  number: number;
  title: string;
  coverage: number;
  isBestPath: boolean;
  selected: Boolean;
  itemIds: string[];
}
