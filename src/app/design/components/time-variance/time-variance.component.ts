import {DesignService} from './../../services/design.service';
import {Component, OnInit, ChangeDetectionStrategy, ViewChild, AfterViewInit, Input, Output, EventEmitter} from '@angular/core';
import {TreeViewItem} from '../../../shared/tree-view-item';
import {NodeType} from '../../../shared/node-type';
import {DomSanitizer} from '@angular/platform-browser';
import {WorkflowVersion} from 'src/app/design/models/workflow-version.model';
import {AppTreViewComponent} from 'src/app/core/components/tree-view/app-tree-view.component';
import {ActivatedRoute, Router} from '@angular/router';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-compare-workflow',
  templateUrl: './time-variance.component.html',
  styleUrls: ['./time-variance.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimeVarianceComponent implements OnInit, AfterViewInit {
  @ViewChild(AppTreViewComponent)
  public treeViewComponent: AppTreViewComponent;

  private _selectedTreeViewItem: TreeViewItem;
  @Output() public closeTimeVariance = new EventEmitter<any>();

  @Input()
  set selectedTreeViewItem(selectedTreeViewItem: TreeViewItem) {
    this._selectedTreeViewItem = selectedTreeViewItem;
    this.treeViewComponent.selectWfItemAndExpand(selectedTreeViewItem.id);
  }
  public dateTimeRange: Date[];
  public workflowVersions: WorkflowVersion[] = [];

  constructor(
    private router: Router,
    private domSanitizer: DomSanitizer,
    private designService: DesignService,
    private route: ActivatedRoute,
    public permissionService: PermissionService
  ) {
  }

  treeViewItemSelected(treeViewItem: TreeViewItem) {
    if (treeViewItem.type !== NodeType.Workflow) {
      this.workflowVersions = [];
      return;
    }
    this._selectedTreeViewItem = treeViewItem;
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
  }

  goBack() {
    this.closeTimeVariance.emit();
  }

  openZoom(event: any) {
    event.preventDefault();
  }

  valueChange($event) {
  }

  pickerClosed() {
    if (this.dateTimeRange && this.dateTimeRange.length > 0) {
      const fromDate = this.dateTimeRange[0].toJSON();
      const toDate = this.dateTimeRange[1] && this.dateTimeRange[1].toJSON();
      if(this.permissionService.hasPermission('/api/Design/GetWorkflowVersionsByDate')){
        this.designService.getWorkflowVersionsByDate(this._selectedTreeViewItem.id, fromDate, toDate).subscribe(result => {
          result.map(x => {
            x.workflowImage = this.domSanitizer.bypassSecurityTrustUrl(x.workflowImage) as string;
            return x;
          });
          this.workflowVersions = result;
          (document.getElementById('searchWorkflow') as HTMLElement).focus();
        });
      }
      else {
        this.workflowVersions = [];
      }
    }
  }
}
