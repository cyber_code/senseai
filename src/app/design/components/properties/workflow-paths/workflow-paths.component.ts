import { AutoUnsubscribe, takeWhileAlive } from 'take-while-alive';
import { Component, OnInit, ViewChild } from '@angular/core';

import { DesignService } from '../../../services/design.service';
import { SessionService } from 'src/app/core/services/session.service';
import { WorkflowItemService } from '../../../services/workflow-item.service';
import {WorkflowService} from '../../../services/workflow.service';
import { WorkflowGraphPath } from 'src/app/design/models/workflow-graph-path.model';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-workflow-paths',
  templateUrl: './workflow-paths.component.html',
  styleUrls: ['./workflow-paths.component.css']
})
@AutoUnsubscribe()
export class WorkflowPathsComponent implements OnInit {
  public paths: WorkflowGraphPath[];
  public allpaths: WorkflowGraphPath[];
  public selectedPaths: WorkflowGraphPath[];
  public value: any = [];
  public disabled : Boolean = false; 

  @ViewChild('msPaths') msPaths;

  constructor(
    private designService: DesignService,
    private sessionService: SessionService,
    private workflowItemService: WorkflowItemService,
    private workflowService: WorkflowService,
    public permissionService: PermissionService
  ){

    
      this.workflowItemService.loadSuggestedPaths$.pipe(takeWhileAlive(this)).subscribe(result => {
        const graph = this.workflowService.getGraphToSave(JSON.parse(result.graph ? result.graph : result));
        const graphJson = JSON.stringify(graph);
        const workContext = this.sessionService.getWorkContext();
        this.paths = [];
        if(this.permissionService.hasPermission('/api/Design/GetPaths')){
          this.designService
          .getPaths(
            workContext.user.tenantId,
            workContext.project.id,
            workContext.subProject.id,
            workContext.defaultSettings.system.id,
            workContext.defaultSettings.catalog.id,
            graphJson
          )
          .subscribe((paths: any) => {
            let shouldApplyDynamicData = this.allpaths.length !== paths.length;
            this.allpaths = paths;
            this.selectedPaths = result.loadWorkflow ? paths.filter(x => result.paths.find(path => path.selected === true && path.number === x.number)) : 
            paths.filter(path => this.selectedPaths.find(x => x.number === path.number));
            if (!result.loadWorkflow && (!this.selectedPaths || this.selectedPaths.length === 0) && paths && paths.length > 0) {
              this.selectedPaths = [paths[0]];
            }
            this.selectedPaths.forEach(x => (x.selected = true));
            this.workflowItemService.workflowPaths = paths;
            this.workflowItemService.workflowHighlight.next(this.selectedPaths);
            this.populatePathsDataSource(paths);
            this.workflowItemService.addDynamicDataForLinkedElCheck.next(paths);
            if (shouldApplyDynamicData) {
              this.workflowItemService.addDynamicDataForLinkedEl.next(paths);
            }
          });
        }
        else {
          this.allpaths = [];
          this.selectedPaths = [];
          this.workflowItemService.workflowPaths = [];
          this.workflowItemService.workflowHighlight.next(this.selectedPaths);
          this.populatePathsDataSource([]);
          this.workflowItemService.addDynamicDataForLinkedElCheck.next([]);
        }
      });
      this.workflowItemService.workflowPathsLoaded$.pipe(takeWhileAlive(this)).subscribe(paths => {
        this.allpaths = paths;
        this.selectedPaths = this.allpaths.filter(x => x.selected === true);
        if ((!this.selectedPaths || this.selectedPaths.length === 0) && paths && paths.length > 0) {
          this.selectedPaths = [paths[0]];
        }
        this.workflowItemService.workflowPaths = paths;
        this.workflowItemService.workflowHighlight.next(this.selectedPaths);
        this.populatePathsDataSource(paths);
      });
    }

  ngOnInit() {}

  private populatePathsDataSource(paths: WorkflowGraphPath[]) {
    this.paths = [...paths];
  }

  valueChanged(newValue: WorkflowGraphPath[]) {
    this.workflowItemService.workflowPaths.forEach(element => {
      element.selected = false;
      if (newValue.find(x => x.number === element.number)) {
        element.selected = true;
      }
    });
    this.workflowItemService.workflowHighlight.next(newValue);
  }
}
