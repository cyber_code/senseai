import {Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {DesignService} from '../../../services/design.service';
import {WorkflowVersion} from 'src/app/design/models/workflow-version.model';
import {WorkflowItemService} from '../../../services/workflow-item.service';
import {isNullOrUndefined} from 'util';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import { PermissionService } from 'src/app/core/services/permission.service';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-workflow-version',
  templateUrl: './workflow-version.component.html',
  styleUrls: ['./workflow-version.component.css']
})
@AutoUnsubscribe()
export class WorkflowVersionComponent implements OnInit {
  public workflowVersions: WorkflowVersion[] = [];
  public selectedWorkflowVersion: WorkflowVersion = {} as WorkflowVersion;
  previousWorkflowVersion: any;
  public rollbackDialogOpen: Boolean = false;
  public versionsDisabled: Boolean = false;
  public noRightsDialogOpen: Boolean = false;

  constructor(private designService: DesignService, 
              private router: Router, private workflowService: WorkflowItemService,
              public permissionService: PermissionService,
              private sessionService: SessionService
              ) {

    
    this.workflowService.workflowSelectionChanged$.pipe(takeWhileAlive(this)).subscribe(workflowId => {
      this.selectedWorkflowVersion = {} as WorkflowVersion;
      this.loadWorkflowVersions(workflowId);
    });

    this.workflowService.rollbackWorkflow.pipe(takeWhileAlive(this)).subscribe(selectedWorkflowVersion => {
      this.selectedWorkflowVersion = selectedWorkflowVersion;
      this.rollback(true);
    });

    this.workflowService.closeRollbackDialog.pipe(takeWhileAlive(this)).subscribe(() => {
      this.close();
    });
  }

  ngOnInit() {
  }

  rollback(skipStatus) {
    const context = this.sessionService.getWorkContext();
    this.designService.rollbackWorkflow(
      context.user.tenantId,
      context.project.id,
      context.subProject.id,
      context.defaultSettings.system.id,
      context.defaultSettings.catalog.id,
      this.selectedWorkflowVersion.workflowId, 
            this.selectedWorkflowVersion.version, 
            skipStatus).subscribe((result : any) => {
      this.workflowService.workflowVersionChanged.next({
        response: result && result.response,
        selectedVersion: this.selectedWorkflowVersion
      });
      this.loadWorkflowVersions(this.selectedWorkflowVersion.workflowId);
    });
    this.rollbackDialogOpen = false;
  }

  open(version) {
    this.previousWorkflowVersion = this.selectedWorkflowVersion;
    this.selectedWorkflowVersion = version;
    if(this.permissionService.hasPermission('/api/Design/RollbackWorkflow')) {
      this.rollbackDialogOpen=true; 
    }
    else this.noRightsDialogOpen = true;
  }

  close() {
    this.rollbackDialogOpen = false;
    this.selectedWorkflowVersion = this.previousWorkflowVersion;
  }

  closeInfo(){
    this.noRightsDialogOpen=false;
  }
  
  loadWorkflowVersions(workflowId: string) {
    if(this.permissionService.hasPermission('/api/Design/GetWorkflowVersions') && !isNullOrUndefined(workflowId)) {
      this.designService.getWorkflowVersions(workflowId).subscribe(result => {
        result.map(x => {
          x['versionText'] = `Version : ${x.version}`;
        });
        this.workflowVersions = result;
        this.versionsDisabled = false;
      });
    }
    else {
      this.workflowVersions = [];
      this.versionsDisabled = true;
    }
  }
}
