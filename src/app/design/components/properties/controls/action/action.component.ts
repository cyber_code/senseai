import {WorkflowItemActionType} from '../../../../../core/models/workflow-item-action-type';
import {WorkflowItemService} from '../../../../services/workflow-item.service';
import {WorkflowItemCustomData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {AutoUnsubscribe, takeWhileAlive} from 'take-while-alive';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
@AutoUnsubscribe()
export class ActionComponent implements OnInit {
  public actions: any[] = [
    {id: WorkflowItemActionType.Authorise, title: 'Authorise'},
    {id: WorkflowItemActionType.Reverse, title: 'Reverse'},
    {id: WorkflowItemActionType.Delete, title: 'Delete'},
    {id: WorkflowItemActionType.See, title: 'See'},
    {id: WorkflowItemActionType.Verify, title: 'Verify'},
    {id: WorkflowItemActionType.Validate, title: 'Validate'}
  ];
  public selectedAction: any = {};
  public workflowItemData: WorkflowItemCustomData;
  public readonly: boolean;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });
  }

  ngOnInit() {
    const action = this.actions.find(x => x.id === this.workflowItemData.ActionType);
    this.selectedAction = action;
  }

  actionTypeChanged(newAction: any) {
    this.selectedAction = this.actions.find(x => x.id === newAction.id);
    this.workflowItemData.ActionType = this.selectedAction.id;
    this.workflowItemService.changeActionType(this.selectedAction.id);
  }
}
