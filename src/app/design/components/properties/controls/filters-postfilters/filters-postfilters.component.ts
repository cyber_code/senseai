import {DynamicData} from './../../../../models/workflow-item-custom-data.model';
import {Attribute} from '../../../../models/attribute.model';
import {WorkflowItemCustomData, Constraint} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit, ViewChild, AfterViewInit, ViewContainerRef, Input} from '@angular/core';
import {DesignService} from 'src/app/design/services/design.service';
import {WorkflowItemService, JsonNeededBy} from 'src/app/design/services/workflow-item.service';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {from, of} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {MessageService} from 'src/app/core/services/message.service';
import {MessageType} from 'src/app/core/models/message.model';
import {GridComponent} from '@progress/kendo-angular-grid';
import {WorkflowItemActionType} from 'src/app/core/models/workflow-item-action-type';
import {AutoUnsubscribe, takeWhileAlive} from 'take-while-alive';
import { WorkflowService } from 'src/app/design/services/workflow.service';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-filters-postfilters',
  templateUrl: './filters-postfilters.component.html',
  styleUrls: ['./filters-postfilters.component.css']
})
@AutoUnsubscribe()
export class FiltersPostFiltersComponent implements OnInit, AfterViewInit {
  public workflowItemData: WorkflowItemCustomData;
  public readonly: boolean;
  public readonlyPostFilter: boolean;
  // this is for now, because later we can use server side filtering in order to not load all the typicals
  public operators: string[];
  public filterOperator: string;
  public filterValue: string;
  public ispostfilter: Boolean = false;
  editedRowIndex: any;
  public dynamicdataindex: number;
  public selectedAttribute: Attribute;
  public attributes: Attribute[];
  public editedFilter: Constraint;
  public opened: Boolean = false;
  private startItemCustomData: WorkflowItemCustomData;
  public constrains: Constraint[] = [];
  public graphJson;
  public selecteFilter: any;
  public selectedPath = '';
  public initialAttributes: Attribute[];
  @ViewChild('grid') grid: GridComponent;
  @ViewChild('cmbAttribute') cmbAttribute;
  @ViewChild('chkbPostFilter') chkbPostFilter;
  @ViewChild('appendTo', {read: ViewContainerRef}) public appendTo: ViewContainerRef;
  @Input() cells;

  constructor(
    private designService: DesignService,
    private workflowItemService: WorkflowItemService,
    private messageService: MessageService,
    private workflowService: WorkflowService,
    public permissionService: PermissionService
  ) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;

    this.constrains = [...this.workflowItemData.Constraints, ...this.workflowItemData.PostConstraints];
    this.startItemCustomData = this.workflowItemService.workflowItemSelected.startWorkflowItemCustomData;
    const selectedPaths = this.workflowItemService.workflowPaths.filter(x => x.selected === true);

    if (selectedPaths.length === 1) {
      this.selectedPath = selectedPaths[0].title;
    }
    this.readonly = !this.workflowItemService.editable;
    this.readonlyPostFilter = this.readonly;

    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
      this.readonlyPostFilter = this.readonly;
    });

    this.workflowItemService.closedDynamicDataPopup$.pipe(takeWhileAlive(this)).subscribe(res => {
      this.close();
    });

    this.workflowItemService.jsonLoaded$.pipe(takeWhileAlive(this)).subscribe(result => {
      if (result.neededBy === JsonNeededBy.Filters) {
        this.opened = true;
        this.graphJson = result.json;
      }
    });
  }

  ngOnInit() {
    let currentCell = this.cells.find(cell => cell.attributes.id === this.workflowItemService.workflowItemSelected.id);
    let startEl = this.workflowService.getLastStateItem(currentCell, {cells: this.cells.map(cell => cell.attributes)});
    if(this.permissionService.hasPermission('/api/Design/GetConditionItem')){
      this.designService.getConditionItem(startEl['AdapterName'] || 'T24WebAdapter').subscribe(result => {
        this.operators = result.operators;
      });
    }
    else {
      this.operators = [];
    }
    this.fillAttributes(false);
  }

  public fillAttributes(enable) {
    if(this.permissionService.hasPermission('/api/Design/SearchAttributesByCatalogIdAndTypicalName')){
      this.designService
      .SearchAttributesByCatalogIdAndTypicalName('', this.workflowItemData.CatalogId, this.getTypicalName())
      .subscribe(result => {
        if (enable) {
          this.readonlyPostFilter = false;
        }
        this.initialAttributes = result.slice(0, 20);
        this.populateAttributeDataSource(this.initialAttributes);
      });

    }
    else {
      if (enable) {
        this.readonlyPostFilter = false;
      }
      this.initialAttributes = [];
      this.populateAttributeDataSource(this.initialAttributes);
    }
  }

  public isFilterClicked() {
    if (
      this.workflowItemData.ActionType === WorkflowItemActionType.EnquiryResult ||
      this.workflowItemData.ActionType === WorkflowItemActionType.EnquiryAction
    ) {
      this.readonlyPostFilter = true;
      this.selectedAttribute = undefined;
      this.fillAttributes(true);
    }
  }

  public applyDynamicData(res: any) {
    if (!isNullOrUndefined(this.dynamicdataindex)) {
      this.constrains[this.dynamicdataindex].DynamicData = {
        Path: res.name,
        SourceWorkflowItemId: res.SourceWorkflowItemId,
        SourceAttributeName: res.SourceAttributeName,
        TargetAttributeName: this.constrains[this.dynamicdataindex].AttributeName,
        FormulaText: '',
        PathName: this.selectedPath
      };
      this.constrains[this.dynamicdataindex].AttributeValue = '';
      this.close();
    }
  }

  ngAfterViewInit() {
    this.cmbAttribute.filterChange
      .asObservable()
      .pipe(
        tap(() => (this.cmbAttribute.loading = true)),
        // wait 300ms after each keystroke before considering the term
        debounceTime(300),
        // ignore new term if same as previous term
        distinctUntilChanged(),
        // switch to new search observable each time the term changes
        switchMap((newfilter: string) => {
          if (newfilter === '' || newfilter.length < 3) {
            //this.cmbAttribute.toggle(false);
            return of(this.initialAttributes);
          }
          if(this.permissionService.hasPermission('/api/Design/SearchAttributesByCatalogIdAndTypicalName')){
            return this.designService.SearchAttributesByCatalogIdAndTypicalName(
              newfilter,
              this.workflowItemData.CatalogId,
              this.getTypicalName()
            );
          }
          else return [] ;
        })
      )
      .subscribe(result => {
        this.populateAttributeDataSource(result);
        this.cmbAttribute.loading = false;
      });
  }

  private getTypicalName() {
    let typicalname = this.workflowItemData.TypicalName || '';
    if (
      !this.ispostfilter &&
      (this.workflowItemData.ActionType === WorkflowItemActionType.EnquiryResult ||
        this.workflowItemData.ActionType === WorkflowItemActionType.EnquiryAction)
    ) {
      typicalname = this.workflowItemData.TypicalName.split('.RESULT')[0];
    } else if (
      this.ispostfilter &&
      (this.workflowItemData.ActionType === WorkflowItemActionType.EnquiryResult ||
        this.workflowItemData.ActionType === WorkflowItemActionType.EnquiryAction) &&
        !typicalname.includes('.RESULT')
    ) {
      typicalname = this.workflowItemData.TypicalName.concat('.RESULT');
    }
    return typicalname;
  }

  public editHandler({sender, rowIndex, dataItem}) {
    this.closeEditor(sender);

    this.editedRowIndex = rowIndex;
    this.editedFilter = Object.assign({}, dataItem);

    sender.editRow(rowIndex);
  }

  public cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({sender, rowIndex, dataItem, isNew}) {
    sender.closeRow(rowIndex);
    dataItem.IsPostFilter = dataItem.IsPostFilter || false;

    if (!isNullOrUndefined(dataItem.DynamicData.Path) && dataItem.DynamicData.Path !== '' && dataItem.AttributeValue !== '') {
      dataItem.AttributeValue = '';
      this.messageService.sendMessage({
        text: `Filter has dynamic data so can't have value`,
        type: MessageType.Error
      });
    }
    this.constrains[rowIndex] = dataItem;
    this.workflowItemData.Constraints = this.constrains.filter(item => item.IsPostFilter === false);
    this.workflowItemData.PostConstraints = this.constrains.filter(item => item.IsPostFilter === true);
    this.editedRowIndex = undefined;
    this.editedFilter = undefined;
  }

  public removeHandler({dataItem, rowIndex}) {
    this.constrains.splice(rowIndex, 1);
    this.workflowItemData.Constraints = this.constrains.filter(item => item.IsPostFilter === false);
    this.workflowItemData.PostConstraints = this.constrains.filter(item => item.IsPostFilter === true);
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.constrains[rowIndex] = this.editedFilter;
    this.workflowItemData.Constraints = this.constrains.filter(item => item.IsPostFilter === false);
    this.workflowItemData.PostConstraints = this.constrains.filter(item => item.IsPostFilter === true);
    this.editedRowIndex = undefined;
    this.editedFilter = undefined;
  }

  attributeChanged(newValue: Attribute = {} as Attribute) {
    this.selectedAttribute = newValue;
  }

  private populateAttributeDataSource(attributes: Attribute[]) {

    if (isNullOrUndefined(this.selectedAttribute) || isNullOrUndefined(this.selectedAttribute.Name)) {
      this.attributes = attributes;
    } else {
      const newItems = attributes.filter(x => x.Name !== this.selectedAttribute.Name);
      this.attributes = [this.selectedAttribute, ...newItems];
    }
  }

  addFilterClicked(event: any) {
    if (
      this.constrains.findIndex(
        e =>
          e.AttributeName === this.selectedAttribute.Name &&
          e.Operator === this.filterOperator &&
          e.AttributeValue === this.filterValue &&
          e.IsPostFilter === this.ispostfilter
      ) !== -1
    ) {
      this.messageService.sendMessage({
        text: `Filter already exists in filters`,
        type: MessageType.Error
      });
      return;
    }
    const constrain = {
      AttributeName: this.selectedAttribute.Name,
      Operator: this.filterOperator,
      AttributeValue: this.filterValue,
      IsPostFilter: this.ispostfilter,
      DynamicData: new DynamicData()
    } as Constraint;
    this.constrains.push(constrain);
    if (!this.ispostfilter) {
      this.workflowItemData.Constraints.push(constrain);
    } else {
      this.workflowItemData.PostConstraints.push(constrain);
    }
  }

  public addDynamicData(dataItem: any) {
    const selectedPaths = this.workflowItemService.workflowPaths.filter(x => x.selected === true);
    if (!selectedPaths || selectedPaths.length === 0) {
      this.messageService.sendMessage({
        type: MessageType.Warning,
        text: 'Please select one path in order to add dynamic data'
      });
      return;
    } else if (selectedPaths.length > 1) {
      this.messageService.sendMessage({
        type: MessageType.Warning,
        text: 'Please select only one path in order to add dynamic data'
      });
      return;
    }
    this.dynamicdataindex = this.constrains.findIndex(
      x =>
        x.AttributeName === dataItem.AttributeName &&
        x.Operator === dataItem.Operator &&
        x.AttributeValue === dataItem.AttributeValue &&
        x.IsPostFilter === dataItem.IsPostFilter
    );
    this.selecteFilter = dataItem;
    this.selecteFilter.Name = dataItem.AttributeName;
    this.workflowItemService.getGraphJson(JsonNeededBy.Filters);
  }

  public removeDynamicData(dataItem: any) {
    this.dynamicdataindex = this.constrains.findIndex(
      x =>
        x.AttributeName === dataItem.AttributeName &&
        x.Operator === dataItem.Operator &&
        x.AttributeValue === dataItem.AttributeValue &&
        x.IsPostFilter === dataItem.IsPostFilter
    );
    this.constrains[this.dynamicdataindex].DynamicData = new DynamicData();
  }

  public close() {
    this.opened = false;
  }
}
