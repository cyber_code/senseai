import {WorkflowItemCustomData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-description',
  templateUrl: './description.component.html',
  styleUrls: ['./description.component.css']
})
@AutoUnsubscribe()
export class DescriptionComponent implements OnInit {
  public workflowItemData: WorkflowItemCustomData;
  public readonly: boolean;
  public previousDescription: string;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });

    this.workflowItemService.closeEditWorkflowPopup.pipe(takeWhileAlive(this)).subscribe(action => {
      if (action === 'descriptionChanged') {
        this.changeDescription(this.previousDescription, false);
      }
    });
  }

  ngOnInit() {
  }

  changeDescription(description, trackChange) {
    if (trackChange) {
      this.previousDescription = this.workflowItemData.Description;
      this.workflowItemService.changeDescription(description);
    }
    this.workflowItemData.Description = description;
  }
} 
