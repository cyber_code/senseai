import {WorkflowItemCustomData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
@AutoUnsubscribe()
export class TitleComponent implements OnInit {
  public workflowItemData: WorkflowItemCustomData;
  public previousTitle: string;
  public readonly: boolean;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;

    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });

    this.workflowItemService.closeEditWorkflowPopup.pipe(takeWhileAlive(this)).subscribe(action => {
      if (action === 'titleChanged') {
        this.titleChanged(this.previousTitle, false);
      }
    });
  }

  ngOnInit() {
  }

  titleChanged(newValue: string, trackChanges: Boolean) {
    if (trackChanges) {
      this.previousTitle = this.workflowItemData.Title;
    }
    this.workflowItemService.workflowItemSelectedModified.next(
      {
        title: newValue,
        IsTitleChanged: true,
        trackChanges: trackChanges
      }
    );
  }
}
