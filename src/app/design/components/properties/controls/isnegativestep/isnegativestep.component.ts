import {WorkflowItemCustomData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-isnegativestep',
  templateUrl: './isnegativestep.component.html',
  styleUrls: ['./isnegativestep.component.css']
})
@AutoUnsubscribe()
export class IsNegativeStepComponent implements OnInit {
  public workflowItemData: WorkflowItemCustomData;
  public readonly: boolean;

  // this is for now, because later we can use server side filtering in order to not load all the typicals

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });
  }

  ngOnInit() {
  }

  chkbNegativeChanged(event) {
    this.workflowItemService.isNegativeStepChanged.next(event.target.checked);
  }
}
