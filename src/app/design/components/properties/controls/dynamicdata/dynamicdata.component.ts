import { Attribute } from '../../../../models/attribute.model';
import { WorkflowItemCustomData, Constraint } from '../../../../models/workflow-item-custom-data.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DesignService } from 'src/app/design/services/design.service';
import { WorkflowItemService, JsonNeededBy } from 'src/app/design/services/workflow-item.service';
import { isNullOrUndefined } from 'util';
import { State, process } from '@progress/kendo-data-query';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent, GridComponent } from '@progress/kendo-angular-grid';
import { takeWhileAlive, AutoUnsubscribe } from 'take-while-alive';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-dynamicdata',
  templateUrl: './dynamicdata.component.html',
  styleUrls: ['./dynamicdata.component.css']
})
@AutoUnsubscribe()
export class DynamicDataComponent implements OnInit {
  public workflowItemData: WorkflowItemCustomData;
  public readonly: boolean;
  public attributes: GridDataResult;
  public allattributes: Attribute[];
  public modes: any[] = [{ id: 0, title: 'Not Defined' }, { id: 1, title: 'Simple' }, { id: 2, title: 'Advanced' }];
  public filtermode: any = { id: 0, title: 'Not Defined' };
  public dynamicdatas: any[] = [];
  public opened = false;
  public constant: string;
  editedRowIndex: any;
  public pagesize: 10;
  public skip = 0;
  public selectedAttribute: any;
  public editedFilter: Constraint;
  public dataItemToDelete: any;
  @ViewChild('grid') grid: GridComponent;
  // this is for now, because later we can use server side filtering in order to not load all the typicals
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: [] // [{ field: 'ProductName', operator: 'contains', value: 'Chef' }]
    }
  };
  public selectedPath = '';
  graphJson: string;

  constructor(private designService: DesignService, 
    private workflowItemService: WorkflowItemService,
    public permissionService: PermissionService) {
    if (this.workflowItemService.workflowItemSelected) {
      this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
      this.filtermode.id = 0;
      this.readonly = !this.workflowItemService.editable;
      const selectedPaths = this.workflowItemService.workflowPaths.filter(x => x.selected === true);
      if (selectedPaths.length === 1) {
        this.selectedPath = selectedPaths[0].title;
      }
      this.dynamicdatas = this.workflowItemData.DynamicDatas ? this.workflowItemData.DynamicDatas.filter(x => x.PathName === this.selectedPath) : [];
      this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
        this.readonly = !editable;
        this.workflowItemService.editable = editable;
      });
      this.workflowItemService.closedDynamicDataPopup$.pipe(takeWhileAlive(this)).subscribe(() => {
        this.close();
      });
      this.workflowItemService.jsonLoaded$.pipe(takeWhileAlive(this)).subscribe(result => {
        if (result.neededBy === JsonNeededBy.DynamicData) {
          this.opened = true;
          this.graphJson = result.json;
        }
      });

      this.workflowItemService.continueBreakLinkage.pipe(takeWhileAlive(this)).subscribe((action) => {
        if (action === 'dynDeleted') {
          delete this.workflowItemData.linkedParentId;
          this.removeDynamic(this.dataItemToDelete);
        }
      })
    }
  }

  ngOnInit() {
    this.fillAttributes(this.workflowItemData.TypicalId);
  }

  public fillAttributes(typicalId: string) {
    this.grid.loading = true;
    if(this.permissionService.hasPermission('/api/Design/SearchAttributes')){
      this.designService.searchAttributes('', typicalId).subscribe(result => {
        this.populateAttributeDataSource(result, null);
        this.grid.loading = false;
      });
    }
    else {
      this.grid.loading = false;
      this.populateAttributeDataSource([], null);
    }
  }

  private populateAttributeDataSource(attributes: Attribute[], callback) {
    this.allattributes = attributes;
    this.allattributes.forEach(element => {
      const dyn = this.dynamicdatas.find(x => x.TargetAttributeName === element.Name);
      if (isNullOrUndefined(dyn)) {
        element.DefValue = '';
      } else {
        element.DefValue = dyn.Path;
      }
    });
    this.attributes = process(attributes, this.state);
    if (callback) callback();
  }

  public cellClick(dataItem: any) {
    // this.filtermode = { id: 0, title: 'Not Defined' };
    this.selectedAttribute = dataItem.dataItem;
  }

  public applyDynamicData(res: any) {
    const elem = this.dynamicdatas.find(x => x.TargetAttributeName === this.selectedAttribute.Name);
    if (isNullOrUndefined(elem)) {
      //const id = this.workflowItemData.DynamicDatas.length === 0 ? 0 : Math.max(...this.workflowItemData.DynamicDatas.map(x => x.Id)) + 1;
      this.workflowItemData.DynamicDatas.push({
        // Id: id,
        Path: res.name,
        SourceWorkflowItemId: res.SourceWorkflowItemId,
        SourceAttributeName: res.SourceAttributeName,
        TargetAttributeName: this.selectedAttribute.Name,
        FormulaText: '',
        PathName: this.selectedPath
      });
    } else {
      elem.Path = res.name;
      elem.SourceWorkflowItemId = res.SourceWorkflowItemId;
      elem.SourceAttributeName = res.SourceAttributeName;
    }
    this.dynamicdatas = this.workflowItemData.DynamicDatas.filter(x => x.PathName === this.selectedPath);
    this.attributes.data.find(x => x.Name === this.selectedAttribute.Name).DefValue = res.name;
    this.close();
  }

  public applyDynamicDataForSuggestions(res) {
    this.workflowItemService.workflowItemSelected = res.selectedItemData;
    this.workflowItemData = res.selectedItemData.customData;
    const bestPath = this.workflowItemService.workflowPaths.find(x => x.isBestPath === true);
    const elem = this.dynamicdatas.find(x => x.TargetAttributeName === res.TargetAttributeName);
    if(this.permissionService.hasPermission('/api/Design/SearchAttributes')){
      this.designService.searchAttributes('', this.workflowItemData.TypicalId).subscribe(result => {
        this.populateAttributeDataSource(result, () => {
          if (isNullOrUndefined(elem)) {
            this.workflowItemData.DynamicDatas.push({
              Path: res.name,
              SourceWorkflowItemId: res.SourceWorkflowItemId,
              SourceAttributeName: res.SourceAttributeName,
              TargetAttributeName: res.TargetAttributeName,
              FormulaText: '',
              PathName: bestPath.title
            });
          } else {
            this.workflowItemData.DynamicDatas.push({
              Path: res.name,
              SourceWorkflowItemId: res.SourceWorkflowItemId,
              SourceAttributeName: res.SourceAttributeName,
              TargetAttributeName: res.TargetAttributeName,
              FormulaText: '',
              PathName: bestPath.title
            });
            elem.Path = res.name;
            elem.SourceWorkflowItemId = res.SourceWorkflowItemId;
            elem.SourceAttributeName = res.SourceAttributeName;
          }
          this.dynamicdatas = this.workflowItemData.DynamicDatas.filter(x => x.PathName === bestPath.title);
          const attribute = this.allattributes.find(x => x.Name === res.TargetAttributeName);
          if (attribute) {
            this.allattributes.find(x => x.Name === res.TargetAttributeName).DefValue = res.name;
            this.attributes = process(this.allattributes, this.state);
          }
        });
      });

    }
    else {
      this.attributes = process( [] , this.state) 
    }
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.state.take = 10;
    this.state.filter = state.filter;
    this.attributes = process(this.allattributes, this.state);
  }

  public pageChange(event: PageChangeEvent): void {
    this.skip = event.skip;
    this.loadItems();
  }

  private loadItems(): void {
    this.state.take = 300000;
    const x = process(this.allattributes, this.state).data;
    this.attributes = {
      data: x.slice(this.skip, this.skip + 10),
      total: x.length
    };
  }

  // public editHandler({ sender, rowIndex, dataItem }) {
  //   this.closeEditor(sender);

  //   this.editedRowIndex = rowIndex;
  //   this.editedFilter = Object.assign({}, dataItem);

  //   sender.editRow(rowIndex);
  // }

  // public cancelHandler({ sender, rowIndex }) {
  //   this.closeEditor(sender, rowIndex);
  // }

  // public saveHandler({ sender, rowIndex, dataItem, isNew }) {
  //   sender.closeRow(rowIndex);
  //   dataItem.IsPostFilter = dataItem.IsPostFilter || false;
  //   this.dynamicdatas[rowIndex] = dataItem;
  //   this.editedRowIndex = undefined;
  //   this.editedFilter = undefined;
  // }
  public removeDynamic(dataItemObj) {
    //this.attributes.data[rowIndex]
    if (this.workflowItemData.linkedParentId) {
      this.dataItemToDelete = dataItemObj;
      this.workflowItemService.workflowLinkageBroken({
        itemIds: [ this.workflowItemData.linkedParentId ],
        action: 'dynDeleted'
      });
      return;
    }
    dataItemObj.dataItem.DefValue = '';
    const index = this.workflowItemData.DynamicDatas.findIndex(
      x => x.TargetAttributeName === dataItemObj.dataItem.Name && x.PathName === this.selectedPath
    );
    if (index >= 0) {
      this.workflowItemData.DynamicDatas.splice(index, 1);
    }
    this.dynamicdatas = this.workflowItemData.DynamicDatas.filter(x => x.PathName === this.selectedPath);
    this.workflowItemService.changedWorkflow.next();
  }

  // public removeHandler({ dataItem, rowIndex }) {
  //   this.dynamicdatas.splice(rowIndex, 1);
  // }
  // private closeEditor(grid, rowIndex = this.editedRowIndex) {
  //   grid.closeRow(rowIndex);
  //   this.dynamicdatas[rowIndex] = this.editedFilter;

  //   this.editedRowIndex = undefined;
  //   this.editedFilter = undefined;
  // }
  // public upClicked(dataItem: any) {
  //   const index = this.dynamicdatas.findIndex(x => x.Id === dataItem.Id);
  //   if (index === 0) {
  //     return;
  //   }
  //   const nextelem = this.dynamicdatas[index - 1];
  //   this.dynamicdatas[index] = nextelem;
  //   this.dynamicdatas[index - 1] = dataItem;
  // }
  // public downClicked(dataItem: any) {
  //   const index = this.dynamicdatas.findIndex(x => x.Id === dataItem.Id);
  //   if (index === this.dynamicdatas.length - 1) {
  //     return;
  //   }
  //   const nextelem = this.dynamicdatas[index + 1];
  //   this.dynamicdatas[index] = nextelem;
  //   this.dynamicdatas[index + 1] = dataItem;
  // }

  public addDataItem(dataItem: any) {
    this.selectedAttribute = this.attributes.data.find(x => x.Name === dataItem.Name);
    this.workflowItemService.getGraphJson(JsonNeededBy.DynamicData);
  }

  public close() {
    this.opened = false;
  }

  // public modeChanged(value: any) {
  //   this.filtermode = value;
  // }
  // public addOperand(value: any, operand: string) {
  //   const id = this.dynamicdatas.length === 0 ? 0 : Math.max(...this.dynamicdatas.map(x => x.Id)) + 1;
  //   this.dynamicdatas.push({ Id: id, AttributeName: operand, Type: 'Operand' });
  // }
}
