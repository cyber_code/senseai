import {WorkflowItemCustomData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-manual-action',
  templateUrl: './manual-action.component.html',
  styleUrls: ['./manual-action.component.css']
})
@AutoUnsubscribe()
export class ManualActionComponent implements OnInit {
  public workflowItemData: WorkflowItemCustomData;
  public previousAction: string;
  public readonly: boolean;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;

    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });

    this.workflowItemService.closeEditWorkflowPopup.pipe(takeWhileAlive(this)).subscribe(action => {
      if (action === 'actionChanged') {
        this.actionChanged(this.previousAction, false);
      }
    });
  }

  ngOnInit() {
  }

  actionChanged(newAction: string, trackChanges: Boolean) {
    if (trackChanges) {
        this.previousAction = this.workflowItemData.Action;
        this.workflowItemService.changeAction(newAction);
      }
      this.workflowItemData.Action = newAction;
  }
}
