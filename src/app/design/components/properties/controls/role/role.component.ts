import {WorkflowItemCustomData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
@AutoUnsubscribe()
export class RoleComponent implements OnInit {
  public workflowItemData: WorkflowItemCustomData;
  public previousRole: string;
  public readonly: boolean;

  constructor(private workflowItemService: WorkflowItemService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;

    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });

    this.workflowItemService.closeEditWorkflowPopup.pipe(takeWhileAlive(this)).subscribe(action => {
      if (action === 'roleChanged') {
        this.roleChanged(this.previousRole, false);
      }
    });
  }

  ngOnInit() {
  }

  roleChanged(newRole: string, trackChanges: Boolean) {
    if (trackChanges) {
        this.previousRole = this.workflowItemData.Role;
        this.workflowItemService.changeRole(newRole);
      }
      this.workflowItemData.Role = newRole;
  }
}
