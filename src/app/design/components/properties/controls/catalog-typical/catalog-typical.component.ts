import {Guid} from './../../../../../shared/guid';
import {SessionService} from 'src/app/core/services/session.service';
import {WorkflowItemCustomData, DynamicData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit, ViewChild, AfterViewInit, Output, EventEmitter, Input} from '@angular/core';
import {DesignService} from 'src/app/design/services/design.service';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {Catalog} from 'src/app/core/models/catalog.model';
import {Typical} from 'src/app/design/models/typical.model';
import {debounceTime, distinctUntilChanged, switchMap, tap, map} from 'rxjs/operators';
import {from, of} from 'rxjs';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import {Helper} from 'src/app/shared/helper';
import {ComboBoxComponent} from '@progress/kendo-angular-dropdowns';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-catalog-typical',
  templateUrl: './catalog-typical.component.html',
  styleUrls: ['./catalog-typical.component.css']
})
@AutoUnsubscribe()
export class CatalogTypicalComponent implements OnInit, AfterViewInit {
  public workflowItemData: WorkflowItemCustomData;
  public selectedCatalog: Catalog;
  public selectedTypical: Typical;
  public previousSelectedTypical: Typical;
  public previousSelectedCatalog: Catalog;
  public previousDynamicData: any;
  public catalogs: Catalog[];
  public readonly: boolean;

  @Output() public typicalChanged = new EventEmitter<Typical>();
  // this is for now, because later we can use server side filtering in order to not load all the typicals
  public allCatalogs: Catalog[];
  public typicals: Typical[];
  private initialTypicals: Typical[] = [];
  @ViewChild('cmbTypicals') cmbTypicals: ComboBoxComponent;
  @ViewChild('cmbCatalogs') cmbCatalogs: ComboBoxComponent;
  @Input() typicalLabel;

  constructor(
    private designService: DesignService,
    private sessionService: SessionService,
    private workflowItemService: WorkflowItemService,
    public permissionService: PermissionService
  ) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.selectedCatalog = this.workflowItemData.getCatalog();
    if (Guid.isEmpty(this.selectedCatalog.id)) {
      const defaultSettings = this.sessionService.getDefaultSettings();
      this.selectedCatalog = defaultSettings.catalog;
      this.workflowItemData.setCatalog(this.selectedCatalog);
    }
    this.selectedTypical = this.workflowItemData.getTypical();
    this.readonly = !this.workflowItemService.editable;

    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });

    this.workflowItemService.closeEditWorkflowPopup.pipe(takeWhileAlive(this)).subscribe(action => {
      if (action === 'typicalChanged') {
        this.selectedTypicalChanged(this.previousSelectedTypical, false, null);
        this.workflowItemData.DynamicDatas = this.previousDynamicData;
      } else if (action === 'catalogChanged') {
        this.selectedTypicalChanged(
          this.previousSelectedTypical,
          false,
          () => {
            this.catalogChanged(this.previousSelectedCatalog, false);
            this.workflowItemData.DynamicDatas = this.previousDynamicData;
          }
        );
      }
    })
  }

  private populateCatalogDataSource(catalogs: Catalog[]) {
    const newItems = catalogs.filter(x => x.id !== this.selectedCatalog.id);
    this.catalogs = [this.selectedCatalog, ...newItems];
  }

  private populateTypicalsDataSource(typicals: Typical[]) {
    const newItems = typicals.filter(x => x.id !== this.selectedTypical.id);
    this.typicals = [this.selectedTypical, ...newItems];
  }

  ngOnInit() {
    const subProject = this.sessionService.getWorkContext().subProject;
    const project = this.sessionService.getWorkContext().project;
    if(this.permissionService.hasPermission('/api/Administration/GetTenantCatalogs')){
      this.designService.getTenantCatalogs(project.id, subProject.id).subscribe(result => {
        this.allCatalogs = result;
        let currentCatalog = result.find(row =>row.id === this.selectedCatalog.id);
        if (currentCatalog && currentCatalog.title !== this.selectedCatalog.title){
          this.workflowItemData.setCatalog(currentCatalog);
          this.selectedCatalog = currentCatalog;
        }
        this.populateCatalogDataSource(result);
        if (this.permissionService.hasPermission('/api/Design/SearchTypicals')){
          this.designService.searchTypicals('', this.selectedCatalog.id).subscribe(res => {
            this.initialTypicals = res;
            this.populateTypicalsDataSource(res);
          });
        }
        else this.initialTypicals = [];
      });
    }
    else {
      this.allCatalogs = [];
      this.populateCatalogDataSource([]);
      this.initialTypicals = [];
      this.populateTypicalsDataSource([]);
    }
  }

  ngAfterViewInit() {
    this.cmbCatalogs.filterChange
      .asObservable()
      .pipe(
        switchMap((value: string) =>
          from([this.allCatalogs]).pipe(
            tap(() => (this.cmbCatalogs.loading = true)),
            map(data => {
              return data.filter(x => Helper.dataSourceFilter(x.title, value));
            })
          )
        )
      )
      .subscribe(filtered => {
        this.catalogs = filtered;
        this.cmbCatalogs.loading = false;
      });
    this.cmbTypicals.filterChange
      .asObservable()
      .pipe(
        tap((newfitler: string) => {
          if (newfitler && newfitler.length > 2) {
            this.cmbTypicals.loading = true;
          } else {
            this.cmbTypicals.toggle(false);
          }
        }),
        // wait 300ms after each keystroke before considering the term
        debounceTime(300),
        // ignore new term if same as previous term
        distinctUntilChanged(),
        // switch to new search observable each time the term changes
        switchMap((newfilter: string = '') => {
          if (newfilter.length < 3) {
            return of(this.initialTypicals);
          }
          if (this.permissionService.hasPermission('/api/Design/SearchTypicals')) { 
            return this.designService.searchTypicals(newfilter, this.selectedCatalog.id);
          }
          else return [];
        })
      )
      .subscribe(result => {
        this.populateTypicalsDataSource(result);
        this.cmbTypicals.loading = false;
      });
  }

  catalogChanged(newValue: Catalog = new Catalog(), trackChange: Boolean) {
    if (this.selectedCatalog.id === newValue.id || !newValue.id) {
      return;
    }
    if (trackChange) {
      this.previousSelectedCatalog = this.selectedCatalog;
      this.previousSelectedTypical = this.selectedTypical;
      this.previousDynamicData = this.workflowItemData.DynamicDatas;
    }
    this.selectedCatalog = newValue;
    this.workflowItemData.setCatalog(this.selectedCatalog);
    if (this.permissionService.hasPermission('/api/Design/SearchTypicals')) { 
      this.designService.searchTypicals('', this.selectedCatalog.id).subscribe(result => {
        if (trackChange) {
          let newTypical = new Typical(Guid.empty, '', []);
          this.selectedTypicalChanged(newTypical, true, () => {
            this.populateTypicalsDataSource(result);
            this.workflowItemService.changeCatalog(this.selectedCatalog);
          });
        } else {
          this.populateTypicalsDataSource(result);
        }
      });
    }
    else {
      if (trackChange) {
        let newTypical = new Typical(Guid.empty, '', []);
        this.selectedTypicalChanged(newTypical, true, () => {
          this.populateTypicalsDataSource([]);
          this.workflowItemService.changeCatalog(this.selectedCatalog);
        });
      } else {
        this.populateTypicalsDataSource([]);
      }
    }
  }

  selectedTypicalChanged(newValue: Typical = {} as Typical, trackChange: Boolean, callback) {
    if (this.selectedTypical.id === newValue.id || !newValue.id) {
      if (callback) {
        callback();
      }
      return;
    }
    if (trackChange) {
      this.previousSelectedTypical = this.selectedTypical;
      this.previousDynamicData = this.workflowItemData.DynamicDatas;
    }
    this.selectedTypical = newValue;
    this.workflowItemData.setTypical(this.selectedTypical);
    if (trackChange) {
      this.typicalChanged.emit(this.selectedTypical);
      this.workflowItemService.changeTypical(this.selectedTypical);
    } else {
      this.populateTypicalsDataSource(this.typicals);
    }

    if (callback) {
      callback();
    }
  }

  typicalFilterChanged(newfilter: string) {
    if(this.permissionService.hasPermission('/api/Design/SearchTypicals')){
      this.designService.searchTypicals(newfilter, this.selectedCatalog.id).subscribe(result => {
        this.populateTypicalsDataSource(result);
      });
    }
    else this.populateTypicalsDataSource([]);
  }
}
