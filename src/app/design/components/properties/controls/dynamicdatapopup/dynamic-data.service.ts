import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {DesignService} from 'src/app/design/services/design.service';
import {WorkflowService} from 'src/app/design/services/workflow.service';
import { Guid } from 'src/app/shared/guid';

@Injectable({
  providedIn: 'root'
})
export class DynamicDataService extends BehaviorSubject<any[]> {
  private data: any[] = [];
  constructor(
    private designService: DesignService,
    private workflowService: WorkflowService
  ) {
    super([]);
  }
  public read(itemId: string = '', json: string = '', parentId: string = Guid.empty, selectedPathItemsIds: string[] = []) {
    if (this.data.length) {
      return super.next(this.data);
    }
    if (!selectedPathItemsIds.find(id => itemId === id)) {
      return super.next([]);
    }

    this.fetch(itemId, json, parentId, selectedPathItemsIds).subscribe(data => {
      this.data = data;
      super.next(data);
    });
  }
  private fetch(itemId: string, json: string, parentId: string, selectedPathItemsIds: string[]): Observable<any[]> {
    const graph = this.workflowService.getGraphToSave(JSON.parse(json));
    const graphJson = JSON.stringify(graph);
    return this.designService.getPreviousWorkflowItems(itemId, graphJson, parentId, selectedPathItemsIds);
  }

  reset() {
    this.data = [];
  }
}
