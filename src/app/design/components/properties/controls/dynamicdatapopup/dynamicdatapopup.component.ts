import {Attribute} from '../../../../models/attribute.model';
import {WorkflowItemCustomData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit, ViewChild, AfterViewInit, Input, OnDestroy, Output, EventEmitter} from '@angular/core';
import {DesignService} from 'src/app/design/services/design.service';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {State, process} from '@progress/kendo-data-query';
import {DataStateChangeEvent, GridDataResult, GridComponent} from '@progress/kendo-angular-grid';
import {DynamicDataService} from './dynamic-data.service';
import {NotificationService} from '@progress/kendo-angular-notification';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import {of} from 'rxjs';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-dynamicdatapopup',
  templateUrl: './dynamicdatapopup.component.html',
  styleUrls: ['./dynamicdatapopup.component.css']
})
@AutoUnsubscribe()
export class DynamicDataPopupComponent implements OnInit, AfterViewInit, OnDestroy {
  public gridData: GridDataResult;
  @ViewChild('grid') grid: GridComponent;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };

  public workflowItemData: WorkflowItemCustomData;
  public readonly: boolean;

  public selectedTypical: string;
  public selectedrow: any;
  public attributes: Attribute[];
  public allattributes: Attribute[];
  public selectedAttribute: Attribute = new Attribute();
  public initialAttributes: Attribute[];
  @Output() applyDynamicData = new EventEmitter<any>();
  @Input() selectedTargetAttribute: any;

  @Input()
  public set graphJson(json: string) {
    if (json !== '') {
      const selectedPaths = this.workflowItemService.workflowPaths.filter(x => x.selected === true);
      if (selectedPaths && selectedPaths.length === 1) {
        const id = (this.workflowItemData.refId && this.workflowItemData.linkedParentId) ? this.workflowItemData.refId : this.workflowItemData.Id;
        this.dynamicDataService.read(id, json, this.workflowItemData.linkedParentId, selectedPaths[0].itemIds);
      }
    }
  }

  @ViewChild('cmbAttribute') cmbAttribute;

  constructor(
    private designService: DesignService,
    private workflowItemService: WorkflowItemService,
    private dynamicDataService: DynamicDataService,
    private notificationService: NotificationService,
    public permissionService: PermissionService
  ) {
    if (this.workflowItemService.workflowItemSelected) {
      this.readonly = !this.workflowItemService.editable;
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });
    }

    this.workflowItemService.continueBreakLinkage.pipe(takeWhileAlive(this)).subscribe((action) => {
      if (action === 'dynChanged') {
        delete this.workflowItemData.linkedParentId;
        this.apply();
      }
    })
    
  }

  ngOnInit() {
    this.grid.loading = true;
    this.dynamicDataService.subscribe(result => {
      if (result) {
        this.gridData = process(result, this.gridState);
      }
      this.grid.loading = false;
    });
  }

  ngAfterViewInit() {
    this.cmbAttribute.filterChange
      .asObservable()
      .pipe(
        tap(() => (this.cmbAttribute.loading = true)),
        // wait 300ms after each keystroke before considering the term
        debounceTime(300),
        // ignore new term if same as previous term
        distinctUntilChanged(),
        // switch to new search observable each time the term changes
        switchMap((newfilter: string) => {
          if (newfilter === '' || newfilter.length < 1) {
            //this.cmbAttribute.toggle(false);
            return of(this.initialAttributes);
          }
          return this.designService.searchAttributes(newfilter, this.selectedrow.typicalId);
        })
      )
      .subscribe(result => {
        this.populateAttributeDataSource(result);
        this.cmbAttribute.loading = false;
      });
  }

  ngOnDestroy(): void {
    this.dynamicDataService.reset();
  }

  public fillAttributes(typicalId: string) {
    this.designService.searchAttributes('', typicalId).subscribe(result => {
      this.initialAttributes = result.slice(0, 20);
      this.populateAttributeDataSource(this.initialAttributes);
      this.designService.getDynamicDataSuggestions(this.selectedTypical, this.workflowItemData.TypicalName).subscribe(res => {
        const suggested = res.find(x => x.TargetAttributeName === this.selectedTargetAttribute.Name);
        if (!isNullOrUndefined(suggested)) {
          const attr = new Attribute();
          attr.Name = suggested.SourceAttributeName;
          this.selectedAttribute = attr;
        }
      });
    });
  }

  private populateAttributeDataSource(attributes: Attribute[]) {
    this.allattributes = attributes;
    this.attributes = attributes;
  }

  public cellClick(dataItem: any) {
    this.selectedrow = dataItem.dataItem;
    this.selectedTypical = dataItem.dataItem.typicalName;
    this.fillAttributes(dataItem.dataItem.typicalId);
    this.selectedAttribute = new Attribute(); //
  }

  public dataStateChange(state: DataStateChangeEvent): void {
    this.gridState = state;
    this.dynamicDataService.read();
  }

  public apply() {
    // this.workflowItemService.applyDynamicDataPopup.next({
    //   SourceWorkflowItemId: this.selectedrow.id,
    //   SourceAttributeName: this.selectedAttribute.Name,
    //   name: '[' + this.selectedrow.title + '].' + this.selectedTypical + '->' + this.selectedAttribute.Name,
    //   type: this.selectedAttribute.DataType
    // });

    if (this.workflowItemData.linkedParentId) {
      this.workflowItemService.workflowLinkageBroken({
        itemIds: [ this.workflowItemData.linkedParentId ],
        action: 'dynChanged'
      });
      return;
    }
    if(this.permissionService.hasPermission('/api/Design/SaveDynamicDataSuggestions')){
      this.designService
      .saveDynamicDataSuggestions(
        this.selectedTypical,
        this.selectedAttribute.Name,
        this.workflowItemData.TypicalName,
        this.selectedTargetAttribute.Name
      )
      .subscribe(res => {
      });
    }
    this.applyDynamicData.emit({
      SourceWorkflowItemId: this.selectedrow.id,
      SourceAttributeName: this.selectedAttribute.Name,
      name: '[' + this.selectedrow.title + '].' + this.selectedTypical + '->' + this.selectedAttribute.Name,
      type: this.selectedAttribute.DataType
    });
    this.workflowItemService.changedWorkflow.next();
    this.dynamicDataService.reset();
  }

  public cancel() {
    this.dynamicDataService.reset();
    this.workflowItemService.closedDynamicDataPopup.next();
  }

  public attributeChanged(item: Attribute) {
    this.selectedAttribute = item;
  }
}
