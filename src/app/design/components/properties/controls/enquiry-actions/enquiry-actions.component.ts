import {WorkflowItemService} from '../../../../services/workflow-item.service';
import {WorkflowItemCustomData} from '../../../../models/workflow-item-custom-data.model';
import {Component, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {DesignService} from 'src/app/design/services/design.service';
import {tap, switchMap, map} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {from} from 'rxjs';
import {AutoUnsubscribe, takeWhileAlive} from 'take-while-alive';
import { PermissionService } from 'src/app/core/services/permission.service';
import { EnquiryAction } from 'src/app/design/models/enquiry-action.model';
import { ComboBoxComponent } from '@progress/kendo-angular-dropdowns';

@Component({
  selector: 'app-enquiry-actions',
  templateUrl: './enquiry-actions.component.html',
  styleUrls: ['./enquiry-actions.component.css']
})
@AutoUnsubscribe()
export class EnquiryActionsComponent implements OnInit, AfterViewInit {
  @ViewChild('cmbActions') cmbActions: ComboBoxComponent;
  public selectedAction = '';
  public workflowItemData: WorkflowItemCustomData;
  public readonly: boolean;
  public actions: Array<EnquiryAction> = [];

  constructor(private designService: DesignService, 
    private workflowItemService: WorkflowItemService,
    public permissionService: PermissionService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });
  }

  ngOnInit() {
    this.fillEnquiryAction(this.workflowItemData.TypicalName, this.workflowItemData.CatalogId);
  }

  public fillEnquiryAction(typicalName: string, catalogId: string) {
    if(this.permissionService.hasPermission('/api/Design/GetEnquiryActions')){
      this.designService.getEnquiryActions(typicalName, catalogId).subscribe(result => {
        this.actions = result;
        const items = this.workflowItemData.Parameters.split(':');
        if (items.length === 1) {
            this.selectedAction = items[0];
        } else {
        this.selectedAction = this.workflowItemData.Parameters.split(':')[1].trim().toString();
        }
      });
    }
    else {
      this.actions = [];
      const items = this.workflowItemData.Parameters.split(':');
      if (items.length === 1) {
          this.selectedAction = items[0];
      } else {
      this.selectedAction = this.workflowItemData.Parameters.split(':')[1].trim().toString();
      }
    }
  }

  // private populateActionsDataSource(actions: string[]) {
  //   this.actions = [...actions];
  // }

  ngAfterViewInit() {
    // this.cmbActions.filterChange
    //   .asObservable()
    //   .pipe(
    //     switchMap(value =>
    //       from([this.allactions]).pipe(
    //         tap(() => (this.cmbActions.loading = true)),
    //         map(data => {
    //           return data.filter(x => x.toLowerCase().indexOf((value as string).toLowerCase()) > -1);
    //         })
    //       )
    //     )
    //   )
    //   .subscribe(filtered => {
    //     this.actions = filtered;
    //     this.cmbActions.loading = false;
    //   });
  }

  actionTypeChanged(newAction: any) {
    if (isNullOrUndefined(newAction)) {
      this.selectedAction = '';
      this.workflowItemData.Parameters = '';
      return;
    }
    this.selectedAction = newAction;
    const sel = this.actions.find(u => u.action === newAction);
    this.workflowItemData.Parameters = sel.type + ' : ' + sel.action;
  }
}
