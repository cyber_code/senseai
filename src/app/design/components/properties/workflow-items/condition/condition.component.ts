import {Component, OnInit, ViewChild, AfterViewInit, Input} from '@angular/core';
import {WorkflowItemCustomData, Constraint, DynamicData} from 'src/app/design/models/workflow-item-custom-data.model';
import {DesignService} from 'src/app/design/services/design.service';
import {SessionService} from 'src/app/core/services/session.service';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {WorkflowService} from 'src/app/design/services/workflow.service';
import {Attribute} from 'src/app/design/models/attribute.model';
import {isNullOrUndefined} from 'util';
import {tap, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {MessageService} from 'src/app/core/services/message.service';
import {MessageType} from 'src/app/core/models/message.model';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-condition',
  templateUrl: './condition.component.html',
  styleUrls: ['./condition.component.css']
})
@AutoUnsubscribe()
export class ConditionComponent implements OnInit, AfterViewInit {
  public workflowItemData: WorkflowItemCustomData;
  private previousWorkflowItemData: WorkflowItemCustomData;
  private startItemCustomData: WorkflowItemCustomData;
  public operators: string[];
  public filterOperator: string;
  public filterValue: string;
  public editedFilter: Constraint;
  editedRowIndex: any;
  public selectedAttribute: Attribute;
  public attributes: Attribute[];
  public readonly: boolean;
  @ViewChild('cmbAttribute') cmbAttribute;
  @Input() cells;

  constructor(
    private designService: DesignService,
    private sessionService: SessionService,
    private workflowItemService: WorkflowItemService,
    private messageService: MessageService,
    private workflowService: WorkflowService,
    public permissionService: PermissionService
  ) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.previousWorkflowItemData = this.workflowItemService.workflowItemSelected.previousWorkflowItemCustomData;
    this.startItemCustomData = this.workflowItemService.workflowItemSelected.startWorkflowItemCustomData;
    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });
  }

  private populateAttributeDataSource(attributes: Attribute[]) {
    if (isNullOrUndefined(this.selectedAttribute)) {
      this.attributes = attributes;
    } else {
      const newItems = attributes.filter(x => x.Name !== this.selectedAttribute.Name);
      this.attributes = [this.selectedAttribute, ...newItems];
    }
  }

  ngOnInit() {
    let currentCell = this.cells.find(cell => cell.attributes.id === this.workflowItemService.workflowItemSelected.id);
    let startEl = this.workflowService.getLastStateItem(currentCell, {cells: this.cells.map(cell => cell.attributes)});
    if(this.permissionService.hasPermission('/api/Design/GetConditionItem')){
      this.designService.getConditionItem(startEl && startEl.customData ? startEl.customData['AdapterName'] : 'T24WebAdapter').subscribe(result => {
        if (!isNullOrUndefined(result)) {
          this.operators = result.operators;
        }
      });
    }
    else {
      this.operators=[];
    }
    const typicalId = this.getTypicalId();
    if(this.permissionService.hasPermission('/api/Design/SearchAttributes')){
      this.designService.searchAttributes('', typicalId).subscribe(result => {
        this.populateAttributeDataSource(result);
      });
    }
    else {
      this.populateAttributeDataSource([]);
    }
  }

  private getTypicalId() {
    let currentCell = this.cells.find(cell => cell.attributes.id === this.workflowItemService.workflowItemSelected.id);
    let step = this.workflowService.getLastItem(currentCell, {cells: this.cells.map(cell => cell.attributes)});
    return step && step.customData.TypicalId;
  }

  ngAfterViewInit() {
    const typicalId = this.getTypicalId();
    this.cmbAttribute.filterChange
      .asObservable()
      .pipe(
        tap(() => (this.cmbAttribute.loading = true)),
        // wait 300ms after each keystroke before considering the term
        debounceTime(300),
        // ignore new term if same as previous term
        distinctUntilChanged(),
        // switch to new search observable each time the term changes
        switchMap((newfilter: string) => {
          return this.designService.searchAttributes(newfilter, typicalId);
        })
      )
      .subscribe(result => {
        this.populateAttributeDataSource(result);
        this.cmbAttribute.loading = false;
      });
  }

  attributeChanged(newValue: Attribute) {
    if (isNullOrUndefined(newValue)) {
      this.selectedAttribute = {} as Attribute;
      return;
    }
    this.selectedAttribute = newValue;
  }

  addFilterClicked(event: any) {
    if (this.workflowItemData.Constraints.findIndex(e => e.AttributeName === this.selectedAttribute.Name) !== -1) {
      this.messageService.sendMessage({
        text: `Attribute ${this.selectedAttribute.Name} already exists in filters`,
        type: MessageType.Error
      });
      return;
    }
    this.workflowItemData.Constraints.push({
      AttributeName: this.selectedAttribute.Name,
      Operator: this.filterOperator,
      AttributeValue: this.filterValue,
      IsPostFilter: false,
      DynamicData: new DynamicData()
    });
  }

  public editHandler({sender, rowIndex, dataItem}) {
    this.closeEditor(sender);

    this.editedRowIndex = rowIndex;
    this.editedFilter = Object.assign({}, dataItem);

    sender.editRow(rowIndex);
  }

  public cancelHandler({sender, rowIndex}) {
    this.closeEditor(sender, rowIndex);
  }

  public saveHandler({sender, rowIndex, dataItem, isNew}) {
    sender.closeRow(rowIndex);
    this.workflowItemData.Constraints[rowIndex] = dataItem;
    this.editedRowIndex = undefined;
    this.editedFilter = undefined;
  }

  public removeHandler({dataItem}) {
    const items = this.workflowItemData.Constraints.filter(item => item.AttributeName !== dataItem.AttributeName);
    this.workflowItemData.Constraints = items;
  }

  private closeEditor(grid, rowIndex = this.editedRowIndex) {
    grid.closeRow(rowIndex);
    this.workflowItemData.Constraints[rowIndex] = this.editedFilter;
    this.editedRowIndex = undefined;
    this.editedFilter = undefined;
  }
}
