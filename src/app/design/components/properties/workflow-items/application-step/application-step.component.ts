import {MessageService} from 'src/app/core/services/message.service';
import {FiltersPostFiltersComponent} from '../../controls/filters-postfilters/filters-postfilters.component';
import {Typical} from 'src/app/design/models/typical.model';
import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {WorkflowItemCustomData} from 'src/app/design/models/workflow-item-custom-data.model';
import {MessageType} from 'src/app/core/models/message.model';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-application-step',
  templateUrl: './application-step.component.html',
  styleUrls: ['./application-step.component.css']
})
@AutoUnsubscribe()
export class ApplicationStepComponent implements OnInit {
  @ViewChild(FiltersPostFiltersComponent)
  public filtersComponent: FiltersPostFiltersComponent;
  public opened = false;
  public openedfilters = false;
  public readonly: boolean;

  public workflowItemData: WorkflowItemCustomData;
  @Input() cells;

  constructor(private workflowItemService: WorkflowItemService, private messageService: MessageService) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;

    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });
  }

  ngOnInit() {
  }

  typicalChanged(typical: Typical) {
    //  this.filtersComponent.fillAttributes(typical.id);
    this.workflowItemData.Constraints = [];
    this.workflowItemData.PostConstraints = [];
    this.workflowItemData.DynamicDatas = [];
  }

  public openDynamicData(item: any) {
    const selectedPaths = this.workflowItemService.workflowPaths.filter(x => x.selected === true);
    if (!selectedPaths || selectedPaths.length === 0) {
      this.messageService.sendMessage({
        type: MessageType.Warning,
        text: 'Please select one path in order to add dynamic data'
      });
      return;
    } else if (selectedPaths.length > 1) {
      this.messageService.sendMessage({
        type: MessageType.Warning,
        text: 'Please select only one path in order to add dynamic data'
      });
      return;
    }
    this.opened = true;
  }

  public close(item: any) {
    this.opened = false;
  }

  public openFilters(item: any) {
    this.openedfilters = true;
  }

  public closefilters(item: any) {
    this.openedfilters = false;
  }
}
