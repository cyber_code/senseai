import {Guid} from 'src/app/shared/guid';
import {WorkflowItemCustomData} from 'src/app/design/models/workflow-item-custom-data.model';
import {Component, EventEmitter, OnInit, Output, Input} from '@angular/core';
import {DesignService} from 'src/app/design/services/design.service';
import {SessionService} from 'src/app/core/services/session.service';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {System} from 'src/app/design/models/system.model';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-start-item',
  templateUrl: './start-item.component.html',
  styleUrls: ['./start-item.component.css']
})
@AutoUnsubscribe()
export class StartItemComponent implements OnInit {
  public systems: System[] = [];
  public selectedSystem: System = new System();
  public workflowItemData: WorkflowItemCustomData;
  public readonly: boolean;
  @Input() cells;

  constructor(
    private designService: DesignService,
    private sessionService: SessionService,
    private workflowItemService: WorkflowItemService,
    public permissionService: PermissionService
  ) {
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });

    this.selectedSystem = this.workflowItemData.getSystem();
    if (Guid.isEmpty(this.selectedSystem.id)) {
      const defaultSettings = this.sessionService.getDefaultSettings();
      this.selectedSystem = defaultSettings.system;
      this.workflowItemData.setSystem(defaultSettings.system);
    }
  }

  ngOnInit() {
    if(this.permissionService.hasPermission('/api/Design/GetSystems')){
      this.designService.getSystems().subscribe(result => {
        this.systems = result;
        this.workflowItemData.TypicalType = '0';
      });
    }
    else {
      this.systems = [];
      this.workflowItemData.TypicalType = '0';
    }

  }

  valueChange(id: string) {
    this.selectedSystem = this.systems.find(x => x.id === id) || new System();
    this.workflowItemData.setSystem(this.selectedSystem);
    this.workflowItemService.changeSystem(this.selectedSystem);
  }
}
