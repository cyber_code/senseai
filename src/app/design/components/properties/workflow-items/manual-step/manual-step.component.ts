import {MessageService} from 'src/app/core/services/message.service';
import {FiltersPostFiltersComponent} from '../../controls/filters-postfilters/filters-postfilters.component';
import {Component, OnInit, ViewChild, Input} from '@angular/core';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {WorkflowItemCustomData} from 'src/app/design/models/workflow-item-custom-data.model';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';

@Component({
  selector: 'app-manual-step',
  templateUrl: './manual-step.component.html',
  styleUrls: ['./manual-step.component.css']
})

@AutoUnsubscribe()
export class ManualStepComponent implements OnInit {

  public readonly: boolean;
  public workflowItemData: WorkflowItemCustomData;
  @Input() cells;

  constructor(private workflowItemService: WorkflowItemService) {
    if (this.workflowItemService.workflowItemSelected) {
        this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    }
    this.readonly = !this.workflowItemService.editable;
    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });
  }

  ngOnInit() {}

}
