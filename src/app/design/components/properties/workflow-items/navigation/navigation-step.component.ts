import {WorkflowItemCustomData} from 'src/app/design/models/workflow-item-custom-data.model';
import {Component, OnInit, ViewChild, AfterViewInit, Input} from '@angular/core';
import {DesignService} from 'src/app/design/services/design.service';
import {SessionService} from 'src/app/core/services/session.service';
import {WorkflowItemService} from 'src/app/design/services/workflow-item.service';
import {tap, switchMap, map} from 'rxjs/operators';
import {isNullOrUndefined} from 'util';
import {from, of} from 'rxjs';
import {Menu} from 'src/app/design/models/menu.model';
import {WorkflowItemActionType} from 'src/app/core/models/workflow-item-action-type';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import {Helper} from 'src/app/shared/helper';
import {ComboBoxComponent} from '@progress/kendo-angular-dropdowns';
import {Guid} from '../../../../../shared/guid';
import {System} from '../../../../models/system.model';
import {WorkflowService} from '../../../../services/workflow.service';
import {KitchenSinkService} from '../../../../services/rapid-services/kitchensink-service';

@Component({
  selector: 'app-navigation-step',
  templateUrl: './navigation-step.component.html',
  styleUrls: ['./navigation-step.component.css']
})
@AutoUnsubscribe()
export class NavigationStepComponent implements OnInit, AfterViewInit {
  public items: any[] = [{id: WorkflowItemActionType.Menu, title: 'Menu'}, {id: WorkflowItemActionType.Tab, title: 'Tab'}];
  public rappid: KitchenSinkService;
  public selectedItem: any = {};
  public readonly: boolean;
  public workflowItemData: WorkflowItemCustomData;
  public startWorkflowData: any;
  public menus: Menu[];
  public initialMenus: Menu[] = [];
  public allmenus: Menu[];
  public selectedMenu: Menu;
  @ViewChild('cmbMenus') cmbMenus: ComboBoxComponent;
  public selectedSystem;
  public defaultSystem: System = new System();
  public subProjectId: any;
  public changedSystem: System = new System();
  @Input() cells;

  constructor(
    private designService: DesignService,
    private sessionService: SessionService,
    private workflowItemService: WorkflowItemService,
    private workflowService: WorkflowService
  ) {
    this.subProjectId = this.sessionService.getWorkContext().subProject.id;
    this.workflowItemData = this.workflowItemService.workflowItemSelected.customData;
    this.readonly = !this.workflowItemService.editable;
    this.selectedMenu = new Menu();
    this.selectedItem = this.items.find(x => x.id === this.workflowItemData.ActionType) as any;

    this.workflowItemService.workflowEditable$.pipe(takeWhileAlive(this)).subscribe(editable => {
      this.readonly = !editable;
      this.workflowItemService.editable = editable;
    });
    this.defaultSystem = this.sessionService.getWorkContext().defaultSettings.system;
  }

  ngOnInit() {
    this.loadMenu();
  }

  loadMenu() {
    let currentCell = this.cells.find(cell => cell.attributes.id === this.workflowItemService.workflowItemSelected.id);
    let startEl = this.workflowService.getLastStateItem(currentCell, {cells: this.cells.map(cell => cell.attributes)});
    if (startEl) {
      this.designService.getAllNavigations(this.subProjectId, startEl ? startEl.customData.SystemId : Guid.empty).subscribe(result => {
        this.allmenus = result;
        this.selectedMenu = (this.selectedItem.id === 0 && this.allmenus.find(x => x.url === this.workflowItemData.Parameters)) || new Menu();
        this.populateMenusDataSource(result);
      });
    }
  }

  private populateMenusDataSource(menus: Menu[]) {
    const newItems = menus.filter(x => x.id !== this.selectedMenu.id);
    this.menus = this.initialMenus = [this.selectedMenu, ...newItems].slice(0, 20);
  }

  valueChange(value: any) {
    this.selectedItem = value;
    this.workflowItemData.ActionType = this.selectedItem.id;
    this.selectedMenu = new Menu();
    this.workflowItemData.Parameters = '';
  }

  ngAfterViewInit() {
    
    if (this.cmbMenus) {
      this.cmbMenus.filterChange
        .asObservable()
        .pipe(
          switchMap((value: string) =>
            from([this.allmenus]).pipe(
              tap(() => (this.cmbMenus.loading = true)),
              map(data => {
                if (value === '' || value.length < 3) {
                  this.cmbMenus.toggle(false);
                  return this.initialMenus;
                }
                return data.filter(x => Helper.dataSourceFilter(x.url, value));
              })
            )
          )
        )
        .subscribe(filtered => {
          this.menus = filtered;
          this.cmbMenus.loading = false;
        });
    }
  }

  menuChanged(newValue: Menu) {
    if (isNullOrUndefined(newValue)) {
      this.selectedMenu = {} as Menu;
      this.workflowItemData.Parameters = '';
      return;
    }
    this.selectedMenu = newValue;
    this.workflowItemData.Parameters = this.selectedMenu.url;
  }
}
