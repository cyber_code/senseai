import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-popup-content',
  templateUrl: './popup-content.component.html',
  styleUrls: ['./popup-content.component.css']
})
export class PopupContentComponent implements OnInit {
  @Output() 
  public closeHelpPopup = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  close() {
    this.closeHelpPopup.emit()
  }

}
