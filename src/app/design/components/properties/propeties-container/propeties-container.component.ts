import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {WorkflowItemActionType} from 'src/app/core/models/workflow-item-action-type';
import * as Wft from '../../../../core/models/workflow-item-type';
import * as Wfact from '../../../../core/models/workflow-item-action-type';
import {WorkflowItemType} from '../../../../core/models/workflow-item-type';

@Component({
  selector: 'app-propeties-container',
  templateUrl: './propeties-container.component.html',
  styleUrls: ['./propeties-container.component.css']
})
export class PropetiesContainerComponent implements OnInit {
  @Input() actionType: WorkflowItemActionType;
  @Input() type: WorkflowItemType;
  @Input() cells: any;
  public WorkflowItemActionType = Wfact.WorkflowItemActionType;
  public WorkflowItemType = Wft.WorkflowItemType;
  public helpPopupOpened = false;
  @Output() public selectedSystem: EventEmitter<any>;
  constructor() {
  }

  ngOnInit() {
  }

  openHelpPopup() {
    this.helpPopupOpened = true;
  }

  closeHelpPopup() {
    this.helpPopupOpened = false;
  }


}
