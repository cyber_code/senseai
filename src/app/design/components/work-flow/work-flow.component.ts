import {Guid} from 'src/app/shared/guid';
import {WorkflowItemActionType} from 'src/app/core/models/workflow-item-action-type';
import * as Wft from '../../../core/models/workflow-item-type';
import * as Wfact from '../../../core/models/workflow-item-action-type';

import {MessageService} from './../../../core/services/message.service';
import {TreeViewItem} from '../../../shared/tree-view-item';
import {isNullOrUndefined} from 'util';
import {Component, ElementRef, OnInit, AfterViewInit, ViewChild, Output, EventEmitter} from '@angular/core';
import {WorkflowToolbarComponent} from '../workflow-toolbar/workflow-toolbar.component';
import {DesignService} from '../../services/design.service';
import {StencilService} from '../../services/rapid-services/stencil-service';
import {ToolbarService} from '../../services/rapid-services/toolbar-service';
import {InspectorService} from '../../services/rapid-services/inspector-service';
import {HaloService} from '../../services/rapid-services/halo-service';
import {KeyboardService} from '../../services/rapid-services/keyboard-service';
import {KitchenSinkService} from '../../services/rapid-services/kitchensink-service';
import {NodeType} from 'src/app/shared/node-type';
import {MessageType} from 'src/app/core/models/message.model';
import {MenuAction} from '../../models/menu-action';
import {AppTreViewComponent} from 'src/app/core/components/tree-view/app-tree-view.component';
import {RapidEventsService} from '../../services/rapid-events.service';
import {WorkflowItem} from '../../models/workflow-item';
import {WorkflowItemService} from '../../services/workflow-item.service';
import {WorkflowService} from '../../services/workflow.service';
import {WorkflowItemCustomData} from '../../models/workflow-item-custom-data.model';
import * as joint from 'src/vendor/rappid';
import {WorkflowItemType} from '../../../core/models/workflow-item-type';
import {Suggestion} from 'src/app/core/models/suggestion.model';
import {SessionService} from 'src/app/core/services/session.service';
import {HighLighGraphHelper} from 'src/app/shared/highlight-graph-helper';
import {AutoUnsubscribe, takeWhileAlive} from 'take-while-alive';
import {ListSuggestions} from 'src/app/core/models/listsuggestions.model';
import {DynamicDataComponent} from '../properties/controls/dynamicdata/dynamicdata.component';
import * as _ from 'lodash';
import {TimeVarianceComponent} from '../time-variance/time-variance.component';
import {ActivatedRoute} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: "app-work-flow",
  templateUrl: "./work-flow.component.html",
  styleUrls: ["./work-flow.component.css"]
})
@AutoUnsubscribe()
export class WorkFlowComponent implements OnInit, AfterViewInit {
  public rappid: KitchenSinkService;
  public showNavbar: Boolean;
  public showTimeVariance: Boolean = false;
  public propertiesCollapsed = false;
  public editMode = false;
  public opened = false;
  public loading = false;
  public gridPreData: any[];
  public gridPostData: any[];
  public selectedTimeVarianceItem: TreeViewItem;
  private selectedTreeViewItem: TreeViewItem;

  public selectedWorkflowItem: WorkflowItem;
  public toggleConnectorText = "Hide";
  public connectorShow = false;
  public offset = { left: 0, top: 0 };
  public linkHolder;
  public versionStatus;

  @ViewChild(TimeVarianceComponent)
  private timeVarianceComponent: TimeVarianceComponent;
  @ViewChild(WorkflowToolbarComponent)
  private workflowToolbarComponent: WorkflowToolbarComponent;
  @ViewChild(AppTreViewComponent)
  private treeViewComponent: AppTreViewComponent;
  public WorkflowItemType = Wft.WorkflowItemType;
  public WorkflowItemActionType = Wfact.WorkflowItemActionType;
  public actionType: WorkflowItemActionType;
  public type: WorkflowItemType;
  public message: string;
  public messageType: string;
  public actions: any[] = [
    { id: WorkflowItemActionType.Authorise, title: "Authorise" },
    { id: WorkflowItemActionType.Reverse, title: "Reverse" },
    { id: WorkflowItemActionType.Delete, title: "Delete" },
    { id: WorkflowItemActionType.See, title: "See" },
    { id: WorkflowItemActionType.Verify, title: "Verify" },
    { id: WorkflowItemActionType.Validate, title: "Validate" }
  ];
  public isNegativeStepDialogOpen = false;
  public negativeTestStep;
  public linkedWorkflows: any = [];
  public isLinkWorkflowDialogOpen: Boolean = false;
  public isCopyWorkflowClicked: Boolean = true;
  public isLinkWorkflowClicked: Boolean = false;
  public isEditWorkflowDialogOpen: Boolean = false;
  public isWorkflowChanged: Boolean = false;
  public breakLinkageItems: any = [];
  public breakLinkageAction: string;
  public openWorkflowWarningDialog: boolean = false;
  public workflowWarningMessage: string = "";
  public issueWorkflow: boolean = false;
  public workflowItemToBeIssued: any;
  public selectedWorkflowVersion: any;
  public loadingExpand: boolean = false;
  ///
  public dialogForLinkedItemsNewVersionOpened = false;
  public selectedItemCurrentVersion = 0;
  public selectedItemToIssue: any;
  public isCalledForWorkflowWithLinkedElements = false;
  constructor(
    private element: ElementRef,
    private designService: DesignService,
    private messageService: MessageService,
    private rapidEventsService: RapidEventsService,
    private workflowItemServie: WorkflowItemService,
    private sessionService: SessionService,
    private workflowItemService: WorkflowItemService,
    private dynamicData: DynamicDataComponent,
    private workflowService: WorkflowService,
    private activatedRoute: ActivatedRoute,
    public permissionService: PermissionService
  ) {
    this.selectedWorkflowItem = { customData: {} } as WorkflowItem;
    rapidEventsService.itemSelected$
      .pipe(takeWhileAlive(this))
      .subscribe(result => {
        if (isNullOrUndefined(result)) {
          // if the use click on the paper, hide suggestions
          this.setAISuggestionsVisiblity(false);
        }

        /// if it is the same element we don't have any reason to update the properties
        if (
          this.actionType &&
          this.selectedWorkflowItem &&
          result &&
          this.selectedWorkflowItem.id === result.id
        ) {
          return;
        }
        this.actionType = null;
        this.type = null;
        // this is a trick in order to re initialize child components, when ActionType doesn't change
        setTimeout(() => {
          this.updateWorkflowItemPropertiesPanel(result);
          this.workflowItemServie.workflowEditable.next(this.editMode);
        }, 0);
      });
    this.workflowItemServie.worfklowItemSelectedModified$
      .pipe(takeWhileAlive(this))
      .subscribe(result => {
        this.updateWorkflowItem(result);
      });

    rapidEventsService.newElementAdded$
      .pipe(takeWhileAlive(this))
      .subscribe(result => {
        const currentCell = this.rappid.graph.getCell(result.cell.id);
        if (!isNullOrUndefined(currentCell)) {
          if (currentCell.attributes.type !== "standard.Image") {
            let graph = this.rappid.getGraphJson();
            graph.cells = graph.cells.filter(
              cell =>
                cell.id !== currentCell.id &&
                (!cell.customData ||
                  !cell.customData.parentElement ||
                  cell.customData.parentElement.id !== currentCell.id)
            );
            this.addToUndoStack(graph.cells);
            this.isWorkflowChanged = true;
          }

          if (result.cell.attributes.type === "fsa.State") {
            if (
              !isNullOrUndefined(
                result.collection.models.find(
                  x =>
                    x.attributes.type === "fsa.State" &&
                    x.cid !== result.cell.cid
                )
              )
            ) {
              currentCell.attributes.attrs.text.text = "Switch\nSystem";
              currentCell.attr("text/font-size", 10);
            } else {
              currentCell.attr("text/font-size", 14);
              currentCell.attributes.size.width = 60;
              currentCell.attributes.size.height = 60;
            }
          } else if (
            currentCell.attributes.customData.ActionType &&
            currentCell.attributes.attrs.root.type !== "Navigation Step"
          ) {
            const image = this.createImage(currentCell);
            this.rappid.graph.addCell(image);
          } else if (currentCell.attributes.type === "standard.Polygon") {
            currentCell.attr("image/x", "0.5%");
          }
        }
      });

    this.workflowItemService.closeEditWorkflowPopup
      .pipe(takeWhileAlive(this))
      .subscribe(action => {
        if (
          action === "cellRemoved" ||
          action === "linkChanged" ||
          action === "copyAdded"
        ) {
          this.rappid.onUndoClick(false);
        }
      });

    this.workflowItemServie.isNegativeStepChanged
      .pipe(takeWhileAlive(this))
      .subscribe(isNegativeStep => {
        let selectedCell = this.rappid.graph.getCell(
          this.selectedWorkflowItem.id
        );
        if (selectedCell) {
          selectedCell.attributes.customData.IsNegativeStep = isNegativeStep;
        }
        if (!isNegativeStep) {
          let graph = this.rappid.getGraphJson();
          let negativeCell = graph.cells.find(
            cell =>
              cell.customData &&
              cell.customData.parentElement &&
              cell.customData.parentElement.id ===
                this.selectedWorkflowItem.id &&
              cell.attrs.root &&
              cell.attrs.root.type === "Negative"
          );
          if (negativeCell) {
            this.rappid.graph.getCell(negativeCell.id).remove();
          }
        } else {
          if (selectedCell) {
            let negativeCell = this.createNegativeStep(selectedCell.attributes);
            this.rappid.graph.addCell(negativeCell);
          }
        }
      });

    this.workflowItemServie.changedWorkflow
      .pipe(takeWhileAlive(this))
      .subscribe(() => {
        this.isWorkflowChanged = true;
      });

    this.workflowItemServie.typicalUpdated$
      .pipe(takeWhileAlive(this))
      .subscribe(typical => {
        this.setModifiedLinkedElement("typicalChanged");
        this.reloadWorkflowPaths();
      });

    this.workflowItemServie.catalogUpdated$
      .pipe(takeWhileAlive(this))
      .subscribe(catalog => {
        this.setModifiedLinkedElement("catalogChanged");
      });

    this.workflowItemServie.actionTypeUpdated$
      .pipe(takeWhileAlive(this))
      .subscribe(() => {
        this.setModifiedLinkedElement("actionTypeChanged");
      });

    this.workflowItemServie.descriptionUpdated$
      .pipe(takeWhileAlive(this))
      .subscribe(() => {
        this.setModifiedLinkedElement("descriptionChanged");
      });

    rapidEventsService.itemPasted$
      .pipe(takeWhileAlive(this))
      .subscribe(elements => {
        elements.map(el => {
          if (
            el.attributes.customData &&
            el.attributes.customData.ActionType &&
            el.attributes.attrs.root.type !== "Navigation Step"
          ) {
            const image = this.createImage(el);
            this.rappid.graph.addCell(image);
          }

          if (
            el.attributes.customData &&
            el.attributes.customData.IsNegativeStep
          ) {
            let negativeCell = this.createNegativeStep(el.attributes);
            this.rappid.graph.addCell(negativeCell);
          }
        });
      });

    this.workflowItemServie.roleUpdated$
      .pipe(takeWhileAlive(this))
      .subscribe(() => {
        this.setModifiedLinkedElement("roleChanged");
      });

    this.workflowItemServie.actionUpdated$
      .pipe(takeWhileAlive(this))
      .subscribe(() => {
        this.setModifiedLinkedElement("actionChanged");
      });

    this.workflowItemService.addDynamicDataForLinkedEl
      .pipe(takeWhileAlive(this))
      .subscribe(() => {
        let graph = this.rappid.getGraphJson();
        let linkedCells = graph.cells.filter(
          cell =>
            cell.customData && cell.customData.Type === WorkflowItemType.Linked
        );
        if (linkedCells.length > 0) {
          linkedCells.map(linkedCell => {
            let children = graph.cells.filter(
              cell =>
                cell.customData &&
                cell.customData.linkedParentId === linkedCell.id
            );
            if (children.length > 0) {
              this.addDynamicDataForLinkedElements(linkedCell, children);
            }
          });
          this.rappid.loadGraphJson(graph);
        }
      });


    this.workflowItemService.addDynamicDataForLinkedElCheck.pipe(takeWhileAlive(this)).subscribe((paths) => {
      this.updateDynamicData(this.rappid.getGraphJson(), paths);
    });

    this.workflowItemService.addDynamicDataForLinkedEl.pipe(takeWhileAlive(this)).subscribe((paths) => {
      let graph = this.rappid.getGraphJson();
      let linkedCells = graph.cells.filter(cell => cell.customData && cell.customData.Type === WorkflowItemType.Linked);
      if (linkedCells.length > 0) {
        linkedCells.map(linkedCell => {
          let children = graph.cells.filter(cell => cell.customData && cell.customData.linkedParentId === linkedCell.id);
          if (children.length > 0) {
            this.addDynamicDataForLinkedElements(linkedCell, children);
          }
        });
      }
    });

    this.workflowItemServie.workflowLinkageBroken$
      .pipe(takeWhileAlive(this))
      .subscribe(res => {
        this.isEditWorkflowDialogOpen = true;
        this.breakLinkageAction = res.action;
        let breakLinkageItems = [];
        if (res.itemIds) {
          for (let i = 0; i < res.itemIds.length; i++) {
            let item = this.rappid.graph.getCell(res.itemIds[i]).attributes;
            breakLinkageItems = breakLinkageItems.concat(item);
          }
        }
        this.breakLinkageItems = breakLinkageItems;
      });

    this.workflowItemServie.workflowVersionChanged$.pipe(takeWhileAlive(this)).subscribe((res: any) => {
      if (res.response) {
        this.issueWorkflow = false;
        this.openWorkflowWarningDialog = true;
        this.workflowWarningMessage = res.response.message;
        this.selectedWorkflowVersion = res.selectedVersion;
      } else if (!isNullOrUndefined(res.selectedVersion.workflowId)
        && !isNullOrUndefined(res.selectedVersion.version)
        && this.permissionService.hasPermission('/api/Design/GetWorkflowVersions')) {
        this.designService.getWorkflowVersion(res.selectedVersion.workflowId, res.selectedVersion.version).subscribe(res => {
          let jsonParsed = (res && res.designerJson && JSON.parse(res.designerJson)) || [];
          const pathJson = (res.pathsJson && JSON.parse(res.pathsJson)) || {paths: []};

          if (jsonParsed.cells && jsonParsed.cells.length === 0) {
            this.clearGraph();
            return;
          }
          this.rappid.loadGraphJson(jsonParsed);
          this.rappid.layoutGraph(true);
          // it should be called after loading graph, otherwise it cannot highlight anything
          this.workflowItemServie.worklfowPathsLoaded(pathJson.paths);
        });
      }
    });


    this.rapidEventsService.postSuggestionClicked$
      .pipe(takeWhileAlive(this))
      .subscribe(() => {
        this.close();
      });

    this.workflowItemServie.workflowHighlight$
      .pipe(takeWhileAlive(this))
      .subscribe(path => {
        HighLighGraphHelper.unhighlightAll(this.rappid.graph);
        HighLighGraphHelper.highlight(this.rappid.graph, []);
        path.forEach(cv => {
          HighLighGraphHelper.highlight(this.rappid.graph, cv.itemIds);
        });
      });
    this.workflowItemServie.getGraphJson$
      .pipe(takeWhileAlive(this))
      .subscribe(neededBy => {
        const json = this.rappid.getGraphJson();
        const graphJson = JSON.stringify(json);
        this.workflowItemServie.sendGraphJson(graphJson, neededBy);
      });
  }

  private setAISuggestionsVisiblity(visible: boolean) {
    const btnPost = document.getElementById("btn-post");
    if (btnPost && btnPost.style) {
      btnPost.style.display = visible ? "" : "none";
    }
  }

  private setModifiedLinkedElement(action) {
    let currentCell = this.rappid.graph.getCell(this.selectedWorkflowItem.id);
    if (
      currentCell &&
      currentCell.attributes.customData &&
      currentCell.attributes.customData.linkedParentId
    ) {
      this.workflowItemServie.workflowLinkageBroken({
        itemIds: [currentCell.attributes.customData.linkedParentId],
        action: action
      });
    } else {
      this.isWorkflowChanged = true;
    }
  }

  private clearGraph() {
    this.rappid.loadGraphJson({ cells: [] });
    this.workflowItemServie.worklfowPathsLoaded([]);
  }

  // #region private methods


  private getSystemId() {
    let cells = this.rappid.graph.getCells();
    let currentCell = cells.find(
      cell =>
        this.selectedWorkflowItem &&
        cell.attributes.id === this.selectedWorkflowItem.id
    );
    let startEl = this.workflowService.getLastStateItem(currentCell, {
      cells: cells.map(cell => cell.attributes)
    });
    if (isNullOrUndefined(startEl) || Guid.isEmpty(startEl.SystemId)) {
      const context = this.sessionService.getWorkContext();
      return context.defaultSettings.system.id;
    } else {
      return startEl.SystemId;
    }
  }

  public drawSuggestion(item: any, presuggestion: boolean) {
    this.close();
    item.workflowItem.Type = parseInt(
      item.workflowItem.Type.toString(),
      10
    ) as WorkflowItemType;
    item.workflowItem.ActionType = parseInt(
      item.workflowItem.ActionType.toString(),
      10
    ) as WorkflowItemActionType;
    this.addNewElement(
      item.workflowItem,
      presuggestion,
      this.selectedWorkflowItem.id
    );
    this.reloadWorkflowPaths();
  }

  private loadSuggestions(openSuggestionWindow) {
    const currentItem = this.workflowItemServie.workflowItemSelected;
    if (
      Guid.isEmpty(currentItem.customData.CatalogId) ||
      Guid.isEmpty(currentItem.customData.TypicalId)
    ) {
      this.opened = true;
      this.gridPreData = [];
      this.gridPostData = [];
      return;
    }
    const context = this.sessionService.getWorkContext();

    const systemId = this.getSystemId();
    const currentGraph = this.rappid.getGraphJson();
    const graphToSave = this.workflowService.getGraphToSave(currentGraph);
    const graphJson = JSON.stringify(graphToSave);
    if (openSuggestionWindow) {
      this.opened = true;
      this.loading = true;
    }
    if(this.permissionService.hasPermission('/api/Design/GetSuggestions')){
      this.designService
      .getSuggestions(
        context.user.tenantId,
        context.project.id,
        context.subProject.id,
        systemId,
        currentItem.customData.getCatalog().id,
        currentItem.customData.refId || currentItem.customData.Id,
        graphJson
      )
      .subscribe(
        (res: any) => {
          if (res && res.postItems) {
            this.gridPostData = res.postItems.map(el => {
              this.loading = false;
              return {
                ...el,
                workflowItem: {
                  ...el.workflowItem,
                  ActionTitle: this.returnActionTitle(
                    el.workflowItem.ActionType
                  )
                }
              };
            });
          } else {
            this.gridPostData = [];
          }

          if (res && res.preItems) {
            this.gridPreData = res.preItems.map(el => {
              this.loading = false;
              return {
                ...el,
                workflowItem: {
                  ...el.workflowItem,
                  ActionTitle: this.returnActionTitle(
                    el.workflowItem.ActionType
                  )
                }
              };
            });
          } else {
            this.gridPreData = [];
          }
          this.setAISuggestionsVisiblity(
            this.gridPreData.length > 0 || this.gridPostData.length > 0
          );

          this.loading = false;
        },
        err => {
          this.loading = false;
        });
    }
    else {
      this.gridPostData = [];
      this.gridPreData = [];
      this.setAISuggestionsVisiblity(this.gridPreData.length > 0 || this.gridPostData.length > 0);

      this.loading = false;
    }
  }

  private reloadWorkflowPaths() {
    if (!this.editMode) {
      // if it is in readonly mode it doesn't make sense to call suggestions
      return;
    }
    let graph = this.rappid.getGraphJson();
    this.workflowItemServie.loadSuggestedPaths.next(JSON.stringify(graph));
  }

  private addNewElement(
    suggestedElementModel: WorkflowItemCustomData,
    presuggestion: boolean,
    selectedItemId: string
  ) {
    const wfItem = this.rappid.graph.attributes.cells.models.find(
      x => x.id === selectedItemId
    );
    const links = this.rappid.graph.getLinks();
    const previousLinks = links.filter(
      l => l.attributes.target.id === selectedItemId
    );
    const postLinks = links.filter(
      l => l.attributes.source.id === selectedItemId
    );
    if (presuggestion && previousLinks.length > 0) {
      previousLinks.forEach(prelink => {
        const elementToAdd = this.rappid.createRapidElement(
          suggestedElementModel
        );
        this.addSuggestion(wfItem, elementToAdd, false);
        prelink.target(elementToAdd);
        this.applyDynamicData(elementToAdd.attributes);
      });
    } else if (presuggestion) {
      const elementToAdd = this.rappid.createRapidElement(
        suggestedElementModel
      );
      this.addSuggestion(wfItem, elementToAdd, false);
      this.applyDynamicData(elementToAdd.attributes);
    } else if (postLinks.length > 0) {
      postLinks.forEach(postlink => {
        const elementToAdd = this.rappid.createRapidElement(
          suggestedElementModel
        );
        this.addSuggestion(wfItem, elementToAdd, true);
        postlink.source(elementToAdd);
        this.applyDynamicData(elementToAdd.attributes);
      });
    } else {
      const elementToAdd = this.rappid.createRapidElement(
        suggestedElementModel
      );
      this.addSuggestion(wfItem, elementToAdd, true);
      this.applyDynamicData(elementToAdd.attributes);
    }
    this.isWorkflowChanged = true;
    this.rappid.layoutGraph(true);
  }

  private addSuggestion(wfItem, elementToAdd, isPostSuggestion) {
    const linkToAdd = this.rappid.createLink();
    this.rappid.graph.addCell(elementToAdd);
    this.rappid.graph.addCell(linkToAdd);
    linkToAdd.source(isPostSuggestion ? wfItem : elementToAdd);
    linkToAdd.target(isPostSuggestion ? elementToAdd : wfItem);
  }

  private applyDynamicData(element) {
    if(this.permissionService.hasPermission('/api/Design/SaveDynamicDataSuggestions')){
    element.customData.DynamicData.map(data => {
        this.designService
        .saveDynamicDataSuggestions(
          element.customData.TypicalName,
          data.targetAttributeName.toUpperCase(),
          this.selectedWorkflowItem.customData.TypicalName,
          data.sourceAttributeName.toUpperCase()
        )
        .subscribe(res => {

        });
        this.dynamicData.applyDynamicDataForSuggestions({
          SourceWorkflowItemId: element.customData.Id,
          SourceAttributeName: data.targetAttributeName.toUpperCase(),
          TargetAttributeName: data.sourceAttributeName.toUpperCase(),
          name: '[' + element.customData.Title + '].' + element.customData.TypicalName + '->' + data.targetAttributeName.toUpperCase(),
          type: undefined,
          selectedItemData: this.selectedWorkflowItem
        });
      });
    }
  }

  private returnActionTitle(actionType) {
    const action = this.actions.find(el => el.id === actionType);
    return action ? action.title : "";
  }

  private createImage(el) {
    const image = new joint.shapes.standard.Image({
      type: "standard.Image",
      isInteractive: false,
      attrs: {
        image: {
          xlinkHref: "assets/actionicons/info.png",
          width: 25,
          height: 25
        },
        root: {
          type: "Info"
        }
      },
      customData: {
        parentElement: el.attributes
      },
      size: {
        width: 25,
        height: 25
      },
      position: {
        x: el.attributes.position.x,
        y: el.attributes.position.y - 30
      }
    });
    return image;
  }

  private createNegativeStep(el) {
    const image = new joint.shapes.standard.Image({
      type: "standard.Image",
      isInteractive: false,
      attrs: {
        image: {
          xlinkHref: "assets/actionicons/minus.png",
          width: 30,
          height: 30
        },
        root: {
          type: "Negative"
        }
      },
      customData: {
        parentElement: el
      },
      size: {
        width: 30,
        height: 30
      },
      position: { x: el.position.x + 30, y: el.position.y - 32 }
    });
    return image;
  }

  private updateWorkflowItem(result: any) {
    let currentCell = this.rappid.graph.getCell(this.selectedWorkflowItem.id);
    if (result.trackChanges) {
      this.setModifiedLinkedElement("titleChanged");
    }

    if (result.title) {
      currentCell.attr("label/text", result.title);
      let oldLength = currentCell.attributes.size.width;
      const oldX = currentCell.attributes.position.x;
      let length = result.title.length * 10;

      if (length < 200) {
        length = 200;
        oldLength = 200;
      }
      const center = (oldX + oldLength) / 2;
      const margin = -((oldX + length) / 2 - center);
      currentCell.resize(length, 40);
      currentCell.translate(margin, 0);
      this.selectedWorkflowItem.customData.Title = result.title;
      this.selectedWorkflowItem.customData.IsTitleChanged = result.IsTitleChanged;
    }
  }

  private updateWorkflowItemPropertiesPanel(result: any) {
    let loadSuggestions = false;
    if (
      isNullOrUndefined(result) ||
      (result.attributes.attrs.root &&
        result.attributes.attrs.root.type === "Expand")
    ) {
      this.type = null;
      this.actionType = null;
      return;
    }
    if (
      result.attributes.attrs.root &&
      result.attributes.attrs.root.type === "Info"
    ) {
      if (this.editMode) {
        const parentElement = result.attributes.customData.parentElement;
        result = result.collection.models.find(
          el => el.id === parentElement.id
        );
        loadSuggestions = true;
      } else {
        return;
      }
    } else if (
      result.attributes.attrs.root &&
      result.attributes.attrs.root.type === "Negative"
    ) {
      if (this.editMode) {
        const parentElement = result.attributes.customData.parentElement;
        result = result.collection.models.find(
          el => el.id === parentElement.id
        );
        this.isNegativeStepDialogOpen = true;
        this.negativeTestStep = this.rappid.getCustomData(result);
      } else {
        return;
      }
    }
    const models = result.collection.models;
    const id = result.id;
    const label = result.attributes.attrs.label || result.attributes.attrs.text;
    const displayText = label.text;
    const customData = this.rappid.getCustomData(result);
    let previousCustomData = null;
    let startCustomData = null;
    // if this element is condition we have to find the previous element in order to know which is the application or enquiry
    if (customData.Type === Wft.WorkflowItemType.Condition) {
      // find connector
      const connectorToCondition = models.find(
        x => x.isLink() && x.attributes.target.id === result.id
      );
      // find source element
      const previousItem =
        connectorToCondition &&
        models.find(
          x => x.attributes.id === connectorToCondition.attributes.source.id
        );
      if (!isNullOrUndefined(previousItem)) {
        previousCustomData = this.rappid.getCustomData(previousItem);
      }
    }

    if (customData.Type === Wft.WorkflowItemType.Start) {
      startCustomData = customData;
    } else {
      startCustomData = this.getStartItem(models);
    }
    this.selectedWorkflowItem = new WorkflowItem(
      id,
      displayText,
      customData,
      startCustomData,
      previousCustomData
    );
    this.propertiesCollapsed = false;
    this.actionType = this.selectedWorkflowItem.customData.ActionType;
    this.type = this.selectedWorkflowItem.customData.Type;
    let item = this.rappid.graph.getCell(this.selectedWorkflowItem.id);
    this.workflowItemServie.itemUpdated.next(item.attributes as any);
    if (loadSuggestions) {
      this.loadSuggestions(true);
    }
  }

  private getStartItem(models: any) {
    const startItem = models.find(x => {
      // this is done in order to merge function of WorkflowItemCustomData
      const itemCustomData = this.rappid.getCustomData(x);
      return (
        itemCustomData && itemCustomData.Type === Wft.WorkflowItemType.Start
      );
    });
    if (!isNullOrUndefined(startItem)) {
      return this.rappid.getCustomData(startItem);
    }
  }

  private setEditMode(editMode: boolean) {
    this.editMode = editMode;
    this.rappid.setEditMode(this.editMode);
    this.treeViewComponent.disable(this.editMode);
    this.rappid.paper.setInteractivity(this.editMode);

    if (this.editMode) {
      this.rappid.keyboardService.keyboard.enable();
    } else {
      this.setAISuggestionsVisiblity(this.editMode);
      this.rappid.keyboardService.keyboard.disable();
    }
    this.workflowItemServie.workflowEditable.next(this.editMode);
  }

  async loadWorkflow(treeViewItem: TreeViewItem, isCopied = false) {
    if (treeViewItem.type === NodeType.Workflow) {
      this.isWorkflowChanged = false;
      this.selectedItemCurrentVersion = 0;
      if(this.permissionService.hasPermission('/api/Design/GetWorkflow')){
        this.designService.getWorkflow(treeViewItem.id).subscribe(async result => {
          let jsonParsed;
          this.selectedItemCurrentVersion = result.versionId || 0;
          if (result && !result.isParsed) {
            jsonParsed = result && JSON.parse(result.designerJson) ? this.getParsedGraph(JSON.parse(result.designerJson)) : {cells: []};
          } else {
            jsonParsed = (result && result.designerJson && JSON.parse(result.designerJson)) || {cells: []};
            if (isCopied) {
              jsonParsed = this.getCopiedCells(jsonParsed);
            }
          }
          let pathJson = (result && result.pathsJson && JSON.parse(result.pathsJson)) || {paths: []};
          this.linkedWorkflows = (result && result.linkedWorkflows) || [];
          if (jsonParsed.length === 0 || (jsonParsed.cells && jsonParsed.cells.length === 0)) {
            this.clearGraph();
          } else {
            this.rappid.loadGraphJson(jsonParsed);
            this.rappid.layoutGraph(false);
            if (result && !result.isParsed) {
              pathJson = {paths: pathJson.paths.map((path, i) => ({...path, number: i}))};
            }
            // it should be called after loading graph, otherwise it cannot highlight anything
            let graph = this.rappid.getGraphJson();
            this.workflowItemServie.loadSuggestedPaths.next({graph: JSON.stringify(graph), loadWorkflow: true, paths: pathJson.paths});
            if (isCopied || (result && !result.isParsed)) {
              this.saveClicked(treeViewItem, false, pathJson);
            }
          }
        });
      }
      else {
        let jsonParsed;
        jsonParsed = {cells: []};
        if (isCopied) {
          jsonParsed = this.getCopiedCells(jsonParsed);
        }
        let pathJson = {paths: []};
        this.linkedWorkflows = [];
        if (jsonParsed.length === 0 || (jsonParsed.cells && jsonParsed.cells.length === 0)) {
          this.clearGraph();
        } else {
          this.rappid.loadGraphJson(jsonParsed);
          this.rappid.layoutGraph(false);
          let graph = this.rappid.getGraphJson();
          this.workflowItemServie.loadSuggestedPaths.next({graph: JSON.stringify(graph), loadWorkflow: true, paths: pathJson.paths});
          if (isCopied) {
            this.saveClicked(treeViewItem, false, pathJson);
          }
        }
      }
      if (this.rappid && this.rappid.hasOwnProperty('undoStack') && this.rappid.hasOwnProperty('redoStack')) {
        this.rappid.undoStack = [];
        this.rappid.redoStack = [];
        this.rappid.turnOffKeyboard();
      }
      }
      else {
        this.clearGraph();
      }
  }

  getParsedGraph(workflow) {
    if(workflow){
      let rapidItems = workflow.Items.map(item => {
        item.Type = Number(item.Type);
        if (item.Type === 0 || item.Type === 4 || item.Type === 3) {
          delete item.ActionType;
        } else if (item.Type === 2) {
          delete item.ActionType;
          item.Title = '';
        }
        let previousId = item.Id;
        let newItem = this.rappid.createRapidElement(item);
        newItem.id = previousId;
        newItem.attributes.id = previousId;
        newItem.attributes.customData.Id = previousId;
        return newItem;
      });

      let rapidLinks = workflow.ItemLinks.map(link => {
        let rapidLink = this.rappid.createLink();
        let sourceElement = rapidItems.find(item => item.attributes.id === link.FromId);
        let targetElement = rapidItems.find(item => item.attributes.id === link.ToId);

        if (sourceElement) {
          rapidLink.source(sourceElement);
        }

        if (targetElement) {
          rapidLink.target(targetElement);
        }
        return rapidLink;
      });
      return {cells: rapidItems.concat(rapidLinks)};
    }

  }

  // position of popUP
  private addTextConnectorsPosition(cellView) {
    this.linkHolder = cellView;
    const link = document.querySelector(`[model-id="${cellView.model.id}"]`);
    this.offset.top =
      link.getBoundingClientRect().top +
      link.getBoundingClientRect().height / 2;
    this.offset.left =
      link.getBoundingClientRect().left +
      link.getBoundingClientRect().width / 2;
    this.onToggle();
  }

  // #endregion
  close() {
    this.opened = false;
    this.gridPreData = [];
    this.gridPostData = [];
  }

  ngOnInit() {}

  ngAfterViewInit(): void {
    this.rappid = new KitchenSinkService(
      this.element.nativeElement as any,
      new StencilService(),
      new ToolbarService(),
      new InspectorService(),
      new HaloService(),
      new KeyboardService(),
      this.rapidEventsService,
      false,
      this.sessionService
    );
    this.rappid.startRappid();
    this.setEditMode(false);
    const that = this;

    this.rappid.graph.on("change:target change:source", params => {
      if (
        (params.attributes.target.id || params.attributes.source.id) &&
        !this.isLinkWorkflowClicked
      ) {
        this.onChangeLink(params);
      }

      if (params.attributes.target.id) {
        this.reloadWorkflowPaths();
      }
    });
    this.rappid.paper.on("link:pointerdblclick", function(cellView) {
      that.connectorShow = false;
      const conditionItem = that.rappid.graph.getCell(
        cellView.model.attributes.source.id
      );
      if (conditionItem.attributes.type === "standard.Polygon") {
        that.addTextConnectorsPosition(cellView);
      }
    });

    this.rappid.paper.on("cell:pointerdown", (elementView, evt) => {
      if (elementView.model.attributes.isInteractive === false) {
        this.rappid.paper.setInteractivity(false);
      } else if (this.editMode) {
        this.rappid.paper.setInteractivity(true);
      }
      const graph = this.rappid.getGraphJson();
      const imageElements = graph.cells.filter(
        el =>
          el.customData &&
          el.customData.parentElement &&
          el.customData.parentElement.id === elementView.model.id
      );
      if (imageElements && imageElements.length > 0) {
        imageElements.map(el => {
          const element = this.rappid.graph.getCell(el.id);
          elementView.model.embed(element);
        });
      } else if (
        elementView.model.attributes.attrs.root &&
        elementView.model.attributes.attrs.root.type === "Expand"
      ) {
        if (this.editMode) {
          const parentElement =
            elementView.model.attributes.customData.parentElement;
          this.onClickLinkedWorkflowElement(parentElement);
        }
      }
    });

    this.rappid.paper.on("cell:pointerup", (elementView, evt) => {
      const element =
        elementView && elementView.model && elementView.model.attributes;

      let models = [];
      if (elementView.model.collection) {
        models = elementView.model.collection.models;
      }

      if (
        element &&
        element.attrs &&
        element.attrs.root &&
        element.customData &&
        element.customData.ActionType &&
        element.attrs.root.type !== "Navigation Step" &&
        !models.find(
          el =>
            el.attributes.customData &&
            el.attributes.customData.parentElement &&
            elementView.model.id === el.attributes.customData.parentElement.id
        )
      ) {
        const image = this.createImage(elementView.model);
        this.rappid.graph.addCell(image);
        if (element.customData.IsNegativeStep) {
          const negativeElement = this.createNegativeStep(
            elementView.model.attributes
          );
          this.rappid.graph.addCell(negativeElement);
        }
      } else if (
        element &&
        element.attrs.root &&
        element.attrs.root.type === "Linked"
      ) {
        if (this.editMode) {
          const fork = <HTMLElement>(
            document.getElementsByClassName("handle fork ne")[0]
          );
          const clone = <HTMLElement>(
            document.getElementsByClassName("handle clone se")[0]
          );
          if (fork) {
            fork.style.display = "none";
          }
          if (clone) {
            clone.style.display = "none";
          }
        }
      }
      if (element && element.customData && element.customData.refId) {
        let link = models.find(
          el => el.attributes.target && el.attributes.target.id === element.id
        );
        let sourceElement = models.find(
          el =>
            link &&
            link.attributes.source &&
            link.attributes.source.id === el.id
        );
        if (!link && element.type !== "standard.Link") {
          delete element.customData.refId;
          delete element.customData.linkedParentId;
          delete element.customData.parentIds;
          this.isWorkflowChanged = true;
        } else if (
          sourceElement &&
          sourceElement.attributes.customData.refId === element.customData.refId
        ) {
          delete element.customData.refId;
          delete element.customData.linkedParentId;
          delete element.customData.parentIds;
          let infoEl = models.find(
            el =>
              el.attributes.customData &&
              el.attributes.customData.parentElement &&
              el.attributes.customData.parentElement.id === element.id
          );
          let elementsToRemove = [link, element, infoEl];
          let graph = this.rappid.getGraphJson();
          graph.cells = graph.cells.filter(
            cell => !elementsToRemove.find(el => el.id === cell.id)
          );
          this.addToUndoStack(graph.cells);
          this.workflowItemServie.workflowLinkageBroken({
            itemIds: [sourceElement.attributes.customData.linkedParentId],
            action: "copyAdded"
          });
        }
      }
    });

    this.rappid.graph.on("remove", (cell, collection, opt) => {
      if (opt.removeLinks) {
        this.removeAllLinks(cell);
      } else if (opt.selection) {
        this.removeSelectedItems(cell);
      } else if (!cell.isLink()) {
        this.removeCell(cell);
      } else {
        this.removeLink(cell);
      }

      if (this.reloadPathsAfterRemove(cell, opt)) {
        this.isWorkflowChanged = true;
        this.reloadWorkflowPaths();
      }
    });

    if (this.rappid.undoRedoClicked) {
      this.rappid.undoRedoClicked.pipe(takeWhileAlive(this)).subscribe(() => {
        this.reloadWorkflowPaths();
      });
    }
  }

  reloadPathsAfterRemove(cell, opt) {
    return (
      cell.attributes.type !== "standard.Image" &&
      !opt.selection &&
      (!cell.attributes.customData ||
        !cell.attributes.customData.linkedParentId) &&
      (cell.attributes.type !== "standard.Link" ||
        !this.rappid.graph.getCell(cell.attributes.source.id) ||
        !this.rappid.graph.getCell(cell.attributes.source.id).attributes
          .customData.linkedParentId)
    );
  }

  removeAllLinks(link) {
    if (link.lastLink) {
      let removedLinks = this.rappid.removedLinks.concat(link);
      this.addToUndoStack(
        this.rappid.getGraphJson().cells.concat(removedLinks)
      );
      if (
        removedLinks.find(
          link =>
            link.attributes.customData &&
            link.attributes.customData.linkedParentId
        )
      ) {
        this.workflowItemServie.workflowLinkageBroken({
          itemIds: [link.attributes.customData.linkedParentId],
          action: "cellRemoved"
        });
      }
      this.rappid.removedLinks = [];
    } else {
      this.rappid.removedLinks.push(link);
    }
  }

  removeSelectedItems(cell) {
    if (this.rappid.selectedItems.length > 0) {
      let selectedItems = this.rappid.selectedItems.map(el => el.attributes);
      this.rappid.selectedItems = [];
      let graph = this.rappid.getGraphJson();
      this.addToUndoStack(graph.cells.concat(cell));
      let imageCells = graph.cells.filter(
        el =>
          el.customData &&
          el.customData.parentElement &&
          selectedItems.find(
            item => el.customData.parentElement.id === item.id
          ) &&
          !selectedItems.find(item => item.id === el.id)
      );
      let sourceElements = graph.cells.filter(
        el =>
          el.source &&
          selectedItems.find(cell => el.source.id === cell.id) &&
          !selectedItems.find(cell => el.id === cell.id)
      );
      let targetElements = graph.cells.filter(
        el =>
          el.target &&
          selectedItems.find(cell => el.target.id === cell.id) &&
          !selectedItems.find(cell => cell.id === el.id)
      );
      let elementsToBeRemoved = [
        ...imageCells,
        ...sourceElements,
        ...targetElements,
        ...selectedItems
      ];
      let allRemovedElements = elementsToBeRemoved.concat(cell.attributes);
      let linkedParentElements = [];
      allRemovedElements.map(el => {
        if (
          el.customData &&
          el.customData.linkedParentId &&
          !linkedParentElements.find(
            linkedEl =>
              linkedEl.customData.linkedParentId ===
              el.customData.linkedParentId
          )
        ) {
          linkedParentElements.push(el);
        }
      });
      if (linkedParentElements.length > 0) {
        this.rappid.loadGraphJson({ cells: graph.cells.concat(cell) });
        this.workflowItemServie.workflowLinkageBroken({
          itemIds: linkedParentElements.map(el => el.customData.linkedParentId),
          action: "cellRemoved"
        });
      } else {
        graph.cells = graph.cells.filter(
          cell => !elementsToBeRemoved.find(el => el.id === cell.id)
        );
        this.rappid.loadGraphJson(graph);
        this.isWorkflowChanged = true;
        this.reloadWorkflowPaths();
      }
    }
  }

  removeCell(cell) {
    let graph = this.rappid.getGraphJson();

    let imageElements = graph.cells.filter(
      el =>
        el.customData &&
        el.customData.parentElement &&
        el.customData.parentElement.id === cell.id
    );

    if (cell.attributes.type !== "standard.Image") {
      let undoElements = this.getUndoStackWithEmbededEl(cell);
      this.addToUndoStack(graph.cells.concat(undoElements));
    }

    let sourceElements = graph.cells.filter(
      el => el.source && el.source.id === cell.attributes.id
    );
    let targetElements = graph.cells.filter(
      el => el.target && el.target.id === cell.attributes.id
    );

    graph.cells = graph.cells.filter(
      cell =>
        !targetElements.find(el => el.id === cell.id) &&
        !sourceElements.find(el => el.id === cell.id) &&
        (!imageElements || !imageElements.find(el => cell.id === el.id))
    );

    if (
      cell.attributes.customData &&
      cell.attributes.customData.Title === "Linked workflow"
    ) {
      this.removeLinkedElement(cell, graph);
    } else if (
      cell.attributes.customData &&
      cell.attributes.customData.linkedParentId
    ) {
      this.rappid.loadGraphJson(graph);
      this.workflowItemServie.workflowLinkageBroken({
        itemIds: [cell.attributes.customData.linkedParentId],
        action: "cellRemoved"
      });
    } else if (cell.attributes.type !== "standard.Image") {
      this.rappid.loadGraphJson(graph);
    }
  }

  removeLinkedElement(cell, graph) {
    if (cell.attributes.customData.linkedParentId) {
      this.rappid.loadGraphJson(graph);
      this.workflowItemServie.workflowLinkageBroken({
        itemIds: [cell.attributes.customData.linkedParentId],
        action: "cellRemoved"
      });
    } else {
      let children = graph.cells.filter(
        el =>
          el.customData &&
          el.customData.parentIds &&
          el.customData.parentIds.find(id => id === cell.attributes.id)
      );
      let newCells = graph.cells.filter(
        cell =>
          !children.find(
            child =>
              child.id === cell.id ||
              (cell.source && cell.source.id === child.id) ||
              (cell.target && cell.target.id === child.id)
          )
      );
      this.rappid.loadGraphJson({ cells: newCells });
      this.rappid.layoutGraph(false);
    }
  }

  getUndoStackWithEmbededEl(cell) {
    let undoElements = [];
    if (cell.attributes.customData.Title === "Linked workflow") {
      let expandEl = this.createExpandElement(cell.attributes);
      undoElements = [cell.attributes, expandEl];
    } else if (
      cell.attributes.customData.ActionType &&
      cell.attributes.attrs.root.type !== "Navigation Step"
    ) {
      let infoEl = this.createImage(cell);
      if (cell.attributes.customData.IsNegativeStep) {
        let negativeElement = this.createNegativeStep(cell.attributes);
        undoElements = [cell.attributes, infoEl, negativeElement];
      } else {
        undoElements = [cell.attributes, infoEl];
      }
    } else {
      undoElements = [cell.attributes];
    }
    return undoElements;
  }

  removeLink(link) {
    let graph = this.rappid.getGraphJson();
    graph.cells = graph.cells.concat(link);
    this.addToUndoStack(graph.cells);
    if (
      link.attributes.customData &&
      link.attributes.customData.linkedParentId
    ) {
      this.workflowItemServie.workflowLinkageBroken({
        itemIds: [link.attributes.customData.linkedParentId],
        action: "cellRemoved"
      });
    }
  }

  addToUndoStack(cells) {
    this.rappid.undoStack.push({
      graph: { cells: cells }
    });
  }

  treeViewItemSelected(treeViewItem: TreeViewItem) {
    if (
      !isNullOrUndefined(this.selectedTreeViewItem) &&
      this.selectedTreeViewItem.id === treeViewItem.id
    ) {
      return;
    }
    let cutCopy = treeViewItem.cutCopy;
    this.selectedTreeViewItem = { ...treeViewItem, cutCopy: false };
    if (this.editMode === false) {
      this.workflowToolbarComponent.updateToolbar(treeViewItem);
      this.loadWorkflow(treeViewItem, cutCopy);
      this.workflowItemServie.workflowSelectionChanged.next(
        treeViewItem.type === NodeType.Workflow ? treeViewItem.id : null
      );
    }
  }

  reloadFolder(treeViewItem: TreeViewItem) {
    this.treeViewComponent.reloadFolderItems(treeViewItem);
  }

  treeViewItemCreated(treeViewItem: TreeViewItem) {
    this.treeViewComponent.addTreeViewElement(treeViewItem);
  }

  treeViewItemDeleted(treeViewItem: TreeViewItem) {
    this.treeViewComponent.deleteTreeViewElement(treeViewItem);
    this.clearGraph();
    this.updateWorkflowItemPropertiesPanel(null);
  }

  treeViewItemRenamed(name: string) {
    this.selectedTreeViewItem.title = name;
    this.treeViewComponent.renameTreeViewElement(name);
  }

  treeViewItemCut(treeViewItem: TreeViewItem) {
    this.treeViewComponent.cutTreeViewElement(treeViewItem);
  }

  editClicked() {
    this.setEditMode(true);
  }

  cancelClicked() {
    this.loadWorkflow(this.selectedTreeViewItem);
    this.updateWorkflowItemPropertiesPanel(null);
    this.setEditMode(false);
  }

  treeViewItemMoved() {}

  disableTreeView(state: boolean) {
    this.treeViewComponent.disable(state);
  }

  onChangeLink(params) {
    if (
      params.attributes.source &&
      params.attributes.source.id &&
      params.attributes.target.id
    ) {
      const sourceId = params.attributes.source.id;
      const sourceCell = this.rappid.graph.getCell(sourceId);
      const targetId = params.attributes.target.id;
      const targetCell = this.rappid.graph.getCell(targetId);
      if (
        sourceCell.attributes.customData &&
        sourceCell.attributes.customData.linkedParentId
      ) {
        let graph = this.rappid.getGraphJson();
        graph.cells = graph.cells.filter(
          cell => cell.id !== params.attributes.id
        );
        this.addToUndoStack(graph.cells);
        this.workflowItemServie.workflowLinkageBroken({
          itemIds: [sourceCell.attributes.customData.linkedParentId],
          action: "linkChanged"
        });
      } else if (
        targetCell.attributes.customData &&
        targetCell.attributes.customData.linkedParentId
      ) {
        let graph = this.rappid.getGraphJson();
        graph.cells = graph.cells.filter(
          cell => cell.id !== params.attributes.id
        );
        this.addToUndoStack(graph.cells);
        this.workflowItemServie.workflowLinkageBroken({
          itemIds: [targetCell.attributes.customData.linkedParentId],
          action: "linkChanged"
        });
      } else {
        this.isWorkflowChanged = true;
      }
    }
  }

  issueClicked(result: any, skipStatus, others: {
      isCalledForWorkflowWithLinkedElements: boolean;
      versionStatus: number; } = null) {
    this.selectedItemToIssue = result;
    this.versionStatus = others ? others.versionStatus : null;
    this.isCalledForWorkflowWithLinkedElements = others ? others.isCalledForWorkflowWithLinkedElements : null;
    // If this item is linked on other workflows, ask user for confirmation
    if (!others && this.linkedWorkflows && this.linkedWorkflows.length > 0 && !skipStatus) {
      if (result.newVersion === true) {
        this.dialogForLinkedItemsNewVersionOpened = true;
      } else {
      this.issueClicked(this.selectedItemToIssue, false, {
        isCalledForWorkflowWithLinkedElements: true,
        versionStatus: 2
      }); }

    } else {
      const context = this.sessionService.getWorkContext();
      if (result.treeViewItem.id !== this.selectedTreeViewItem.id) {
        return;
      }
      // HighLighGraphHelper.unhighlightAll(this.rappid.graph);
      this.rappid.paper.toPNG((dataURL: string) => {
        this.designService
          .issueWorkflow(
            context.user.tenantId,
            context.project.id,
            context.subProject.id,
            context.defaultSettings.system.id,
            context.defaultSettings.catalog.id,
            this.selectedTreeViewItem.id,
            dataURL,
            result.newVersion,
            skipStatus,
            others && others.isCalledForWorkflowWithLinkedElements ? others.versionStatus : 2,
            others && others.isCalledForWorkflowWithLinkedElements ? this.selectedItemCurrentVersion : 0
          )
          .subscribe((res: any) => {
            if (
              others &&
              others.isCalledForWorkflowWithLinkedElements === true
            ) {
              this.closeDialogForLinkedItemsNewVersionOpened();
            }
            if (res) {
              if (res.response) {
                this.workflowWarningMessage = res.response.message;
                this.openWorkflowWarningDialog = true;
                this.issueWorkflow = true;
                this.workflowItemToBeIssued = result;
              } else {
                this.workflowItemServie.workflowSelectionChanged.next(
                  this.selectedTreeViewItem.id
                );
                this.messageService.sendMessage({
                  type: MessageType.Info,
                  text: `workflow ${this.selectedTreeViewItem.title} issued !`
                });
                if (this.openWorkflowWarningDialog) {
                  this.closeWorkflowWarningDialog();
                }
                this.designService
                  .postIssueWorkflow(
                    this.selectedTreeViewItem.id,
                    context.user.tenantId,
                    context.project.id,
                    context.subProject.id,
                    context.defaultSettings.system.id,
                    context.defaultSettings.catalog.id
                  )
                  .subscribe(() => {});
              }
            } else if (this.openWorkflowWarningDialog) {
              this.closeWorkflowWarningDialog();
            }
          });
      });
    }
  }

  issueClickedForWorkflowWithLinkedElements(versionStatus) {
    this.issueClicked(this.selectedItemToIssue, false, {
      isCalledForWorkflowWithLinkedElements: true,
      versionStatus: versionStatus
    });
  }

  public closeDialogForLinkedItemsNewVersionOpened() {
    this.dialogForLinkedItemsNewVersionOpened = false;
  }

  saveClicked(treeViewItem: TreeViewItem, showMessage = true, paths?) {
    if (treeViewItem.id !== this.selectedTreeViewItem.id) {
      return;
    }
    const currentGraph = this.rappid.getGraphJson();
    const graph = this.workflowService.getGraphToSave(currentGraph);
    const graphJson = JSON.stringify(graph);
    this.saveWorkflow(graphJson, showMessage, paths);
  }

  saveWorkflow(graphJson, showMessage?, paths?) {
    const pathsJson = !paths ? JSON.stringify({paths: this.workflowItemService.workflowPaths}) : JSON.stringify(paths);
    this.designService.saveWorkflow(this.selectedTreeViewItem.id, graphJson, pathsJson).subscribe(res => {
      if (res) {
        if (showMessage) {
          this.messageService.sendMessage({type: MessageType.Info, text: `Workflow ${this.selectedTreeViewItem.title} is saved!`});
        } else {
          this.loadWorkflow(this.selectedTreeViewItem);
        }
      }
      this.workflowToolbarComponent.resetMenu(MenuAction.Save);
      this.setEditMode(false);
    });
  }

  onDrop() {
    if (this.editMode === false) {
      return;
    }
    this.isLinkWorkflowDialogOpen = true;
  }

  public setWorkflow() {
    if (this.isCopyWorkflowClicked) {
      this.copyWorkflow();
    } else {
      this.linkWorkflow();
    }
  }

  public copyWorkflowClicked() {
    this.isCopyWorkflowClicked = true;
    this.isLinkWorkflowClicked = false;
  }

  public linkWorkflowClicked() {
    this.isCopyWorkflowClicked = false;
    this.isLinkWorkflowClicked = true;
  }

  copyWorkflow() {
    this.addToUndoStack(this.rappid.getGraphJson().cells);
    this.designService
      .getWorkflow(this.treeViewComponent.draggedItem)
      .subscribe(res => {
        let selected = (res.designerJson && JSON.parse(res.designerJson)) || {
          cells: []
        };
        const existing = this.rappid.getGraphJson();
        if (selected.cells.length === 0) {
          this.closeLinkWorkflowDialog();
          return;
        }

        selected = this.getCopiedCells(selected);
        // update links

        existing.cells.push(...selected.cells);
        this.rappid.loadGraphJson(existing);
        // update custom id with the new id

        const json = JSON.stringify({
          cells: existing.cells
        });
        document.getElementById("btn-layout").click();
        this.workflowItemServie.loadSuggestedPaths.next(json);
        this.closeLinkWorkflowDialog();
      });
  }

  getCopiedCells(designerJson) {
    designerJson.cells.forEach(element => {
      const newId = Guid.newGuid();
      const sourceToChange = designerJson.cells.filter(
        source => source.source && source.source.id === element.id
      );
      const targetToChange = designerJson.cells.filter(
        target => target.target && target.target.id === element.id
      );
      if (element.id) {
        element.previousId = element.id;
        element.id = newId;
      }
      sourceToChange.forEach(el => (el.source.id = newId));
      targetToChange.forEach(el => (el.target.id = newId));
    });
    designerJson.cells.forEach(x => {
      if (x && x.customData) {
        x.customData.Id = x.id;
        if (x.customData.DynamicDatas && x.customData.DynamicDatas.length > 0) {
          x.customData.DynamicDatas.forEach(item => {
            const element = designerJson.cells.find(
              el => el.previousId === item.SourceWorkflowItemId
            );
            if (element) {
              item.SourceWorkflowItemId = element.id;
            }
          });
        }
      }
    });

    return designerJson;
  }

  linkWorkflow() {
    if (this.selectedTreeViewItem.id === this.treeViewComponent.draggedItem) {
      this.closeLinkWorkflowDialog();
      this.messageService.sendMessage({
        text: "You can not link a workflow with itself!",
        type: MessageType.Error
      });
      return;
    }
    this.addToUndoStack(this.rappid.getGraphJson().cells);
    this.designService
      .getWorkflow(this.treeViewComponent.draggedItem)
      .subscribe(async res => {
        const cells = this.rappid.graph.getCells();
        const paths = this.workflowItemServie.workflowPaths;
        const selectedPaths = paths.filter(path => path.selected);
        const allItemsId = this.returnPathItemIds(selectedPaths);
        const newElement = this.rappid.createLinkedWorkflowElement(res);
        newElement.attributes.customData.Id = newElement.attributes.id;
        this.rappid.graph.addCell(newElement);
        this.resizeWorkflowElement(res.title, newElement);
        for (let i = 0; i < cells.length; i++) {
          if (
            !cells.find(
              el =>
                el.attributes.source &&
                el.attributes.source.id &&
                el.attributes.source.id === cells[i].attributes.id
            ) &&
            allItemsId.find(
              id =>
                cells[i].attributes.id === id ||
                (cells[i].attributes.customData &&
                  cells[i].attributes.customData.refId === id)
            ) &&
            cells[i].attributes.type !== "standard.Image" &&
            cells[i].attributes.type !== "standard.Link"
          ) {
            const linkToAdd = this.rappid.createLink();
            this.rappid.graph.addCell(linkToAdd);
            linkToAdd.source(cells[i]);
            linkToAdd.target(newElement);
          }
        }
        this.closeLinkWorkflowDialog();
        this.rappid.layoutGraph(true);
      });
  }

  returnPathItemIds(paths) {
    let allItemsId = [];
    paths.map(path => {
      allItemsId = allItemsId.concat(path.itemIds);
    });
    return allItemsId;
  }

  createExpandElement(parentElement) {
    const expand = new joint.shapes.standard.Image({
      type: "standard.Image",
      isInteractive: false,
      attrs: {
        image: {
          xlinkHref: !parentElement.customData.isExpanded
            ? "assets/actionicons/expand.png"
            : "assets/actionicons/collapse.png",
          width: 25,
          height: 25
        },
        root: {
          type: "Expand"
        }
      },
      size: {
        width: 25,
        height: 25
      },
      customData: {
        parentElement: parentElement
      },
      position: {
        x: parentElement.position.x,
        y: parentElement.position.y - 30
      }
    });
    return expand;
  }

  onClickLinkedWorkflowElement(linkedElement) {
    if (!linkedElement.customData.isExpanded) {
      this.loadingExpand = true;
      this.expandLinkedWorkflowElement(linkedElement);
    } else {
      this.loadingExpand = true;
      this.collapseLinkedWorkflowElement(linkedElement);
    }
  }

  collapseLinkedWorkflowElement(linkedElement) {
    let cells = [];
    let graph = this.rappid.getGraphJson();
    this.addToUndoStack(graph.cells);
    let children = graph.cells.filter(
      cell =>
        cell.customData &&
        cell.customData.parentIds &&
        cell.customData.parentIds.find(id => id === linkedElement.id)
    );
    graph.cells.map(cell => {
      let sourceItem = children.find(
        step => cell.source && step.id === cell.source.id
      );
      let targetItem = !children.find(
        step => cell.target && step.id === cell.target.id
      );
      if (sourceItem && targetItem) {
        let existsTarget = cells.find(
          element =>
            element.source &&
            element.target &&
            element.target.id === cell.target.id &&
            element.source.id === linkedElement.id
        );
        if (!existsTarget) {
          cells.push({
            ...cell,
            id: Guid.newGuid(),
            source: { id: linkedElement.id }
          });
        }
      } else if (cell.id === linkedElement.id) {
        cell.customData.isExpanded = false;
        cells.push(cell);
      } else {
        cells.push(cell);
      }
    });
    let newCells = cells.filter(
      cell =>
        cell.type !== "standard.Image" &&
        !children.find(child => cell.id === child.id)
    );
    this.rappid.loadGraphJson({ cells: newCells });
    this.rappid.layoutGraph(false);
    this.loadingExpand = false;
    this.highlightLinkedElements();
  }

  getLinkedWorkflowSteps(linkedElement, sourceElements) {
    const versionId = isNullOrUndefined(linkedElement.customData.versionId) ? 
      linkedElement.customData.VersionId : linkedElement.customData.versionId;
    return this.designService
      .getWorkflowByVersion(
        linkedElement.customData.WorkflowId,
        versionId
      )
      .map(res => {
        let designerJson = (res &&
          res.designerJson &&
          JSON.parse(res.designerJson)) || { cells: [] };
        let pathsJson = (res && res.pathsJson && JSON.parse(res.pathsJson)) || {
          paths: []
        };
        if (!res.isParsed) {
          designerJson = this.getParsedGraph(designerJson);
          pathsJson = {
            paths: pathsJson.paths.map((path, i) => ({ ...path, number: i }))
          };
        }
        const itemIds = this.returnPathItemIds(pathsJson.paths);
        let linkedWorkflowCells = designerJson.cells.map(cell =>
          cell.attributes ? cell.attributes : cell
        );
        let linkedElementParentIds =
          (linkedElement.customData && linkedElement.customData.parentIds) ||
          [];
        let childrenElementParentIds = linkedElementParentIds.concat(
          linkedElement.id
        );

        let startElement = linkedWorkflowCells.find(
          cell =>
            cell.customData &&
            cell.customData.Title === "Start" &&
            cell.customData.Type === WorkflowItemType.Start
        );

        linkedWorkflowCells = linkedWorkflowCells.filter(
          cell =>
            ((!startElement || cell.id !== startElement.id) &&
              itemIds.find(
                itemId =>
                  itemId === cell.id ||
                  (cell.target && cell.target.id === itemId)
              )) ||
            sourceElements.find(el => el.id === cell.id)
        );

        let newCustomData = {
          linkedParentId: linkedElement.id,
          parentIds: childrenElementParentIds
        };

        let newLinkedWorkflowCells = linkedWorkflowCells.map(cell => {
          let newId = Guid.newGuid();
          let customData = cell.customData
            ? {
                ...cell.customData,
                ...newCustomData,
                Id: newId,
                refId: cell.id
              }
            : {
                ...newCustomData,
                refId: cell.id
              };
          if (
            cell.source &&
            startElement &&
            cell.source.id === startElement.id
          ) {
            return {
              ...cell,
              id: newId,
              source: { id: linkedElement.id },
              customData: customData
            };
          }
          return {
            ...cell,
            id: newId,
            customData: customData
          };
        });

        let linkedCells = newLinkedWorkflowCells.map(cell => {
          if (cell.type === "standard.Link") {
            let sourceRefCell = newLinkedWorkflowCells.find(
              el => el.customData.refId === cell.source.id
            );
            let targetRefCell = newLinkedWorkflowCells.find(
              el => el.customData.refId === cell.target.id
            );
            if (
              (!sourceRefCell || !targetRefCell) &&
              cell.source.id !== linkedElement.id
            ) {
              cell.remove = true;
            }
            if (sourceRefCell) {
              cell.source.id = sourceRefCell.id;
            }
            if (targetRefCell) {
              cell.target.id = targetRefCell.id;
            }
          }
          return cell;
        });

        return {
          cells: linkedCells.filter(cell => !cell.remove),
          paths: pathsJson.paths
        };
      });
  }

  setGraphWithLinkedWorkflow(
    linkedElement,
    paths,
    sourceElements,
    currentWorkflowCells,
    linkedWorkflowCells
  ) {
    linkedWorkflowCells.map(linkedCell => {
      if (
        !linkedWorkflowCells.find(
          cell => cell.source && cell.source.id === linkedCell.id
        ) &&
        !linkedCell.source
      ) {
        sourceElements.map(el => {
          currentWorkflowCells = currentWorkflowCells.filter(
            cell => cell.id !== el.attributes.id
          );
          linkedWorkflowCells.push({
            ...el.attributes,
            id: Guid.newGuid(),
            source: { id: linkedCell.id }
          });
        });
      }
    });

    let currentElement = this.rappid.graph.getCell(linkedElement.id);

    if (currentElement) {
      currentElement.attributes.customData.isExpanded = true;
      currentElement.attributes.customData.Steps = linkedWorkflowCells;
      currentElement.attributes.customData.Paths = paths;
    }

    this.addDynamicDataForLinkedElements(currentElement, linkedWorkflowCells);
    let allCells = currentWorkflowCells.concat(linkedWorkflowCells);
    this.rappid.loadGraphJson({ cells: allCells });
    this.rappid.layoutGraph(false, false);
    this.loadingExpand = false;
  }

  expandLinkedWorkflowElement(linkedElement) {
    this.addToUndoStack(this.rappid.getGraphJson().cells);
    const currentWorkflowCells = this.rappid.graph.getCells();

    const sourceElements = currentWorkflowCells.filter(
      el =>
        el.attributes.source &&
        el.attributes.target &&
        el.attributes.target.id &&
        el.attributes.source.id === linkedElement.id
    );

    this.getLinkedWorkflowSteps(linkedElement, sourceElements).subscribe(
      linkedWorkflowCells => {
        this.setGraphWithLinkedWorkflow(
          linkedElement,
          linkedWorkflowCells.paths,
          sourceElements,
          currentWorkflowCells,
          linkedWorkflowCells.cells
        );
        this.highlightLinkedElements();
      }
    );
  }

  highlightLinkedElements() {
    let selectedPaths = this.workflowItemService.workflowPaths.filter(
      x => x.selected === true
    );
    this.workflowItemService.workflowHighlight.next(selectedPaths);
  }

  updateDynamicData(graph, paths){
    graph.cells =graph.cells.map(row =>{
      if (row && row.customData && row.customData.DynamicDatas && row.customData.DynamicDatas.length){
        row.customData.DynamicDatas = row.customData.DynamicDatas.filter(dynamic =>{
          let currentPath = paths.find(path => path.title === dynamic.PathName);
          if (currentPath && currentPath.itemIds){
            return currentPath.itemIds.find(row => row === dynamic.SourceWorkflowItemId) ? true : false;
          }
          return false;
        });
      }
      if (this.workflowItemService.workflowItemSelected &&
        row.id === this.workflowItemService.workflowItemSelected.id) {
        this.workflowItemService.workflowItemSelected.customData.DynamicDatas = row.customData.DynamicDatas;
        return {...this.workflowItemService.workflowItemSelected, position: {x: row.position.x, y: row.position.y}};
      } else {
        return row;
      }
    });
    this.rappid.loadGraphJson(graph);
  }

  addDynamicDataForLinkedElements(parentElement, linkedCells) {
    parentElement = parentElement.attributes
      ? parentElement.attributes
      : parentElement;
    let linkedPaths = parentElement.customData.Paths;
    linkedCells.map(cell => {
      if (
        cell.customData &&
        cell.customData.DynamicDatas &&
        cell.customData.DynamicDatas.length > 0
      ) {
        cell.customData.PreviousDynamicDatas = cell.customData
          .PreviousDynamicDatas
          ? cell.customData.PreviousDynamicDatas
          : cell.customData.DynamicDatas;
        let dynamicDatas = [];
        cell.customData.PreviousDynamicDatas.map(dynamicData => {
          let linkedPath = linkedPaths.find(
            path => path.title === dynamicData.PathName
          );
          let linkedPathsItems = linkedPath ? linkedPath.itemIds : [];
          this.workflowItemService.workflowPaths.map(workflowPath => {
            let matchIds = workflowPath.itemIds.filter(id =>
              linkedPathsItems.find(linkedPathId => linkedPathId === id)
            );
            if (
              matchIds.length === linkedPathsItems.length &&
              !dynamicDatas.find(
                dynData => dynData.PathName === workflowPath.title
              )
            ) {
              dynamicDatas.push({
                ...dynamicData,
                PathName: workflowPath.title
              });
            }
          });
        });
        cell.customData.DynamicDatas = dynamicDatas;
      }
    });
  }

  resizeWorkflowElement(title, element) {
    let oldLength = element.attributes.size.width;
    const oldX = element.attributes.position.x;
    let length = title.length * 10;

    if (length < 200) {
      length = 200;
      oldLength = 200;
    }
    const center = (oldX + oldLength) / 2;
    const margin = -((oldX + length) / 2 - center);
    element.resize(length, 40);
    element.translate(margin, 0);
  }

  private closeLinkWorkflowDialog() {
    this.isLinkWorkflowDialogOpen = false;
    this.isCopyWorkflowClicked = true;
    this.isLinkWorkflowClicked = false;
  }

  onToggle(state?): void {
    this.rappid.addTextToConnector(this.linkHolder, state);
    this.connectorShow = !this.connectorShow;
    this.toggleConnectorText = this.connectorShow ? "Hidе" : "Show";
  }

  public closeEditWorkflowDialog() {
    this.isEditWorkflowDialogOpen = false;
    this.workflowItemServie.closeEditWorkflowPopup.next(
      this.breakLinkageAction
    );
  }

  breakLinkage() {
    let graph = this.rappid.getGraphJson();
    let newGraph = graph;
    this.workflowItemServie.continueBreakLinkage.next(this.breakLinkageAction);
    for (let i = 0; i < this.breakLinkageItems.length; i++) {
      let item = this.breakLinkageItems[i];
      newGraph = this.breakLinkageByElement(newGraph, item);
    }
    this.rappid.loadGraphJson(newGraph);
    this.rappid.layoutGraph(false);
    this.breakLinkageItems = [];
    this.breakLinkageAction = "";
    this.isEditWorkflowDialogOpen = false;
    this.isWorkflowChanged = true;
    delete this.workflowItemService.workflowItemSelected.customData.linkedParentId;
    delete this.workflowItemService.workflowItemSelected.customData.parentIds;
    delete this.workflowItemService.workflowItemSelected.customData.refId;
    this.reloadWorkflowPaths();
  }

  breakLinkageByElement(graph, modifiedCell) {
    let sourceElements = graph.cells.filter(
      cell => cell.target && cell.target.id === modifiedCell.id
    );

    let targetElements = graph.cells.filter(
      cell => cell.source && cell.source.id === modifiedCell.id
    );

    let cells = graph.cells.filter(
      cell =>
        cell.id !== modifiedCell.id &&
        (!cell.customData ||
          !cell.customData.parentElement ||
          cell.customData.parentElement.id !== modifiedCell.id) &&
        !sourceElements.find(link => link.id === cell.id) &&
        !targetElements.find(link => link.id === cell.id)
    );

    let cellsWithDynData = this.getCellsWithDynamicData(cells);

    let newCells = this.getCellsWithoutLinkageRef(
      cellsWithDynData,
      modifiedCell
    );

    for (let i = 0; i < sourceElements.length; i++) {
      for (let j = 0; j < targetElements.length; j++) {
        const sourceElement = sourceElements[i];
        const targetElement = targetElements[j];
        const newLink = {
          ...sourceElement,
          id: Guid.newGuid(),
          source: { id: sourceElement.source.id },
          target: { id: targetElement.target.id }
        };
        newCells.push(newLink);
      }
    }

    if (modifiedCell.customData && modifiedCell.customData.linkedParentId) {
      const parentElement = graph.cells.find(
        cell => cell.id === modifiedCell.customData.linkedParentId
      );

      if (parentElement) {
        const newGraph = { cells: newCells };
        return this.breakLinkageByElement(newGraph, parentElement);
      } else {
        return { cells: newCells };
      }
    } else {
      return { cells: newCells };
    }
  }

  getCellsWithDynamicData(cells) {
    return cells.map(cell => {
      let dynamicDatas = [];
      if (cell.customData && cell.customData.DynamicDatas) {
        if (
          (this.breakLinkageAction === "dynChanged" ||
            this.breakLinkageAction === "dynDeleted") &&
          cell.id === this.workflowItemServie.workflowItemSelected.id
        ) {
          dynamicDatas = this.workflowItemServie.workflowItemSelected.customData
            .DynamicDatas;
        } else {
          dynamicDatas = cell.customData.DynamicDatas;
        }
        cell.customData.DynamicDatas = dynamicDatas.map(data => {
          let refEl = cells.find(
            el =>
              el.customData && el.customData.refId === data.SourceWorkflowItemId
          );
          if (refEl) {
            return { ...data, SourceWorkflowItemId: refEl.id };
          } else {
            return data;
          }
        });
      }
      if (cell.customData && cell.customData.PostConstraints && cell.customData.PostConstraints.DynamicData) {
        cell.customData.PostConstraints.DynamicData = cell.customData.PostConstraints.DynamicData.map(data => {
          let refEl = cells.find(
            el =>
              el.customData && el.customData.refId === data.SourceWorkflowItemId
          );
          if (refEl) {
            return { ...data, SourceWorkflowItemId: refEl.id };
          } else {
            return data;
          }
        });
      }
      if (cell.customData && cell.customData.Constraints && cell.customData.Constraints.DynamicData) {
        cell.customData.Constraints.DynamicData = cell.customData.Constraints.DynamicData.map(data => {
          let refEl = cells.find(
            el =>
              el.customData && el.customData.refId === data.SourceWorkflowItemId
          );
          if (refEl) {
            return { ...data, SourceWorkflowItemId: refEl.id };
          } else {
            return data;
          }
        });
      }
      return cell;
    });
  }

  getCellsWithoutLinkageRef(cells, modifiedCell) {
    return cells.map(cell => {
      if (
        cell.customData &&
        cell.customData.linkedParentId === modifiedCell.id
      ) {
        if (!cell.customData.Id) {
          delete cell.customData;
        } else {
          delete cell.customData.linkedParentId;
          delete cell.customData.parentIds;
          delete cell.customData.refId;
        }
      }
      return cell;
    });
  }

  saveWorkflowChanges() {
    const currentGraph = this.rappid.getGraphJson();
    const graph = this.workflowService.getGraphToSave(currentGraph);
    this.isWorkflowChanged = false;
    this.saveWorkflow(JSON.stringify(graph));
  }

  closeNegativeStepDialog() {
    this.isNegativeStepDialogOpen = false;
  }

  changeExpectedError(event) {
    let expectedError = event.target.value.trim("");
    this.negativeTestStep.ExpectedError = expectedError;
  }

  saveExpectedError() {
    this.selectedWorkflowItem.customData.ExpectedError = this.negativeTestStep.ExpectedError;
    this.closeNegativeStepDialog();
  }

  changeStepSkip() {
    if (this.issueWorkflow) {
      this.issueClicked(
        this.workflowItemToBeIssued,
        true,
        this.versionStatus
          ? {
              isCalledForWorkflowWithLinkedElements: this
                .isCalledForWorkflowWithLinkedElements,
              versionStatus: this.versionStatus
            }
          : null
      );
    } else {
      this.workflowItemServie.rollbackWorkflow.next(
        this.selectedWorkflowVersion
      );
      this.openWorkflowWarningDialog = false;
    }
  }

  closeWorkflowWarningDialog() {
    if (!this.issueWorkflow) {
      this.workflowItemServie.closeRollbackDialog.next();
    }
    this.openWorkflowWarningDialog = false;
    this.issueWorkflow = false;
  }

  timeVarianceClicked(selectedTreeViewItem) {
    this.selectedTimeVarianceItem = selectedTreeViewItem;
    this.showTimeVariance = true;
  }

  closeTimeVariance() {
    this.showTimeVariance = false;
  }

  setLoading(event) {
    this.loading = event;
  }
}
