import { DataFlowService } from './../../../data-management/services/data-flow.service';
import { SignalRService } from './../../../product/services/signalr.service';
import { Component, EventEmitter, OnInit, Output, ViewChild, TemplateRef, ElementRef, AfterViewInit, Input } from '@angular/core';
import { SubProject } from 'src/app/core/models/sub-project.model';
import { AdministrationService } from 'src/app/core/services/administration.service';
import { SessionService } from 'src/app/core/services/session.service';
import { NodeType } from 'src/app/shared/node-type';
import { TreeViewItem } from 'src/app/shared/tree-view-item';
import { isNullOrUndefined } from 'util';
import { MenuAction } from '../../models/menu-action';
import { MessageType } from 'src/app/core/models/message.model';
import { DesignService } from '../../services/design.service';
import { Folder } from '../../../core/models/folder.model';
import { MessageService } from '../../../core/services/message.service';
import { Router } from '@angular/router';
import { MenuItem } from '../../models/menu-item.model';
import { MenuComponent } from '@progress/kendo-angular-menu';
import { WindowService } from '@progress/kendo-angular-dialog';
import { State, process } from '@progress/kendo-data-query';
import { SelectionEvent } from '@progress/kendo-angular-grid';
import { Guid } from 'src/app/shared/guid';
import { ImportArisService } from '../../services/import-aris.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { takeWhileAlive, AutoUnsubscribe } from 'take-while-alive';
import { LicenseService } from 'src/app/core/services/licenses.service';
import {Licenses} from 'src/app/core/models/licenses.model';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-workflow-toolbar',
  templateUrl: './workflow-toolbar.component.html',
  styleUrls: ['./workflow-toolbar.component.css']
})
@AutoUnsubscribe()
export class WorkflowToolbarComponent implements OnInit {
  public license = Licenses;
  constructor(
    private router: Router,
    private session: SessionService,
    private administrationService: AdministrationService,
    private designService: DesignService,
    private messageService: MessageService,
    private sessionService: SessionService,
    private windowService: WindowService,
    private importArisService: ImportArisService,
    private signalRService: SignalRService,
    private configuration: ConfigurationService,
    private dataFlow: DataFlowService,
    public licenseService: LicenseService,
    public permissionService: PermissionService
  ) {
    this.isPopupOpen = false;
    this.inProcessModel = new ToolbarActionModel();
    this.allMenuItems = [
      new MenuItem([NodeType.Root], MenuAction.NewFolder, true),
      new MenuItem([NodeType.Folder], MenuAction.NewWorkflow, true),
      new MenuItem([NodeType.Workflow], MenuAction.Edit, true),
      new MenuItem([NodeType.Workflow], MenuAction.Save, false),
      new MenuItem([NodeType.Workflow], MenuAction.Cancel, false),
      new MenuItem([NodeType.Folder, NodeType.Workflow], MenuAction.Rename, true),
      new MenuItem([NodeType.Workflow], MenuAction.Copy, true),
      new MenuItem([NodeType.Folder], MenuAction.Paste, false),
      new MenuItem([NodeType.Workflow], MenuAction.Cut, true),
      new MenuItem([NodeType.Folder, NodeType.Workflow], MenuAction.Delete, true),
      new MenuItem([NodeType.Workflow], MenuAction.Issue, true),
      // new MenuItem([NodeType.Workflow], MenuAction.ViewCoverage, true),
      new MenuItem([NodeType.Workflow], MenuAction.DownloadVideo, false)
    ];
    this.dataFlow.workflowRecorded$.pipe(takeWhileAlive(this)).subscribe(_workflowId => {
      this.recordingOpen = false;
      this.treeViewItemCreated.emit(new TreeViewItem(_workflowId, this.recordedTitle, NodeType.Workflow));
      this.menuClickOnWorkflow(MenuAction.Edit);
    });
    this.dataFlow.workflowCancelled$.pipe(takeWhileAlive(this)).subscribe(_workflowId => {
      this.recordingOpen = false;
      this.messageService.sendMessage({ text: 'Workflow recording was stopped!', type: MessageType.Error });
    });
  }

  @Input() rappid;
  public menuItems: MenuItem[];
  public recordedTitle = '';
  private allMenuItems: MenuItem[];
  private selectedTreeViewItem: TreeViewItem;
  private _subProject: SubProject;
  public copyCutModel: CopyCutModel;
  public radioOptions = ['Blank', 'Import'];
  public newIssue = true;
  public inProcessModel: ToolbarActionModel;
  public isPopupOpen: Boolean;
  public newWorkflowOpen = false;
  public typicals = [];
  public generatedWorkflows = [];
  public selectedTypical;
  public selectedCatalog;
  public generatedWorkflowTitle = '';
  public recordingOpen = false;
  public selectableSettings = {
    checkboxOnly: true,
    mode: 'multiple'
  };
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 5,
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public workflowToPreview;
  public selectedWorkflows = [];
  public selectedDataSetsIds = [];
  public gridData;
  public file: File;

  public isDialogOpen: Boolean;
  public isIssueOpen: Boolean;
  public dialogOpened = false;
  public windowOpened = false;
  public timeVarianceVisible = false;
  public isViewCoverageOpen = false;
  public openComputerGeneratedDialog = false;
  public isComputerAssistedChecked = true;
  public isComputerGeneratedChecked = false;
  public isRecordChecked = false;
  public isImportedChecked = false;
  public showLoadingMessage = false;
  public workflowCoverage = 0;
  public dataCoverage = 0;
  @Output() public reloadFolder = new EventEmitter<TreeViewItem>();
  @Output() public treeViewItemCreated = new EventEmitter<TreeViewItem>();
  @Output() public treeViewItemDeleted = new EventEmitter<TreeViewItem>();
  @Output() public treeViewItemRenamed = new EventEmitter<string>();
  @Output() public disableTreeView = new EventEmitter<boolean>();
  @Output() public treeViewItemCut = new EventEmitter<TreeViewItem>();
  @Output() public saveClicked = new EventEmitter<TreeViewItem>();
  @Output() public issueClicked = new EventEmitter<any>();
  @Output() public editClicked = new EventEmitter<boolean>();
  @Output() public cancelClicked = new EventEmitter<any>();
  @Output() public varianceClicked = new EventEmitter<any>();
  @Output() public loading = new EventEmitter<any>();
  @ViewChild('menu') menu: MenuComponent;

  ngOnInit() {
    this.menuItems= this.allMenuItems.filter(item=>{
      if(this.permissionService.hasPermission('/api/Administration/AddFolder') && item.types.filter(t => t === NodeType.Root).length > 0 ) {
        return true;
      }
      return false;
    })
  }

  updateToolbar(treeViewItem: TreeViewItem) {
    this.selectedTreeViewItem = treeViewItem;
    //this.menuItems = [...this.allMenuItems.filter(m => m.types.filter(t => t === treeViewItem.type).length > 0)];
    this.menuItems= [...this.allMenuItems.filter(m=>{
      if(m.types.filter(t => t === treeViewItem.type).length > 0){
        return this.hasPermission(treeViewItem, m);
      }
      return false;
    })]
    this.setPasteVisibility(this.inProcessModel.action === MenuAction.Cut || this.inProcessModel.action === MenuAction.Copy);
    this.timeVarianceVisible = treeViewItem.type === NodeType.Workflow && this.permissionService.hasPermission('/api/Design/GetWorkflowVersionsByDate');
    if (treeViewItem.type === NodeType.Workflow) {
      this.enableDisableDownloadMenu();
    }
  }


  hasPermission(treeViewItem, menuItem){
    if(treeViewItem.type===NodeType.Root){
        return this.hasPermissionRoot();
    }
    else if (treeViewItem.type===NodeType.Folder){
      return this.hasPermissiononSingleItemFolder(menuItem);
    }
    else if(treeViewItem.type===NodeType.Workflow) {
      return this.hasPermissionSingleItemWorkflow(menuItem);
    }
    return false;
  }

  hasPermissionRoot(){
    if(this.permissionService.hasPermission('/api/Administration/AddFolder')){
      return true;
    }
    return false;
  }

  hasPermissiononSingleItemFolder(menuItem:MenuItem){
    if(menuItem.menuAction==MenuAction.NewWorkflow && this.permissionService.hasPermission('/api/Design/AddWorkflow')) return true ;
    else if (menuItem.menuAction==MenuAction.Rename && this.permissionService.hasPermission('/api/Administration/UpdateFolder')) return true ;
    else if (menuItem.menuAction===MenuAction.Paste && this.permissionService.hasPermission('/api/Design/PasteWorkflow')) return true ;
    else if (menuItem.menuAction===MenuAction.Delete && this.permissionService.hasPermission('/api/Administration/DeleteFolder')) return true ;
    return false;
  }

  hasPermissionSingleItemWorkflow(menuItem:MenuItem) {
    if(menuItem.menuAction===MenuAction.Edit && this.permissionService.hasPermission('/api/Design/SaveWorkflow')) return true ;
    else if(menuItem.menuAction===MenuAction.Save && this.permissionService.hasPermission('/api/Design/SaveWorkflow')) return true ;
    else if(menuItem.menuAction===MenuAction.Cancel && this.permissionService.hasPermission('/api/Design/SaveWorkflow')) return true ;
    else if(menuItem.menuAction===MenuAction.Rename && this.permissionService.hasPermission('/api/Design/RenameWorkflow')) return true ;
    else if(menuItem.menuAction===MenuAction.Copy && this.permissionService.hasPermission('/api/Design/PasteWorkflow')) return true ;
    else if(menuItem.menuAction===MenuAction.Cut && this.permissionService.hasPermission('/api/Design/PasteWorkflow')) return true ;
    else if(menuItem.menuAction===MenuAction.Delete && this.permissionService.hasPermission('/api/Design/DeleteWorkflow')) return true ;
    else if(menuItem.menuAction===MenuAction.Issue && this.permissionService.hasPermission('/api/Design/IssueWorkflow')) return true ;
    else if(menuItem.menuAction===MenuAction.DownloadVideo && this.permissionService.hasPermission('/api/Design/DownloadVideo')) return true ;
    else if(menuItem.menuAction===MenuAction.ViewCoverage && this.permissionService.hasPermission('/api/Design/GetWorkflowCoverage')) return true ;
    return false ;
  }

  private enableDisableDownloadMenu() {
    const downloadMenu = this.menuItems.filter(m => m.menuAction === MenuAction.DownloadVideo)[0];
    if(downloadMenu){
      downloadMenu.isActive = false;
      this.designService.hasVideo(this.selectedTreeViewItem.id).subscribe((x: any) => {
        if (x.hasVideo) {
          downloadMenu.isActive = true;
        }
      });
    }
  }

  resetMenu(menuAction: MenuAction) {
    if (menuAction === MenuAction.Save || menuAction === MenuAction.Cancel) {
      this.menuItems.forEach(item => {
        if (item.menuAction === MenuAction.Save || item.menuAction === MenuAction.Cancel) {
          item.isActive = false;
        } else {
          item.isActive = true;
        }
      });
      this.enableDisableDownloadMenu();
    }
    this.disableTreeView.emit(false);
  }

  menuItemClick(itemClicked) {
    if (this.isPopupOpen) {
      return;
    }
    const menuActionClicked = itemClicked.data.menuAction;

    if (this.selectedTreeViewItem.type === NodeType.Root) {
      this.menuClickOnRoot(menuActionClicked);
    } else if (this.selectedTreeViewItem.type === NodeType.Folder) {
      this.menuClickOnFolder(menuActionClicked);
    } else if (this.selectedTreeViewItem.type === NodeType.Workflow) {
      this.menuClickOnWorkflow(menuActionClicked);
    }
  }

  timeVarianceClicked() {
    this.varianceClicked.emit(this.selectedTreeViewItem);
  }

  public menuClickOnWorkflow(menuAction: MenuAction) {
    this.inProcessModel.action = menuAction;
    if (menuAction === MenuAction.Rename) {
      this.disableTreeView.emit(true);
      this.inProcessModel.popupTitle = 'Rename workflow';
      this.inProcessModel.text = this.selectedTreeViewItem.title;
      this.isPopupOpen = true;
    } else if (menuAction === MenuAction.Delete) {
      this.disableTreeView.emit(true);
      this.inProcessModel.popupTitle = 'Are you sure you want to delete : ' + this.selectedTreeViewItem.title + ' Workflow ?';
      this.isDialogOpen = true;
    } else if (menuAction === MenuAction.Cut || menuAction === MenuAction.Copy) {
      if (menuAction === MenuAction.Cut) {
        this.messageService.sendMessage({type: MessageType.Info, text: `Workflow ${this.selectedTreeViewItem.title} moved!`});
      } else if (menuAction === MenuAction.Copy) {
        this.messageService.sendMessage({type: MessageType.Info, text: `Workflow ${this.selectedTreeViewItem.title} copied!`});
      }
      this.copyCutModel = new CopyCutModel(menuAction, this.selectedTreeViewItem);
      this.inProcessModel.action = menuAction;
    } else if (menuAction === MenuAction.Edit) {
      // this.disableTreeView.emit(true);
      this.editClicked.emit();
      this.menuItems.forEach(item => {
        if (item.menuAction === MenuAction.Save || item.menuAction === MenuAction.Cancel) {
          item.isActive = true;
        } else {
          item.isActive = false;
        }
      });
    } else if (menuAction === MenuAction.Save) {
      this.saveClicked.emit(this.selectedTreeViewItem);
    } else if (menuAction === MenuAction.Cancel) {
      this.cancelClicked.emit(TreeViewItem);
      this.resetMenu(menuAction);
    } else if (menuAction === MenuAction.Issue && MenuAction.Edit) {
      this.disableTreeView.emit(true);
      this.inProcessModel.popupTitle = 'Are you sure you want to Issue : ' + this.selectedTreeViewItem.title + ' Workflow ?';
      this.isIssueOpen = true;
      this.newIssue = true;
    } else if (menuAction === MenuAction.ViewCoverage) {
      this.setCoverage();
    } else if (menuAction === MenuAction.DownloadVideo) {
      this.downloadVideo();
    }
  }

  private menuClickOnFolder(menuAction: MenuAction) {
    this.inProcessModel.action = menuAction;
    if (menuAction === MenuAction.NewWorkflow) {
      this.disableTreeView.emit(true);
      this.newWorkflowOpen = true;
    } else if (menuAction === MenuAction.NewFolder) {
      this.disableTreeView.emit(true);
      this.inProcessModel.text = this.selectedTreeViewItem.title;
      this.isPopupOpen = true;
    } else if (menuAction === MenuAction.Rename) {
      this.disableTreeView.emit(true);
      this.inProcessModel.popupTitle = 'Rename Folder';
      this.inProcessModel.text = this.selectedTreeViewItem.title;
      this.isPopupOpen = true;
    } else if (menuAction === MenuAction.Delete) {
      this.disableTreeView.emit(true);
      this.inProcessModel.popupTitle = 'Are you sure you want to delete : ' + this.selectedTreeViewItem.title + ' Folder ?';
      this.isDialogOpen = true;
    } else if (menuAction === MenuAction.Paste) {
      this.designService
        .pasteWorkflow(this.copyCutModel.treeViewItem.id, this.selectedTreeViewItem.id, this.copyCutModel.type)
        .subscribe(res => {
          this.treeViewItemCreated.emit(new TreeViewItem(res.id, res.title, NodeType.Workflow, [], true));
        });
      if (this.copyCutModel.menuAction === MenuAction.Cut) {
        this.copyCutModel = new CopyCutModel(null, null);
        this.setPasteVisibility(false);
      }
      this.inProcessModel.action = null;
      if (this.copyCutModel.menuAction === MenuAction.Cut) {
        this.messageService.sendMessage({ type: MessageType.Info, text: `Workflow ${this.selectedTreeViewItem.title} is moved!` });
      }
      if (this.copyCutModel.menuAction) {
        this.messageService.sendMessage({ type: MessageType.Info, text: `Workflow ${this.selectedTreeViewItem.title} is copied!` });
      }
    }
  }

  private menuClickOnRoot(menuAction: MenuAction) {
    if (menuAction === MenuAction.NewFolder) {
      this.inProcessModel.popupTitle = 'Create a new folder';
      this.inProcessModel.action = MenuAction.NewFolder;
      this.isPopupOpen = true;
      this.disableTreeView.emit(true);
    }
  }

  submit() {
    if (this.selectedTreeViewItem.type === NodeType.Root) {
      this.processActionOnRoot();
    } else if (this.selectedTreeViewItem.type === NodeType.Folder) {
      this.processActionOnFolder();
    } else if (this.selectedTreeViewItem.type === NodeType.Workflow) {
      this.processActionOnWorkflow();
    }
  }

  public newclicked() {
    this.newIssue = true;
  }

  public updatedclicked() {
    this.newIssue = false;
  }

  private processActionOnWorkflow() {
    if (this.inProcessModel.action === MenuAction.Rename) {
      this.designService.updateWorkflow(this.inProcessModel.text, this.selectedTreeViewItem.id).subscribe(result => {
        this.treeViewItemRenamed.emit(this.inProcessModel.text);
        this.selectedTreeViewItem.title = this.inProcessModel.text;
        this.close();
      });
    } else if (this.inProcessModel.action === MenuAction.Delete) {
      this.designService.deleteWorkflow(this.selectedTreeViewItem.id).subscribe(res => {
        this.treeViewItemDeleted.emit(new TreeViewItem(this.selectedTreeViewItem.id, this.inProcessModel.text, NodeType.Workflow));
        this.close();
      });
    } else if (this.inProcessModel.action === MenuAction.Issue) {
      this.issueClicked.emit({ treeViewItem: this.selectedTreeViewItem, newVersion: this.newIssue });
      this.close();
    }
  }

  private processActionOnFolder() {
    if (this.inProcessModel.action === MenuAction.Rename) {
      this.administrationService.updateFolder(this.inProcessModel.text, this.selectedTreeViewItem.id).subscribe(res => {
        this.selectedTreeViewItem.title = this.inProcessModel.text;
        this.close();
      });
    } else if (this.inProcessModel.action === MenuAction.NewWorkflow) {
      if (this.isImportedChecked) {
        if (!this.showLoadingMessage) {
          this.showLoadingMessage = true;
          let catalog = this.sessionService.getDefaultSettings().catalog;
          let system = this.sessionService.getDefaultSettings().system;
          let formData = new FormData();
          formData.append('file', this.file);
          this.importArisService
            .importAris(formData, this.selectedTreeViewItem.id, system.id ? system.id : Guid.empty, catalog.id ? catalog.id : Guid.empty)
            .subscribe(
              res => {
                this.close();
                if (res) {
                  this.messageService.sendMessage({ text: 'Workflow was imported successfully!', type: MessageType.Success });
                }
                this.reloadFolder.emit(this.selectedTreeViewItem);
              },
              err => {
                this.close();
              }
            );
        }
      } else {
        let title = this.inProcessModel.text;
        this.designService.addWorkflow(this.inProcessModel.text, this.selectedTreeViewItem.id).subscribe(res => {
          if (this.isRecordChecked) {
            this.recordedTitle = this.inProcessModel.text;
            this.close();
            const tenantId = this.sessionService.getUser().tenantId;
            const projectId = this.sessionService.getProject().id;
            const subProjectId = this.sessionService.getSubProject().id;
            const system = this.sessionService.getDefaultSettings().system;
            const catalog = this.sessionService.getDefaultSettings().catalog;
            const token = this.sessionService.getToken();
            const href =
              'valtestengine:senseai?TenantId=' +
              tenantId +
              '&ProjectId=' +
              projectId +
              '&SubProjectId=' +
              subProjectId +
              '&FolderId=' +
              this.selectedTreeViewItem.id +
              '&CatalogId=' +
              catalog.id +
              '&SystemId=' +
              system.id +
              '&WorkflowId=' +
              res.id +
              '&WorkflowTitle=' +
              title +
              '&ApiBaseURL=' +
              this.configuration.serverSettings.apiUrl.substr(0, this.configuration.serverSettings.apiUrl.length - 1) +
              '&token=' +
              token;

            this.recordingOpen = true;
            this.signalRService.startConnection();
            this.signalRService.SaveRecordedWorkflow();
            this.signalRService.CancelRecordedWorkflow();
            window.location.href = href;
            this.close();
            this.isRecordChecked = false;
          } else {
            this.treeViewItemCreated.emit(new TreeViewItem(res.id, title, NodeType.Workflow));
            this.close();
            this.menuClickOnWorkflow(MenuAction.Edit);
          }
        });
      }
    } else if (this.inProcessModel.action === MenuAction.Delete) {
      this.administrationService.deleteFolder(this.selectedTreeViewItem.id).subscribe(res => {
        this.treeViewItemDeleted.emit(new TreeViewItem(this.selectedTreeViewItem.id, this.inProcessModel.text, NodeType.Folder));
        this.close();
      });
    }
  }

  setCoverage() {
    const defaultSettings = this.session.getDefaultSettings();
    this.designService
      .getWorkflowCoverage(
        this.session.getUser().tenantId,
        this.session.getProject().id,
        this.session.getSubProject().id,
        defaultSettings.system.id,
        defaultSettings.catalog.id,
        this.selectedTreeViewItem.id
      )
      .subscribe(res => {
        this.workflowCoverage = res.workflowCoverage;
        this.dataCoverage = res.dataCoverage;
        this.isViewCoverageOpen = true;
      });
  }
  downloadVideo() {
    this.loading.emit(true);
    this.designService.downloadVideo(this.selectedTreeViewItem.id).subscribe(
      (x: any) => {
        const newBlob = new Blob([x], { type: 'application/vxml' });
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }
        const data = window.URL.createObjectURL(newBlob);
        const link = document.createElement('a');
        link.href = data;
        link.download = this.selectedTreeViewItem.id + '.mp4';
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
        setTimeout(() => {
          window.URL.revokeObjectURL(data);
          link.remove();
          this.loading.emit(false);
        }, 100);
      },
      error => {
        this.loading.emit(false);
        this.messageService.sendMessage({ text: 'There was an error during the download of the file!', type: MessageType.Error });
      }
    );
  }
  computerAssistedChecked() {
    this.isComputerAssistedChecked = true;
    this.isComputerGeneratedChecked = false;
    this.isImportedChecked = false;
    this.isRecordChecked = false;
  }

  computerGeneratedChecked() {
    this.isComputerAssistedChecked = false;
    this.isComputerGeneratedChecked = true;
    this.isImportedChecked = false;
    this.isRecordChecked = false;
  }

  importChecked() {
    this.isComputerAssistedChecked = false;
    this.isComputerGeneratedChecked = false;
    this.isImportedChecked = true;
    this.isRecordChecked = false;
  }
  recordWorkflow() {
    this.isComputerAssistedChecked = false;
    this.isComputerGeneratedChecked = false;
    this.isImportedChecked = false;
    this.isRecordChecked = true;
  }

  disableGenerateBtn() {
    return this.generatedWorkflowTitle.trim() === '' || !this.selectedTypical;
  }

  changeTitle(event) {
    this.generatedWorkflowTitle = event.target.value;
  }

  changeTypical(typical) {
    this.selectedTypical = typical;
  }

  private processActionOnRoot() {
    if (this.inProcessModel.action === MenuAction.NewFolder) {
      this.administrationService
        .addFolder({ subProjectId: this.getSubProject().id, title: this.inProcessModel.text } as Folder)
        .subscribe(res => {
          this.treeViewItemCreated.emit(new TreeViewItem(res.id, res.title, NodeType.Folder));
          this.close();
        });
    }
  }

  close(isImported?, isRecorded?) {
    this.disableTreeView.emit(false);
    this.newWorkflowOpen = false;
    this.isPopupOpen = false;
    this.isDialogOpen = false;
    this.isIssueOpen = false;
    this.isViewCoverageOpen = false;
    if (!isImported && !isRecorded) {
      this.isComputerAssistedChecked = true;
      this.isComputerGeneratedChecked = false;
      this.isImportedChecked = false;
      this.isRecordChecked = false;
    }
    this.showLoadingMessage = false;
    this.inProcessModel = new ToolbarActionModel();
  }

  next() {
    if (this.isComputerGeneratedChecked) {
      let catalog = this.sessionService.getDefaultSettings().catalog;
      this.selectedCatalog = catalog;
      if (!catalog.id) {
        this.messageService.sendMessage({ text: 'The default catalog has not been selected!', type: MessageType.Warning });
        this.close();
        return;
      }
      if(this.permissionService.hasPermission('/api/Design/SearchTypicals')){
        this.designService.searchTypicals('', catalog.id).subscribe(typicals => {
          this.typicals = typicals;
          this.openComputerGeneratedDialog = true;
          this.close();
        });
      }
      else {
          this.typicals = [];
          this.openComputerGeneratedDialog = true;
          this.close();
      }
    } else {
      this.close(this.isImportedChecked, this.isRecordChecked);
      this.inProcessModel.action = MenuAction.NewWorkflow;
      this.inProcessModel.popupTitle = 'New Workflow: Type';
      this.isPopupOpen = true;
    }
  }

  closeRecording() {
    this.recordingOpen = false;
  }

  onChangeFile(event) {
    let file = event.target.files[0];
    if (file) {
      this.file = file;
    }
  }

  public disableCreateBtn() {
    return !this.selectedWorkflows || this.selectedWorkflows.length === 0;
  }

  public handleChange(search) {
    if (this.selectedCatalog && this.selectedCatalog.id && this.permissionService.hasPermission('/api/Design/SearchTypicals')) {
      this.designService.searchTypicals(search, this.selectedCatalog.id).subscribe(typicals => {
        this.typicals = typicals;
      });
    }
  }

  private getSubProject(): SubProject {
    if (isNullOrUndefined(this._subProject)) {
      this._subProject = this.session.getWorkContext().subProject;
    }
    return this._subProject;
  }

  setPasteVisibility(active: boolean) {
    const paste = this.menuItems.find(item => item.menuAction === MenuAction.Paste);
    if (isNullOrUndefined(paste)) {
      return;
    }
    paste.isActive = active;
  }

  closeComputerGeneratedDialog() {
    this.openComputerGeneratedDialog = false;
    this.selectedTypical = null;
    this.generatedWorkflowTitle = '';
    this.generatedWorkflows = [];
    this.resetGrid();
  }

  resetGridState() {
    this.gridState = {
      sort: [],
      skip: 0,
      take: 5,
      filter: {
        logic: 'and',
        filters: []
      }
    };
  }

  resetGrid() {
    this.gridData = null;
    this.selectedWorkflows = [];
    this.selectedDataSetsIds = [];
    this.resetGridState();
  }

  createWorkflows() {
    let workflows = this.selectedWorkflows.map(workflow => {
      let designerJson = this.getGraphForComputerGeneratedWorkflows(workflow);
      return {
        title: this.generatedWorkflowTitle,
        description: '',
        workflowPathsJson: JSON.stringify({ paths: workflow.workflowPath.map((path, i) => ({ ...path, title: 'Path ' + (i + 1) })) }),
        folderId: this.selectedTreeViewItem.id,
        json: designerJson
      };
    });
    this.designService.saveComputerGeneratedWorkflows(workflows).subscribe(res => {
      if (res) {
        this.messageService.sendMessage({ text: 'Workflows were sucessfully saved!', type: MessageType.Success });
      }
      this.reloadFolder.emit(this.selectedTreeViewItem);
      this.closeComputerGeneratedDialog();
    });
  }

  getGraphForComputerGeneratedWorkflows(workflow) {
    let rapidItems = workflow.workflowItem.map(item => {
      if (item.actionType === -1) {
        delete item.actionType;
      }

      for (let key in item) {
        let upper = key.charAt(0).toUpperCase() + key.substring(1);
        if (upper !== key) {
          item[upper] = item[key];
          delete item[key];
        }
      }
      let previousId = item.Id;
      let newItem = this.rappid.createRapidElement(item);
      newItem.id = previousId;
      newItem.attributes.id = previousId;
      newItem.attributes.customData.Id = previousId;
      return newItem;
    });

    let rapidLinks = workflow.workflowItemLink.map(link => {
      let rapidLink = this.rappid.createLink();
      let sourceElement = rapidItems.find(item => item.attributes.id === link.sourceId);
      let targetElement = rapidItems.find(item => item.attributes.id === link.targetId);

      if (sourceElement) {
        rapidLink.source(sourceElement);
      }

      if (targetElement) {
        rapidLink.target(targetElement);
      }

      return rapidLink;
    });

    let graph = { cells: rapidItems.concat(rapidLinks) };
    return JSON.stringify(graph);
  }

  generateWorkflows() {
    this.resetGrid();
    let tenantId = this.sessionService.getUser().tenantId;
    let projectId = this.sessionService.getProject().id;
    let subProjectId = this.sessionService.getSubProject().id;
    let system = this.sessionService.getDefaultSettings().system;
    let catalog = this.sessionService.getDefaultSettings().catalog;
    this.designService
      .getComputerGeneratedWorkflows(
        tenantId,
        projectId,
        subProjectId,
        system && system.id ? system.id : Guid.empty,
        catalog && catalog.id ? catalog.id : Guid.empty,
        this.generatedWorkflowTitle,
        this.selectedTypical.id,
        this.selectedTypical.title
      )
      .subscribe(res => {
        if (res && res.workflows) {
          let generatedWorkflows = res.workflows.map((workflow, i) => {
            let steps = workflow.workflowItem.length;
            let paths = workflow.workflowPath.length;
            return {
              ...workflow,
              id: i,
              title:
                workflow.workflowTitle +
                '  (' +
                steps +
                (steps === 1 ? ' Step, ' : ' Steps, ') +
                paths +
                (paths === 1 ? ' Path)' : ' Paths)')
            };
          });
          this.generatedWorkflows = generatedWorkflows;
          this.gridData = process(generatedWorkflows, this.gridState);
        }
      });
  }

  onStateChange(state: State) {
    this.gridState = state;
    this.gridData = process(this.generatedWorkflows, state);
  }

  hasRecordingPermission() {
    return (this.licenseService.hasLicense(this.license.Recording) && 
    this.permissionService.hasPermission('/api/notificationhub') && 
    this.permissionService.hasPermission('/api/Design/SaveRecordedWorkflow') &&
    this.permissionService.hasPermission('/api/Design/SaveVideo') &&
    this.permissionService.hasPermission('/api/Design/DownloadVideo') &&
    this.permissionService.hasPermission('/api/Design/HasVideo') &&
    this.permissionService.hasPermission('/api/Design/CancelRecordedWorkflow') &&
    this.permissionService.hasPermission('/api/Data/AddVEnquiry'));
  }

  selectionChanged($event: SelectionEvent) {
    let selectedWorkflows = [];
    if ($event.selectedRows.length > 0) {
      selectedWorkflows = this.selectedWorkflows.concat($event.selectedRows.map(row => row.dataItem));
    } else if ($event.deselectedRows.length > 0) {
      selectedWorkflows = this.selectedWorkflows.filter(workflow => !$event.deselectedRows.find(row => row.dataItem.id === workflow.id));
    }
    this.selectedDataSetsIds = selectedWorkflows.map(row => row.id);
    this.selectedWorkflows = selectedWorkflows;
  }

  previewWorkflow(dataItem: any, template: TemplateRef<any>) {
    this.workflowToPreview = dataItem;
    this.windowService.open({
      title: `Preview Workflow`,
      content: template,
      width: 1200,
      height: 850
    });
  }
}

export class ToolbarActionModel {
  popupTitle: string;
  action: MenuAction;
  text: string;
  worklowTypeSelected: string;
}

export class CopyCutModel {
  // according to API type 0 = Copy and and Cut =1
  menuAction: MenuAction;
  type: number;
  treeViewItem: TreeViewItem;

  constructor(menuAction: MenuAction, treeViewItem: TreeViewItem) {
    if (menuAction === MenuAction.Copy) {
      this.menuAction = menuAction;
      this.type = 0;
    } else if (menuAction === MenuAction.Cut) {
      this.menuAction = menuAction;
      this.type = 1;
    }
    this.treeViewItem = treeViewItem;
  }
}
