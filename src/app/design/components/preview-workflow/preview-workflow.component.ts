import { Component, OnInit, AfterViewInit, ElementRef, Input } from '@angular/core';
import { InspectorService } from './../../../design/services/rapid-services/inspector-service';
import { StencilService } from 'src/app/design/services/rapid-services/stencil-service';
import { ToolbarService } from 'src/app/design/services/rapid-services/toolbar-service';
import { HaloService } from 'src/app/design/services/rapid-services/halo-service';
import { KeyboardService } from 'src/app/design/services/rapid-services/keyboard-service';
import { KitchenSinkService } from 'src/app/design/services/rapid-services/kitchensink-service';
import { RapidEventsService } from 'src/app/design/services/rapid-events.service';
import { SessionService } from 'src/app/core/services/session.service';

@Component({
  selector: 'app-preview-workflow',
  templateUrl: './preview-workflow.component.html',
  styleUrls: ['./preview-workflow.component.css']
})
export class PreviewWorkflowComponent implements OnInit, AfterViewInit {
  private rappid: KitchenSinkService;
  @Input() public workflow: any;

  constructor(
    private rapidEventsService: RapidEventsService,
    private element: ElementRef,
    private sessionService: SessionService,
  ) {}

  ngOnInit(): void {
    this.rappid = new KitchenSinkService(
      this.element.nativeElement as any,
      new StencilService(),
      new ToolbarService(),
      new InspectorService(),
      new HaloService(),
      new KeyboardService(),
      this.rapidEventsService,
      false,
      this.sessionService
    );

    this.rappid.startRappid();
    this.loadWorkflow();
  }

  ngAfterViewInit(): void {}

  async loadWorkflow() {

    let rapidItems = this.workflow.workflowItem.map(item => {
        if (item.actionType === -1) {
          delete item.actionType;
        }

        for(let key in item){
            let upper = key.charAt(0).toUpperCase() + key.substring(1);
            if( upper !== key ){ 
                item[ upper ] = item[key];
                delete item[key];
            }
        }
        return this.rappid.createRapidElement({...item, PreviousId: item.Id});
    });
    
    let rapidLinks = this.workflow.workflowItemLink.map(link => {
        let rapidLink = this.rappid.createLink();
        let sourceElement = rapidItems.find(item => item.attributes.customData.PreviousId === link.sourceId);
        let targetElement = rapidItems.find(item => item.attributes.customData.PreviousId === link.targetId);

        if (sourceElement) {
            rapidLink.source(sourceElement);
        }

        if (targetElement) {
            rapidLink.target(targetElement);
        }

        return rapidLink;
    });

    let graph = {cells: rapidItems.concat(rapidLinks)};
    this.rappid.loadGraphJson(graph);
    this.rappid.layoutGraph(false, true);
  }
}
   
