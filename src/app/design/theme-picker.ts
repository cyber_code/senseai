import * as joint from '../../vendor/rappid';
import * as _ from 'lodash';

namespace ThemePicker {
  export interface MainView {
    commandManager: joint.dia.CommandManager;
    paper: joint.dia.Paper;
    graph: joint.dia.Graph;
  }
}

export class ThemePicker extends joint.ui.Toolbar {
  constructor(options: { mainView: ThemePicker.MainView }) {
    super({
      className:
        _.result(joint.ui.Toolbar.prototype, 'className') + ' theme-picker'
    });

    this.mainView = options.mainView;
  }

  options: {
    tools: Array<{ [key: string]: any }>;
  };

  mainView: ThemePicker.MainView;

  init() {
    const options = [
      {value: 'modern', content: 'Modern'},
      {value: 'dark', content: 'Dark'},
      {value: 'material', content: 'Material'}
    ];

    const themes = {
      type: 'select-button-group',
      name: 'theme-picker',
      multi: false,
      selected: _.findIndex(options, {value: this.defaultTheme}),
      options,
      attrs: {
        '.joint-select-button-group': {
          'data-tooltip': 'Change Theme',
          'data-tooltip-position': 'bottom'
        }
      }
    };

    this.options.tools = [themes];
    this.on('theme-picker:option:select', this.onThemeSelected, this);

    super.init();
  }

  onThemeSelected(option: any) {
    joint.setTheme(option.value);
    if (this.mainView) {
      this.adjustAppToTheme(this.mainView, option.value);
    }
  }

  adjustAppToTheme(app: ThemePicker.MainView, theme: string) {
    app.commandManager.stopListening();

    const linkColor = theme === 'dark' ? '#f6f6f6' : '#222138';

    const themedLinks = app.graph.getLinks();
    const defaultLink = app.paper.options.defaultLink;
    if (defaultLink instanceof joint.dia.Link) {
      themedLinks.push(defaultLink);
    }

    _.invoke(themedLinks, 'attr', {
      '.connection': {stroke: linkColor},
      '.marker-target': {fill: linkColor},
      '.marker-source': {fill: linkColor}
    });

    // Material design has no grid shown.
    if (theme === 'material') {
      app.paper.options.drawGrid = false;
      app.paper.clearGrid();
    } else {
      app.paper.options.drawGrid = true;
      app.paper.drawGrid();
    }

    app.commandManager.listen();
  }
}
