import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { SessionService } from 'src/app/core/services/session.service';
import {MessageService} from 'src/app/core/services/message.service';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';

@Injectable()
export class ImportArisService {
  private designUrl: string;

  constructor(
        private configuration: ConfigurationService,
        private http: HttpClient,
        private sessionService: SessionService,
        private httpExecutorService: HttpExecutorService
    ) {
      this.designUrl = `${configuration.serverSettings.apiUrl}Design/`;
  }


  importAris(formModel, folderId, systemId, catalogId): Observable<any> {
    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    const url = this.designUrl + 'ImportArisWorkflow?folderId=' + folderId + '&SystemId=' + systemId + '&CatalogId=' + catalogId;
    return this.http.post(url, formModel, {headers : headers}).pipe(
        map((res: any) => {
          return this.httpExecutorService.handleResponse(url, res);
        }),
        catchError(error => {
          return this.httpExecutorService.handleError(error);
        })
    );
  }
 }

