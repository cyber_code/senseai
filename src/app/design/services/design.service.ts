import {SessionService} from './../../core/services/session.service';
import {DynamicData} from './../models/workflow-item-custom-data.model';
import {Observable, of, Subject} from 'rxjs';
import {map, filter} from 'rxjs/operators';
import {Folder} from 'src/app/core/models/folder.model';
import {HttpExecutorService} from 'src/app/core/services/http-executor.service';
import {Workflow} from '../models/workflow.model';
import {
  AddWorkflow,
  SaveWorkflow,
  RenameWorkflow,
  CutWorkflow,
  PasteWorkflow,
  DeleteWorkflow,
  IssueWorkflow,
  GetWorkflows,
  SearchWorkflows,
  GetWorkflow,
  GetSystems,
  GetCatalogs,
  GetTenantCatalogs,
  AddTenantCatalog,
  AddProjectCatalog,
  AddSubProjectCatalog,
  UpdateCatalog,
  GetCatalog,
  GetSystemCatalog,
  AddSystem,
  UpdateSystem,
  DeleteSystem,
  GetActionItem,
  GetConditionItem,
  SearchAttributes,
  SearchTypicals,
  GetTypicals,
  GetWorkflowVersion,
  GetWorkflowVersions,
  GetSuggestions,
  GetWorkflowCoverage,
  RollbackWorkflow,
  GetWorkflowVersionsByDate,
  GetAllNavigations,
  GetPaths,
  GetPreviousWorkflowItems,
  GetDynamicDataSuggestions,
  SaveDynamicDataSuggestions,
  SearchAttributesByCatalogIdAndTypicalName,
  PostIssueWorkflow,
  GetEnquiryActions,
  GetWorkflowByTitle,
  DeleteCatalog,
  UpdateSystemCatalog,
  AddSystemCatalog,
  GetCatalogSystem,
  AddTypicalManually,
  UpdateTypicalManually,
  DeleteTypicalManually,
  GetSystemTags,
  GetSystem,
  AddSystemTag,
  UpdateSystemTag,
  DeleteSystemTag,
  GetComputerGeneratedWorkflows,
  SaveComputerGeneratedWorkflows,
  HasVideo,
  AddHierarchyType,
  UpdateHierarchyType,
  DeleteHierarchyType,
  GetHierarchyTypes,
  GetAdapters,
  GetTypicalAttributes,
  AddTypicalAttributeManually,
  UpdateTypicalAttributeManually, DeleteTypicalAttributeManually, GetWorkflowByVersion
} from '../commands-queries/workflow-command-queries';
import {CommandMethod} from 'src/app/shared/command-method';
import {ConfigurationService} from 'src/app/core/services/configuration.service';
import {TreeViewItem} from 'src/app/shared/tree-view-item';
import {NodeType} from 'src/app/shared/node-type';
import {GetFolders} from 'src/app/core/services/command-queries/command-queries';
import {Injectable} from '@angular/core';
import {isNullOrUndefined} from 'util';
import {System} from '../models/system.model';
import {Catalog} from '../models/catalog.model';
import {ActionItems} from '../models/actionItem.model';
import {ConditionItem} from '../models/conditionalItem.model';
import {Attribute} from '../models/attribute.model';
import {Typical} from '../models/typical.model';
import {WorkflowVersion} from 'src/app/design/models/workflow-version.model';
import {Cacheable, CacheBuster} from 'ngx-cacheable';
import {ListSuggestions} from 'src/app/core/models/listsuggestions.model';
import {Suggestion} from 'src/app/core/models/suggestion.model';
import {Menu} from '../models/menu.model';
import {WorkflowGraphPath} from '../models/workflow-graph-path.model';
import {WorkflowItemCustomData} from '../models/workflow-item-custom-data.model';
import {WorkflowItemType} from 'src/app/core/models/workflow-item-type';
import {Guid} from 'src/app/shared/guid';
import {SystemTag} from '../models/system-tag.model';
import {HierarchyType} from '../../process-managment/models/hierarchy-type.model';
import {IssueType} from '../../process-managment/models/issue-type.model';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Adapter} from '../models/adapter.model';
import { EnquiryAction } from '../models/enquiry-action.model';

const workflowVersionNotifier = new Subject<void>();

@Injectable()
export class DesignService {
  private designUrl: string;
  private dataUrl: string;
  private administrationUrl: string;
  private configureUrl: string;

  private subject = new Subject<any>();


  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService,
    private http: HttpClient
  ) {
    this.designUrl = `${configurationService.serverSettings.apiUrl}Design/`;
    this.administrationUrl = `${
      configurationService.serverSettings.apiUrl
      }Administration/`;
    this.dataUrl = `${configurationService.serverSettings.apiUrl}Data/`;
    this.configureUrl = `${configurationService.serverSettings.apiUrl}Process/Configure/`;
  }

  sendMessage(message: string) {
    this.subject.next({text: message});
  }


  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  getAllFolders(subProjectId: string): Observable<Folder[]> {
    return this.httpExecutor.executeQuery<Folder[]>(
      this.designUrl,
      new GetFolders(subProjectId)
    );
  }

  getWorkflows(folderId: string): Observable<Workflow[]> {
    return this.httpExecutor.executeQuery<any>(
      this.designUrl,
      new GetWorkflows(folderId)
    );
  }

  searchWorkflows(
    searchString: string,
    subProjectId: string
  ): Observable<Workflow[]> {
    return this.httpExecutor.executeQuery<Workflow[]>(
      this.designUrl,
      new SearchWorkflows(searchString, subProjectId)
    );
  }

  fetchWorkflows(folderId: string, isIssued?: boolean, generatedTestCases?: boolean): Observable<TreeViewItem[]> {
    return this.getWorkflows(folderId).pipe(
      map(workflows => {
        return workflows.map(
          item => new TreeViewItem(item.id, item.title, NodeType.Workflow, null, null, item.status, item.isIssued, item.generatedTestCases)
        ).filter(row =>{
          if (isIssued)
            return row.isIssued === true;
          if (generatedTestCases)
            return row.generatedTestCases === true;
          return true;
        });
      })
    );
  }

  addWorkflow(
    title: string,
    folderId: string,
    description?: string
  ): Observable<Workflow> {
    if (isNullOrUndefined(description)) {
      description = '';
    }
    return this.httpExecutor.executeCommand<Workflow>(
      this.designUrl,
      new AddWorkflow(title, folderId, description),
      CommandMethod.POST
    );
  }

  updateWorkflow(title: string, workflowId: string): Observable<Workflow> {
    return this.httpExecutor.executeCommand<Workflow>(
      this.designUrl,
      new RenameWorkflow(title, workflowId),
      CommandMethod.PUT
    );
  }

  cutWorkflow(workflowId: string) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.designUrl,
      new CutWorkflow(workflowId),
      CommandMethod.POST
    );
  }

  pasteWorkflow(
    workflowId: string,
    folderId: string,
    type: number
  ): Observable<Workflow> {
    return this.httpExecutor
      .executeCommand<Workflow>(
        this.designUrl,
        new PasteWorkflow(workflowId, folderId, type),
        CommandMethod.POST
      )
      .map(workflow => {
        const d = new Workflow();
        d.id = workflow['workflowId'];
        d.title = workflow['workflowName'];
        return d;
      });
  }

  deleteWorkflow(workflowId: string): Observable<Workflow> {
    return this.httpExecutor.executeCommand<Workflow>(
      this.designUrl,
      new DeleteWorkflow(workflowId),
      CommandMethod.DELETE
    );
  }

  getComputerGeneratedWorkflows(
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    workflowTitle: string,
    typicalId: string,
    typicalTitle: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.designUrl,
      new GetComputerGeneratedWorkflows(
        tenantId,
        projectId,
        subProjectId,
        systemId,
        catalogId,
        workflowTitle,
        typicalId,
        typicalTitle
      ),
      CommandMethod.POST
    );
  }

  saveComputerGeneratedWorkflows(workflows: any): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.designUrl,
      new SaveComputerGeneratedWorkflows(workflows),
      CommandMethod.POST
    );
  }

  @CacheBuster({
    cacheBusterNotifier: workflowVersionNotifier
  })
  issueWorkflow(
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    workflowId: string,
    workflowImage: string,
    newVersion: boolean,
    skipStatus: boolean,
    versionStatus: number = 0,
    currentVersionId: number = 0
  ): Observable<Workflow> {
    return this.httpExecutor.executeCommand<Workflow>(
      this.designUrl,
      new IssueWorkflow(
        tenantId,
        projectId,
        subProjectId,
        systemId,
        catalogId,
        workflowId,
        workflowImage,
        newVersion,
        skipStatus,
        versionStatus,
        currentVersionId
        ),
      CommandMethod.POST
    );
  }

  postIssueWorkflow(
    workflowId: string,
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string
  ): Observable<Workflow> {
    return this.httpExecutor.executeCommand<Workflow>(
      this.designUrl,
      new PostIssueWorkflow(
        workflowId,
        tenantId,
        projectId,
        subProjectId,
        systemId,
        catalogId
      ),
      CommandMethod.POST
    );
  }

  saveWorkflow(workflowId: string, json: string, workflowPathsJson: string) {
    return this.httpExecutor.executeCommand<Workflow>(
      this.designUrl,
      new SaveWorkflow(workflowId, json, workflowPathsJson),
      CommandMethod.PUT
    );
  }

  getWorkflow(workflowId: string): Observable<Workflow> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetWorkflow(workflowId)
    );
  }

  getWorkflowByVersion(workflowId: string, versionId: number): Observable<Workflow> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetWorkflowByVersion(workflowId, versionId)
    );
  }

  getWf(workflowId: string): Observable<any> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetWorkflow(workflowId)
    );
  }

  getDynamicDataSuggestions(
    SourceTypicalName: string,
    TargetTypicalName: string
  ): Observable<DynamicData[]> {
    return this.httpExecutor
      .executeQuery<any[]>(
        this.designUrl,
        new GetDynamicDataSuggestions(SourceTypicalName, TargetTypicalName)
      )
      .pipe(
        map(dynamics =>
          dynamics.map(dynamic => {
            const d = new DynamicData();
            d.SourceAttributeName = dynamic['sourceAttributeName'];
            d.TargetAttributeName = dynamic['targetAttributeName'];
            return d;
          })
        )
      );
  }

  saveDynamicDataSuggestions(
    SourceTypicalName: string,
    SourceAttributeName: string,
    TargetTypicalName: string,
    TargetAttributeName: string
  ): Observable<void> {
    return this.httpExecutor.executeCommand(
      this.designUrl,
      new SaveDynamicDataSuggestions(
        SourceTypicalName,
        SourceAttributeName,
        TargetTypicalName,
        TargetAttributeName
      ),
      CommandMethod.POST
    );
  }

  @Cacheable()
  getSystems(): Observable<System[]> {
    return this.httpExecutor.executeQuery(this.designUrl, new GetSystems());
  }

  @Cacheable()
  getSystem(Id: string): Observable<System> {
    return this.httpExecutor.executeQuery(this.designUrl, new GetSystem(Id));
  }

  @Cacheable()
  getCatalogs(subProjectId: string): Observable<Catalog[]> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetCatalogs(subProjectId)
    );
  }

  @Cacheable()
  getTenantCatalogs(projectId: string, subProjectId: string): Observable<Catalog[]> {
    return this.httpExecutor.executeQuery(
      this.administrationUrl,
      new GetTenantCatalogs(projectId, subProjectId)
    );
  }

  @Cacheable()
  addTenantCatalog(
    title: string,
    description: string,
    type: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new AddTenantCatalog(title, description, type),
      CommandMethod.POST
    );
  }

  @Cacheable()
  addProjectCatalog(
    projectId: string,
    title: string,
    description: string,
    type: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new AddProjectCatalog(projectId, title, description, type),
      CommandMethod.POST
    );
  }

  @Cacheable()
  addSubProjectCatalog(
    projectId: string,
    subProjectId: string,
    title: string,
    description: string,
    type: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new AddSubProjectCatalog(projectId, subProjectId, title, description, type),
      CommandMethod.POST
    );
  }

  @Cacheable()
  updateCatalog(
    id: string,
    title: string,
    description: string,
    type: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new UpdateCatalog(id, title, description, type),
      CommandMethod.PUT
    );
  }

  @Cacheable()
  deleteCatalog(id: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new DeleteCatalog(id),
      CommandMethod.DELETE
    );
  }

  @Cacheable()
  addSystem(title: string, adapterName: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new AddSystem(title, adapterName),
      CommandMethod.POST
    );
  }

  @Cacheable()
  updateSystem(
    id: string,
    title: string,
    adapterName: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new UpdateSystem(id, title, adapterName),
      CommandMethod.PUT
    );
  }

  @Cacheable()
  deleteSystem(id: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new DeleteSystem(id),
      CommandMethod.DELETE
    );
  }

  @Cacheable()
  getAllNavigations(subProjectId: string, systemId: string): Observable<Menu[]> {
    return this.httpExecutor
      .executeQuery<Menu[]>(this.dataUrl, new GetAllNavigations(subProjectId, systemId))
      .pipe(
        map(response => {
          return response || [];
        })
      );
  }

  getCatalog(catalogId: string): Observable<Catalog> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetCatalog(catalogId)
    );
  }

  getSystemCatalog(
    subProjectId: string,
    systemId: string,
    catalogId: string
  ): Observable<any> {
    return this.httpExecutor.executeQuery(
      this.administrationUrl,
      new GetSystemCatalog(subProjectId, systemId, catalogId)
    );
  }

  getCatalogSystem(subProjectId: string, catalogId: string): Observable<any> {
    return this.httpExecutor.executeQuery(
      this.administrationUrl,
      new GetCatalogSystem(subProjectId, catalogId)
    );
  }

  updateSystemCatalog(
    id: string,
    subProjectId: string,
    systemId: string,
    catalogId: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new UpdateSystemCatalog(id, subProjectId, systemId, catalogId),
      CommandMethod.PUT
    );
  }

  addSystemCatalog(
    subProjectId: string,
    systemId: string,
    catalogId: string
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new AddSystemCatalog(subProjectId, systemId, catalogId),
      CommandMethod.POST
    );
  }

    getEnquiryActions(typicalName: string, catalogId: string): Observable<EnquiryAction[]> {
      typicalName = (typicalName || '').split('.RESULT')[0];
    if (typicalName === '') {
      return of([]);
    }
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetEnquiryActions(typicalName, catalogId)
    ); }

  // getEnquiryActions(
  //   typicalName: string,
  //   catalogId: string
  // ): Observable<string[]> {
  //    = (typicalName || '').split('.RESULT')[0];
  //   if (typicalName === '') {
  //     return of([typicalName]);
  //   }
  //   return this.httpExecutor.executeQuery(
  //     this.designUrl,
  //     new GetEnquiryActions(typicalName, catalogId)
  //   );
  // }

  @Cacheable()
  getActionItem(adapterName: string): Observable<ActionItems> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetActionItem(adapterName)
    );
  }

  @Cacheable()
  getConditionItem(adapterName: string): Observable<ConditionItem> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetConditionItem(adapterName)
    );
  }

  @Cacheable()
  searchAttributes(
    searchString: string,
    typicalId: string
  ): Observable<Attribute[]> {
    if (Guid.isEmpty(typicalId)) {
      return of([]);
    }
    // TODO ask backend to return the same type of Attribute camelCase or PascalCase
    // this is a temp trick to bypass error
    return this.httpExecutor
      .executeQuery<any[]>(
        this.designUrl,
        new SearchAttributes(searchString, typicalId)
      )
      .pipe(
        map(
          attributes =>
            (attributes &&
              attributes.map(attr => {
                return {
                  Name: attr.name,
                  DataType: attr.dataType
                } as Attribute;
              })) ||
            []
        )
      );
  }

  @Cacheable()
  SearchAttributesByCatalogIdAndTypicalName(
    searchString: string,
    catalogId: string,
    typicalName: string
  ): Observable<Attribute[]> {
    if (Guid.isEmpty(typicalName)) {
      return of([]);
    }
    return this.httpExecutor
      .executeQuery<any[]>(
        this.designUrl,
        new SearchAttributesByCatalogIdAndTypicalName(
          searchString,
          catalogId,
          typicalName
        )
      )
      .pipe(
        map(
          attributes =>
            (attributes &&
              attributes.map(attr => {
                return {
                  Name: attr.name,
                  DataType: attr.dataType
                } as Attribute;
              })) ||
            []
        )
      );
  }

  @Cacheable()
  searchTypicals(
    searchString: string,
    cataloglId: string
  ): Observable<Typical[]> {
    if (Guid.isEmpty(cataloglId)) {
      return of([]);
    }
    return this.httpExecutor.executeQuery<Typical[]>(
      this.designUrl,
      new SearchTypicals(searchString, cataloglId)
    );
  }

  getTypicals(catalogId: string): Observable<Typical[]> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetTypicals(catalogId)
    );
  }

  addTypicalManually(
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    title: string,
    type: number,
    tenantId: string,
    hasAutomaticId?: boolean,
    isConfiguration?: boolean
  ): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new AddTypicalManually(
        projectId,
        subProjectId,
        systemId,
        catalogId,
        title,
        type,
        tenantId,
        hasAutomaticId,
        isConfiguration
      ),
      CommandMethod.POST
    );
  }

  updateTypicalManually(id: string, title: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new UpdateTypicalManually(id, title),
      CommandMethod.PUT
    );
  }

  @Cacheable()
  deleteTypicalManually(Id: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new DeleteTypicalManually(Id),
      CommandMethod.DELETE
    );
  }

  getWorkflowVersion(
    workflowId: string,
    version: number
  ): Observable<WorkflowVersion> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetWorkflowVersion(workflowId, version)
    );
  }

  getPreviousWorkflowItems(
    currentId: string,
    designerJson: string,
    parentId: string,
    selectedPathItemsIds: string[]
  ): Observable<WorkflowItemCustomData[]> {
    return this.httpExecutor.executeCommand(
      this.designUrl,
      new GetPreviousWorkflowItems(
        currentId,
        designerJson,
        parentId,
        selectedPathItemsIds
      ),
      CommandMethod.POST
    );
  }

  getWorkflowVersions(workflowId: string): Observable<WorkflowVersion[]> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new GetWorkflowVersions(workflowId)
    );
  }

  hasVideo(workflowId: string): Observable<boolean> {
    return this.httpExecutor.executeQuery(
      this.designUrl,
      new HasVideo(workflowId)
    );
  }

  downloadVideo(workflowId: string): Observable<Blob> {

    let headers = {};
    headers = new HttpHeaders().append('Authorization', 'Bearer ' + this.sessionService.getToken());
    return this.http.get(this.designUrl + 'DownloadVideo?WorkflowId=' + workflowId, {responseType: 'blob', headers: headers});
  }

  getWorkflowCoverage(
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    workflowId: string
  ): any {
    return this.httpExecutor.executeCommand<any>(
      this.designUrl,
      new GetWorkflowCoverage(
        tenantId,
        projectId,
        subProjectId,
        systemId,
        catalogId,
        workflowId
      ),
      CommandMethod.POST
    );
  }

  @Cacheable()
  getSuggestions(
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    currentItemId: string,
    json: string
  ): Observable<ListSuggestions> {
    return this.httpExecutor
      .executeCommand<ListSuggestions>(
        this.designUrl,
        new GetSuggestions(
          tenantId,
          projectId,
          subProjectId,
          systemId,
          catalogId,
          currentItemId,
          json
        ),
        CommandMethod.POST
      )
      .pipe(
        map((res: any) => {
          if (isNullOrUndefined(res)) {
            return new ListSuggestions();
          }
          res.preItems = res.preItems.map(x => {
            x.workflowItem = {
              Id: x.workflowItem['id'],
              Title: x.workflowItem['title'],
              Description: x.workflowItem['description'],
              Type: x.workflowItem['type'],
              CatalogId: x.workflowItem['catalogId'],
              CatalogName: x.workflowItem['catalogName'],
              TypicalId: x.workflowItem['typicalId'],
              SystemId: x.workflowItem['systemId'],
              TypicalName: x.workflowItem['typicalName'],
              TypicalType: x.workflowItem['typicalType'],
              ActionType: x.workflowItem['actionType'],
              Parameters: x.workflowItem['parameters'],
              DynamicData: x.workflowItem['dynamicData'],
              ActionTitle: ''
            } as any;
            return x;
          });
          res.postItems = res.postItems.map(x => {
            x.workflowItem = {
              Id: x.workflowItem['id'],
              Title: x.workflowItem['title'],
              Description: x.workflowItem['description'],
              Type: x.workflowItem['type'],
              CatalogId: x.workflowItem['catalogId'],
              CatalogName: x.workflowItem['catalogName'],
              TypicalId: x.workflowItem['typicalId'],
              SystemId: x.workflowItem['systemId'],
              TypicalName: x.workflowItem['typicalName'],
              TypicalType: x.workflowItem['typicalType'],
              ActionType: x.workflowItem['actionType'],
              Parameters: x.workflowItem['parameters'],
              DynamicData: x.workflowItem['dynamicData'],
              ActionTitle: ''
            } as any;
            return x;
          });
          return res;
        })
      );
  }

  @Cacheable({
    cacheBusterObserver: workflowVersionNotifier
  })

  getWorkflowVersionsByDate(
    id: string,
    fromDate: string,
    toDate: string
  ): Observable<WorkflowVersion[]> {
    return this.httpExecutor.executeQuery<WorkflowVersion[]>(
      this.designUrl,
      new GetWorkflowVersionsByDate(id, fromDate, toDate)
    );
  }

  rollbackWorkflow(
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    workflowId: string,
    version: number,
    skipStatus: boolean
  ):
    Observable<void> {
    return this.httpExecutor.executeCommand(
      this.designUrl,
      new RollbackWorkflow(tenantId, projectId, subProjectId, systemId, catalogId, workflowId, version, skipStatus),
      CommandMethod.PUT
    );
  }

  getPaths(
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    json: string
  ):
    Observable<WorkflowGraphPath[]> {
    return this.httpExecutor
      .executeCommand(
        this.designUrl,
        new GetPaths(
          tenantId,
          projectId,
          subProjectId,
          systemId,
          catalogId,
          json
        ),
        CommandMethod.POST
      )
      .pipe(
        map(x => {
          if (isNullOrUndefined(x)) {
            return [];
          }
          return x && x['workflowPath'];
        })
      );
  }

  getWorkflowByTitle(
    tenantId: string,
    projectId: string,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    workflowTitle: string
  ):
    Observable<Workflow> {
    return this.httpExecutor.executeQuery<Workflow>(
      this.designUrl,
      new GetWorkflowByTitle(
        tenantId,
        projectId,
        subProjectId,
        systemId,
        catalogId,
        workflowTitle
      )
    );
  }

  @Cacheable()
  getSystemTags(systemId: string): Observable<SystemTag[]> {
    return this.httpExecutor.executeQuery(
      this.administrationUrl,
      new GetSystemTags(systemId)
    );
  }

  @Cacheable()
  addSystemTag(
    systemId: string,
    title: string,
    description: string
  ):
    Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new AddSystemTag(systemId, title, description),
      CommandMethod.POST
    );
  }

  @Cacheable()
  updateSystemTag(
    id: string,
    systemId: string,
    title: string,
    description: string
  ):
    Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new UpdateSystemTag(id, systemId, title, description),
      CommandMethod.PUT
    );
  }

  @Cacheable()
  deleteSystemTag(id: string): Observable<any> {
    return this.httpExecutor.executeCommand(
      this.administrationUrl,
      new DeleteSystemTag(id),
      CommandMethod.DELETE
    );
  }

  @Cacheable()
  getAdapters(): Observable<Adapter[]> {
    return this.httpExecutor.executeQuery(this.administrationUrl, new GetAdapters());
  }

  getTypicalAttributes(typicalId: string,): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.dataUrl, new GetTypicalAttributes(typicalId), CommandMethod.GET);
  }

  addTypicalAttributeManually(name: string, typicalId: string): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.dataUrl, new AddTypicalAttributeManually(name, typicalId), CommandMethod.POST);
  }

  updateTypicalAttributeManually(name: string, typicalId: string, newName: string): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.dataUrl, new UpdateTypicalAttributeManually(name, typicalId, newName), CommandMethod.PUT);
  }

  deleteTypicalAttributeManually(name: string, typicalId: string, isTypicalChange: boolean): Observable<any> {
    return this.httpExecutor.executeCommand<any>(
      this.dataUrl, new DeleteTypicalAttributeManually(name, typicalId, isTypicalChange), CommandMethod.DELETE);
  }

}
