import {Injectable} from '@angular/core';
import { Guid } from 'src/app/shared/guid';

@Injectable()
export class WorkflowService {
    getGraphToSave(graph) {
        let cells = graph.cells.filter(el => el.type !== 'standard.Image' && !el.linkedParentId);
        let startItem = graph.cells.find(el => el.type === 'fsa.State' && el.customData && el.customData.Title === 'Start');
        graph.cells = cells.map(cell => {
          if (cell.customData && cell.customData.ActionType >= 0 && startItem) {
            cell.customData.AdapterName = startItem.customData.AdapterName;
            return cell;
          }
          return cell;
        });
        const linkedCells = cells.filter(el =>  el.attrs && el.attrs.root && el.attrs.root.type === 'Linked');
        for (let i = 0; i < linkedCells.length; i++) {
            graph = this.collapseLinkedWorkflowElement(linkedCells[i], graph);
        }

        let newCells = graph.cells.map(cell => {
          if (cell.customData && cell.customData.ActionType >= 0) {
            let state = this.getLastStateItem(cell, graph);
            if (state) {
              cell.customData.AdapterName = state.customData.AdapterName;
              cell.customData.SystemId = state.customData.SystemId;
            }
            return cell;
          }
          return cell;
        });

        graph.cells = newCells;
        return graph;
    }

    getLastStateItem(cell, graph) {
      let link = graph.cells.find(el => el.target && el.target.id === cell.id);
      let source = graph.cells.find(el => link && link.source.id === el.id);
      if (source && source.type === 'fsa.State') {
        return source;
      }

      if (!source) {
        return null;
      }

      return this.getLastStateItem(source, graph);

    }

    getLastItem(cell, graph) {
      let link = graph.cells.find(el => el.target && el.target.id === cell.id);
      let source = graph.cells.find(el => link && link.source.id === el.id);
      if (source && source.customData && source.customData.TypicalId && source.customData.TypicalId !== Guid.empty) {
        return source;
      }

      if (!source) {
        return null;
      }

      return this.getLastItem(source, graph);

    }

    collapseLinkedWorkflowElement(linkedElement, graph) {
        let cells  = [];
        let children = graph.cells.filter(cell => 
          cell.customData && cell.customData.parentIds && 
          cell.customData.parentIds.find(id => id === linkedElement.id)
        );
        graph.cells.map(cell => {
          let sourceItem = children.find(step => cell.source && step.id === cell.source.id);
          let targetItem = !children.find(step => cell.target && step.id === cell.target.id);
          if (sourceItem && targetItem) {
            let existsTarget = cells.find(element => element.source && element.target &&
                element.target.id === cell.target.id && element.source.id === linkedElement.id);
            if (!existsTarget) {
              cells.push({
                ...cell, 
                id: Guid.newGuid(), 
                source: {id: linkedElement.id}
              });
            }
          } else if (cell.id === linkedElement.id) {
            cell.customData.isExpanded = false;
            cells.push(cell);
          } else {
            cells.push(cell);
          }
        })
        let newCells = cells.filter(cell => !children.find(child => cell.id === child.id));
        return {cells: newCells};
      }
}