import { WorkflowItemType } from '../../../core/models/workflow-item-type';
import { ui, dia } from '../../../../vendor/rappid';
import * as appShapes from '../../../../shapes/app-shapes';
import { WorkflowItemActionType } from '../../../core/models/workflow-item-action-type';

export class StencilService {
  stencil: ui.Stencil;

  create(paperScroller: ui.PaperScroller, snaplines: ui.Snaplines) {
    this.stencil = new ui.Stencil({
      paper: paperScroller,
      snaplines: snaplines,
      width: 460,
      groups: this.getStencilGroups(),
      dropAnimation: true,
      groupsToggleButtons: true,
      paperOptions: function() {
        return {
          model: new dia.Graph(
            {},
            {
              cellNamespace: appShapes
            }
          ),
          cellViewNamespace: appShapes
        };
      },
      // search: {
      //     '*': ['type', 'attrs/text/text', 'attrs/.label/text'],
      //     'org.Member': ['attrs/.rank/text', 'attrs/.name/text']
      // },
      // Use default Grid Layout
      layout: true,
      // Remove tooltip definition from clone
      dragStartClone: cell => cell.clone().removeAttr('root/dataTooltip')
    });
  }

  setShapes() {
    this.stencil.load(this.getStencilShapes());
  }

  getStencilGroups() {
    return <{ [key: string]: ui.Stencil.Group }>{
      standard: { index: 1, label: 'Standard shapes' }
      // fsa: { index: 2, label: 'State machine' },
      // pn: { index: 3, label: 'Petri nets' },
      // erd: { index: 4, label: 'Entity-relationship' },
      // uml: { index: 5, label: 'UML' },
      // org: { index: 6, label: 'ORG' }
    };
  }

  getStencilShapes() {
    return {
      standard: [
        {
          type: 'fsa.State',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            circle: {
              width: 60,
              height: 60,
              fill: '#ffffff',
              stroke: '#c8d6e5',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Start',
              fill: 'black',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Bold',
              'font-size': 11,
              'stroke-width': 0
            }
          },
          customData: {
            Type: WorkflowItemType.Start,
            Id: '00000000-0000-0000-0000-000000000000',
            SystemId: '00000000-0000-0000-0000-000000000000',
            AdapterName: '',
            Title: '',
            Description: '',
            CatalogId: '00000000-0000-0000-0000-000000000000',
            CatalogName: '',
            TypicalId: '00000000-0000-0000-0000-000000000000',
            TypicalName: '',
            Parameters: '',
            DataSetsItemIds: [],
            DynamicDatas: [],
            Constraints: [],
            PostConstraints: []
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Rectangle',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil',
              type: 'Application Step'
            },
            body: {
              width: 190,
              height: 30,
              fill: 'white',
              stroke: '#c8d6e5',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 20,
              ry: 20
            },
            image: {
              xlinkHref: 'assets/actionicons/actionstep.svg',
              width: 50,
              height: 30,
              x: 0,
              y: 5
            },
            label: {
              text: 'Application Step',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0
            }
          },
          customData: {
            Type: WorkflowItemType.Action,
            Id: '00000000-0000-0000-0000-000000000000',
            SystemId: '00000000-0000-0000-0000-000000000000',
            AdapterName: '',
            Title: '',
            Description: '',
            CatalogId: '00000000-0000-0000-0000-000000000000',
            CatalogName: '',
            TypicalId: '00000000-0000-0000-0000-000000000000',
            TypicalName: '',
            ActionType: WorkflowItemActionType.Input,
            Parameters: '',
            DataSetsItemIds: [],
            DynamicDatas: [],
            Constraints: [],
            PostConstraints: []
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Rectangle',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil',
              type: 'Perform Action Step'
            },
            body: {
              width: 190,
              height: 30,
              fill: 'white',
              stroke: '#c8d6e5',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 20,
              ry: 20
            },
            image: {
              xlinkHref: 'assets/actionicons/checkapplication.svg',
              width: 50,
              height: 30,
              x: 0,
              y: 5
            },
            label: {
              text: 'Perform Action Step',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0
            }
          },
          customData: {
            Type: WorkflowItemType.Action,
            Id: '00000000-0000-0000-0000-000000000000',
            SystemId: '00000000-0000-0000-0000-000000000000',
            AdapterName: '',
            Title: '',
            Description: '',
            CatalogId: '00000000-0000-0000-0000-000000000000',
            CatalogName: '',
            TypicalId: '00000000-0000-0000-0000-000000000000',
            TypicalName: '',
            ActionType: WorkflowItemActionType.Delete,
            Parameters: '',
            DataSetsItemIds: [],
            DynamicDatas: [],
            Constraints: [],
            PostConstraints: []
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Rectangle',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil',
              type: 'Check Enquiry Step'
            },
            body: {
              width: 190,
              height: 30,
              fill: 'white',
              stroke: '#c8d6e5',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 20,
              ry: 20
            },
            image: {
              xlinkHref: 'assets/actionicons/checkenquiry.svg',
              width: 50,
              height: 30,
              x: 0,
              y: 5
            },
            label: {
              text: 'Check Enquiry Step',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0
            }
          },
          customData: {
            Type: WorkflowItemType.Action,
            Id: '00000000-0000-0000-0000-000000000000',
            SystemId: '00000000-0000-0000-0000-000000000000',
            AdapterName: '',
            Title: '',
            Description: '',
            CatalogId: '00000000-0000-0000-0000-000000000000',
            CatalogName: '',
            TypicalId: '00000000-0000-0000-0000-000000000000',
            TypicalName: '',
            ActionType: WorkflowItemActionType.EnquiryResult,
            Parameters: '',
            DataSetsItemIds: [],
            DynamicDatas: [],
            Constraints: [],
            PostConstraints: []
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Rectangle',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil',
              type: 'Enquiry Action Step'
            },
            body: {
              width: 190,
              height: 30,
              fill: 'white',
              stroke: '#c8d6e5',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 20,
              ry: 20
            },
            image: {
              xlinkHref: 'assets/actionicons/enquirystep.svg',
              width: 50,
              height: 30,
              x: 0,
              y: 5
            },
            label: {
              text: 'Enquiry Action Step',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0
            }
          },
          customData: {
            Type: WorkflowItemType.Action,
            Id: '00000000-0000-0000-0000-000000000000',
            SystemId: '00000000-0000-0000-0000-000000000000',
            AdapterName: '',
            Title: '',
            Description: '',
            CatalogId: '00000000-0000-0000-0000-000000000000',
            CatalogName: '',
            TypicalId: '00000000-0000-0000-0000-000000000000',
            TypicalName: '',
            ActionType: WorkflowItemActionType.EnquiryAction,
            Parameters: '',
            DataSetsItemIds: [],
            DynamicDatas: [],
            Constraints: [],
            PostConstraints: []
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Rectangle',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil',
              type: 'Navigation Step'
            },
            body: {
              width: 190,
              height: 30,
              fill: 'white',
              stroke: '#c8d6e5',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 20,
              ry: 20
            },
            image: {
              xlinkHref: 'assets/actionicons/navigationstep.svg',
              width: 50,
              height: 30,
              x: 0,
              y: 5
            },
            label: {
              text: 'Navigation Step',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0
            }
          },
          customData: {
            Type: WorkflowItemType.Action,
            Id: '00000000-0000-0000-0000-000000000000',
            SystemId: '00000000-0000-0000-0000-000000000000',
            AdapterName: '',
            Title: '',
            Description: '',
            CatalogId: '00000000-0000-0000-0000-000000000000',
            CatalogName: '',
            TypicalId: '00000000-0000-0000-0000-000000000000',
            TypicalName: '',
            ActionType: WorkflowItemActionType.Menu,
            Parameters: '',
            DataSetsItemIds: [],
            Constraints: [],
            DynamicDatas: [],
            PostConstraints: []
          }
        },
        {
          type: 'standard.Rectangle',
          attrs: {
            root: {
              dataTooltip: 'Rectangle',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil',
              type: 'Manual'
            },
            body: {
              width: 190,
              height: 30,
              fill: 'white',
              stroke: '#c8d6e5',
              strokeWidth: 2,
              strokeDasharray: '0',
              rx: 20,
              ry: 20
            },
            image: {
              xlinkHref: 'assets/actionicons/manualstep.svg',
              width: 50,
              height: 30,
              x: 0,
              y: 5
            },
            label: {
              text: 'Manual Step',
              fill: 'black',
              fontFamily: 'Roboto Condensed',
              fontWeight: 'Bold',
              fontSize: 14,
              strokeWidth: 0
            }
          },
          customData: {
            Type: WorkflowItemType.Manual,
            Id: '00000000-0000-0000-0000-000000000000',
            SystemId: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            Role: '',
            Action: '',
            DataSetsItemIds: [],
            Constraints: [],
            DynamicDatas: [],
            PostConstraints: []
          }
        },
        {
          type: 'standard.Polygon',
          attrs: {
            root: {
              dataTooltip: 'Rhombus',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            body: {
              // refPoints: '80,0 160,30 80,60 0,30',
              fill: 'white',
              stroke: '#c8d6e5',
              strokeWidth: 2,
              strokeDasharray: '0'
            },
            image: {
              xlinkHref: 'assets/actionicons/condition.svg',
              x: '1%',
              y: '0.25%'
            }
          },
          customData: {
            Type: WorkflowItemType.Condition,
            Id: '00000000-0000-0000-0000-000000000000',
            SystemId: '00000000-0000-0000-0000-000000000000',
            Title: '',
            Description: '',
            CatalogId: '00000000-0000-0000-0000-000000000000',
            CatalogName: '',
            TypicalId: '00000000-0000-0000-0000-000000000000',
            TypicalName: '',
            Parameters: '',
            DataSetsItemIds: [],
            DynamicDatas: [],
            Constraints: [],
            PostConstraints: []
          }
        }
      ],
      fsa: [
        {
          type: 'fsa.StartState',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'Start State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            circle: {
              width: 50,
              height: 30,
              fill: '#61549c',
              'stroke-width': 0
            },
            text: {
              text: 'startState',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        },
        {
          type: 'fsa.EndState',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'End State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.inner': {
              fill: '#6a6c8a',
              stroke: 'transparent'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'endState',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        },
        {
          type: 'fsa.State',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            circle: {
              fill: '#6a6c8a',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'state',
              fill: '#c6c7e2',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        }
      ],
      pn: [
        {
          type: 'pn.Place',
          preserveAspectRatio: true,
          tokens: 3,
          attrs: {
            root: {
              dataTooltip: 'Place',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.root': {
              fill: 'transparent',
              stroke: '#61549c',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            '.tokens circle': {
              fill: '#6a6c8a',
              stroke: '#000',
              'stroke-width': 0
            },
            '.label': {
              text: '',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            }
          }
        },
        {
          type: 'pn.Transition',
          preserveAspectRatio: true,
          attrs: {
            root: {
              dataTooltip: 'Transition',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            rect: {
              rx: 3,
              ry: 3,
              width: 12,
              height: 50,
              fill: '#61549c',
              stroke: '#7c68fc',
              'stroke-width': 0,
              'stroke-dasharray': '0'
            },
            '.label': {
              text: 'transition',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'stroke-width': 0,
              fill: '#222138'
            }
          }
        }
      ],
      erd: [
        {
          type: 'erd.Entity',
          attrs: {
            root: {
              dataTooltip: 'Entity',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              rx: 3,
              ry: 3,
              fill: '#31d0c6',
              'stroke-width': 2,
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Entity',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.WeakEntity',
          attrs: {
            root: {
              dataTooltip: 'Weak Entity',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#26ff51',
              'stroke-width': 2,
              points: '100,0 100,60 0,60 0,0',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#26ff51',
              stroke: 'transparent',
              points: '97,5 97,55 3,55 3,5',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Weak entity',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Relationship',
          attrs: {
            root: {
              dataTooltip: 'Relationship',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: '#61549c',
              stroke: 'transparent',
              'stroke-width': 2,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Relation',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.IdentifyingRelationship',
          attrs: {
            root: {
              dataTooltip: 'Identifying Relationship',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#6a6c8a',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#6a6c8a',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Relation',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.ISA',
          attrs: {
            root: {
              dataTooltip: 'ISA',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            text: {
              text: 'ISA',
              fill: '#f6f6f6',
              'letter-spacing': 0,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            polygon: {
              fill: '#26ff51',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            }
          }
        },
        {
          type: 'erd.Key',
          attrs: {
            root: {
              dataTooltip: 'Key',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#fe854f',
              stroke: 'transparent',
              display: 'block',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Key',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Normal',
          attrs: {
            root: {
              dataTooltip: 'Normal',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: '#26ff51',
              stroke: 'transparent',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Normal',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Multivalued',
          attrs: {
            root: {
              dataTooltip: 'Mutltivalued',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#fe854f',
              stroke: 'transparent',
              rx: 43,
              ry: 21,
              'stroke-dasharray': '0'
            },
            text: {
              text: 'MultiValued',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              fill: '#f6f6f6',
              'stroke-width': 0
            }
          }
        },
        {
          type: 'erd.Derived',
          attrs: {
            root: {
              dataTooltip: 'Derived',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.outer': {
              fill: 'transparent',
              stroke: '#fe854f',
              'stroke-dasharray': '0'
            },
            '.inner': {
              fill: '#26ff51',
              stroke: 'transparent',
              display: 'block',
              'stroke-dasharray': '0'
            },
            text: {
              text: 'Derived',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11,
              'stroke-width': 0
            }
          }
        }
      ],
      uml: [
        {
          type: 'uml.Class',
          name: 'Class',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Class',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              top: 2,
              fill: '#61549c',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },
        {
          type: 'uml.Interface',
          name: 'Interface',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Interface',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              fill: '#fe854f',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },
        {
          type: 'uml.Abstract',
          name: 'Abstract',
          attributes: ['+attr1'],
          methods: ['-setAttr1()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'Abstract',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-class-name-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-attrs-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-methods-rect': {
              fill: '#6a6c8a',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8
            },
            '.uml-class-name-text': {
              ref: '.uml-class-name-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-attrs-text': {
              ref: '.uml-class-attrs-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            },
            '.uml-class-methods-text': {
              ref: '.uml-class-methods-rect',
              'ref-y': 0.5,
              'y-alignment': 'middle',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 11
            }
          }
        },

        {
          type: 'uml.State',
          name: 'State',
          events: ['entry/', 'create()'],
          size: {
            width: 150,
            height: 100
          },
          attrs: {
            root: {
              dataTooltip: 'State',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.uml-state-body': {
              fill: '#26ff51',
              stroke: '#f6f6f6',
              'stroke-width': 1,
              rx: 8,
              ry: 8,
              'stroke-dasharray': '0'
            },
            '.uml-state-separator': {
              stroke: '#f6f6f6',
              'stroke-width': 1,
              'stroke-dasharray': '0'
            },
            '.uml-state-name': {
              fill: '#f6f6f6',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            },
            '.uml-state-events': {
              fill: '#f6f6f6',
              'font-size': 11,
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal'
            }
          }
        }
      ],
      org: [
        {
          type: 'org.Member',
          attrs: {
            root: {
              dataTooltip: 'Member',
              dataTooltipPosition: 'left',
              dataTooltipPositionSelector: '.joint-stencil'
            },
            '.rank': {
              text: 'Rank',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-size': 12,
              'font-weight': 'Bold',
              'text-decoration': 'none'
            },
            '.name': {
              text: 'Person',
              fill: '#f6f6f6',
              'font-family': 'Roboto Condensed',
              'font-weight': 'Normal',
              'font-size': 10
            },
            '.card': {
              fill: '#31d0c6',
              stroke: 'transparent',
              'stroke-width': 0,
              'stroke-dasharray': '0'
            },
            image: {
              width: 32,
              height: 32,
              x: 16,
              y: 13,
              ref: <string>null,
              'ref-x': <number>null,
              'ref-y': <number>null,
              'y-alignment': <string>null,
              'xlink:href': 'assets/member-male.png'
            }
          }
        }
      ]
    };
  }

  getShapeModel(itemType: WorkflowItemType, actionType: WorkflowItemActionType) {
    const defaultModels = this.getStencilShapes().standard;

    const defaultActionType = this.getDefaultActionType(actionType);
    // get default model based on type and action type

    const model = defaultModels.find(x => {
      if (itemType === WorkflowItemType.Start) {
        return x.customData.Type === itemType;
      } else if (itemType === WorkflowItemType.Condition) {
        return x.customData.Type === itemType;
      } else if (itemType === WorkflowItemType.Manual) {
        return x.customData.Type === itemType;
      } else {
        return x.customData.Type === itemType && x.customData.ActionType === defaultActionType;
      }
    });
    if (model) {
      // override default action type with the original one
      model.customData.ActionType = actionType;
    }
    return model;
  }

  /**
   * we have many action types that are grouped in some shapes, and in these cases there is a default value per shape.
   * in order to get the model based on actionType we have to search by default action type.
   * @param actionType WorkflowItemActionType
   */
  private getDefaultActionType(actionType: WorkflowItemActionType): WorkflowItemActionType {
    if (actionType === WorkflowItemActionType.Reverse ||
      actionType === WorkflowItemActionType.See ||
      actionType === WorkflowItemActionType.Verify ||
      actionType === WorkflowItemActionType.Delete ||
      actionType === WorkflowItemActionType.Authorise ||
      actionType === WorkflowItemActionType.Validate
    ) {
      // put default action type since in the default model this is the default action type
      actionType = WorkflowItemActionType.Delete;
    } else if (actionType === WorkflowItemActionType.Tab) {
      actionType = WorkflowItemActionType.Menu;
    } else {
      actionType = actionType;
    }
    return actionType;
  }
}