import * as joint from '../../../../vendor/rappid';
import * as _ from 'lodash';
import {StencilService} from './stencil-service';
import {ToolbarService} from './toolbar-service';
import {InspectorService} from './inspector-service';
import {HaloService} from './halo-service';
import {KeyboardService} from './keyboard-service';
import * as appShapes from '../../../../shapes/app-shapes';
import {RapidEventsService} from '../rapid-events.service';
import {Subject} from 'rxjs';
import {WorkflowItemCustomData} from '../../models/workflow-item-custom-data.model';
import {WorkflowItemType} from 'src/app/core/models/workflow-item-type';
import {SessionService} from 'src/app/core/services/session.service';
import {isNullOrUndefined} from 'util';

export class KitchenSinkService {
  el: Element;

  graph: joint.dia.Graph;
  paper: joint.dia.Paper;
  public paperScroller: joint.ui.PaperScroller;

  commandManager: joint.dia.CommandManager;
  snaplines: joint.ui.Snaplines;
  clipboard: joint.ui.Clipboard;
  selection: joint.ui.Selection;
  navigator: joint.ui.Navigator;

  stencilService: StencilService;
  toolbarService: ToolbarService;
  inspectorService: InspectorService;
  haloService: HaloService;
  keyboardService: KeyboardService;
  public graphJson: any;
  public undoStack: any = [];
  public redoStack: any = [];
  public removedLinks: any = [];
  public selectedItems: any = [];
  public isSectionSelected: boolean = false;
  public addInfoAndExpandToGraph: boolean = true;
  lastUndoElement: any;
  lastRedoElement: any;
  rapidEventService: RapidEventsService;
  isEditMode: boolean;


  undoRedoClicked: Subject<any> = new Subject<any>();
  undoRedoClicked$ = this.undoRedoClicked.asObservable();

  constructor(
    el: Element,
    stencilService: StencilService,
    toolbarService: ToolbarService,
    inspectorService: InspectorService,
    haloService: HaloService,
    keyboardService: KeyboardService,
    rapidEventService: RapidEventsService,
    isEditMode: boolean,
    private sessionService?: SessionService
  ) {
    this.el = el;
    this.isEditMode = isEditMode;
    // apply current joint js theme
    const view = new joint.mvc.View({el: this.el});

    this.stencilService = stencilService;
    this.toolbarService = toolbarService;
    this.inspectorService = inspectorService;
    this.haloService = haloService;
    this.keyboardService = keyboardService;
    this.rapidEventService = rapidEventService;
  }

  getGraphJson() {
    return this.graph.toJSON();
  }

  loadGraphJson(graphJson: any) {
    this.graph.fromJSON(graphJson);
  }

  startRappid() {
    joint.setTheme('modern');

    this.initializePaper();
    this.initializeStencil();
    this.initializeSelection();
    this.initializeToolsAndInspector();
    this.initializeNavigator();
    this.initializeToolbar();
    this.initializeKeyboardShortcuts();
    this.initializeTooltips();
  }

  initializePaper() {
    const graph = (this.graph = new joint.dia.Graph(
      {},
      {
        cellNamespace: appShapes
      }
    ));

    graph.on('add', (cell: joint.dia.Cell, collection: any, opt: any) => {
      if (opt.stencil) {
        this.inspectorService.create(cell);

        this.rapidEventService.itemSelected.next(cell);
        this.rapidEventService.newElementAdded.next({cell: cell, collection: collection});
      }
    });

    this.commandManager = new joint.dia.CommandManager({graph: graph});

    const paper = (this.paper = new joint.dia.Paper({
      width: 1000,
      height: 1000,
      gridSize: 10,
      drawGrid: true,
      model: graph,
      cellViewNamespace: appShapes,
      defaultLink: <joint.dia.Link>new appShapes.app.Link()
    }));

    paper.on('blank:mousewheel', _.partial(this.onMousewheel, null), this);
    paper.on('cell:mousewheel', this.onMousewheel.bind(this));

    this.snaplines = new joint.ui.Snaplines({paper: paper});

    const paperScroller = (this.paperScroller = new joint.ui.PaperScroller({
      paper,
      autoResizePaper: true,
      cursor: 'grab'
    }));
    this.renderPlugin('.paper-container', paperScroller);
    paperScroller.render().center();
  }

  addTextToConnector(link, state) {
    const ara = this.graph.getCell(link.model.id);
    if (state === 'yes') {
      ara.label(0, {
        position: {
          distance: 0.5,
          offset: 9
        },
        attrs: {rect: {fill: '#eeeeee'}, text: {text: 'Y', 'font-size': 15, ref: 'rect'}}
      });
    } else if (state === 'no') {
      ara.label(0, {
        position: {
          distance: 0.5,
          offset: 9
        },
        attrs: {rect: {fill: '#eeeeee'}, text: {text: 'N', 'font-size': 15, ref: 'rect'}}
      });
    } else if ((state = 'remove')) {
      ara.label(0, {position: {distance: 0, offset: 0}, attrs: {text: {text: '', 'font-size': 1}}});
    }
  }

  initializeStencil() {
    this.stencilService.create(this.paperScroller, this.snaplines);
    this.renderPlugin('.stencil-container', this.stencilService.stencil);
    this.stencilService.setShapes();
  }

  initializeSelection() {
    this.clipboard = new joint.ui.Clipboard();
    this.selection = new joint.ui.Selection({paper: this.paper});
    const keyboard = this.keyboardService.keyboard;

    // Initiate selecting when the user grabs the blank area of the paper while the Shift key is pressed.
    // Otherwise, initiate paper pan.
    if (this.isEditMode) {
      keyboard.on('delete shift+delete', () => {
        this.graph.removeCells(this.selection.collection.toArray(), {
          selection: (this.selectedItems.length > 0),
          clear: true
        } as joint.dia.Cell.DisconnectableOptions);
      });
    }

    this.paper.on('blank:pointerdown', (evt: JQuery.Event, x: number, y: number) => {
      if (keyboard.isActive('shift', evt)) {
        this.isSectionSelected = true;
        this.selection.startSelecting(evt);
      } else {
        this.selection.cancelSelection();
        this.paperScroller.startPanning(evt);
      }
      this.rapidEventService.itemSelected.next(null);
    });

    this.paper.on('blank:pointerup', (evt: JQuery.Event, x: number, y: number) => {
      if (this.isSectionSelected) {
        this.selectedItems = this.selection.collection.models;
        this.isSectionSelected = false;
      }
    });

    this.paper.on('element:pointerdown', (elementView: joint.dia.ElementView, evt: JQuery.Event) => {
      this.rapidEventService.itemSelected.next(elementView.model);

      // Select an element if CTRL/Meta key is pressed while the element is clicked.
      if (keyboard.isActive('ctrl meta', evt)) {
        this.selection.collection.add(elementView.model);
      }
    });

    this.selection.on(
      'selection-box:pointerdown',
      (elementView: joint.dia.ElementView, evt: JQuery.Event) => {
        // Unselect an element if the CTRL/Meta key is pressed while a selected element is clicked.
        if (keyboard.isActive('ctrl meta', evt)) {
          this.selection.collection.remove(elementView.model);
        }
      },
    );
  }

  initializeToolsAndInspector() {
    this.paper.on('element:pointerup', (elementView: joint.dia.ElementView) => {
      const element = elementView.model;

      if (this.selection.collection.contains(element)) {
        return;
      }

      new joint.ui.FreeTransform({
        cellView: elementView,
        allowRotation: false,
        preserveAspectRatio: !!element.get('preserveAspectRatio'),
        allowOrthogonalResize: element.get('allowOrthogonalResize') !== false
      }).render();
      if (this.isEditMode) {
        this.haloService.create(elementView);
      }
      this.selection.collection.reset([]);
      this.selection.collection.add(element, {silent: true});
      this.paper.removeTools();
      this.inspectorService.create(element);
    });

    this.paper.on('link:pointerup', this.linkPointerUp.bind(this));

    this.paper.on('link:mouseenter', (linkView: joint.dia.LinkView) => {
      // Open tool only if there is none yet
      if (linkView.hasTools()) {
        return;
      }

      const ns = joint.linkTools;
      const toolsView = new joint.dia.ToolsView({
        name: 'link-hover',
        tools: [new ns.Vertices({vertexAdding: false}), new ns.TargetArrowhead()]
      });

      linkView.addTools(toolsView);
    });

    this.paper.on('link:mouseleave', (linkView: joint.dia.LinkView) => {
      // Remove only the hover tool, not the pointerdown tool
      if (linkView.hasTools('link-hover')) {
        linkView.removeTools();
      }
    });

    this.graph.on('change', (cell: joint.dia.Cell, opt: any) => {
      if (!cell.isLink() || !opt.inspector) {
        return;
      }

      const ns = joint.linkTools;
      const toolsView = new joint.dia.ToolsView({
        name: 'link-inspected',
        tools: [new ns.Boundary({padding: 15})]
      });

      cell.findView(this.paper).addTools(toolsView);
    });
  }

  public linkPointerUp(linkView: joint.dia.LinkView) {
    const link = linkView.model;
    const ns = joint.linkTools;
    const toolsView = new joint.dia.ToolsView({
      name: 'link-pointerdown',
      tools: [
        new ns.Vertices(),
        new ns.SourceAnchor(),
        new ns.TargetAnchor(),
        new ns.TargetArrowhead(),
        new ns.Segments(),
        new ns.Boundary({padding: 15}),
        new ns.Remove({offset: -20, distance: 40})
      ]
    });
    this.selection.collection.reset([]);
    this.selection.collection.add(link, {silent: true});
    const paper = this.paper;
    joint.ui.Halo.clear(paper);
    joint.ui.FreeTransform.clear(paper);
    paper.removeTools();
    if (this.isEditMode) {
      linkView.addTools(toolsView);
    }
    this.inspectorService.create(link);
  }

  initializeNavigator() {
    const navigator = (this.navigator = new joint.ui.Navigator({
      width: 240,
      height: 115,
      paperScroller: this.paperScroller,
      zoom: false,
      paperOptions: {
        elementView: appShapes.NavigatorElementView,
        linkView: appShapes.NavigatorLinkView,
        cellViewNamespace: {
          /* no other views are accessible in the navigator */
        }
      }
    }));

    this.renderPlugin('.navigator-container', navigator);
  }

  initializeToolbar() {
    this.toolbarService.create(this.commandManager, this.paperScroller, !this.isEditMode);

    this.toolbarService.toolbar.on({
      'undo:pointerclick': this.onUndoClick.bind(this),
      'redo:pointerclick': this.onRedoClick.bind(this),
      'svg:pointerclick': this.openAsSVG.bind(this),
      'png:pointerclick': this.openAsPNG.bind(this),
      'fullscreen:pointerclick': joint.util.toggleFullScreen.bind(joint.util, document.body),
      'to-front:pointerclick': this.selection.collection.invoke.bind(this.selection.collection, 'toFront'),
      'to-back:pointerclick': this.selection.collection.invoke.bind(this.selection.collection, 'toBack'),
      'layout:pointerclick': this.layoutGraph.bind(this, true, false),
      'snapline:change': this.changeSnapLines.bind(this),
      'clear:pointerclick': this.clearGraph.bind(this),
      'print:pointerclick': this.paper.print.bind(this.paper),
      'grid-size:change': this.paper.setGridSize.bind(this.paper)
    });
    this.renderPlugin('.toolbar-container', this.toolbarService.toolbar);
  }

  onUndoClick(reloadPaths = true) {
    let lastElement = this.undoStack.pop();
    if (lastElement) {
      this.redoStack.push({
        graphToLoad: this.getGraphJson(),
        graphToUndo: lastElement.graph,
        action: lastElement.action
      });
      this.loadGraphJson(lastElement.graph);
      this.rapidEventService.itemSelected.next(null);
      if (lastElement.action !== 'clearGraph' && reloadPaths) {
        this.undoRedoClicked.next(true);
      }
    }
  }

  onRedoClick() {
    let lastElement = this.redoStack.pop();
    if (lastElement) {
      this.undoStack.push({
        graph: lastElement.graphToUndo
      });
      this.loadGraphJson(lastElement.graphToLoad);
      this.rapidEventService.itemSelected.next(null);
      if (lastElement.action !== 'clearGraph') {
        this.undoRedoClicked.next(true);
      }
    }
  }

  clearGraph() {
    this.undoStack.push({
      graph: this.getGraphJson(),
      action: 'clearGraph'
    });
    this.loadGraphJson({cells: []});
  }

  layoutGraph(centerContent, hideIcons?) {
    if (window.location.href.includes('test-generation') || hideIcons) {
      this.layoutDirectedGraph(centerContent);
    } else {
      const graph = this.getGraphJson();
      graph.cells.map(el => {
        if (el.type === 'standard.Image') {
          let image = this.graph.getCell(el.id);
          if (image) {
            image.remove();
          }
        }
      });
      this.layoutDirectedGraph(centerContent);
      this.addInfoAndExpandElements();
    }
  }

  addInfoAndExpandElements() {
    const cells = this.getGraphJson().cells;
    if (cells) {
      for (let i = 0; i < cells.length; i++) {
        if (cells[i].customData && cells[i].customData.IsNegativeStep) {
          const image = new joint.shapes.standard.Image({
            type: 'standard.Image',
            isInteractive: false,
            attrs: {
              image: {
                xlinkHref: 'assets/actionicons/minus.png',
                width: 30,
                height: 30
              },
              root: {
                type: 'Negative'
              }
            },
            size: {
              width: 30,
              height: 30
            },
            customData: {
              parentElement: cells[i]
            },
            position: {x: cells[i].position.x + 30, y: cells[i].position.y - 32}
          });
          this.graph.addCell(image);
        }

        if (
          cells[i].customData &&
          cells[i].customData.ActionType &&
          cells[i].attrs &&
          cells[i].attrs.root &&
          cells[i].attrs.root.type !== 'Navigation Step'
        ) {
          const image = new joint.shapes.standard.Image({
            type: 'standard.Image',
            isInteractive: false,
            attrs: {
              image: {
                xlinkHref: 'assets/actionicons/info.png',
                width: 25,
                height: 25
              },
              root: {
                type: 'Info'
              }
            },
            size: {
              width: 25,
              height: 25
            },
            customData: {
              parentElement: cells[i]
            },
            position: {x: cells[i].position.x, y: cells[i].position.y - 30}
          });
          this.graph.addCell(image);
        } else if (cells[i].attrs && cells[i].attrs.root && cells[i].attrs.root.type === 'Linked') {
          const image = new joint.shapes.standard.Image({
            type: 'standard.Image',
            isInteractive: false,
            attrs: {
              image: {
                xlinkHref: !cells[i].customData.isExpanded ? 'assets/actionicons/expand.png' : 'assets/actionicons/collapse.png',
                width: 25,
                height: 25
              },
              root: {
                type: 'Expand'
              }
            },
            size: {
              width: 25,
              height: 25
            },
            customData: {
              parentElement: cells[i]
            },
            position: {x: cells[i].position.x, y: cells[i].position.y - 30}
          });
          this.graph.addCell(image);
        }
      }
    }
  }

  changeSnapLines(checked: boolean) {
    if (checked) {
      this.snaplines.startListening();
      this.stencilService.stencil.options.snaplines = this.snaplines;
    } else {
      this.snaplines.stopListening();
      this.stencilService.stencil.options.snaplines = null;
    }
  }

  pasteCells() {
    // console.log(1);
    let pastedCells = this.clipboard.pasteCells(this.graph, {
      translate: {dx: 20, dy: 20},
      useLocalStorage: false
    });
    let elements = _.filter(pastedCells, cell => cell.isElement());
    // // AI-1131
    // elements = elements.map(el => {
    //   el.attributes.customData.Id = el.id;
    //   return el;
    // });
    this.rapidEventService.itemPasted.next(elements);
    localStorage.removeItem('joint.ui.Clipboard.cells');
  }

  turnOffKeyboard() {
    this.keyboardService.keyboard.off('ctrl+v');
    this.turnOnKeyboard();
  }

  turnOnKeyboard() {
    this.keyboardService.keyboard.on('ctrl+v', (ev) => {
      // if (this.isEditMode) {
      //   this.pasteCells();
      // }
    });
  }

  initializeKeyboardShortcuts() {
    if (this.isEditMode) {
      this.keyboardService.create(this.graph, this.clipboard, this.selection, this.paperScroller, this.commandManager);

    }
  }

  initializeTooltips(): joint.ui.Tooltip {
    return new joint.ui.Tooltip({
      rootTarget: document.body,
      target: '[data-tooltip]',
      direction: joint.ui.Tooltip.TooltipArrowPosition.Auto,
      padding: 10
    });
  }

  openAsSVG() {
    this.paper.toSVG(
      (svg: string) => {
        new joint.ui.Lightbox({
          image: 'data:image/svg+xml,' + encodeURIComponent(svg),
          downloadable: true,
          fileName: 'Rappid'
        }).open();
      },
      {preserveDimensions: true, convertImagesToDataUris: true}
    );
  }

  openAsPNG() {
    this.paper.toPNG(
      (dataURL: string) => {
        new joint.ui.Lightbox({
          image: dataURL,
          downloadable: true,
          fileName: 'Rappid'
        }).open();
      },
      {padding: 10}
    );
  }

  onMousewheel(cellView: joint.dia.CellView, evt: JQuery.Event, ox: number, oy: number, delta: number) {
    if (this.keyboardService.keyboard.isActive('alt', evt)) {
      evt.preventDefault();
      this.paperScroller.zoom(delta * 0.2, {min: 0.2, max: 5, grid: 0.2, ox, oy});
    }
  }

  layoutDirectedGraph(centerContent) {
    joint.layout.DirectedGraph.layout(this.graph, {
      setVertices: true,
      rankDir: 'TB',
      marginX: 100,
      marginY: 100
    });
    if (centerContent) {
      this.paperScroller.centerContent();
    }
  }

  renderPlugin(selector: string, plugin: any): void {
    this.el.querySelector(selector).appendChild(plugin.el);
    plugin.render();
  }

  setEditMode(editMode: boolean) {
    this.isEditMode = editMode;
    this.initializeToolbar();
    this.initializeKeyboardShortcuts();
  }

  createRapidElement(suggestedElementModel: WorkflowItemCustomData) {
    const itemConfig = this.stencilService.getShapeModel(suggestedElementModel.Type, suggestedElementModel.ActionType);
    let elementToAdd;

    if (!isNullOrUndefined(itemConfig) && itemConfig.customData.Type === WorkflowItemType.Start) {
      elementToAdd = new joint.shapes.standard.Circle(itemConfig);
      elementToAdd.resize(60, 60);
      elementToAdd.attr('body/stroke', '#c8d6e5');
      elementToAdd.attr('text/font-size', '14');
    } else if (!isNullOrUndefined(itemConfig) && (itemConfig.customData.Type === WorkflowItemType.Action ||
      itemConfig.customData.Type === WorkflowItemType.Manual)) {
      elementToAdd = new joint.shapes.standard.Rectangle(itemConfig);
      this.resizeWorkflowElement(suggestedElementModel.Title, elementToAdd);
    } else if (suggestedElementModel.Type === WorkflowItemType.Linked) {
      elementToAdd = this.createLinkedWorkflowElement({title: suggestedElementModel.Title, id: suggestedElementModel.WorkflowId});
      this.resizeWorkflowElement(suggestedElementModel.Title, elementToAdd);
    }
    if (!isNullOrUndefined(itemConfig) && itemConfig.customData.Type === WorkflowItemType.Condition) {
      elementToAdd = new joint.shapes.standard.Polygon(itemConfig);
      elementToAdd.resize(200, 40);
      elementToAdd.attr('image/x', '0.5%');
    }

    elementToAdd.attributes.customData = Object.assign(this.getCustomData(elementToAdd), suggestedElementModel);
    elementToAdd.attr('label/text', suggestedElementModel.Title);
    elementToAdd.attributes.customData.Id = elementToAdd.id;
    return elementToAdd;
  }

  resizeWorkflowElement(title, element) {
    let oldLength = element.attributes.size.width;
    const oldX = element.attributes.position.x;
    let length = title.length * 10;

    if (length < 200) {
      length = 200;
      oldLength = 200;
    }
    const center = (oldX + oldLength) / 2;
    const margin = -((oldX + length) / 2 - center);
    element.resize(length, 40);
    element.translate(margin, 0);
  }

  getCustomData(workflowItemModel: any) {
    workflowItemModel.attributes.customData = Object.assign(new WorkflowItemCustomData(), workflowItemModel.attributes.customData);
    return workflowItemModel.attributes.customData as WorkflowItemCustomData;
  }

  createLink() {
    const link = new joint.shapes.standard.Link({
      router: {
        name: 'normal'
      },
      connector: {
        name: 'rounded'
      }
    });

    link.attr({
      line: {
        stroke: '#c8d6e5',
        'stroke-width': 2,
        'stroke-dasharray': '0'
      }
    });

    return link;
  }

  createLinkedWorkflowElement(workflow) {
    let defaultSettings = this.sessionService.getDefaultSettings();
    const newElementConfig = {
      type: 'standard.Rectangle',
      attrs: {
        root: {
          type: 'Linked'
        },
        body: {
          width: 190,
          height: 30,
          fill: 'white',
          stroke: '#c8d6e5',
          strokeWidth: 2,
          strokeDasharray: '0',
          rx: 20,
          ry: 20
        },
        image: {
          xlinkHref: 'assets/actionicons/link.png',
          width: 50,
          height: 30,
          x: 0,
          y: 5
        },
        label: {
          text: workflow.title,
          fill: 'black',
          fontFamily: 'Roboto Condensed',
          fontWeight: 'Bold',
          fontSize: 14,
          strokeWidth: 0
        }
      },
      customData: {
        Type: WorkflowItemType.Linked,
        Title: 'Linked workflow',
        Description: '',
        CatalogId: defaultSettings.catalog.id,
        CatalogName: defaultSettings.catalog.title,
        WorkflowId: workflow.id,
        isExpanded: false,
        versionId: workflow.versionId
      }
    };
    const newElement = new joint.shapes.standard.Rectangle(newElementConfig);
    return newElement;
  }
}


export default KitchenSinkService;
