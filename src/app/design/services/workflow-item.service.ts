import {Typical} from './../models/typical.model';
import {WorkflowItem} from '../models/workflow-item';
import {BehaviorSubject, Subject} from 'rxjs';
import {Injectable} from '@angular/core';
import {WorkflowVersion} from 'src/app/design/models/workflow-version.model';
import {WorkflowGraphPath} from '../models/workflow-graph-path.model';
import { Catalog } from '../models/catalog.model';
import {SessionService} from '../../core/services/session.service';

@Injectable()
export class WorkflowItemService {
  itemUpdated: Subject<WorkflowItem> = new Subject<WorkflowItem>();
  itemUpdated$ = this.itemUpdated.asObservable();

  workflowItemSelected: WorkflowItem;
  editable: Boolean = false;
  workflowItemSelectedModified: Subject<any> = new Subject<any>();
  worfklowItemSelectedModified$ = this.workflowItemSelectedModified.asObservable();

  workflowEditable: Subject<boolean> = new Subject<boolean>();
  workflowEditable$ = this.workflowEditable.asObservable();

  postSuggestedWorkflowItems: any[] = [];
  preSuggestedWorkflowItems: any[] = [];

  private _typicalUpdated: Subject<Typical> = new Subject<Typical>();
  typicalUpdated$ = this._typicalUpdated.asObservable();

  private _catalogUpdated: Subject<Catalog> = new Subject<Catalog>();
  catalogUpdated$ = this._catalogUpdated.asObservable();

  private _actionTypeUpdated: Subject<any> = new Subject<any>();
  actionTypeUpdated$ = this._actionTypeUpdated.asObservable();

  private _descriptionUpdated: Subject<string> = new Subject<string>();
  descriptionUpdated$ = this._descriptionUpdated.asObservable();

  private _roleUpdated: Subject<string> = new Subject<string>();
  roleUpdated$ = this._roleUpdated.asObservable();

  private _actionUpdated: Subject<string> = new Subject<string>();
  actionUpdated$ = this._actionUpdated.asObservable();

  private _workflowLinkageBroken: Subject<any> = new Subject<any>();
  workflowLinkageBroken$ = this._workflowLinkageBroken.asObservable();

  suggestionSelected: Subject<any> = new Subject<any>();
  suggestionSelected$ = this.suggestionSelected.asObservable();

  workflowSelectionChanged: Subject<string> = new Subject<string>();
  workflowSelectionChanged$ = this.workflowSelectionChanged.asObservable();

  rollbackWorkflow: Subject<any> = new Subject<any>();
  rollbackWorkflow$ = this.rollbackWorkflow.asObservable();


  closeRollbackDialog: Subject<any> = new Subject<any>();
  closeRollbackDialog$ = this.closeRollbackDialog.asObservable();

  workflowVersionChanged: Subject<any> = new Subject<any>();
  workflowVersionChanged$ = this.workflowVersionChanged.asObservable();

  // this should be called in order to load saved paths of workflows
  private _worklfowPathsLoaded: Subject<any> = new Subject<any>();
  workflowPathsLoaded$ = this._worklfowPathsLoaded.asObservable();

  // this should be called when suggestion comes and we want to get paths and best path
  loadSuggestedPaths: Subject<any> = new Subject<any>();
  loadSuggestedPaths$ = this.loadSuggestedPaths.asObservable();

  private _getGraphJson: Subject<JsonNeededBy> = new Subject<JsonNeededBy>();
  getGraphJson$ = this._getGraphJson.asObservable();

  private _jsonLoaded: Subject<GraphJson> = new Subject<GraphJson>();
  jsonLoaded$ = this._jsonLoaded.asObservable();

  workflowHighlight: Subject<any> = new Subject<any>();
  workflowHighlight$ = this.workflowHighlight.asObservable();

  closedDynamicDataPopup: Subject<any> = new Subject<any>();
  closedDynamicDataPopup$ = this.closedDynamicDataPopup.asObservable();

  closeEditWorkflowPopup: Subject<any> = new Subject<any>();
  closedEditWorkflowPopup$ = this.closeEditWorkflowPopup.asObservable();

  isNegativeStepChanged: Subject<any> = new Subject<any>();
  isNegativeStepChanged$ = this.isNegativeStepChanged.asObservable();

  addDynamicDataForLinkedEl: Subject<any> = new Subject<any>();
  addDynamicDataForLinkedEl$ = this.addDynamicDataForLinkedEl.asObservable();

  addDynamicDataForLinkedElCheck: Subject<any> = new Subject<any>();
  addDynamicDataForLinkedElCheck$ = this.addDynamicDataForLinkedElCheck.asObservable();

  continueBreakLinkage: Subject<any> = new Subject<any>();
  continueBreakLinkage$ = this.continueBreakLinkage.asObservable();

  changedWorkflow: Subject<any> = new Subject<any>();
  changedWorkflow$ = this.changedWorkflow.asObservable();

  applyDynamicDataPopup: Subject<any> = new Subject<any>();
  applyDynamicDataPopup$ = this.applyDynamicDataPopup.asObservable();
  workflowPaths: WorkflowGraphPath[] = [];

  private _systemUpdated: Subject<any> = new Subject<any>();
  systemUpdated$ = this._systemUpdated.asObservable();

  constructor(private sessionService: SessionService) {
    const system = this.sessionService.getWorkContext().defaultSettings.system;
    this.changeSystem(system);
    this.itemUpdated$.subscribe(itemSelected => {
      this.workflowItemSelected = itemSelected;
    });
    this.workflowEditable$.subscribe(result => {
      this.editable = result;
    });
  }

  getGraphJson(neededBy: JsonNeededBy) {
    this._getGraphJson.next(neededBy);
  }

  sendGraphJson(graphJson: string, neededBy: JsonNeededBy) {
    this._jsonLoaded.next({json: graphJson, neededBy: neededBy});
  }

  changeTypical(newTypical: Typical) {
    this._typicalUpdated.next(newTypical);
  }

  changeCatalog(newCatalog: Catalog) {
    this._catalogUpdated.next(newCatalog);
  }

  changeActionType(newActionType: any) {
    this._actionTypeUpdated.next(newActionType);
  }

  changeDescription(newDescription: string) {
    this._descriptionUpdated.next(newDescription);
  } 

  changeRole(newRole: string) {
    this._roleUpdated.next(newRole);
  }

  changeAction(newAction: string) {
    this._actionUpdated.next(newAction);
  }

  workflowLinkageBroken(brokenLinkageItem: any) {
    this._workflowLinkageBroken.next(brokenLinkageItem);
  }

  worklfowPathsLoaded(paths: WorkflowGraphPath[]) {
    this._worklfowPathsLoaded.next(paths);
  }

  changeSystem(newSystem: any) {
    this._systemUpdated.next(newSystem);
  }
}

export enum JsonNeededBy {
  Paths,
  DynamicData,
  Filters
}

export class GraphJson {
  neededBy: JsonNeededBy;
  json: string;
}
