import {AuthGuard} from '../shared/auth.guards';
import {DataManagementComponent} from './components/data-management/data-management.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProjectSelectionGuard} from '../shared/project-selection.guards';

const routes: Routes = [
  {
    path: 'data-management',
    component: DataManagementComponent,
    canActivate: [AuthGuard, ProjectSelectionGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class DataManagementRoutingModule {
}
