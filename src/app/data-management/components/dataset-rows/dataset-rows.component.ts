import {DataAttribute} from './../../../test-generation/models/data-attribute';
import {map} from 'rxjs/operators/map';
import {GridComponent} from '@progress/kendo-angular-grid';
import {DataSet} from './../../models/path-dataset.model';
import {Component, OnInit, Input, ViewChild, Output, EventEmitter} from '@angular/core';
import {DataManagementService} from '../../services/data-management.service';
import {WindowService, WindowRef} from '@progress/kendo-angular-dialog';
import {ViewDatasetRowComponent} from '../view-dataset-row/view-dataset-row.component';
import {State, process} from '@progress/kendo-data-query';
import {PermissionService} from 'src/app/core/services/permission.service';
import { inputs } from '@syncfusion/ej2-angular-gantt/src/gantt/gantt.component';
import { WorkflowPathItem } from 'src/app/data-management/models/workflowPathItem.model';


@Component({
  selector: 'app-dataset-rows',
  templateUrl: './dataset-rows.component.html',
  styleUrls: ['./dataset-rows.component.css']
})
export class DatasetRowsComponent implements OnInit {
  @Input() public dataSet: DataSet;
  @Input() public pathitem: WorkflowPathItem;
  @Output() public closeParent = new EventEmitter<string>();
  @ViewChild('grid') grid: GridComponent;
  @ViewChild(ViewDatasetRowComponent)
  public viewDataSetRow: ViewDatasetRowComponent;
  selectedRow: any = {};
  rows: any[] = [];
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10,
    filter: {
      logic: 'and',
      filters: [] // [{ field: 'ProductName', operator: 'contains', value: 'Chef' }]
    }
  };
  public datasetsWindow: WindowRef;
  public showIsLinkedLastVersionDialog = false;
  public showIsLinkedNotLastVersionDialog = false;
  public rowId;

  constructor(
    private dataManagementService: DataManagementService,
    private windowService: WindowService,
    public permissionsService: PermissionService
  ) {
  }

  ngOnInit() {
    this.grid.loading = true;
    this.dataManagementService
      .getDataSetRows(this.dataSet.id)
      .subscribe(result => {
        this.rows = result.rows;
        this.grid.loading = false;
      });
  }

  viewRow(row: any, rowTemplate: any) {
    this.selectedRow = row;
    this.datasetsWindow = this.windowService.open({
      title: 'Row details',
      content: rowTemplate,
      width: 920,
      height: 850
    });
    this.datasetsWindow.result.subscribe(res => {
      this.dataManagementService
        .getDataSetRows(this.dataSet.id)
        .subscribe(result => {
          this.rows = result.rows;
        });
    });
  }

  public onStateChange(state: State) {
    this.gridState = state;
    this.grid.data = process(this.rows, this.gridState);
  }

  removeHandler($event) {
    const rowId = $event.dataItem.rowId;
    this.rowId = rowId;
    this.dataManagementService
      .deleteDataSetItem(
        rowId,
        this.pathitem.workflowId,
        this.pathitem.workflowPathId,
        this.pathitem.pathItemId,
        this.dataSet.id,
        1,
        this.pathitem.tsIndex)
      .subscribe(res => {
      this.grid.loading = false;
      this.handleResponse(res);
    });
  }

  handleResponse(res) {
    if (res.isLinkedLastVersion === true) {
      this.showIsLinkedLastVersionDialog = true;
    }
    if (res.isLinkedNotLastVersion === true) {
      this.showIsLinkedNotLastVersionDialog = true;
    }
    if (!res.isLinkedLastVersion && !res.isLinkedNotLastVersion) {
      this.rows = this.rows.filter(x => x.rowId !== this.rowId);
      this.dataSet.nrRows = this.dataSet.nrRows - 1;
      this.showIsLinkedLastVersionDialog = false;
      this.showIsLinkedNotLastVersionDialog = false;
      this.closeParent.emit(res.id);
    }
  }

  close(action: string) {
    this.viewDataSetRow.grid.closeCell();
    this.viewDataSetRow.grid.cancelCell();
    if (action === 'confirm') {
      const dsRows = this.selectedRow['row'].map(x => {
        return {
          name: x['Name'],
          value: x['Value']
        } as DataAttribute;
      });

      this.dataManagementService
        .updateDataSetItem(this.selectedRow.rowId, this.dataSet.id, {
          attributes: dsRows
        })
        .subscribe(result => {
          this.datasetsWindow.close();
        });
    } else {
      this.datasetsWindow.close();
    }
  }

  private closeDialog(): void {
    this.showIsLinkedLastVersionDialog = false;
  }

  private closeNotLastDialog(): void {
    this.showIsLinkedNotLastVersionDialog = false;
  }

  public updateClick(): void {
    this.dataManagementService
      .deleteDataSetItem(
        this.rowId,
        this.pathitem.workflowId,
        this.pathitem.workflowPathId,
        this.pathitem.pathItemId,
        this.dataSet.id,
        2,
        this.pathitem.tsIndex)
      .subscribe(res => {
      this.grid.loading = false;
      this.handleResponse(res);
    });
  }

  public createNewClick(): void {
    this.dataManagementService
      .deleteDataSetItem(
        this.rowId,
        this.pathitem.workflowId,
        this.pathitem.workflowPathId,
        this.pathitem.pathItemId,
        this.dataSet.id,
        3,
        this.pathitem.tsIndex)
      .subscribe(res => {
      this.grid.loading = false;
      this.handleResponse(res);
    });
  }

  public createNewNotLastClick(): void {
    this.dataManagementService
      .deleteDataSetItem(
        this.rowId,
        this.pathitem.workflowId,
        this.pathitem.workflowPathId,
        this.pathitem.pathItemId,
        this.dataSet.id,
        4,
        this.pathitem.tsIndex)
      .subscribe(res => {
      this.grid.loading = false;
      this.handleResponse(res);
    });
  }
}
