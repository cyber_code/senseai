import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild
} from '@angular/core';
import {SuggestedDataSet, DataSet} from '../../models/path-dataset.model';
import {DataManagementService} from '../../services/data-management.service';
import {WorkflowPathItem} from '../../models/workflowPathItem.model';
import {SelectionEvent, GridComponent} from '@progress/kendo-angular-grid';
import {PermissionService} from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-suggested-datasets',
  templateUrl: './suggested-datasets.component.html',
  styleUrls: ['./suggested-datasets.component.css']
})
export class SuggestedDatasetsComponent implements OnInit {
  private createdDataSets: string[] = [];
  public selectedDataSetsIds: string[] = [];
  public suggestedDataSets: SuggestedDataSet[] = [];
  @Input() public pathItem: WorkflowPathItem;
  @Input() public workflowId: string;
  @Output() public dataSetCreated = new EventEmitter<DataSet>();
  @ViewChild('grid') grid: GridComponent;

  constructor(
    private dataManagementService: DataManagementService,
    public permissionService: PermissionService) {
    
  }

  ngOnInit() {
    if (this.pathItem.typicalName === '') {
      return;
    }
    this.grid.loading = true;
    if(this.permissionService.hasPermission('/api/Data/GetSuggestedDataSets')){
      this.dataManagementService
      .getSuggestedDataSets(
        this.pathItem.catalogId,
        this.pathItem.typicalName,
        this.pathItem.typicalId,
        this.workflowId,
        this.pathItem.pathItemId
      )
      .subscribe(result => {
        this.suggestedDataSets = result;
        this.grid.loading = false;
      });
    }
    else {
      this.suggestedDataSets = [];
      this.grid.loading = false;
    }
  }

  dataSetSelectionChanged($event: SelectionEvent) {
    if ($event.selectedRows.length === 0) {
      return;
    }
    const selectedDataSets = $event.selectedRows.map(
      row => row.dataItem as SuggestedDataSet
    )[0];

    if (this.createdDataSets.find(x => x === selectedDataSets.title)) {
      return;
    }
    this.dataManagementService
      .addDataSet(this.pathItem.typicalId, selectedDataSets)
      .subscribe(result => {
        // notify parent that a new data set is created
        this.dataSetCreated.emit({
          id: result.id,
          title: selectedDataSets.title,
          nrRows: selectedDataSets.rows.length,
          coverage: 0,
          attributes: [],
          aiGenerated: true
        });
        this.selectedDataSetsIds = [];
      });
  }
}
