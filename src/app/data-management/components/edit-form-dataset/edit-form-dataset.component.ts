import { Component, ElementRef, EventEmitter, Input, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GridComponent } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';
import { WorkflowPathItem } from 'src/app/data-management/models/workflowPathItem.model';
import { AutoUnsubscribe, takeWhileAlive } from 'take-while-alive';
import { DataAttribute } from '../../models/data-attribute';
import { DataSet } from '../../models/path-dataset.model';
import { DataManagementService } from '../../services/data-management.service';
import { EditService } from './edit.service';

@Component({
  selector: "app-edit-form-dataset",
  templateUrl: "./edit-form-dataset.component.html",
  styleUrls: ["./edit-form-dataset.component.css"]
})
@AutoUnsubscribe()
export class EditFormDatasetComponent implements OnInit {
  public active = false;
  public editForm: FormGroup = new FormGroup({
    id: new FormControl(),
    title: new FormControl("", Validators.required),
    coverage: new FormControl("0")
  });
  @ViewChild("grid") grid: GridComponent;
  public gridData: any;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10,
    filter: {
      logic: "and",
      filters: [] // [{ field: 'ProductName', operator: 'contains', value: 'Chef' }]
    }
  };
  public changes: any = {};
  public combinations: DataAttribute[] = [];
  public checkedShowAll: boolean = true;
  public mandatory: number = 2;
  public showIsLinkedLastVersionDialog = false;
  public showIsLinkedNotLastVersionDialog = false;
  isLoadingUpdateOrCreateNewDataset = false;

  @Input() public isNew = false;
  @Input() public pathItem: WorkflowPathItem;
  @Output() public refreshDatasets = new EventEmitter<string>();

  @Input()
  public set model(dataSet: DataSet) {
    this.editForm.reset(dataSet);
    this.active = dataSet !== undefined;
    if (this.active) {
      this.editService.read(this.pathItem.typicalId, this.mandatory, dataSet);
    }
  }

  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() save: EventEmitter<DataSet> = new EventEmitter();
  private element: any;

  constructor(private dataManagementService: DataManagementService, private formBuilder: FormBuilder, public editService: EditService, private el: ElementRef, private renderer: Renderer2) {
    // this.element = el.nativeElement;
  }

  ngOnInit() {
    this.processData();
  }

  public onSave(e): void {
    e.preventDefault();
    this.saveChanges(this.grid);
    // this.active = false;
    this.checkedShowAll = true;
    this.mandatory = 2;
    this.resetGridState();
  }

  public onCancel(e): void {
    e.preventDefault();
    this.editService.cancelChanges();
    this.closeForm();
  }

  private closeForm(): void {
    this.active = false;
    this.checkedShowAll = true;
    this.mandatory = 2;
    this.resetGridState();
    this.editService.cancelChanges();
    this.cancel.emit();
    this.editService.reset();
  }

  private closeDialog(): void {
    this.showIsLinkedLastVersionDialog = false;
  }

  private closeNotLastDialog(): void {
    this.showIsLinkedNotLastVersionDialog = false;
  }

  //#region combinations
  public onStateChange(state: State): void {
    if (state.filter.filters.length === 0) {
      // const btnElement = this.element.querySelector('#clearFilters');
      // btnElement.click();
      // this.renderer.listen(btnElement, 'click', () => {
      this.clearFilters();
      // });
    } else {
      this.gridState = state;
    }
    this.grid.closeCell();
    this.grid.cancelCell();
    this.editService.read(
      this.pathItem.typicalId,
      this.mandatory,
      this.editForm.value
    );
    this.processData();
  }

  processData() {
    this.editService.pipe(takeWhileAlive(this)).subscribe(data => {
      this.gridData = process(data, this.gridState);
    });
  }

  public cellClickHandler({
    sender,
    rowIndex,
    columnIndex,
    dataItem,
    isEdited
  }) {
    if (!isEdited) {
      sender.editCell(rowIndex, columnIndex, this.createFormGroup(dataItem));
    }
  }

  public cellCloseHandler(args: any) {
    const { formGroup, dataItem } = args;

    if (!formGroup.valid) {
      args.preventDefault();
    } else if (formGroup.dirty) {
      this.editService.assignValues(dataItem, formGroup.value);
      this.editService.update(dataItem);
    }
  }

  public addHandler({ sender }) {
    sender.addRow(this.createFormGroup(new DataAttribute()));
  }

  public cancelHandler({ sender, rowIndex }) {
    sender.closeRow(rowIndex);
  }

  public saveHandler({ sender, formGroup, rowIndex }) {
    if (formGroup.valid) {
      this.editService.create(formGroup.value);
      sender.closeRow(rowIndex);
    }
  }

  public removeHandler({ sender, dataItem }) {
    this.editService.remove(dataItem);

    sender.cancelCell();
  }

  public saveChanges(grid: any): void {
    grid.closeCell();
    grid.cancelCell();
    this.grid.loading = true;
    this.editService
      .saveChanges(
        this.pathItem.workflowId,
        this.pathItem.workflowPathId,
        this.pathItem.pathItemId,
        this.pathItem.typicalId,
        this.pathItem.typicalName,
        this.pathItem.typicalType,
        this.editForm.value,
        1,
        this.pathItem.tsIndex
      )
      .subscribe(res => {
        this.grid.loading = false;
        //this.save.emit(this.editForm.value);
        this.handleResponse(res);
      });
    //this.editService.reset();
  }

  handleResponse(res) {
    if (res.isLinkedLastVersion === true) {
      this.showIsLinkedLastVersionDialog = true;
    }
    if (res.isLinkedNotLastVersion === true) {
      this.showIsLinkedNotLastVersionDialog = true;
    }
    if (!res.isLinkedLastVersion && !res.isLinkedNotLastVersion) {
      this.save.emit(this.editForm.value);
      this.closeForm();
    }
  }

  public updateClick(): void {
    this.isLoadingUpdateOrCreateNewDataset = true;
    this.editService
      .saveChanges(
        this.pathItem.workflowId,
        this.pathItem.workflowPathId,
        this.pathItem.pathItemId,
        this.pathItem.typicalId,
        this.pathItem.typicalName,
        this.pathItem.typicalType,
        this.editForm.value,
        2,
        this.pathItem.tsIndex
      )
      .subscribe(
        (res) => {
          this.isLoadingUpdateOrCreateNewDataset = false;
          this.closeForm();
          this.refreshDatasets.emit(undefined);
        },
        (err) => {
          this.isLoadingUpdateOrCreateNewDataset = false;
        }
      );
    this.showIsLinkedLastVersionDialog = false;
  }

  public createNewClick(): void {
    this.isLoadingUpdateOrCreateNewDataset = true;
    this.editService
      .saveChanges(
        this.pathItem.workflowId,
        this.pathItem.workflowPathId,
        this.pathItem.pathItemId,
        this.pathItem.typicalId,
        this.pathItem.typicalName,
        this.pathItem.typicalType,
        this.editForm.value,
        3,
        this.pathItem.tsIndex
      )
      .subscribe(
        (res) => {
          this.isLoadingUpdateOrCreateNewDataset = false;
          this.closeForm();
          this.refreshDatasets.emit(res.id);
        },
        (err) => {
          this.isLoadingUpdateOrCreateNewDataset = false;
        }
      );
    this.showIsLinkedLastVersionDialog = false;
  }

  public createNewNotLastClick(): void {
    this.isLoadingUpdateOrCreateNewDataset = true;
    this.editService
      .saveChanges(
        this.pathItem.workflowId,
        this.pathItem.workflowPathId,
        this.pathItem.pathItemId,
        this.pathItem.typicalId,
        this.pathItem.typicalName,
        this.pathItem.typicalType,
        this.editForm.value,
        3,
        this.pathItem.tsIndex
      )
      .subscribe(
        (res) => {
          this.isLoadingUpdateOrCreateNewDataset = false;
          this.closeForm();
          this.refreshDatasets.emit(res.id);
        },
        (err) => {
          this.isLoadingUpdateOrCreateNewDataset = false;
        }
      );
    //this.showIsLinkedLastVersionDialog = false;
    this.showIsLinkedNotLastVersionDialog = false;
  }

  public cancelChanges(grid: any): void {
    grid.cancelCell();
    this.editService.cancelChanges();
    this.showIsLinkedLastVersionDialog = false;
  }

  public createFormGroup(dataItem: any): FormGroup {
    return this.formBuilder.group({
      name: dataItem.name,
      value: dataItem.value
    });
  }

  public checkShowMandatoryFields(evt): void {
    this.checkedShowAll = false;
    this.mandatory = 1;
    this.resetGridState();
    this.editService.read(
      this.pathItem.typicalId,
      this.mandatory,
      this.editForm.value
    );
    this.processData();
  }

  public checkShowAll(evt): void {
    this.checkedShowAll = true;
    this.mandatory = 2;
    this.resetGridState();
    this.editService.read(
      this.pathItem.typicalId,
      this.mandatory,
      this.editForm.value
    );
    this.processData();
  }

  resetGridState() {
    this.gridState = {
      sort: [],
      skip: 0,
      take: 10,
      filter: {
        logic: "and",
        filters: []
      }
    };
  }

  public clearFilters() {
    this.gridState = {
      sort: [],
      skip: 0,
      take: 10,
      filter: {
        logic: 'and',
        filters: [{ field: 'value', operator: 'contains', value: '' }]
      }
    };
    this.grid.closeCell();
    this.grid.cancelCell();
  }

  //#endregion
}
