import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

import { Observable } from "rxjs/Observable";
import { zip } from "rxjs/observable/zip";
import { map } from "rxjs/operators/map";
import { DataManagementService } from "../../services/data-management.service";
import { DataSet, DataSetCombination } from "../../models/path-dataset.model";
import { isNullOrUndefined } from "util";
import { Combinator } from "@progress/kendo-data-query/dist/npm/common.interfaces";

const CREATE_ACTION = "create";
const UPDATE_ACTION = "update";
const REMOVE_ACTION = "destroy";

const itemIndex = (item: any, data: any[]): number => {
  for (let idx = 0; idx < data.length; idx++) {
    if (data[idx].name === item.name) {
      return idx;
    }
  }

  return -1;
};

const cloneData = (data: any[]) => data.map(item => Object.assign({}, item));

@Injectable()
export class EditService extends BehaviorSubject<any[]> {
  private data: any[] = [];
  private originalData: any[] = [];
  private createdItems: any[] = [];
  private updatedItems: any[] = [];
  private deletedItems: any[] = [];

  constructor(private dataManagementService: DataManagementService) {
    super([]);
  }

  public read(typicalId: string, mandatory, dataSet: DataSet) {
    if (this.data.length) {
      if (mandatory === 1) {
        const mandatoryData = this.data.filter(data => data.isRequired);
        return super.next(mandatoryData);
      }
      return super.next(this.data);
    }
    this.fetch(typicalId, mandatory, dataSet).subscribe(data => {
      this.data = data;
      this.originalData = cloneData(data);
      super.next(data);
    });
  }

  public create(item: any): void {
    this.createdItems.push(item);
    this.data.unshift(item);

    super.next(this.data);
  }

  public update(item: any): void {
    if (!this.isNew(item)) {
      const index = itemIndex(item, this.updatedItems);
      if (index !== -1) {
        this.updatedItems.splice(index, 1, item);
      } else {
        this.updatedItems.push(item);
      }
    } else {
      const index = this.createdItems.indexOf(item);
      this.createdItems.splice(index, 1, item);
    }
  }

  public remove(item: any): void {
    let index = itemIndex(item, this.data);
    this.data.splice(index, 1);

    index = itemIndex(item, this.createdItems);
    if (index >= 0) {
      this.createdItems.splice(index, 1);
    } else {
      this.deletedItems.push(item);
    }

    index = itemIndex(item, this.updatedItems);
    if (index >= 0) {
      this.updatedItems.splice(index, 1);
    }

    super.next(this.data);
  }

  public isNew(item: any): boolean {
    return !item.name;
  }

  public hasChanges(): boolean {
    return Boolean(
      this.deletedItems.length ||
        this.updatedItems.length ||
        this.createdItems.length
    );
  }

  public saveChanges(
    workflowId: string,
    workflowPathId: string,
    workflowPathItemId: string,
    typicalId: string,
    typicalName: string,
    typicalType: string,
    dataSet: DataSet,
    updateStatus: number,
    index: number
  ): Observable<any> {
    // set existing all combinations
    dataSet.combinations = this.data.map(attr => {
      return {
        attribute: attr.name,
        values: (attr.value && attr.value.split("|")) || []
      };
    });
    if (this.hasChanges()) {
      const updatedCombinations = this.updatedItems.map(x => {
        return {
          attribute: x.name,
          values: x.value.split("|")
        } as DataSetCombination;
      });
      // replace the ones that are updated
      dataSet.combinations.forEach(c => {
        const updated = updatedCombinations.find(
          x => x.attribute === c.attribute
        );
        if (updated) {
          c.values = updated.values;
        }
      });
    }
    const filledcombination = [];
    dataSet.combinations.forEach(el => {
      if (el.values && el.values.length > 0) {
        filledcombination.push(el);
      }
    });
    dataSet.combinations = filledcombination;
    const isNew = isNullOrUndefined(dataSet.id);
    if (isNew) {
      return this.dataManagementService.addManualDataSet(
        typicalId,
        typicalName,
        typicalType,
        dataSet
      );
    } else {
      return this.dataManagementService.updateManualDataSet(
        workflowId,
        workflowPathId,
        workflowPathItemId,
        typicalId,
        typicalName,
        typicalType,
        dataSet,
        updateStatus,
        index
      );
    }
  }

  public cancelChanges(): void {
    this.reset();

    this.data = this.originalData;
    this.originalData = cloneData(this.originalData);
    super.next(this.data);
  }

  public assignValues(target: any, source: any): void {
    Object.assign(target, source);
  }

  public reset() {
    this.data = [];
    this.deletedItems = [];
    this.updatedItems = [];
    this.createdItems = [];
  }

  private getValuefromCombination(
    combinations: DataSetCombination[],
    attribute: string
  ): any {
    const comb = combinations.find(m => m.attribute === attribute);
    if (isNullOrUndefined(comb)) {
      return "";
    } else {
      return comb.values && comb.values.join("|");
    }
  }

  public fetch(
    typicalId: string,
    mandatory: number,
    dataSet: DataSet
  ): Observable<any[]> {
    if (isNullOrUndefined(dataSet.id)) {
      return this.dataManagementService.getTypicalAttributes(
        typicalId,
        mandatory
      );
    } else {
      return this.dataManagementService
        .getDataSetWithCombinations(dataSet.id, mandatory)
        .pipe(
          map(x => {
            return (x.attributes || []).map(c => {
              return {
                name: c["name"],
                value: this.getValuefromCombination(x.combinations, c["name"]),
                isRequired: c["isRequired"]
              };
            });
          })
        );
    }
  }
}
