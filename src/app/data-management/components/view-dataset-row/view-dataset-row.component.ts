import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {DataAttribute} from '../../models/data-attribute';
import {GridDataResult, GridComponent} from '@progress/kendo-angular-grid';
import {State, process} from '@progress/kendo-data-query';
import {FormGroup, FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-view-dataset-row',
  templateUrl: './view-dataset-row.component.html',
  styleUrls: ['./view-dataset-row.component.css']
})
export class ViewDatasetRowComponent implements OnInit {
  instanceAttributes: DataAttribute[] = [];
  public gridData: GridDataResult;
  @ViewChild('grid') grid: GridComponent;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10,
    filter: {
      logic: 'and',
      filters: [] // [{ field: 'ProductName', operator: 'contains', value: 'Chef' }]
    }
  };
  private createdItems: any[] = [];
  private updatedItems: any[] = [];

  @Input()
  public set model(row: any) {
    this.instanceAttributes = row.row;
    this.gridData = process(this.instanceAttributes, this.gridState);
  }

  itemIndex = (item: any, data: any[]): number => {
    for (let idx = 0; idx < data.length; idx++) {
      if (data[idx].name === item.name) {
        return idx;
      }
    }

    return -1;
  };

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit() {
  }

  //#region combinations
  public onStateChange(state: State) {
    this.gridState = state;
    this.gridData = process(this.instanceAttributes, this.gridState);
  }

  public cellClickHandler({
                            sender,
                            rowIndex,
                            columnIndex,
                            dataItem,
                            isEdited
                          }) {
    if (!isEdited) {
      sender.editCell(rowIndex, columnIndex, this.createFormGroup(dataItem));
    }
  }

  public cellCloseHandler(args: any) {
    const {formGroup, dataItem} = args;

    if (!formGroup.valid) {
      // prevent closing the edited cell if there are invalid values.
      args.preventDefault();
    } else if (formGroup.dirty) {
      this.assignValues(dataItem, formGroup.value);
      this.update(dataItem);
    }
  }

  public createFormGroup(dataItem: any): FormGroup {
    return this.formBuilder.group({
      Name: dataItem.Name,
      Value: dataItem.Value
    });
  }

  public assignValues(target: any, source: any): void {
    Object.assign(target, source);
  }

  public update(item: any): void {
    if (!this.isNew(item)) {
      const index = this.itemIndex(item, this.updatedItems);
      if (index !== -1) {
        this.updatedItems.splice(index, 1, item);
      } else {
        this.updatedItems.push(item);
      }
    } else {
      const index = this.createdItems.indexOf(item);
      this.createdItems.splice(index, 1, item);
    }
  }

  public isNew(item: any): boolean {
    return !item.name;
  }
}
