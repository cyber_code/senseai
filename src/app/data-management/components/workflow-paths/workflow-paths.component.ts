import {GridComponent} from '@progress/kendo-angular-grid';
import {Component, OnInit, ViewChild} from '@angular/core';
import {DataManagementService} from '../../services/data-management.service';
import {DataFlowService} from '../../services/data-flow.service';
import {isNullOrUndefined} from 'util';
import {WorkflowPath} from '../../models/workflowPath.model';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import {PermissionService} from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-workflow-paths',
  templateUrl: './workflow-paths.component.html',
  styleUrls: ['./workflow-paths.component.css']
})
@AutoUnsubscribe()
export class WorkflowPathsComponent implements OnInit {
  @ViewChild('grid') grid: GridComponent;
  public selectedWorkflowPath: WorkflowPath;
  public workflowPaths: WorkflowPath[];
  public workflowId: string;
  public selectedKeys: string[] = [];
  public loadingPending: Boolean = false;

  constructor(
    private dataManagementService: DataManagementService,
    private dataFlow: DataFlowService,
    public permissionService: PermissionService
  ) {
    this.dataFlow.workflowSelected$
      .pipe(takeWhileAlive(this))
      .subscribe(_workflowId => {
        this.loadWorkflowPaths(_workflowId);
      });
    this.dataFlow.pathItemsLoaded$.pipe(takeWhileAlive(this)).subscribe(x => {
      this.loadingPending = false;
    });
  }

  private loadWorkflowPaths(_workflowId: string) {
    this.workflowId = _workflowId;
    this.loadingPending = false;
    this.selectedKeys = [];
    // clear path items
    this.dataFlow.changeWorkflowPath(new WorkflowPath());
    if (isNullOrUndefined(_workflowId)) {
      this.workflowPaths = [];
    } else {
      this.grid.loading = true;
      if (this.permissionService.hasPermission('/api/Design/GetWorkflow') && this.permissionService.hasPermission('/api/Data/GetWorkflowPaths')) {
        this.dataManagementService
        .getWorkflowPaths(_workflowId)
        .subscribe(_workflowPaths => {
          this.workflowPaths = _workflowPaths ? _workflowPaths : [];
          this.grid.loading = false;
        });
      }
      else {
      this.workflowPaths=[];
      this.grid.loading = false;
      }
    }
  }

  ngOnInit() {
  }

  handleSelection($event: any) {
    if (this.selectedWorkflowPath) {
      this.selectedKeys = [this.selectedWorkflowPath.pathId];
    } else {
      this.selectedKeys = [];
    }
  }

  pathClicked(path: WorkflowPath) {
    if (this.loadingPending) {
      return;
    }
    this.selectedWorkflowPath = path;
    path.workflowId = this.workflowId;
    this.dataFlow.changeWorkflowPath(path);
    this.loadingPending = true;
    this.selectedKeys = [path.pathId];
  }
}
