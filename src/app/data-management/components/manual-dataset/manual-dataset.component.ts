import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { WindowService } from '@progress/kendo-angular-dialog';
import { GridComponent, GridDataResult, SelectableSettings, SelectionEvent } from '@progress/kendo-angular-grid';
import { process, State } from '@progress/kendo-data-query';
import { Observable } from 'rxjs';
import { PermissionService } from 'src/app/core/services/permission.service';
import { WorkflowPathItem } from 'src/app/data-management/models/workflowPathItem.model';
import { isNullOrUndefined } from 'util';
import { DataSet } from '../../models/path-dataset.model';
import { DataFlowService } from '../../services/data-flow.service';
import { DataManagementService } from './../../services/data-management.service';

@Component({
  selector: 'app-manual-dataset',
  templateUrl: './manual-dataset.component.html',
  styleUrls: ['./manual-dataset.component.css']
})
export class ManualDatasetComponent implements OnInit {
  public dataSets: DataSet[] = [];
  public selectedDataSetsIds: string[] = [];
  selectedDataSet: DataSet;
  @Input() pathItem: WorkflowPathItem;
  @ViewChild('grid') grid: GridComponent;
  public view: Observable<GridDataResult>;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 5,
    filter: {
      logic: 'and',
      filters: [] // [{ field: 'ProductName', operator: 'contains', value: 'Chef' }]
    }
  };
  public pagesize: 10;
  public skip = 0;
  public editDataItem: DataSet;
  public isNew: boolean;
  public selectableSettings: SelectableSettings;
  public viewDsWindow;

  constructor(
    private dataManagementService: DataManagementService,
    private dataFlowService: DataFlowService,
    private windowService: WindowService,
    public permissionService: PermissionService
  ) {
    this.selectableSettings = {
      mode: 'single'
    };
  }

  ngOnInit() {
    // load datasets for this path item
    if(this.permissionService.hasPermission('/api/Data/GetDataSets')){
      this.loadDataSets();
    }
  }

  private loadDataSets(datasetNewId?, closeviewDsWindow = false) {
    this.dataSets = [];
    if (this.pathItem.typicalName === '') {
      return;
    }
    this.grid.loading = true;
    this.dataManagementService
      .getDataSets(this.pathItem.catalogId, this.pathItem.typicalName, this.pathItem.typicalId)
      .subscribe(response => {
        this.dataSets = response || [];
        this.setSelectedRows(datasetNewId);
        this.grid.data = process(this.dataSets, this.gridState);
        this.grid.loading = false;
        if (closeviewDsWindow) {
          this.viewDsWindow.close();
        }
      });
  }

  setSelectedRowFirst(datasetNewId){
    let dataObject = this.dataSets.filter((data) => data.id === datasetNewId || this.selectedDataSetsIds[0] );
    let index = this.dataSets.indexOf(dataObject[0]);
    this.dataSets.unshift(this.dataSets[index]);
    this.dataSets.splice(index+1,1);
  }

  public onStateChange(state: State) {
    this.gridState = state;
    this.grid.data = process(this.dataSets, this.gridState);
  }

  addSuggestedDataSet(dataSet: DataSet) {
    this.dataSets = [dataSet, ...this.dataSets];
    this.grid.data = process(this.dataSets, this.gridState);
  }

  private setSelectedRows(datasetNewId) {
    const selectedIds = this.dataFlowService.getDataSetsPerPath().dataSetsIds;
    const dataSetPerPathItem =
      !isNullOrUndefined(selectedIds) &&
      selectedIds.find(x => x.PathItemId === this.pathItem.pathItemId && x.TSIndex === this.pathItem.tsIndex);
    this.selectedDataSetsIds = datasetNewId ? [datasetNewId] :
      (dataSetPerPathItem && dataSetPerPathItem.Ids) || [];
    this.setSelectedRowFirst(datasetNewId);
  }

  public selectionChanged($event: SelectionEvent) {
    const selectedIds = $event.selectedRows.map(x => x.dataItem.id);
    const deselectedIds = $event.deselectedRows.map(x => x.dataItem.id);
    this.selectedDataSetsIds = selectedIds;
    this.selectedDataSetsIds = this.selectedDataSetsIds.filter(x =>
      isNullOrUndefined(deselectedIds.find(v => v === x))
    );
  }

  public addHandler($event: any) {
    this.editDataItem = new DataSet();
    this.isNew = true;
  }

  public editHandler({dataItem}) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
  }

  public saveHandler(product: DataSet) {
    this.loadDataSets();
  }

  viewRows(item: DataSet, rowsTemplate: any) {
    this.selectedDataSet = item;
    this.viewDsWindow = this.windowService.open({
      title: `${item.title} rows`,
      content: rowsTemplate,
      width: 920,
      height: 850
    });
  }

  public removeHandler({dataItem}) {
    this.dataManagementService.deleteDataSet(dataItem.id).subscribe(result => {
      this.dataSets = this.dataSets.filter(x => x.id !== dataItem.id);
    });
  }


}
