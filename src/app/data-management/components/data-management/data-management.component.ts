import {MessageService} from 'src/app/core/services/message.service';
import {Component, OnInit, ViewChild} from '@angular/core';
import {TreeViewItem} from 'src/app/shared/tree-view-item';
import {isNullOrUndefined} from 'util';
import {NodeType} from 'src/app/shared/node-type';
import {DataFlowService} from '../../services/data-flow.service';
import {DataManagementService} from '../../services/data-management.service';
import {MessageType} from 'src/app/core/models/message.model';
import {ProjectSelectionService} from 'src/app/core/components/project-selection/project-selection.service';
import {takeWhileAlive, AutoUnsubscribe} from 'take-while-alive';
import { DesignService } from 'src/app/design/services/design.service';
import {WorkflowItemActionType} from 'src/app/core/models/workflow-item-action-type';
import { SessionService } from '../../../core/services/session.service';
import { guid } from '@progress/kendo-angular-dropdowns/dist/es2015/util';
import { Guid } from 'src/app/shared/guid';
import { WorkflowPath } from 'src/app/test-generation/models/workflow-path.model';
import { DataSetsPerPathItem } from '../../models/workflowPath.model';
import { PathItemsComponent } from '../path-items/path-items.component';
import { PermissionService } from 'src/app/core/services/permission.service';

@Component({
  selector: 'app-data-management',
  templateUrl: './data-management.component.html',
  styleUrls: ['./data-management.component.css']
})
@AutoUnsubscribe()
export class DataManagementComponent implements OnInit {
  @ViewChild('pathItems') pathItems: PathItemsComponent;
  selectedTreeViewItem: TreeViewItem;
  public workflowId: string;
  public menuDisabled: Boolean = true;

  public workflowWarningDialogOpen: boolean = false;
  public workflowWarningMessage: string = '';
  public selectedItem: any;
  public hasInputItems = true;

  constructor(
    private dataFlow: DataFlowService,
    private dataManagementService: DataManagementService,
    private messageService: MessageService,
    private projectSelectionService: ProjectSelectionService,
    private designService: DesignService,
    private sessionService: SessionService,
    public permissionService: PermissionService
  ) {
    this.projectSelectionService.subProjectSelected$
      .pipe(takeWhileAlive(this))
      .subscribe(p => {
        this.dataFlow.changeWorkflowId(null);
      });
    this.dataFlow.workflowPathSelected$
      .pipe(takeWhileAlive(this))
      .subscribe(path => {
        this.menuDisabled = isNullOrUndefined(path.pathId);
      });
  }

  ngOnInit() {
    this.dataFlow.workflowSelected$
      .pipe(takeWhileAlive(this))
      .subscribe(_workflowId => (this.workflowId = _workflowId));
  }

  menuClick($event, skipStatus: boolean = false) {
    const inputs = this.pathItems.workflowPathItems.filter(x => +x.actionType === WorkflowItemActionType.Input).length;
    if ($event.item.data === 'generateTC') {
      const dataSetsPerPath = this.dataFlow.getDataSetsPerPath();
      if (
        (dataSetsPerPath.dataSetsIds &&
        dataSetsPerPath.dataSetsIds.length === 0 && this.hasInputItems) ||
        !this.checkForEmtpyGiud(dataSetsPerPath.dataSetsIds) ||
        dataSetsPerPath.dataSetsIds.length !== inputs
      ) {
        this.messageService.sendMessage({
          text: 'Please select data sets for all Application steps, in order to generate test cases!',
          type: MessageType.Warning
        });
        return;
      }
      const workContext = this.sessionService.getWorkContext();
      this.dataManagementService
        .generateTestCases(
          workContext.user.tenantId,
          workContext.project.id,
          workContext.subProject.id,
          workContext.defaultSettings.catalog.id,
          workContext.defaultSettings.system.id,
          this.selectedTreeViewItem.id,
          dataSetsPerPath.pathId,
          dataSetsPerPath.dataSetsIds,
          skipStatus
        )
        .subscribe((res: any) => {
          if (res && res.response) {
            this.selectedItem = $event;
            this.workflowWarningMessage = res.response.message;
            this.openWorkflowWarningDialog();
          } else if (res) {
            this.messageService.sendMessage({
              text: ' Test Cases are generated in the background!',
              type: MessageType.Info
            });
            this.closeWorkflowWarningDialog();
          }
        });
    }
  }

  checkForEmtpyGiud(pathIds: DataSetsPerPathItem[]) {
    let isValid = true;
    if (pathIds.find(element => element.Ids.length === 0)) {
      isValid = false;
      return isValid;
    }
    pathIds.forEach(element => {
      element.Ids.forEach(element2 => {
          if (!element2 || element2 === Guid.empty) {
            isValid = false;
            return isValid;
          }
        });
      });
      return isValid;
    }

  treeViewItemSelected(treeViewItem: TreeViewItem) {
    if (
      !isNullOrUndefined(this.selectedTreeViewItem) &&
      this.selectedTreeViewItem.id === treeViewItem.id
    ) {
      return;
    }
    this.selectedTreeViewItem = treeViewItem;
    if (treeViewItem.type === NodeType.Workflow) {
      this.designService.getWorkflow(this.selectedTreeViewItem.id).subscribe(res => {
        let designer = (res && res.designerJson && JSON.parse(res.designerJson)) || {cells: []};
        if (designer.cells.length > 0 && !designer.cells.find(cell =>
          cell.customData && cell.customData.ActionType === WorkflowItemActionType.Input)) {
          this.hasInputItems = false;
          this.menuDisabled = false;
        } else {
          this.hasInputItems = true;
        }
      });
      this.dataFlow.changeWorkflowId(treeViewItem.id);
    } else {
      this.dataFlow.changeWorkflowId(null);
      this.hasInputItems = true;
    }
  }

  proceedTestCaseGeneration() {
    this.menuClick(this.selectedItem, true);
  }

  closeWorkflowWarningDialog() {
    this.workflowWarningDialogOpen = false;
  }

  openWorkflowWarningDialog() {
    this.workflowWarningDialogOpen = true;
  }
}
