import {DataAttribute} from './data-attribute';
import {Attribute} from 'src/app/design/models/attribute.model';

export class PathDataSet {
  id: string;
  title: string;
  pathItemId: string;
}

export class PathDataSets {
  error: string;
  hasResult: boolean;
  result: PathDataSet[];
  successful: boolean;
}

export class SuggestedDataSet {
  title: string;
  rows: DataSetRow[];
}

export class DataSetRow {
  attributes: DataAttribute[];
}

export class DataSet {
  id: string;
  title: string;
  nrRows: number;
  coverage: number;
  combinations?: DataSetCombination[];
  attributes: Attribute[];
  aiGenerated: boolean;
}

export class DataSetCombination {
  attribute: string;
  values: string[];
}

export class DataSetItem {
  id: string;
  dataSetId: string;
  row: DataSetRow;
}
