export class DataAttribute {
  name: string;
  value: string;
}
