export class WorkflowPathItem {
  workflowId: string;
  workflowPathId: string;
  pathItemId: string;
  pathItemTitle: string;
  typicalName: string;
  typicalId: string;
  actionType: string;
  typicalType: string;
  dataSetId: string;
  catalogId: string;
  tsIndex: number;
}
