export class WorkflowPath {
  pathId: string;
  workflowId: string;
  pathTitle: string;
  coverage: number;
  dataSetsIds: DataSetsPerPathItem[];
}

export class DataSetsPerPathItem {
  PathItemId: string;
  TSIndex: number;
  Ids: string[];
}
