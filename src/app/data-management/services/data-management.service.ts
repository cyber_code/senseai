import { isNullOrUndefined, isBuffer } from 'util';
import { DataAttribute } from '../models/data-attribute';
import { SessionService } from '../../core/services/session.service';
import {
  GetSuggestedDataSets,
  AddDataSet,
  DeleteDataSetItem,
  GetDataSetItem,
  DeleteDataSet,
  GetTypicalAttributes,
  AddManualDataSet,
  UpdateManualDataSet,
  GetDataSet,
  UpdateDataSetItem,
  GetTypicalByName,
  SetDataSetForPathItem
} from '../commands-queries/data-management-command-queries';
import { Observable, of } from 'rxjs';
import { HttpExecutorService } from 'src/app/core/services/http-executor.service';
import { ConfigurationService } from 'src/app/core/services/configuration.service';
import { Injectable } from '@angular/core';
import {
  GetWorkflowPaths,
  GetWorkflowPathItems,
  GetDataSets,
  GenerateTestCases
} from '../commands-queries/data-management-command-queries';
import { WorkflowPath, DataSetsPerPathItem } from '../models/workflowPath.model';
import { WorkflowPathItem } from '../models/workflowPathItem.model';
import { SuggestedDataSet, DataSet, DataSetRow } from '../models/path-dataset.model';
import { CommandMethod } from 'src/app/shared/command-method';
import { map } from 'rxjs/operators';

@Injectable()
export class DataManagementService {
  private dataUrl: string;

  constructor(
    private configurationService: ConfigurationService,
    private httpExecutor: HttpExecutorService,
    private sessionService: SessionService
  ) {
      this.dataUrl = `${configurationService.serverSettings.apiUrl}Data/`;
  }

  generateTestCases(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string,
    workflowId: string, workflowPathId: string, dataSetsPerPathItem: DataSetsPerPathItem[], skipStatus: boolean): Observable<void> {
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new GenerateTestCases(tenantId, projectId, subProjectId, catalogId, systemId, workflowId,
         workflowPathId, dataSetsPerPathItem, skipStatus),
      CommandMethod.POST
    ); }

  setDataSetForPathItem(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string,
    workflowId: string, workflowPathId: string, workflowPathItemId: string, dataSetId: string, skipStatus: boolean,
    index: number): Observable<void> {
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new SetDataSetForPathItem(tenantId, projectId, subProjectId, catalogId, systemId, workflowId,
         workflowPathId, workflowPathItemId, dataSetId, skipStatus, index),
      CommandMethod.POST
    ); }

  getWorkflowPaths(WorkflowId: string): Observable<WorkflowPath[]> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetWorkflowPaths(WorkflowId));
  }

  getWorkflowPathItems(workflowPathId: string): Observable<WorkflowPathItem[]> {
    return this.httpExecutor.executeQuery<any>(this.dataUrl, new GetWorkflowPathItems(workflowPathId));
  }

  getDataSets(catalogId: string, typicalName: string, typicalId: string): Observable<DataSet[]> {
    const workContext = this.sessionService.getWorkContext();
    return this.httpExecutor.executeQuery(
      this.dataUrl,
      new GetDataSets(workContext.subProject.id, workContext.defaultSettings.system.id, catalogId, typicalName, typicalId)
    );
  }

  getSuggestedDataSets(
    catalogId: string,
    typicalName: string,
    typicalId: string,
    workflowId: string,
    pathItemId: string
  ): Observable<SuggestedDataSet[]> {
    const workContext = this.sessionService.getWorkContext();
    return this.httpExecutor.executeQuery<any>(
      this.dataUrl,
      new GetSuggestedDataSets(
        workContext.user.tenantId,
        workContext.project.id,
        workContext.subProject.id,
        workContext.defaultSettings.system.id,
        catalogId,
        typicalName,
        typicalId,
        workflowId,
        pathItemId
      )
    );
  }

  addDataSet(typicalId: string, suggestedDataSet: SuggestedDataSet): Observable<any> {
    const workContext = this.sessionService.getWorkContext();

    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new AddDataSet(
        workContext.subProject.id,
        workContext.defaultSettings.system.id,
        workContext.defaultSettings.catalog.id,
        typicalId,
        suggestedDataSet
      ),
      CommandMethod.POST
    );
  }

  getDataSetWithCombinations(dataSetId: string, mandatory: number): Observable<DataSet> {
    return this.httpExecutor.executeQuery(
      this.dataUrl,
      new GetDataSet(dataSetId, mandatory)
    );
  }

  getDataSetRows(datasetId: string): Observable<any> {
    return this.httpExecutor.executeQuery(this.dataUrl, new GetDataSetItem(datasetId)).pipe(
      map(x => {
        const dataSetWithRows = {};
        if (isNullOrUndefined(x)) {
          return dataSetWithRows;
        }
        dataSetWithRows['id'] = x['dataSetId'];
        let i = 1;
        dataSetWithRows['rows'] = (x['rows'] as Array<any>).map(dataSetRow => {
          return {
            rowId: dataSetRow.id,
            row: JSON.parse(dataSetRow.row),
            rowNo: (i++).toString()
          };
        });

        return dataSetWithRows;
      })
    );
  }

  deleteDataSetItem(id: string,
                    workflowId: string,
                    workflowPathId: string,
                    workflowPathItemId: string,
                    dataSetId: string,
                    deleteStatus: number,
                    index: number): Observable<void> {
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new DeleteDataSetItem(id, workflowId, workflowPathId, workflowPathItemId, dataSetId, deleteStatus, index),
      CommandMethod.DELETE);
    }

  updateDataSetItem(id: string, dataSetId: string, row: DataSetRow): Observable<void> {
    return this.httpExecutor.executeCommand(this.dataUrl, new UpdateDataSetItem(id, dataSetId, row), CommandMethod.PUT);
  }

  deleteDataSet(id: string): Observable<void> {
    return this.httpExecutor.executeCommand(this.dataUrl, new DeleteDataSet(id), CommandMethod.DELETE);
  }

  addManualDataSet(typicalId, typicalName, typicalType, dataset: DataSet): Observable<any> {
    const workContext = this.sessionService.getWorkContext();
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new AddManualDataSet(
        workContext.user.tenantId,
        workContext.project.id,
        workContext.subProject.id,
        workContext.defaultSettings.system.id,
        workContext.defaultSettings.catalog.id,
        typicalId,
        typicalName,
        typicalType,
        dataset
      ),
      CommandMethod.POST
    );
  }

  updateManualDataSet(workflowId: string,
                      workflowPathId: string,
                      workflowPathItemId: string,
                      typicalId,
                      typicalName,
                      typicalType,
                      dataset: DataSet,
                      updateStatus: number,
                      index: number): Observable<any> {
    const workContext = this.sessionService.getWorkContext();
    return this.httpExecutor.executeCommand(
      this.dataUrl,
      new UpdateManualDataSet(
        workContext.user.tenantId,
        workContext.project.id,
        workContext.subProject.id,
        workContext.defaultSettings.system.id,
        workContext.defaultSettings.catalog.id,
        workflowId,
        workflowPathId,
        workflowPathItemId,
        typicalId,
        typicalName,
        typicalType,
        dataset,
        updateStatus,
        index
      ),
      CommandMethod.PUT
    );
  }

  getTypicalAttributes(typicalId: string, mandatory: number): Observable<DataAttribute[]> {
    return this.httpExecutor
      .executeQuery<any[]>(this.dataUrl, new GetTypicalAttributes(typicalId, mandatory))
      .pipe(map(x => ((x && x['attributes']) as any[]) || []))
      .pipe(
        map(attrs => {
          return attrs.map(x => {
            return {
              name: x['name'],
              value: '',
              isRequired: x['isRequired']
            } as DataAttribute;
          });
        })
      );
  }

  GetTypicalByName(name: string, type: number): Observable<any> {
    return this.httpExecutor.executeQuery(this.dataUrl, new GetTypicalByName(name, type));
  }
}
