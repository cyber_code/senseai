import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { isNullOrUndefined } from 'util';
import { DataSetsPerPathItem, WorkflowPath } from '../models/workflowPath.model';

@Injectable()
export class DataFlowService {
  private workflowSelected = new Subject<string>();
  workflowSelected$ = this.workflowSelected.asObservable();

  private workflowRecorded = new Subject<string>();
  workflowRecorded$ = this.workflowRecorded.asObservable();

  private workflowCancelled = new Subject<string>();
  workflowCancelled$ = this.workflowCancelled.asObservable();

  private workflowPathSelected = new Subject<WorkflowPath>();
  workflowPathSelected$ = this.workflowPathSelected.asObservable();

  private projectSelected = new Subject<string>();
  projectSelected$ = this.projectSelected.asObservable();

  private subProjectSelected = new Subject<string>();
  subProjectSelected$ = this.subProjectSelected.asObservable();

  private pathItemsLoaded = new Subject<string>();
  pathItemsLoaded$ = this.pathItemsLoaded.asObservable();

  private workflowPath: WorkflowPath;

  constructor() {
    this.workflowPath = {} as WorkflowPath;
  }

  changeWorkflowId(workflowId: string) {
    this.workflowSelected.next(workflowId);
  }
  recordFinished(workflowId: string) {
    this.workflowRecorded.next(workflowId);
  }
  recordCanceled(workflowId: string) {
    this.workflowCancelled.next(workflowId);
  }
  changeWorkflowPath(workflowPath: WorkflowPath) {
    this.workflowPath = {
      pathId: workflowPath.pathId,
      dataSetsIds: []
    } as WorkflowPath;
    this.workflowPathSelected.next(workflowPath);
  }

  firePathItemsLoaded(pathId: string) {
    this.pathItemsLoaded.next(pathId);
  }

  setSelectedDataSetsPerPath(dataSetsPerPath: DataSetsPerPathItem) {
    // tslint:disable-next-line: max-line-length
    const existing = this.workflowPath.dataSetsIds.find(x => x.PathItemId === dataSetsPerPath.PathItemId && x.TSIndex === dataSetsPerPath.TSIndex);
    if (isNullOrUndefined(existing)) {
      this.workflowPath.dataSetsIds.push(dataSetsPerPath);
    } else {
      existing.Ids = dataSetsPerPath.Ids;
    }
  }

  getDataSetsPerPath() {
    return this.workflowPath;
  }
}
