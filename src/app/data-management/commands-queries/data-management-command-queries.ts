import { SuggestedDataSet, DataSetRow, DataSet, DataSetCombination } from './../models/path-dataset.model';
import { ApiAction } from 'src/app/core/services/command-queries/api-action';
import { DataSetsPerPathItem } from '../models/workflowPath.model';

export class GetWorkflowPaths extends ApiAction {
  WorkflowId: string;

  constructor(WorkflowId: string) {
    super('GetWorkflowPaths');
    this.WorkflowId = WorkflowId;
  }
}

export class GetWorkflowPathItems extends ApiAction {
  WorkflowPathId: string;

  constructor(WorkflowPathId: string) {
    super('GetWorkflowPathItems');
    this.WorkflowPathId = WorkflowPathId;
  }
}

export class GetDataSets extends ApiAction {
  SubProjectId: string;
  SystemId: string;
  CatalogId: string;
  TypicalName: string;
  TypicalId: string;

  constructor(subProjectId: string, systemId: string, catalogId: string, typicalName: string, typicalId: string) {
    super('GetDataSets');
    this.SubProjectId = subProjectId;
    this.SystemId = systemId;
    this.CatalogId = catalogId;
    this.TypicalName = typicalName;
    this.TypicalId = typicalId;
  }
}

export class GetSuggestedDataSets extends ApiAction {
  TenantId: string;
  ProjectId: string;
  SubProjectId: string;
  SystemId: string;
  CatalogId: string;
  TypicalName: string;
  TypicalId: string;
  WorkflowId: string;
  PathItemId: string;

  constructor(
    tenantId: string,
    projectId,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    typicalName: string,
    typicalid: string,
    workflowId: string,
    pathItemId: string
  ) {
    super('GetSuggestedDataSets');
    this.TenantId = tenantId;
    this.ProjectId = projectId;
    this.SubProjectId = subProjectId;
    this.SystemId = systemId;
    this.CatalogId = catalogId;
    this.TypicalName = typicalName;
    this.TypicalId = typicalid;
    this.WorkflowId = workflowId;
    this.PathItemId = pathItemId;
  }
}

export class GenerateTestCases extends ApiAction {
  TenantId: string;
  ProjectId: string;
  SubProjectId: string;
  SystemId: string;
  CatalogId: string;
  workflowId: string;
  workflowPathId: string;
  dataSetsIds: DataSetsPerPathItem[];
  skipStatus: boolean;

  constructor(
    tenantId: string,
    projectId,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    workflowId: string,
    workflowPathId: string,
    dataSets: DataSetsPerPathItem[],
    skipStatus: boolean
    ) {
    super('GenerateTestCases');
    this.TenantId = tenantId;
    this.ProjectId = projectId;
    this.SubProjectId = subProjectId;
    this.SystemId = systemId;
    this.CatalogId = catalogId;
    this.workflowId = workflowId;
    this.workflowPathId = workflowPathId;
    this.dataSetsIds = dataSets;
    this.skipStatus = skipStatus;
  }
}

export class SetDataSetForPathItem extends ApiAction {
  TenantId: string;
  ProjectId: string;
  SubProjectId: string;
  SystemId: string;
  CatalogId: string;
  workflowId: string;
  workflowPathId: string;
  workflowPathItemId: string;
  dataSetId: string;
  skipStatus: boolean;
  index: number;

  constructor(
    tenantId: string,
    projectId,
    subProjectId: string,
    systemId: string,
    catalogId: string,
    workflowId: string,
    workflowPathId: string,
    workflowPathItemId: string,
    dataSetId: string,
    skipStatus: boolean,
    index: number
    ) {
    super('SetDataSetForPathItem');
    this.TenantId = tenantId;
    this.ProjectId = projectId;
    this.SubProjectId = subProjectId;
    this.SystemId = systemId;
    this.CatalogId = catalogId;
    this.workflowId = workflowId;
    this.workflowPathId = workflowPathId;
    this.workflowPathItemId = workflowPathItemId;
    this.dataSetId = dataSetId;
    this.skipStatus = skipStatus;
    this.index = index;
  }
}

export class AddDataSet extends ApiAction {
  SubProjectId: string;
  SystemId: string;
  CatalogId: string;
  TypicalId: string;
  title: string;
  rows: DataSetRow[];

  constructor(subProjectId: string, systemId: string, catalogId: string, typicalId: string, dataSet: SuggestedDataSet) {
    super('AddDataSet');
    this.SubProjectId = subProjectId;
    this.SystemId = systemId;
    this.CatalogId = catalogId;
    this.TypicalId = typicalId;
    this.title = dataSet.title;
    this.rows = dataSet.rows;
  }
}

export class AddManualDataSet extends ApiAction {
  TenantId: string;
  ProjectId: string;
  SubProjectId: string;
  SystemId: string;
  CatalogId: string;
  TypicalId: string;
  TypicalName: string;
  TypicalType: string;
  title: string;
  coverage: number;
  combinations: DataSetCombination[];

  constructor(tenantId: string, projectId: string, subProjectId: string, systemId: string, catalogId: string,
      typicalId: string, typicalName: string, typicalType: string, dataSet: DataSet) {
    super('AddManualDataSet');
    this.TenantId = tenantId;
    this.ProjectId = projectId;
    this.SubProjectId = subProjectId;
    this.SystemId = systemId;
    this.CatalogId = catalogId;
    this.TypicalId = typicalId;
    this.TypicalName = typicalName;
    this.TypicalType = typicalType;
    this.title = dataSet.title;
    this.coverage = dataSet.coverage || 0;
    this.combinations = dataSet.combinations;
  }
}

export class UpdateManualDataSet extends ApiAction {
  id: string;
  TenantId: string;
  ProjectId: string;
  SubProjectId: string;
  SystemId: string;
  CatalogId: string;
  WorkflowId: string;
  WorkflowPathId: string;
  WorkflowPathItemId: string;
  TypicalId: string;
  TypicalName: string;
  TypicalType: string;
  title: string;
  coverage: number;
  combinations: DataSetCombination[];
  updateStatus: number;
  index: number;

  constructor(tenantId: string,
              projectId: string,
              subProjectId: string,
              systemId: string,
              catalogId: string,
              workflowId: string,
              workflowPathId: string,
              workflowPathItemId: string,
              typicalId: string,
              typicalName: string,
              typicalType: string,
              dataSet: DataSet,
              updateStatus: number,
              index: number) {
    super('UpdateManualDataSet');
    this.id = dataSet.id;
    this.TenantId = tenantId;
    this.ProjectId = projectId;
    this.SubProjectId = subProjectId;
    this.SystemId = systemId;
    this.CatalogId = catalogId;
    this.WorkflowId = workflowId;
    this.WorkflowPathId = workflowPathId;
    this.WorkflowPathItemId = workflowPathItemId;
    this.TypicalId = typicalId;
    this.TypicalType = typicalType;
    this.TypicalName = typicalName;
    this.title = dataSet.title;
    this.coverage = dataSet.coverage || 0;
    this.combinations = dataSet.combinations;
    this.updateStatus = updateStatus;
    this.index = index;
  }
}

export class DeleteDataSetItem extends ApiAction {
  Id: string;
  WorkflowId: string;
  WorkflowPathId: string;
  WorkflowPathItemId: string;
  DataSetId: string;
  DeleteStatus: number;
  Index: number;

  constructor(id: string,
              workflowid: string,
              workflowPathId: string,
              workflowPathItemId: string,
              dataSetId: string,
              deleteStatus: number,
              index: number) {
    super('DeleteDataSetItem');
          this.Id = id;
          this.WorkflowId = workflowid;
          this.WorkflowPathId = workflowPathId;
          this.WorkflowPathItemId = workflowPathItemId;
          this.DataSetId = dataSetId;
          this.DeleteStatus = deleteStatus;
          this.Index = index;
  }
}

export class DeleteDataSet extends ApiAction {
  Id: string;

  constructor(id: string) {
    super('DeleteDataSet');
    this.Id = id;
  }
}

export class UpdateDataSetItem extends ApiAction {
  id: string;
  dataSetId: string;
  row: DataSetRow;

  constructor(id: string, dataSetId: string, row: DataSetRow) {
    super('UpdateDataSetItem');
    this.id = id;
    this.dataSetId = dataSetId;
    this.row = row;
  }
}

export class GetDataSetItem extends ApiAction {
  DataSetId: string;

  constructor(dataSetId: string) {
    super('GetDataSetItem');
    this.DataSetId = dataSetId;
  }
}

export class GetDataSet extends ApiAction {
  Id: string;
  Mandatory: number;

  constructor(dataSetId: string, mandatory: number) {
    super('GetDataSet');
    this.Id = dataSetId;
    this.Mandatory = mandatory;
  }
}

export class GetTypicalAttributes extends ApiAction {
  TypicalId: string;
  Mandatory: number;
  constructor(typicalId: string, mandatory: number) {
    super('GetTypicalAttributes');
    this.TypicalId = typicalId;
    this.Mandatory = mandatory;
  }
}
export class GetTypicalByName extends ApiAction {
  name: string;
  type: number;

  constructor(name: string, type: number) {
    super('GetTypicalByName');
    this.name = name;
    this.type = type;
  }
}
