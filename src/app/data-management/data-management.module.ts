import {InputsModule} from '@progress/kendo-angular-inputs';
import {EditService} from './components/edit-form-dataset/edit.service';
import {DataManagementRoutingModule} from './data-management-routing.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreModule} from '../core/core.module';
import {WorkflowPathsComponent} from './components/workflow-paths/workflow-paths.component';
import {PathItemsComponent} from './components/path-items/path-items.component';
import {LayoutModule} from '@progress/kendo-angular-layout';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TreeViewModule} from '@progress/kendo-angular-treeview';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {WindowModule, DialogModule} from '@progress/kendo-angular-dialog';
import {ButtonsModule} from '@progress/kendo-angular-buttons';
import {PopupModule} from '@progress/kendo-angular-popup';
import {GridModule} from '@progress/kendo-angular-grid';
import {DropDownsModule} from '@progress/kendo-angular-dropdowns';
import {MenuModule} from '@progress/kendo-angular-menu';
import {DataManagementComponent} from './components/data-management/data-management.component';
import {DataFlowService} from './services/data-flow.service';
import {DataManagementService} from './services/data-management.service';
import {EditFormDatasetComponent} from './components/edit-form-dataset/edit-form-dataset.component';
import {ManualDatasetComponent} from './components/manual-dataset/manual-dataset.component';
import {SuggestedDatasetsComponent} from './components/suggested-datasets/suggested-datasets.component';
import {DatasetRowsComponent} from './components/dataset-rows/dataset-rows.component';
import {ViewDatasetRowComponent} from './components/view-dataset-row/view-dataset-row.component';

@NgModule({
  imports: [
    CommonModule,
    DataManagementRoutingModule,
    CoreModule,
    CommonModule,
    LayoutModule,
    BrowserModule,
    BrowserAnimationsModule,
    TreeViewModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    WindowModule,
    ButtonsModule,
    DialogModule,
    PopupModule,
    MenuModule,
    DropDownsModule,
    GridModule,
    InputsModule
  ],
  providers: [DataFlowService, DataManagementService, EditService],
  declarations: [
    DataManagementComponent,
    WorkflowPathsComponent,
    PathItemsComponent,
    EditFormDatasetComponent,
    ManualDatasetComponent,
    SuggestedDatasetsComponent,
    DatasetRowsComponent,
    ViewDatasetRowComponent
  ],
  exports: [DataManagementComponent]
})
export class DataManagementModule {
}
