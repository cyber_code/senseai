// /**
//  * Main function for the Webchat.
//  * It just creates the element that the bot is gonna be attached, once it gets clicked (lazy-loading).
//  */
// (function () {
//   "use strict";
//
//   var DEFAULT_ACCENT = '#008694';
//   var DEFAULT_SUBTLE = '#767676'; // With contrast 4.5:1 to white
//   var PADDING_REGULAR = 10;
//
//   /**
//    * Bot Specific Props
//    * @type {{tokenURL: string, headerText: string, avatarImageURL: string, bot: {}, user: {}}}
//    */
//   var botPreferences = {
//     // tokenURL: '',
//     secret: 'XJiDnF532fQ.PBbs87vFdkTyxsCWRpJFUm0t1OK177iF4X9mEEccepg',
//     bot: { id: 'aa-bot-staging' }, // This is used as the bot name
//     user: { name: 'You', id: '' }, // User.id should be defined from the session or a cookie
//     greetingActivityMessages: [
//       {
//         type: 'message',
//         text: "Hi there, I'm AA-bot. Nice to meet you!"
//       },
//       {
//         type: 'message',
//         text: "Are you ready to start designing your new product?",
//         suggestedActions: {
//           actions: [
//             {
//               "type": "imBack",
//               "title": "Start",
//               "value": "Start"
//             }
//           ]
//         }
//       }
//     ],
//     styleSetOptions: {
//       // Color and paddings
//       accent: DEFAULT_ACCENT,
//       backgroundColor: '#e2e2e2',
//       paddingRegular: PADDING_REGULAR,
//       paddingWide: 20,
//       subtle: DEFAULT_SUBTLE,
//
//       // fonts
//       fontSizeSmall: '80%',
//
//       // Avatar
//       avatarSize: 40,
//       botAvatarImage: 'https://res.cloudinary.com/helvia-io/image/upload/v1554110070/Validata%20Bot/V-192x192.png',
//       botAvatarInitials: 'KS',
//       userAvatarImage: 'https://res.cloudinary.com/helvia-io/image/upload/v1558093703/images_h24dcz.png',
//       userAvatarInitials: 'YOU',
//
//       // Bubble
//       bubbleBackground: '#eceff1',
//       bubbleBorder: 'solid 1px #E6E6E6',
//       bubbleBorderRadius: '5px',
//       bubbleFromUserBackground: '#008694',
//       bubbleFromUserBorder: 'solid 1px #E6E6E6',
//       bubbleFromUserBorderRadius: '5px',
//       bubbleFromUserTextColor: 'white',
//       bubbleImageHeight: 240,
//       bubbleMaxWidth: 480, // screen width = 600px
//       bubbleMinHeight: 40,
//       bubbleMinWidth: 250, // min screen width = 300px, Edge requires 372px (https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/13621468/)
//       bubbleTextColor: 'Black',
//
//       // Root
//       rootHeight: '100%',
//       rootWidth: '100%',
//
//       // Send box
//       hideSendBox: false,
//       hideUploadButton: true,
//       microphoneButtonColorOnDictate: '#F33',
//       sendBoxBackground: 'White',
//       sendBoxButtonColor: '#767676',
//       sendBoxButtonColorOnDisabled: '#CCC',
//       sendBoxButtonColorOnFocus: '#333',
//       sendBoxButtonColorOnHover: '#333',
//       sendBoxHeight: 40,
//       sendBoxTextColor: 'Black',
//
//       // Visually show spoken text
//       showSpokenText: true,
//
//       // Suggested actions
//       suggestedActionBackground: 'White',
//       suggestedActionBorder: 'solid 1px #008694',
//       suggestedActionBorderRadius: '5px',
//       suggestedActionTextColor: DEFAULT_ACCENT,
//       suggestedActionDisabledBackground: 'White',
//       suggestedActionDisabledBorder: 'solid 2px #E6E6E6',
//       suggestedActionDisabledTextColor: DEFAULT_SUBTLE,
//       suggestedActionHeight: 40,
//
//       // Timestamp
//       timestampColor: DEFAULT_SUBTLE,
//
//       // Transcript overlay buttons (e.g. carousel and scroll to bottom)
//       transcriptOverlayButtonBackground: 'rgba(0, 0, 0, .6)',
//       transcriptOverlayButtonBackgroundOnFocus: 'rgba(0, 0, 0, .8)',
//       transcriptOverlayButtonBackgroundOnHover: 'rgba(0, 0, 0, .8)',
//       transcriptOverlayButtonColor: 'White',
//       transcriptOverlayButtonColorOnFocus: 'White',
//       transcriptOverlayButtonColorOnHover: 'White',
//
//       // Video
//       videoHeight: 270, // based on bubbleMaxWidth, 480 / 16 * 9 = 270
//
//       // Connectivity UI
//       connectivityIconPadding: PADDING_REGULAR * 1.2,
//       connectivityMarginLeftRight: PADDING_REGULAR * 1.4,
//       connectivityMarginTopBottom: PADDING_REGULAR * 0.8,
//       connectivityTextSize: 12,
//       failedConnectivity: '#C50F1F',
//       slowConnectivity: '#EAA300',
//       slowConnectivityText: '#5E5E5E',
//
//       textContent: {
//         fontFamily: "'Roboto', 'Arial', sans-serif",
//         fontWeight: 'bold'
//       }
//     },
//     retrieveUserIdFromLogin: function () { // Let's keep this asynchronous
//       return new Promise(function(resolve){
//         var userId;
//         if (document.getElementById('BotSessionId')) {
//           userId = document.getElementById('BotSessionId').value;
//         }
//         resolve(userId);
//       });
//     }
//   };
//
//   if (detectIE()) {
//     dynamicallyLoadScript("https://cdn.polyfill.io/v3/polyfill.min.js");
//     dynamicallyLoadScript("https://cdnjs.cloudflare.com/ajax/libs/fetch/2.0.4/fetch.min.js");
//   }
//   dynamicallyLoadScript('https://cdn.jsdelivr.net/npm/node-uuid@1.4.8/uuid.min.js');
//   dynamicallyLoadScript('https://cdn.botframework.com/botframework-webchat/4.4.1/webchat-es5.js');
//
//   /**
//    * Create the HTML Elements for the Bot
//    * @type {HTMLElement}
//    */
//   var div = document.createElement("div");
//   document.getElementsByTagName('body')[0].appendChild(div);
//   div.outerHTML = "<div id='botDiv' class='chat-container is-hide'></span><div id='botChat' role='main'><span>LOADING</span></div></div>" +
//     "<div><a class='btn-chat btn-sonar' style='display: none'><i class='chat-icon material-icons'>chat_bubble_outline</i></a></div>";
//
//   var botHasBeenClickedAtLeastOnce = false;
//   var directLineConnection;
//
//   /**
//    * Event handler for the bot widget.
//    */
//   var chat = document.querySelector('.chat-container');
//   var icon = document.querySelector('.chat-icon');
//   var button = document.querySelector('.btn-chat');
//   button.addEventListener("click",function(e){
//     toggleWC(chat, icon);
//     if (!botHasBeenClickedAtLeastOnce) {
//       setUpWebchat(botPreferences, 5).then(function (dlc) {
//         directLineConnection = dlc;
//         botHasBeenClickedAtLeastOnce = true;
//         // directLineConnection.postActivity({
//         //     type: 'event',
//         //     name: 'newUserClickedWebchat',
//         //     from: botPreferences.user,
//         //     value: {
//         //         fullURL: window.location.href,
//         //         domainName: window.location.origin
//         //     }
//         // }).subscribe(function (id) {});
//       }).catch(function (reason) {
//         console.log(reason);
//         botHasBeenClickedAtLeastOnce = false;
//         toggleWC(chat, icon);
//       });
//     } else {
//       directLineConnection.postActivity({
//         type: 'event',
//         name: 'gettingMetadata',
//         from: botPreferences.user,
//         value: {
//           fullURL: window.location.href,
//           domainName: window.location.origin
//         }
//       }).subscribe(function (id) {});
//     }
//   },false);
//
//   /**
//    * detect IE
//    * returns version of IE or false, if browser is not Internet Explorer
//    */
//   function detectIE() {
//     var ua = window.navigator.userAgent;
//
//     var msie = ua.indexOf('MSIE ');
//     if (msie > 0) {
//       // IE 10 or older => return version number
//       return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
//     }
//
//     var trident = ua.indexOf('Trident/');
//     if (trident > 0) {
//       // IE 11 => return version number
//       var rv = ua.indexOf('rv:');
//       return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
//     }
//
//     var edge = ua.indexOf('Edge/');
//     if (edge > 0) {
//       // Edge (IE 12+) => return version number
//       return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
//     }
//
//     // other browser
//     return false;
//   }
//
//   /**
//    * Loads to the Webpage the preferred script
//    * @param url
//    */
//   function dynamicallyLoadScript(url) {
//     var script = document.createElement("script");  // create a script DOM node
//     script.src = url;  // set its src to the provided URL
//     script.type= "text/javascript";
//     document.getElementsByTagName( "head" )[0].appendChild(script);  // add it to the end of the head section of the page (could change 'head' to 'body' to add it to the end of the body section instead)
//   }
//
//   /**
//    * Toggles (Maximize/Minimize) the Webchat
//    * @param chat
//    * @param icon
//    */
//   function toggleWC(chat, icon) {
//     icon.classList.toggle('is-active');
//     chat.classList.toggle('is-visible');
//     chat.classList.toggle('is-hide');
//     if (icon.innerHTML === 'chat_bubble_outline') {
//       icon.innerHTML = 'close';
//     } else if (icon.innerHTML === 'close') {
//       icon.innerHTML = 'chat_bubble_outline';
//     }
//   }
//
//   /**
//    * Performs the loading-rendering of the Webchat into the webpage
//    * @returns {*}
//    */
//   function setUpWebchat(botPreferences, retries) {
//
//     return getUserId(botPreferences)
//       .then(function (userId) {
//         botPreferences.user.id = userId;
//         return setUpDirectLineConnection(botPreferences);
//       })
//       .then(function (dlconnection) {
//         if (isNewConversation()) {
//           sendGreetingMessage(dlconnection, botPreferences);
//         }
//
//         setCookieForWebchat('token', dlconnection.token, 365);
//         setCookieForWebchat('conversationId', dlconnection.conversationId, 365);
//         setCookieForWebchat('watermark', dlconnection.watermark, 365);
//         setCookieForWebchat('streamUrl', dlconnection.streamUrl, 365);
//
//         return dlconnection;
//       })
//       .catch(function (error) {
//         // console.log("Function setUpDirectLineConnection(...) failed (remaining retries: " + retries + "): " + error);
//         if (retries > 0) {
//           if (getCookieForWebchat('token')) deleteCookieForWebchat('token');
//           if (getCookieForWebchat('conversationId')) deleteCookieForWebchat('conversationId');
//           if (getCookieForWebchat('watermark')) deleteCookieForWebchat('watermark');
//           if (getCookieForWebchat('streamUrl')) deleteCookieForWebchat('streamUrl');
//
//           return setUpWebchat(botPreferences, retries-1)
//             .then(function (value) {
//               return value;
//             })
//             .catch(function (reason) {
//               throw reason;
//             });
//         } else {
//           throw error;
//         }
//       });
//   }
//
//   /**
//    * Checks where to get the userID from
//    *
//    * @param botPreferences
//    * @returns {*}
//    */
//   function getUserId(botPreferences) {
//     return botPreferences.retrieveUserIdFromLogin()
//       .then(function (response) {
//         var userId = '';
//         if (response) {
//           userId = response;
//         } else {
//           if (isNewUser()) {
//             userId = uuid.v4();
//           } else {
//             userId = getCookieForWebchat('wcUserId');
//             deleteCookieForWebchat('wcUserId');
//           }
//           setCookieForWebchat('wcUserId', userId, 365); // Keep user's ID for a year
//         }
//         return userId;
//       })
//       .catch(function (err) {
//         throw err;
//       });
//   }
//
//   /**
//    * Sends recursively the Quick Greetings
//    *
//    * @param dlconnection
//    * @param botPreferences
//    * @returns {Promise}
//    */
//   function sendGreetingMessage(dlconnection, botPreferences) {
//     return new Promise(function (resolve, reject) {
//       if (botPreferences.greetingActivityMessages && botPreferences.greetingActivityMessages.length > 0) {
//         var activityMsg = botPreferences.greetingActivityMessages.shift();
//         activityMsg.from = botPreferences.bot;
//         dlconnection.postActivity(activityMsg).subscribe(function () {
//           sendGreetingMessage(dlconnection, botPreferences)
//             .then(function (value) { resolve(); })
//             .catch(function (reason) { reject(reason); });
//         });
//       } else {
//         resolve();
//       }
//     });
//
//   }
//
//   /**
//    * Sets up DirectLine connection for the Webchat
//    * @param botPreferences
//    */
//   function setUpDirectLineConnection(botPreferences) {
//     return new Promise(function (resolve, reject) {
//       var dlconnectionPromise,
//         directLineOptions = { webSocket: false },
//         isNewConvo = isNewConversation();
//
//       if (!window.WebChat) return reject(new Error("Webchat Library has not been loaded, yet"));
//       // You can modify the style set by providing a limited set of style options
//       botPreferences.styleSet = window.WebChat.createStyleSet(botPreferences.styleSetOptions);
//
//       if (isNewConvo) {
//         dlconnectionPromise = createNewDirectLineConnection(directLineOptions, botPreferences, 5);
//       } else {
//         directLineOptions.token = getCookieForWebchat('token');
//         directLineOptions.conversationId = getCookieForWebchat('conversationId');
//         // directLineOptions.streamUrl = getCookieForWebchat('streamUrl');
//         directLineOptions.watermark = getCookieForWebchat('watermark');
//         dlconnectionPromise = loadOldDirectLineConnection(directLineOptions);
//       }
//
//       dlconnectionPromise
//         .then(function(dlconnection) {
//
//           var store = window.WebChat.createStore({}, function (arg) {
//             var dispatch = arg.dispatch;
//             return function (next) {
//               return function (action) {
//                 // The following code is for convenience only. It not required for production.
//                 switch (action.type) {
//                   case 'DIRECT_LINE/CONNECT_FULFILLED':
//                     resolve(dlconnection);
//                     break;
//                   case 'DIRECT_LINE/CONNECT_REJECTED':
//                     reject(new Error("DirectLine failed to connect."));
//                     break;
//                 }
//                 return next(action);
//               };
//             };
//           });
//
//           /********************************
//            * We are using Botchat v4.4.1 *
//            ********************************/
//           var renderWebChatOptions = {
//             directLine: dlconnection,
//             styleSet: botPreferences.styleSet,
//             username: botPreferences.user.name,
//             disabled: false,
//             store: store,
//             webSpeechPonyfillFactory: window.WebChat.createBrowserWebSpeechPonyfillFactory()
//           };
//           if (botPreferences.user.id) renderWebChatOptions.userID = botPreferences.user.id;
//           window.WebChat.renderWebChat(renderWebChatOptions, document.getElementById('botChat'));
//
//           document.querySelector('#botChat > *').focus();
//
//           return dlconnection;
//         })
//         .catch(function(error) {
//           // console.log(error);
//           reject(error);
//         });
//     });
//   }
//
//   /**
//    * Creates a new DirectLine connection for the user and the bot
//    * It requests a token from the backoffice of the bot and then it uses it to initialize the conversation
//    *
//    * @returns {Promise<DirectLine | never>}
//    */
//   function createNewDirectLineConnection(directLineOptions, botPreferences, retries) {
//     if (botPreferences.tokenURL) {
//       return window.fetch(botPreferences.tokenURL, { method: 'POST' })
//         .then(function (res) {
//           return res.json();
//         })
//         .then(function (json) {
//           if (!json.token) throw new Error("There is no token on the response of HBF");
//           directLineOptions.token = json.token;
//           return new window.WebChat.createDirectLine(directLineOptions);
//         })
//         .catch(function (error) {
//           // Here most probably will be reported an error from HBF
//           // console.error("Error while creating new DL Connection (remaining retries: " + retries + "): " + error);
//           if (retries > 0) {
//             return delay(500)
//               .then(function () {
//                 return createNewDirectLineConnection(botPreferences, retries-1);
//               })
//               .then(function (value) {
//                 return value;
//               })
//               .catch(function (reason) {
//                 throw reason;
//               });
//           } else {
//             throw error;
//           }
//         });
//     } else if (botPreferences.secret) {
//       directLineOptions.secret = botPreferences.secret;
//       return Promise.resolve(new window.WebChat.createDirectLine(directLineOptions));
//     } else {
//       throw new Error('Neither token nor secret were passed for the bot');
//     }
//   }
//
//   function delay(ms) {
//     return new Promise(function (resolve) {
//       setTimeout(resolve, ms);
//     });
//   }
//
//   /**
//    * Loads an old DirectLine Connection for the user and the bot
//    * It uses the data stored in the cookies and reconnects, fetching the history too.
//    */
//   function loadOldDirectLineConnection(directLineOptions) {
//     return new Promise(function (resolve, reject) {
//       var hasToken = !!directLineOptions.token;
//       var hasConversationId = !!directLineOptions.conversationId;
//
//       if (!hasToken) {
//         reject(new Error("Trying to load old conversation without token"));
//       } else if (!hasConversationId) {
//         reject(new Error("Trying to load old conversation without conversationId"));
//       } else {
//         resolve(new window.WebChat.createDirectLine(directLineOptions));
//       }
//     });
//   }
//
//   /**
//    * Checks if there is a conversation stored in the cookies
//    *
//    * @returns {boolean}
//    */
//   function isNewConversation() {
//     var convId = getCookieForWebchat('conversationId');
//     return !convId;
//   }
//
//   /**
//    * Checks if there is a User
//    *
//    * @returns {boolean}
//    */
//   function isNewUser() {
//     var wcUserId = getCookieForWebchat('wcUserId');
//     return !wcUserId;
//   }
//
//   /**
//    * Sets a cookie
//    *
//    * @param cname
//    * @param cvalue
//    * @param exdays
//    */
//   function setCookieForWebchat(cname, cvalue, exdays) {
//     var cookie = cname + "=" + cvalue + ";";
//     if (exdays) {
//       var d = new Date();
//       d.setTime(d.getTime() + (exdays*24*60*60*1000));
//       var expires = "expires="+ d.toUTCString() + ";";
//       cookie += expires
//     }
//     if (window.location.host.indexOf('localhost') === -1) {
//       cookie += "domain=" + window.location.host + ";";
//     }
//     cookie += "path=" + window.location.pathname;
//     document.cookie = cookie;
//   }
//
//   /**
//    * Deletes a cookie
//    *
//    * @param cname
//    */
//   function deleteCookieForWebchat(cname) {
//     var cookie = cname + "=;";
//     var d = new Date();
//     d.setTime(1);
//     var expires = "expires="+ d.toUTCString() + ";";
//     cookie += expires;
//     if (window.location.host.indexOf('localhost') === -1) {
//       cookie += "domain=" + window.location.host + ";";
//     }
//     cookie += "path=" + window.location.pathname;
//     document.cookie = cookie;
//   }
//
//   /**
//    * Fetches a cookie
//    *
//    * @param cname
//    * @returns {string}
//    */
//   function getCookieForWebchat(cname) {
//     var name = cname + "=";
//     var decodedCookie = decodeURIComponent(document.cookie);
//     var ca = decodedCookie.split(';');
//     for(var i = 0; i <ca.length; i++) {
//       var c = ca[i];
//       while (c.charAt(0) == ' ') {
//         c = c.substring(1);
//       }
//       if (c.indexOf(name) == 0) {
//         return c.substring(name.length, c.length);
//       }
//     }
//     return "";
//   }
//
// }());
/**
 * Main function for the Webchat.
 * It just creates the element that the bot is gonna be attached, once it gets clicked (lazy-loading).
 */
(function() {
    "use strict";

    var DEFAULT_ACCENT = '#008694';
    var DEFAULT_SUBTLE = '#767676'; // With contrast 4.5:1 to white
    var PADDING_REGULAR = 10;

    /**
     * Bot Specific Props
     * @type {{tokenURL: string, headerText: string, avatarImageURL: string, bot: {}, user: {}}}
     */
    var botPreferences = {
        // tokenURL: '',
        secret: 'XJiDnF532fQ.PBbs87vFdkTyxsCWRpJFUm0t1OK177iF4X9mEEccepg',
        bot: { id: 'aa-bot-staging' }, // This is used as the bot name
        user: { name: 'You', id: '' }, // User.id should be defined from the session or a cookie
        greetingActivityMessages: [{
                type: 'message',
                text: "Hi there, I'm AA-bot. Nice to meet you!"
            },
            {
                type: 'message',
                text: "Are you ready to start designing your new product?",
                suggestedActions: {
                    actions: [{
                        "type": "imBack",
                        "title": "Start",
                        "value": "Start"
                    }]
                }
            }
        ],
        styleSetOptions: {
            // Color and paddings
            accent: DEFAULT_ACCENT,
            backgroundColor: '#e2e2e2',
            paddingRegular: PADDING_REGULAR,
            paddingWide: 20,
            subtle: DEFAULT_SUBTLE,

            // fonts
            fontSizeSmall: '80%',

            // Avatar
            avatarSize: 40,
            botAvatarImage: 'https://res.cloudinary.com/helvia-io/image/upload/v1554110070/Validata%20Bot/V-192x192.png',
            botAvatarInitials: 'KS',
            userAvatarImage: 'https://res.cloudinary.com/helvia-io/image/upload/v1558093703/images_h24dcz.png',
            userAvatarInitials: 'YOU',

            // Bubble
            bubbleBackground: '#eceff1',
            bubbleBorder: 'solid 1px #E6E6E6',
            bubbleBorderRadius: '5px',
            bubbleFromUserBackground: '#008694',
            bubbleFromUserBorder: 'solid 1px #E6E6E6',
            bubbleFromUserBorderRadius: '5px',
            bubbleFromUserTextColor: 'white',
            bubbleImageHeight: 240,
            bubbleMaxWidth: 480, // screen width = 600px
            bubbleMinHeight: 40,
            bubbleMinWidth: 250, // min screen width = 300px, Edge requires 372px (https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/13621468/)
            bubbleTextColor: 'Black',

            // Root
            rootHeight: '100%',
            rootWidth: '100%',

            // Send box
            hideSendBox: false,
            hideUploadButton: true,
            microphoneButtonColorOnDictate: '#F33',
            sendBoxBackground: 'White',
            sendBoxButtonColor: '#767676',
            sendBoxButtonColorOnDisabled: '#CCC',
            sendBoxButtonColorOnFocus: '#333',
            sendBoxButtonColorOnHover: '#333',
            sendBoxHeight: 40,
            sendBoxTextColor: 'Black',

            // Visually show spoken text
            showSpokenText: true,

            // Suggested actions
            suggestedActionBackground: 'White',
            suggestedActionBorder: 'solid 1px #008694',
            suggestedActionBorderRadius: '5px',
            suggestedActionTextColor: DEFAULT_ACCENT,
            suggestedActionDisabledBackground: 'White',
            suggestedActionDisabledBorder: 'solid 2px #E6E6E6',
            suggestedActionDisabledTextColor: DEFAULT_SUBTLE,
            suggestedActionHeight: 40,

            // Timestamp
            timestampColor: DEFAULT_SUBTLE,

            // Transcript overlay buttons (e.g. carousel and scroll to bottom)
            transcriptOverlayButtonBackground: 'rgba(0, 0, 0, .6)',
            transcriptOverlayButtonBackgroundOnFocus: 'rgba(0, 0, 0, .8)',
            transcriptOverlayButtonBackgroundOnHover: 'rgba(0, 0, 0, .8)',
            transcriptOverlayButtonColor: 'White',
            transcriptOverlayButtonColorOnFocus: 'White',
            transcriptOverlayButtonColorOnHover: 'White',

            // Video
            videoHeight: 270, // based on bubbleMaxWidth, 480 / 16 * 9 = 270

            // Connectivity UI
            connectivityIconPadding: PADDING_REGULAR * 1.2,
            connectivityMarginLeftRight: PADDING_REGULAR * 1.4,
            connectivityMarginTopBottom: PADDING_REGULAR * 0.8,
            connectivityTextSize: 12,
            failedConnectivity: '#C50F1F',
            slowConnectivity: '#EAA300',
            slowConnectivityText: '#5E5E5E',

            textContent: {
                fontFamily: "'Roboto', 'Arial', sans-serif",
                fontWeight: 'bold'
            }
        },
        retrieveUserIdFromLogin: function() { // Let's keep this asynchronous
            return new Promise(function(resolve) {
                var userId;
                if (document.getElementById('BotSessionId')) {
                    userId = document.getElementById('BotSessionId').value;
                }
                resolve(userId);
            });
        }
    };

    if (detectIE()) {
        dynamicallyLoadScript("https://cdn.polyfill.io/v3/polyfill.min.js");
        dynamicallyLoadScript("https://cdnjs.cloudflare.com/ajax/libs/fetch/2.0.4/fetch.min.js");
    }
    dynamicallyLoadScript('https://cdn.jsdelivr.net/npm/node-uuid@1.4.8/uuid.min.js');
    dynamicallyLoadScript('https://cdn.botframework.com/botframework-webchat/4.4.1/webchat-es5.js');

    /**
     * Create the HTML Elements for the Bot
     * @type {HTMLElement}
     */
    var div = document.createElement("div");
    document.getElementsByTagName('body')[0].appendChild(div);
    div.outerHTML = "<div id='botDiv' class='chat-container is-hide'></span><div id='botChat' role='main'><span>LOADING</span></div></div>" +
        "<div><a class='btn-chat btn-sonar' style='display: none'><i class='chat-icon material-icons'>chat_bubble_outline</i></a></div>";

    var botHasBeenClickedAtLeastOnce = false;
    var directLineConnection;

    /**
     * Event handler for the bot widget.
     */
    var chat = document.querySelector('.chat-container');
    var icon = document.querySelector('.chat-icon');
    var button = document.querySelector('.btn-chat');
    button.addEventListener("click", function(e) {
        toggleWC(chat, icon);
        if (!botHasBeenClickedAtLeastOnce) {
            setUpWebchat(botPreferences, 5).then(function(dlc) {
                directLineConnection = dlc;
                botHasBeenClickedAtLeastOnce = true;
                // directLineConnection.postActivity({
                //     type: 'event',
                //     name: 'newUserClickedWebchat',
                //     from: botPreferences.user,
                //     value: {
                //         fullURL: window.location.href,
                //         domainName: window.location.origin
                //     }
                // }).subscribe(function (id) {});
            }).catch(function(reason) {
                console.log(reason);
                botHasBeenClickedAtLeastOnce = false;
                toggleWC(chat, icon);
            });
        } else {
            directLineConnection.postActivity({
                type: 'event',
                name: 'gettingMetadata',
                from: botPreferences.user,
                value: {
                    fullURL: window.location.href,
                    domainName: window.location.origin
                }
            }).subscribe(function(id) {});
        }
    }, false);

    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */
    function detectIE() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    }

    /**
     * Loads to the Webpage the preferred script
     * @param url
     */
    function dynamicallyLoadScript(url) {
        var script = document.createElement("script"); // create a script DOM node
        script.src = url; // set its src to the provided URL
        script.type = "text/javascript";
        document.getElementsByTagName("head")[0].appendChild(script); // add it to the end of the head section of the page (could change 'head' to 'body' to add it to the end of the body section instead)
    }

    /**
     * Toggles (Maximize/Minimize) the Webchat
     * @param chat
     * @param icon
     */
    function toggleWC(chat, icon) {
        icon.classList.toggle('is-active');
        chat.classList.toggle('is-visible');
        chat.classList.toggle('is-hide');
        if (icon.innerHTML === 'chat_bubble_outline') {
            icon.innerHTML = 'close';
        } else if (icon.innerHTML === 'close') {
            icon.innerHTML = 'chat_bubble_outline';
        }
    }

    /**
     * Performs the loading-rendering of the Webchat into the webpage
     * @returns {*}
     */
    function setUpWebchat(botPreferences, retries) {

        return getUserId(botPreferences)
            .then(function(userId) {
                botPreferences.user.id = userId;
                return setUpDirectLineConnection(botPreferences);
            })
            .then(function(dlconnection) {
                if (isNewConversation()) {
                    sendGreetingMessage(dlconnection, botPreferences);
                }

                setCookieForWebchat('token', dlconnection.token, 365);
                setCookieForWebchat('conversationId', dlconnection.conversationId, 365);
                setCookieForWebchat('watermark', dlconnection.watermark, 365);
                setCookieForWebchat('streamUrl', dlconnection.streamUrl, 365);

                return dlconnection;
            })
            .catch(function(error) {
                // console.log("Function setUpDirectLineConnection(...) failed (remaining retries: " + retries + "): " + error);
                if (retries > 0) {
                    if (getCookieForWebchat('token')) deleteCookieForWebchat('token');
                    if (getCookieForWebchat('conversationId')) deleteCookieForWebchat('conversationId');
                    if (getCookieForWebchat('watermark')) deleteCookieForWebchat('watermark');
                    if (getCookieForWebchat('streamUrl')) deleteCookieForWebchat('streamUrl');

                    return setUpWebchat(botPreferences, retries - 1)
                        .then(function(value) {
                            return value;
                        })
                        .catch(function(reason) {
                            throw reason;
                        });
                } else {
                    throw error;
                }
            });
    }

    /**
     * Checks where to get the userID from
     *
     * @param botPreferences
     * @returns {*}
     */
    function getUserId(botPreferences) {
        return botPreferences.retrieveUserIdFromLogin()
            .then(function(response) {
                var userId = '';
                if (response) {
                    userId = response;
                } else {
                    if (isNewUser()) {
                        userId = uuid.v4();
                    } else {
                        userId = getCookieForWebchat('wcUserId');
                        deleteCookieForWebchat('wcUserId');
                    }
                    setCookieForWebchat('wcUserId', userId, 365); // Keep user's ID for a year
                }
                return userId;
            })
            .catch(function(err) {
                throw err;
            });
    }

    /**
     * Sends recursively the Quick Greetings
     *
     * @param dlconnection
     * @param botPreferences
     * @returns {Promise}
     */
    function sendGreetingMessage(dlconnection, botPreferences) {
        return new Promise(function(resolve, reject) {
            if (botPreferences.greetingActivityMessages && botPreferences.greetingActivityMessages.length > 0) {
                var activityMsg = botPreferences.greetingActivityMessages.shift();
                activityMsg.from = botPreferences.bot;
                dlconnection.postActivity(activityMsg).subscribe(function() {
                    sendGreetingMessage(dlconnection, botPreferences)
                        .then(function(value) {
                            resolve();
                        })
                        .catch(function(reason) {
                            reject(reason);
                        });
                });
            } else {
                resolve();
            }
        });

    }

    /**
     * Sets up DirectLine connection for the Webchat
     * @param botPreferences
     */
    function setUpDirectLineConnection(botPreferences) {
        return new Promise(function(resolve, reject) {
            var dlconnectionPromise,
                directLineOptions = { webSocket: false },
                isNewConvo = isNewConversation();

            if (!window.WebChat) return reject(new Error("Webchat Library has not been loaded, yet"));
            // You can modify the style set by providing a limited set of style options
            botPreferences.styleSet = window.WebChat.createStyleSet(botPreferences.styleSetOptions);

            if (isNewConvo) {
                dlconnectionPromise = createNewDirectLineConnection(directLineOptions, botPreferences, 5);
            } else {
                directLineOptions.token = getCookieForWebchat('token');
                directLineOptions.conversationId = getCookieForWebchat('conversationId');
                // directLineOptions.streamUrl = getCookieForWebchat('streamUrl');
                directLineOptions.watermark = getCookieForWebchat('watermark');
                dlconnectionPromise = loadOldDirectLineConnection(directLineOptions);
            }

            dlconnectionPromise
                .then(function(dlconnection) {

                    var store = window.WebChat.createStore({}, function(arg) {
                        var dispatch = arg.dispatch;
                        return function(next) {
                            return function(action) {
                                // The following code is for convenience only. It not required for production.
                                switch (action.type) {
                                    case 'DIRECT_LINE/CONNECT_FULFILLED':
                                        resolve(dlconnection);
                                        break;
                                    case 'DIRECT_LINE/CONNECT_REJECTED':
                                        reject(new Error("DirectLine failed to connect."));
                                        break;
                                }
                                return next(action);
                            };
                        };
                    });

                    /********************************
                     * We are using Botchat v4.4.1 *
                     ********************************/
                    var renderWebChatOptions = {
                        directLine: dlconnection,
                        styleSet: botPreferences.styleSet,
                        username: botPreferences.user.name,
                        disabled: false,
                        store: store,
                        webSpeechPonyfillFactory: window.WebChat.createBrowserWebSpeechPonyfillFactory()
                    };
                    if (botPreferences.user.id) renderWebChatOptions.userID = botPreferences.user.id;
                    window.WebChat.renderWebChat(renderWebChatOptions, document.getElementById('botChat'));

                    document.querySelector('#botChat > *').focus();

                    return dlconnection;
                })
                .catch(function(error) {
                    // console.log(rror);
                    reject(error);
                });
        });
    }

    /**
     * Creates a new DirectLine connection for the user and the bot
     * It requests a token from the backoffice of the bot and then it uses it to initialize the conversation
     *
     * @returns {Promise<DirectLine | never>}
     */
    function createNewDirectLineConnection(directLineOptions, botPreferences, retries) {
        if (botPreferences.tokenURL) {
            return window.fetch(botPreferences.tokenURL, { method: 'POST' })
                .then(function(res) {
                    return res.json();
                })
                .then(function(json) {
                    if (!json.token) throw new Error("There is no token on the response of HBF");
                    directLineOptions.token = json.token;
                    return new window.WebChat.createDirectLine(directLineOptions);
                })
                .catch(function(error) {
                    // Here most probably will be reported an error from HBF
                    // console.error("Error while creating new DL Connection (remaining retries: " + retries + "): " + error);
                    if (retries > 0) {
                        return delay(500)
                            .then(function() {
                                return createNewDirectLineConnection(botPreferences, retries - 1);
                            })
                            .then(function(value) {
                                return value;
                            })
                            .catch(function(reason) {
                                throw reason;
                            });
                    } else {
                        throw error;
                    }
                });
        } else if (botPreferences.secret) {
            directLineOptions.secret = botPreferences.secret;
            return Promise.resolve(new window.WebChat.createDirectLine(directLineOptions));
        } else {
            throw new Error('Neither token nor secret were passed for the bot');
        }
    }

    function delay(ms) {
        return new Promise(function(resolve) {
            setTimeout(resolve, ms);
        });
    }

    /**
     * Loads an old DirectLine Connection for the user and the bot
     * It uses the data stored in the cookies and reconnects, fetching the history too.
     */
    function loadOldDirectLineConnection(directLineOptions) {
        return new Promise(function(resolve, reject) {
            var hasToken = !!directLineOptions.token;
            var hasConversationId = !!directLineOptions.conversationId;

            if (!hasToken) {
                reject(new Error("Trying to load old conversation without token"));
            } else if (!hasConversationId) {
                reject(new Error("Trying to load old conversation without conversationId"));
            } else {
                resolve(new window.WebChat.createDirectLine(directLineOptions));
            }
        });
    }

    /**
     * Checks if there is a conversation stored in the cookies
     *
     * @returns {boolean}
     */
    function isNewConversation() {
        var convId = getCookieForWebchat('conversationId');
        return !convId;
    }

    /**
     * Checks if there is a User
     *
     * @returns {boolean}
     */
    function isNewUser() {
        var wcUserId = getCookieForWebchat('wcUserId');
        return !wcUserId;
    }

    /**
     * Sets a cookie
     *
     * @param cname
     * @param cvalue
     * @param exdays
     */
    function setCookieForWebchat(cname, cvalue, exdays) {
        var cookie = cname + "=" + cvalue + ";";
        if (exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString() + ";";
            cookie += expires
        }
        if (window.location.host.indexOf('localhost') === -1) {
            cookie += "domain=" + window.location.host + ";";
        }
        cookie += "path=" + window.location.pathname;
        document.cookie = cookie;
    }

    /**
     * Deletes a cookie
     *
     * @param cname
     */
    function deleteCookieForWebchat(cname) {
        var cookie = cname + "=;";
        var d = new Date();
        d.setTime(1);
        var expires = "expires=" + d.toUTCString() + ";";
        cookie += expires;
        if (window.location.host.indexOf('localhost') === -1) {
            cookie += "domain=" + window.location.host + ";";
        }
        cookie += "path=" + window.location.pathname;
        document.cookie = cookie;
    }

    /**
     * Fetches a cookie
     *
     * @param cname
     * @returns {string}
     */
    function getCookieForWebchat(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

}());
